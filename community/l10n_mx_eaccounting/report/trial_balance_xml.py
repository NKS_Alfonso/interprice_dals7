# -*- encoding: utf-8 -*-
# ##########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Openpyme (<http://openpyme.mx>)
#
#
#    Coded by: Salvador Martínez (chavamm.83@gmail.com)
#              Miguel Angel Villafuerte Ojeda (mikeshot@gmail.com)
#              Luis Felipe Lores Caignet (luisfqba@gmail.com)
#              Agustín Cruz Lozano (agustin.cruz@openpyme.mx)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
import xml.etree.ElementTree as ET
from .report_to_file import ReportToFile
from openerp.addons.account_financial_report_webkit.report.trial_balance import TrialBalanceWebkit
logger = logging.getLogger(__name__)


class TrialBalanceXML(TrialBalanceWebkit):
    """
    Class to generate and export the trial balance in xml format
    """
    def generate_report(self):
        """
        Creates the trial balance in XML format
        """
        import tempfile
        from datetime import datetime
        period = self._get_info(self.datas, 'period_from', 'account.period')
        time_period = datetime.strptime(period.date_start, "%Y-%m-%d")
        target_move = self._get_info(self.datas, 'target_move', 'trial.balance.webkit')
        Etree = ET.ElementTree()
        balanza = ET.Element('BCE:Balanza')
        #namespace   
        balanza.set("xsi:schemaLocation", 
                    "www.sat.gob.mx/esquemas/ContabilidadE/1_1/BalanzaComprobacion http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/BalanzaComprobacion/BalanzaComprobacion_1_1.xsd")
        balanza.set("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        balanza.set("xmlns:BCE", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/BalanzaComprobacion")

        balanza.set('Version', '1.1')
        balanza.set('RFC', self.localcontext['company'].partner_id.vat_split)
        balanza.set('Mes', str(time_period.month).rjust(2, '0'))
        balanza.set('Anio', str(time_period.year))
        balanza.set('TipoEnvio', self.datas['type_send'])
        # If type_send equals to "C" puts the value of the "stop_period" field
        # as last modification of trial balance
        if self.datas['type_send'] == "C":
            balanza.set('FechaModBal', self.datas['last_modification'])

        Etree._setroot(balanza)

        periodList = self.pool['account.period'].search(self.cr, self.uid,
                                                        [('date_start', '<', period.date_start)],
                                                        context=None)
        periodList = periodList if len(periodList) > 0 else [-1]
        periodList = str(periodList)[1:-1]

        for account in self.objects:
            account_ids = self.pool['account.account'].search(self.cr, self.uid,
                                                              [('parent_id', 'child_of', account.id)],
                                                              context=None)
            if account.sat_group_id:
                account_ids = account_ids if len(account_ids) > 0 else [-1]
                account_ids = str(account_ids)[1:-1]
                if target_move.id == 'posted':
                    initialBalancePast = "WHERE aa.id IN (%s) AND aml.period_id IN (%s) " \
                                         "AND am.state IN ('posted')" \
                                       % (account_ids, periodList)
                    moveState = "WHERE aa.id IN (%s) AND aml.period_id=%s " \
                                "AND am.state IN ('posted')" \
                                % (account_ids, period.id)
                else:
                    initialBalancePast = "WHERE aa.id IN (%s) AND aml.period_id IN (%s) " \
                                         "AND am.state IN ('posted','draft')" \
                                         % (account_ids, periodList)
                    moveState = "WHERE aa.id IN (%s) AND aml.period_id=%s " \
                                "AND am.state IN ('posted','draft')" \
                                % (account_ids, period.id)
                query = """ SELECT sum(aml.debit) AS debit,
                            sum(aml.credit) AS credit,
                            (select sum(aml.debit) -  sum(aml.credit)
                                FROM account_account aa
                                INNER JOIN account_move_line aml
                                ON aa.id=aml.account_id
                                INNER JOIN account_move am
                                ON aml.move_id=am.id
                                %s) as initialbalance
                            FROM account_account aa
                            INNER JOIN account_move_line aml
                            ON aa.id=aml.account_id
                            INNER JOIN account_move am
                            ON aml.move_id=am.id
                            %s """ % (initialBalancePast, moveState)

                self.cr.execute(query)
                accounts = self.cr.dictfetchone()

                debit = accounts.get('debit') or 0.0
                credit = accounts.get('credit') or 0.0
                initialbalance = accounts.get('initialbalance') or 0.0
                cumulatedbalance = initialbalance + debit - credit or 0.0

                cuenta = ET.SubElement(balanza, 'BCE:Ctas')
                cuenta.set("NumCta", str(account.code))
                cuenta.set("SaldoIni", str(initialbalance))
                cuenta.set("Debe", str(debit))
                cuenta.set("Haber", str(credit))
                cuenta.set("SaldoFin", str(cumulatedbalance))

        # Write data into temporal file
        with tempfile.NamedTemporaryFile(delete=False) as report:
            Etree.write(report.name, encoding='UTF-8')
            fname = report.name

        return fname

ReportToFile(
    'report.account.account_report_trial_balance_xml',
    'account.account', parser=TrialBalanceXML
)
