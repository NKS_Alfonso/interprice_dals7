# -*- coding: utf-8 -*-

{
	'name': 'Configuracion-Usuarios',
	'version': '1.0',
	'category': 'Contabilidad',
	'author': 'Israel Cabrera Juarez(gvdeto)',
	'maintainer': '',
	'website': '',
	#'images' : ['ventas/static/images/ventas.jpg'],
	# 'icon': "ventas/static/src/img/icon.png",
	'installable': True, 
	'active': False,
	'description': 'Configuracion',
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base','sale','sale_stock'
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		
		#Archivo principal de menús
		
		#------------------------------Devoluciones ----------------#
		'secciones/res_users/res_users.xml',
		
		
		
		
		

		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
