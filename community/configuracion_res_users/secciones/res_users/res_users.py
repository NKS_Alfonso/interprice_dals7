# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                                #
#  @creacion    :  (aaaa/mm/dd)                                                                                                                      #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _

from openerp.tools.safe_eval import safe_eval as eval
import pytz
from openerp import SUPERUSER_ID

class sale_order(osv.osv):
    #funcion sobreescrita para restringir que un vendedor pueda vender solo del almacen Facturacion ligado al
    #centro de costos al que esta ligado.
    def _get_default_warehouse(self, cr, uid, context=None):
        #---------------add-------------------------------------------
        obj_res_user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        centro_costo_id = obj_res_user.centro_costo_id.id
        #-------------------------------------------------------------
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id),('centro_costo_id','=',centro_costo_id),('type','=','fac')], context=context)
        if not warehouse_ids:
           return False
        return warehouse_ids[0]
    
    
    def _get_default_warehouse_sales(self, cr, uid, context=None):
        #---------------add-------------------------------------------
        obj_res_user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        centro_costo_id = obj_res_user.centro_costo_id.id
        #-------------------------------------------------------------
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id),('centro_costo_id','=',centro_costo_id),('type','=','fac')], context=context)
        if not warehouse_ids:
           return False
        return warehouse_ids[0]
    
    
    _inherit = 'sale.order'
    _columns = {
                'warehouse_id_sales' : fields.many2one('stock.warehouse','Almacen de ventas'),
             }
    
    _defaults = {
        'warehouse_id': _get_default_warehouse,
        'warehouse_id_sales' : _get_default_warehouse_sales,
                }
    
sale_order()

class res_users(osv.osv):
    _inherit = 'res.users'
    
    _columns={
        'centro_costo_id':fields.many2one('account.cost.center','Centro de costos'),
        }

res_users()





# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
