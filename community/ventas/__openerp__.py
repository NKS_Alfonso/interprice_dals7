# -*- coding: utf-8 -*-

{
	'name': 'Ventas Consignacion',
	'version': '1.0',
	'category': 'Ventas Consignacion',
	'author': 'Israel Cabrera Juarez(gvdeto)',
	'maintainer': '',
	'website': '',
	'images' : ['ventas/static/images/ventas.jpg'],
	# 'icon': "ventas/static/src/img/icon.png",
	'installable': True, 
	'active': False,
	'description': 'Ventas',
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base',
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		
		#Archivo principal de menús
		'menus.xml',
		
		#------------------------------Ventas ----------------#
		'secciones/venta/venta.xml',
		#------------------------------Productos ----------------#
		'secciones/producto/producto.xml',
		#------------------------------Vendedor ----------------#
		'secciones/vendedor/vendedor.xml',
		#configuracion
		'secciones/res_config/res_config.xml'
		
		
		

		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
