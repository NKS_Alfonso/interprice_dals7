# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                            #
#  @creacion    :  (aaaa/mm/dd)                                                                                                            #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
from openerp.osv import fields, osv
from datetime import datetime, date

#Modulo :: Producto
class _gv_producto( osv.osv ) :

  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                         Variables Privadas y Publicas (No variables del API de OPENERP)                                          #
  #                                                                                                                                                  #
  ####################################################################################################################################################  
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Privados (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Publicos (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  def create_folio_sale_order(self, cr, uid, ids, context=None):
      """
      Asigna un nuevo folio para crear una orden de venta
      """
      # Asignando un folio a la venta consignacion
      name_order = ""
      cr.execute('SELECT COUNT(*) FROM sale_order')
      count_folio = cr.fetchone()[0]
      if ( count_folio == 0 ) :    
          name_order = 'SO001'
      else:
          cr.execute('select substr(name,4,2 ) from sale_order order by id desc')
          folio = int(cr.fetchone()[0]) +1
          if( folio >=1 and folio <=10 ) :
              cr.execute('select substr(name,1,4 ) from sale_order order by id desc')
              folio_aux = cr.fetchone()[0]
              name_order = str(folio_aux) + str(folio)
      return name_order
    
  def create_sale_order(self, cr, uid, ids, context=None):
      """
      Crea una nueva orden de venta
      """
      cr.execute("SELECT nombre FROM _gv_venta_consignacion ORDER BY id desc")
      origen = cr.fetchone()[0]
      sale_order = self.pool.get('sale.order')
      cr.execute("""SELECT current_date""")
      fecha = cr.fetchone()[0]
      name_order = self.create_folio_sale_order(cr, uid, ids, context=context)
      #se crea la orden de compra
      for partner in self.browse(cr, uid, ids, context=context):
          partner_id = partner.res_partner_id
          warehouse_id = partner.warehouse_id
          if (partner_id) :
              sale_order.create(cr, uid, {
                  'name': name_order,
                  'partner_id': int(partner_id),
                  'date_order': str(fecha),
                  'warehouse_id': int(warehouse_id),
                  'picking_policy': 'direct',
                  'order_policy': 'manual',
                  'origin':str(origen),
                  }, context=context)
          else:
            raise osv.except_osv( ('Alerta!'), ('Seleccione un cliente') )
           
          
  def create_sale_order_lines(self, cr, uid, ids, context=None):
        """
        Crea las lineas en la orden de venta ya creada
        """
        self.create_sale_order(cr, uid, ids, context=context)
        #se crean las lineas de la orden
        sale_order_line = self.pool.get('sale.order.line')
        for producto in self.browse(cr, uid, ids, context=context):
            product_id = producto.nombre
            product_uom_qty = producto.cantidad
            # cr.execute("""SELECT lst_price FROM product_product WHERE id=%s""",(product_id,))
            # lst_price = cr.fetchone()[0]
            cr.execute("""SELECT id FROM sale_order ORDER BY id desc""")
            order_id = cr.fetchone()[0]
            cr.execute("""SELECT name_template FROM product_product  WHERE id=%s""",(int(product_id),))
            name_template = cr.fetchone()[0]
            sale_order_line.create(cr, uid, {
                        'name': str(name_template),
                        'order_id': order_id,
                        'product_id': int(product_id),
                        'product_uom_qty': product_uom_qty,
                        # 'price_unit': lst_price,
                        # 'pricelist_id':float(2.00)
                        }, context=context)
            self.write(cr, uid, ids, {'state': 'facturado'}, context=context)
        return True

  #---------------------------------------------------------------------------------------------------------------------------------------------------
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                          Metodos OPENERP de Procesos Publicos (No metodos para reportes)                                         #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  STATE_SELECTION = [
        ('abierto', 'Abierto'),
        ('facturado', 'Facturado'),
        ('cancelado', 'Cancelado'),
        ('finalizado', 'finalizado'),
    ]
    
  def cancelacion(self, cr, uid, ids, context=None):
       """cancela el producto asignado"""
       self.write(cr, uid, ids, {'state': 'cancelado'}, context=context)
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                          OPENERP Metodos ORM                                                                 ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                            Metodos OnChange                                                                  ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  def onchage_actualiza_facturar(self, cr, uid, ids, facturar) :
      """
      Onchange que se ejecuta en cuando el campo facturar es seleccionado,
      Actualiza el valor del campo con un True o False
      """
      try:
          if (facturar == True) :
            for id in ids:
                cr.execute("""UPDATE _gv_producto_consignacion SET facturar=True WHERE id=%s""",(id,))
              
            return {'value' : {}}
          else:
             for id in ids:
                cr.execute("""UPDATE _gv_producto_consignacion SET facturar=False WHERE id=%s""",(id,))
              
             return {'value' : {}}
      except:
        return {'value' : {}}
        
  def onchage_actualiza_cancelar(self, cr, uid, ids, cancelar) :
      """Onchange que se ejecuta en cuando el campo cancelar es seleccionado,
         Actualiza el valor del campo con un True o False"""
      try:
        if (cancelar == True) :
          for id in ids:
              cr.execute("""UPDATE _gv_producto_consignacion SET cancelar=True WHERE id=%s""",(id,))
            
          return {'value' : {}}
        else:
           for id in ids:
              cr.execute("""UPDATE _gv_producto_consignacion SET cancelar=False WHERE id=%s""",(id,))
            
           return {'value' : {}}
      except:
        return {'value' : {}}
      
  def onchage_actualiza_consignatario(self, cr, uid, ids, res_partner_id) :
      # raise osv.except_osv( ('Alerta!'), ('!=X-')+str(res_partner_id))
      """Onchange que se ejecuta en cuando el campo res_partner_id es seleccionado,
         Actualiza el valor del campo el seleccionado res_partner_id"""
      try:
        for id in ids:
            cr.execute("""UPDATE _gv_producto_consignacion SET res_partner_id=%s WHERE id=%s""",(int(res_partner_id),id,))
          
        return {'value' : {}}
      except:
        return {'value' : {}}
      
  def onchage_actualiza_cantidad_facturada(self, cr, uid, ids, cantidad_facturada) :
      """Onchange que se ejecuta en cuando el campo cantidad_facturada es seleccionado,
         Actualiza el valor del campo el seleccionado cantidad_facturada"""
      try:
          for id in ids:
              cr.execute("""UPDATE _gv_producto_consignacion SET cantidad_facturada=%s WHERE id=%s""",(float(cantidad_facturada),id,))
              cr.commit()
          return {'value' : {}}
      except:
        return {'value' : {}}
      
  def onchage_actualiza_cantidad_regresada(self, cr, uid, ids, cantidad_regresada) :
      """Onchange que se ejecuta en cuando el campo cantidad_regresada es seleccionado,
         Actualiza el valor del campo el seleccionado cantidad_regresada"""
      try:
          for id in ids:
              cr.execute("""UPDATE _gv_producto_consignacion SET cantidad_regresada=%s WHERE id=%s""",(float(cantidad_regresada),id,))
            
          return {'value' : {}}
      except:
        return {'value' : {}}
  
  
  def onchage_actualiza_total(self, cr, uid, ids,costo, cantidad,nombre, warehouse_id,context=None) :
      """Onchange que se ejecuta en cuando el campo costo es seleccionado,
         Actualiza el valor del campo el seleccionado costo_total"""
      disponible = 0
      try:
        cr.execute("""SELECT SUM(sq.qty)
                          FROM stock_warehouse AS sw INNER JOIN 
                          stock_location AS sl ON sw.lot_stock_id = sl.id
                          INNER JOIN stock_quant sq ON sl.id=sq.location_id
                          WHERE sl.usage='internal' and sw.id=%s
                          and product_id=%s""",(warehouse_id, nombre))     
        if cr.rowcount:
            quants = cr.fetchone()[0]
            if quants is None or quants is False or quants==0.0 or quants < cantidad:
                    if quants > 0:
                      disponible = quants
                    return {'value' : {'nombre':0,'cantidad':disponible,'costo_total':costo*cantidad,'cantidad':disponible,'costo':0.0},
                      'warning':{'title':'Alerta!!',
                                 'message':('No hay suficiente stock! Preve vender %s unidades pero solo %s estan disponibles en esta ubicacion') % \
                                  (cantidad,disponible)}}
            else:
                return {'value' : {'costo_total':costo*cantidad}}
      except:
        return {}
    
      
  def onchage_imprime(self, cr, uid, ids, pricelist_id,nombre,cantidad):
     try:
       if not nombre:
         pass
       else:
           tarifa = self.pool.get('product.pricelist').price_get(cr, uid, pricelist_id, nombre, cantidad or 1.00, partner=None, context=None)
           for f in tarifa :
             price_unit = tarifa[f]
           if (price_unit == False) :
              cr.execute("""SELECT product_tmpl_id FROM product_product WHERE id=%s""",(nombre,))
              product_tmpl_id = cr.fetchone()[0]
              cr.execute("SELECT list_price FROM product_template WHERE id=%s",(product_tmpl_id,))
              list_price = cr.fetchone()[0]
              return {'value' : {'costo': float(list_price)},
                 # 'warning':{'title': 'Alerta', 'message': 'SIN RESULTADO '+str(product_tmpl_id)}
                  }
           else:
              return {'value' : {'costo': float(price_unit)},
                 # 'warning':{'title': 'Alerta', 'message': 'resultado : '+str(nombre)}
                  }
     except:
        return {'value' : {}}
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                      Metodos para campos "function"                                                          ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  def _get_estado_ventas(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene el estado de la venta consignacion en la que se encuentra posicionado el usuario"""
      try:
          result = {}
          mostrar = False
          for id in ids:
            cr.execute('''SELECT vc.state FROM _gv_venta_consignacion AS vc
                          JOIN  _gv_producto_consignacion AS pc ON vc.id=ventas_id WHERE pc.id=%s''',(id,)),
            estado_actual = cr.fetchone()[0]
            if( estado_actual == 'borrador'):
              mostrar = True
    
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = mostrar
          return	result
      except:
        return result
      
  def _get_total(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene el estado de la venta consignacion en la que se encuentra posicionado el usuario"""
      try:
          result = {}
          costo_total = 0.00
          for id in ids:
            cr.execute('''SELECT costo_total FROM _gv_venta_consignacion AS vc
                          JOIN  _gv_producto_consignacion AS pc ON vc.id=ventas_id WHERE pc.id=%s''',(id,)),
            costo_total = cr.fetchone()[0]
            # if( estado_actual == 'borrador'  or estado_actual == 'confirmado'):
            #   mostrar = True
    
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = costo_total
          return	result
      except:
        return result
      
  def _get_facturados(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene el estado de la venta consignacion en la que se encuentra posicionado el usuario"""
      try:
          result = {}
          facturados = 0.00
          for id in ids:
            cr.execute('''SELECT facturados FROM _gv_venta_consignacion AS vc
                          JOIN  _gv_producto_consignacion AS pc ON vc.id=ventas_id WHERE pc.id=%s''',(id,)),
            facturados = cr.fetchone()[0]
            # if( estado_actual == 'borrador'  or estado_actual == 'confirmado'):
            #   mostrar = True
    
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = facturados
          return	result
      except:
        return result
  
  def _get_regresados(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene el estado de la venta consignacion en la que se encuentra posicionado el usuario"""
      try:
          result = {}
          regresados = 0.00
          for id in ids:
            cr.execute('''SELECT regresados FROM _gv_venta_consignacion AS vc
                          JOIN  _gv_producto_consignacion AS pc ON vc.id=ventas_id WHERE pc.id=%s''',(id,)),
            regresados = cr.fetchone()[0]
            # if( estado_actual == 'borrador'  or estado_actual == 'confirmado'):
            #   mostrar = True
    
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = regresados
          return	result
      except:
        return result
    
  def _get_state_picking(self, cr, uid, ids, name, arg, context = {}  ):
      """regresa el estado actual del albaran relacionado a este producto"""
      result = {}
      mostrar = False
      state = ""
      estado_picking = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).stock_piking_id.state
      if (estado_picking == 'draft'):  state = "Borrador"
      elif(estado_picking == 'cancel'): state = "Cancelado"
      elif(estado_picking == 'waiting'): state = "Esperando otra operacion"
      elif(estado_picking == 'confirmed'): state = "Esperando disponibilidad"
      elif(estado_picking == 'partially_available'): state = "Parcialmente Disponible"
      elif(estado_picking == 'assigned'): state = "Listo para transferir"
      elif(estado_picking == 'done'): state = "Transferido"
      else: state = ''
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = state
      return result
  
  def _get_state_sale_order(self, cr, uid, ids, name, arg, context = {}  ):
      """regresa el estado actual de la venta relacionado con el producto"""
      result = {}
      mostrar = False
      state = ""
      estado_order = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).sale_order_id.state
      if (estado_order == 'draft'):  state = "Borrador"
      elif(estado_order == 'sent'): state = "Cotizaciones enviadas"
      elif(estado_order == 'cancel'): state = "Cancelado"
      elif(estado_order == 'waiting_date'): state = "Esperando fecha planificada"
      elif(estado_order == 'progress'): state = "Pedido de venta"
      elif(estado_order == 'manual'): state = "Pedido a facturar"
      elif(estado_order == 'shipping_exept'): state = "Exepcion de envio"
      elif(estado_order == 'invoice_exept'): state = "Exepcion de factura"
      elif(estado_order == 'done'): state = "Completado"
      else: state = ''
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = state
      return result
    
  def _get_state_vc(self, cr, uid, ids, name, arg, context = {}  ):
      """regresa el estado actual de la venta  consignacion relacionada con el producto"""
      try:
        result = {}
        mostrar = False
        estado = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).ventas_id.state
        if (estado == 'borrador'):  mostrar = True
        else: mostrar = False
        for record in self.browse( cr, uid, ids, context = context):
            result[record.id] = mostrar
        return result
      except:return {}
  def _get_state_albaran_venta(self, cr, uid, ids, name, arg, context = {}  ):
      """regresa obtiene el estado del albaran principal de esta venta"""
      try:
        result = {}
        mostrar = False
        estado = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).ventas_id.state
        if (estado == 'abierto'):
          estado_picking = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).ventas_id.state_picking
          if estado_picking != 'Transferido':   mostrar = True
        for record in self.browse( cr, uid, ids, context = context):
            result[record.id] = mostrar
        return result
      except:return {}
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                  Atributos basicos de un modelo OPENERP                                                      ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Nombre del modelo
  _name = 'gv.producto'
  
  #Nombre de la tabla
  _table = '_gv_producto_consignacion'
  
  #Nombre de la descripcion al usuario en las relaciones m2o hacia este módulo
  _rec_name = 'nombre'
  
  #Cláusula SQL "ORDER BY"
  _order = 'res_partner_id'

  #Columnas y/o campos Tree & Form
  
  _columns = {
    
    # =========================================  OpenERP Campos Basicos (integer, char, text, float, etc...)  ====================================== #
    'nombre' : fields.many2one( 'product.product', required = True, readonly = True, store = True, help = "Guarda el id de los productos"),
    'cantidad': fields.float('Cantidad', required = True ),
    'state': fields.selection(STATE_SELECTION, 'Estado', readonly=True, select=True, copy=False),
    'facturar': fields.boolean('Facturar'),
    'cancelar': fields.boolean('Regresar'),
    'cantidad_facturada' : fields.float('Facturar'),
    'cantidad_regresada' : fields.float('Regresar'),
    'costo': fields.float('Costo'),
    'costo_total': fields.float('Total'),
    'facturados': fields.float('Facturados'),
    'regresados': fields.float('cancelados'),
    'fecha_factura_cancela': fields.datetime("Fecha"),
    
    # ========================================================  Relaciones [many2one](m2o) ========================================================= #
    'ventas_id' : fields.many2one ( 'gv.venta', 'Venta', help="Guarda el id de la venta a la que este ligido" ),
    'id_ventas' : fields.many2one ( 'gv.venta', 'Venta', help="Guarda el id de la venta a la que este ligido" ),
    'res_partner_id' : fields.many2one ('res.partner', 'Cliente',
                                         help="Guarda el id del cliente"),
    'warehouse_id': fields.many2one('stock.warehouse', 'Almacén', required = True),
    'pricelist_id': fields.many2one('product.pricelist', 'Tarifa', required=True,),
    'sale_order_id': fields.many2one('sale.order', 'SO'),
    'stock_piking_id': fields.many2one('stock.picking', 'Albarán'),
    # ========================================================  Relaciones [one2many](o2m) ========================================================= #
    
    # ========================================================  Relaciones [many2many](m2m) ======================================================== #
    
    # ======================================================== Campos "function" (function) ======================================================== #
    'estado_venta': fields.function(_get_estado_ventas,
                                    help='Obtiene el estado actual de la venta consignación a la que esta ligado',
                                    type='boolean',
                                    method=True),
    'costo_total_function': fields.function(_get_total, store=True),
    'facturados_function': fields.function(_get_facturados),
    'regresados_function': fields.function(_get_regresados),
    #subir al servidor
    'state_sale_order': fields.char("char"),
    'state_stok_picking': fields.char("char"),
    'state_vc': fields.function(_get_state_vc, type="boolean"),
    'state_stok_picking_vc': fields.function(_get_state_albaran_venta, type="boolean")
    
  }
  
  #Valores por defecto de los campos del diccionario [_columns]
  _defaults = {
    'state': 'abierto',
    'estado_venta' : lambda self, cr, uid, context : ( context['estado_venta'] ) if ( context and ( 'estado_venta' in context ) ) else ( None ),
    'warehouse_id' : lambda self, cr, uid, context : ( context['warehouse_id'] ) if ( context and ( 'warehouse_id' in context ) ) else ( None ),
    'pricelist_id' : lambda self, cr, uid, context : ( context['pricelist_id'] ) if ( context and ( 'pricelist_id' in context ) ) else ( None ),
    'res_partner_id' : lambda self, cr, uid, context : ( context['res_partner_id'] ) if ( context and ( 'res_partner_id' in context ) ) else ( None ),
    'facturados': 0.00,
    'regresados': 0.00
  }
  
  #Restricciones de BD (constraints)
  _sql_constraints = []
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                              Metodos para validacion de la lista: [_constraints]                                             ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Restricciones desde codigo
  _constraints = []
  
_gv_producto()


# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
