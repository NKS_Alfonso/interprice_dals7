# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                            #
#  @creacion    : (aaaa/mm/dd)                                                                                                            #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
import logging
_logger = logging.getLogger(__name__)

#Modulo :: Venta
class _gv_venta( osv.osv ) :

  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                         Variables Privadas y Publicas (No variables del API de OPENERP)                                          #
  #                                                                                                                                                  #
  ####################################################################################################################################################  
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Privados (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Publicos (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  #################################################################################################################################################### 
  def venta_confirmacion(self, cr, uid, ids, context=None):
       """
       confirma la venta: pasa de estado borrador a confirmacion
       """
       for estado in self.browse(cr, uid, ids, context=context):
           estado_venta = estado.state
           if (estado_venta == 'borrador') : 
              self.write(cr, uid, ids, {'state': 'confirmado'}, context=context)
           elif(estado_venta == 'confirmado') :
              self.write(cr, uid, ids, {'state': 'abierto'}, context=context)
           elif(estado_venta == 'abierto') :
                if (estado.total_venta > 0):
                    raise osv.except_osv( ('Alerta!'), ('No se puede finalizar una venta con un monto mayor que 0.00'))
                else:
                    self.write(cr, uid, ids, {'state': 'filalizado'}, context=context)
               
       
  def venta_cancelar(self, cr, uid, ids, context=None):
      """
      Cancela la venta consignacion mientras todos los productos esten en estado abierto
      """
      for id in ids:
       cr.execute("""SELECT COUNT(*) FROM _gv_producto_consignacion
                     WHERE state in('facturado') AND ventas_id=%s""",(id,))
       count = cr.fetchone()[0]
       if (count >= 1) :
         raise osv.except_osv( ('Alerta!'), ('No se puede cancelar una venta consignación que tiene lineas facturadas') )
      else:
        cr.execute("UPDATE _gv_producto_consignacion SET state='cancelado' WHERE ventas_id=%s",(id,))
        self.write(cr, uid, ids, {'state': 'cancelado'}, context=context)
        
       
  def seleccionar(self, cr, uid, ids, context=None):
       """
       Selecciona todos los productos consigandos para ser facturados
       mientras no se encuentren en estado cancelado o facturado
       """
       for id in ids:
           cr.execute("UPDATE _gv_producto_consignacion SET facturar=True WHERE state in('abierto') and ventas_id=%s",(id,))
       return True
      
  def create_folio_sale_order(self, cr, uid, ids, context=None):
     """
     Asigna un nuevo folio para crear una orden de venta
     """
     sufbijo = "SO"
     sufbijo10 = "SO0"
     name = ""
     #--------------------secuencias----------------------------------------------------
     cr.execute("""SELECT COUNT(name) FROM sale_order""")
     count = cr.fetchone()[0]
     if( count == 0) :
          name = "SO001"
     else:
       cr.execute('select substr(name,3,1 ) from sale_order order by id desc')
       pos_3 =  cr.fetchone()[0]
       cr.execute('select substr(name,4,1 ) from sale_order order by id desc')
       pos_4 =  cr.fetchone()[0]
       if ( pos_3 == '0' and pos_4 == '0') :
            cr.execute('select substr(name,5,100 ) from sale_order order by id desc')
            pos_5 =  cr.fetchone()[0]
            if( pos_5 == '9'):
                name = "SO010"
            else:
               pos_6 = int(pos_5) + 1
               name = sufbijo + str(pos_3) + str(pos_4) +str(pos_6)
       elif(pos_3 == '0' and pos_4 != '0'):
            cr.execute('select substr(name,4,100 ) from sale_order order by id desc')
            pos_7 =  cr.fetchone()[0]
            if( pos_7 == '99'):
               name = "SO100"
            else:
               pos_8 = int(pos_7) + 1
               name = sufbijo10 + str(pos_8)
       else:
            cr.execute('select substr(name,3,100 ) from sale_order order by id desc')
            pos_9 =  int(cr.fetchone()[0]) + 1
            name = sufbijo + str(pos_9)
     return name
     #-----------------------------------------------------------------------------  
 
  def create_sale_order(self, cr, uid, ids, res_partner_id, context=None):
      """
      Crea una nueva orden de venta
      """
      
      sale_order = self.pool.get('sale.order')
      for id in ids:
        cr.execute("SELECT nombre FROM _gv_venta_consignacion WHERE id=%s",(id,))
        origen = cr.fetchone()[0]
      
      name_order = self.create_folio_sale_order(cr, uid, ids, context=context)
      # raise osv.except_osv( ('Alerta!'), ('xxxX-')+str(name_order))
      for venta in self.browse(cr, uid, ids, context= context) :
          warehouse_id = venta.warehouse_id
          pricelist_id = venta.pricelist_id
          vendedor_id = venta.vendedor_id
      sale_order.create(cr, uid, {
          'name': name_order,
          'partner_id': int(res_partner_id),
          'warehouse_id': int(warehouse_id),
          'picking_policy': 'direct',
          'order_policy': 'picking',
          'origin':str(origen),
          'pricelist_id':int(pricelist_id),
          }, context=context)
          
  def factura_grupal(self, cr, uid, ids, context=None):
      """
      Factura todos los productos seleccionados que se encuentren en estado abierto
      """
      producto = self.pool.get('gv.producto')
      contador = 0
      partner_id = 0
      #-------------------------obtiene la ubicacion del productoa consigna------------
      for location in self.browse(cr, uid, ids, context=context):
          warehouse_id = location.warehouse_id
          location_id=location.stock_location_id.id
          total_venta = location.total_venta
          pricelist_id = location.pricelist_id
          res_partner = location.vendedor_id
          id_location = location.stock_warehouse_id.id_location
          id_warehouse=location.stock_warehouse_id.id_warehouse.id
      #--------------------------------------------------------------------------------
      for id in ids:
       cr.execute("""SELECT COUNT(cantidad_facturada) FROM _gv_venta_consignacion AS vc 
                     INNER JOIN _gv_producto_consignacion AS pc ON vc.id=pc.ventas_id
                     WHERE pc.state in('abierto') AND (pc.cantidad_facturada>0 AND vc.id=%s)""",(id,))
       count_facturar = cr.fetchone()[0]
       self.create_sale_order(cr, uid, ids, res_partner, context=context)
       
       if ( count_facturar == 0) : 
          raise osv.except_osv( ('Alerta!'), ('No hay ningun producto seleccionado para facturar o ya han sido cancelados o facturados') )
       else:
            for id in ids:
              cr.execute("""SELECT id FROM _gv_producto_consignacion
                            WHERE state in('abierto') and (cantidad_facturada>0 and ventas_id=%s) order by res_partner_id desc""",(id,))
              lista_productos = cr.fetchall()
              # cr.execute("""SELECT id_warehouse FROM _res_configuracion""")
              # id_warehouse = cr.fetchone()[0]
              # cr.execute("""SELECT id_location FROM _res_configuracion""")
              # id_location = cr.fetchone()[0]
              cr.execute("SELECT nombre FROM _gv_venta_consignacion WHERE id=%s",(id,))
              origen = cr.fetchone()[0]
              stock_move = self.pool.get('stock.move')
              stock_picking=self.pool.get('stock.picking')
              picking_type = self.pool.get('stock.picking.type').search(cr, uid, [('warehouse_id','=',id_warehouse),
                                                                           ('code','=','internal'),
                                                                           ('active','=', True)])
              # raise osv.except_osv( ('Alerta!'), (str(picking_type)))
              new_picking=stock_picking.create(cr, uid, {
                                          'picking_type_id': picking_type[0],
                                          'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                          'origin': str(origen),
                                          'partner_id': int(res_partner),
                                          'picking_consigna':'rg',
                                          }, context=context)
              for recorre in lista_productos :
                  cr.execute("SELECT cantidad_facturada FROM _gv_producto_consignacion where id=%s",(recorre,))
                  cantidad_facturada = cr.fetchone()[0]
                  cr.execute("SELECT cantidad FROM _gv_producto_consignacion where id=%s",(recorre,))
                  cantidad = cr.fetchone()[0]
                 
                  cr.execute("SELECT nombre FROM _gv_producto_consignacion where id=%s",(recorre,))
                  product_id = cr.fetchone()[0]
                  cr.execute("""SELECT upper(translate(name_template,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜñ','aeiouAEIOUaeiouAEIOUÑ'))FROM product_product  WHERE id=%s""",(int(product_id),))
                  name_template = cr.fetchone()[0]
                  cr.execute("""SELECT res_partner_id FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                  res_partner_id = cr.fetchone()[0]
                  sale_order_line = self.pool.get('sale.order.line')
                  cr.execute("""SELECT cantidad FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                  product_uom_qty = cr.fetchone()[0]
                
                  #------------------------------------------------------
                  cr.execute("""SELECT facturados FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                  facturados = cr.fetchone()[0]
                  cr.execute("""SELECT costo FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                  costo = cr.fetchone()[0]
                  cr.execute("""SELECT regresados FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                  regresados = cr.fetchone()[0]
                  # raise osv.except_osv( ('Alerta!'), (str(facturados)+":"+str(regresados)+":"+str(cantidad)+":"+str(cantidad_facturada)))
                  if((facturados+regresados>cantidad) or cantidad_facturada+facturados+regresados>cantidad):
                      raise osv.except_osv( ('Alerta!'), ('La cantidad a facturar sobrepasa la cantidad consignada'))

                  if (facturados < cantidad):
                      if contador == 0 :
                        total_v = total_venta-(costo*cantidad_facturada)
                        contador = 1
                      else:
                        total_v = total_v-(costo*cantidad_facturada)
                    
                      #------------------------------------------------------
                      cr.execute("""SELECT id FROM sale_order ORDER BY id desc""")
                      order_id = cr.fetchone()[0]
                      id_order_line=sale_order_line.create(cr, uid, {
                              'name': unicode(name_template),
                              'order_id': order_id,
                              'product_id': int(product_id),
                              'product_uom_qty': cantidad_facturada,
                              'price_unit':float(costo),
                              }, context=context)
                      
                      contador_while = 1
                      
                      # while (contador_while <= int(cantidad_facturada)):
                      producto.create(cr, uid, {
                                'nombre': product_id,
                                'ventas_id': id,
                                'cantidad': cantidad_facturada,
                                'costo': costo,
                                'res_partner_id': res_partner_id,
                                'state': 'facturado',
                                'fecha_factura_cancela': fields.datetime.now(),
                                'warehouse_id' : int(warehouse_id),
                                'pricelist_id':int(pricelist_id),
                                'sale_order_id': order_id,
                                'stock_piking_id': new_picking
                              
                                }, context=context)
                        # contador_while = contador_while + 1
                    
                      if((facturados+cantidad_facturada)==cantidad or (facturados+regresados+cantidad_facturada)==cantidad):
                        cr.execute("UPDATE _gv_producto_consignacion SET state='finalizado' WHERE id=%s",(recorre,))
                        cr.execute("UPDATE _gv_venta_consignacion SET total_venta=%s WHERE id=%s",(total_v,id))
                        
                      else:
                     
                        cr.execute("UPDATE _gv_venta_consignacion SET total_venta=%s WHERE id=%s",(total_v,id))
                  
                      total_facturados = facturados+cantidad_facturada
                      cr.execute("UPDATE _gv_producto_consignacion SET facturados=%s WHERE id=%s",(total_facturados,recorre,))
                  else:
                      total_facturados = facturados+cantidad_facturada
                      cr.execute("UPDATE _gv_producto_consignacion SET facturados=%s WHERE id=%s",(total_facturados,recorre,))
                  
                  #---------------crea albaran de regreso de prestamos a facturacion
                  id_move=stock_move.create(cr, uid, {
                                        'name': unicode(name_template),
                                        'product_id': int(product_id),
                                        'product_uom_qty': float(cantidad_facturada),
                                        'picking_id' : new_picking,
                                        'product_uom': 1,
                                        'location_id':int(id_location),
                                        'location_dest_id':int(location_id),
                                        # 'price_unit':float(costo),
                                        }, context=context)
                  stock_picking.action_confirm(cr, uid, new_picking, context=None)
                  cr.execute("""UPDATE stock_move SET price_unit=%s WHERE id=%s""",(costo,id_move))
                  #-----------------finaliza la compra si el saldo es iguala cero
                  estado_actual = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state
                  if( estado_actual == 'abierto'):
                    for id in ids:
                         cr.execute("""SELECT total_venta FROM _gv_venta_consignacion
                                       where id=%s""",(id,))
                         total_venta = float(cr.fetchone()[0])
                         if ( total_venta == 0) :
                             time.sleep(2)
                             mostrar = True
                             # self.write(cr, uid, ids, {'state': 'filalizado'}, context=context)
           
  
  def crear_poliza(self, cr, uid, ids, context=None):
      """Crea una poliza con los datos de la venta consignacion"""
      move_line_id = 0
      period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
      folio_consigna = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).nombre
      total_venta = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).total_venta
      journal_id= self.pool.get('account.journal').search(cr, uid, [('code','=',"VC")])[0]
      for obj in self.browse(cr,uid,ids):
          account_move_id = 0
          # res_currency_id = obj.account_currency_id.id
          for u in self.pool.get('res.users').browse(cr,uid,uid):
               company_id = u.company_id.id
      poliza_id = self.pool.get('account.move').create(cr, uid, {'journal_id':journal_id, 'ref':folio_consigna,'date':datetime.datetime.now(),'company_id':company_id}, context=context)
      obj_poliza = self.pool.get('account.move').browse( cr, uid, poliza_id, context=context)
      obj_journal = self.pool.get('account.journal').browse( cr, uid, journal_id, context=context)
      # raise osv.except_osv( ('Alerta!'), ('Datos')+str(obj_poliza.period_id.id)+" : "+str(journal_id)+" : "+str(poliza_id))
      move_line_id=self.pool.get('account.move.line').create(cr, uid, {
                                         'move_id':poliza_id,
                                         'period_id':period_ids[0],
                                         'date':datetime.datetime.now(),
                                         'name': folio_consigna,
                                         'account_id':obj_journal.default_debit_account_id.id,
                                         'debit':total_venta,
                                         'credit':0,
                                         })
      move_line_id=self.pool.get('account.move.line').create(cr, uid, {
                                         'move_id':poliza_id,
                                         'period_id':period_ids[0],
                                         'date':datetime.datetime.now(),
                                         'name': folio_consigna,
                                         'account_id':obj_journal.default_credit_account_id.id,
                                         'debit':0,
                                         'credit':total_venta,
                                         })
      for id in ids:
        cr.execute("UPDATE _gv_venta_consignacion SET account_move_id=%s WHERE id=%s",(poliza_id,id,))
  def crear_albaran(self, cr, uid, ids, context= None):
      # self.crear_poliza(cr, uid, ids, context=None)
      """
      Crea un albaran de salida del almacen de facturacion al almacen de prestamos
      """
      # cr.execute("""SELECT id_location FROM _res_configuracion""")
      # location_dest_id= cr.fetchone()[0]
      stock_picking=self.pool.get('stock.picking')
      stock_move = self.pool.get('stock.move')
      for id in ids:
        cr.execute("SELECT nombre FROM _gv_venta_consignacion WHERE id=%s",(id,))
        origen = cr.fetchone()[0]
        for venta in self.browse(cr, uid, ids, context= context) :
          warehouse_id = venta.warehouse_id
          location = venta.stock_location_id
          partner =  venta.vendedor_id
          location_dest_id = venta.stock_warehouse_id.id_location
          picking_type = self.pool.get('stock.picking.type').search(cr, uid, [('warehouse_id','=',warehouse_id.id),
                                                                              ('code','=','internal'),
                                                                              ('active','=', True)])
          #----------Crea albaran-----------------------------------------------------------------------------------
          
          new_picking=stock_picking.create(cr, uid, {
                                    'picking_type_id': picking_type[0],
                                    'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                    'origin': str(origen),
                                    'partner_id': int(partner),
                                    'picking_consigna':'fp',
                                    }, context=context)
          cr.execute("UPDATE _gv_venta_consignacion SET stock_piking_id=%s WHERE id=%s",(new_picking,id,))
          for id in ids:
              cr.execute("""SELECT COUNT(pc.nombre) FROM _gv_venta_consignacion AS vc 
                            INNER JOIN _gv_producto_consignacion AS pc ON vc.id=pc.ventas_id
                            WHERE vc.id=%s""",(id,))
              count_facturar = cr.fetchone()[0]
              cr.execute("""SELECT COUNT(pc.cantidad) FROM _gv_venta_consignacion AS vc 
                            INNER JOIN _gv_producto_consignacion AS pc ON vc.id=pc.ventas_id
                            WHERE pc.cantidad=0 AND vc.id=%s""",(id,))
              count_cantidad = cr.fetchone()[0]
              if ( count_facturar == 0 or count_cantidad > 0) : 
                 raise osv.except_osv( ('Alerta!'), ('''Verifique que la venta que desea confirmar tenga por lo menos..
                                                        una linea de productos y que las cantidades de cada linea sean
                                                       mayor a cero''') )
              else:
                  for id in ids:
                    cr.execute("""
                                  SELECT id FROM _gv_producto_consignacion
                                  WHERE state in('abierto') and (ventas_id=%s) order by res_partner_id desc""",(id,))
                    lista_productos = cr.fetchall()
                    for venta in self.browse(cr, uid, ids, context= context) :
                        location_id = venta.stock_location_id
                    for recorre in lista_productos :
                        cr.execute("SELECT nombre FROM _gv_producto_consignacion where id=%s",(recorre,))
                        product_id = cr.fetchone()[0]
                        cr.execute("""SELECT upper(translate(name_template,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜñ','aeiouAEIOUaeiouAEIOUÑ'))FROM product_product  WHERE id=%s""",(int(product_id),))
                        name_template = cr.fetchone()[0]
                        cr.execute("""SELECT res_partner_id FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        res_partner_id = cr.fetchone()[0]
                        sale_order_line = self.pool.get('sale.order.line')
                        cr.execute("""SELECT cantidad FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        product_uom_qty = cr.fetchone()[0]
                        cr.execute("""SELECT costo FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        costo = cr.fetchone()[0]
                        
                        #--------------crea las lineas al albaran----------------------------------------------------------------
                        id_move=stock_move.create(cr, uid, {
                                         'name': unicode(name_template),
                                         'product_id': int(product_id),
                                         'product_uom_qty': float(product_uom_qty),
                                         'picking_id' : new_picking,
                                         'product_uom': 1,
                                         'location_id':int(location_id),
                                         'location_dest_id':int(location_dest_id),
                                         'price_unit':float(costo),
                                         }, context=context)
                        cr.execute("""UPDATE stock_move SET price_unit=%s WHERE id=%s""",(costo,id_move))
                    # stock_picking.action_confirm(cr, uid, new_picking, context=None)
                    
                    self.write(cr, uid, ids, {'state': 'abierto'}, context=context)
                    
  def crear_albaran_regreso(self, cr, uid, ids, context= None):
      """
      Crea un albaran de salida del almacen de facturacion al almacen de prestamos
      """
      # raise osv.except_osv( ('Alerta!'), ('!=X-')+str(guardar))
      # cr.execute("""SELECT id_location FROM _res_configuracion""")
      # location_origen= cr.fetchone()[0]
      # cr.execute("""SELECT id_warehouse FROM _res_configuracion""")
      # id_warehouse = cr.fetchone()[0]
      contador = 0
      producto = self.pool.get('gv.producto')
      stock_picking=self.pool.get('stock.picking')
      stock_move = self.pool.get('stock.move')
      for id in ids:
        cr.execute("SELECT nombre FROM _gv_venta_consignacion WHERE id=%s",(id,))
        origen = cr.fetchone()[0]
        for venta in self.browse(cr, uid, ids, context= context) :
          warehouse_id = venta.warehouse_id
          partner =  venta.vendedor_id
          total_venta = venta.total_venta
          pricelist_id =  venta.pricelist_id
          location_origen = venta.stock_warehouse_id.id_location.id
          id_warehouse=venta.stock_warehouse_id.id_warehouse.id
          picking_type = self.pool.get('stock.picking.type').search(cr, uid, [('warehouse_id','=',id_warehouse),
                                                                              ('code','=','internal'),
                                                                              ('active','=', True)])
          # raise osv.except_osv( ('Alerta!'), ('Seleccione un cliente')+str(picking_type) )
          #----------Crea albaran-----------------------------------------------------------------------------------
          new_picking=stock_picking.create(cr, uid, {
                                    'picking_type_id': picking_type[0],
                                    'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                    'origin': str(origen),
                                    'partner_id': int(partner),
                                    'picking_consigna':'rg',
                                    }, context=context)
          for id in ids:
              cr.execute("""SELECT COUNT(cantidad_regresada) FROM _gv_venta_consignacion AS vc 
                            INNER JOIN _gv_producto_consignacion AS pc ON vc.id=pc.ventas_id
                            WHERE pc.state in('abierto') AND (pc.cantidad_regresada>0 AND vc.id=%s)""",(id,))
              count_cancelar = cr.fetchone()[0]
              # raise osv.except_osv( ('Alerta!'), ('!=X-')+str(count_cancelar))
              if ( count_cancelar == 0) : 
                 raise osv.except_osv( ('Alerta!'), ('Seleccione un producto para cancelar') )
              else:
                  for id in ids:
                    cr.execute("""SELECT id FROM _gv_producto_consignacion
                                  WHERE state in('abierto') and (cantidad_regresada>0 and ventas_id=%s) order by res_partner_id desc""",(id,))
                    lista_productos = cr.fetchall()
                    for venta in self.browse(cr, uid, ids, context= context) :
                        location_id = venta.stock_location_id.id
                    for recorre in lista_productos :
                        cr.execute("SELECT nombre FROM _gv_producto_consignacion where id=%s",(recorre,))
                        product_id = cr.fetchone()[0]
                        cr.execute("""SELECT name_template FROM product_product  WHERE id=%s""",(int(product_id),))
                        name_template = cr.fetchone()[0]
                        cr.execute("""SELECT res_partner_id FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        res_partner_id = cr.fetchone()[0]
                        sale_order_line = self.pool.get('sale.order.line')
                        cr.execute("""SELECT cantidad FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        cantidad = cr.fetchone()[0]
                        
                        #----------------
                        cr.execute("SELECT cantidad_regresada FROM _gv_producto_consignacion where id=%s",(recorre,))
                        cantidad_facturada = cr.fetchone()[0]
                        cr.execute("""SELECT costo FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        costo = cr.fetchone()[0]
                       
                        #----------------#############################
                        cr.execute("""SELECT facturados FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        facturados = cr.fetchone()[0]
                        cr.execute("""SELECT regresados FROM _gv_producto_consignacion WHERE id=%s""",(recorre,))
                        regresados = cr.fetchone()[0]
                        if((facturados+regresados>cantidad) or cantidad_facturada+facturados+regresados>cantidad):
                              raise osv.except_osv( ('Alerta!'), ('La cantidad a regresar sobrepasa la cantidad consignada'))
                        if (regresados < cantidad):
                            if contador == 0 :
                              total_v = total_venta-(costo*cantidad_facturada)
                              contador = 1
                            else:
                              total_v = total_v-(costo*cantidad_facturada)
                          
                            
                            if((regresados+cantidad_facturada)==cantidad or (regresados+facturados+cantidad_facturada)==cantidad):
                              cr.execute("UPDATE _gv_producto_consignacion SET state='finalizado' WHERE id=%s",(recorre,))
                              cr.execute("UPDATE _gv_venta_consignacion SET total_venta=%s WHERE id=%s",(total_v,id))
                             
                            else:
                              
                              cr.execute("UPDATE _gv_venta_consignacion SET total_venta=%s WHERE id=%s",(total_v,id))
                        
                            total_facturados = regresados+cantidad_facturada
                            cr.execute("UPDATE _gv_producto_consignacion SET regresados=%s WHERE id=%s",(total_facturados,recorre,))
                        else:
                          
                            total_facturados = regresados+cantidad_facturada
                            cr.execute("UPDATE _gv_producto_consignacion SET facturados=%s WHERE id=%s",(total_facturados,recorre,))
                        #----------------######################################################################3
                        #--------------crea las lineas al albaran----------------------------------------------------------------
                        id_move=stock_move.create(cr, uid, {
                                         'name': unicode(name_template),
                                         'product_id': int(product_id),
                                         'product_uom_qty': float(cantidad_facturada),
                                         'picking_id' : new_picking,
                                         'product_uom': 1,
                                         'location_id':location_origen,
                                         'location_dest_id':int(location_id),
                                         'price_unit':float(costo),
                                         }, context=context)
                        # raise osv.except_osv( ('Alerta!'), ('!=X-')+str(name_template))
                        cr.execute("""UPDATE stock_move SET price_unit=%s WHERE id=%s""",(costo,id_move))
                        #_-----------------------------------------------------------
                        contador_while = 1
                      
                        # while (contador_while <= int(cantidad_facturada)):
                        producto.create(cr, uid, {
                                  'nombre': product_id,
                                  'ventas_id': id,
                                  'cantidad': cantidad_facturada,
                                  'costo': costo,
                                  'res_partner_id': int(partner),
                                  'state': 'cancelado',
                                  'fecha_factura_cancela': fields.datetime.now(),
                                  'warehouse_id' : int(warehouse_id),
                                  'pricelist_id':int(pricelist_id),
                                  'stock_piking_id' : new_picking,
                                  }, context=context)
                          # contador_while = contador_while + 1
                        #------------------------------------------------------------
                        # cr.execute("UPDATE _gv_producto_consignacion SET state='cancelado' WHERE id=%s",(recorre,))
                        cr.execute("UPDATE _gv_venta_consignacion SET total_venta=%s WHERE id=%s",(total_v,id))
                        # self.write(cr, uid, ids, {'total_venta': total_v}, context=context)
                    
                    stock_picking.action_confirm(cr, uid, new_picking, context=None)
                    # self.write(cr, uid, ids, {'state': 'confirmado'}, context=context)
                    
  
  def _get_almacen_origen(self, cr, uid, context=None):
        cr.execute("""SELECT warehouse_id FROM _res_configuracion""")
        almacen= cr.fetchone()[0]
        if not almacen:
            raise osv.except_osv( ('Error!'),  ('Debe configurar un almacén origen'))
        return almacen
  
  def _get_location_origen(self, cr, uid, context=None):
       try:
          cr.execute("""SELECT location_id FROM _res_configuracion""")
          location= cr.fetchone()[0]
          if not location:
              raise osv.except_osv(('Error!'), ('Debe configurar un almacen origen'))
          return location
       except:
           raise osv.except_osv(('Error!'), (('Debe configurar antes un almacen origen y destino en Configuracion de almacen')))
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                          Metodos OPENERP de Procesos Publicos (No metodos para reportes)                                         #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  STATE_SELECTION = [
        ('borrador', 'Borrador'),
        ('confirmado', 'Confirmado'),
        ('abierto', 'Abierto'),
        ('cancelado','Cancelado'),
        ('filalizado', 'Finalizado'),
    ]
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                          OPENERP Metodos ORM                                                                 ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  def create( self, cr, uid, vals, context = None ):
    """
    Método "create" que se ejecuta justo antes (o al momento) de CREAR un nuevo registro en OpenERP
    * Argumentos OpenERP: [ cr, uid, vals, context ], Asigna un folio a la nueva venta de cosignacion
    @param
    @return bool
    """
    nuevo_id = None
    folio_venta = "VC0001"
    sub_folio = "VC000"
    sub_folio_10 = "VC00"
    folio_prefijo = "VC"
    #-------------------------------------------------------------
    
    #-------------------------------------------------------------
    # Asignando un folio a la venta consignacion
    cr.execute('SELECT COUNT(*) FROM _gv_venta_consignacion')
    count_folio = cr.fetchone()[0]
    if ( count_folio == 0 ) :    
        vals['nombre'] = folio_venta
    else:
        cr.execute('select substr(nombre,3,10 ) from _gv_venta_consignacion order by id desc')
        folio = int(cr.fetchone()[0])+1
        if( folio >=1 and folio <=10 ) :
            nuevo_folio = sub_folio + str(folio)
            vals['nombre'] = nuevo_folio
        elif(folio>10 and folio<=100):
            nuevo_folio = sub_folio_10 + str(folio)
            vals['nombre'] = nuevo_folio
    nuevo_id = super( _gv_venta, self).create( cr, uid, vals, context = context )
    return nuevo_id
  
  def write( self, cr, uid, ids, vals, context = None ):
    """
    Metodo "write" se ejecuta antes de MODIFICAR el registro en OpenERP
    * Argumentos OpenERP: [ cr, uid, ids, vals, context ]
    @param
    @return bool
    """

    # raise osv.except_osv( ('Alerta!'), ('!=X-')+str("hola"))
    data  = self.browse(cr, uid, ids)
    if data.state == 'abierto' and data.stock_piking_id.state=='done':
     # raise osv.except_osv( ('Alerta!'), ("hola"))
     for id in ids:
         cr.execute("""SELECT COUNT(cantidad_facturada) FROM _gv_venta_consignacion AS vc 
                      INNER JOIN _gv_producto_consignacion AS pc ON vc.id=pc.ventas_id
                      WHERE pc.state in('abierto') AND (pc.cantidad_facturada>0 AND vc.id=%s)""",(id,))
         count_facturar = cr.fetchone()[0]
         # raise osv.except_osv( ('Alerta!'), (str(id)))
         if (count_facturar > 0):
             for venta in self.browse(cr, uid, ids, context= context) :
               if (venta.state == 'abierto') :
                   self.factura_grupal(cr, uid, ids, context=context)
         time.sleep(1)
         cr.execute("""SELECT COUNT(cantidad_regresada) FROM _gv_venta_consignacion AS vc 
                         INNER JOIN _gv_producto_consignacion AS pc ON vc.id=pc.ventas_id
                         WHERE pc.state in('abierto') AND (pc.cantidad_regresada>0 AND vc.id=%s)""",(id,))
         count_cancelar = cr.fetchone()[0]
         if ( count_cancelar > 0) : 
              self.crear_albaran_regreso(cr, uid, ids, context=context)
    booleano_proceso = super( _gv_venta, self).write( cr, uid, ids, vals, context = context )
    for id in ids:
      cr.execute("""UPDATE _gv_producto_consignacion SET cantidad_facturada=0.00,cantidad_regresada=0.00  WHERE ventas_id=%s""",(id,))
    return booleano_proceso
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                            Metodos OnChange                                                                  ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  def onchange_warehouse(self, cr, uid, ids, warehouse_id, context=None) :
      """Onchange que se ejecuta en cuando el campo cantidad_facturada es seleccionado,
         Actualiza el valor del campo el seleccionado cantidad_facturada"""
      try:
          cr.execute("""SELECT sl.id
                     FROM stock_warehouse AS sw INNER JOIN 
                     stock_location AS sl ON sw.lot_stock_id = sl.id
                     WHERE sl.usage='internal' and sw.id=%s""",(warehouse_id,))
          location_id = cr.fetchone()[0]
          # self.write(cr, uid, ids, {'stock_location_id': location_id}, context=context)
          return {'value' : {'stock_location_id':location_id}}
      except:
        return {'value' : {}}
     
  def onchange_warehouse_res_config(self, cr, uid, ids, stock_warehouse_id, context=None) :
     """Onchange que se selecciona la configuracion es seleccionado,"""
     try:
          cr.execute("""SELECT location_id FROM _res_configuracion
                     WHERE id=%s""",(stock_warehouse_id,))
          location_id = cr.fetchone()[0]
          cr.execute("""SELECT warehouse_id FROM _res_configuracion
                     WHERE id=%s""",(stock_warehouse_id,))
          warehouse_id = cr.fetchone()[0]
          # self.write(cr, uid, ids, {'stock_location_id': location_id}, context=context)
          return {'value' : {'stock_location_id':location_id,'warehouse_id':warehouse_id}}
     except:
         return {'value' : {}} 
  def cancelacion(self, cr, uid, ids, context=None):
       """
       cancela el producto asignado
       """
       self.write(cr, uid, ids, {'state': 'cancelado'}, context=context)
       
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                      Metodos para campos "function"                                                          ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
   #permisos admin en solicitud_vehiculo
  def _get_estado(self, cr, uid, ids, name, arg, context = {} ):
     try:
          result = {}
          mostrar = False
          estado_actual = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state
      
          if( estado_actual == 'borrador' or estado_actual=='confirmado'):
              mostrar = True
    
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = mostrar
          return	result
     except:return {}
  def _get_total_venta(self, cr, uid, ids, name, arg, context = {} ):
     try:
          result = {}
          mostrar = 0.00
          estado_actual = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state
          data = self.browse(cr, uid, ids)
          if( estado_actual == 'borrador' and data.stock_warehouse_id):
             for id in ids:
                  cr.execute("""SELECT SUM(costo_total) FROM _gv_producto_consignacion
                                where ventas_id=%s""",(id,))
                  mostrar = cr.fetchone()[0]
                  self.write(cr, uid, ids, {'total_venta': mostrar}, context=context)
                  cr.execute("""UPDATE _gv_venta_consignacion SET total_tree=%s WHERE id=%s""",(mostrar,id,))
                  #obtiene la ubicacin fisica del almacen seleccionado
                 
                  warehouse_id = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).stock_warehouse_id.warehouse_id.id
                  cr.execute("""SELECT sl.id
                                  FROM stock_warehouse AS sw INNER JOIN 
                                  stock_location AS sl ON sw.lot_stock_id = sl.id
                                  WHERE sl.usage='internal' and sw.id=%s""",(warehouse_id,))
                  if cr.rowcount:
                        location_id = cr.fetchone()[0]
                        # self.write(cr, uid, ids, {'stock_location_id': int(location_id)}, context=context)
                        # 
                        cr.execute("""SELECT id FROM stock_warehouse WHERE id=%s""",(warehouse_id,))
                        if cr.rowcount:
                              warehouse = cr.fetchone()[0]
                              # raise osv.except_osv( ('Alerta!'), ('Go')+str(warehouse)+str(location_id))
                              self.write(cr, uid, ids, {'warehouse_id': int(warehouse),'stock_location_id': int(location_id)}, context=context)
                              # c.execute("""update _gv_venta_consignacion set stock_location_id=%s where id=%s""",(int(location_id),id))
    
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = mostrar
          return	result
     except:return {}  
  def _get_total_facturados(self, cr, uid, ids, name, arg, context = {} ):
    try:
      result = {}
      total_facturados = 0.00
      for id in ids:
           cr.execute("""SELECT SUM(facturados*costo)
                      FROM _gv_venta_consignacion AS vc INNER JOIN _gv_producto_consignacion AS pc
                      ON vc.id=pc.ventas_id WHERE vc.id=%s""",(id,))
           total_facturados = cr.fetchone()[0]

      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = total_facturados
      return	result
    except:
       return result
      
  def _get_total_regresados(self, cr, uid, ids, name, arg, context = {} ):
    try:
      result = {}
      total_regresados = 0.00
      for id in ids:
           cr.execute("""SELECT SUM(regresados*costo)
                      FROM _gv_venta_consignacion AS vc INNER JOIN _gv_producto_consignacion AS pc
                      ON vc.id=pc.ventas_id WHERE vc.id=%s""",(id,))
           total_regresados = cr.fetchone()[0]

      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = total_regresados
      return	result
    except:
       return result
  
  def _get_finalizar_venta(self, cr, uid, ids, name, arg, context = {} ):
     """Finaliza la venta cuando el total es =  a 0"""
     try:
          result = {}
          mostrar = False
          bandera1 =False
          bandera2 =True
          contador = 0
          contador2 = 0
          estado_actual = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state
          venta = self.pool.get('gv.venta').browse( cr, uid, ids)
          if( estado_actual == 'abierto'):
              for id in ids:
                   cr.execute("""select count(*) from  _gv_producto_consignacion AS pc 
                                  INNER JOIN _gv_venta_consignacion AS vc  ON vc.id=pc.ventas_id 
                                  where pc.state='finalizado' and vc.id=%s""",(id,))
                   count_finalizados = cr.fetchone()[0]
              for this in venta.gv_productos_o2m:
                       if this.state == 'finalizado':
                         contador +=1
                       else:contador-=1              
              # raise osv.except_osv( ('Alerta!'), ('Go')+str(contador)+str(count_finalizados))
              if (contador == count_finalizados):
                   cr.execute("""select count(*) from  _gv_producto_consignacion AS pc 
                   INNER JOIN _gv_venta_consignacion AS vc  ON vc.id=pc.ventas_id 
                   where  pc.state_stok_picking='Transferido'
                   and vc.id=%s""",(id,))
                   count_transferidos = cr.fetchone()[0]
                   for count in venta.productos_o2m:
                             if count.state_stok_picking == 'Transferido':
                               contador2 +=1
                               bandera1 =True
                             else: bandera2 = False
                   if contador2 >0 and contador > 0 and bandera1 == True and bandera2 == True:
                         # raise osv.except_osv( ('Alerta!'), ('Go')+str(contador2)+str())
                         if (contador2 == count_transferidos):
                            mostrar = True
                            # raise osv.except_osv( ('Alerta!'), ('Go'))
                            self.write(cr, uid, ids, {'state': 'filalizado'}, context=context)
             # for id in ids:
             #      cr.execute("""SELECT total_venta FROM _gv_venta_consignacion
             #                    where id=%s""",(id,))
             #      total_venta = float(cr.fetchone()[0])
             #      if ( total_venta == 0) :
             #          time.sleep(2)
             #          mostrar = True
             #          self.write(cr, uid, ids, {'state': 'filalizado'}, context=context)
             #          
          for record in self.browse( cr, uid, ids, context = context):
              result[record.id] = mostrar
          return	result
     except:return {}
  #pasar al servidor 2249 
  def _get_state_picking(self, cr, uid, ids, name, arg, context = {}  ):
     """regresa el estado actual del albaran relacionado a esta venta"""
     try:
      result = {}
      mostrar = False
      state = ""
      estado_picking = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).stock_piking_id.state
      if (estado_picking == 'draft'):  state = "Borrador"
      elif(estado_picking == 'cancel'):
          state = "Cancelado"
          for id in ids:
               cr.execute("""update _gv_venta_consignacion set state='borrador' where id=%s""",(id,))
      elif(estado_picking == 'waiting'): state = "Esperando otra operacion"
      elif(estado_picking == 'confirmed'): state = "Esperando disponibilidad"
      elif(estado_picking == 'partially_available'): state = "Parcialmente Disponible"
      elif(estado_picking == 'assigned'): state = "Listo para transferir"
      else: state = 'Transferido'
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = state
      return result
     except:return {}
  #pasar al servidor 2249 
  def _get_state_picking_pricipal(self, cr, uid, ids, name, arg, context = {}  ):
     """regresa el estado actual del albaran relacionado a esta venta"""
     try:
      result = {}
      state = True
      unlink_ids = []
      if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state == 'abierto' or self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state == 'filalizado':
          if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m:
               for this in self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m:
                    if this.state_sale_order == 'Cancelado' and this.state_stok_picking == 'Cancelado':
                         this.unlink()
                         # raise osv.except_osv( ('Alerta!'), (str(this.nombre.id)+":"+str(this.state_sale_order)+":"+str(this.state_stok_picking)))
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = state
      return result
     except:return {}
  #pasar al servidor 2249
  def get_state_picking(self, cr, uid, ids, estado, context = None ):
     """regresa el estado actual del albaran relacionado a este producto"""
     try:
          state = ""
          estado_picking = estado
          if (estado_picking == 'draft'):  state = "Borrador"
          elif(estado_picking == 'cancel'): state = "Cancelado"
          elif(estado_picking == 'waiting'): state = "Esperando otra operacion"
          elif(estado_picking == 'confirmed'): state = "Esperando disponibilidad"
          elif(estado_picking == 'partially_available'): state = "Parcialmente Disponible"
          elif(estado_picking == 'assigned'): state = "Listo para transferir"
          elif(estado_picking == 'done'): state = "Transferido"
          else: state = ''
          
          return state 
     except:return {}    
  def get_state_sale_order(self, cr, uid, ids, estado, context = None):
     """regresa el estado actual de la venta relacionado con el producto"""
     try:
          state = ""
          estado_order = estado
          if (estado_order == 'draft'):  state = "Borrador"
          elif(estado_order == 'sent'): state = "Cotizaciones enviadas"
          elif(estado_order == 'cancel'): state = "Cancelado"
          elif(estado_order == 'waiting_date'): state = "Esperando fecha planificada"
          elif(estado_order == 'progress'): state = "Pedido de venta"
          elif(estado_order == 'manual'): state = "Pedido a facturar"
          elif(estado_order == 'shipping_exept'): state = "Exepcion de envio"
          elif(estado_order == 'invoice_exept'): state = "Exepcion de factura"
          elif(estado_order == 'done'): state = "Completado"
          else: state = ''
              
          return state
     except:return {}
  def _get_write_state_line(self, cr, uid, ids, name, arg, context = {}  ):
     """Obtiene los estados actuales de cada albaran y SO de cada movimiento
         regresado o facturado"""
     result = {}
     state = True
     unlink_ids = []
     obj_producto = self.pool.get('gv.producto')
     venta = self.pool.get('gv.venta').browse( cr, uid, ids)
      # try:
     if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state != 'borrador':
         if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m:
              for this in self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m:
                  state_picking = self.get_state_picking(cr, uid, ids,this.stock_piking_id.state,context)
                  state_so = self.get_state_sale_order(cr, uid, ids, this.sale_order_id.state, context)
                  obj_producto.write(cr, uid ,this.id,{'state_stok_picking':state_picking,'state_sale_order':state_so})
                  if this.state_sale_order == 'Cancelado' and this.state_stok_picking == 'Cancelado' or \
                                              this.state == 'cancelado' and this.state_stok_picking == 'Cancelado':
                    if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).gv_productos_o2m:
                        for index in self.pool.get('gv.venta').browse( cr, uid, ids[0] ).gv_productos_o2m:
                         if this.nombre.id == index.nombre.id :
                             total_venta = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).total_venta
                             total_venta = total_venta+(this.costo*this.cantidad)
                             for id in ids:
                                  cr.execute("""update _gv_venta_consignacion set total_venta=%s where id=%s""",(float(total_venta),id,))
                                  cr.commit()
                                  if this.state_sale_order == 'Cancelado' and this.state_stok_picking == 'Cancelado':
                                   facturados_function = index.facturados-this.cantidad
                                   cr.execute("""update _gv_producto_consignacion set facturados=%s,state='abierto' where id=%s""",(facturados_function,int(index.id)))
                                   cr.commit()
                                   this.unlink()
                                  if this:
                                   if this.state == 'cancelado' and this.state_stok_picking == 'Cancelado':
                                    facturados_function = index.regresados-this.cantidad
                                    cr.execute("""update _gv_producto_consignacion set regresados=%s,state='abierto' where id=%s""",(facturados_function,int(index.id)))
                                    cr.commit()
                                    this.unlink()
                                   if (self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m) and venta.state_picking!='Cancelado':
                                        pass
                                   elif(self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m) and venta.state_picking=='Cancelado':
                                        for id in ids:
                                         cr.execute("""update _gv_venta_consignacion set state='borrador' where id=%s""",(id,))
                                         cr.commit()           
     if venta.state =='filalizado' and venta.total_venta > 0:
          self.write(cr, uid, ids, {'state': 'abierto'}, context=context)
     for record in self.browse( cr, uid, ids, context = context):
         result[record.id] = state
     return result
      # except:return {}
  #pasar al servidor 2249 
  def _get_cancelaciones(self, cr, uid, ids, name, arg, context = {}  ):
      """regresa el estado actual del albaran relacionado a esta venta"""
      result = {}
      state = False
      if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).state=='abierto':
          if self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m:
              for this in self.pool.get('gv.venta').browse( cr, uid, ids[0] ).productos_o2m:
                   if ((this.state_sale_order == 'Cancelado' and this.state_stok_picking) == 'Cancelado' or ( not this.state_sale_order and this.state_stok_picking == 'Cancelado')):
                        pass
               
                        this.unlink()
                        # raise osv.except_osv( ('Alerta!'), (str(this.nombre.id)+":"+str(this.state_sale_order)+":"+str(this.state_stok_picking)))
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = state
      return result
    
  #considerar si se sube al servidor o no 
  def _get_warehouse(self, cr, uid, ids, name, arg, context = {} ):
      result = {}
      mostrar = False
      data = self.browse(cr, uid, ids)
      if data.warehouse_id and data.state=='borrador':
          warehouse_id = self.pool.get('gv.venta').browse( cr, uid, ids[0] ).warehouse_id.id
          cr.execute("""SELECT sl.id
                              FROM stock_warehouse AS sw INNER JOIN 
                              stock_location AS sl ON sw.lot_stock_id = sl.id
                              WHERE sl.usage='internal' and sw.id=%s""",(warehouse_id,))
          if cr.rowcount():
              location_id = cr.fetchone()[0]
              for id in ids:
                   cr.execute("""update _gv_venta_consignacion set stock_location_id=%s where id=%s""",(int(location_id),id))
          
              mostrar = True

      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = mostrar
      return	result
    
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                  Atributos basicos de un modelo OPENERP                                                      ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Nombre del modelo
  _name = 'gv.venta'
  
  #Nombre de la tabla
  _table = '_gv_venta_consignacion'
  
  #Nombre de la descripcion al usuario en las relaciones m2o hacia este módulo
  _rec_name = 'nombre'
  
  #Clausula SQL "ORDER BY"
  _order = 'nombre'

  #Columnas y/o campos Tree & Form
  
  _columns = {
    
     # =========================================  OpenERP Campos Basicos (integer, char, text, float, etc...)  ====================================== #
    
    'nombre' : fields.char( 'Venta', size = 255, required = False, readonly = True, store = True, help = "Guarda el folio de la VENTA"),
    'state': fields.selection(STATE_SELECTION, 'Estado', readonly=True, select=True, copy=False),
    'fecha_inicio' : fields.date('Fecha Inicio', required = True),
    # 'fecha_fin' : fields.date('Fecha de finalización', required = False),
    'total_tree': fields.float('Total'),
    #pasar al servidor 2249
    'state_picking':fields.function(_get_state_picking, type="char"),
    # ========================================================  Relaciones [many2one](m2o) ========================================================= #
    'stock_warehouse_id' : fields.many2one( '_res_config', 'Configuración', required = True),
    'vendedor_id' : fields.many2one( 'res.partner', 'Consignatario', required = True),
    'warehouse_id': fields.many2one('stock.warehouse', 'Almacén',),
    'stock_location_id' : fields.many2one('stock.location','Ubicación'),
    'pricelist_id': fields.many2one('product.pricelist', 'Tarifa', required=True,),
    'total_venta' : fields.float('Debe'),
    'consignador': fields.many2one('res.users', 'Consignador', readonly=True, store=True),
    # ========================================================  Relaciones [one2many](o2m) ========================================================= #
    'gv_productos_o2m' : fields.one2many( 'gv.producto', 'ventas_id' , 'Productos',
                                            domain=['|',('state', '=','abierto'), ('state', '=','finalizado')]),
   
    'productos_o2m' : fields.one2many( 'gv.producto', 'ventas_id' , 'Productos',
                                      domain=['|',('state', '=', 'facturado'),('state', '=', 'cancelado')]),
    'stock_piking_id': fields.many2one('stock.picking', 'Albarán',),
    'account_move_id': fields.many2one('account.move', 'Poliza'),
    # ========================================================  Relaciones [many2many](m2m) ======================================================== #
    
    # ======================================================== Campos "function" (function) ======================================================== #
    'estado_venta': fields.function(_get_estado, string='Pasa el estado actual de la venta a productos', type='boolean', method=True),
    'total' : fields.function(_get_total_venta,'Debe', type="float"),
    'total_facturados' : fields.function(_get_total_facturados,'Total Facturados', type="float",),
    'total_regresados' : fields.function(_get_total_regresados,'Total Regresados', type="float"),
    'finaliza_venta': fields.function(_get_finalizar_venta,'Finalizar venta', type="boolean"),
    #pasar al servidor 249
    # 'unlink_historial': fields.function(_get_unlink_historial, type="boolean"),
    'write_lines': fields.function(_get_write_state_line, type="boolean")
    
    
  }
  
  #Valores por defecto de los campos del diccionario [_columns]
  _defaults = {
    'state': 'borrador',
    'estado_venta': True,
    'fecha_inicio': lambda *a: time.strftime('%Y-%m-%d'),
    'consignador': lambda self,cr,uid,context=None: uid,
    # 'warehouse_id':_get_almacen_origen,
    # 'stock_location_id':_get_location_origen,
  
  }
  
  #Restricciones de BD (constraints)
  _sql_constraints = []
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                              Metodos para validacion de la lista: [_constraints]                                             ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Restricciones desde codigo
  _constraints = []
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                            Metodos para reportes                                                             ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###

  #---------------------------------------------------------------------------------------------------------------------------------------------------
  def imprimir_reporte_venta( self, cr, uid, ids, context = {} ) :
    """
    Método para descargar el reporte en formato PDF
    """
    if context is None :
      context = {}
    data = {}
    data['ids'] = context.get( 'active_ids', [] )
    data['model'] = context.get( 'active_model', 'ir.ui.menu' )
    
    data['form'] = self.read( cr, uid, ids, [ 'id', ] )[0]
    #data['form'].update( self.read( cr, uid, ids, [ 'id', ], context = context )[0] )
    datas = {
      'ids': [],
      'model': 'gv.venta',
      'form': data,
    }
    #Retorna el nombre del reporte y el tipo de datos report.xml
    return {
      'type' : 'ir.actions.report.xml',
      'report_name' : 'poliza_venta',
      'datas' : datas,
    }

  
_gv_venta()


# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
