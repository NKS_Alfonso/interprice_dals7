#-*- coding: utf-8 -*-

#########################################################################################################################
#  @version  : 1.0                                                                                                      #
#  @autor    : ICJ                                                                                                      #
#  @creacion : (aaaa/mm/dd)                                                                                             #
#  @linea    : Maximo, 121 caracteres                                                                                   #
#  -------------------------------------------------------------------------------------------------------------------  #
#  -------------------------------------------------------------------------------------------------------------------  #
#  @vea      :                                                                                                          #
#########################################################################################################################

#Importando las clases necesarias para construir un modelo OpenERP 
from openerp.osv import fields, osv

#Importando clases necesarias para el manejo de fechas
from datetime import date, datetime

import time
import locale

#Importando clases para la generación del reporte en formato pdf
from openerp import pooler
from lxml import etree
from openerp.report import report_sxw
from openerp.tools.translate import _


# :::::::::::::::::::::::::::::::::::::::::::::::::::: REFERENCIAS A SOBREESCRIBIR [INICIA] :::::::::::::::::::::::::::::::::::::::::::::::::::::::: #

#Seccion que contiene una o varias referencias temporales a otros modulos.
#Estas clases seran sobreescritas por el modulo que contendra la implementacion completa de la funcionalidad final.
#No se admiten comentarios en los metodos de estas clases.
#Para ver mas documentacion, consulte el modulo final que sobreescriben a estas clases.

"""
Con estas implementaciones se evitan los errores de instalacion del modulo ya que las referencias m2o, o2m o m2m necesitan que las relaciones (tablas)
de los modelos con los que se relacionan, esten creadas previamente. Sin embargo esto no siempre es posible en nuestra manera de construir los modulos
por lo que se necesita incluir un "resumen" de la clase solo con el nombre del modelo y la tabla a crear en la BD. Una vez creada, la relacion ya
existe y es posible crear los campos "x2x", luego cuando se incluyan las clases con la informacion completa del modelo, estos resumenes seran
sobreescritos. A continuacion un "resumen" de ejemplo (siempre debe llevar un comentario que indique de que modelo se trata):

#Referencia a [Modelo en Conflicto]
class arc_modelo_en_conflicto( osv.osv ) :
  _name = 'arc_modelo_en_conflicto'
  _table = '_arc_modelo_en_conflicto'
arc_modelo_en_conflicto()
"""

# :::::::::::::::::::::::::::::::::::::::::::::::::::: REFERENCIAS A SOBREESCRIBIR [FINALIZA] :::::::::::::::::::::::::::::::::::::::::::::::::::::: #

#Modelo para gestionar el reporte de producto
class reporte_venta( report_sxw.rml_parse ) :
  
  #######################################################################################################################
  #                    Variables de Clase, Privadas y Publicas (Independientes al API de OpenERP)                       #
  #######################################################################################################################
  
  #variable que define al reporte
  _description = "Reporte de la venta"
  
  #######################################################################################################################
  #                                 Metodos Privados (Independientes al API de OpenERP)                                 #
  #######################################################################################################################
  
  #----------------------------------------------------------------------------------------------------------------------
  def __init__( self, cr, uid, name, context = None ) :
    """
    Metodo "__init__" para instanciar objetos a partir de esta clase y pasar de formato rml a pdf
    * Argumentos OpenERP: [cr, uid, ids, name, context] 
    """
    super( reporte_venta, self ).__init__( cr, uid, name, context = context )
    self.localcontext.update({
      'time' : time,
      'locale' : locale,
      # 'get_producto' : self.get_producto,
    })
    self.context = context
    
  #######################################################################################################################
  #                                 Metodos Publicos (Independientes al API de OpenERP)                                 #
  #######################################################################################################################
 #---------------------------------------------------------------------------------------------------------------------- 
  # def get_producto( self, data ):
  #   """
  #   Metodo que obtiene los datos para llenar el reporte de producto
  #   @return (dict)
  #   """
  #   id_producto = data[ 'form' ].get( 'id', False )
  #   if id_producto == False or id_producto == '' or id_producto == 0 or id_producto is None :
  #     return { 'value' : {} }
  #   else :
  #     self.cr.execute(
  #       """
  #       SELECT
  #         p.nombre AS nombre_producto,
  #         p.codigo AS codigo,
  #         um.descripcion AS unidad_medida,
  #         p.precio AS precio,
  #         p.descripcion AS descripcion,
  #         p.categoria AS categoria,
  #         CASE p.activo
  #           WHEN TRUE THEN 'Activo'
  #           WHEN FALSE THEN 'Inactivo'
  #         END AS estado
  #       FROM _cap_producto AS p
  #       LEFT JOIN _cap_cat_unidad_medida AS um
  #       ON p.unidad_medida_m2o_id = um.id
  #       WHERE p.id = %s
  #       """, ( int( id_producto ), ))
  #     datos = self.cr.dictfetchall()
  #     return datos

#########################################################################################################################
#                                                        Extras                                                         #
#########################################################################################################################

#------------------------------------------------------------------------------------------------------------------------
#Nombre del reporte, nombre del model'o (clase del .py),ruta del rml, parser con el nombre de la clase y header el encabezado del reporte
#para emplear el template rml definido

report_sxw.report_sxw( 'report.poliza_venta',
                       '_gv_venta',
                       'mis_modulos/ventas/secciones/venta/report/reporte_venta.rml',
                       parser = reporte_venta,
                       header = True)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: