# -*- coding: utf-8 -*-

{
	'name': 'Dev-Notas de credito',
	'version': '1.0',
	'category': 'Devoluciones',
	'sequence': 56,
	'author': 'Israel Cabrera Juarez(gvdeto)',
	'maintainer': '',
	'website': '',
	#'images' : ['ventas/static/images/ventas.jpg'],
	# 'icon': "ventas/static/src/img/icon.png",
	'installable': True, 
	'active': False,
	'description': 'Devoluciones',
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base','account','stock_account'
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		
		#Archivo principal de menús
		'menus.xml',
		
		#------------------------------Devoluciones ----------------#
		'secciones/dev/dev.xml',
		
		
		
		
		

		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
