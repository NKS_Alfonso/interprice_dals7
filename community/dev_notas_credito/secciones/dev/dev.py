# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                            #
#  @creacion    :  (aaaa/mm/dd)                                                                                                            #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp import models, fields as fieldsv8
#Modulo :: Producto
class _gv_devoluciones( osv.osv ) :

  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                         Variables Privadas y Publicas (No variables del API de OPENERP)                                          #
  #                                                                                                                                                  #
  ####################################################################################################################################################  
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Privados (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Publicos (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
 
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                          OPENERP Metodos ORM                                                                 ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                            Metodos OnChange                                                                  ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  def onchage_actualiza_facturar(self, cr, uid, ids, facturar) :
      """
      Onchange que se ejecuta en cuando el campo facturar es seleccionado,
      Actualiza el valor del campo con un True o False
      """
      try:
          if (facturar == True) :
            for id in ids:
                cr.execute("""UPDATE _gv_producto_consignacion SET facturar=True WHERE id=%s""",(id,))
              
            return {'value' : {}}
          else:
             for id in ids:
                cr.execute("""UPDATE _gv_producto_consignacion SET facturar=False WHERE id=%s""",(id,))
              
             return {'value' : {}}
      except:
        return {'value' : {}}
        
  def _get_state_albaran_venta(self, cr, uid, ids, name, arg, context = {}  ):
      """regresa obtiene el estado del albaran principal de esta venta"""
      try:
        result = {}
        mostrar = False
        estado = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).ventas_id.state
        if (estado == 'abierto'):
          estado_picking = self.pool.get('gv.producto').browse( cr, uid, ids[0] ).ventas_id.state_picking
          if estado_picking != 'Transferido':   mostrar = True
        for record in self.browse( cr, uid, ids, context = context):
            result[record.id] = mostrar
        return result
      except:return {}
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                  Atributos basicos de un modelo OPENERP                                                      ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Nombre del modelo
  _name = 'gv.devolucion'
  
  #Nombre de la tabla
  _table = '_gv_devolucion'
  
  #Nombre de la descripcion al usuario en las relaciones m2o hacia este módulo
  _rec_name = 'name'
  
  #Cláusula SQL "ORDER BY"
  _order = 'date desc'

  #Columnas y/o campos Tree & Form
  
  _columns = {
    
    # =========================================  OpenERP Campos Basicos (integer, char, text, float, etc...)  ====================================== #
    'name':fields.char('Número', size=254, required=True),
    'date': fields.datetime('Fecha',required=False),
    'order_id': fields.char('Venta/Compra',size=254),
    # ========================================================  Relaciones [many2one](m2o) ========================================================= #
    'picking_id' : fields.many2one( 'stock.picking', 'Albarán',required = False, readonly = False, help = "Guarda el id del albaran"),
    'invoice_id': fields.many2one('account.invoice', 'Factura/Referencia', required = False,help = "Guarda el id de la factura"),
    'nota_id': fields.many2one('account.invoice', 'Nota credito', required = False,help = "Guarda el id de la factura"),
    'partner_id': fields.many2one('res.partner', 'Cliente/Proveedor', readonly=False, help = "Guarda el del cliente"),
    'responsable_id': fields.many2one('res.partner','Responsable',required=False),
    # ========================================================  Relaciones [one2many](o2m) ========================================================= #
    
    # ========================================================  Relaciones [many2many](m2m) ======================================================== #
    
    # ======================================================== Campos "function" (function) ======================================================== #
  
    
  }
  
  #Valores por defecto de los campos del diccionario [_columns]
  _defaults = {
  
    #'estado_venta' : lambda self, cr, uid, context : ( context['estado_venta'] ) if ( context and ( 'estado_venta' in context ) ) else ( None ),
   
  }
  
  #Restricciones de BD (constraints)
  _sql_constraints = []
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                              Metodos para validacion de la lista: [_constraints]                                             ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Restricciones desde codigo
  _constraints = []
  
_gv_devoluciones()


class stock_return_picking(osv.osv_memory):
    _inherit = 'stock.return.picking'
    _columns = {
        'motivo' : fields.selection([
                                                            ('dev', 'Devolucion'),
                                                            ('dev_cancel', '"ErrorInterno'),
                                                            ], 'Asunto', required=False),
        
         'picking_id': fields.many2one('stock.picking', 'Stock Picking'),
    }
    
    _defaults = {
        'picking_id': lambda self, cr, uid, context=None: self.pool.get('stock.picking').browse(cr, uid, context['active_id'], context=context),
        'motivo':'dev',
    }   
    def numero_serie(self, cr, uid, ids, context=None):
        cad = ""
        data = self.browse(cr,uid,ids)
        obj_return = self.pool.get('stock.return.picking.line')
        for k in data.picking_id.pack_operation_ids:
            for l in data.product_return_moves:
                if k.product_id.id == l.product_id.id:
                    if (l.quantity <= k.product_qty) and (l.copy_cantidad==l.quantity):
                        obj_return.write(cr, uid, l.id, {'lot_id':k.lot_id.id}, context=context)
                    elif(l.quantity>l.copy_cantidad):
                        raise osv.except_osv(('Warning!'), ("Verique la cantidad"))
                    
        return {
            
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.return.picking',
            'target': 'new',
            'res_id': ids[0],
            
    }
    
    def _prepare_invoice(self, cr, uid, ids, context=None):
        data =  self.browse(cr, uid, ids, context=context)
        obj_order = self.pool.get('sale.order')
        order_id = obj_order.search(cr, uid, [('name','=',data.picking_id.origin)],context=context)[0]
        order = obj_order.browse(cr, uid, order_id, context=context)
        cuenta = data.picking_id.partner_id.property_account_receivable.id
        cliente = data.picking_id.partner_id.id
        if data.picking_id.partner_id and data.picking_id.partner_id.property_payment_term.id:
            pay_term = data.picking_id.partner_id.property_payment_term.id
        else:
            pay_term = False
        return {
            'type': 'out_refund',
            'account_id': cuenta,
            'partner_id':cliente,
            #'invoice_line': [(6, 0, lines)],
            'currency_id' : order.pricelist_id.currency_id.id,
            'payment_term': pay_term,
            'company_id': order.company_id and order.company_id.id or False,
            'date_invoice': fields.date.today(),
            'journal_id': self.pool.get('account.journal').search(cr, uid, [('code','=','DEVCL')])[0],
        }          
    def create_returns(self, cr, uid, ids, context=None):
        """Funcion heredada y modificada para la devolcion de cllientes y proveedores"""
        #---------------------------------------------
        obj_invoice = self.pool.get('account.invoice')
        obj_dev = self.pool.get('gv.devolucion')
        id_p = ""
        data =  self.browse(cr, uid, ids, context=context)
        invoice_id = 0
        id_user = self.pool.get('res.users').browse(cr, uid,uid, context=context)
        responsable_id = self.pool.get('res.partner').browse(cr, uid, id_user.partner_id.id, context=context )
        #-----------
        if data.picking_id.picking_type_id.code=='outgoing':
            cr.execute("""SELECT COUNT(*) FROM account_invoice WHERE origin=%s""",(data.picking_id.name,))
            count = cr.fetchone()[0]
            if count>0:
                invoice_id = self.pool.get('account.invoice').search(cr, uid, [('origin','=',data.picking_id.name)])[0]
        elif data.picking_id.picking_type_id.code=='incoming':
            cr.execute("""SELECT COUNT(*) FROM account_invoice WHERE origin=%s""",(data.picking_id.origin,))
            count = cr.fetchone()[0]
            if count>0:
                invoice_id = self.pool.get('account.invoice').search(cr, uid, [('origin','=',data.picking_id.origin)])[0]
        #-----------
        #inv = self._prepare_invoice(cr, uid, ids, context=context)
        
        #inv_id = self.pool.get('account.invoice').create(cr, uid, inv)
        #---------------------------------------------
        res =self.numero_serie(cr, uid, ids, context=context)
        dic = super(stock_return_picking, self).create_returns(cr, uid, ids, context=context)
       
        #--------
        for this in str(dic['domain'][15:]):
            if this != ']' and this != ')':
                id_p+=this
        pik = int(id_p)
        sufix_dev = ''
        obj_dev.create(cr, uid,{'name':sufix_dev+data.picking_id.name,
                                 'picking_id':pik,
                                 'partner_id':data.picking_id.partner_id.id,
                                 'invoice_id':invoice_id,
                                 'responsable_id':responsable_id.id,
                                 'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                                 'order_id':data.picking_id.origin,
                                 }, context=context)
        obj_picking = self.pool['stock.picking'].browse(cr, uid, [pik], context=None)
        if obj_picking:
            # cr.execute("""SELECT id FROM stock_picking_type WHERE code='outgoing'""")
            obj_write = {'dev': data.motivo, 'origin': sufix_dev + obj_picking.origin}
            # if cr.rowcount > 0:
                # picking_type_id = cr.fetchone()[0]
                # obj_write.update({'picking_type_id': picking_type_id})
            obj_picking.write(obj_write)

        # cr.execute("""UPDATE stock_picking SET dev=%s WHERE id=%s""",(data.motivo,pik,))
        #raise osv.except_osv(('Warning!'), ("la cantidad del producto ")+str(pik))
        #return res
stock_return_picking()


class stock_return_picking_line(osv.osv_memory):
    _inherit = "stock.return.picking.line"
   
    _columns = {'copy_cantidad': fields.float('Copia-quantity')}

stock_return_picking_line()


class stock_picking(osv.osv):
    _inherit = 'stock.picking'
    #aqui me quede---------------------------------------------------------------
    def _update_location_move(self, cr, uid, ids, name, args, context):
        """Verifica si el proveedor tiene un rfc o no"""
        obj_warehouse = self.pool.get('stock.warehouse')
        #raise osv.except_osv(('Warning!'), ("la cantidad del producto ")+str(warehous.lot_stock_id))
        if not ids : return {}
        result = {}
        dev= False
        #try:
        for this in self.browse(cr,uid,ids):
            if this.dev=='dev' and this.state=='assigned' and this.picking_type_id.code=='incoming':
                #------validacion para verificar si la factura esta cancelada
                cr.execute("""SELECT COUNT(*) FROM account_invoice WHERE origin=%s""",(this.origin,))
                count = cr.fetchone()[0]
                if count>0:
                    invoice_id = self.pool.get('account.invoice').search(cr, uid, [('origin','=',this.origin)])[0]
                    obj_invoice = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
                #---------------------regresan a soporte cuando la factura no esta cancelelada
                    if obj_invoice.state!='cancel':
                        for mov in this.move_lines:
                            warehouse = obj_warehouse.search(cr, uid, [('lot_stock_id','=',mov.location_dest_id.id)])[0]
                            warehous = obj_warehouse.browse(cr, uid, warehouse, context=context)
                            if warehous.stock_warehouse_type_id.code == 'fac' or warehous.stock_warehouse_type_id.code == 'pre' \
                                    or warehous.stock_warehouse_type_id.code == 'sop':
                                id_w = obj_warehouse.search(cr, uid, [('stock_warehouse_type_id','=',warehous.stock_warehouse_type_id.id),('centro_costo_id','=',warehous.centro_costo_id.id)])[0]
                                obj_id_w = obj_warehouse.browse(cr, uid, id_w, context=context)
                                cr.execute("update stock_move set location_dest_id=%s where id=%s",(obj_id_w.lot_stock_id.id,mov.id,))
                                
                        dev= False
                    #si la factura esta cancelada entonces devolver los productos a la ubicacion de donde salio
                    elif obj_invoice.state=='cancel':
                        cr.execute("""SELECT COUNT(*) FROM stock_picking WHERE name=%s""",(this.origin,))
                        count_fa = cr.fetchone()[0]
                        #raise osv.except_osv(('Warning!'), ("la cantidad del producto ")+str(count_fa))
                        if count_fa>0:
                            id_pik = self.pool.get('stock.picking').search(cr, uid, [('name','=',this.origin)], context=context)[0]
                            obj_pik =  self.pool.get('stock.picking').browse(cr, uid, id_pik, context=context)
                            for m in obj_pik.move_lines:
                                    cr.execute("""UPDATE stock_move SET location_dest_id=%s WHERE picking_id=%s""",(m.location_id.id,this.id,))
                            cr.execute("""UPDATE stock_picking SET dev='dev_cancel' WHERE id=%s""",(this.id,))
            
           # if this.dev=='dev' and this.state=='assigned' and this.picking_type_id.code=='outgoing':
               # pass
        result[this.id] =  dev
        return result
        #except:
            #return {}
    _columns = {
                'dev':fields.char('Rererencia', size=254),
                'update_moves' : fields.function(_update_location_move,type="boolean"),}
    
stock_picking()


class stock_warehouse(osv.osv):
    _inherit = 'stock.warehouse'
    _columns = {
    #     'type' : fields.selection([ ('fac', 'Facturacion'),
    #                                 ('pre', 'Prestamo'),
    #                                 ('sop', 'Soporte'),
    #                                 ('tra', 'Transito'),
    #                                 ('scrp', 'Scrap'),
    #                                 ], 'Tipo', required=False),
        'stock_warehouse_type_id':fields.many2one('stock.warehouse.type', 'Tipo', ondelete="cascade", required=False)
    }



class StockWarehouseType(models.Model):
    _name = 'stock.warehouse.type'
    _rec_name = 'name'

    code = fieldsv8.Char(string='code', required=True)
    name = fieldsv8.Char(string='name', required=True)

    _constraints = [
        ('code_uniq', 'unique (code)', 'You can not have two codes whit the same code!')
    ]


class account_invoice(osv.Model):
    _inherit = 'account.invoice'
    
    _columns={
        'dev':fields.char('Motivo',size=254),
        'picking_dev_id': fields.many2one('stock.picking','Albaran')
        }
    
account_invoice()

class stock_invoice_onshipping(osv.osv_memory):
    _inherit = 'stock.invoice.onshipping'

stock_invoice_onshipping()

#solo falta poner el lige en tre el wizard stock.invoice.onshipping y el albaran y que desde ahi se cree la nota de credito..
#
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
