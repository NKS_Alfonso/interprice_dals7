# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from functools import wraps
from openerp.exceptions import except_orm
from openerp.tools.translate import _


def todo_rate_ctx_v7(func):
    """
    This decorator sets on context the 'type' of invoice or voucher.

    In order for this decorator to work, it must receive a kw argument for
    'voucher_id' or the voucher id on *args[2] same for 'cr' and 'uid'
    at args[0] and args[1] respectively.

    If a 'context' **kwarg is found it will used as base for building new context.
    """
    @wraps(func)
    def add_type_to_context(self, *args, **kwargs):
        if kwargs.get('voucher_id', False) or isinstance(args[2], int):
            cr = args[0]
            uid = args[1]
            context = kwargs.get('context', False) or {}
            try:
                voucher = self.pool.get('account.voucher').browse(cr,
                                                                  uid,
                                                                  kwargs.get('voucher_id', False) \
                                                                  or args[2],
                                                                  context=context)
            except Exception:
                raise except_orm(_('Warning!'), _('No voucher found'))

            context.update({'todo_ttype': voucher.type})
            kwargs['context'] = context
            return func(self, *args, **kwargs)
    return add_type_to_context
