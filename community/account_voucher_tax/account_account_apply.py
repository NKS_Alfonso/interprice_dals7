# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, fields, models


class AccountAccountApply(models.Model):
    _inherit = 'account.account.apply'
    generate_taxes = fields.Boolean('Genera impuestos', default=True)
