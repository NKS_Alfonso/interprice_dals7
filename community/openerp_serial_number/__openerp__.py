# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################

{
    'name': 'ProductSerial',
    'version': '8.0',
    'category': 'Generic Modules',
    'description': """
      Este módulo exporta un archivo csv que puede utilizar con cualquier software de hoja de cálculo.
      el archivo exporta los codigos de los productos en recepción para asignarles una serie a los
      productos que la requieran, despues importa el mismo archivo con las sries ya asignadas
      
      
      .- Valida que cada producto que requiera serie se le ha asignado una
      
      .- Valida que la serie no este asiganda a un producto con el mismo codigo
      
      .- Valida que los productos que no requieran serie no se les asigne una
      
                   """,
    'author': 'Israel Cabrera Juarez(gvadeto)',
    'website': 'http://www.ejemplo.com',
    'depends': ['base','stock', 'sale_stock'],
    'data': [
        'wizard/psn_view_wizard.xml',
        'view/psn_view.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: