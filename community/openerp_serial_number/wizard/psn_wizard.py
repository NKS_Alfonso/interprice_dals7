# -*- coding: utf-8 -*-
##############################################################################
#
#   Israel Cabrera Juarez
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import cStringIO
from openerp.tools.misc import get_iso_codes
from openerp import tools

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime
import time

#----------------------------------------------------------
# psn_export
#----------------------------------------------------------
class psn_export(osv.osv_memory):
    _name = "psn.export"


    _columns = {
            'name': fields.char('File Name', readonly=True),
            'data': fields.binary('File', readonly=True),
            'picking_id': fields.many2one('stock.picking', 'Albaran'),
    }
    _defaults = { 
        'name': 'series.csv',
    }

    def act_getfile(self, cr, uid, ids,id_activo, context=None):
        picking_id = context.get('active_id', False)
        file_obj = self.pool.get('product.serial.number.file')
        file_ids = file_obj.search(cr, uid, [('picking_id','=',picking_id)])
        if file_ids:
            psn_file = file_obj.browse(cr, uid, file_ids)[0]
            buf = cStringIO.StringIO()
            buf.write(psn_file.data)
            out = base64.encodestring(buf.getvalue())
            buf.close()
            filename_psn = psn_file.name+".csv"
            write_vals = {
                'name': filename_psn,
                'data': out,
            }
            self.write(cr, uid, ids, write_vals, context=context)
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'psn.export',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': ids[0],
                'views': [(False, 'form')],
                'target': 'new',
            }
        else:
             self.pool.get('stock.transfer_details_items').create_psn_file(cr, uid, ids, picking_id,id_activo,context=context)
             self.act_getfile(cr, uid, ids, id_activo, context=context)
             return self.act_getfile(cr, uid, ids, id_activo, context=context)
        
    def wizard_stock_transfer_details(self, cr, uid, ids, context = None):
        cr.execute("""SELECT picking_id FROM psn_export""")
        picking_id = cr.fetchall()
        cr.commit()
        cr.execute("""delete from product_serial_number_file""")
        cr.execute("""delete from psn_export""")
        cr.execute("""update stock_transfer_details set validar_codigos=False""")
        return self.pool['stock.picking'].do_enter_transfer_details(cr,uid,picking_id,context=context)
       
        
   
psn_export()


