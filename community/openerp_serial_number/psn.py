# -*- coding: utf-8 -*-
##############################################################################
#
#   Israel Cabrera Juarez
#   
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64, StringIO, csv
from openerp.osv import osv, fields, orm
from openerp.tools.translate import _
from datetime import datetime
import time
import openerp.netsvc as netsvc
# from openerp import models, fields, api

header_fields = ['Serie', 'SKU']
#----------------------------------------------------------
# stock.transfer_details
#----------------------------------------------------------
class stock_transfer_details(osv.Model):
    #------------------------------------------------------------------------------------------#
    #                   Metodos para campos "function"                                         #
    #------------------------------------------------------------------------------------------#
    """stock Transfer Details"""
    _inherit = 'stock.transfer_details'
    

    _columns = {

        'psn_file_ids': fields.one2many('product.serial.number.file','stock_transfer_details_id','PSN File'),
        'archivo': fields.binary('Archivo', required = False),
        # 'nombre_archivo': fields.binary('Archivo', required = False),
        'csv_separator': fields.selection([(',',','),(';',';')], 'CSV Separator', required=False),
        'validar_codigos': fields.boolean('Validar codigos', help = 'Verificar si los productos necesitan serie o no...'),
        
    }

    _defaults = {
        'psn_ids': False,
        'csv_separator': ',',
    }


    #------------------------------------------------------------------------------------------#
    #                   Metodos onchange                                                       #
    #------------------------------------------------------------------------------------------#
    def onchange_valida_codigos(self, cr, uid, ids, validar_codigos,context=None):
      """"""
      if validar_codigos == True:
       return {}
    #------------------------------------------------------------------------------------------#
    #                   Metodos (otros)                                                        #
    #------------------------------------------------------------------------------------------#
    def act_getfile(self, cr, uid, ids, context=None):
        """"""
        data = self.browse(cr,uid,ids)[0]
        item_ids = data.item_ids
        id_activo = data.id
        psn_export_obj = self.pool.get("psn.export")
        psn_export_id = psn_export_obj.create(cr, uid, {'picking_id':context.get('active_id',False)}, context=context)
        return psn_export_obj.act_getfile(cr, uid, [psn_export_id], id_activo, context=context)
    
    
    	
stock_transfer_details()

#----------------------------------------------------------
# Product Serial Number File
#----------------------------------------------------------
class product_serial_number_file(osv.Model):
    """Product Serial Number File"""
    _name = 'product.serial.number.file'
    _description = 'Product Serial Number File'

    _columns = {
        'name': fields.char('Nombre', size=64, required=True),
        'data': fields.text('Contenido'),
        'picking_id': fields.many2one('stock.picking', 'Albaran'),
        'stock_transfer_details_id': fields.many2one('stock.transfer_details', 'Albaran'),
        'echo' :  fields.boolean('Realizado', required = True)
    }

product_serial_number_file()


