# -*- coding: utf-8 -*-

{
	'name': 'Reportes',
	'version': '1.0',
	'category': 'gvadeto',
	'author': 'Israel Cabrera Juarez(gvadeto)2015',
	'description': """
									Funcionalidad: Este módulo se instala exclusivamente para reportes
									""",
	'maintainer': '',
	# 'website': '',
	'installable': True, 
	'active': False,
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base','sale','stock','account','purchase',
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		
		#Archivo principal de menus
		#~ 'menus.xml',
		
		#------------------------------Ventas, Almacen----------------#
		#~ 'secciones/ventas/reportes_venta.xml',
		#~ 'secciones/ventas/reportes_venta_cliente.xml',
		#~ 'secciones/ventas/reportes_venta_articulo.xml',
		#~ 'secciones/almacen/reportes_almacen.xml',
		#~ 'secciones/compras/reportes_compra.xml',
		#~ 'secciones/compras/reportes_compra_proveedor.xml',
		#~ 'secciones/compras/reportes_compra_art.xml',
		#~ 'secciones/almacen/reportes_inventario_historico.xml',
		#~ 'secciones/almacen/reportes_inventario_analitico.xml',
		#~ 'secciones/almacen/reportes_inventario_existencia.xml',
		#~ 
		
		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
