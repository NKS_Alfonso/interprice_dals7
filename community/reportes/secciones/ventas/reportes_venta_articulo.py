# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Israel Cabrera Juarez(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

from openerp.osv import fields, osv
from openerp import models, api, _
import base64
import cStringIO

##################################################################################
#                    ventas_x_articulo                                           #
#################################################################################
class ventas_x_articulo( osv.osv ) :
  def cambio_status_fac(self, status):
      state_fac = ""

      if status == u'open': state_fac = "Abierta"
      if status == u'paid': state_fac = "Pagada"
      if status == u'done': state_fac = "Hecho"
      if status == u'draft': state_fac = "Borrador"
      if status == u'cancel': state_fac = "Cancelado"
      return state_fac

  def _write_lines(self, cr, uid, ids, context=None):
      invoice_lines = self.pool.get('stock.move')
      doc = ""
      quantity = 0
      for this in self.browse(cr, uid, ids):
          for invoice_id in this.stock_move_ids:
              number = "S/N"
              move = self.pool.get('stock.move').browse(cr, uid, invoice_id.id, context=context)
              cantidad = move.product_qty
              price = move.precio_unitario

              cr.execute(""" select ai.id from stock_move sk inner join stock_picking sp on sk.picking_id = sp.id
                             inner join account_invoice ai on sp.name = ai.origin where sk.origin=%s AND sk.id =%s """,
                             (invoice_id.origin, invoice_id.id,))
              data = cr.fetchone()
              datos = self.pool.get('account.invoice').browse(cr, uid, data, context=context)
              amount_total = abs(datos.amount_total)
              cost = abs(move.costo_total)
              uti = move.utilidad
              
              if datos.type == 'out_refund':
                  doc = 'NCC'
                  quantity = cantidad * -1
                  precio = price * -1
                  total = amount_total * -1
              if datos.type == 'out_invoice':
                  doc = 'FAC'
                  quantity = move.product_qty
                  precio = move.precio_unitario
                  total = amount_total
                  
              if datos.documento == 'FAC' and cost > 0:
                  utilidad = (uti / cost) * 100

              if datos.documento == 'NCC' and cost > 0:
                  utilidad = (uti / cost) * 100

              # if move.costo_total == 0:
              #     utilidad = 0.00

              x = self.cambio_status_fac(datos.state)

              if move.origin:
                  cr.execute("SELECT id from sale_order where name=%s", (move.origin,))
                  if cr.rowcount:
                      id_venta = cr.fetchone()[0]
                     
                      venta_obj = self.pool.get('sale.order').browse(cr, uid, id_venta, context=context)
                      cr.execute("SELECT price_unit from sale_order_line AS sol INNER JOIN sale_order AS so ON sol.order_id=so.id where product_id=%s and order_id=%s",
                                (move.product_id.id, id_venta,))
                      price_init = cr.fetchone()[0]

                      cr.execute("select name from sale_order where name=%s", (invoice_id.origin,))
                      if cr.rowcount:
                          folio_venta = cr.fetchone()[0]
                         
                          cr.execute("select id from stock_picking where origin=%s", (folio_venta,))
                          if cr.rowcount:
                              picking_id = cr.fetchone()[0]
                              cr.execute("select pricelist_id from sale_order where name=%s", (folio_venta,))
                              pricelist_id = cr.fetchone()[0]
                              price_obj = self.pool.get('product.pricelist').browse(cr, uid, pricelist_id, context=context)
                              
                              almacen = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
                              
                              cr.execute("select number from account_invoice where origin=%s", (almacen.name,))

                              if cr.rowcount:
                                  number_fac = cr.fetchone()[0]
                                  
                                  cr.execute("SELECT invoice_datetime from account_invoice where origin=%s",
                                             (almacen.name,))
                                  if cr.rowcount:
                                      date_invoice = cr.fetchone()[0]
                                      
                                      cr.execute("""select pp.cost from product_product AS p
                                                    INNER JOIN product_template AS pt ON p.product_tmpl_id=pt.id
                                                    INNER JOIN product_price_history AS pp ON pt.id=pp.product_template_id
                                                    where pp.product_template_id=%s  and pp.create_date<=%s order by pp.id desc""",
                                                    (move.product_id.id, date_invoice))
                                      if cr.rowcount:
                                          costo_producto = cr.fetchone()[0]
                                          if costo_producto < 1:
                                            costo_producto = 0.0

                                      else:
                                          costo_producto = 0.0
                                      if move.precio < 0:
                                         costo_producto = costo_producto * -1
                                          
                                  if price_init == 0.00:
                                      p_utilidad = -100.00
                                  elif (price_init > 0.00):
                                      importe = move.product_uom_qty * price_init
                                      costo_total = costo_producto * move.product_uom_qty
                                      p_utilidad = 0

                                  invoice_lines.write(cr, uid, invoice_id.id,
                                                      {'clave_articulo': move.product_id.default_code,
                                                       'descripcion': move.product_id.name_template,
                                                       'sucursal': move.warehouse_id.name,
                                                       'numero': datos.number,
                                                       'precio_unitario': price_init,
                                                       'precio': precio,
                                                       'importe': total,
                                                       'clave_agente': uid,
                                                       'name_agente': datos.user_id.name,
                                                       'clave_cliente': move.partner_id.id,
                                                       'name_cliente': datos.partner_id.name,
                                                       'costo_unitario': costo_producto,
                                                       'costo_total': costo_producto * move.product_uom_qty,
                                                       'descuento': float(price_obj.version_id.items_id.price_discount * -100),
                                                       'p_utilidad': utilidad,
                                                       'utilidad': total - costo_total,
                                                       'documento': doc,
                                                       'cantidad': quantity,
                                                       'currency': datos.currency_id.name,
                                                       'state_fac': x,
                                                       'pay_method': datos.partner_id.pay_method_id.name,
                                                       'marca': move.product_id.categ_id.name,
                                                       'seccion': move.product_id.categ_id.parent_id.parent_id.name,
                                                       },
                                                      context=context)
                                  origin = ""
      return True

  def action_wizard(self, cr, uid, ids, context=None):
    columna = "\"Clave Articulo\",\"Descripcion\",\"Marca\",\"Seccion\",\"Documento\",\"Sucursal\",\"Folio\",\"Cantidad\",\"Precio\",\"Importe\",\"Costo Unitario\",\"Costo Total\",\"% Utilidad\",\"Monto\",\"Descuento\",\"Cliente\",\"Pago\",\"Vendedor\",\"Estado\"\n"
    nombre_arch = "Ventas por articulo y cliente"

    for this in self.browse(cr, uid, ids):
        for fac in this.stock_move_ids:
          columna += "\""+unicode(fac.clave_articulo).encode("latin-1")+"\","+"\""+unicode(fac.descripcion.replace('"',"''")).encode("latin-1")+"\","+"\""+unicode(fac.marca).encode("latin-1")+"\","+"\""+unicode(fac.seccion).encode("latin-1")+"\","+"\""+unicode(fac.documento).encode("latin-1")+"\","+"\""+unicode(fac.sucursal).encode("latin-1")+"\","+"\""+unicode(fac.numero).encode("latin-1")+"\","\
           +"\""+unicode(fac.product_qty).encode("latin-1")+"\","+"\""+unicode(fac.precio_unitario).encode("latin-1")+"\","+"\""+unicode(fac.importe).encode("latin-1")+"\","+"\""+unicode(fac.costo_unitario).encode("latin-1")+"\","\
           +"\""+unicode(fac.costo_total).encode("latin-1")+"\","+"\""+unicode(fac.p_utilidad).encode("latin-1")+"\","+"\""+unicode(fac.utilidad).encode("latin-1")+"\","+"\""+unicode(fac.descuento).encode("latin-1")+"\","+"\""+unicode(fac.name_cliente).encode("latin-1")+"\","+"\""+unicode(fac.pay_method).encode("latin-1")+"\","+"\""+unicode(fac.name_agente).encode("latin-1")+"\","+unicode(fac.state_fac).encode("latin-1")+"\n"
        
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_venta_x_articulo_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}

  def filtrar(self, cr, uid, ids, context=None):
    """Funcion que actualiza la tabla account_invoice el campo gv_venta_x_fecha_id"""
    for this in self.browse(cr, uid, ids):
      date_from = this.date_from
      date_to = this.date_to
      product_id = int(this.product_id)
      for this in self.browse(cr, uid, ids):

        for id in ids:
          cr.execute("""UPDATE stock_move set gv_venta_x_articulo_id=null""")
          cr.commit()
          if this.select_by == 'all' and this.filter_by == 'full':
              cr.execute("""UPDATE stock_move SET gv_venta_x_articulo_id=%s  where origin is not null
                                  and substring(origin,1,2)='SO' and invoice_state not in('none','2binvoiced')
                                  and date >=%s and date<=%s """, (id, this.date_from, date_to,))
              cr.commit()
          if this.select_by == 'all' and this.filter_by == 'customer':
              cr.execute("""UPDATE stock_move SET gv_venta_x_articulo_id=%s  where partner_id=%s and origin is not null
                                  and substring(origin,1,2)='SO' and invoice_state not in('none','2binvoiced')
                                  and date >=%s and date<=%s """, (id, this.partner_id.id, this.date_from, date_to,))
              cr.commit()
          if this.select_by == 'product' and this.filter_by == 'full':
              cr.execute("""UPDATE stock_move SET gv_venta_x_articulo_id=%s  where product_id=%s and origin is not null
                                  and substring(origin,1,2)='SO' and invoice_state not in('none','2binvoiced')
                                  and date >=%s and date<=%s """, (id, product_id, this.date_from, date_to,))
              cr.commit()
          if this.select_by == 'product' and this.filter_by == 'customer':
              cr.execute("""UPDATE stock_move SET gv_venta_x_articulo_id=%s  where product_id=%s and partner_id=%s and origin is not null
                                  and substring(origin,1,2)='SO' and invoice_state not in('none','2binvoiced')
                                  and date >=%s and date<=%s """, (id, product_id, this.partner_id.id, this.date_from, date_to,))
              cr.commit()

    self._write_lines(cr, uid, ids, context=None)
    return True

  
  #Nombre del modelo
  _name = 'gv.venta.x.articulo'
  #Nombre de la tabla
  _table = '_gv_reportes'
  _rec_name = 'date_from'
  _mail_flat_thread = False
  _mail_mass_mailing = _('Customers')
  #Columnas y/o campos Tree & Form
 
  _columns = {
    'date_from': fields.datetime('DE: ', required=True),
    'date_to': fields.datetime('A: ', required=True),
    'select_by': fields.selection([
            ('all', 'Todos'),
            ('product', 'Producto'),
            ], 'Filtrar por:', required=True),
      'filter_by': fields.selection([
          ('full', 'Todos'),
          ('customer', 'Cliente'),
      ], 'Filtrar por:', required=True),
    'stock_move_ids': fields.one2many('stock.move','gv_venta_x_articulo_id'),
    'product_id': fields.many2one('product.product', 'Producto:',),
    'partner_id': fields.many2one('res.partner', 'Cliente:', domain=[('customer', '=', True)])
    
  }
ventas_x_articulo()
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

