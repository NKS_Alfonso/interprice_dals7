# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Israel Cabrera Juarez(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
##################################################################################
#                    ventas_x_fecha                                              #
#################################################################################
class ventas_x_fecha( osv.osv ) :
  
  def _get_warehouse(self, cr, uid,ids, context=None):
        invoice_lines = self.pool.get('account.invoice')
  
        origin = ""
        costo_venta = 0.0
        costo_producto = 0.00

        for this in self.browse(cr, uid, ids):
            for invoice_id in this.sale_order_ids:
                factura = self.pool.get('account.invoice').browse(cr, uid, invoice_id.id, context=context)
                origin = factura.origin

                if origin:
                  cr.execute(" SELECT id FROM stock_picking WHERE name=%s", (origin,))
                  if cr.rowcount:
                    ware_house = cr.fetchone()[0]
                    almacen = self.pool.get('stock.picking').browse(cr, uid, ware_house, context=context)
                    cr.execute(" SELECT id FROM sale_order WHERE name=%s", (almacen.origin,))
                    if cr.rowcount:
                      id_venta = cr.fetchone()[0]
                      obj_venta = self.pool.get('sale.order').browse(cr, uid, id_venta, context=context)
            
                    for cost in factura.invoice_line:
                        if factura.documento == 'FAC':
                          cr.execute(""" SELECT pp.cost FROM product_product AS p
                                          INNER JOIN product_template AS pt ON p.product_tmpl_id=pt.id
                                          INNER JOIN product_price_history AS pp ON pt.id=pp.product_template_id
                                          WHERE pp.product_template_id=%s AND pp.create_date<=%s ORDER BY pp.id DESC """,
                                          (cost.product_id.id, invoice_id.date_invoice))
                          if cr.rowcount:
                            costo_producto = cr.fetchone()[0]
                          else:
                              costo_producto = 0
                          costo_venta += (costo_producto * cost.quantity)

                        else:
                            cr.execute(""" SELECT pp.cost FROM product_product AS p
                                            INNER JOIN product_template AS pt ON p.product_tmpl_id=pt.id
                                            INNER JOIN product_price_history AS pp ON pt.id=pp.product_template_id
                                            WHERE pp.product_template_id=%s AND pp.create_date<=%s ORDER BY pp.id DESC """,
                                            (cost.product_id.id, invoice_id.date_invoice))
                            costo_producto = cr.fetchone()[0]
                            costo_venta = (costo_producto * cost.quantity) * -1


                        invoice_lines.write(cr, uid, invoice_id.id, {'sucursal': obj_venta.warehouse_id.name,
                                                                     'costo_unitario': costo_producto,
                                                                     'costo': costo_venta,
                                                                     }, context=context)

                        origin = ""
                        costo_venta=0.0
                      
        return True

  def _get_doc(self, cr, uid, ids, context=None):
      doc = ''
      subtotal = 0
      total = 0
      invoice_lines = self.pool.get('account.invoice')
      for this in self.browse(cr, uid, ids):
          for invoice_id in this.sale_order_ids:
              docu = self.pool.get('account.invoice').browse(cr, uid, invoice_id.id, context=context)

              if docu.type == 'out_invoice':
                  doc = 'FAC'
                  subtotal = docu.amount_untaxed
                  total = docu.amount_total
                  
              if docu.type == 'out_refund':
                  doc = 'NCC'
                  subtotal = docu.amount_untaxed * -1
                  total = docu.amount_total * -1
              invoice_lines.write(cr, uid, invoice_id.id, {'documento': doc,
                                                           'subtotal': subtotal,
                                                           'total': total,
                                                           }, context=context)

  def _get_saldo(self, cr, uid,ids, context=None):
        invoice_lines = self.pool.get('account.invoice')
        utilidad = 0.00
        for this in self.browse(cr, uid, ids):
            cr.execute("""SELECT id from account_invoice where gv_venta_x_fecha_id=%s""",(int(this),))
            for invoice_id in this.sale_order_ids:
                factura = self.pool.get('account.invoice').browse(cr, uid,invoice_id.id, context=context)
                #(1-(factura.costo/factura.amount_untaxed))*100,
                
                credito=factura.residual
                amount_untaxed=abs(factura.total)
                cost = abs(factura.costo)
                monto = amount_untaxed - cost
                
                if factura.documento == 'FAC' and cost > 0:
                    utilidad = (monto / cost) * 100

                if factura.documento == 'NCC' and cost > 0:
                    utilidad = (monto / cost) * 100

                if cost <= 0:
                    utilidad = 100.00
                invoice_lines.write(cr, uid, invoice_id.id, {
                                                            'saldo': credito,
                                                            'utilidad': utilidad,
                                                            'monto': monto,      
                                                            },context=context)
                       
        return True
  
  def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Fecha\",\"Documento\",\"Sucursal\",\"Folio\",\"Cliente\",\"Status\",\"Moneda\",\"Subtotal\",\"Impuesto\",\"Total\",\"Costo\",\"Saldo\",\"% Utilidad\",\"Monto Utilidad\"\n"
    nombre_arch = "Ventas"
    lista = []
    state = ""
    for this in self.browse(cr, uid, ids):
        for fac in this.sale_order_ids:
          if fac.state == 'open': state="Abierto"
          if fac.state == 'paid': state="Pagado" 
          columna += "\""+unicode(fac.date_invoice).encode("latin-1")+"\","+"\""+unicode(fac.documento).encode("latin-1")+"\","+"\""+unicode(fac.sucursal).encode("latin-1")+"\","+"\""+unicode(fac.number).encode("latin-1")+"\","+"\""+unicode(fac.partner_id.name).encode("latin-1")+"\","\
           +"\""+unicode(state).encode("latin-1")+"\","+"\""+unicode(fac.currency_id.name).encode("latin-1")+"\","+"\""+unicode(fac.subtotal).encode("latin-1")+"\","+"\""+unicode(fac.amount_tax).encode("latin-1")+"\","\
           +"\""+unicode(fac.total).encode("latin-1")+"\","+"\""+unicode(fac.costo).encode("latin-1")+"\","+"\""+unicode(fac.saldo).encode("latin-1")+"\","+"\""+unicode(fac.utilidad).encode("latin-1")+"\","+"\""+unicode(fac.monto).encode("latin-1")+"\","+"\n"
        
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_venta_x_fecha_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}

  def filtrar(self, cr, uid, ids, context=None):
    call = 0
    """Funcion que actualiza la tabla account_invoice el campo gv_venta_x_fecha_id"""
    for this in self.browse(cr, uid, ids):
      date_from = this.date_from
      date_to = this.date_to
      for id in ids:
        cr.execute("""UPDATE account_invoice set gv_venta_x_fecha_id=null""")
        cr.execute("""UPDATE account_invoice SET gv_venta_x_fecha_id=%s
                      WHERE (date_invoice>=%s and date_invoice<=%s)
                      and state in('open','paid') and type in('out_invoice','out_refund')""",(id,date_from,date_to,))
        cr.commit()
    self._get_saldo(cr, uid, ids, context=None)
    self._get_warehouse(cr, uid, ids, context=None)
    self._get_doc(cr, uid, ids, context=None)
    return True
  
  #Nombre del modelo
  _name = 'gv.venta.x.fecha'
  #Nombre de la tabla
  _table = '_gv_reportes'
  _rec_name = 'date_from'
  #Columnas y/o campos Tree & Form
 
  _columns = {
    
    'date_from': fields.datetime('DE: ', required=True),
    'date_to': fields.datetime('A: ',required=True),
    'sale_order_ids': fields.one2many('account.invoice','gv_venta_x_fecha_id'),  
  }
  _defaults = {}

ventas_x_fecha()

##################################################################################
#                    account_invoice                                             #
#################################################################################
class account_invoice(models.Model):
    _inherit = 'account.invoice'
    def sucursal(self, cr, uid, ids, name, arg, context=None):
     res={}
     id_venta = ""
     for this in self.browse(cr, uid, ids) :
       origin = this.origin
       # id_venta = self.pool.get('sale.order').search(cr, uid,[('name', '=', origin)])
       cr.execute("select id from sale_order where name=%s",(this.origin,))
       if cr.rowcount:
         id_venta = cr.fetchone()[0]
         cr.execute('SELECT sw.name FROM stock_warehouse AS sw INNER JOIN sale_order AS so ON sw.id = so.warehouse_id WHERE so.id=%s',(id_venta,))
         almacen= cr.fetchone()[0]
        
       for record in self.browse(cr, uid, ids, context=context):
                res[record.id] = almacen
     return res
    _columns = {
              #CAMPO RELACIONADO PARA LOS REPORTES POR FECHA
              'gv_venta_x_fecha_id' : fields.many2one('gv.venta.x.fecha'),
              #CAMPO RELACIONADO PARA LOS REPORTES POR ARTICULO
              'gv_venta_x_articulo_id' : fields.many2one('gv.venta.x.articulo'),
              'stock_id': fields.many2one('gv.venta.x.art', 'Sale'),
              #CAMPO RELACIONADO PARA LOS REPORTES POR CLIENTE
              'gv_venta_x_cliente_id' : fields.many2one('gv.venta.x.cliente'),
              'sucursal': fields.char('Sucursal', size=254),
              'documento': fields.char('Documento', size=250),
              'saldo': fields.float('Saldo', digits_compute= dp.get_precision('Product Price')),
              'utilidad': fields.float('Utilidad', digits_compute= dp.get_precision('Product Price')),
              'monto': fields.float('Monto', digits_compute= dp.get_precision('Product Price')),
              'numero_cliente': fields.integer('Numero cliente', size=250),
              'numero_agente': fields.integer('Numero agente', size=250),
              'costo': fields.float('Costo', digits_compute= dp.get_precision('Product Price')),
              'costo_unitario': fields.float('Pre'),
              'folio_venta': fields.char('Pedido de venta', size = 254),
              'subtotal': fields.float('Subtotal'),
              'total': fields.float('Total'),
              
               }
account_invoice()
#TERMINA LA CLASE account_invoice

##################################################################################
#                    product_template                                             #
#################################################################################
class product_template(models.Model):
    _inherit = 'product.template'
    _columns = {
           
              #CAMPO RELACIONADO PARA LOS REPORTES POR FECHA
              'gv_venta_x_articulo_id' : fields.many2one('gv.venta.x.articulo'),
               } 
         
product_template()
#TERMINA LA CLASE product_template

##################################################################################
#                    stock_move                                                 #
#################################################################################

class stock_move(models.Model):
    _inherit = 'stock.move'
    
    def _get_sucursal(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene la sucursal de donde se saco el producto"""
      result = {}
      for this in self.browse(cr, uid, ids):
          name = this.warehouse_id.name
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = name
      return	result
    
    
    def _get_number(self, cr, uid, ids, name, arg, context = {} ):
       """Obtiene el folio de la factura correspondiente a este producto"""
       invoice_obj = self.pool.get('account.invoioce')
       result = {}
       for this in self.browse(cr, uid, ids):
            if not this.origin:
               for record in self.browse( cr, uid, ids, context = context):
                  result[record.id] = "S/F"
               return	result
            else:
                cr.execute("SELECT id FROM account_invoice WHERE origin=%s",(this.origin,))
                if not  cr.rowcount: pass
                else:
                 id_invoice = cr.fetchone()[0]
                 # raise osv.except_osv( ('Alerta!'), ('No')+str(id_invoice) )
                 cr.execute("SELECT number FROM account_invoice WHERE id=%s",(id_invoice,))
                 if not  cr.rowcount: pass
                 else:
                  folio = str(cr.fetchone()[0])
          
                  for record in self.browse( cr, uid, ids, context = context):
                   result[record.id] = folio
                  return	result
      
    def _get_qty(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene la sucursal de donde se saco el producto"""
      result = {}
      for this in self.browse(cr, uid, ids):
          qty = this.product_uom_qty
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = qty
      return	result
    
    def _get_precio_unico(self, cr, uid, ids, name, arg, context = {} ):
      """Obtiene la sucursal de donde se saco el producto"""
      result = {}
      for this in self.browse(cr, uid, ids):
          qty = this.product_uom_qty
      for record in self.browse( cr, uid, ids, context = context):
          result[record.id] = qty
      return	result
    
    _columns = {
           
              #CAMPO RELACIONADO PARA LOS REPORTES POR FECHA
              'gv_venta_x_articulo_id' : fields.many2one('gv.venta.x.articulo'),
              #-----------------------------------------------------------------
              'clave_articulo': fields.char('Clave Articulo'),
              'descripcion': fields.char('Descripcion'),
              'documento': fields.char('Documento', size=34),
              'sucursal': fields.char('Sucursal'),
              'numero': fields.char('Folio'),
              'cantidad': fields.float('Cantidad'),
              'precio_unitario': fields.float('Precio'),
              'importe': fields.float('Importe'),
              'costo_unitario': fields.float('Pre'),
              'costo_total' : fields.float('costo_total'),
              'importe' : fields.float('importe'),
              'p_utilidad': fields.float("% utilidad",digits_compute= dp.get_precision('Product Price')),
              'utilidad': fields.float("Utilidad"),
              'descuento': fields.float("Descuento"),
              'clave_cliente': fields.char(""),
              'name_cliente': fields.char(""),
              'clave_agente': fields.integer(""),
              'name_agente': fields.char(""),
              'currency': fields.char('Moneda'),
              'precio': fields.float('Precio'),
              'pay_method': fields.char(""),
              'marca': fields.char('Marca'),
              'seccion': fields.char('Sección'),
              
              ###camppos para el reportes de series en inventario
              
            'union': fields.char('Union', size=254),
            'folio':fields.char('Folio', size=250),
            'fecha': fields.datetime('Fecha', size=250),
            'clave': fields.char('Clave Articulo', size=250),
            'descripcion': fields.char('Descripcion Articulo ', size=254),
            'lote': fields.char('Lote', size=254),
            'fecha_creada': fields.char('Fecha Creada', size=254),
            'pedimento': fields.char('Pedimento', size=254),
            
            'gv_venta_x_compras_id': fields.many2one('gv.venta.x.compras'),
               }
         
stock_move()
#TERMINA LA CLASE stock_move

##################################################################################
#                    wizard_export                                              #
#################################################################################
class wizard_export(osv.osv_memory):
    _name = "wizard.export"
    
    _columns = {
            'name': fields.char('File Name', readonly=True),
            'data': fields.binary('File', readonly=True),
            #CAMPO RELACIONADO PARA LOS REPORTES POR FECHA
            'gv_venta_x_fecha_id': fields.many2one('gv.venta.x.fecha'),
            #CAMPO RELACIONADO PARA LOS REPORTES POR ARTICULO
            'gv_venta_x_articulo_id': fields.many2one('gv.venta.x.articulo'),
            #CAMPO RELACIONADO PARA LOS REPORTES POR CLIENTE
            'gv_venta_x_articulo_id': fields.many2one('gv.venta.x.cliente'),
            #CAMPO PARA EL REPORTE DE INVENTARIO HISTORICO
            'gv_inventario_historico_id': fields.many2one('gv.inventario.historico'),
            #CAMPO PARA EL REPORTE DE INVENTARIO CON EXISTENCIAS
            'gv_inventario_existencias_id': fields.many2one('gv.inventario.existencias'),
            #CAMPO PARA EL REPORTE DE INVENTARIO CON EXISTENCIAS
            'gv_inventario_analitico_id': fields.many2one('gv.inventario.analitico'),
            #CAMPO PARA EL REPORTE DE SERIES EN INVENTARIO
            'gv_venta_x_compras_id':fields.many2one('gv.venta.x.compras'),
            
    }
    _defaults = {}

#TERMINA LA CLASE wizard_export 
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

