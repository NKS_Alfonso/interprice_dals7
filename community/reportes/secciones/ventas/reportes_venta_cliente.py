# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Israel Cabrera Juarez(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs

##################################################################################
#                    ventas_x_cliente                                           #
#################################################################################
class ventas_x_cliente( osv.osv ) :
  def _get_warehouse(self, cr, uid,ids, context=None):
        invoice_lines = self.pool.get('account.invoice')
        # lotes_ids = ""
        origin = ""
        for this in self.browse(cr, uid, ids):
            for invoice_id in this.sale_order_ids:
                factura = self.pool.get('account.invoice').browse(cr, uid,invoice_id.id, context=context)
                origin=factura.origin
                cr.execute("select warehouse_id from sale_order where name=%s",(origin,))
                if cr.rowcount:
                  ware_house = cr.fetchone()[0]
                  almacen = self.pool.get('stock.warehouse').browse(cr, uid,ware_house, context=context)
                  invoice_lines.write(cr, uid, invoice_id.id, {'sucursal': almacen.name}, context=context)
                  origin = ""

  def _get_saldo(self, cr, uid, ids, context=None):
      invoice_lines = self.pool.get('account.invoice')
      for this in self.browse(cr, uid, ids):
          cr.execute("""SELECT id from account_invoice where gv_venta_x_cliente_id=%s""", (int(this),))
          costo_venta = 0.
          for invoice_id in this.sale_order_ids:
              factura = self.pool.get('account.invoice').browse(cr, uid, invoice_id.id, context=context)
              credito = factura.partner_id.credit
              
                    
              if factura.origin:
                  cr.execute("select id from stock_picking where name=%s", (factura.origin,))
                  if cr.rowcount:
                      picking_id = cr.fetchone()[0]
                      obj_oicking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
                      sucursal = obj_oicking.picking_type_id.warehouse_id.name
                  else:
                      sucursal = ''
                  # --------------------------------------------------------------------------------------
                  for cost in factura.invoice_line:
                      if factura.documento == 'FAC':
                          cr.execute("""select pp.cost from product_product AS p
                                       INNER JOIN product_template AS pt ON p.product_tmpl_id=pt.id
                                       INNER JOIN product_price_history AS pp ON pt.id=pp.product_template_id
                                       where pp.product_template_id=%s and pp.create_date<=%s order by pp.id desc""",
                                     (cost.product_id.id, invoice_id.date_invoice))
                          if cr.rowcount:
                            costo_producto = cr.fetchone()[0]
                          else:
                            costo_producto = 0
                          costo_venta += (costo_producto * cost.quantity)
                      else:
                          cr.execute("""select pp.cost from product_product AS p
                              INNER JOIN product_template AS pt ON p.product_tmpl_id=pt.id
                              INNER JOIN product_price_history AS pp ON pt.id=pp.product_template_id
                              where pp.product_template_id=%s and pp.create_date<=%s order by pp.id desc""",
                                     (cost.product_id.id, invoice_id.date_invoice))
                          costo_producto = cr.fetchone()[0]
                          costo_venta = (costo_producto * cost.quantity) * -1
                          # --------------------------------------------------------------------------------------
                          
                  amount_untaxed = abs(factura.total)
                  cost1 = abs(costo_venta)
                  
                  monto = amount_untaxed - cost1
    
                  if factura.documento == 'FAC' and cost1 > 0:
                        utilidad = (monto / cost1) * 100
    
                  if factura.documento == 'NCC' and cost1 > 0:
                        utilidad = (monto / cost1) * 100
    
                  if cost1 <= 0:
                        utilidad = 100.00       
                  invoice_lines.write(cr, uid, invoice_id.id, {
                                                              'sucursal': sucursal,
                                                              'numero_cliente': factura.partner_id.id,
                                                              'numero_agente': uid,
                                                              'folio_venta': obj_oicking.origin,
                                                              'costo': costo_venta,
                                                              'utilidad': utilidad,
                                                              'monto': monto,
                                                              'saldo': factura.residual
                                                              }, context=context)
                  costo_venta = 0.0

  def _get_doc(self, cr, uid, ids, context=None):
      doc = ''
      subtotal = 0
      total = 0
      invoice_lines = self.pool.get('account.invoice')
      for this in self.browse(cr, uid, ids):
          for invoice_id in this.sale_order_ids:
              docu = self.pool.get('account.invoice').browse(cr, uid, invoice_id.id, context=context)

              if docu.type == 'out_invoice':
                  doc = 'FAC'
                  subtotal = docu.amount_untaxed
                  total = docu.amount_total
              if docu.type == 'out_refund':
                  doc = 'NCC'
                  subtotal = docu.amount_untaxed * -1
                  total = docu.amount_total * -1
              invoice_lines.write(cr, uid, invoice_id.id, {'documento': doc,
                                                           'subtotal': subtotal,
                                                           'total': total,
                                                           }, context=context)
  
  def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Fecha\",\"Documento\",\"Sucursal\",\"Folio\",\"Cliente\",\"Status\",\"Moneda\",\"Subtotal\",\"Impuesto\",\"Total\",\"Costo\",\"Saldo\",\"% Utilidad\",\"Monto Utilidad\"\n"
    nombre_arch = "Ventas"
    lista = []
    state = ""
    for this in self.browse(cr, uid, ids):
        for fac in this.sale_order_ids:
          if fac.state == 'open': state="Abierto"
          if fac.state == 'paid': state="Pagado" 
          columna += "\""+unicode(fac.date_invoice).encode("latin-1")+"\","+"\""+unicode(fac.documento).encode("latin-1")+"\","+"\""+unicode(fac.sucursal).encode("latin-1")+"\","+"\""+unicode(fac.number).encode("latin-1")+"\","+"\""+unicode(fac.partner_id.name).encode("latin-1")+"\","\
           +"\""+unicode(state).encode("latin-1")+"\","+"\""+unicode(fac.currency_id.name).encode("latin-1")+"\","+"\""+unicode(fac.subtotal).encode("latin-1")+"\","+"\""+unicode(fac.amount_tax).encode("latin-1")+"\","\
           +"\""+unicode(fac.total).encode("latin-1")+"\","+"\""+unicode(fac.costo).encode("latin-1")+"\","+"\""+unicode(fac.residual).encode("latin-1")+"\","+"\""+unicode(fac.utilidad).encode("latin-1")+"\","+"\""+unicode(fac.monto).encode("latin-1")+"\","+"\n"
        
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_venta_x_cliente_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}

  def filtrar(self, cr, uid, ids, context=None):
      """Funcion que actualiza la tabla account_invoice el campo gv_ventas_x_cliente_id"""
      data = self.browse(cr, uid, ids)
      cr.execute("""UPDATE account_invoice SET gv_venta_x_cliente_id=null""")

      if data.select_by == 'all':
          cr.execute("""UPDATE account_invoice SET gv_venta_x_cliente_id=%s WHERE (date_invoice>=%s and date_invoice<=%s)
                                                                and state in('open','paid') and type in('out_invoice','out_refund')""",
                     (data.id, data.date_from, data.date_to,))

      elif data.select_by == 'customer':
          cr.execute("""UPDATE account_invoice SET gv_venta_x_cliente_id=%s WHERE (date_invoice>=%s and date_invoice<=%s)
                                                                and state in('open','paid') and partner_id=%s and type in('out_invoice','out_refund')""",
                     (data.id, data.date_from, data.date_to, data.partner_id.id))
          cr.commit()
      self._get_saldo(cr, uid, ids, context=None)
      self._get_doc(cr, uid, ids, context=None)
      return True

  
  #Nombre del modelo
  _name = 'gv.venta.x.cliente'
  #Nombre de la tabla
  _table = '_gv_reportes'
  _rec_name = 'date_from'
  #Columnas y/o campos Tree & Form
 
  _columns = {
    
    'date_from': fields.datetime('DE: ', required=True),
    'date_to': fields.datetime('A: ', required=True),
    'partner_id': fields.many2one('res.partner', 'Cliente:', required=True, domain=[('customer', '=',True)]),
    'sale_order_ids': fields.one2many('account.invoice', 'gv_venta_x_cliente_id'),
    'select_by': fields.selection([('all', 'Todos'), ('customer', 'Cliente'), ], 'Filtrar por:', required=True, ),
  }
  _defaults = {}

ventas_x_cliente()
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

