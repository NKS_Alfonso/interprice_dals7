# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : Viridiana Cruz Santos                                                                                               #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
##################################################################################
#                    compras_x_fecha                                              #
#################################################################################
class compras_x_fecha( osv.osv ) :
  
  
  #Nombre del modelo
 _name = 'gv.compra.x.fecha'
  #Nombre de la tabla
 _table = '_reporte_compras'
 #_rec_name = 'date_from'



 def imprimir_listado_compras(self, cr, uid, ids, context=None):

  data = self.browse(cr, uid, ids)
  cr.execute("""DELETE FROM _reporte_compras WHERE id!=%s""",(data.id,))
  cr.execute("""UPDATE purchase_order set gv_compra_x_fecha_id=null """)

  cr.execute(""" UPDATE purchase_order SET gv_compra_x_fecha_id=%s
                      WHERE date_order>=%s and date_order<=%s and state !='cancel' and state!= 'draft' """,
                      (data.id,data.date_from,data.date_to,))

  self.tipo_cambio(cr, uid, ids, context=context)

 def cambio_status_fac(self, status):
      state_fac = ""
      if status == u'open': state_fac = "Abierta"
      if status == u'paid': state_fac = "Pagada"
      if status == u'done': state_fac = "Hecho"
      if status == u'draft': state_fac = "Borrador"
      if status == u'cancel': state_fac = "Cancelado"
      return state_fac

 def tipo_cambio(self, cr, uid, ids, context=None):
  cr.execute (""" SELECT id FROM res_currency where name='MXN' """)
  moneda_id = cr.fetchone()[0]

  for compras in self.browse(cr, uid, ids):
    for compra in compras.order_ids :
      if compra.currency_id.name == 'USD' :
        if compra.currency_rate_alter == 0.00 :
          cr.execute ("""SELECT rate FROM res_currency_rate 
             WHERE name<=%s AND currency_id=%s """,(compra.date_order,moneda_id,))
          result = cr.fetchone()[0]
          obj_compra = self.pool.get('purchase.order').write(cr, uid, compra.id, {'rate': result}, context=context)

        if compra.currency_rate_alter > 0.00 :
          obj_compra = self.pool.get('purchase.order').write(cr, uid, compra.id, {'currency_rate_alter': compra.rate},
                                                                                  context=context)

        cr.execute (""" SELECT id FROM account_invoice WHERE origin=%s """, (compra.name,))
        result = cr.fetchone()[0]
        invoice = self.pool.get('account.invoice').browse(cr, uid, result, context=context)
        x = self.cambio_status_fac(invoice.state)

        cr.execute (""" SELECT name FROM stock_picking WHERE origin=%s """, (compra.name,))
        albaran = cr.fetchone()[0]

        obj_compra = self.pool.get('purchase.order').write(cr, uid, compra.id,{
                                                                               'saldo_USD': invoice.residual,
                                                                               'state_fac': x,
                                                                               'number': invoice.number,
                                                                               'ren': albaran },
                                                                               context=context)

 def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Referencia PO\",\"Folio FAC\",\"REN\",\"Fecha\",\"Proveedor\",\"Tasa\",\"Tipo de cambio\",\"Moneda\",\"Subtotal\",\"Total\",\"Estado PROD\",\"Estado FAC\"\n"
    nombre_arch = "Compras por fechas"
    
    data = self.browse(cr, uid, ids)
    for compra in data.order_ids :
        if compra.state == 'approved': state="Disponible"
        if compra.state == 'done': state="Hecho"
        if compra.state == 'except_picking': state="Excepcion de envio"
        if compra.state == 'except_invoice': state="Excepcion de factura"
        columna += "\""+unicode(compra.name).encode("latin-1")+"\","\
                  +"\""+unicode(compra.number).encode("latin-1")+"\","\
                  +"\""+unicode(compra.ren).encode("latin-1")+"\","\
                  +"\""+unicode(compra.date_order).encode("latin-1")+"\","\
                  +"\""+unicode(compra.partner_id.name).encode("latin-1")+"\","\
                  +"\""+unicode(compra.rate).encode("latin-1")+"\","\
                  +"\""+unicode(compra.currency_rate_alter).encode("latin-1")+"\","\
                  +"\""+unicode(compra.currency_id.name).encode("latin-1")+"\","\
                  +"\""+unicode(compra.amount_untaxed).encode("latin-1")+"\","\
                  +"\""+unicode(compra.amount_total).encode("latin-1")+"\","\
                  +"\""+unicode(state).encode("latin-1")+"\","\
                  +"\""+unicode(compra.state_fac).encode("latin-1")+"\","+"\n"

    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_compra_x_fecha_id':ids[0]
    }
    id=self.pool.get('wizard.export.fecha').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export.fecha',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}
  #Columnas y/o campos Tree & Form
 
 _columns = {
    'date_from': fields.datetime('DE: ', required=True),
    'date_to': fields.datetime('A: ', required=True),
    'order_ids' : fields.one2many('purchase.order', 'gv_compra_x_fecha_id', 'Compras', readonly=True),
  }

 _defaults = {}

compras_x_fecha()

class purchase_order( osv.osv ):
  _inherit = 'purchase.order'

  _columns = {
     'gv_compra_x_fecha_id' : fields.many2one ('gv.compra.x.fecha', 'Compra'),
     'rate' : fields.float (),
     'saldo_USD' : fields.float (),
     'number' : fields.char(visible=False),
     'ren' : fields.char(),
     'state_fac': fields.char(visible=False),
     # 'currency_rate_alter': fields.integer(),
     'name_cost': fields.char('Costos adicionales', size=254),
  } 
purchase_order()

class wizard_export_fecha(osv.osv_memory):
    _name = "wizard.export.fecha"
    
    _columns = {
            'name': fields.char('File Name', readonly=True),
            'data': fields.binary('File', readonly=True),
             #CAMPO PARA EL REPORTE DE COMRAS POR FECHA
            'gv_compra_x_fecha_id' : fields.many2one('gv.compra.x.fecha')
    }
    _defaults = {}

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

