# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : Viridiana Cruz Santos                                                                                               #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
##################################################################################
#                    compras_x_articulo                                              #
#################################################################################
class compras_x_art( osv.osv ) :

 #Nombre del modelo
 _name = 'gv.compra.x.art'
 #Nombre de la tabla
 _table = '_reporte_compras_art'
 #_rec_name = 'date_from'

  #Columnas y/o campos Tree & Form
 _columns = {
    'date_from': fields.datetime('DE: ', required=True),
    'date_to': fields.datetime('A: ', required=True),
    'select_by': fields.selection([
            ('all', 'Todos'),
            ('product', 'Producto'),
            ], 'Filtrar por:', required=True,),
    
    'stock_move_ids': fields.one2many('stock.move','stock_id'),
    'product_id': fields.many2one('product.product', 'Producto',),
  }
 
 def filtrar(self, cr, uid, ids, context=None):
   data = self.browse(cr, uid, ids)
   cr.execute("""UPDATE stock_move set stock_id=null""")

   if data.select_by=='all' :
      cr.execute("""UPDATE stock_move SET stock_id=%s  where origin is not null
                           and substring(origin,1,2)='PO'
                           and date >=%s and date<=%s """,(data.id,data.date_from,data.date_to,))

      
   elif data.select_by=='product' :
      cr.execute("""UPDATE stock_move SET stock_id=%s  where origin is not null
                           and substring(origin,1,2)='PO'
                           and date >=%s and date<=%s and product_id=%s """,
                           (data.id,data.date_from,data.date_to,data.product_id.id,))
      
   self.tipo_cambio(cr, uid, ids, context=context)

 def cambio_status_fac(self, status):
     state_fac = ""

     if status == u'open': state_fac = "Abierta"
     if status == u'paid': state_fac = "Pagada"
     if status == u'done': state_fac = "Hecho"
     if status == u'draft': state_fac = "Borrador"
     if status == u'cancel': state_fac = "Cancelado"
     return state_fac

 def tipo_cambio(self, cr, uid, ids, context=None):

  for compras in self.browse(cr, uid, ids):
    for compra in compras.stock_move_ids :
      
      move = self.pool.get('stock.move').browse(cr, uid,compra.id, context=context)

      cr.execute (""" SELECT id FROM account_invoice WHERE origin=%s """, (compra.origin,))
      result = cr.fetchone()[0]
      invoice = self.pool.get('account.invoice').browse(cr, uid, result, context=context)

      x = self.cambio_status_fac(invoice.state)

      obj_compra = self.pool.get('stock.move').write(cr, uid, compra.id,{'importe': move.product_uos_qty*move.price_unit,
                                                                         'code_art': move.product_id.default_code,
                                                                         'name_template_art': move.product_id.name_template,
                                                                         'state_fac': x,
                                                                         'number': invoice.number,
                                                                         # 'partner_id': invoice.partner_id.name,
                                                                         'amount_total': invoice.amount_total,
                                                                         }, context=context)



         
 def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Referencia PO\",\"Folio FAC\",\"Proveedor\",\"Clave artículo\",\"Descripción\",\"Cantidad\",\"Precio unitario\",\"Subtotal\",\"Total FAC\",\"Estado PROD\",\"Estado FAC\"\n"
    nombre_arch = "Compras por artículo"
    
    data = self.browse(cr, uid, ids)
    for compra in data.stock_move_ids :
        if compra.state == 'assigned': state = "Disponible"
        if compra.state == 'done': state = "Hecho"
        columna += "\""+unicode(compra.origin).encode("latin-1")+"\","\
                  +"\""+unicode(compra.number).encode("latin-1")+"\","\
                  +"\""+unicode(compra.partner_id).encode("latin-1")+"\","\
                  +"\""+unicode(compra.code_art).encode("latin-1")+"\","\
                  +"\""+unicode(compra.name_template_art).encode("latin-1")+"\","\
                  +"\""+unicode(compra.product_uos_qty).encode("latin-1")+"\","\
                  +"\""+unicode(compra.price_unit).encode("latin-1")+"\","\
                  +"\""+unicode(compra.importe).encode("latin-1")+"\","\
                  +"\""+unicode(compra.amount_total).encode("latin-1")+"\"," \
                  +"\""+unicode(state).encode("latin-1") + "\"," \
                  +"\""+unicode(compra.state_fac).encode("latin-1")+"\","+"\n"

    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_compra_x_art_id':ids[0]
    }
    id=self.pool.get('wizard.export.art').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export.art',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}   
    
 _defaults = {}

compras_x_art()

class stock_move( osv.osv ) :
 _inherit = 'stock.move'
 
  #Columnas y/o campos Tree & Form
 _columns = {
    'stock_id': fields.many2one('gv.compra.x.art', 'Move'),
    'number' : fields.char(visible=False),
    'state_fac': fields.char(visible=False),
    'amount_total': fields.integer(),
    'code_art' : fields.char(),
    'name_template_art': fields.char(),
    # 'partner_id': fields.char(),
  }
 
 _defaults = {}

stock_move()

class wizard_export_art(osv.osv_memory):
    _name = "wizard.export.art"
    
    _columns = {
            'name': fields.char('File Name', readonly=True),
            'data': fields.binary('File', readonly=True),
             #CAMPO PARA EL REPORTE DE COMRAS POR PROVEEDOR
            'gv_compra_x_art_id' : fields.many2one('gv.compra.x.art')
    }
    _defaults = {}

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

