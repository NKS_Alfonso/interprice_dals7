# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Javier Barajas(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
import logging
_logger = logging.getLogger(__name__)

class inventario_analitico(osv.osv):
    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    def action_wizard(self, cr, uid, ids, context=None):
        """Funcion que exporta a exel"""
        columna = "\"CLAVE ARTICULO\",\"DESCRIPCION\",\"EXISTENCIA ANTERIOR\",\"CANTIDAD\",\"COSTO UNITARIO DEL MOVIMIENTO\",\"COSTO PROMEDIO\",\"FECHA MOVIMIENTO\",\"NO.FOLIO\",\"TIPO DOCUMENTO\",\"NO.ALMACEN\",\"STATUS\",\"PRECIO\",\"MONEDA\",\"EXISTENCIAS\",\"NO.FACTURA\",\"NO.PROVEEDOR\",\"PROVEEDOR\",\"CATEGORIA\"\n"
        nombre_arch = "inventario_analitico"
        lista = self.browse(cr, uid, ids)
        for fac in lista:
            #cr.execut("""select timezone('utc',fecha_movimiento) from _gv_inventario_analitico where id=%s""",
            #          (fac.id,))
            fecha_movimiento = self.utc_mexico_city(fac.fecha_movimiento)
            columna += "\"" + unicode(fac.clave_articulo).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.descripcion.replace('"', "''")).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.existencia_anterior).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.cantidad).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.costo_movimiento).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.costo_promedio).encode("utf-8") + "\"," \
                       + "\"" + unicode(fecha_movimiento[:19]).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.no_folio).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.tipo_documento).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.numero_almacen).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.estado).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.precio).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.moneda).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.existencias).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.numero_factura).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.numero_proveedor).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.proveedor).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.categoria.parent_id.parent_id.name).encode("utf-8") \
                       + "/" + unicode(fac.categoria.parent_id.name).encode("utf-8") + "/" + unicode(
                fac.categoria.name).encode("utf-8") + "\"," + "\n"

        # --------- termina contenido archivo
        buf = cStringIO.StringIO()
        buf.write(columna)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        filename_psn = nombre_arch + ".csv"
        # --------end------------------------
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': filename_psn,
            'type_file': 'csv',
            'file': out,
        }, context=None)

    def obtenerInfo(self, cr, data):
        lista_almacenes = {}

        #strCondicionConsignacion = ''  # Todos los articulos
        #if data.select_by == 'sinconsigna':  # Articulos sin consignacion
        #    strCondicionConsignacion = "AND SW.type != 'pre'"
        #elif data.select_by == 'soconsigna':  # Articulos que tengan consignacion solamente
        #    strCondicionConsignacion = "AND SW.type = 'pre'"

        strCondicionProductos = ''  # Todos los productos
        if data.iIdProductoInicial.default_code and data.iIdProductoFinal.default_code:
            strCondicionProductos = """ PP.default_code>='{}' AND PP.default_code<='{}'""".format(
                data.iIdProductoInicial.default_code, data.iIdProductoFinal.default_code)
        elif data.iIdProductoInicial.default_code:
            strCondicionProductos = """ PP.default_code ='{}'""".format(data.iIdProductoInicial.default_code)

        strCondicionAlmacenes = ''  # Todos los Almacenes
        if data.iAlmacenInicial.id and data.iAlmacenFinal.id:
            strCondicionAlmacenes = """{} HT.warehouse_id >= {} AND HT.warehouse_id <= {}""".format(
                (' AND 'if len(strCondicionProductos) > 0 else ''), data.iAlmacenInicial.id, data.iAlmacenFinal.id)
        elif data.iAlmacenInicial.id:
            strCondicionAlmacenes = """{} HT.warehouse_id = {}""".format((' AND 'if len(strCondicionProductos) > 0 else ''), data.iAlmacenInicial.id)

        strWhere = ''
        if len(strCondicionProductos) > 0 or len(strCondicionAlmacenes) > 0:
            strWhere = ' WHERE'

        strConsulta = """
                SELECT  PP.id iIdProducto,
                        PP.product_tmpl_id iIdTmpl,
                        default_code strSku,
                        name_template strProducto,
                        existencia_anterior iExistenciaAnterior,
                        existencia_posterior iExistenciaPosterior,
                        product_qty_move iCantidad,
                        sum(/*SM.product_uom_qty */ SM.price_unit) fCostoMovimiento,
                        HT.create_date dtFecha,
                        -- CASE WHEN picking_origin = '' THEN SI.name ELSE picking_origin END strFolio,
                        CASE WHEN picking_origin = '' AND SI.name IS NULL THEN HT.document_origin 
                            WHEN picking_origin = '' AND SI.name IS NOT NULL THEN SI.name ELSE picking_origin END strFolio,
                        GVD.order_id AS strFolioOrigenDevolucion,
                        CASE WHEN GVD.order_id LIKE 'PO%' THEN 'NCP' WHEN GVD.order_id LIKE 'SO%' THEN 'NCC' ELSE '' END AS strtipodevolucion,
                        HT.warehouse_id iIdAlmacen,
                        SM.price_unit fPrecio,
                        --SPT.code str_tipo_albaran,
                        CASE WHEN SPT.code is null THEN HT.mrp_picking_type_code ELSE SPT.code END str_tipo_albaran,
                        picking_origin str_picking_origen,
                        document_origin str_documento_origen,
                        categ_id i_categoria_producto,
                        PT.id i_id_template,
                        SP.date_done AS dtfechatransferencia,
                        HT.picking_id picking_id
                FROM    _history_stock HT
                        JOIN product_product AS PP
                        ON HT.product_id=PP.id
                        LEFT JOIN stock_move AS SM
                        ON SM.picking_id = HT.picking_id
                        AND SM.product_id = HT.product_id
                        LEFT JOIN stock_inventory AS SI
                        ON SI.id = ajuste_id
                        LEFT JOIN stock_picking AS SP
                        ON SP.id = HT.picking_id
                        LEFT JOIN stock_picking_type AS SPT
                        ON SPT.id = SP.picking_type_id
                        LEFT JOIN product_template AS PT
                        ON PT.id = PP.product_tmpl_id
                        LEFT JOIN stock_warehouse AS SW
                        ON HT.warehouse_id = SW.id
                        LEFT JOIN _gv_devolucion AS GVD
                        ON GVD.picking_id= HT.picking_id
                --WHERE   HT.create_date between '' AND to_timestamp('','YYYY-MM-DD HH24:MI:SS')
                {}
                {}
                {}

                GROUP BY PP.id,
                        PP.product_tmpl_id,
                        default_code,
                        name_template,
                        existencia_anterior,
                        existencia_posterior,
                        product_qty_move,
                        HT.create_date,
                        SI.name,
                        GVD.order_id,
                        HT.warehouse_id,
                        SM.price_unit,
                        SPT.code,
                        picking_origin,
                        document_origin,
                        categ_id,
                        PT.id,
                        SP.date_done,
                        HT.mrp_picking_type_code,
                        HT.picking_id
                ORDER BY HT.create_date ASC
        """

        cr.execute(strConsulta.format(
            #data.dtDesde,
            #data.dtHasta,
            #"", #strCondicionConsignacion,
            strWhere,
            strCondicionProductos,
            strCondicionAlmacenes))
        if cr.rowcount:
            lista_almacenes = cr.dictfetchall()

        return lista_almacenes

    def obtenerAlmacenes(self, cr):
        info = {}
        #strConsulta = """SELECT id, name, type FROM stock_warehouse"""
        strConsulta = """SELECT sw.id, sw.name, swt.code FROM stock_warehouse sw left JOIN stock_warehouse_type swt ON sw.stock_warehouse_type_id = swt.id;"""
        cr.execute(strConsulta)
        if cr.rowcount:
            res = cr.dictfetchall()
            for fila in res:
                info.update({fila['id'] : {'nombre' : fila['name'], 'tipo' : fila['code']}})
                #info.update({fila['id'] : {'nombre' : fila['name'] }})
                #info[fila['id']] = fila['name']
        return info

    def obtenerFactura(self, cr, str_picking_origen, i_id_producto):
        res = {}
        strConsulta = """
            select
            number str_numero,
            rc.name str_moneda,
            rp.name str_proveedor,
            rp.id i_proveedor,
            price_unit fprecio,
            price_subtotal fcostomovimiento,
            quantity icantidad,
            ai.type str_type
            from account_invoice ai
            join res_currency rc
            on rc.id = ai.currency_id
            join res_partner rp
            on rp.id = ai.partner_id
            join account_invoice_line ail
            ON ail.invoice_id = ai.id
            where ai.origin = %s
            AND product_id = %s
        """
        cr.execute(strConsulta, (str_picking_origen, i_id_producto))
        if cr.rowcount:
            res = cr.dictfetchall()[0]
        return res

    def obtenerVenta(self, cr, str_picking_origen, i_id_producto):
        res = {}
        strConsulta = """
          select '' str_numero, rc.name str_moneda, rp.name str_proveedor, rp.id i_proveedor,
          sum(price_unit) fprecio, sum(price_unit /* product_uom_qty*/) fcostomovimiento, sum(product_uom_qty) icantidad
          from sale_order so
          JOIN res_company rcom
          ON rcom.id = so.company_id
          join res_currency rc
          on rc.id = rcom.currency_id
          join res_partner rp
          on rp.id = so.partner_id
          join sale_order_line sol
          on sol.order_id = so.id
          where so.name = %s
          AND product_id = %s
          group by rc.name, rp.name, rp.id
        """
        cr.execute(strConsulta, (str_picking_origen, i_id_producto))
        if cr.rowcount:
            res = cr.dictfetchall()[0]
        return res

    def obtenerCompra(self, cr, str_picking_origen):
        res = {}
        strConsulta = """
          select '' str_numero, rc.name str_moneda, rp.name str_proveedor, rp.id i_proveedor
          from purchase_order po
          join res_currency rc
          on rc.id = po.currency_id
          join res_partner rp
          on rp.id = po.partner_id
          where po.name = %s
        """
        cr.execute(strConsulta, (str_picking_origen,))
        if cr.rowcount:
            res = cr.dictfetchall()[0]
        return res

    def _get_analitico_por_consulta(self, cr, uid, data, context=None):
        lista = self.obtenerInfo(cr, data)
        lista_almacenes = self.obtenerAlmacenes(cr)
        obj_analitico = self.pool.get('gv.inventario.analitico')

        cr.execute("DELETE FROM _gv_inventario_analitico WHERE create_uid = {}".format(uid))

        type_dic = {
            'fac': 'Facturacion',
            'pre': 'Prestamo',
            'sop': 'Soporte',
            'tra': 'Transito',
            'scrp': 'Scrap'
        }

        arrIds = []
        line = {}
        line_list = []
        for objeto in lista:
            cr.execute(
                """SELECT cost FROM product_price_history WHERE to_char(create_date,'YYYY-MM-DD HH24:MI:SS')<=%s AND product_template_id=%s ORDER BY create_date DESC""",
                (objeto['dtfecha'], objeto['i_id_template']))
            if cr.rowcount:
                costo_promedio = float("{0:.2f}".format(cr.fetchone()[0]))
            else:
                costo_promedio = 0.0
            if objeto['str_picking_origen']:
                tipo_documento = 'ALBARAN'
                stock_picking_id = self.pool.get(
                    'stock.picking').search(
                    cr, uid, [
                        ('name', '=', objeto['str_picking_origen'])
                    ], context=context
                )
                stock_picking = self.pool.get(
                    'stock.picking').browse(
                    cr, uid, stock_picking_id,
                    context=context
                )
                if stock_picking:
                    tipo_documento = stock_picking.picking_type_id.name
            elif not objeto['str_picking_origen'] and not objeto['picking_id']:
                tipo_documento = 'PRODUCCION'
            else:
                tipo_documento = 'AJUSTE INVENTARIO'

            #costo_movimiento = objeto['fcostomovimiento']
            costo_movimiento = "" #objeto['fprecio']
            precio = "" #objeto['fprecio']
            moneda = ""
            numero_factura = '' #objeto['str_documento_origen']
            partner_id = ''
            name_partner = ''

            if objeto['str_tipo_albaran'] in ['outgoing', 'incoming', 'internal']:
                #Preguntamos, si el documento de origen es un WH/in o WH/OUT hablamos de una devolucion, entonces hay que buscar
                #la factura para esta nota de credito
                bEsNota = False
                objeto_factura = {}
                #Primero revizamos si es una Devolucion, (modificar consulta para unicr con tabla de devoluciones)
                if objeto['strtipodevolucion'] == 'NCP' or objeto['strtipodevolucion'] == 'NCC':
                    objeto_factura = self.obtenerFactura(cr, objeto['str_picking_origen'], objeto['iidproducto'])
                    bEsNota = True
                    numero_factura = objeto_factura['str_numero'] if 'str_numero' in objeto_factura else ''
                    #Si no tiene una documento creado aun, debemos tomar entonces la informacion de la factura de donde venga
                    if len(objeto_factura) == 0:
                        objeto_factura = self.obtenerFactura(cr, objeto['str_documento_origen'], objeto['iidproducto'])
                        numero_factura = 'NC Pendiente'
                elif (objeto['str_tipo_albaran'] == 'outgoing' ):
                    objeto_factura = self.obtenerFactura(cr, objeto['str_picking_origen'], objeto['iidproducto'])
                elif (objeto['str_tipo_albaran'] == 'incoming' ):
                    objeto_factura = self.obtenerFactura(cr, objeto['str_documento_origen'], objeto['iidproducto'])
                    if len(objeto_factura) == 0:
                        objeto_factura = self.obtenerFactura(cr, objeto['str_picking_origen'], objeto['iidproducto'])

                if not bEsNota:
                    numero_factura = objeto_factura['str_numero'] if 'str_numero' in objeto_factura else ''


                #costo_movimiento = objeto_factura['fcostomovimiento']
                # if len(objeto_factura) == 0:
                #     if objeto['str_tipo_albaran'] == 'outgoing':
                #         objeto_factura = self.obtenerVenta(cr, objeto['str_documento_origen'], objeto['iidproducto'])
                #     if objeto['str_tipo_albaran'] == 'incoming':
                #         objeto_factura = self.obtenerCompra(cr, objeto['str_documento_origen'])
                #Existe la factura ya sea para notas de credito, compras o ventas
                if 'str_moneda' in objeto_factura:
                    # trae la tasa para realizar la conversión del
                    # costo promedio a la moneda del documento actual.
                    rate_sale = 0
                    rate_purchase = 0
                    if objeto_factura['str_moneda']:
                        cr.execute("""
                          select rate_sale, rate
                          from res_currency_rate where currency_id =
                          (select id from res_currency where name = %s)
                          and name <= (select date_done from stock_picking as sp
                                        inner join account_invoice as ai
                                        on sp.name = ai.origin
                                        where internal_number = %s and type = %s limit 1)
                                        order by name desc limit 1""",
                                   [objeto_factura['str_moneda'],
                                    objeto_factura['str_numero'],
                                    objeto_factura['str_type']]
                                   )
                        if cr.rowcount:
                            rate = cr.fetchone()
                            rate_sale = rate[0]
                            rate_purchase = rate[1]
                    if objeto['str_tipo_albaran'] == 'incoming' and objeto['strtipodevolucion'] == '': #Compra PO
                        costo_movimiento = objeto_factura['fprecio']
                        precio = 0
                    elif objeto['str_tipo_albaran'] == 'incoming' and objeto['strtipodevolucion'] != '': #NCC
                        costo_movimiento = round(costo_promedio / (1 / rate_sale if rate_sale else 1), ndigits=2)
                        precio = objeto_factura['fprecio']
                    elif objeto['str_tipo_albaran'] == 'outgoing' and objeto['strtipodevolucion'] == '': #Venta SO
                        costo_movimiento = round(costo_promedio / (1 / rate_sale if rate_sale else 1), ndigits=2)
                        precio = objeto_factura['fprecio']
                    elif objeto['str_tipo_albaran'] == 'outgoing' and objeto['strtipodevolucion'] != '': #NCP
                        costo_movimiento = objeto_factura['fprecio']
                        precio = objeto_factura['fprecio']  #Pendiente este valor, precio debe ser el costo unitario de la PO de donde viene
                    moneda = objeto_factura['str_moneda'] if 'str_moneda' in objeto_factura else ''
                    partner_id = objeto_factura['i_proveedor'] if 'i_proveedor' in objeto_factura else ''
                    name_partner = objeto_factura['str_proveedor'] if 'i_proveedor' in objeto_factura else ''
                #No existe documento entonces tomaremos los valores originales, solo condicionando que exista
                else:
                    if (objeto['str_documento_origen'][:5].replace("\\", '-') == 'WH-IN'):
                        costo_movimiento = 0
                    else:
                        precio = 0

                # Nuevo código insertado: Documentos 'incoming' 'internal' y
                # 'outgoing' creados en Movimientos de inventario.
                if not objeto_factura:
                    document_inventory_id = self.pool.get(
                        'document.inventory').search(
                        cr, uid, [
                            ('number', '=', objeto['str_documento_origen'])
                        ], context=context
                    )
                    document_inventory = self.pool.get(
                        'document.inventory').browse(
                        cr, uid, document_inventory_id,
                        context=context
                    )
                    if document_inventory:
                        tipo_documento = document_inventory.type_document.name
                        # Consulta para traer el precio de la linea creada
                        cr.execute(""" SELECT dil.cost FROM document_inventory di
                        inner join document_inventory_line dil
                        on di.id=dil.document_id
                        WHERE dil.product_id=%s and di.id=%s """,
                        (objeto['iidproducto'], document_inventory.id,))

                        if cr.rowcount:
                            c_movimiento = cr.fetchone()[0]
                        else:
                            costo_prueba = 0.0

                        costo_movimiento = c_movimiento
                        precio = costo_movimiento
                    if objeto['strfolio']:
                        cr.execute("""
                            Select aj.id from account_journal as aj
                            inner join document_inventory_type as dit on dit.journal_id = aj.id
                            inner join document_inventory as di on di.type_document = dit.id
                            inner join stock_picking as sp on sp.document_inventory_id = di.id
                            inner join _history_stock as _hs on _hs.picking_id = sp.id
                            where _hs.picking_origin = %s limit 1
                        """, [objeto['strfolio']])
                        if cr.rowcount:
                            journal_id = cr.fetchone()[0]
                            journal = self.pool.get(
                                'account.journal').browse(
                                cr, uid, journal_id, context=context
                            )
                            moneda = (journal.currency.name or
                                      journal.company_id.currency_id.name)
            almacen = lista_almacenes[objeto['iidalmacen']]['nombre'] + str("(") + str(
                    type_dic.get(lista_almacenes[objeto['iidalmacen']]['tipo'], 'N/D')) + str(")")
            if not line_list:
                objeto.update({'iexistenciaanterior': 0,
                               'iexistenciaposterior': objeto['icantidad']})
                line = {'producto': objeto['strproducto'],
                        'extistencia': objeto['icantidad'],
                        'almacen': almacen
                        }
                line_list.append(line)
            else:
                iftoExist = False
                for line in line_list:
                    if objeto['strproducto'] == line['producto'] and almacen == line['almacen']:
                        existenciaanterior = line['extistencia']
                        if objeto['str_tipo_albaran'] in ['outgoing']:
                            existenciaposterior = line['extistencia'] - abs(objeto['icantidad'])
                        elif objeto['str_tipo_albaran'] in ['incoming']:
                            existenciaposterior = line['extistencia'] + abs(objeto['icantidad'])
                        elif objeto['str_tipo_albaran'] in ['internal']:
                            existenciaposterior = line['extistencia'] + (objeto['icantidad'])

                        objeto.update({'iexistenciaanterior': existenciaanterior,
                                       'iexistenciaposterior': existenciaposterior})
                        line.update({'extistencia': existenciaposterior
                                     })
                        iftoExist = True
                        break
                if not iftoExist:
                    objeto.update({'iexistenciaanterior': 0,
                                   'iexistenciaposterior': objeto['icantidad']})
                    line = {'producto': objeto['strproducto'],
                            'extistencia': objeto['icantidad'],
                            'almacen': almacen
                            }
                    line_list.append(line)

            vals = {
                #'gv_inventario_analitico_id': data.id,
                'clave_articulo': objeto['strsku'],
                'descripcion': objeto['strproducto'],
                'cantidad': objeto['icantidad'],
                'costo_movimiento': costo_movimiento,
                'costo_promedio': costo_promedio,
                'fecha_movimiento': objeto['dtfecha'],
                'no_folio': objeto['strfolio'],
                'tipo_documento': tipo_documento,
                'numero_almacen': lista_almacenes[objeto['iidalmacen']]['nombre'] + str("(") + str(
                    type_dic.get(lista_almacenes[objeto['iidalmacen']]['tipo'], 'N/D')) + str(")"),
                # 'numero_almacen': lista_almacenes[objeto['iidalmacen']]['nombre'],
                'estado': 'FINALIZADO',
                'precio': precio,
                'moneda': moneda,
                'numero_factura': numero_factura if numero_factura else '',
                'existencia_anterior': objeto['iexistenciaanterior'],
                'existencias': objeto['iexistenciaposterior'],
                'numero_proveedor': partner_id,
                'proveedor': name_partner,
                'categoria': objeto['i_categoria_producto'],
            }
            if objeto['dtfecha'] >= data.dtDesde and objeto['dtfecha'] <= data.dtHasta :
                arrIds.append(obj_analitico.create(cr, uid, vals, context=context))

        return arrIds

    # Nombre del modelo
    _name = 'gv.inventario.analitico'
    # Nombre de la tabla
    _table = '_gv_inventario_analitico'
    _rec_name = 'id'
    # Columnas y/o campos Tree & Form
    _order = ' fecha_movimiento asc'

    _columns = {

        'date_from': fields.datetime('DE: ', required=False),
        'date_to': fields.datetime('Final: ', required=False),
        'almacen': fields.char('ALMACEN', size=254),
        'clave_articulo': fields.char('CLAVE ARTICULO', size=254),
        'descripcion': fields.char('DESCRIPCION', size=254),
        'cantidad': fields.char('CANTIDAD', size=254),
        'existencia_anterior': fields.integer('EXISTECIA ANTERIOR'),
        'costo_movimiento': fields.float('COSTO UNITARIO MOVIENTO'),
        'costo_promedio': fields.float('COSTO PROMEDIO'),
        'fecha_movimiento': fields.datetime('FECHA MOVIMIENTO'),
        'no_folio': fields.char('NO.FOLIO', size=254),
        'tipo_documento': fields.char('TIPO DE DOCUMENTO', size=254),
        'numero_almacen': fields.char('NO.ALMACEN', size=254),
        'estado': fields.char('STATUS', size=254),
        'precio': fields.float('PRECIO'),
        'moneda': fields.char('MONEDA'),
        'existencias': fields.float('EXISTENCIAS', size=254),
        'numero_factura': fields.char('NO.FACTURA', size=254),
        'numero_proveedor': fields.char('NO.PROVEEDOR', size=254),
        'proveedor': fields.char('PROVEEDOR', size=254),
        'categoria': fields.many2one('product.category', 'CATEGORIA', ),
        'select_by': fields.selection([
            ('consigna', 'Incluir articulos en consignacion'),
            ('sinconsigna', 'Sin Incluir articulos en consignacion'),
            ('soconsigna', 'Solo articulos en consignacion'),
        ], 'Consginacion:', required=False, ),
        'product_id': fields.many2one('product.product', 'Inicial'),
        'id_product': fields.many2one('product.product', 'Final'),
        'partner_id': fields.many2one('res.partner', 'Inicial'),
        'id_partner': fields.many2one('res.partner', 'Final'),
        'gv_inventario_analitico_id': fields.many2one('gv.inventario.analitico', 'inventarioid'),
        'gv_inventario_analitico_ids': fields.one2many('gv.inventario.analitico', 'gv_inventario_analitico_id',
                                                       'Historial'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Inicial'),
        'warehouse_dest_id': fields.many2one('stock.warehouse', 'Final'),

        'warehouse_aux_id': fields.many2one('stock.warehouse', 'Inicial', domain="[('gvconsigacion', '=', True)]"),
        'warehouse_aux_dest_id': fields.many2one('stock.warehouse', 'Final', domain="[('gvconsigacion', '=', True)]"),
        'active_all': fields.boolean('Todos:'),
        'delete_records': fields.boolean('Elimanar registros de reportes')

    }



class inventario_analitico_filtros(osv.osv):
    _name = "r.filtros.inventario.analitico"

    _columns = {

        'dtDesde': fields.datetime('Desde: ', required=False),
        'dtHasta': fields.datetime('Hasta: ', required=False),
        #'select_by': fields.selection([
        #    ('consigna', 'Incluir artículos en consignación'),
        #    ('sinconsigna', 'Sin incluir artículos en consignación'),
        #    ('soconsigna', 'Solo artículos en consignación'),
        #], 'Consginación:', required=False, ),
        'iIdProductoInicial': fields.many2one('product.product', 'Inicial'),
        'iIdProductoFinal': fields.many2one('product.product', 'Final'),

        'iAlmacenInicial': fields.many2one('stock.warehouse', 'Inicial'),
        'iAlmacenFinal': fields.many2one('stock.warehouse', 'Final'),
    }

    def ValidarFiltros(self, cr, uid, data, context=None):
        if (not data.iAlmacenInicial and data.iAlmacenFinal) or (not data.iIdProductoInicial and data.iIdProductoFinal):
            raise osv.except_osv(_('Alerta!'), _("Verifique que sus filtros sean correctos"))
        if(data.dtDesde > data.dtHasta):
            raise osv.except_osv(_('Alerta!'), _("La fecha inicial no puede ser mayor a la fecha final"))

    def Pantalla(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)
        self.ValidarFiltros(cr, uid, data, context=None)
        list_ids = self.pool.get('gv.inventario.analitico')._get_analitico_por_consulta(cr, uid, data, context=None)
        #print list_ids
        return {
            'name': 'Title',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'gv.inventario.analitico',
            'target': 'current',
            'domain': [('id', 'in', list_ids)],
            # 'target': 'new',
        }

    def Archivo(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)
        self.ValidarFiltros(cr, uid, data, context=None)
        list_ids = self.pool.get('gv.inventario.analitico')._get_analitico_por_consulta(cr, uid, data, context=None)
        list_ids = self.pool.get('gv.inventario.analitico').search(cr, uid,[('id','in',list_ids)], context=None, order='fecha_movimiento asc')
        id_archivo = self.pool.get('gv.inventario.analitico').action_wizard(cr, uid, list_ids, context=None)

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id_archivo),
            'target': 'self',
        }

    def onchage_validate_id_product(self, cr, uid, ids, id_product, product_id):
        """
          Valida que la fecha que elijan sea menor a la fecha actual..
        """
        try:
            object_product = self.pool.get('product.product')
            cr.execute("select default_code from product_product where id=%s", (product_id,))
            product_one = cr.fetchone()[0]
            cr.execute("select default_code from product_product where id=%s", (id_product,))
            product_2 = cr.fetchone()[0]
            if product_2 < product_one:
                return {'value': {'product_id': id_product, 'id_product': product_id},}
        except:
            return {'value': {}}

    def _get_min_warehouse(self, cr, uid, context=None):
        try:
            cr.execute("""select min(id) from stock_warehouse""")
            first = cr.fetchone()[0]
            return first
        except Exception, ex:
            return False

    _defaults = {
        # 'warehouse_id':_get_min_warehouse,
    }
