# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Javier Barajas(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
import logging
_logger = logging.getLogger(__name__)

class inventario_existencia(osv.osv):
    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    def action_wizard(self, cr, uid, ids, context=None):
        """Funcion que exporta a exel"""
        columna = "\"ALMACEN\",\"CLAVE ARTICULO\",\"DESCRIPCION\",\"EXISTENCIA\",\"DISPONIBLE\",\"APARTADO\",\"COSTO PROMEDIO UNITARIO\",\"IMPORTE\"\n"
        nombre_arch = "inventario_existencias"
        lista = self.browse(cr, uid, ids)
        for fac in lista:
            columna += "\"" + unicode(fac.almacen).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.clave_articulo).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.descripcion.replace('"', "''")).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.existencia).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.disponible).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.apartado).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.costo_promedio_unitario).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.importe).encode("utf-8") + "\"," \
                       + "\n"

        # --------- termina contenido archivo
        buf = cStringIO.StringIO()
        buf.write(columna)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        filename_psn = nombre_arch + ".csv"
        # --------end------------------------
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': filename_psn,
            'type_file': 'csv',
            'file': out,
        }, context=None)

    def obtenerInfoExistencias(self, cr, data):
        #strCondicionConsignacion = ''  # Todos los articulos
        #if data.select_by == 'sinconsigna':  # Articulos sin consignacion
        #    strCondicionConsignacion = "AND sw.type != 'pre'"
        #elif data.select_by == 'soconsigna':  # Articulos que tengan consignacion solamente
        #    strCondicionConsignacion = "AND sw.type = 'pre'"

        strCondicionProductos = ''  # Todos los productos
        if data.iIdProductoInicial.default_code and data.iIdProductoFinal.default_code:
            strCondicionProductos = """AND pp.default_code>='{}' AND pp.default_code<='{}'""".format(
                data.iIdProductoInicial.default_code, data.iIdProductoFinal.default_code)
        elif data.iIdProductoInicial.default_code:
            strCondicionProductos = """AND pp.default_code ='{}'""".format(data.iIdProductoInicial.default_code)

        strCondicionAlmacenes = ''  # Todos los Almacenes
        if data.iAlmacenInicial.id and data.iAlmacenFinal.id:
            strCondicionAlmacenes = """AND sw.id >= {} AND sw.id <= {}""".format(
                data.iAlmacenInicial.id, data.iAlmacenFinal.id)
        elif data.iAlmacenInicial.id:
            strCondicionAlmacenes = """AND sw.id = {}""".format(data.iAlmacenInicial.id)

        # Obtenemos la Informacion de los productos con existencia
        cr.execute("""SELECT  sq.product_id AS iIdProducto,
                                            pp.default_code AS strCveProd,
                                            pp.name_template AS strNombreProd,
                                            sw.name AS strAlmacen,
                                            sw.id AS iIdAlmacen,
                                            swt.code AS strTipoAlmacen,
                                            count(*) AS iCantidad,
                                            sum(sq.qty) AS fSumatoria,
                                            tdis.fDisponible,
                                            tapar.fApartado
                                    FROM 	stock_quant AS sq
                                            INNER JOIN stock_location AS sl
                                            ON sq.location_id=sl.id
                                            INNER JOIN stock_warehouse AS sw
                                            INNER JOIN stock_warehouse_type swt ON sw.stock_warehouse_type_id = swt.id
                                            ON sl.id= sw.lot_stock_id
                                            INNER JOIN product_product AS pp
                                            ON sq.product_id = pp.id
                                            LEFT JOIN (	SELECT 	sq.product_id AS iProducto,
                                                                sum(sq.qty) AS fDisponible,
                                                                sw.id AS iIdAlmacen
                                                        FROM 	stock_quant AS sq
                                                                INNER JOIN stock_location AS sl
                                                                ON sq.location_id=sl.id
                                                                INNER JOIN stock_warehouse AS sw
                                                                ON sl.id= sw.lot_stock_id
                                                        WHERE 	sl.usage='internal'
                                                                and sq.reservation_id is null
                                                                {}
                                                        GROUP BY sq.product_id, sw.id, sw.name	) tdis
                                            ON tdis.iIdAlmacen = sw.id
                                            AND tdis.iProducto = sq.product_id
                                            LEFT JOIN (	SELECT 	sq.product_id AS iProducto,
                                                                sum(sq.qty) AS fApartado,
                                                                sw.id AS iIdAlmacen
                                                        FROM 	stock_quant AS sq
                                                                INNER JOIN stock_location AS sl
                                                                ON sq.location_id=sl.id
                                                                INNER JOIN stock_warehouse AS sw
                                                                ON sl.id= sw.lot_stock_id
                                                        WHERE 	sl.usage='internal'
                                                                and sq.reservation_id is not null
                                                                {}
                                                        GROUP BY sq.product_id, sw.id, sw.name	) tapar
                                            ON tapar.iIdAlmacen = sw.id
                                            AND tapar.iProducto = sq.product_id
                                    WHERE 	sl.usage='internal'
                                            AND pp.active=True
                                            {}
                                            {}
                                            {}
                                    GROUP BY sq.product_id, sw.id, sw.name, swt.code, tdis.fDisponible, tapar.fApartado, pp.default_code, pp.name_template
                                    """.format("", #strCondicionConsignacion,
                                               "", #strCondicionConsignacion,
                                               strCondicionProductos,
                                               strCondicionAlmacenes,
                                               "", #strCondicionConsignacion
                                               )
                    )
        lista_productos = {}
        for (iIdProducto, strCveProd, strNombreProd, strAlmacen, iIdAlmacen, strTipoAlmacen, iCantidad, fSumatoria, fDisponible, fApartado) in cr.fetchall():
            #lista_productos.update({iIdProducto: {iIdAlmacen: {'iCantidad': iCantidad,
            #                                                   'iSumatoria': fSumatoria,
            #                                                   'strAlmacen': strAlmacen,
            #                                                   'strTipoAlmacen': strTipoAlmacen,
            #                                                   'iDisponible': fDisponible,
            #                                                   'iApartado': fApartado,
            #                                                   'strCveProd': strCveProd,
            #                                                   'strNombreProd': strNombreProd}}})
            if not data.mostrarglobal:
                if not lista_productos:
                    lista_productos.update(
                        {
                            iIdProducto: {
                                iIdAlmacen: {
                                    'iCantidad': iCantidad,
                                    'iSumatoria': fSumatoria,
                                    'strAlmacen': strAlmacen,
                                    'strTipoAlmacen': strTipoAlmacen,
                                    'iDisponible': fDisponible,
                                    'iApartado': fApartado,
                                    'strCveProd': strCveProd,
                                    'strNombreProd': strNombreProd
                                }
                            }
                        }
                    )
                else:
                    if iIdProducto in lista_productos:
                        lista_productos[iIdProducto].update({
                            iIdAlmacen: {
                                'iCantidad': iCantidad,
                                'iSumatoria': fSumatoria,
                                'strAlmacen': strAlmacen,
                                'strTipoAlmacen': strTipoAlmacen,
                                'iDisponible': fDisponible,
                                'iApartado': fApartado,
                                'strCveProd': strCveProd,
                                'strNombreProd': strNombreProd
                                }
                            }
                        )
                    else:
                        lista_productos.update(
                            {
                                iIdProducto: {
                                    iIdAlmacen: {
                                        'iCantidad': iCantidad,
                                        'iSumatoria': fSumatoria,
                                        'strAlmacen': strAlmacen,
                                        'strTipoAlmacen': strTipoAlmacen,
                                        'iDisponible': fDisponible,
                                        'iApartado': fApartado,
                                        'strCveProd': strCveProd,
                                        'strNombreProd': strNombreProd
                                    }
                                }
                            }
                        )
            else:
                if iIdProducto not in lista_productos:
                    lista_productos.update({
                        iIdProducto: {
                            'global': {
                                'iCantidad': iCantidad,
                                'iSumatoria': fSumatoria,
                                'strAlmacen': "Global",
                                'strTipoAlmacen': False,
                                'iDisponible': fDisponible,
                                'iApartado': fApartado,
                                'strCveProd': strCveProd,
                                'strNombreProd': strNombreProd
                            }
                        }
                    })
                else:
                    cantidad = (iCantidad or 0) + (lista_productos[iIdProducto]['global']['iCantidad'] or 0)
                    sumatoria = (fSumatoria or 0.0) + (lista_productos[iIdProducto]['global']['iSumatoria'] or 0.0)
                    disponible = (fDisponible or 0.0) + (lista_productos[iIdProducto]['global']['iDisponible'] or 0.0)
                    apartado = (fApartado or 0.0) + (lista_productos[iIdProducto]['global']['iApartado'] or 0.0)
                    lista_productos.update(
                        {
                            iIdProducto: {
                                'global': {
                                    'iCantidad': cantidad,
                                    'iSumatoria': sumatoria,
                                    'strAlmacen': "Global",
                                    'strTipoAlmacen': False,
                                    'iDisponible': disponible,
                                    'iApartado': apartado,
                                    'strCveProd': strCveProd,
                                    'strNombreProd': strNombreProd
                                }
                            }
                        }
                    )
        return lista_productos

    def obtenerCostos(self, cr):
        # Obtenemos la lista de precios
        cr.execute("""SELECT pp.id AS iIdProducto,
                          COALESCE((SELECT round(CAST(pph.cost AS NUMERIC),2) from product_price_history pph
                          WHERE pp.product_tmpl_id=pph.product_template_id AND pph.cost > 0
                          ORDER BY pph.datetime DESC LIMIT 1),0) fCosto
                        FROM product_product AS pp
                          ORDER BY pp.id DESC
                    """)
        lista_costos_productos = {}
        for (iIdProducto, fCosto) in cr.fetchall():
            lista_costos_productos.update({iIdProducto: fCosto})
        return lista_costos_productos

    def _get_existencia(self, cr, uid, data, context=None):
        """muchos productos muchos almacenes"""
        obj_inventario = self.pool.get('r.inventario.existencias.datos')
        lista_productos = self.obtenerInfoExistencias(cr, data)
        lista_costos_productos = self.obtenerCostos(cr)

        cr.execute("DELETE FROM r_inventario_existencias_datos WHERE create_uid = {}".format(uid))

        type_dic = {
            'fac': 'Facturacion',
            'pre': 'Prestamo',
            'sop': 'Soporte',
            'tra': 'Transito',
            'scrp': 'Scrap'
        }

        arrIds = []
        for producto in lista_productos:
            for almacen in lista_productos[producto]:
                temp = lista_productos[producto][almacen]
                total = temp['iSumatoria']
                strAlmacenNombre = temp['strAlmacen']
                disponible = temp['iDisponible']
                apartado = temp['iApartado']
                tipoAlmacen = type_dic.get(temp['strTipoAlmacen'], 'N/D')
                strCveProd = temp['strCveProd']
                strNombreProd = temp['strNombreProd']
                costo = lista_costos_productos.get(producto, 0)

                vals = {
                    #'gv_inventario_existencias_id': data.id,
                    'almacen': strAlmacenNombre + str('(') + tipoAlmacen + str(')'),
                    'clave_articulo': strCveProd,
                    'descripcion': strNombreProd,
                    'disponible': disponible,
                    'apartado': apartado,
                    'existencia': total,
                    'costo_promedio_unitario': costo,
                    'importe': total * costo}
                if total > 0:
                    arrIds.append(obj_inventario.create(cr, uid, vals, context=context))
        return arrIds

    # Nombre del modelo
    _name = 'r.inventario.existencias.datos'
    # Nombre de la tabla
    _table = 'r_inventario_existencias_datos'
    #_rec_name = 'clave_articulo'
    # Columnas y/o campos Tree & Form
    _order = 'clave_articulo asc'

    _columns = {
            'almacen':fields.char('ALMACEN', size=254),
            'clave_articulo': fields.char('CLAVE ARTICULO'),
            'descripcion': fields.char('DESCRIPCION', size=254),
            'existencia': fields.float('EXISTENCIA'),
            'disponible': fields.float('Disponible'),
            'apartado': fields.float('Apartado'),
            'costo_promedio_unitario': fields.float('COSTO PROMEDIO UNITARIO', digits=(12, 2),),
            'importe': fields.float('IMPORTE', digits=(12, 2),),
            #'select_by': fields.selection([
            #        ('consigna', 'Incluir articulos en consignacion'),
            #        ('sinconsigna', 'Sin Incluir articulos en consignacion'),
            #        ('soconsigna', 'Solo articulos en consignacion'),
            #    ], 'Consginacion:', required=False, )
    }


class inventario_existencia_filtros(osv.osv):
    _name = "r.filtros.inventario.existencias"

    _columns = {
        #'select_by': fields.selection([
        #    ('consigna', 'Incluir artículos en consignación'),
        #    ('sinconsigna', 'Sin incluir artículos en consignación'),
        #    ('soconsigna', 'Solo artículos en consignación'),
        #], 'Consginación:', required=False, ),
        'mostrarglobal': fields.boolean(
            string="Existencias globales",
            required=False,
            help="Al activar esta casilla los productos se "
                 "mostraran en global"
        ),
        'iIdProductoInicial': fields.many2one('product.product', 'Inicial'),
        'iIdProductoFinal': fields.many2one('product.product', 'Final'),

        'iAlmacenInicial': fields.many2one('stock.warehouse', 'Inicial'),
        'iAlmacenFinal': fields.many2one('stock.warehouse', 'Final'),
    }

    def ValidarFiltros(self, cr, uid, data, context=None):
        if (not data.iAlmacenInicial and data.iAlmacenFinal) or (not data.iIdProductoInicial and data.iIdProductoFinal):
            raise osv.except_osv(_('Alerta!'), _("Verifique que sus filtros sean correctos"))

    def Pantalla(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)
        self.ValidarFiltros(cr, uid, data, context=None)
        list_ids = self.pool.get('r.inventario.existencias.datos')._get_existencia(cr, uid, data, context=None)
        #print list_ids
        return {
            'name': 'Title',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.inventario.existencias.datos',
            'target': 'current',
            'domain': [('id', 'in', list_ids)],
            # 'target': 'new',
        }

    def Archivo(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)
        self.ValidarFiltros(cr, uid, data, context=None)
        list_ids = self.pool.get('r.inventario.existencias.datos')._get_existencia(cr, uid, data, context=None)
        list_ids = self.pool.get('r.inventario.existencias.datos').search(cr, uid,[('id','in',list_ids)], context=None, order='clave_articulo asc')
        id_archivo = self.pool.get('r.inventario.existencias.datos').action_wizard(cr, uid, list_ids, context=None)

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id_archivo),
            'target': 'self',
        }

    def onchage_validate_id_product(self, cr, uid, ids, id_product, product_id):
        try:
            object_product = self.pool.get('product.product')
            cr.execute("select default_code from product_product where id=%s", (product_id,))
            product_one = cr.fetchone()[0]
            cr.execute("select default_code from product_product where id=%s", (id_product,))
            product_2 = cr.fetchone()[0]
            if product_2 < product_one:
                return {'value': {'product_id': id_product, 'id_product': product_id},}
        except:
            return {'value': {}}

    def _get_min_warehouse(self, cr, uid, context=None):
        try:
            cr.execute("""select min(id) from stock_warehouse""")
            first = cr.fetchone()[0]
            return first
        except Exception, ex:
            return False

    def onchage_mostrar_producto_global(self, cr, uid, ids, mostrarglobal):
        value = {}
        if mostrarglobal:
            value = {'value': {
                'iAlmacenInicial': False,
                'iAlmacenFinal': False,
                }
             }
        return value

    _defaults = {
        'mostrarglobal': False,
        # 'warehouse_id':_get_min_warehouse,
    }
