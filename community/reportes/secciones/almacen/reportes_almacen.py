# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Israel Cabrera Juarez(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
#from reportes.secciones.reportes_almacen import existencia_historico as obj
#objeto_historico = obj()

################################################################################################################################################################################################
#                                                                     series_inventario                                                                                                        #
################################################################################################################################################################################################
class series_inventario( osv.osv ):
  
 def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Union\",\"Folio\",\"Fecha\",\"Clave articulo\",\"Descripcion\",\"Sucursal\",\"Lote\",\"Fecha creada\",\"Pedimento\",\n"
    nombre_arch ="Series en inventario"
    lista = []
    state = ""
    for this in self.browse(cr, uid, ids):
        for fac in this.stock_move_ids:
          columna += "\""+unicode(fac.union).encode("latin-1")+"\","+"\""+unicode(fac.folio).encode("latin-1")+"\","+"\""+unicode(fac.fecha).encode("latin-1")+"\","+"\""+unicode(fac.clave).encode("latin-1")+"\","+"\""+unicode(fac.descripcion).encode("latin-1")+"\","\
           +"\""+unicode(fac.sucursal).encode("latin-1")+"\","+"\""+unicode(fac.lote).encode("latin-1")+"\","+"\""+unicode(fac.fecha_creada).encode("latin-1")+"\","+"\""+unicode(fac.pedimento).encode("latin-1")+"\","+"\n"
      
    
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_venta_x_compras_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}
 def _almacen_compras(self, cr, uid,ids, context=None):
      invoice_lines = self.pool.get('stock.move')
      pedimento = ""
      albaran  =""
      for this in self.browse(cr, uid, ids):
        for invoice_id in this.stock_move_ids:
          obj_stock_move = self.pool.get('stock.move').browse(cr, uid,invoice_id.id, context=context)
          origin= obj_stock_move.origin
          if origin:
            id_compra = self.pool.get('purchase.order').search(cr, uid,[('name','=',origin)], context=context)[0]
            obj_purchase = self.pool.get('purchase.order').browse(cr, uid,id_compra, context=context)
            
            #if obj_stock_move.picking_id.pedimento:
                 #pedimento = obj_stock_move.picking_id.pedimento
              
            if obj_stock_move.origin:
              albaran= "ALBARAN"
            invoice_lines.write(cr, uid, invoice_id.id,{
                                                       'union': albaran,
                                                       'folio': obj_stock_move.picking_id.name,
                                                       'fecha': obj_purchase.date_order,
                                                       'clave': obj_stock_move.product_id.default_code,
                                                       'descripcion': obj_stock_move.product_id.name_template,
                                                       'sucursal': obj_stock_move.warehouse_id.name,
                                                       'lote': obj_stock_move.picking_id.lot,
                                                       'fecha_creada': obj_stock_move.picking_id.create_date,
                                                       #'pedimento':pedimento,
                                                       
                                              
                                              },context=context)
            
          if not origin:
            if (obj_stock_move.inventory_id):
              for index in obj_stock_move.inventory_id.line_ids:
                 if obj_stock_move.product_id==index.product_id:
                    lote= index.prod_lot_id.name
                    if (obj_stock_move.inventory_id.location_id.usage=='internal'):
                      ubicacion= obj_stock_move.inventory_id.location_id.id
                      id_warehouse= self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id','=', ubicacion)], context=context)[0]
                      obj_warehouse = self.pool.get('stock.warehouse').browse(cr, uid, id_warehouse, context=context)
                      invoice_lines.write(cr, uid, invoice_id.id,{
                                                                 'union':'',
                                                                 'folio': obj_stock_move.name,
                                                                 'fecha': obj_stock_move.inventory_id.date,
                                                                 'clave': obj_stock_move.product_id.default_code,
                                                                 'descripcion': obj_stock_move.product_id.name_template,
                                                                 'sucursal': obj_warehouse.name,
                                                                 'lote':lote,
                                                                 'fecha_creada':obj_stock_move.picking_id.create_date,
                                                                 #'pedimento':'',
                                                        },context=context)
        pedimento=""
        albaran  =""
      return True
              
 def filtrar(self, cr, uid, ids, context=None):
    for this in self.browse(cr, uid, ids):
      cr.execute("""UPDATE stock_move SET gv_venta_x_compras_id= null""")
      date_from = this.date_from
      date_to = this.date_to
      product_id = this.product_id
      for id in ids:
        if (this.select=='all'):
            cr.execute("""UPDATE stock_move SET gv_venta_x_compras_id=%s WHERE (date>=%s and date<=%s)
                          and substring(origin,1,2)='PO' and state='done' or picking_id is null""",(id,date_from,date_to,))
            
        elif this.select =='product':
            cr.execute("""UPDATE stock_move SET gv_venta_x_compras_id=%s WHERE (date>=%s and date<=%s)
                          and (substring(origin,1,2)='PO' or substring(name,1,2)='FV') and product_id=%s and state='done'""",(this.id,this.date_from,this.date_to,this.product_id.id,))
            
            cr.commit()
        self._almacen_compras(cr, uid, ids, context=None)
   
 # Nombre del modelo
 _name = 'gv.venta.x.compras'
 # Nombre de la tabla
 _table = '_gv_reporte'
 _rec_name = 'id'
 # Columnas y/o campos Tree & Form
 _columns = {
           'select': fields.selection([
                ('all', 'Todos'),
                ('product', 'Artículo'),
                ],'Filtrar por:', required=False),
            'product_id': fields.many2one('product.product', 'Artículo:'),
            'stock_move_ids': fields.one2many('stock.move','gv_venta_x_compras_id'),
            'date_from': fields.datetime('De: ', required=True),
            'date_to': fields.datetime('A: ', required=True),
            
    }
        
 _defaults = {}
  
series_inventario()

##############################################################################################################################################################################################
#                                                           existencia_historico                                                                                                             #
##############################################################################################################################################################################################

class existencia_historico( osv.osv ) :
  
  def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Almacen\",\"Clave articulo\",\"Descripcion\",\"Existencia\",\"Costo promedio unitario\",\"Importe\"\n"
    nombre_arch = "Inventario histórico"
    lista = []
    state = ""
    data = self.browse(cr, uid, ids)
    for this in self.browse(cr, uid, ids):
        for fac in this.gv_inventario_historico_ids:
          columna += "\""+unicode(fac.almacen).encode("latin-1")+"\","+"\""+unicode(fac.clave_articulo).encode("latin-1")+"\","\
          +"\""+unicode(fac.descripcion).encode("latin-1")+"\","+"\""+unicode(fac.existencia).encode("latin-1")+"\","\
          +"\""+unicode(fac.costo_promedio_unitario).encode("latin-1")+"\","+"\""+unicode(fac.importe).encode("latin-1")+"\","+"\n"
        
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_inventario_historico_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    cr.execute("""DELETE FROM _gv_inventario_historico WHERE id!=%s""",(data.id,))
    # --------end-------------------------------------------------------------------#
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}
  
  def _get_filtros(self, cr, uid, ids, context=None):
      """Verifica los tres filtros.
         Incluir articulos en consignacion
         Sin Incluir articulos en consignacion
         Solo articulos en consignacion
      """
      data  = self.browse(cr, uid, ids)
      if data.select_by == 'consigna':
        if data.product_id and data.id_product and data.warehouse_id and data.warehouse_dest_id:
           cr.execute("""SELECT ht.id from _history_stock AS ht INNER JOIN product_product AS pp ON ht.product_id=pp.id
                                    where ht.warehouse_id>=%s
                                    and ht.warehouse_id<=%s
                                    and to_char(ht.create_date,'yyyy-mm-dd')<=%s
                                    and pp.default_code>=%s
                                    and pp.default_code<=%s
                                    """,(data.warehouse_id.id,
                                         data.warehouse_dest_id.id,
                                         data.date_from,
                                         data.product_id.default_code,
                                         data.id_product.default_code,))
           if cr.rowcount:
              lista = cr.fetchall()
              return lista
           else: raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
           
      #sin incluir articulos en consignacion    
      elif data.select_by == 'sinconsigna':
         if data.product_id and data.id_product and data.warehouse_id and data.warehouse_dest_id:
           cr.execute("""SELECT ht.id from _history_stock AS ht INNER JOIN product_product AS pp ON ht.product_id=pp.id
                                    where ht.warehouse_id>=%s
                                    and ht.warehouse_id<=%s
                                    and to_char(ht.create_date,'yyyy-mm-dd')<=%s
                                    and pp.default_code>=%s
                                    and pp.default_code<=%s
                                    and (select count(id_warehouse) from _res_configuracion where id_warehouse=ht.warehouse_id)=0
                                    """,(data.warehouse_id.id,
                                         data.warehouse_dest_id.id,
                                         data.date_from,
                                         data.product_id.default_code,
                                         data.id_product.default_code,))
           if cr.rowcount:
              lista = cr.fetchall()
              return lista 
           else: raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
      #Solo articulos en consignacion
      else:
        if data.product_id and data.id_product and data.warehouse_id and data.warehouse_dest_id:
           cr.execute("""SELECT ht.id from _history_stock AS ht INNER JOIN product_product AS pp ON ht.product_id=pp.id
                                    where ht.warehouse_id>=%s
                                    and ht.warehouse_id<=%s
                                    and to_char(ht.create_date,'yyyy-mm-dd')<=%s
                                    and pp.default_code>=%s
                                    and pp.default_code<=%s
                                    and (select count(id_warehouse) from _res_configuracion where id_warehouse=ht.warehouse_id)>0
                                    """,(data.warehouse_id.id,
                                         data.warehouse_dest_id.id,
                                         data.date_from,
                                         data.product_id.default_code,
                                         data.id_product.default_code,))
           if cr.rowcount:
              lista = cr.fetchall()
              return lista
           else: raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
           
  def _get_historico(self, cr, uid ,ids, context=None):
      """Reporte historico"""
      lista = self._get_filtros(cr, uid, ids, context=None)
      data = self.browse(cr, uid,ids)
      obj_history = self.pool.get('history.stock')
      obj_historico = self.pool.get('gv.inventario.historico')
      for this in lista:
        objeto=obj_history.browse(cr, uid, this, context=context)
        cr.execute("""select cost from product_price_history
                   where create_date<=%s and product_template_id=%s
                   order by create_date desc""",(objeto.create_date,
                                                 objeto.product_id.product_tmpl_id.id))
        
        if cr.rowcount: costo_promedio = cr.fetchone()[0]
        else: costo_promedio = 0.0
        vals = {
                     'date_from':time.strftime('%Y-%m-%d'),
                     'date_to':objeto.create_date,
                     'gv_inventario_historico_id':data.id,
                     'almacen':unicode(objeto.warehouse_id.name).encode("latin-1")+str("(")+str(objeto.warehouse_id.id)+str(")"),
                     'clave_articulo': objeto.product_id.default_code,
                     'descripcion':objeto.product_id.name_template,
                     'existencia':objeto.existencia_posterior,
                     'costo_promedio_unitario':costo_promedio,
                     'importe':objeto.existencia_posterior*costo_promedio,
    
                     }
        obj_historico.create(cr, uid, vals, context=context)
        
  def filtrar(self, cr, uid, ids, context=None):
    call = 0
    """Reporte historico"""
    data =self.browse(cr, uid, ids)
    if not data.date_from or not data.select_by or not data.warehouse_id\
    or not data.warehouse_dest_id or not data.product_id or not data.id_product:
       raise osv.except_osv(_('Alerta!'), _("Verifique que sus datos sean correctos"))
    #if data.gv_inventario_historico_ids:
       #cr.execute("""DELETE FROM _gv_inventario_historico WHERE gv_inventario_historico_id=%s""",(data.id,))
       #cr.commit()
    self._get_historico(cr, uid, ids, context=None)
    return True
  
  def onchage_date_from(self, cr, uid, ids, date_from) :
      """
      Valida que la fecha no sea la actual ni mayor 
      """
      # # try:
      return {'value' : {},
            'warning': {'title': 'Alerta',
                    'message': str(time.strftime(date_from))+" : "+str(datetime.today())}}
      return {'value' : {},
        'warning': {'title': 'Alerta',
                    'message': str(time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))+" : "+str(datetime.today()-relativedelta(days=1))},}
      # except:
      #   return {'value' : {}}
  
  #funciones onchange
  @api.onchange('warehouse_aux_id')
  def _onchange_warehouse_id_aux(self):
        self.warehouse_id = self.warehouse_aux_id
        
  @api.onchange('warehouse_aux_dest_id')
  def _onchange_warehouse_dest_aux(self):
        self.warehouse_dest_id = self.warehouse_aux_dest_id
  
  def onchage_validate_date(self, cr, uid, ids, date_from) :
      """
        Valida que la fecha sea menor a la fecha actual..
      """
      try:
        if date_from<datetime.now().strftime('%Y-%m-%d'):
          return {'value' : {},}
        else:return {'value' : {'date_from':0},
                     'warning':{'title':'Alerta','message':'Favor de elegir una fecha menor a la fecha actual'}}
      except:
         return {'value' : {}}
        
  def onchage_validate_id_product(self, cr, uid, ids, id_product, product_id) :
      """
        Valida que se elija un producto
      """
      try:
        object_product = self.pool.get('product.product')
        cr.execute("select default_code from product_product where id=%s", (product_id,))
        product_one = cr.fetchone()[0]
        cr.execute("select default_code from product_product where id=%s", (id_product,))
        product_2 = cr.fetchone()[0]
        if product_2<product_one:
          return {'value' : {'product_id':id_product,'id_product':product_id},}
      except:
          return {'value' : {}}
  
  def onchage_active_all(self, cr, uid, ids, active_all) :
      
      try:
        if active_all == 'all':
          cr.execute(""" SELECT min(default_code) FROM product_product """)
          first = cr.fetchone()[0]
          min_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',first)])[0]
          cr.execute(""" SELECT max(default_code) FROM product_product """)
          maxx = cr.fetchone()[0]
          max_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',maxx)])[0]
          return {'value' : {'product_id':min_id, 'id_product':max_id},}
        else:
         return {'value' : {'product_id':0, 'id_product':0},}
      except:
        return {'value' : {}}
      
  #Nombre del modelo
  _name = 'gv.inventario.historico'
  #Nombre de la tabla
  _table = '_gv_inventario_historico'
  _rec_name = 'id'
  #Columnas y/o campos Tree & Form
 
  _columns = {
    'date_from': fields.date('DE: ', required=True),
    'date_to':fields.datetime('Fecha'),
    'almacen':fields.char('Almacén', size=254),
    'clave_articulo': fields.char('Clave artículo'),
    'descripcion': fields.char('Descripción', size=254),
    'existencia': fields.float('Existencia'),
    'costo_promedio_unitario': fields.float('Costo promedio unitario',digits_compute= dp.get_precision('Product Price')),
    'importe': fields.float('Importe',digits_compute= dp.get_precision('Product Price')),
    'select_by': fields.selection([
            ('consigna', 'Incluir artículos en consignación'),
            ('sinconsigna', 'Sin incluir artículos en consignación'),
            ('soconsigna', 'Solo artículos en consignación'),
            ],'Consginación:', required=False,),
    'product_id':fields.many2one('product.product', 'Inicial:'),
    'id_product':fields.many2one('product.product','Final:'),
    'partner_id':fields.many2one('res.partner','Inicial'),
    'id_partner':fields.many2one('res.partner','Final'),
    'gv_inventario_historico_ids':fields.one2many('gv.inventario.historico','gv_inventario_historico_id','Historial'),
    'gv_inventario_historico_id':fields.many2one('gv.inventario.historico','inventarioid'),
    
    'warehouse_id': fields.many2one('stock.warehouse','Inicial:'),
    'warehouse_dest_id': fields.many2one('stock.warehouse','Final:'),
    
    'warehouse_aux_id': fields.many2one('stock.warehouse','Inicial:', domain="[('type', '=', 'pre')]"),
    'warehouse_aux_dest_id': fields.many2one('stock.warehouse','Final:', domain="[('type', '=', 'pre')]"),
    #'active_all':fields.boolean('Todos:'),
    'active_all':fields.selection([
            ('all', 'Todos'),
            ('product', 'Artículo'),
            ],'Filtrar por:', required=False,),
    'delete_records':fields.boolean('Elimanar registros de reportes'),
  }
  
  def _get_min_product(self, cr, uid, context=None):
        try:
            cr.execute("""select min(id) from product_product""")
            first = cr.fetchone()[0]
            product_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',first)])[0]
            return product_id
        except Exception, ex:
            return False
         
  def _get_max_product(self, cr, uid, context=None):
        try:
            cr.execute("""select max(id) from product_product""")
            ultimate = cr.fetchone()[0]
            product_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',ultimate)])[0]
            return product_id
        except Exception, ex:
          return False
         
  @api.onchange('select_by')
  def _onchange_select_by(self):
   if self.select_by == 'consigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_dest_id = max_id
      
   elif self.select_by == 'sinconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse WHERE type IN ('fac', 'sop', 'tra', 'scrp') """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse WHERE type IN ('fac', 'sop', 'tra', 'scrp') """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_dest_id = max_id
      
   if self.select_by == 'soconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse WHERE type='pre' """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_aux_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse WHERE type='pre' """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_aux_dest_id = max_id
      
  def _get_min_warehouse(self, cr, uid, context=None):
        try:
            cr.execute(""" SELECT min(id) FROM stock_warehouse """)
            first = cr.fetchone()[0]
            return first
        except Exception, ex:
              return False
         
  def _get_max_warehouse(self, cr, uid, context=None):
        try:
            cr.execute(""" SELECT max(id) FROM stock_warehouse """)
            ultimate = cr.fetchone()[0]
            return ultimate
        except Exception, ex:
            return False
         
  _defaults = {
    'warehouse_id':_get_min_warehouse,
    'warehouse_dest_id':_get_max_warehouse,
    'warehouse_aux_id':_get_min_warehouse,
    'warehouse_aux_dest_id':_get_max_warehouse,
    'product_id':_get_min_product,
    'id_product':_get_max_product,
    'select_by': 'consigna' ,
   }
  
existencia_historico()

################################################################################################################################################################################################
#                                                              existencia_inventario                                                                                                           #
################################################################################################################################################################################################
class existencia_inventario( osv.osv ) :
  
  def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Almacen\",\"Clave articulo\",\"Descripcion\",\"Existencia\",\"Disponible\",\"Apartado\",\"Costo promedio unitario\",\"Importe\"\n"
    nombre_arch = "Inventario existencias"
    lista = []
    state = ""
    for this in self.browse(cr, uid, ids):
        for fac in this.gv_inventario_existencias_ids:
          columna += "\""+unicode(fac.almacen).encode("latin-1")+"\","+"\""+unicode(fac.clave_articulo).encode("latin-1")+"\","\
          +"\""+unicode(fac.descripcion).encode("latin-1")+"\","+"\""+unicode(fac.existencia).encode("latin-1")+"\","\
          +"\""+ unicode(fac.disponible).encode("latin-1")+"\","+"\""+unicode(fac.apartado).encode("latin-1")+"\","\
          +"\""+unicode(fac.costo_promedio_unitario).encode("latin-1")+"\","+"\""+unicode(fac.importe).encode("latin-1")+"\",""\n"
        
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_inventario_existencias_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}

  def funcion_pricipal_filtrar(self,cr, uid, ids, context=None):
    data = self.browse(cr, uid, ids)
    #incluir articulos en consignacion
    data =self.browse(cr, uid, ids)
    if data.gv_inventario_existencias_ids:
       cr.execute("""DELETE FROM _gv_inventario_existencias WHERE gv_inventario_existencias_id=%s""",(data.id,))
       cr.commit()
    if data.select_by == 'soconsigna':
          self.many2many(cr, uid, ids, context=None)
      #
          if(data.product_id and not data.id_product):
           if(data.warehouse_id and data.warehouse_dest_id):
              self.many2many(cr, uid, ids, context=None)
    elif(data.select_by == 'noconsigna'): 
      if(data.warehouse_id and data.warehouse_dest_id) or (data.warehouse_id and not data.warehouse_dest_id):
          self.just_consignacion(cr, uid, ids, context=None)
    else:
       self.all_menos_consignacion(cr, uid, ids, context=None)
          
  def sin_consigna(self, cr, uid, ids, context=None):
    """un producto en un almacen"""
    raise osv.except_osv(_('Error!'), _('productos que no estan en consignacion'))
    obj_inventario= self.pool.get('gv.inventario.existencias')
    this =self.browse(cr, uid, ids)
    cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                  WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s
              """,(this.product_id.id,this.warehouse_id.id,))
    total = cr.fetchone()[0]
    cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                  WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is null
              """,(this.product_id.id,this.warehouse_id.id,))
    disponible = cr.fetchone()[0]
    
    cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                  WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is not null
              """,(this.product_id.id,this.warehouse_id.id,))    
    apartado = cr.fetchone()[0]
    vals = {
                     'gv_inventario_existencias_id':this.id,
                     'almacen': this.warehouse_id.name,
                     'clave_articulo':this.product_id.default_code,
                     'descripcion':this.product_id.name_template,
                     'disponible':disponible,
                     'apartado':apartado,
                     'existencia':total}
              # raise osv.except_osv(_('Error!'), _(str(swid)))
    obj_inventario.create(cr, uid, vals, context=context)
    
  def many2many(self, cr, uid, ids, context=None):
    """muchos productos muchos almacenes"""
    obj_inventario= self.pool.get('gv.inventario.existencias')
    data =self.browse(cr, uid, ids)
    lista = []
    if data.product_id and data.id_product:
      cr.execute("""SELECT id from product_product where default_code>=%s and default_code<=%s and active=True""",(data.product_id.default_code, data.id_product.default_code))
      lista_productos = cr.fetchall()
    elif  data.product_id and not data.id_product:
      cr.execute("""SELECT id from product_product where id=%s""",(data.product_id.id,))
      lista_productos = cr.fetchall()
    elif  data.id_product and not data.product_id:
      cr.execute("""SELECT id from product_product where id=%s""",(data.id_product.id,))
      lista_productos = cr.fetchall()
    if data.warehouse_id and not data.warehouse_dest_id:
      cr.execute("""SELECT id from stock_warehouse where id=%s""",(data.warehouse_id.id,))
      lista_almacenes = cr.fetchall()
    elif (data.warehouse_id and data.warehouse_dest_id):
      cr.execute("""SELECT id from stock_warehouse where id>=%s and id<=%s""",(data.warehouse_id.id, data.warehouse_dest_id.id))
      lista_almacenes = cr.fetchall()
    elif data.warehouse_dest_id and not data.warehouse_id:
      cr.execute("""SELECT id from stock_warehouse where id=%s""",(data.warehouse_dest_id.id,))
      lista_almacenes = cr.fetchall()
    for this in lista_productos:
      prod=self.pool.get('product.product').search(cr, uid,[('id', '=',this)])[0]
      product_obj = self.pool.get('product.product').browse(cr, uid,prod,context=context)
      for dat in lista_almacenes:
        cr.execute("""SELECT count(*),sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s
                  """,(this,dat,))
        for (c,sm) in cr.fetchall() :
          if c >0:total = sm
          else:total=0.0
          
        cr.execute("""SELECT ph.cost FROM product_product AS pp 
                      INNER JOIN  product_template AS pt ON pp.product_tmpl_id=pt.id
                      INNER JOIN product_price_history AS ph ON pt.id=ph.product_template_id 
                      where pp.id=%s order by ph.cost desc
                  """,(this,))
        costo = cr.fetchone()[0]
        self.pool.get('stock.warehouse').search(cr, uid,[('id', '=',dat)])[0]
        warehouse_obj = self.pool.get('stock.warehouse').browse(cr, uid,dat,context=context)
        cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is null
                  """,(this,dat,))
        disponible = cr.fetchone()[0]
        
        cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is not null
                  """,(this,dat,))    
        apartado = cr.fetchone()[0]
        vals = {
                         'gv_inventario_existencias_id':data.id,
                         'almacen': warehouse_obj.name,
                         'clave_articulo':product_obj.default_code,
                         'descripcion':product_obj.name_template,
                         'disponible':disponible,
                         'apartado':apartado,
                         'existencia':total,
                         'costo_promedio_unitario':costo,
                         'importe':total*costo}
        if total>0:
          obj_inventario.create(cr, uid, vals, context=context)
          
  def one2one(self, cr, uid, ids, context=None):
    """un producto en un almacen"""
    obj_inventario= self.pool.get('gv.inventario.existencias')
    this =self.browse(cr, uid, ids)
    cr.execute("""SELECT ph.cost FROM product_product AS pp 
                      INNER JOIN  product_template AS pt ON pp.product_tmpl_id=pt.id
                      INNER JOIN product_price_history AS ph ON pt.id=ph.product_template_id 
                      where pp.id=%s order by ph.cost desc
                  """,(this.product_id.id,))
    costo = cr.fetchone()[0]
    cr.execute("""SELECT count(*),sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                  WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s
              """,(this.product_id.id,this.warehouse_id.id,))
    for (c,sm) in cr.fetchall() :
          if c >0:total = sm
          else:total=0.0
    cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                  WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is null
              """,(this.product_id.id,this.warehouse_id.id,))
    disponible = cr.fetchone()[0]
    
    cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                  WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is not null
              """,(this.product_id.id,this.warehouse_id.id,))    
    apartado = cr.fetchone()[0]
    vals = {
                     'gv_inventario_existencias_id':this.id,
                     'almacen': this.warehouse_id.name,
                     'clave_articulo':this.product_id.default_code,
                     'descripcion':this.product_id.name_template,
                     'disponible':disponible,
                     'apartado':apartado,
                     'existencia':total,
                     'costo_promedio_unitario':costo,
                     'importe':total*costo}
              # raise osv.except_osv(_('Error!'), _(str(swid)))
    obj_inventario.create(cr, uid, vals, context=context)
 
  def all_menos_consignacion(self,cr, uid, ids, context=None):
    cr.execute("""select id_warehouse from _res_configuracion""")
    lista_almacen = cr.fetchall()
    this =self.browse(cr, uid, ids)
    for upd in lista_almacen:cr.execute("update stock_warehouse set gvconsigacion=True where id=%s",(upd,))
    """muchos productos muchos almacenes"""
    obj_inventario= self.pool.get('gv.inventario.existencias')
    data =self.browse(cr, uid, ids)
    lista = []
    if data.product_id and data.id_product:
      cr.execute("""SELECT id from product_product where default_code>=%s and default_code<=%s and active=True""",(data.product_id.default_code, data.id_product.default_code))
      lista_productos = cr.fetchall()
    elif  data.product_id and not data.id_product:
      cr.execute("""SELECT id from product_product where id=%s""",(data.product_id.id,))
      lista_productos = cr.fetchall()
    elif  data.id_product and not data.product_id:
      cr.execute("""SELECT id from product_product where id=%s""",(data.id_product.id,))
      lista_productos = cr.fetchall()
    if data.warehouse_id and not data.warehouse_dest_id:
      raise osv.except_osv(_('Error!'), _("Pruba1"))
      cr.execute("""SELECT id from stock_warehouse where id=%s""",(data.warehouse_id.id,))
      lista_almacenes = cr.fetchall()
    elif (data.warehouse_id and data.warehouse_dest_id):
      cr.execute("""SELECT id from stock_warehouse where id>=%s and id<=%s and (gvconsigacion =False or gvconsigacion is null)""",(data.warehouse_id.id, data.warehouse_dest_id.id))
      lista_almacenes = cr.fetchall()
      #raise osv.except_osv(_('Error!'), _(str(lista_almacenes)))
    elif data.warehouse_dest_id and not data.warehouse_id:
      raise osv.except_osv(_('Error!'), _("Pruba3"))
      cr.execute("""SELECT id from stock_warehouse where id=%s""",(data.warehouse_dest_id.id,))
      lista_almacenes = cr.fetchall()
    #raise osv.except_osv(_('Error!'), _(str(lista_productos)+":"+str(lista_almacenes)))
    for this in lista_productos:
      prod=self.pool.get('product.product').search(cr, uid,[('id', '=',this)])[0]
      product_obj = self.pool.get('product.product').browse(cr, uid,prod,context=context)
      for dat in lista_almacenes:
        cr.execute("""SELECT count(*),sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s
                  """,(this,dat,))
        for (c,sm) in cr.fetchall() :
          if c >0:total = sm
          else:total=0.0
        #raise osv.except_osv(_('Error!'), _(str(total)))
        cr.execute("""SELECT ph.cost FROM product_product AS pp 
                      INNER JOIN  product_template AS pt ON pp.product_tmpl_id=pt.id
                      INNER JOIN product_price_history AS ph ON pt.id=ph.product_template_id 
                      where pp.id=%s order by ph.cost desc
                  """,(this,))
        costo = cr.fetchone()[0]
        self.pool.get('stock.warehouse').search(cr, uid,[('id', '=',dat)])[0]
        warehouse_obj = self.pool.get('stock.warehouse').browse(cr, uid,dat,context=context)
        cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is null
                  """,(this,dat,))
        disponible = cr.fetchone()[0]
        
        cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is not null
                  """,(this,dat,))    
        apartado = cr.fetchone()[0]
        vals = {
                         'gv_inventario_existencias_id':data.id,
                         'almacen': warehouse_obj.name,
                         'clave_articulo':product_obj.default_code,
                         'descripcion':product_obj.name_template,
                         'disponible':disponible,
                         'apartado':apartado,
                         'existencia':total,
                         'costo_promedio_unitario':costo,
                         'importe':total*costo}
        if total>0:
          obj_inventario.create(cr, uid, vals, context=context)
                   
  def just_consignacion(self, cr, uid, ids, context=None):
    """un producto en un almacen"""
    obj_inventario= self.pool.get('gv.inventario.existencias')
    data =self.browse(cr, uid, ids)
    ############################################################################################################################################################################################
    if data.product_id and data.id_product:
      cr.execute("""SELECT id from product_product where default_code>=%s and default_code<=%s and active=True""",(data.product_id.default_code, data.id_product.default_code))
      lista_productos = cr.fetchall()
    elif  data.product_id and not data.id_product:
      cr.execute("""SELECT id from product_product where id=%s""",(data.product_id.id,))
      lista_productos = cr.fetchall()
    elif  data.id_product and not data.product_id:
      cr.execute("""SELECT id from product_product where id=%s""",(data.id_product.id,))
      lista_productos = cr.fetchall()
    if data.warehouse_aux_id and not data.warehouse_aux_dest_id:
      cr.execute("""SELECT id_warehouse from _res_configuracion where id_warehouse=%s""",(data.warehouse_aux_id.id,))
      lista_almacenes = cr.fetchall()
    elif (data.warehouse_aux_id and data.warehouse_aux_dest_id):
      cr.execute("""SELECT id_warehouse from _res_configuracion where id_warehouse>=%s and id_warehouse<=%s""",(data.warehouse_aux_id.id, data.warehouse_aux_dest_id.id))
      lista_almacenes = cr.fetchall()
    elif data.warehouse_aux_dest_id and not data.warehouse_aux_id:
      cr.execute("""SELECT id_warehouse from _res_configuracion where id_warehouse=%s""",(data.warehouse_aux_dest_id.id,))
      lista_almacenes = cr.fetchall()
    #########################################################################################################################################################################################
    this =self.browse(cr, uid, ids)
    for dat in lista_almacenes:
      warehouse_id=self.pool.get('stock.warehouse').search(cr, uid,[('id', '=',dat)])[0]
      obj_warehouse = self.pool.get('stock.warehouse').browse(cr, uid,warehouse_id,context=context)
      for j in lista_productos:
        product_id=self.pool.get('product.product').search(cr, uid,[('id', '=', j)])[0]
        obj_product = self.pool.get('product.product').browse(cr, uid,product_id,context=context)
        cr.execute("""SELECT ph.cost FROM product_product AS pp 
                      INNER JOIN  product_template AS pt ON pp.product_tmpl_id=pt.id
                      INNER JOIN product_price_history AS ph ON pt.id=ph.product_template_id 
                      where pp.id=%s order by ph.cost desc
                  """,(j,))
        costo = cr.fetchone()[0]
        cr.execute("""SELECT count(*),sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s
                  """,(j,dat,))
        for (c,sm) in cr.fetchall() :
          if c >0:total = sm
          else:total=0.0
        # total = cr.fetchone()[0]
        cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is null
                  """,(j,dat,))
        disponible = cr.fetchone()[0]
        
        cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                      INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                      WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is not null
                  """,(j,dat,))    
        apartado = cr.fetchone()[0]
        vals = {
                         'gv_inventario_existencias_id':this.id,
                         'almacen': obj_warehouse.name,
                         'clave_articulo':obj_product.default_code,
                         'descripcion':obj_product.name_template,
                         'disponible':disponible,
                         'apartado':apartado,
                         'existencia':total,
                         'costo_promedio_unitario':costo,
                         'importe':total*costo}
        if total> 0:
          obj_inventario.create(cr, uid, vals, context=context)
        
  def all_less_consignacion(self, cr, uid, ids, context=None):
    """un producto en un almacen"""
    obj_inventario= self.pool.get('gv.inventario.existencias')
    cr.execute("""select id_warehouse from _res_configuracion""")
    lista_almacenes = cr.fetchall()
    this =self.browse(cr, uid, ids)
    for upd in lista_almacenes:cr.execute("update stock_warehouse set gvconsigacion=True where id=%s",(upd,))

    cr.execute("""select sw.id, sq.product_id,sum(sq.qty) from stock_quant AS sq INNER JOIN stock_location AS sl ON sq.location_id=sl.id
                    INNER JOIN stock_warehouse AS sw ON sl.id=sw.lot_stock_id  where sw.gvconsigacion=False group by 1,2,sq.qty""")
    lista_productos = cr.fetchall()
    # raise osv.except_osv(_('Error!'), _(str(lista_productos)))
    for (sm,pro,qty) in lista_productos:
      product_id=self.pool.get('product.product').search(cr, uid,[('id', '=', int(pro))])[0]
      obj_product = self.pool.get('product.product').browse(cr, uid,product_id,context=context)
      warehouse_id=self.pool.get('stock.warehouse').search(cr, uid,[('id', '=',sm)])[0]
      obj_warehouse = self.pool.get('stock.warehouse').browse(cr, uid,warehouse_id,context=context)
      cr.execute("""SELECT ph.cost FROM product_product AS pp 
                      INNER JOIN  product_template AS pt ON pp.product_tmpl_id=pt.id
                      INNER JOIN product_price_history AS ph ON pt.id=ph.product_template_id 
                      where pp.id=%s order by ph.cost desc
                  """,(pro,))
      costo = cr.fetchone()[0]
      cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                    INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                    WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s
                """,(pro,sm,))
      total = cr.fetchone()[0]
      cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                    INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                    WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is null
                """,(pro,sm,))
      disponible = cr.fetchone()[0]
      
      cr.execute("""SELECT sum(sq.qty) FROM stock_quant AS sq INNER JOIN stock_location AS sl
                    INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                    WHERE sq.product_id=%s AND sl.usage='internal' and sw.id=%s and sq.reservation_id is not null
                """,(pro,sm,))    
      apartado = cr.fetchone()[0]
      vals = {
                       'gv_inventario_existencias_id':this.id,
                       'almacen': obj_warehouse.name,
                       'clave_articulo':obj_product.default_code,
                       'descripcion':obj_product.name_template,
                       'disponible':disponible,
                       'apartado':apartado,
                       'existencia':total,
                       'costo_promedio_unitario':costo,
                       'importe':total*costo}
                # raise osv.except_osv(_('Error!'), _(str(swid)))
      if total>0:
          obj_inventario.create(cr, uid, vals, context=context)
   
  @api.onchange('warehouse_aux_id')
  def _onchange_warehouse_id_aux(self):
        self.warehouse_id = self.warehouse_aux_id
        
  @api.onchange('warehouse_aux_dest_id')
  def _onchange_warehouse_dest_aux(self):
        self.warehouse_dest_id = self.warehouse_aux_dest_id

  def onchage_warehouse_dest(self, cr, uid, ids, warehouse_dest_id, warehouse_id):
      """
      Valida que la fecha se eliga no sea la actual ni mayor que la actual
      """
      if warehouse_dest_id < warehouse_id:
        return {'value' : {'warehouse_dest_id': 0},
              'warning': {'title': 'Alerta',
                      'message': 'El almacen es menor que el almacen incial'}}
      else:
        return {'value' : {} }
  
  def onchage_validate_id_product(self, cr, uid, ids, id_product, product_id) :
      """
        Valida que la fecha que elijan sea menor a la fecha actual..
      """
      try:
        object_product = self.pool.get('product.product')
        cr.execute("select default_code from product_product where id=%s", (product_id,))
        product_one = cr.fetchone()[0]
        cr.execute("select default_code from product_product where id=%s", (id_product,))
        product_2 = cr.fetchone()[0]
        if product_one>product_2:
          return {'value' : {'product_id':id_product,'id_product':product_id},}
      except:
          return {'value' : {}}
         
  def onchage_active_all(self, cr, uid, ids, active_all) :
      """
       Todos los productos, ordenados por SKU
      """
      try:
        if active_all == 'all':
          cr.execute("""select min(default_code) from product_product""")
          first = cr.fetchone()[0]
          min_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',first)])[0]
          cr.execute("""select max(default_code) from product_product""")
          maxx = cr.fetchone()[0]
          max_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',maxx)])[0]
          return {'value' : {'product_id':min_id, 'id_product':max_id},}
        else:return {'value' : {'product_id':0, 'id_product':0},}
      except:
        return {'value' : {}}
      
  #Nombre del modelo
  _name = 'gv.inventario.existencias'
  #Nombre de la tabla
  _table = '_gv_inventario_existencias'
  _rec_name = 'id'
  #Columnas y/o campos Tree & Form
 
  _columns = {
    'warehouse_aux_id':fields.many2one('stock.warehouse', 'Inicial:', domain="[('gvconsigacion', '=', True)]"),
    'warehouse_aux_dest_id':fields.many2one('stock.warehouse', 'Final:',domain="[('gvconsigacion', '=', True)]"),
    'warehouse_id':fields.many2one('stock.warehouse', 'Inicial:'),
    'warehouse_dest_id':fields.many2one('stock.warehouse', 'Final:'),
    'almacen':fields.char('Almacén', size=254),
    'clave_articulo': fields.char('Clave artículo'),
    'descripcion': fields.char('Descripción', size=254),
    'existencia': fields.float('Existencia'),
    'disponible': fields.float('Disponible'),
    'apartado': fields.float('Apartado'),
    'costo_promedio_unitario': fields.float('Costo promedio unitario',digits_compute= dp.get_precision('Product Price')),
    'importe': fields.float('Importe',digits_compute= dp.get_precision('Product Price')),
    'select_by': fields.selection([
            ('soconsigna', 'Incluir artículos en consignación'),
            ('siconsigna', 'Sin incluir artículos en consignación'),
            ('noconsigna', 'Solo artículos en consignación'),
            ],'Consginación:', required=False,),
    'product_id':fields.many2one('product.product', 'Inicial:'),
    'id_product':fields.many2one('product.product','Final:'),
    'partner_id':fields.many2one('res.partner','Inicial:'),
    'id_partner':fields.many2one('res.partner','Final:'),
    'gv_inventario_existencias_id':fields.many2one('gv.inventario.existencias','inventarioid'),
    'gv_inventario_existencias_ids':fields.one2many('gv.inventario.existencias','gv_inventario_existencias_id','Historial'),
    'delete_records':fields.boolean('Elimanar registros de reportes'),
    'active_all': fields.selection([
            ('all', 'Todos'),
            ('product', 'Artículo'),
            ],'Filtrar por:', required=False,),
  }
    
  def _get_min_product(self, cr, uid, context=None):
        try:
            cr.execute("""select min(default_code) from product_product""")
            first = cr.fetchone()[0]
            product_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',first)])[0]
            return product_id
        except Exception, ex:
            return False
         
  def _get_max_product(self, cr, uid, context=None):
        try:
            cr.execute("""select max(default_code) from product_product""")
            ultimate = cr.fetchone()[0]
            product_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',ultimate)])[0]
            return product_id
        except Exception, ex:
          return False
         
  @api.onchange('select_by')
  def _onchange_select_by(self):
   if self.select_by == 'soconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_dest_id = max_id
      
   elif self.select_by == 'siconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse WHERE type IN ('fac', 'sop', 'tra', 'scrp') """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse WHERE type IN ('fac', 'sop', 'tra', 'scrp') """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_dest_id = max_id
      
   if self.select_by == 'noconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse WHERE type='pre' """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_aux_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse WHERE type='pre' """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_aux_dest_id = max_id
   
  def _get_min_warehouse(self, cr, uid, context=None):
        try:
            cr.execute(""" SELECT min(id) FROM stock_warehouse """)
            first = cr.fetchone()[0]
            return first
        except Exception, ex:
            return False
         
  def _get_max_warehouse(self, cr, uid, context=None):
        try:
            cr.execute(""" SELECT max(id) FROM stock_warehouse """)
            ultimate = cr.fetchone()[0]
            return ultimate
        except Exception, ex:
            return False       
         
  _defaults = {
    'warehouse_id':_get_min_warehouse,
    'warehouse_dest_id':_get_max_warehouse,
    'warehouse_aux_id':_get_min_warehouse,
    'warehouse_aux_dest_id':_get_max_warehouse,
    'product_id':_get_min_product,
    'id_product':_get_max_product,
    'select_by':'soconsigna',
  }

existencia_inventario()

###############################################################################################################################################################################################
#                                                        analitico del inventario                                                                                                             #
##############################################################################################################################################################################################
class analitico_inventario( osv.osv ) :
  
  def action_wizard(self, cr, uid, ids, context=None):
    """Funcion que exporta a exel"""
    columna = "\"Clave articulo\",\"Descripcion\",\"Cantidad\",\"Costo del movimiento\",\"Costo promedio\",\
    \"Fecha movimiento\",\"No.folio\",\"Tipo documento\",\"No.almacen\",\"Estado\",\"Precio\",\"Moneda\",\"Existencias\",\"No.factura\",\"No.proveedor\",\"Proveedor\",\"Categoria\"\n"
    nombre_arch = "Inventario analítico"
    lista = []
    state = ""
    data = self.browse(cr, uid, ids)
    for this in self.browse(cr, uid, ids):
        for fac in this.gv_inventario_analitico_ids:
          cr.execute("""select timezone('utc',fecha_movimiento) from _gv_inventario_analitico where id=%s""",(fac.id,))
          fecha_movimiento = str(cr.fetchone()[0])
          columna +="\""+unicode(fac.clave_articulo).encode("latin-1")+"\","\
          +"\""+unicode(fac.descripcion.replace('"',"''")).encode("latin-1")+"\","\
          +"\""+unicode(fac.cantidad).encode("latin-1")+"\","\
          +"\""+unicode(fac.costo_movimiento).encode("latin-1")+"\","\
          +"\""+unicode(fac.costo_promedio).encode("latin-1")+"\","\
          +"\""+unicode(fecha_movimiento[:19]).encode("latin-1")+"\","\
          +"\""+unicode(fac.no_folio).encode("latin-1")+"\","\
          +"\""+unicode(fac.tipo_documento).encode("latin-1")+"\","\
          +"\""+unicode(fac.numero_almacen).encode("latin-1")+"\","\
          +"\""+unicode(fac.estado).encode("latin-1")+"\","\
          +"\""+unicode(fac.precio).encode("latin-1")+"\","\
          +"\""+unicode(fac.moneda).encode("latin-1")+"\","\
          +"\""+unicode(fac.existencias).encode("latin-1")+"\","\
          +"\""+unicode(fac.numero_factura).encode("latin-1")+"\","\
          +"\""+unicode(fac.numero_proveedor).encode("latin-1")+"\","\
          +"\""+unicode(fac.proveedor).encode("latin-1")+"\","\
          +"\""+unicode(fac.categoria.parent_id.parent_id.name).encode("latin-1")\
              +"/"+unicode(fac.categoria.parent_id.name).encode("latin-1")+"/"+unicode(fac.categoria.name).encode("latin-1")+"\","+"\n"
        
    #--------- termina contenido archivo
    buf = cStringIO.StringIO()
    buf.write(columna)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    filename_psn = nombre_arch+".csv"
    write_vals = {
    'name': filename_psn,
    'data': out,
    'gv_inventario_analitico_id':ids[0]
    }
    id=self.pool.get('wizard.export').create(cr, uid, write_vals, context= context)
    cr.execute("""DELETE FROM _gv_inventario_analitico WHERE id!=%s""",(data.id,))
    # --------end------------------------
    return {
    'type': 'ir.actions.act_window',
    'res_model': 'wizard.export',
    'view_mode': 'form',
    'view_type': 'form',
    'res_id': id,
    'views': [(False, 'form')],
    'target': 'new',
}    

  def _get_filtros(self, cr, uid, ids, context=None):
      """verifica los tres filtros. Incluir articulos en consignacion
      Sin Incluir articulos en consignacion
      Solo articulos en consignacion
      """
      data  = self.browse(cr, uid, ids)
      if data.select_by == 'consigna':
        if data.product_id and data.id_product and data.warehouse_id and data.warehouse_dest_id:
           cr.execute("""SELECT ht.id from _history_stock AS ht INNER JOIN product_product AS pp ON ht.product_id=pp.id
                                    where ht.warehouse_id>=%s
                                    and ht.warehouse_id<=%s
                                    and ht.create_date>=%s
                                    and ht.create_date<=%s
                                    and pp.default_code>=%s
                                    and pp.default_code<=%s
                                    """,(data.warehouse_id.id,
                                         data.warehouse_dest_id.id,
                                         data.date_from,
                                         data.date_to,
                                         data.product_id.default_code,
                                         data.id_product.default_code,))
           if cr.rowcount:
              lista_almacenes = cr.fetchall()
              return lista_almacenes
           else: raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
           
      #sin incluir articulos en consignacion    
      elif data.select_by == 'sinconsigna':
         if data.product_id and data.id_product and data.warehouse_id and data.warehouse_dest_id:
           cr.execute("""SELECT ht.id from _history_stock AS ht INNER JOIN product_product AS pp ON ht.product_id=pp.id
                                    where ht.warehouse_id>=%s
                                    and ht.warehouse_id<=%s
                                    and ht.create_date>=%s
                                    and ht.create_date<=%s
                                    and pp.default_code>=%s
                                    and pp.default_code<=%s
                                    and (select count(id_warehouse) from _res_configuracion where id_warehouse=ht.warehouse_id)=0
                                    """,(data.warehouse_id.id,
                                         data.warehouse_dest_id.id,
                                         data.date_from,
                                         data.date_to,
                                         data.product_id.default_code,
                                         data.id_product.default_code,))
           if cr.rowcount:
              lista_almacenes = cr.fetchall()
              return lista_almacenes
           else: raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
           
      #Solo articulos en consignacion
      else:
        if data.product_id and data.id_product and data.warehouse_id and data.warehouse_dest_id:
           cr.execute("""SELECT ht.id from _history_stock AS ht INNER JOIN product_product AS pp ON ht.product_id=pp.id
                                    where ht.warehouse_id>=%s
                                    and ht.warehouse_id<=%s
                                    and ht.create_date>=%s
                                    and ht.create_date<=%s
                                    and pp.default_code>=%s
                                    and pp.default_code<=%s
                                    and (select count(id_warehouse) from _res_configuracion where id_warehouse=ht.warehouse_id)>0
                                    """,(data.warehouse_id.id,
                                         data.warehouse_dest_id.id,
                                         data.date_from,
                                         data.date_to,
                                         data.product_id.default_code,
                                         data.id_product.default_code,))
           if cr.rowcount:
              lista_almacenes = cr.fetchall()
              return lista_almacenes
           else: raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
 
  def _get_analitico(self, cr, uid ,ids, context=None):
      lista = self._get_filtros(cr, uid, ids, context=None)
      data = self.browse(cr, uid,ids)
      obj_history = self.pool.get('history.stock')
      obj_analitico = self.pool.get('gv.inventario.analitico')
    
      # cr.execute("""SELECT id FROM _history_stock where create_date<=%s""",(data.date_from,))
      # if cr.rowcount:
      for this in lista:
        objeto=obj_history.browse(cr, uid, this, context=context)
        cr.execute("""SELECT cost FROM product_price_history WHERE to_char(create_date,'YYYY-MM-DD HH24:MI:SS')<=%s AND product_template_id=%s ORDER BY create_date DESC""",(objeto.create_date,objeto.product_id.product_tmpl_id.id))
        if cr.rowcount: costo_promedio = cr.fetchone()[0]
        else: costo_promedio = 0.0
        if objeto.picking_origin: tipo_documento = 'ALBARAN'
        else: tipo_documento = 'AJUSTE INVENTARIO'
        
        #albaranes (salida)
        if objeto.picking_id:
          no_folio=objeto.picking_origin
          if objeto.picking_id.picking_type_id.code == 'outgoing':
            for cost_m in objeto.picking_id.move_lines:
              if cost_m.product_id == objeto.product_id:
                costo_movimiento = cost_m.price_unit * cost_m.product_uom_qty
                precio = cost_m.price_unit
                try:
                    factura_id = self.pool.get('account.invoice').search(cr, uid,[('origin','=',objeto.picking_origin)], context=context)[0]
                    objeto_factura = self.pool.get('account.invoice').browse(cr, uid,factura_id,context=context)
                    moneda = objeto_factura.currency_id.name
                    numero_factura = objeto_factura.number or ''
                    partner_id = objeto_factura.partner_id.id
                    name_partner = objeto_factura.partner_id.name
                except:
                  cr.execute("""SELECT id FROM sale_order WHERE name=%s""",(objeto.document_origin,))
                  if cr.rowcount:
                     venta_id = int(cr.fetchone()[0])
                     #venta_id = self.pool.get('sale.order').search(cr, uid,[('name','=',objeto.document_origin)], context=context)[0]
                     objeto_venta = self.pool.get('sale.order').browse(cr, uid,venta_id,context=context)
                     numero_factura = ''
                     moneda = objeto_venta.company_id.currency_id.name
                     partner_id = objeto_venta.partner_id.id
                     name_partner = objeto_venta.partner_id.name
                  else:
                     #raise osv.except_osv(_('Alerta!'), _(str(objeto.picking_id)+str(objeto.picking_id.picking_type_id.id)+str(objeto.picking_id.picking_type_id.code)))
                     #objeto_venta = self.pool.get('sale.order').browse(cr, uid,venta_id,context=context)
                     numero_factura = ''
                     moneda = 'MXN'
                     partner_id = ''
                     name_partner = ''
                  
           #albaranes (entrada)      
          elif(objeto.picking_id.picking_type_id.code == 'incoming'):
            for cost_m in objeto.picking_id.move_lines:
              if cost_m.product_id == objeto.product_id:
                costo_movimiento = cost_m.price_unit * cost_m.product_uom_qty
                precio = cost_m.price_unit
                try:
                    factura_id = self.pool.get('account.invoice').search(cr, uid,[('origin','=',objeto.picking_origin)], context=context)[0]
                    objeto_factura = self.pool.get('account.invoice').browse(cr, uid,factura_id,context=context)
                    moneda = objeto_factura.currency_id.name
                    numero_factura = objeto_factura.number or ''
                    partner_id = objeto_factura.partner_id.id
                    name_partner = objeto_factura.partner_id.name
                except:
                  compra_id = self.pool.get('purchase.order').search(cr, uid,[('name','=',objeto.document_origin)], context=context)[0]
                  objeto_compra = self.pool.get('purchase.order').browse(cr, uid,compra_id,context=context)
                  numero_factura = ''
                  moneda = objeto_compra.currency_id.name
                  partner_id = objeto_compra.partner_id.id
                  name_partner = objeto_compra.partner_id.name
                  
          #albaranes (transferencias)     
          else:
            for cost_m in objeto.picking_id.move_lines:
              if cost_m.product_id == objeto.product_id:
                costo_movimiento = cost_m.price_unit * cost_m.product_uom_qty
                precio = cost_m.price_unit
                numero_factura = ''
                moneda = 'MXN'
                partner_id = ''
                name_partner = ''
        elif objeto.ajuste_id:
          costo_movimiento = 0.00
          precio = 0.0
          moneda = "MXN"
          numero_factura = ''
          partner_id = ''
          name_partner = ''
          no_folio = objeto.ajuste_id.name
         
        vals = {
                     'date_from':time.strftime('%Y-%m-%d'),
                     'date_to':time.strftime('%Y-%m-%d'),
                     'gv_inventario_analitico_id':data.id,
                     'clave_articulo': objeto.product_id.default_code,
                     'descripcion':objeto.product_id.name_template,
                     'cantidad':objeto.product_qty_move,
                     'costo_movimiento':costo_movimiento,
                     'costo_promedio':costo_promedio,
                     'fecha_movimiento':objeto.create_date,
                     'no_folio':no_folio,
                     'tipo_documento':tipo_documento,
                     'numero_almacen':unicode(objeto.warehouse_id.name).encode("latin-1")+str("(")+str(objeto.warehouse_id.id)+str(")"),
                     'estado':'FINALIZADO',
                     'precio':precio,
                     'moneda':moneda,
                     'numero_factura':numero_factura,
                     'existencia_anterior':objeto.existencia_anterior,
                     'existencias':objeto.existencia_posterior,
                     'numero_proveedor':partner_id,
                     'proveedor':name_partner,
                     'categoria':objeto.product_id.product_tmpl_id.categ_id.id,
                     }
        obj_analitico.create(cr, uid, vals, context=context)
        
  def filtrar(self, cr, uid, ids, context=None):
    """Funcion que actualiza la tabla account_invoice el campo gv_venta_x_fecha_id"""
    data =self.browse(cr, uid, ids)
    if not data.date_from or not data.date_to or not data.select_by or not data.warehouse_id\
    or not data.warehouse_dest_id or not data.product_id or not data.id_product:
       raise osv.except_osv(_('Alerta!'), _("Verifique que sus datos sean correctos"))
    
    #if data.gv_inventario_analitico_ids:
       #cr.execute("""DELETE FROM _gv_inventario_analitico WHERE gv_inventario_analitico_id=%s""",(data.id,))
       #cr.commit()
    self._get_analitico(cr, uid, ids, context=None)
    # self._get_warehouse(cr, uid, ids, context=None)
    return True
         
  def onchage_date_from(self, cr, uid, ids, date_from) :
      """
      Valida que la fecha se eliga no sea la actual ni mayor que la actual
      """
      # # try:
      return {'value' : {},
            'warning': {'title': 'Alerta',
                    'message': str(time.strftime(date_from))+" : "+str(datetime.today())}}
      return {'value' : {},
        'warning': {'title': 'Alerta',
                    'message': str(time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))+" : "+str(datetime.today()-relativedelta(days=1))},}
      # except:
      #   return {'value' : {}}
  
  #Nombre del modelo
  _name = 'gv.inventario.analitico'
  #Nombre de la tabla
  _table = '_gv_inventario_analitico'
  _rec_name = 'id'
  #Columnas y/o campos Tree & Form
 
  _columns = {
    'date_from': fields.datetime('De:', required=True),
    'date_to': fields.datetime('A: ', required=True),
    'almacen':fields.char('ALMACEN', size=254),
    'clave_articulo': fields.char('Clave artículo', size=254),
    'descripcion': fields.char('Descripción', size=254),
    'cantidad': fields.char('Cantidad',size=254),
    'existencia_anterior': fields.integer('Existencia anterior'),
    'costo_movimiento': fields.float('Costo movimiento'),
    'costo_promedio': fields.float('Costo promedio'),
    'fecha_movimiento': fields.datetime('Fecha movimiento'),
    'no_folio': fields.char('No.folio',size=254),
    'tipo_documento': fields.char('Tipo de documento',size=254),
    'numero_almacen': fields.char('No.almacén',size=254),
    'estado': fields.char('Estado',size=254),
    'precio': fields.float('Precio'),
    'moneda': fields.char('Moneda'),
    'existencias': fields.float('Existencias', size=254),
    'numero_factura': fields.char('No.factura',size=254),
    'numero_proveedor': fields.char('No.proveedor',size=254),
    'proveedor': fields.char('Proveedor',size=254),
    'categoria': fields.many2one('product.category','Categoría',),
    'select_by': fields.selection([
            ('consigna', 'Incluir artículos en consignación'),
            ('sinconsigna', 'Sin incluir artículos en consignación'),
            ('soconsigna', 'Solo artículos en consignación'),
            ], 'Consginación:', required=False,),
    'product_id':fields.many2one('product.product', 'Inicial:'),
    'id_product':fields.many2one('product.product','Final:'),
    'partner_id':fields.many2one('res.partner','Inicial:'),
    'id_partner':fields.many2one('res.partner','Final:'),
    'gv_inventario_analitico_id':fields.many2one('gv.inventario.analitico','inventarioid'),
    'gv_inventario_analitico_ids':fields.one2many('gv.inventario.analitico','gv_inventario_analitico_id','Historial'),
    'warehouse_id': fields.many2one('stock.warehouse','Inicial:'),
    'warehouse_dest_id': fields.many2one('stock.warehouse','Final:'),
    
    'warehouse_aux_id': fields.many2one('stock.warehouse','Inicial:',domain="[('gvconsigacion', '=', True)]"),
    'warehouse_aux_dest_id': fields.many2one('stock.warehouse','Final:',domain="[('gvconsigacion', '=', True)]"),
    #'active_all':fields.boolean('Todos:'),
    'active_all' : fields.selection([
            ('all', 'Todos'),
            ('product', 'Artículo'),
            ],'Filtrar por:', required=False,),
    'delete_records':fields.boolean('Elimanar registros de reportes'),
  }
  
  #funciones onchange
  @api.onchange('warehouse_aux_id')
  def _onchange_warehouse_id_aux(self):
        self.warehouse_id = self.warehouse_aux_id
        
  @api.onchange('warehouse_aux_dest_id')
  def _onchange_warehouse_dest_aux(self):
        self.warehouse_dest_id = self.warehouse_aux_dest_id
  
  def onchage_active_all(self, cr, uid, ids, active_all) :
      """
       Todos los productos, ordenados por SKU
      """
      try:
        if active_all == 'all':
          cr.execute("""select min(default_code) from product_product""")
          first = cr.fetchone()[0]
          min_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',first)])[0]
          cr.execute("""select max(default_code) from product_product""")
          maxx = cr.fetchone()[0]
          max_id=self.pool.get('product.product').search(cr, uid,[('default_code', '=',maxx)])[0]
          return {'value' : {'product_id':min_id, 'id_product':max_id},}
        else:return {'value' : {'product_id':0, 'id_product':0},}
      except:
        return {'value' : {}}
      
  @api.onchange('select_by')
  def _onchange_select_by(self):
   if self.select_by == 'consigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_dest_id = max_id
      
   elif self.select_by == 'sinconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse WHERE type IN ('fac', 'sop', 'tra', 'scrp') """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse WHERE type IN ('fac', 'sop', 'tra', 'scrp') """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_dest_id = max_id
      
   if self.select_by == 'soconsigna':
      self.env.cr.execute(""" SELECT min(id) FROM stock_warehouse WHERE type='pre' """)
      min_id = self.env.cr.fetchone()[0]      
      self.warehouse_aux_id = min_id
      self.env.cr.execute(""" SELECT max(id) FROM stock_warehouse WHERE type='pre' """)
      max_id = self.env.cr.fetchone()[0]      
      self.warehouse_aux_dest_id = max_id
  
  def _get_min_warehouse(self, cr, uid, context=None):
        try:
            cr.execute(""" SELECT min(id) FROM stock_warehouse """)
            first = cr.fetchone()[0]
            return first
        except Exception, ex:
            return False
         
  def _get_max_warehouse(self, cr, uid, context=None):
        try:
            cr.execute(""" SELECT max(id) FROM stock_warehouse """)
            ultimate = cr.fetchone()[0]
            return ultimate
        except Exception, ex:
            return False
         
  def onchage_validate_id_product(self, cr, uid, ids, id_product, product_id) :
      """
        Valida que la fecha que elijan sea menor a la fecha actual..
      """
      try:
        object_product = self.pool.get('product.product')
        cr.execute("select default_code from product_product where id=%s", (product_id,))
        product_one = cr.fetchone()[0]
        cr.execute("select default_code from product_product where id=%s", (id_product,))
        product_2 = cr.fetchone()[0]
        if product_one>product_2:
          return {'value' : {'product_id':id_product,'id_product':product_id},}
      except:
          return {'value' : {}}
  
  _defaults = {
    'warehouse_id':_get_min_warehouse,
    'warehouse_dest_id':_get_max_warehouse,
    'warehouse_aux_id':_get_min_warehouse,
    'warehouse_aux_dest_id':_get_max_warehouse,
    'select_by':'consigna',
  }

analitico_inventario()

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

