# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Gerardo García, Agustín Cruz
#    Copyright Fedrojesa S.A. de C.V. 2013 (<http://openpyme.mx>)
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv


class account_move(osv.Model):
    _inherit = "account.move"

    _columns = {
        'use_in_diot': fields.boolean(
            'Use in DIOT',
            help='Unset if you do not want to include this movement into'
            ' DIOT report'
        )
    }


class account_move_line(osv.Model):
    _inherit = "account.move.line"

    _columns = {
        'use_in_diot': fields.related(
            'move_id', 'use_in_diot', type="boolean", store=False,
            string="Use in DIOT"
        )
    }

    def create(self, cr, uid, vals, context=None):
        """
        Check the partner and set default value for column use in diot
        according to partner information
        """
        partner_obj = self.pool.get('res.partner')
        if context is None:
            context = {}
        # Prevent fail when no partner asigned to movement
        if 'partner_id' in vals and vals['partner_id']:
            partner = partner_obj.browse(cr, uid, vals['partner_id'], context)
            if partner.vat_subjected:
                acc_mov_obj = self.pool.get('account.move')
                acc_mov = acc_mov_obj.browse(cr, uid, vals['move_id'], context)
                if not acc_mov.use_in_diot:
                    acc_mov_obj.write(cr, uid, [acc_mov.id],
                                      {'use_in_diot': True}, context)
        return super(account_move_line, self).create(cr, uid, vals, context)
