# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Gerardo García, Agustín Cruz
#    Copyright Fedrojesa S.A. de C.V. 2013 (<http://openpyme.mx>)
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "DIOT Report",
    "version": "1.0.7",
    "depends": [
        "l10n_mx_base_vat_split",
        "l10n_mx_account_tax"
    ],
    'category': 'Mexico',
    "description": """
DIOT Report
===========

Create DIOT Report for Mexico
    """,
    'author': 'Openpyme.mx',
    "website": "http://www.openpyme.mx",
    'data': [
        "view/partner_view.xml",
        "view/account_view.xml",
        "view/account_tax_view.xml",
        "wizard/wizard_diot_report_view.xml",
    ],
    'installable': True,
    'active': False,
}
