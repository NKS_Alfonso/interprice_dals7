# -*- coding: utf-8 -*-
##############################################################################
#
#    Authors: Gerardo García, Agustín Cruz
#    Copyright Fedrojesa S.A. de C.V. 2013 (<http://openpyme.mx>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _


class res_partner(osv.Model):
    _inherit = 'res.partner'

    _columns = {
        'type_of_third':fields.selection([
            ('04', '04 - National Supplier'),
            ('05', '05 - Foreign Supplier'),
            ('15', '15 - Global Supplier')],
            'Type of Third (DIOT)', help=_('Type of third for this partner')),
        'type_of_operation':fields.selection([
            ('03', '03 - Professional Services'),
            ('06', '06 - Building Rental'),
            ('85', '85 - Others')],
            'Type of Operation (DIOT)'),
        'nacionality_diot' : fields.char(_('Nacionality'), size=100,
                                help=_('Type nacionality when foreign supplier')
                             ),
    }
