# -*- coding: utf-8 -*-
##############################################################################
#
#    Authors: Gerardo García, Agustín Cruz
#    Copyright Fedrojesa S.A. de C.V. 2013 (<http://openpyme.mx>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _

import base64
import logging

_logger = logging.getLogger(__name__)


class wizard_account_diot_mx(osv.TransientModel):
    _name = 'account.diot.report'
    _description = 'Account - Mexico DIOT Report'

    _columns = {
        'name': fields.char('File Name', size=25, readonly=True),
        'company_id': fields.many2one(
            'res.company', 'Company', required=True, select=True
        ),
        'period_id': fields.many2one(
            'account.period', 'Period',
            help=_('Select period for your report'), required=True
        ),
        'filename': fields.char('Filename', size=128,
                                readonly=True, help='This is File name'),
        'file': fields.binary('File', readonly=True,
                              help='This file, you can import the SAT'),
        'filename_xls': fields.char(
            'Filename', size=128, readonly=True, help='This is File name'
        ),
        'file_xls': fields.binary(
            'File', readonly=True, help='This file, you can import the SAT'
        ),
        'state': fields.selection([('choose', 'Choose'),
                                   ('get', 'Get'),
                                   ('not_file', 'Not File')]),
        'target_move': fields.selection([('posted', 'All Posted Entries'),
                                         ('all', 'All Entries'),
                                         ], 'Target Moves', required=True),
    }

    _defaults = {
        'state': 'choose',
        'target_move': 'posted',
    }

    def default_get(self, cr, uid, fields, context=None):
        """
        This function load in the wizard, the company used by the user, and
        the previous period to the current
        """
        import datetime
        from dateutil.relativedelta import relativedelta

        comp = self.pool.get('res.company')
        data = super(wizard_account_diot_mx, self).default_get(
            cr, uid, fields, context=context
        )
        time_now = datetime.date.today() + relativedelta(months=-1)
        company_id = comp._company_default_get(
            cr, uid, 'account.diot.report', context=context
        )
        period_id = self.pool.get('account.period').search(
            cr, uid,
            [('date_start', '<=', time_now),
             ('date_stop', '>=', time_now),
             ('company_id', '=', company_id)]
        )
        if period_id:
            data.update({'company_id': company_id,
                        'period_id': period_id[0]})
        return data

    def create_report(self, cr, uid, ids, context=None):
        """
        This function is used when click on button
        and call function that properly create report
        """
        this = self.browse(cr, uid, ids)[0]
        res, state = self._calculate_diot(cr, uid, ids, context)
        period = this.period_id

        if state == 'partners':
            return {
                'name': _('This suppliers do not have the information'
                          ' necessary for the DIOT report'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'res.partner',
                'type': 'ir.actions.act_window',
                'domain': [
                    ('id', 'in', res),
                    '|', ('active', '=', False), ('active', '=', True)
                ],
            }
        # There are some lines with out partner ask user to fix them
        if state == 'lines':
            return {
                'name': _('This lines do not have partner and cannot be '
                          'processed for Diot report'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
            }
        # There are some lines with partner market as not vat subjected
        if state == 'vat_subjected':
            return {
                'name': _('Lines assigned to a partner not vat subjected.'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
            }
        # Select state for redraw the form
        if len(res):
            state = 'get'
            name = "%s-%s-%s.txt" % ("DIOT", this.company_id.name, period.name)
            name_xls = "%s-%s-%s.xls" % ("DIOT", this.company_id.name, period.name)
            # Write record
            self.write(cr, uid, ids, {
                'state': state,
                'file': base64.encodestring(self._get_csv(res)),
                'filename': name,
                'file_xls': base64.encodestring(self._get_xls(res)),
                'filename_xls': name_xls
                }, context=context)
        else:
            state = 'not_file'
            self.write(cr, uid, ids, {'state': state}, context=context)

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_id': this.id,
            'views': [(False, 'form')],
            'res_model': 'account.diot.report',
            'target': 'new',
        }

    def _calculate_diot(self, cr, uid, ids, context=None):
        """
        This function create the file for report to DIOT, take the amount base
        paid by partner in each tax, in the period and company selected.
        """
        if context is None:
            context = {}

        # Validate tax configuration
        tax_obj = self.pool.get('account.tax')
        tax_ids = tax_obj.search(
            cr, uid,
            [('tax_categ', '=', 'vat'), ('type_tax_use', '=', 'purchase')],
            context=context
        )
        # Variable for get all the account_ids to use on the report
        acc_ids = []
        for tax in tax_obj.browse(cr, uid, tax_ids, context=context):
            if not tax.diot_group or not tax.account_reconcile_id:
                raise osv.except_osv(
                    _('Error!'),
                    _('Tax %s is not configured correctly' % tax.name)
                )
            else:
                acc_ids.append(tax.account_reconcile_id.id)

        # Get data to filter account move lines for report
        this = self.browse(cr, uid, ids)[0]
        company = this.company_id
        _logger.debug("Selecting company %s" % company.name)
        period = this.period_id
        _logger.debug("Selecting period %s" % period.name)
        state = this.target_move
        _logger.debug("Selecting %s entries" % state)

        # Get lines to analyze on report
        acm_line_obj = self.pool.get('account.move.line')
        acml_ids = acm_line_obj.search(
            cr, uid,
            [('company_id', '=', company.id),
             ('period_id', '=', period.id),
             ('account_id', 'in', acc_ids),
             ('tax2_id', 'in', tax_ids),
             ],
            context=context,
        )
        # Init analysis
        suppliers = {}
        fix_lines = []
        review_lines = []
        for acml in acm_line_obj.browse(cr, uid, acml_ids, context=context):
            # Filter move lines according to account.move
            # status selected by user on wizard and by
            # use_in_diot flag
            if (
                (state == 'posted' and acml.move_id.state != 'posted')
                or not acml.move_id.use_in_diot
            ):
                continue
            if not acml.partner_id:
                fix_lines.append(acml.id)
                continue
            if not acml.partner_id.vat_subjected:
                review_lines.append(acml.id)
                continue
            supplier = suppliers.setdefault(
                acml.partner_id.id,
                {'base_16': 0,
                 'base_11': 0,
                 'base_0': 0,
                 'base_ex': 0,
                 'ret_vat': 0
                 })
            # Get partner data so we don't need to browse it again
            supplier['partner'] = acml.partner_id
            # Adding tax base according to proper tax
            if acml.tax2_id.diot_group == 'vat_16':
                supplier['base_16'] += abs(acml.tax2_base)
            elif acml.tax2_id.diot_group == 'vat_11':
                supplier['base_11'] += abs(acml.tax2_base)
            elif acml.tax2_id.diot_group == 'vat_0':
                supplier['base_0'] += abs(acml.tax2_base)
            elif acml.tax2_id.diot_group == 'no_vat':
                supplier['base_ex'] += abs(acml.tax2_base)
            # Add VAT retentions if present
            if acml.tax_ret:
                supplier['ret_vat'] += abs(acml.tax_ret)
        # Return special response to indicate that there are lines
        # to fix in order to properly create the report
        if fix_lines:
            return fix_lines, 'lines'
        # Return special response to indicate that there are lines
        # to review to ensure data is correct for create the report
        if review_lines:
            return review_lines, 'vat_subjected'

        # Init response
        from constants import CODES
        res = []
        fix_partners = []
        for supplier_id in suppliers:
            supplier = suppliers[supplier_id]['partner']
            data = suppliers[supplier_id]
            # Validate DIOT information on partner form
            if (
                not supplier.vat_split or not supplier.type_of_third
                or not supplier.type_of_operation
                or (supplier.type_of_third == '05' and not supplier.country_id)
            ):
                fix_partners.append(supplier.id)
                continue

            _logger.debug("Adding supplier %s to report" % supplier.name)
            code = (supplier.country_id.code
                    if supplier.country_id.code in CODES else 'XX')
            res.append({
                'h1': supplier.type_of_third,
                'h2': supplier.type_of_operation,
                'h3': (supplier.vat_split.encode('utf-8')
                       if supplier.type_of_third == '04' else ''),
                'h4': (supplier.vat_split.encode('utf-8')
                       if supplier.type_of_third == '05' else ''),
                'h5': (supplier.name.encode('utf-8')
                       if supplier.type_of_third == '05' else ''),
                'h6': code if supplier.type_of_third == '05' else '',
                'h7': (supplier.nacionality_diot
                       if supplier.type_of_third == '05' else ''),
                'h8': ("%.0f" % data['base_16'] if 'base_16' in data and
                       data['base_16'] > 0 else ''),
                'h9': '',
                'h10': '',
                'h11': ("%.0f" % data['base_11'] if 'base_11' in data and
                        data['base_11'] > 0 else ''),
                'h12': '',
                'h13': '',
                'h14': '',
                'h15': '',
                'h16': '',
                'h17': '',
                'h18': '',
                'h19': ("%.0f" % data['base_0'] if 'base_0' in data and
                        data['base_0'] > 0 else ''),
                'h20': ("%.0f" % data['base_ex'] if 'base_ex' in data and
                        data['base_ex'] > 0 else ''),
                'h21': ("%.0f" % data['ret_vat'] if 'ret_vat' in data and
                        data['ret_vat'] > 0 else ''),
                'h22': '',
            })
        # Return special response for display list of partners
        # to fix for report
        if fix_partners:
            return fix_partners, 'partners'
        # Return response with data for create report
        return res, True

    def _get_csv(self, lines, delimiter='|'):
        """
        Generates a txt file for import into SAT Diot program
        """
        import csv
        import tempfile

        names = [
            'h1',
            'h2',
            'h3',
            'h4',
            'h5',
            'h6',
            'h7',
            'h8',
            'h9',
            'h10',
            'h11',
            'h12',
            'h13',
            'h14',
            'h15',
            'h16',
            'h17',
            'h18',
            'h19',
            'h20',
            'h21',
            'h22',
            'h23'  # We needed to add an extra column to force delimeter
        ]

        with tempfile.NamedTemporaryFile(delete=False) as fcsv:
            csvwriter = csv.DictWriter(
                fcsv, delimiter=delimiter, fieldnames=names
            )
            for line in lines:
                csvwriter.writerow(line)

        with open(fcsv.name, 'r') as fname:
            data = fname.read()

        return data

    def _get_xls(self, lines):
        """
        Generates an excel file for review DIOT results
        """
        import tempfile
        from xlwt import Workbook

        book = Workbook(encoding="UTF-8")
        sheet = book.add_sheet('DIOT')
        # Write head for sheet
        from constants import HEADERS
        for cell in range(1, 22):
            sheet.write(0, cell - 1, HEADERS['h' + str(cell)])
        # Write data into sheet
        count = 1
        for line in lines:
            for cell in range(1, 22):
                try:
                    t = float(line['h' + str(cell)])
                except:
                    t = line['h' + str(cell)]
                sheet.write(count, cell - 1, t)
            count += 1

        # Write data into file
        with tempfile.NamedTemporaryFile(delete=False) as fcsv:
            book.save(fcsv.name)

        with open(fcsv.name, 'r') as fname:
            data = fname.read()

        return data
