# -*- encoding: utf-8 -*-
from openerp import models, fields, api
from openerp.tools import float_compare, float_round
from openerp.exceptions import Warning


class account_invoice(models.Model):
    _inherit = 'account.invoice'

    cost_center_id = fields.Many2one(
        'account.cost.center', 'Centro de Costos',
        ondelete="cascade", required=False)

    def action_move_create(self, cr, uid, ids, context=None):
        try:
            super(account_invoice, self).action_move_create(
                cr, uid, ids, context=context)
            move_line_obj = self.pool.get('account.move.line')
            obj_invoice = self.pool.get('account.invoice').read(cr, uid, ids)
            centro_costo_id = obj_invoice[0]['cost_center_id'][0]
            move_id = obj_invoice[0]['move_id'][0]
            account_move_ids = move_line_obj.search(
                cr, uid, [('move_id', '=', move_id)])
            move_line_obj.write(
                cr, uid, account_move_ids, {'cost_center_id': centro_costo_id})
            move_line_ids = [line for line in move_line_obj.browse(
                cr, uid, account_move_ids)]
            if obj_invoice[0] and obj_invoice[0].get('type') == 'in_invoice':
                tc_alter = 0
                if obj_invoice[0] and obj_invoice[0].get(
                        'currency_rate_alter') > 0:
                    tc_alter = obj_invoice[0].get('currency_rate_alter')
                for line in move_line_ids:
                    if line.currency_id.id or obj_invoice[0].get(
                        'currency_id')[0] != line.company_id.currency_id.id:
                        if tc_alter > 0:
                            if line.debit:
                                debit = abs(
                                    tc_alter * line.amount_currency) or -abs(
                                        tc_alter * line.amount_currency)
                                move_line_obj.write(
                                    cr, uid, line.id,
                                    {'debit': debit})
                            else:
                                credit = abs(
                                    tc_alter * line.amount_currency) or -abs(
                                        tc_alter * line.amount_currency
                                        )
                                move_line_obj.write(
                                    cr, uid, line.id,
                                    {'credit': credit})
                        else:
                            if obj_invoice[0] and obj_invoice[0].get(
                                    'picking_dev_id'):
                                picking_id = obj_invoice[0].get(
                                    'picking_dev_id')[0]
                                rate_picking = self.get_rate_date_picking(
                                    cr, uid, ids, picking_id,
                                    line.currency_id, context=context)
                                if rate_picking[0] > 0:
                                    tc = float_round((1 / rate_picking[0]), 4)
                                else:
                                    tc = float_round(
                                        (1 / line.currency_id.rate_silent), 4)
                                if line.debit:
                                    debit = abs(
                                        tc * line.amount_currency) or -abs(
                                            tc * line.amount_currency)
                                    move_line_obj.write(
                                        cr, uid, line.id,
                                        {'debit': debit},
                                        context=context)
                                else:
                                    credit = abs(
                                        tc * line.amount_currency) or -abs(
                                            tc * line.amount_currency
                                        )
                                    move_line_obj.write(
                                        cr, uid, line.id,
                                        {'credit': credit},
                                        context=context)
        except Exception, e:
            print 'ERROR ' + str(e)
        finally:
            return True

    def get_rate_date_picking(
            self, cr, uid, ids, picking_id=None, currency=None, context=None):
        currency_id = currency.id
        picking = picking_id
        rate = 0
        cr.execute(
            """
            SELECT
                rcr.rate FROM
            res_currency_rate rcr
            JOIN stock_picking as sp
            ON sp.id = {}
            WHERE rcr.currency_id = {}
            AND rcr.name <= sp.date_done
            AND rcr.rate > 0
            ORDER BY rcr.name DESC
            LIMIT 1;
            """.format(picking, currency_id))
        rate_query = cr.fetchone()
        if rate_query:
            rate = rate_query
        return rate

    def create(self, cr, uid, vals, context=None):
        try:
            centro_costo_id = 0
            purchase_id = 0
            doc_name = ""

            if vals.get('name'):
                doc_name = vals.get('name')
            elif vals.get('origin'):
                doc_name = vals.get('origin')
            if doc_name:
                # BUSCAMOS EN VENTAS
                obj_sale_order_ids = self.pool.get('sale.order').search(cr, uid, [('name', '=', doc_name)])
                if len(obj_sale_order_ids) > 0:
                    #print obj_sale_order_ids
                    obj_sale_order = self.pool.get('sale.order').browse(cr, uid, obj_sale_order_ids)
                    centro_costo_id = obj_sale_order[0]['centro_costo_id']['id']
                else:
                    # BUSCAMOS EN COMPRAS
                    obj_purchase_order_ids = self.pool.get('purchase.order').search(cr, uid, [('name', '=', doc_name)])
                    if len(obj_purchase_order_ids) > 0:
                        obj_purchase_order = self.pool.get('purchase.order').browse(cr, uid, obj_purchase_order_ids)
                        centro_costo_id = obj_purchase_order[0]['centro_costo_id']['id']
                    else:
                        # BUSCAMOS EN ALBARANES DE ENTRADA
                        obj_stock_pikcing_in_ids = self.pool.get('stock.picking').search(cr, uid, [('name', '=', doc_name)])
                        if len(obj_stock_pikcing_in_ids) > 0:
                            obj_stock_pikcing_in = self.pool.get('stock.picking').browse(cr, uid, obj_stock_pikcing_in_ids)
                            #print 'ids stock in ' + str(obj_stock_pikcing_in)
                            purchase_obj = self.pool.get('purchase.order').search(
                                cr, uid, [('name', '=', obj_stock_pikcing_in.origin)], limit=1, context=context)
                            if len(purchase_obj) > 0:
                                purchase_id = self.pool.get('purchase.order').browse(cr, uid, purchase_obj)
                            #print 'compra id ' + str(purchase_id)
                            centro_costo_id = purchase_id.centro_costo_id.id
                        # else:
                        # 	# BUSCAMOS EN ALBARANES DE SALIDA
                        # 	obj_stock_pikcing_out_ids = self.pool.get('stock.picking.out').search(cr, uid, [('name', '=', doc_name)])
                        # 	if len(obj_stock_pikcing_out_ids) > 0:
                        # 		obj_stock_pikcing_out = self.pool.get('stock.picking.out').browse(cr, uid, obj_stock_pikcing_out_ids)
                        # 		sale_id = obj_stock_pikcing_out[0]['sale_id']['id']
                        # 		obj_sale_order = self.pool.get('sale.order').browse(cr, uid, [sale_id])
                        # 		centro_costo_id = obj_sale_order[0]['centro_costo_id']['id']

            if centro_costo_id > 0:
                vals.update({'cost_center_id': centro_costo_id})
        except Exception, e:
            print e
        finally:
            return super(account_invoice, self).create(
                cr, uid, vals, context=context)

    @api.multi
    def invoice_validate(self):
        res = super(account_invoice, self).invoice_validate()
        if self.picking_dev_id and 'PO' in self.picking_dev_id.origin:
            if self.picking_dev_id.state == 'done':
                order_obj = self.env['purchase.order']
                order_id = order_obj.search([(
                    'name', '=', self.picking_dev_id.origin)])
                if order_id and order_id.state == 'approved':
                    order_id.state = 'done'

        return res

    def write(self, cr, uid, ids, vals, context=None):
        #print 'WRITE INVOICE '
        return super(account_invoice, self).write(
            cr, uid, ids, vals, context=context)


class account_move_line(models.Model):
    _inherit = 'account.move.line'

    cost_center_id = fields.Many2one('account.cost.center', 'Centro de Costos', ondelete="cascade", required=False)

    def _query_get(self, cr, uid, obj='l', context=None):
        query = ""
        cost_center_ids_list = []
        str_cost_center_ids = ""
        query = super(account_move_line, self)._query_get(cr, uid, obj, context=context)
        try:
            if context.get('ifrs_id',False):
                ifrs_id = context.get('ifrs_id')
                ifrs_obj = self.pool.get('ifrs.ifrs').read(cr,uid,ifrs_id)
                cost_center_ids_list = ifrs_obj['cost_center_ids']
                str_cost_center_ids = ""
                for c in cost_center_ids_list:
                    str_cost_center_ids += str(c) + ','

                query += ' AND '+obj+'.cost_center_id IN (%s)' % (str_cost_center_ids[:-1])
            elif context.get('active_model', False):
                model = context.get('active_model')
                if model == 'wizard.report':
                    wiz_report_id = context.get('active_id')
                    wiz_report_obj = self.pool.get(model).read(cr,uid,wiz_report_id)
                    cost_center_ids_list = wiz_report_obj['cost_center_ids']
                    str_cost_center_ids = ""
                    for c in cost_center_ids_list:
                        str_cost_center_ids += str(c) + ','

                    if str_cost_center_ids != "":
                        query += ' AND '+obj+'.cost_center_id IN (%s)' % (str_cost_center_ids[:-1])
        except Exception, e:
            print 'ERROR ' + str(e)
        finally:
            return query

    #@api.model
    def add_costcenter_line(self, move_ids=None, line_ids=None, cost_center_id=None):
        """
        Argumentos
        1.- Recibe una lista o un objecto de account.move
        2.- Recibe el (int)cost_center_id de account.invoice
        """
        result = None
        if move_ids:
            try:
                if isinstance(move_ids, object):
                    move_ids.line_id.write({'cost_center_id':cost_center_id})
                    result = True
                else:
                    line_ids = self.search([("move_id", "=", move_ids)])
                    line_obj = self.browse(line_ids)
                    line_obj.write({'cost_center_id':cost_center_id})
                    result = True
            except Exception as e:
                result = False
                raise Warning('Error : {}'.format(e))

        if line_ids:
            try:
                line_obj = self.browse(line_ids)
                line_obj.write({'cost_center_id':cost_center_id})
                result = True
            except Exception as e:
                result = False
                raise Warning('Error : {}'.format(e))

        return result


class account_voucher(models.Model):
    _inherit = 'account.voucher'

    cost_center_id = fields.Many2one(
        'account.cost.center', 'Centro de Costos', ondelete="cascade",
        required=False)

    def proforma_voucher(self, cr, uid, ids, context=None):
        super(account_voucher, self).proforma_voucher(
            cr, uid, ids, context=context)
        try:
            obj_voucher = self.pool.get('account.voucher').read(cr, uid, ids)
            account_move_line_ids = obj_voucher[0]['move_ids']
            centro_costo_id = obj_voucher[0]['cost_center_id']
            env = api.Environment(cr, uid, context)
            move_line_obj = env['account.move.line']
            result = move_line_obj.add_costcenter_line(
                line_ids=account_move_line_ids,
                cost_center_id=centro_costo_id[0])
        except Exception, e:
            print 'ERROR ' + str(e)
        finally:
            return True
