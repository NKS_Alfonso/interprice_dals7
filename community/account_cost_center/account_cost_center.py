# -*- encoding: utf-8 -*-
from openerp import models, fields, api

class account_cost_center(models.Model):
    _name = 'account.cost.center'
    _description = """ Creates a Cost center configurator"""

    
    name = fields.Char("Name", size=64, required=True)
    active = fields.Boolean("Active")

    _defaults = {
    	'active':True,
    }