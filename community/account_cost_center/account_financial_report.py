# -*- encoding: utf-8 -*-
from openerp.report import report_sxw
from openerp import models, fields, api
import pdb

class account_financial_report(models.Model):
    _inherit = "afr"
    _description = """Add a new many2many field related to model account.cost.center"""
    cost_center_ids = fields.Many2many("account.cost.center", "afr_cost_center","cost_center_id", "afr_id", "Cost Centers")

class report_wizard(models.Model):
	_inherit = "wizard.report"

	def onchange_afr_id(self, cr, uid, ids, afr_id, context=None):
		res = super(report_wizard, self).onchange_afr_id(cr, uid, ids, afr_id, context=context)
		if res.get('value', False) and afr_id:
			account_fin_report_obj = self.pool.get('afr').browse(cr, uid, afr_id, context=context)
			res['value'].update({'cost_center_ids': [cent.id for cent in account_fin_report_obj.cost_center_ids]})
		return res

	cost_center_ids = fields.Many2many('account.cost.center', 'costcenter_wiz_rel', 'wizard_id','cost_center_id', 'Centro de Costos')
	# def print_report(self, cr, uid, ids, data, context=None):
	# 	context = {}
	# 	obj = self.read(cr,uid,ids)
	# 	cost_center_ids_list = obj[0]['cost_center_ids']
	# 	str_cost_center_ids = ""

	# 	for c in cost_center_ids_list:
	# 		str_cost_center_ids += str(c) + ','

	# 	context['cost_center_ids'] = str_cost_center_ids[:-1]
	# 	return super(report_wizard,self).print_report(cr, uid, ids, data, context=context)