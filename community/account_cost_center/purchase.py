# -*- encoding: utf-8 -*-
from openerp import models, fields, api

class purchase_order(models.Model):
	_inherit = 'purchase.order'

	centro_costo_id = fields.Many2one('account.cost.center', 'Centro de Costos', ondelete="cascade", required=False, readonly=False)

	def create(self, cr, uid, vals, context=None):
		try:
			print vals
			picking_type_id = vals['picking_type_id']
			stock_picking_type_obj = self.pool.get('stock.picking.type').browse(cr, uid, picking_type_id)
			warehouse_id = stock_picking_type_obj['warehouse_id']['id']
			obj_warehouse = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id)#.centro_costo_id
			centro_costo_id = obj_warehouse['centro_costo_id']['id']
			vals['centro_costo_id'] = centro_costo_id
		except Exception, e:
			print e
		finally:
			return super(purchase_order, self).create(cr, uid, vals, context=context)

	def write(self, cr, uid, ids, vals, context=None):
		try:
			print vals
			picking_type_id = vals['picking_type_id']
			stock_picking_type_obj = self.pool.get('stock.picking.type').browse(cr, uid, picking_type_id)
			warehouse_id = stock_picking_type_obj['warehouse_id']['id']
			obj_warehouse = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id)#.centro_costo_id
			centro_costo_id = obj_warehouse['centro_costo_id']['id']
			vals['centro_costo_id'] = centro_costo_id
		except Exception, e:
			print e
		finally:
			return super(purchase_order, self).write(cr, uid, ids, vals, context=context)