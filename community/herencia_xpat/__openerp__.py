{
    'name': 'herencia_xpath',
    'version': '1.0',
    'description': """DESHABILITA EL CREAR Y EDITAR EN LAS RELACIONES MANY2ONE DE LOS SIGUIENTES MODELOS:
    Ventas, Compras, Almacen(ajustes de inventario), Res_partner, Contabilidad(facturas de cliente, pagos de cliente, factuas de proveedores,
    pagos de proveedores, cuentas, polizas, impuestos, diarios, notas de credito por cliente, notas de credito por proveedor), Reportes""",
    'author': 'Yessica Vazquez Guerrero',
    'depends': ['base','purchase'],
    'data': [
       
        'xpath.xml',
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}