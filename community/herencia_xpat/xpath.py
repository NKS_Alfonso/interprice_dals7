# -*- coding: utf-8 -*-
from openerp.osv import  osv

class purchase_order (osv.osv):

  _inherit ='purchase.order'
  _columns = {}
  
purchase_order()

class sale_order (osv.osv):

  _inherit ='sale.order'
  _columns = {}
  
sale_order()

class stock_inventory (osv.osv):
  
  _inherit ='stock.inventory'
  _columns = {}
  
stock_inventory()

class res_partner (osv.osv):
  
  _inherit ='res.partner'
  _columns = {}
  
res_partner()

class account_invoice(osv.osv):
  
  _inherit ='account.invoice'
  _columns = {}
  
account_invoice()

class stock_picking(osv.osv):
  
  _inherit ='stock.picking'
  _columns = {}
  
stock_picking()

class stock_return_picking(osv.osv):
  
  _inherit ='stock.return.picking'
  _columns = {}
  
stock_return_picking()


class account_voucher (osv.osv):
  
  _inherit ='account.voucher'
  _columns = {}
  
account_voucher()

class account_move(osv.osv):
  
  _inherit ='account.move'
  _columns = {}
  
account_move()

class account_journal(osv.osv):
  
  _inherit ='account.journal'
  _columns = {}
  
account_journal()

class account_account(osv.osv):
  
  _inherit ='account.account'
  _columns = {}
  
account_account()

class account_tax(osv.osv):
  
  _inherit ='account.tax'
  _columns = {}
  
account_tax()








