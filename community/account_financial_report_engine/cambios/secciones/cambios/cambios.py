# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : Viridiana Cruz Santos(gvadeto)2016                                                                                              #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime

##################################################################################
#                      cambios de fecha en factura                               #
#################################################################################
class account_invoice( osv.osv ):
  _inherit = 'account.invoice'
  
  _columns = {}
  _defaults = {
      'date_invoice': lambda *a: time.strftime('%Y-%m-%d'),
    } 
account_invoice()

class stock_invoice_onshipping( osv.osv ):
  _inherit = 'stock.invoice.onshipping'
            
  _columns = {
    'date_invoice_stock': fields.date(),
    }
  _defaults = {
      'date_invoice_stock': lambda *a: time.strftime('%Y-%m-%d'),
      'invoice_date' : lambda *a: time.strftime('%Y-%m-%d'),
    } 
account_invoice()

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:

