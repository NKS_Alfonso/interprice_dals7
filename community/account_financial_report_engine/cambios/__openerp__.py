# -*- coding: utf-8 -*-

{
	'name': 'Cambios factura',
	'version': '1.0',
	'category': 'contabilidad',
	'author': 'Viridiana Cruz Santos(gvadeto)2016',
	'description': """
									Funcionalidad: Este módulo se instala exclusivamente para cambios en factura
									""",
	'maintainer': '',
	# 'website': '',
	'installable': True, 
	'active': False,
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base','sale','stock','account','purchase'
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		
		#Archivo principal de menus
		'menus.xml',
		
		#------------------------------Cambios----------------#
		'secciones/cambios/cambios.xml',		
		
		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
