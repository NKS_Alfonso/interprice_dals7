# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################
from openerp import models, api


class account_invoice(models.Model):

    _inherit = "account.invoice"

    @api.multi
    def invoice_pay_customer(self):
        self.ensure_one()
        res = super(account_invoice, self).invoice_pay_customer()
        res['context']['default_net_amount'] = (
            self.type in ('out_refund', 'in_refund') and -self.residual
            or self.residual)
        return res


    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line.price_subtotal',
        'move_id.line_id.account_id.type',
        'move_id.line_id.amount_residual',
        # Fixes the fact that move_id.line_id.amount_residual, being not stored and old API, doesn't trigger recomputation
        'move_id.line_id.reconcile_id',
        'move_id.line_id.amount_residual_currency',
        'move_id.line_id.currency_id',
        'move_id.line_id.reconcile_partial_id.line_partial_ids.invoice.type',
    )
    # An invoice's residual amount is the sum of its unreconciled move lines and,
    # for partially reconciled move lines, their residual amount divided by the
    # number of times this reconciliation is used in an invoice (so we split
    # the residual amount between all invoice)
    def _compute_residual(self):
        #import pdb; pdb.set_trace()
        variable_residual=0
        #self.residual = 0.0
        # Each partial reconciliation is considered only once for each invoice it appears into,
        # and its residual amount is divided by this number of invoices
        partial_reconciliations_done = []

        if self.id:
            select_sql=('''
                        SELECT 
                            AI.id self0,
                            AML.id line1,
                            AA.type account_id_type2,
                            CASE WHEN AML.reconcile_partial_id is not null THEN True ELSE False END AS reconcile_partial3,
                            AML.reconcile_partial_id reconcile_partial_id4,
                            CASE WHEN AI.currency_id  is not null THEN AI.currency_id  ELSE 0 END AS self_currency_id5,
                            CASE WHEN AML.currency_id  is not null THEN AML.currency_id ELSE 0 END AS line_currency_id6,
                            CASE WHEN AML.currency_id  is not null THEN True ELSE False END AS line_currency7,
                            CASE WHEN AML.reconcile_id is not null THEN True ELSE False END AS reconcile8,
                            COALESCE(AA.reconcile,False) account_id_reconcile9,
                            CASE WHEN AML.currency_id is not null THEN COALESCE(AML.amount_currency,0) ELSE COALESCE(AML.debit - AML.credit,0) END AS move_line_total10,
                            CASE 
                                WHEN AML.currency_id is not null and AML.amount_currency < 0 THEN -1
                                WHEN AML.currency_id is not null and AML.amount_currency > 0 THEN 1
                                WHEN AML.currency_id is null and (AML.debit - AML.credit) < 0 THEN -1
                                WHEN AML.currency_id is null and (AML.debit - AML.credit) > 0 THEN 1
                            ELSE 
                                1
                            END AS sign11,
                            COALESCE((AML.debit - AML.credit),0) line_total_in_company_currency12,
                            CASE WHEN COMPANY.currency_id  is not null THEN COMPANY.currency_id ELSE 0 END AS line_company_id_currency_id13,
                            CURRENCY.rounding rounding14
                        FROM  account_invoice AI
                        LEFT JOIN account_move AM ON AI.move_id=AM.id
                        LEFT JOIN account_move_line AML ON AM.id=AML.move_id
                        LEFT JOIN account_account AA ON AML.account_id=AA.id
                        LEFT JOIN res_company COMPANY ON AML.company_id=COMPANY.id
                        LEFT JOIN res_currency CURRENCY ON AML.currency_id=CURRENCY.id
                        where AI.id=%s and (AA.type='receivable' or AA.type='payable')
                        GROUP BY AI.id,AML.id,AA.id,COMPANY.id,CURRENCY.id
                        '''%
                       (self.id))
            self.env.cr.execute(select_sql)
            select = self.env.cr.fetchall()
            #import pdb; pdb.set_trace()
            cur_obj = self.pool.get('res.currency')
            for line_sql in select:
                #print('_compute_residual 1.1')
                amount_residual_currency=0
                amount_residual=0

                line = self.env['account.move.line'].browse(line_sql[1])
                if line_sql[3]:
                    if line_sql[4] in partial_reconciliations_done:
                        continue

                #calcula valor de amount_residual_currency y amount_residual
                context = {}
                if not line_sql[8] and line_sql[9]:
                    if line_sql[7]:
                        move_line_total = line_sql[10]
                        sign = line_sql[11]
                    else:
                        move_line_total = line_sql[10]
                        sign = line_sql[11]
                    line_total_in_company_currency =  line_sql[12]
                    context_unreconciled = context.copy()
                    if line_sql[3]:
                        reconcile_partial_line_partial_sql=('''
                                        SELECT 
                                            AML.id line0,
                                            CASE WHEN AML.currency_id  is not null THEN AML.currency_id  ELSE 0 END AS payment_line_currency1,
                                            CASE WHEN AML.currency_id is not null THEN True ELSE False END AS payment_line_currency_id2,
                                            COALESCE(AML.amount_currency,0) amount_currency3,
                                            AML.date date4,
                                            COALESCE((AML.debit - AML.credit),0) payment_line_debit_credit5
                                        FROM account_move_line AML
                                        where AML.reconcile_partial_id=%s and AML.id != %s
                                    '''%
                                   (line_sql[4],line_sql[1]))
                        self.env.cr.execute(reconcile_partial_line_partial_sql)
                        reconcile_partial_line_partial = self.env.cr.fetchall()

                        for payment_line in reconcile_partial_line_partial:
                            #import pdb; pdb.set_trace()
                            if payment_line[2] and line_sql[7] and payment_line[1] == line_sql[6]:
                                    move_line_total += payment_line[3]
                            else:
                                if line_sql[7]:
                                    context_unreconciled.update({'date': payment_line[4]})
                                    amount_in_foreign_currency = cur_obj.compute(cr, uid, line_sql[13], line_sql[6], payment_line[5], round=False, context=context_unreconciled)
                                    move_line_total += amount_in_foreign_currency
                                else:
                                    move_line_total += payment_line[5]
                            line_total_in_company_currency += payment_line[5]
                    result = move_line_total
                    amount_residual_currency =  sign * (line_sql[7] and self.env['res.currency'].browse(line_sql[6]).round(result)  or result)
                    amount_residual = sign * line_total_in_company_currency
                #calcula valor de amount_residual_currency y amount_residual

                #import pdb; pdb.set_trace()
                if line_sql[6] == line_sql[5]:
                    line_amount = amount_residual_currency if line_sql[7] else amount_residual
                else:
                    line_amount = self.env['res.currency'].browse(line_sql[13]).compute(amount_residual, self.currency_id)
                #print('_compute_residual 1.2')

                if line_sql[3]:
                    #print('_compute_residual 2.1')

                    reconcile_partial_line_partial_sql2=('''
                                    SELECT 
                                        AML.id pline0,
                                        CASE WHEN AI.id  is not null THEN True ELSE False END AS pline_invoice1,
                                        COALESCE(AI.type,'NA') pline_invoice_type2,
                                        AI.id pline_invoice_id3
                                    FROM account_move_line AML
                                    LEFT JOIN account_move AM ON AML.move_id=AM.id
                                    LEFT JOIN account_invoice AI ON AM.id=AI.move_id
                                    where AML.reconcile_partial_id=%s
                                    GROUP BY AML.id,AI.id
                                '''%
                               (line_sql[4]))
                    self.env.cr.execute(reconcile_partial_line_partial_sql2)
                    reconcile_partial_line_partial2 = self.env.cr.fetchall()

                    partial_reconciliation_invoices = set()
                    for pline in reconcile_partial_line_partial2:
                        if pline[1] and self.type == pline[2]:
                            partial_reconciliation_invoices.update([pline[3]])
                    line_amount = self.currency_id.round(line_amount / len(partial_reconciliation_invoices))
                    partial_reconciliations_done.append(line_sql[4])
                    #print('_compute_residual 2.2')
                variable_residual += line_amount
        self.residual = max(variable_residual, 0.0)

        """
        for line in self.sudo().move_id.line_id:
            if line.account_id.type not in ('receivable', 'panyable'):
                continue
            if line.reconcile_partial_id and line.reconcile_partial_id.id in partial_reconciliations_done:
                continue
            # Get the correct line residual amount
            if line.currency_id == self.currency_id:
                line_amount = line.amount_residual_currency if line.currency_id else line.amount_residual
            else:
                from_currency = line.company_id.currency_id.with_context(date=line.date)
                line_amount = from_currency.compute(line.amount_residual, self.currency_id)
            # For partially reconciled lines, split the residual amount
            if line.reconcile_partial_id:
                partial_reconciliation_invoices = set()
                for pline in line.reconcile_partial_id.line_partial_ids:
                    if pline.invoice and self.type == pline.invoice.type:
                        partial_reconciliation_invoices.update([pline.invoice.id])
                line_amount = self.currency_id.round(line_amount / len(partial_reconciliation_invoices))
                partial_reconciliations_done.append(line.reconcile_partial_id.id)
            self.residual += line_amount
        self.residual = max(self.residual, 0.0)
        """