# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round, float_is_zero
from openerp.osv import osv


class account_voucher(models.Model):

    _inherit = "account.voucher"

    net_amount = fields.Float(
        'Amount',
        digits=dp.get_precision('Account'),
        required=True,
        default=0.0,    # for compatibility with other modules
        readonly=True,
        states={'draft': [('readonly', False)]},
        help='Amount Paid With Journal Method',
    )
    paylines_amount = fields.Float(
        _('Paylines Amount'),
        compute='_get_paylines_amount',
        digits=dp.get_precision('Account'),
        help=_('Amount Paid With Paylines: checks, withholdings, etc.'),
    )
    amount = fields.Float(
        string='Total Amount',
        compute='_get_amount',
        inverse='_set_net_amount',
        help='Total Amount Paid',
        copy=False,
        store=True,
    )
    # we created amount_readonly because we keep amount invisible so
    # it can be setted (if we make amount readonly it wont be setted).
    amount_readonly = fields.Float(
        related='amount',
        string='Total Amount',
        digits=dp.get_precision('Account'),
        readonly=True,
        )
    dummy_journal_id = fields.Many2one(
        related='journal_id',
        readonly=True,
        string='Dummy Journa',
        help='Field used for new api onchange methods over journal',
        )
    net_amount_hide = fields.Boolean(
        string='Hide net amount',
        compute="_compute_net_amount_hide"
        )

    @api.depends('journal_id')
    def _compute_net_amount_hide(self):
        if self.journal_id.account_account_apply_id.code not in (u'cash', u'bank'):
            self.net_amount_hide = True
        else:
            self.net_amount_hide = False

    @api.one
    @api.depends('net_amount')
    def _get_amount(self):
        self.amount = self.paylines_amount + self.net_amount

    @api.one
    def _set_net_amount(self):
        self.net_amount = self.amount - self.paylines_amount

    @api.one
    def _get_paylines_amount(self):
        self.paylines_amount = self.get_paylines_amount()[self.id]

    @api.multi
    def get_paylines_amount(self):
        res = {}
        for voucher in self:
            res[voucher.id] = 0.0
        return res

    @api.model
    def paylines_moves_create(
            self, voucher, move_id, company_currency, current_currency):
        """This function will be inherited by other modules that whant to add
        paylines and whant to make custom accout.move.lines, this function
        returns a total for all the lines created with this method
        """
        paylines_total = 0.0
        # If net amount create first move line (journal line)
        if voucher.net_amount:
            """
            move_line = self.create_first_line(
                voucher, move_id, company_currency, current_currency)
            paylines_total = move_line.debit - move_line.credit
            """
            """
            paylines_total = self.create_first_line(voucher, move_id, company_currency, current_currency)
            """
            v_create_uid=self.env.user.id
            v_write_uid=self.env.user.id
            v_ref=self.env['account.move'].browse(move_id).ref
            v_move_id=move_id
            v_name=voucher.name or '/'
            amount = voucher.net_amount

            if voucher.cost_center_id:
                v_cost_center_id=voucher.cost_center_id.id
            else:
                v_cost_center_id='''null'''

            if voucher.date:
                v_date="'"+voucher.date+"'"
            else:
                v_date='null'

            if voucher.date_due:
                v_date_maturity="'"+voucher.date_due+"'"
            else:
                v_date_maturity='null'

            v_company_id='''null'''
            if voucher.account_id:
                v_account_id=voucher.account_id.id
                v_company_obj=self.env['account.account'].browse(v_account_id).company_id
                if v_company_obj:
                    v_company_id=v_company_obj.id
            else:
                v_account_id='''null'''

            v_currency_id='''null'''
            if company_currency != current_currency:
                if current_currency:
                    v_currency_id=current_currency
                
            if voucher.partner_id :
                v_partner_id=voucher.partner_id.id
            else:
                v_partner_id='''null'''

            if voucher.journal_id:
                v_journal_id=voucher.journal_id.id
            else:
                v_journal_id='''null'''

            if voucher.period_id:
                v_period_id=voucher.period_id.id
            else:
                v_period_id='''null'''

            debit = credit = 0.0
            if voucher.type in ('purchase', 'payment'):
                credit = self._convert_amount(amount, voucher.id)
            elif voucher.type in ('sale', 'receipt'):
                debit = self._convert_amount(amount, voucher.id)
            if debit < 0: credit = -debit; debit = 0.0
            if credit < 0: debit = -credit; credit = 0.0
            sign = debit - credit < 0 and -1 or 1

            v_credit=credit
            v_debit=debit
            paylines_total=v_debit - v_credit

            if company_currency != current_currency:
                v_amount_currency=(sign * abs(amount))
            else:
                v_amount_currency=0

            insert_sql=(('''insert into account_move_line 
                            (
                                create_uid,write_uid,create_date,write_date,date_created,date,currency_id,partner_id,
                                journal_id,account_id,period_id,move_id,credit,debit,name,amount_currency,blocked,
                                centralisation,state,not_move_diot,is_tax_voucher,company_id,ref,cost_center_id,date_maturity
                            ) 
                            values (
                                    %s,%s,now() at time zone 'UTC',now() at time zone 'UTC',CURRENT_DATE,%s,%s,%s,
                                    %s,%s,%s,%s,'%s','%s','%s','%s',False,'normal','valid',False,False,%s,'%s',%s,%s
                                    ) 
                            RETURNING id;
                        ''')% (
                                v_create_uid,v_write_uid,v_date,v_currency_id,v_partner_id,v_journal_id,v_account_id,v_period_id,
                                v_move_id,v_credit,v_debit,v_name,v_amount_currency,v_company_id,v_ref,v_cost_center_id,v_date_maturity
                        ))
            #print(insert_sql)
            self.env.cr.execute(insert_sql)
            insert_id=self.env.cr.fetchall()
            move_line=insert_id[0][0]
            #print(move_line)


        return paylines_total

    @api.model
    def create_first_line(
            self, voucher, move_id, company_currency, current_currency):
        """Function that creates first move line and return the move line
        created.
        """
        move_lines = self.env['account.move.line']
        name = voucher.name or '/'
        payment_date = voucher.date_due
        amount = voucher.net_amount
        account = voucher.account_id
        partner = voucher.partner_id
        prepare_move_line=self.prepare_move_line(voucher, amount, move_id, name, company_currency,current_currency, payment_date, account, partner)
        
        v_create_uid=self.env.user.id
        v_write_uid=self.env.user.id
        v_ref=self.env['account.move'].browse(move_id).ref

        if voucher.cost_center_id:
            v_cost_center_id=voucher.cost_center_id.id
        else:
            v_cost_center_id='''null'''

        if prepare_move_line['date']:
            v_date="'"+prepare_move_line['date']+"'"
        else:
            v_date='null'

        if prepare_move_line['date_maturity']:
            v_date_maturity="'"+prepare_move_line['date_maturity']+"'"
        else:
            v_date_maturity='null'

        v_company_id='''null'''
        if prepare_move_line['account_id']:
            v_account_id=prepare_move_line['account_id']
            v_company_obj=self.env['account.account'].browse(prepare_move_line['account_id']).company_id
            if v_company_obj:
                v_company_id=v_company_obj.id
        else:
            v_account_id='''null'''

        if prepare_move_line['currency_id']:
            v_currency_id=prepare_move_line['currency_id']
        else:
            v_currency_id='''null'''

        if prepare_move_line['partner_id']:
            v_partner_id=prepare_move_line['partner_id']
        else:
            v_partner_id='''null'''

        if prepare_move_line['journal_id']:
            v_journal_id=prepare_move_line['journal_id']
        else:
            v_journal_id='''null'''

        if prepare_move_line['period_id']:
            v_period_id=prepare_move_line['period_id']
        else:
            v_period_id='''null'''

        if prepare_move_line['move_id']:
            v_move_id=prepare_move_line['move_id']
        else:
            v_move_id='''null'''

        if prepare_move_line['name']:
            v_name=prepare_move_line['name']
        else:
            v_name='''null'''

        v_credit=prepare_move_line['credit']
        v_debit=prepare_move_line['debit']
        v_paylines_total=v_debit - v_credit

        if prepare_move_line['amount_currency']:
            v_amount_currency=prepare_move_line['amount_currency']
        else:
            v_amount_currency=0


        
        insert_sql=(('''insert into account_move_line 
                        (
                            create_uid,write_uid,create_date,write_date,date_created,date,currency_id,partner_id,
                            journal_id,account_id,period_id,move_id,credit,debit,name,amount_currency,blocked,
                            centralisation,state,not_move_diot,is_tax_voucher,company_id,ref,cost_center_id,date_maturity
                        ) 
                        values (
                                %s,%s,now() at time zone 'UTC',now() at time zone 'UTC',CURRENT_DATE,%s,%s,%s,
                                %s,%s,%s,%s,'%s','%s','%s','%s',False,'normal','valid',False,False,%s,'%s',%s,%s
                                ) 
                        RETURNING id;
                    ''')% (
                            v_create_uid,v_write_uid,v_date,v_currency_id,v_partner_id,v_journal_id,v_account_id,v_period_id,
                            v_move_id,v_credit,v_debit,v_name,v_amount_currency,v_company_id,v_ref,v_cost_center_id,v_date_maturity
                    ))
        #print(insert_sql)
        self.env.cr.execute(insert_sql)
        insert_id=self.env.cr.fetchall()
        move_line=insert_id[0][0]
        #print(move_line)
        """
        move_line = move_lines.create(
            self.prepare_move_line(
                voucher, amount, move_id, name, company_currency,
                current_currency, payment_date, account, partner)
                )
        
        return move_line
        """
        return v_paylines_total

    @api.model
    def prepare_move_line(
            self, voucher, amount, move_id, name, company_currency,
            current_currency, date_due, account, partner):
        """
        Function that will be use to create first move line and can be used to
        add every payline you add in your custom module.
        """
        debit = credit = 0.0
        if voucher.type in ('purchase', 'payment'):
            credit = self._convert_amount(amount, voucher.id)
        elif voucher.type in ('sale', 'receipt'):
            debit = self._convert_amount(amount, voucher.id)
        if debit < 0: credit = -debit; debit = 0.0
        if credit < 0: debit = -credit; credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        move_line = {
                'name': name,
                'debit': debit,
                'credit': credit,
                'account_id': account.id,
                'partner_id': partner.id,
                'move_id': move_id,
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'currency_id': company_currency != current_currency and  current_currency or False,
                'amount_currency': (sign * abs(amount) # amount < 0 for refunds
                    if company_currency != current_currency else 0.0),
                'date': voucher.date,
                'date_maturity': date_due or False,
            }
        return move_line

    def action_move_line_create(self, cr, uid, ids, context=None):
        #print ('Pago 1')
        #raise osv.except_osv(_('Error!'),_("Aqui pago 1"))
        '''
        We overwrite this function in order to give the posibility of adding
        paylines. We mark with # CHANGE where we change the code.
        ORIGINAL DESCRIPTION:
        Confirm the vouchers given in ids and create the journal entries for
        each of them
        '''
        if context is None:
            context = {}
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')

        #FUNCION voucher_move_line_tax_create
        bank_statement_line_obj = self.pool.get('account.bank.statement.line')
        #move_line_obj = self.pool.get('account.move.line')
        cur_obj = self.pool.get('res.currency')
        object_dp = self.pool.get('decimal.precision')
        round_val = object_dp.precision_get(cr, uid, 'Account')
        #company_currency = self._get_company_currency(cr, uid, voucher_id, context)
        #current_currency = self._get_current_currency(cr, uid, voucher_id, context)
        move_ids = []
        move_reconcile_id = []
        context = dict(context)
        amount_tax_currency = 0.0
        taxes_lines = True

        #FUNCION voucher_move_line_tax_create

        for voucher in self.browse(cr, uid, ids, context=context):
            local_context = dict(
                context, force_company=voucher.journal_id.company_id.id)
            if voucher.move_id:
                continue
            company_currency = self._get_company_currency(
                cr, uid, voucher.id, context)
            current_currency = self._get_current_currency(
                cr, uid, voucher.id, context)
            rate_customer = cur_obj.browse(cr,
                                           uid,
                                           voucher.payment_rate_currency_id.id,
                                           context=context
                                           ).rate_sale
            rate_supplier = cur_obj.browse(cr,
                                           uid,
                                           voucher.payment_rate_currency_id.id,
                                           context=context
                                           ).rate

            # we select the context to use accordingly if it's a multicurrency
            # case or not
            context = self._sel_context(cr, uid, voucher.id, context)
            # But for the operations made by _convert_amount, we always need to
            # give the date in the context
            ctx = context.copy()
            ctx.update({'date': voucher.date})
            # Create the account move record.
            move_id = move_pool.create(cr, uid, self.account_move_get(
                cr, uid, voucher.id, context=context), context=context)
            # Get the name of the account_move just created
            move_pool_obj=move_pool.browse(cr, uid, move_id, context=context)
            name = move_pool_obj.name
            v_ref=move_pool_obj.ref

            # CHANGE
            # COMENTADO
            # Create the first line of the voucher
            # move_line_id = move_line_pool.create(cr, uid, self.first_move_line_get(
            #     cr, uid, voucher.id, move_id, company_currency, current_currency, local_context), local_context)
            # move_line_brw = move_line_pool.browse(
            #     cr, uid, move_line_id, context=context)
            # line_total = move_line_brw.debit - move_line_brw.credit
            # END COMENTADO
            # AGREGADO
            # if voucher.type in ('payment', 'receipt'):
            # Create move line for echa payline
            # for payline in voucher.payline_ids:
            line_total = self.paylines_moves_create(
                cr, uid, voucher, move_id, company_currency,
                current_currency, context)
            # END AGREGADO
            # END CHANGE
            rec_list_ids = []
            if voucher.type == 'sale':
                line_total = line_total - \
                    self._convert_amount(
                        cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            elif voucher.type == 'purchase':
                line_total = line_total + \
                    self._convert_amount(
                        cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            # Create one move line per voucher line where amount is not 0.0
            line_total, rec_list_ids = self.voucher_move_line_create(
                cr, uid, voucher.id, line_total, move_id, company_currency, current_currency, context)
            if line_total != 0.0 and voucher.currency_id.rounding >= float_round(
                    line_total, precision_rounding=voucher.currency_id.rounding) >= voucher.currency_id.rounding * -1 or \
                    self.total_payment(voucher, company_currency, current_currency):
                move_id_obj = move_pool_obj
                #move_pool.browse(cr, uid, [move_id])
                line_total = 0.0
                _total = 0.0
                _total_credit = 0.0
                _total_debit = 0.0
                amount_to_update = 0.0
                line_id_obj = False
                for line in move_id_obj.line_id:
                    if line.name == u'/':
                        _total_credit += line.credit
                        _total_debit += line.debit
                        _total += line.credit - line.debit
                if move_id_obj.line_id:
                    line_id_obj = move_id_obj.line_id[-1]
                _new_field_amount = 0.0
                if line_id_obj and float_round(_total, precision_rounding=voucher.currency_id.rounding) != 0.0:
                    if line_id_obj.credit:
                        _field = 'credit'
                        _field_amount = _total_credit
                        _new_field_amount = line_id_obj.credit
                        _field_amount_counterpart = _total_debit
                    else:
                        _field = 'debit'
                        _field_amount = _total_debit
                        _new_field_amount = line_id_obj.debit
                        _field_amount_counterpart = _total_credit
                    _res_more = float_round(_field_amount + _total, 2)
                    _res_minus = float_round(_field_amount - _total, 2)
                    if _res_more == float_round(_field_amount_counterpart, 2):
                        amount_to_update = _new_field_amount + _total
                    elif _res_minus == float_round(_field_amount_counterpart, 2):
                        amount_to_update = _new_field_amount - _total
                    if amount_to_update:
                        line_id_obj.write({_field: amount_to_update})

            # Create the writeoff line if needed
            ml_writeoff = self.writeoff_move_line_get(
                cr, uid, voucher.id, line_total, move_id, name, company_currency, current_currency, local_context)
            if ml_writeoff:
                move_line_pool.create(cr, uid, ml_writeoff, local_context)
            # We post the voucher.
            self.write(cr, uid, [voucher.id], {
                'move_id': move_id,
                'state': 'posted',
                'number': name,
            })
            if voucher.journal_id.entry_posted:
                move_pool.post(cr, uid, [move_id], context={})
            # We automatically reconcile the account move lines.
            #print ('Pago 4')
            #raise osv.except_osv(_('Error!'),_("Aqui pago 4"))


            
            #INICIO FUNCIONALIDAD DE LA FUNCION voucher_move_line_tax_create

            context.update({'amount_voucher': voucher.amount or 0.0})
            # Verificamos si la aplicacion del diario esta configurada
            # como nota de credito, las notas de credito NO deben generar
            # asieto de impuesto en la poliza de la aplicacion y tampoco
            # se asiento de las polizas que se esten saldando.
            if (voucher.journal_id.account_account_apply_id.code or 'None')\
                    in (u'refund'):
                # Se invalida la generacion de lineas de impuesto si es nota
                # de credito.
                taxes_lines = False
            if not voucher.journal_id.account_account_apply_id.generate_taxes:
                taxes_lines = False

            for line in voucher.line_ids:
                if not line.amount:
                    continue

                factor = self.get_percent_pay_vs_invoice(
                    cr, uid, line.amount_original, line.amount,
                    context=context)

                move = line.move_line_id
                mv_line_dicts = [{
                    'counterpart_move_line_id': move.id,
                    'credit': move.debit,
                    'debit': move.credit,
                    'name': move.name}]

                bank_statement_line_obj._get_factor_type(
                    cr, uid, False, voucher.type, context=context)

                context['journal_special'] = voucher.journal_id.special_journal
                context['todo_currency_rate_alter'] = line.voucher_id.currency_rate_alter
                context['todo_voucher_id'] = voucher.id

                dict_tax = dict()
                if taxes_lines:
                    dict_tax = bank_statement_line_obj._get_move_line_tax(
                        cr, uid, mv_line_dicts, context=context)

                for move_line_tax_dict in dict_tax:
                    #print('Parte 5.4.1 voucher_move_line_tax_create')
                    line_tax_id = move_line_tax_dict.get('tax_id')
                    amount_base_secondary =\
                        line_tax_id.amount and\
                        line.amount_original / (1 + line_tax_id.amount) or\
                        move_line_tax_dict.get('amount_base_secondary')
                    account_tax_voucher =\
                        move_line_tax_dict.get('account_tax_voucher')
                    account_tax_collected =\
                        move_line_tax_dict.get('account_tax_collected')
                    amount_total_tax = move_line_tax_dict.get('amount', 0)

                    move_line_rec = []

                    context['date'] = voucher.date
                    reference_amount = amount_total_tax * abs(factor)
                    if rate_customer <= 0:
                        raise osv.except_osv(_('Wrong exchange rate'),
                                             _("The exchange rate for customer \
                                               should be bigger than zero.\n \
                                               Please review currency configuration."))
                    if rate_supplier <= 0:
                        raise osv.except_osv(_('Wrong exchange rate'),
                                             _("The exchange rate for supplier \
                                               should be bigger than zero.\n \
                                               Please review currency configuration."))
                    statement_currency_line = False
                    if current_currency != line.currency_id.id:
                        statement_currency_line = line.currency_id.id

                    if (current_currency != company_currency
                            or statement_currency_line):
                        amount_tax_currency += cur_obj.compute(
                            cr, uid,
                            statement_currency_line or current_currency,
                            company_currency,
                            reference_amount, context=context)
                    else:
                        amount_tax_currency += round(
                            reference_amount, round_val)

                    move_lines_tax = self._preparate_move_line_tax(
                        cr, uid, account_tax_voucher, account_tax_collected,
                        move_id, voucher.type, voucher.partner_id.id,
                        voucher.period_id.id, voucher.journal_id.id,
                        voucher.date, company_currency, reference_amount,
                        reference_amount, current_currency, False,
                        move_line_tax_dict.get('tax_id'),
                        move_line_tax_dict.get('tax_analytic_id'),
                        amount_base_secondary, factor,
                        statement_currency_line=statement_currency_line,
                        context=context, voucher_id=voucher.id)

                    for move_line_tax in move_lines_tax:
                        #print('creando detalles poliza')
                        
                        v_create_uid=uid
                        v_write_uid=uid

                        if move_line_tax['date']:
                            v_date=move_line_tax['date']
                        else:
                            v_date='''null'''

                        v_is_tax_voucher=move_line_tax['is_tax_voucher']

                        v_company_id='''null'''
                        if move_line_tax['account_id']:
                            v_account_id=move_line_tax['account_id']
                            v_company_obj=self.pool('account.account').browse(cr, uid, move_line_tax['account_id'], context=context).company_id
                            if v_company_obj:
                                v_company_id=v_company_obj.id
                        else:
                            v_account_id='''null'''


                        if move_line_tax['period_id']:
                            v_period_id=move_line_tax['period_id']
                        else:
                            v_period_id='''null'''

                        v_amount_base=move_line_tax['amount_base']

                        if move_line_tax['partner_id']:
                            v_partner_id=move_line_tax['partner_id']
                        else:
                            v_partner_id='''null'''

                        if move_line_tax['move_id']:
                            v_move_id=move_line_tax['move_id']
                        else:
                            v_move_id='''null'''
                        
                        if move_line_tax['tax_id']:
                            v_tax_id=move_line_tax['tax_id']
                        else:
                            v_tax_id='''null'''
                        
                        v_analytic_account_id='''null'''
                        if "analytic_account_id" in move_line_tax:
                            if move_line_tax['analytic_account_id']:
                                v_analytic_account_id=move_line_tax['analytic_account_id']
    
                        if move_line_tax['name']:
                            v_name=move_line_tax['name']
                        else:
                            v_name='''null'''

                        if move_line_tax['journal_id']:
                            v_journal_id=move_line_tax['journal_id']
                        else:
                            v_journal_id='''null'''

                        v_amount_tax_unround=move_line_tax['amount_tax_unround']
                        v_credit=move_line_tax['credit']
                        v_debit=move_line_tax['debit']

                        if move_line_tax['tax_voucher_id']:
                            v_tax_voucher_id=move_line_tax['tax_voucher_id']
                        else:
                            v_tax_voucher_id='''null'''

                        v_quantity=move_line_tax['quantity']

                        v_currency_id='''null'''
                        if "currency_id" in move_line_tax:
                            if move_line_tax['currency_id']:
                                v_currency_id=move_line_tax['currency_id']

                        v_amount_currency=0
                        if "amount_currency" in move_line_tax:
                            if move_line_tax['amount_currency']:
                                v_amount_currency=move_line_tax['amount_currency']

                        
                        #import pdb; pdb.set_trace()
                        insert_sql=(('''insert into account_move_line (
                                                                        create_uid,write_uid,write_date,create_date,date_created,date,
                                                                        is_tax_voucher,account_id,period_id,amount_base,partner_id,move_id,
                                                                        tax_id,analytic_account_id,name,journal_id,amount_tax_unround,credit,debit,
                                                                        tax_voucher_id,quantity,not_move_diot,ref,company_id,blocked,centralisation,state,
                                                                        currency_id,amount_currency
                                        ) 
                                        values (
                                                %s,%s,now() at time zone 'UTC',now() at time zone 'UTC',CURRENT_DATE,'%s',%s,
                                                %s,%s,'%s',%s,%s,%s,%s,'%s',%s,'%s','%s','%s',%s,'%s',False,'%s',%s,False,'normal','valid',
                                                %s,'%s'
                                                ) 
                                        RETURNING id;
                                    ''')% (
                                            v_create_uid,v_write_uid,v_date,v_is_tax_voucher,v_account_id,v_period_id,v_amount_base,v_partner_id,v_move_id,v_tax_id,
                                            v_analytic_account_id,v_name,v_journal_id,v_amount_tax_unround,v_credit,v_debit,v_tax_voucher_id,v_quantity,v_ref,v_company_id,
                                            v_currency_id,v_amount_currency
                                    ))
                        #print(insert_sql)
                        cr.execute(insert_sql)
                        insert_id=cr.fetchall()
                        move_create=insert_id[0][0]
                        #print(move_create)
                        move_ids.append(move_create)
                        move_line_rec.append(move_create)

                    move_rec_exch = bank_statement_line_obj.\
                        _get_exchange_reconcile(
                            cr, uid, move_line_tax_dict, move_line_rec,
                            line.amount, line.amount_unreconciled,
                            voucher, company_currency,
                            current_currency, context=context)
                    move_reconcile_id.append(move_rec_exch[1])

            if voucher.journal_id.special_journal:
                move_line_writeoff_tax = self.writeoff_move_line_tax_get(
                    cr, uid, voucher, amount_tax_currency, move_id,
                    voucher.number, company_currency, current_currency,
                    move_reconcile_id, context=context)
                if move_line_writeoff_tax:
                    move_line_pool.create(
                        cr, uid, move_line_writeoff_tax, context=context)

            #FIN FUNCIONALIDAD DE LA FUNCION voucher_move_line_tax_create


            #RECORRE DETALLES FACTURAS
            reconcile = False
            for rec_ids in rec_list_ids:
                if len(rec_ids) >= 2:
                    ctx.update({'with_writeoff': False if voucher.payment_option == u'without_writeoff' else True,
                                'fix_writeoff_amounts': True,
                                'todo_ttype': voucher.type,
                                'todo_writeoff_amount': voucher.writeoff_amount,
                                'company_currency_id': company_currency,
                                'currency_id': current_currency})
                    reconcile = move_line_pool.reconcile_partial(
                        cr, uid, rec_ids, writeoff_acc_id=voucher.writeoff_acc_id.id, writeoff_period_id=voucher.period_id.id, writeoff_journal_id=voucher.journal_id.id, context=ctx)
            #print ('Pago 5')

        #RECORRE DETALLES IMPUESTOS
        #voucher_move_line_tax_create
        for rec_ids_tax in move_reconcile_id:
            if len(rec_ids_tax) >= 2:
                move_line_pool.reconcile_partial(cr, uid, rec_ids_tax)
        #voucher_move_line_tax_create
        return True

    def total_payment(self, voucher, company_currency, current_currency):
        if company_currency != current_currency:
            if voucher.writeoff_amount == 0.0:
                return True
            else:
                return False
        else:
            return False
