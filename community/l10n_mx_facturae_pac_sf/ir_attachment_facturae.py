# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Copyright (c) 2010 Vauxoo - http://www.vauxoo.com/
#    All Rights Reserved.
#    info Vauxoo (info@vauxoo.com)
############################################################################
#    Coded by: moylop260 (moylop260@vauxoo.com)
#    Launchpad Project Manager for Publication: Nhomar Hernandez - nhomar@vauxoo.com
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import urllib
import httplib, urllib
import re
import md5
import requests
from openerp.tools.translate import _
from openerp.osv import fields, osv, orm
from openerp import tools
from openerp import netsvc
from openerp.tools.misc import ustr
import wizard
import base64
import xml.dom.minidom
import time
import StringIO
import uuid
import tempfile
import os
import sys
import codecs
from xml.dom import minidom
import xml.dom.minidom
import urllib
from openerp.tools.translate import _
from datetime import datetime, timedelta
from pytz import timezone
import hashlib
import time
from openerp import tools
import logging
_logger = logging.getLogger(__name__)
try:
    from SOAPpy import WSDL
except:
    _logger.warning('Install Package SOAPpy with the command "sudo apt-get install python-soappy".')

class ir_attachment_facturae_mx(osv.Model):
    _inherit = 'ir.attachment.facturae.mx'

    def _get_type(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        _types = super(ir_attachment_facturae_mx, self)._get_type(cr, uid, ids, context=context)
        _types.extend([
            ('cfdi32_pac_sf', 'CFDI 3.2 Solución Factible'),

        ])
        return _types

    #pac diverza
    def get_driver_fc_sign(self):
        factura_mx_type__fc = {}
        try:
            factura_mx_type__fc = super(ir_attachment_facturae_mx, self).get_driver_fc_sign()
            if factura_mx_type__fc == None:
                factura_mx_type__fc = {}
            factura_mx_type__fc.update({'cfdi32_pac_sf': self._upload_ws_file})
        except:
            None
        return factura_mx_type__fc
    
    #pac solucion factible
    def get_driver_fc_sign_sf(self):
       factura_mx_type__fc = {}
       try:
           factura_mx_type__fc = super(ir_attachment_facturae_mx, self).get_driver_fc_sign_sf()
           if factura_mx_type__fc == None:
               factura_mx_type__fc = {}
           factura_mx_type__fc.update({'cfdi32_pac_sf': self._upload_ws_file_sf})
       except:
           None
       return factura_mx_type__fc
    
    def get_driver_fc_cancel(self):
        factura_mx_type__fc = super(ir_attachment_facturae_mx, self).get_driver_fc_cancel()
        if factura_mx_type__fc == None:
            factura_mx_type__fc = {}
        factura_mx_type__fc.update({'cfdi32_pac_sf': self.sf_cancel})
        return factura_mx_type__fc
    
    #cancelacion diverza
    def get_driver_div_cancel(self):
      factura_mx_type__fc = super(ir_attachment_facturae_mx, self).get_driver_div_cancel()
      if factura_mx_type__fc == None:
          factura_mx_type__fc = {}
      factura_mx_type__fc.update({'cfdi32_pac_sf': self.div_cancel})
      return factura_mx_type__fc
    
    _columns = {
        'type': fields.selection(_get_type, 'Type', type='char', size=64,
                                 required=True, readonly=True, help="Type of Electronic Invoice"),
    }
    
    #cancelar pac diverza
    #cancelar pac diverza
    def div_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        msg = ''
        certificate_obj = self.pool.get('res.company.facturae.certificate')
        pac_params_obj = self.pool.get('params.pac')
        invoice_obj = self.pool.get('account.invoice')
        for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
            status = False
            invoice = ir_attachment_facturae_mx_id.invoice_id

            qry = """
                           SELECT id FROM params_pac
                                             where active=True and method_type ='cancel'
                                             AND company_id = %s
                                             ORDER BY sequence DESC LIMIT 1
                       """ % (invoice.company_emitter_id.id)
            cr.execute(qry)
            if cr.rowcount:
                pac_ids = cr.fetchone()
                file_globals = invoice_obj._get_file_globals(cr, uid, [invoice.id], context=context)
                pac_params_brw = pac_params_obj.browse(cr, uid, pac_ids, context=context)[0]
                rfc_emitter = invoice.company_id.address_invoice_parent_company_id.vat[2:]

                token = pac_params_brw.password
                wsdl_url = pac_params_brw.url_webservice

                wsdl_client = False
                headers = {"x-auth-token": token}
                fname_cer_no_pem = file_globals['fname_cer']
                cerCSD = fname_cer_no_pem and base64.encodestring(open(fname_cer_no_pem, "r").read()) or ''
                fname_key_no_pem = file_globals['fname_key']
                keyCSD = fname_key_no_pem and base64.encodestring(open(fname_key_no_pem, "r").read()) or ''
                zip = False  # Validar si es un comprimido zip, con la extension del archivo
                contrasenaCSD = file_globals.get('password', '')
                uuids = invoice.cfdi_folio_fiscal  # cfdi_folio_fiscal

                url_request = wsdl_url + '/' + rfc_emitter + '/' + uuids
                # print  url_request
                headers = {"x-auth-token": token}
                resource_request = requests.post(url=url_request, headers=headers)

                response_msg = str(resource_request.status_code) + " / " + resource_request.reason
                status_uuid = resource_request.reason

                if resource_request.status_code == 200:
                    msg = _('\n- The process of cancellation\
                    has completed correctly.')
                    invoice_obj.write(cr, uid, [invoice.id], {
                        'cfdi_fecha_cancelacion': time.strftime(
                        '%Y-%m-%d %H:%M:%S')
                    })
                    status = True
                    #raise orm.except_orm(_('Warning'), _(str(msg)))
                elif 'No es posible cancelar el comprobante, el comprobante ya fue cancelado, previamente' in str(response_msg):
                    status = True
                    #raise orm.except_orm(_('Warning'), _(str('Cancelado previmante ----- :)')))
                else:
                    # raise osv.except_osv(_('Error'), _(str(response_msg)))
                    raise orm.except_orm(_('Warning'), _(str(response_msg)))
            else:
                msg = _('Error')
        return {'message': msg, 'status_uuid': status_uuid, 'status': status}
    
    def sf_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        msg = ''
        certificate_obj = self.pool.get('res.company.facturae.certificate')
        pac_params_obj = self.pool.get('params.pac')
        invoice_obj = self.pool.get('account.invoice')
        for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
            status = False
            invoice = ir_attachment_facturae_mx_id.invoice_id
            pac_params_ids = pac_params_obj.search(cr, uid, [
                ('method_type', '=', 'pac_sf_cancelar'),
                ('company_id', '=', invoice.company_emitter_id.id),
                ('active', '=', True),
            ], limit=1, context=context)
            pac_params_id = pac_params_ids and pac_params_ids[0] or False
            if pac_params_id:
                file_globals = invoice_obj._get_file_globals(
                    cr, uid, [invoice.id], context=context)
                pac_params_brw = pac_params_obj.browse(
                    cr, uid, [pac_params_id], context=context)[0]
                user = pac_params_brw.user
                password = pac_params_brw.password
                wsdl_url = pac_params_brw.url_webservice
                namespace = pac_params_brw.namespace
                wsdl_client = False
                wsdl_client = WSDL.SOAPProxy(wsdl_url, namespace)
                fname_cer_no_pem = file_globals['fname_cer']
                cerCSD = fname_cer_no_pem and base64.encodestring(
                    open(fname_cer_no_pem, "r").read()) or ''
                fname_key_no_pem = file_globals['fname_key']
                keyCSD = fname_key_no_pem and base64.encodestring(
                    open(fname_key_no_pem, "r").read()) or ''
                zip = False  # Validar si es un comprimido zip, con la extension del archivo
                contrasenaCSD = file_globals.get('password', '')
                uuids = invoice.cfdi_folio_fiscal  # cfdi_folio_fiscal
                params = [
                    user, password, uuids, cerCSD, keyCSD, contrasenaCSD]
                wsdl_client.soapproxy.config.dumpSOAPOut = 0
                wsdl_client.soapproxy.config.dumpSOAPIn = 0
                wsdl_client.soapproxy.config.debug = 0
                wsdl_client.soapproxy.config.dict_encoding = 'UTF-8'
                result = wsdl_client.cancelar(*params)
                codigo_cancel = result['status'] or ''
                status_cancel = result['resultados'] and result[
                    'resultados']['status'] or ''
                uuid_nvo = result['resultados'] and result[
                    'resultados']['uuid'] or ''
                mensaje_cancel = _(tools.ustr(result['mensaje']))
                msg_nvo = result['resultados'] and result[
                    'resultados']['mensaje'] or ''
                status_uuid = result['resultados'] and result[
                    'resultados']['statusUUID'] or ''
                folio_cancel = result['resultados'] and result[
                    'resultados']['uuid'] or ''
                if codigo_cancel == '200' and status_cancel == '200' and\
                        status_uuid == '201':
                    msg +=  mensaje_cancel + _('\n- The process of cancellation\
                    has completed correctly.\n- The uuid cancelled is:\
                    ') + folio_cancel
                    invoice_obj.write(cr, uid, [invoice.id], {
                        'cfdi_fecha_cancelacion': time.strftime(
                        '%Y-%m-%d %H:%M:%S')
                    })
                    status = True
                else:
                    raise orm.except_orm(_('Warning'), _('Cancel Code: %s.-Status code %s.-Status UUID: %s.-Folio Cancel: %s.-Cancel Message: %s.-Answer Message: %s.') % (
                        codigo_cancel, status_cancel, status_uuid, folio_cancel, mensaje_cancel, msg_nvo))
            else:
                msg = _('Not found information of webservices of PAC, verify that the configuration of PAC is correct')
        return {'message': msg, 'status_uuid': status_uuid, 'status': status}
    
   
     #timbre Pac diverza

    def _upload_ws_file(self, cr, uid, ids, fdata=None, context=None):
        '''

        :param cr:
        :param uid:
        :param ids:
        :param fdata: File.xml codification in base64
        :param context:
        :return:
        '''
        if context is None:
            context = {}
        invoice_obj = self.pool.get('account.invoice')
        pac_params_obj = invoice_obj.pool.get('params.pac')
        for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
            invoice = ir_attachment_facturae_mx_id.invoice_id
            comprobante = invoice_obj._get_type_sequence(
                cr, uid, [invoice.id], context=context)
            cfd_data = base64.decodestring(fdata or invoice_obj.fdata)
            xml_res_str = xml.dom.minidom.parseString(cfd_data)
            #xml_aux##########################################################################
            xmldoc = xml_res_str = xml.dom.minidom.parseString(cfd_data)
            ###################################################################################
            xml_res_addenda = invoice_obj.add_addenta_xml(
                cr, uid, xml_res_str, comprobante, context=context)
            xml_res_str_addenda = xml_res_addenda.toxml('UTF-8')
            xml_res_str_addenda = xml_res_str_addenda.replace(codecs.BOM_UTF8, '')
            xml_res_str_addenda = xml_res_str_addenda.replace('\n', '')
            if tools.config['test_report_directory']:#TODO: Add if test-enabled:
                ir_attach_facturae_mx_file_input = ir_attachment_facturae_mx_id.file_input and ir_attachment_facturae_mx_id.file_input or False
                fname_suffix = ir_attach_facturae_mx_file_input and ir_attach_facturae_mx_file_input.datas_fname or ''
                open( os.path.join(tools.config['test_report_directory'], 'l10n_mx_facturae_pac_div' + '_' + \
                  'before_upload' + '-' + fname_suffix), 'wb+').write( xml_res_str_addenda )
            compr = xml_res_addenda.getElementsByTagName(comprobante)[0]
            date = compr.attributes['fecha'].value
            date_format = datetime.strptime(
                date, '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d')
            context['date'] = date_format
            invoice_ids = [invoice.id]
            file = False
            msg = ''
            cfdi_xml = False
            #buscar parametros del pac 
            # pac_params_ids = pac_params_obj.search(cr, uid, [
            #     ('method_type', '=', 'sign'), (
            #         'company_id', '=', invoice.company_emitter_id.id), (
            #             'active', '=', True)], limit=1, context=context)
            qry= """
                SELECT id FROM params_pac
                                  where active=True and method_type ='sign'
                                  AND company_id = %s
                                  ORDER BY sequence DESC LIMIT 1
            """ % (invoice.company_emitter_id.id)
            cr.execute(qry)
            if cr.rowcount:
                pac_params = pac_params_obj.browse(cr, uid, cr.fetchone(), context)[0]
                #contrasenia
                token = pac_params.password
                #url_webservice
                wsdl_url = pac_params.url_webservice
                if wsdl_url:
                    file_globals = invoice_obj._get_file_globals(
                        cr, uid, invoice_ids, context=context)
                    fname_cer_no_pem = file_globals['fname_cer']
                    cerCSD = fname_cer_no_pem and base64.encodestring(
                        open(fname_cer_no_pem, "r").read()) or ''
                    fname_key_no_pem = file_globals['fname_key']
                    keyCSD = fname_key_no_pem and base64.encodestring(
                        open(fname_key_no_pem, "r").read()) or ''
                    cfdi = xml_res_str_addenda
                    zip = False  # Validar si es un comprimido zip, con la extension del archivo
                    contrasenaCSD = file_globals.get('password', '')
                    _md5 = hashlib.md5()
                    _md5.update(xml_res_str_addenda)
                    strXmlEnMD5 = _md5.digest()
                    oHeaders = {
                        "x-auth-token":token,
                        # "Content-MD5":strXmlEnMD5,
                        "Content-Type": "application/vnd.sat.cfdi+xml;charset=UTF-8"
                    }
                    print oHeaders
                    resultado = requests.post(url=wsdl_url, data = cfdi,  headers = oHeaders)
                    response_code = str(resultado.status_code)
                    cfdi_xml = resultado.text
                    htz = int(invoice_obj._get_time_zone(
                        cr, uid, [ir_attachment_facturae_mx_id.invoice_id.id], context=context))
                    if resultado.status_code != 200:
                        raise osv.except_osv(_('Warning'), self.status_code_diverza(resultado.status_code))
                    else:
                        file = base64.encodestring(cfdi_xml or '')
                        result = []
                        pattern = '([A-Za-z]*=".*?")'
                        string = ['selloSAT','noCertificadoSAT','FechaTimbrado','UUID']
                        res = re.findall(pattern, cfdi_xml)
                        for o in string:
                            result.append(''.join([s for s in res if o in s]).replace(o+'=','').replace('"',''))
							
                        if cfdi_xml:
							#Creamos el objeto xml a partir de la respuesta de diverza
                            cfdiTimbrado = xml.dom.minidom.parseString(cfdi_xml)
                            #Creamos el archivo fisico para crear la cadena original en la misma ruta de este archivo
                            rutaArchivo = "/tmp/"
                            rutaArchivoXml = rutaArchivo+"xml"+str(uuid.uuid4().hex)+"archivo_cfdi.xml"
							
                            fp = open(rutaArchivoXml, 'w')
                            cfdiTimbrado.writexml(fp)
                            fp.close()
                            _pwd_file =os.path.dirname(os.path.realpath(__file__))
							#Se ejecuta xsltproc en la terminal de linux para generar la cadena original
                            cadenaOriginal = os.popen("xsltproc "+_pwd_file+"/cadenaoriginal_TFD_1_0.xslt "+rutaArchivoXml).read()
							#Se elimina el archivo fisico que contiene el cfdi
                            os.system("rm "+rutaArchivoXml)
						
                            cfdi_data = {
                                'cfdi_sello': result[0] or False,
                                'cfdi_no_certificado': result[1] or False,
                                'cfdi_fecha_timbrado': result[2],
                                'cfdi_xml': base64.decodestring(cfdi_xml or ''),
                                'cfdi_folio_fiscal': result[3] or '',
                                'pac_id': pac_params.id,
                                'cfdi_cadena_original' : cadenaOriginal
                            }
                        
                            invoice_obj.write(cr, uid, [invoice.id], cfdi_data)                            
                            sSello = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['sello'].value
                            sSelloCfd = cfdiTimbrado.getElementsByTagName('tfd:TimbreFiscalDigital')[0].attributes['selloCFD'].value
                            
                            if sSello != sSelloCfd:
                                cfdi_xml = False
                                msg = 'Problemas al validar el timbre'
                        else:
                            msg += _(u"Can't extract the file XML of PAC")
        #if file == True:
        return {'file': file, 'msg': msg, 'cfdi_xml': cfdi_xml}

    def status_code_diverza(self, code):
        _error_code = {
            440: '-CFDI no válido, no fue posible validar el XML contra esquema.',
            441: '- No se logró obtener el número de certificado.',
            442: '- No es un emisor válido.',
            443: '- XML no válido según esquema, la estructura del comprobante no corresponde a lo definido por la regulación del SAT.',
            444: '- El RFC el emisor no corresponde al RFC del CSD.',
            445: ' - El CSD se encuentra en la lista de revocación del SAT.',
            446: ' - El CSD aún no se encuentra en la lista de certificados del SAT.',
            447: ' - El Comprobante fue firmado con una FIEL.',
            448: ' - EL RFC del emisor no existe conforme al régimen autorizado (Lista de validación de régimen) LCO.',
            449: ' - Han pasado más de 72 horas desde la creación del comprobante.',
            450: ' - Timbre generado previamente.',
            451: ' - No se encontró un comprobante.',
            452: ' - CFDI timbrado previamente.',
            453: ' - Namespace o prefijo utilizado en complemento no es válido.',
            455: ' - Fecha de generación de CFDI fuera de rango. 72 Hrs.',
            456: ' - El RFC emisor del CFDI no corresponde al RFC del CSD.',
            457: ' - El CFDI fue emitido fuera de la vigencia del CSD.',
            458: ' - El número de certificado del CFDI no corresponde al número de serie del certificado de sello.',
            459: ' - El certificado de sello no fue emitido por una autoridad certificadora válida (SAT).',
            460: ' - El sello del comprobante no es válido.',
            461: ' - CFDI no válido, el xml recibido no es Comprobante Fiscal Digital.',
            462: ' - El prefijo cfdi es requerido.',
            463: ' - Error al eliminar addenda del comprobante.',
            464: ' - SchemaLocation no válido para la versión del comprobante.',
            465: ' - Versión de comprobante no válida, a partir del 1ero De Julio 2012 solo se podrá timbrar Comprobantes versión 3.2.',
            466: ' - Carácter no válido {invalidCharacter}, el sello del comprobante no debe contener secuencias de escape.',
            467: ' - Elemento {elemento} es requerido.',
            468: ' - No se logró obtener los elementos requeridos para el timbrado del comprobante.',
            469: ' - El namespace del elemento principal no es válido.',
            470: ' - Timbrado previamente.',
            471: ' - Se ha suspendido el servicio.',
            540: ' - Error al almacenar Comprobante en BD.',
            541: ' - No se logró realizar el firmado del timbre, intente más tarde.',
            542: ' - Timbre fiscal no logro ser procesado E. {errorConsole}',
            543: ' - No es posible leer el comprobante recibido en Base64, el Comprobante no es válido.'
        }
        if code in _error_code:
            return unicode("codigo: %s mesaje:%s" % (code, _error_code[code])).decode('utf8')
        else:
            return "codigo: %s " % (code, )

    def _upload_ws_file_sf(self, cr, uid, ids, fdata=None, context=None):
        """
        @params fdata : File.xml codification in base64
        """
        if context is None:
            context = {}
        invoice_obj = self.pool.get('account.invoice')
        pac_params_obj = invoice_obj.pool.get('params.pac')
        for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
            invoice = ir_attachment_facturae_mx_id.invoice_id
            comprobante = invoice_obj._get_type_sequence(
                cr, uid, [invoice.id], context=context)
            cfd_data = base64.decodestring(fdata or invoice_obj.fdata)
            xml_res_str = xml.dom.minidom.parseString(cfd_data)
            xml_res_addenda = invoice_obj.add_addenta_xml(
                cr, uid, xml_res_str, comprobante, context=context)
            xml_res_str_addenda = xml_res_addenda.toxml('UTF-8')
            xml_res_str_addenda = xml_res_str_addenda.replace(codecs.BOM_UTF8, '')
            
            if tools.config['test_report_directory']:#TODO: Add if test-enabled:
                ir_attach_facturae_mx_file_input = ir_attachment_facturae_mx_id.file_input and ir_attachment_facturae_mx_id.file_input or False
                fname_suffix = ir_attach_facturae_mx_file_input and ir_attach_facturae_mx_file_input.datas_fname or ''
                open( os.path.join(tools.config['test_report_directory'], 'l10n_mx_facturae_pac_sf' + '_' + \
                  'before_upload' + '-' + fname_suffix), 'wb+').write( xml_res_str_addenda )
            compr = xml_res_addenda.getElementsByTagName(comprobante)[0]
            date = compr.attributes['fecha'].value
            date_format = datetime.strptime(
                date, '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d')
            context['date'] = date_format
            invoice_ids = [invoice.id]
            file = False
            msg = ''
            cfdi_xml = False
            pac_params_ids = pac_params_obj.search(cr, uid, [
                ('method_type', '=', 'pac_sf_firmar'), (
                    'company_id', '=', invoice.company_emitter_id.id), (
                        'active', '=', True)], limit=1, context=context)
            if pac_params_ids:
                pac_params = pac_params_obj.browse(
                    cr, uid, pac_params_ids, context)[0]
                user = pac_params.user
                password = pac_params.password
                wsdl_url = pac_params.url_webservice
                namespace = pac_params.namespace
                url = 'https://solucionfactible.com/ws/services/Timbrado'
                testing_url = 'http://testing.solucionfactible.com/ws/services/Timbrado'
                if (wsdl_url == url) or (wsdl_url == testing_url):
                    pass
                else:
                    raise osv.except_osv(_('Warning'), _('Web Service URL \
                        o PAC incorrect'))
                if namespace == 'http://timbrado.ws.cfdi.solucionfactible.com':
                    pass
                else:
                    raise osv.except_osv(_('Warning'), _(
                        'Namespace of PAC incorrect'))
                if 'testing' in wsdl_url:
                    msg += _(u'WARNING, SIGNED IN TEST!!!!\n\n')
                wsdl_client = WSDL.SOAPProxy(wsdl_url, namespace)
                if True:  # if wsdl_client:
                    file_globals = invoice_obj._get_file_globals(
                        cr, uid, invoice_ids, context=context)
                    fname_cer_no_pem = file_globals['fname_cer']
                    cerCSD = fname_cer_no_pem and base64.encodestring(
                        open(fname_cer_no_pem, "r").read()) or ''
                    fname_key_no_pem = file_globals['fname_key']
                    keyCSD = fname_key_no_pem and base64.encodestring(
                        open(fname_key_no_pem, "r").read()) or ''
                    cfdi = base64.encodestring(xml_res_str_addenda)
                    zip = False  # Validar si es un comprimido zip, con la extension del archivo
                    contrasenaCSD = file_globals.get('password', '')
                    params = [
                        user, password, cfdi, zip]
                    wsdl_client.soapproxy.config.dumpSOAPOut = 0
                    wsdl_client.soapproxy.config.dumpSOAPIn = 0
                    wsdl_client.soapproxy.config.debug = 0
                    wsdl_client.soapproxy.config.dict_encoding = 'UTF-8'
                    resultado = wsdl_client.timbrar(*params)
                    htz = int(invoice_obj._get_time_zone(
                        cr, uid, [ir_attachment_facturae_mx_id.invoice_id.id], context=context))
                    mensaje = _(tools.ustr(resultado['mensaje']))
                    resultados_mensaje = resultado['resultados'] and \
                        resultado['resultados']['mensaje'] or ''
                    folio_fiscal = resultado['resultados'] and \
                        resultado['resultados']['uuid'] or ''
                    codigo_timbrado = resultado['status'] or ''
                    codigo_validacion = resultado['resultados'] and \
                        resultado['resultados']['status'] or ''
                    if codigo_timbrado == '311' or codigo_validacion == '311':
                        raise osv.except_osv(_('Warning'), _(
                            'Unauthorized.\nCode 311'))
                    elif codigo_timbrado == '312' or codigo_validacion == '312':
                        raise osv.except_osv(_('Warning'), _(
                            'Failed to consult the SAT.\nCode 312'))
                    elif codigo_timbrado == '200' and codigo_validacion == '200':
                        fecha_timbrado = resultado[
                            'resultados']['fechaTimbrado'] or False
                        fecha_timbrado = fecha_timbrado and time.strftime(
                            '%Y-%m-%d %H:%M:%S', time.strptime(
                                fecha_timbrado[:19], '%Y-%m-%dT%H:%M:%S')) or False
                        fecha_timbrado = fecha_timbrado and datetime.strptime(
                            fecha_timbrado, '%Y-%m-%d %H:%M:%S') + timedelta(
                                hours=htz) or False
                        cfdi_data = {
                            'cfdi_cbb': resultado['resultados']['qrCode'] or False,  # ya lo regresa en base64
                            'cfdi_sello': resultado['resultados'][
                            'selloSAT'] or False,
                            'cfdi_no_certificado': resultado['resultados'][
                            'certificadoSAT'] or False,
                            'cfdi_cadena_original': resultado['resultados'][
                            'cadenaOriginal'] or False,
                            'cfdi_fecha_timbrado': fecha_timbrado,
                            'cfdi_xml': base64.decodestring(resultado[
                            'resultados']['cfdiTimbrado'] or ''),  # este se necesita en uno que no es base64
                            'cfdi_folio_fiscal': resultado['resultados']['uuid'] or '',
                            'pac_id': pac_params.id,
                        }
                        msg += mensaje + "." + resultados_mensaje + \
                            " Folio Fiscal: " + folio_fiscal + "."
                        msg += _(
                                u"\nMake Sure to the file really has generated correctly to the SAT\nhttps://www.consulta.sat.gob.mx/sicofi_web/moduloECFD_plus/ValidadorCFDI/Validador%20cfdi.html")
                        if cfdi_data.get('cfdi_xml', False):
                            url_pac = '</"%s"><!--Para validar el XML CFDI puede descargar el certificado del PAC desde la siguiente liga: https://solucionfactible.com/cfdi/00001000000102699425.zip-->' % (
                                comprobante)
                            cfdi_data['cfdi_xml'] = cfdi_data[
                                'cfdi_xml'].replace('</"%s">' % (comprobante), url_pac)
                            file = base64.encodestring(
                                cfdi_data['cfdi_xml'] or '')
                            # invoice_obj.cfdi_data_write(cr, uid, [invoice.id],
                            # cfdi_data, context=context)
                            cfdi_xml = cfdi_data.pop('cfdi_xml')
                        if cfdi_xml:
                            invoice_obj.write(cr, uid, [invoice.id], cfdi_data)
                            ##buscando la poliza de la factura que se esta timbrando
                            obj_gv_cfd_moves = self.pool.get('gv.cfdi.moves')#objeto de los cfdo asociados a la poliza
                            move_id = self.pool.get('account.move').search(cr, uid, [('name','=',invoice.number),('journal_id','=',invoice.journal_id.id)])[0]
                            serie = xml_res_str.getElementsByTagName('cfdi:Comprobante')[0].attributes['serie'].value
                            fecha_emision = xml_res_str.getElementsByTagName('cfdi:Comprobante')[0].attributes['fecha'].value
                            #obj_gv_cfd_moves.create(cr, uid, {
                            #                'fecha_emision': fecha_emision,
                            #                'tipo': 'Ingreso',
                            #                'serie':serie,
                            #                'folio': invoice.number,
                            #                'uuid': cfdi_data['cfdi_folio_fiscal'],
                            #                'rfc_tax': invoice.partner_id.vat[2:],
                            #                'razon_social': invoice.partner_id.name,
                            #                'total': invoice.amount_total,
                            #                'move_id': move_id}, context=context)
                           # self.pool.get('account.move').write(cr, uid, move_id, {'folio_fiscal_factura':cfdi_data['cfdi_folio_fiscal']},context=context)
                            cfdi_data['cfdi_xml'] = cfdi_xml
                        else:
                            msg += _(u"Can't extract the file XML of PAC")
                    else:
                            #move_id = self.pool.get('account.move').search(cr, uid, [('name','=',invoice.number),('journal_id','=',invoice.journal_id.id)])[0]
                            #self.pool.get('account.move').write(cr, uid, move_id, {'invoices_ids':invoice.id},context=context)
                        raise orm.except_orm(_('Warning'), _('Stamped Code: %s.-Validation code %s.-Folio Fiscal: %s.-Stamped Message: %s.-Validation Message: %s.') % (
                            codigo_timbrado, codigo_validacion, folio_fiscal, mensaje, resultados_mensaje))
            else:
                msg += 'Not found information from web services of PAC, verify that the configuration of PAC is correct'
                raise osv.except_osv(_('Warning'), _(
                    'Not found information from web services of PAC, verify that the configuration of PAC is correct'))
            return {'file': file, 'msg': msg, 'cfdi_xml': cfdi_xml}
#     def _upload_ws_file(self, cr, uid, ids, fdata=None, context=None):
#         """
#         @params fdata : File.xml codification in base64
#         """
#         if context is None:
#             context = {}
#         invoice_obj = self.pool.get('account.invoice')
#         pac_params_obj = invoice_obj.pool.get('params.pac')
#         for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
#             invoice = ir_attachment_facturae_mx_id.invoice_id
#             comprobante = invoice_obj._get_type_sequence(
#                 cr, uid, [invoice.id], context=context)
#             cfd_data = base64.decodestring(fdata or invoice_obj.fdata)
#             xml_res_str = xml.dom.minidom.parseString(cfd_data)
#             xml_res_addenda = invoice_obj.add_addenta_xml(
#                 cr, uid, xml_res_str, comprobante, context=context)
#             xml_res_str_addenda = xml_res_addenda.toxml('UTF-8')
#             #eliminacion de la firma de codificacion UTF-8 tambien conocido como BOM, elimina los espacion en blanco
#             xml_res_str_addenda = xml_res_str_addenda.replace(codecs.BOM_UTF8, '')
#             
#             if tools.config['test_report_directory']:#TODO: Add if test-enabled:
#                 ir_attach_facturae_mx_file_input = ir_attachment_facturae_mx_id.file_input and ir_attachment_facturae_mx_id.file_input or False
#                 fname_suffix = ir_attach_facturae_mx_file_input and ir_attach_facturae_mx_file_input.datas_fname or ''
#                 open( os.path.join(tools.config['test_report_directory'], 'l10n_mx_facturae_pac_sf' + '_' + \
#                   'before_upload' + '-' + fname_suffix), 'wb+').write( xml_res_str_addenda )
#             compr = xml_res_addenda.getElementsByTagName(comprobante)[0]
#             date = compr.attributes['fecha'].value
#             date_format = datetime.strptime(
#                 date, '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d')
#             context['date'] = date_format
#             invoice_ids = [invoice.id]
#             file = False
#             msg = ''
#             cfdi_xml = False
#             
#             pac_params_ids = pac_params_obj.search(cr, uid, [
#                 ('method_type', '=', 'pac_sf_firmar'), (
#                     'company_id', '=', invoice.company_emitter_id.id), (
#                         'active', '=', True)], limit=1, context=context)
#             if pac_params_ids:
#                 pac_params = pac_params_obj.browse(
#                     cr, uid, pac_params_ids, context)[0]
#                 user = pac_params.user
#                 password = pac_params.password
#                 wsdl_url = pac_params.url_webservice
#                 namespace = pac_params.namespace
#                 url = 'https://solucionfactible.com/ws/services/Timbrado'
#                 testing_url = 'http://testing.solucionfactible.com/ws/services/Timbrado'
#                 if (wsdl_url == url) or (wsdl_url == testing_url):
#                     pass
#                 else:
#                     raise osv.except_osv(_('Warning'), _('Web Service URL \
#                         o PAC incorrect'))
#                 if namespace == 'http://timbrado.ws.cfdi.solucionfactible.com':
#                     pass
#                 else:
#                     raise osv.except_osv(_('Warning'), _(
#                         'Namespace of PAC incorrect'))
#                 if 'testing' in wsdl_url:
#                     msg += _(u'WARNING, SIGNED IN TEST!!!!\n\n')
#                
#                 
#                  ####################Pac DIVERZA------------------------------------
#             strUrl = "https://staging.diverza.com/stamp"
#             strToken = "ABCD1234"
#             strTimbre = ""
#             strMensaje = ""
#             bTimbradoExitoso = False
#             strXmlEnMD5 = md5.new(xml_res_str_addenda).digest()
#             oHeaders = {
#                 "x-auth-token":strToken,
#                 "Content-MD5":strXmlEnMD5,
#                 "Content-Type": "application/vnd.sat.cfdi+xml;charset=UTF-8"
#             }
#             # try:
#             oResponse = requests.post(url=strUrl, data=xml_res_str_addenda, headers=oHeaders)
#             # raise osv.except_osv(_('Warning'), _('Timbre fiscal')+str(oResponse.status_code))
#             if(oResponse.status_code == 200):                
#                 cfdi_xml = oResponse.text
#                 #xml sin timbre
#                # xml_response = xml.dom.minidom.parseString(xml_res_str_addenda)
#                
#                 file = base64.encodestring(
#                     cfdi_xml or '')
#                 result = []
#                 pattern = '([A-Za-z]*=".*?")'
#                 string = ['selloSAT','noCertificadoSAT','FechaTimbrado','UUID']
#                 res = re.findall(pattern, cfdi_xml)
#                 for o in string:
#                     result.append(''.join([s for s in res if o in s]).replace(o+'=','').replace('"',''))
#                 cfdi_data = {
#                     'cfdi_sello': result[0] or False,
#                     'cfdi_no_certificado': result[1] or False,
#                     'cfdi_fecha_timbrado': result[2],
#                     'cfdi_xml': base64.decodestring(cfdi_xml or ''),
#                     'cfdi_folio_fiscal': result[3] or '',
#                     'pac_id': pac_params.id,
#                 }
#                 cfdi_xml = cfdi_data.pop('cfdi_xml')
#                 if cfdi_xml:
#                     invoice_obj.write(cr, uid, [invoice.id], cfdi_data)
#                     cfdi_data['cfdi_xml'] = cfdi_xml
#                 else:
#                     msg += _(u"Can't extract the file XML of PAC")
# 
#         return {'file': file, 'msg': msg, 'cfdi_xml': cfdi_xml}
#             #     raise osv.except_osv(_('Warning'), _('Timbre fiscal')+str(xml_res_str_addenda)+" <-------------  :  -----------> "+str(respuesta))
#             # else:
#             #     
#             #     self.strMensaje = oResponse.text
#             # except:
#             #     print("Problemas para ejecutar el timbrado.")
#             # return bTimbradoExitoso
#             ###################------------------------------------------------               
# # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
