# -*- coding: utf-8 -*-

{
	'name': 'crm_clientes',
	'version': '1.0',
	'category': 'gvadeto',
	'author': 'Viridiana Cruz Santos(gvadeto)2016',
	'description': """
									Funcionalidad: Este módulo se instala exclusivamente para crm
									""",
	'maintainer': '',
	'installable': True, 
	'active': False,
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base',
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#Archivo principal de menus
		'menus.xml',
		#------------------------------Clientes----------------#
		'secciones/clientes/cliente.xml',	
		
		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
