# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : Viridiana Cruz Santos                                                                                                              #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from datetime import datetime, timedelta
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp import models, api, _

##################################################################################
#                      agregación de campo Tipo en clientes                      #
#################################################################################
class res_partner( osv.osv ):
  _inherit = 'res.partner'
  
  @api.multi
  def onchange_type(self, is_company):
      tipo = ""
      super(res_partner, self).onchange_type(is_company)
      if is_company == True:
          return {'value' : {'tipo':'Cliente'},}
      if is_company == False:
          return {'value' : {'tipo':'Oportunidad'},}
      
  _columns = {
    'tipo' : fields.char('Tipo', size=15),
  }
  _defaults = {
    'tipo' : False
    } 
res_partner()


class config_rate_crm(osv.osv):
  _name = 'gv.config.rate.crm'
  _table = '_gv_config_rate_crm'
  _description = 'Moneda crm gv'
  _columns = {'currency_id': fields.many2one('res.currency','Moneda')}
 

config_rate_crm()
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2: