# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                                #
#  @creacion    :  (aaaa/mm/dd)                                                                                                                      #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
import xml.dom.minidom
import base64
import cStringIO
from openerp import models, api, _
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _
import logging
from openerp.tools.safe_eval import safe_eval as eval
import pytz
from openerp import SUPERUSER_ID
_logger = logging.getLogger(__name__)

class account_move(osv.osv):
    _inherit = "account.move"
    ### continuear con esta funcion lunes
    
    def proveedor(self, cr, uid, ids, context=None):
        proveedor = ""
        obj_invoice = self.pool.get('account.invoice')
        obj_voucher = self.pool.get('account.voucher')
        data = self.browse(cr, uid, ids)
        cr.execute("""SELECT COUNT(*) FROM account_invoice WHERE move_id=%s""",(data.id,))
        count = cr.fetchone()[0]
        if count > 0: 
            id_invoice = obj_invoice.search(cr, uid, [('move_id','=',data.id)], context=context)
            invoice = obj_invoice.browse(cr, uid, id_invoice, context=context)
            proveedor = invoice.partner_id.name
            #raise osv.except_osv(('Warning!'), (str(proveedor)))
        else:
            cr.execute("""SELECT COUNT(*) FROM account_voucher WHERE number=%s""",(data.name,))
            count_voucher = cr.fetchone()[0]
            if count_voucher > 0:
                id_voucher = obj_voucher.search(cr, uid, [('number','=',data.name)], context=context)
                voucher = obj_voucher.browse(cr, uid, id_voucher, context=context)
                proveedor = voucher.partner_id.name
                #raise osv.except_osv(('Warning!'), (str(proveedor)))
        return proveedor
    
    
    def informacion_diot(self, cr, uid, ids, context=None):
        total_erogacion = 0.00
        importe_base = 0.00
        iva_retenido = 0.00
        importe_iva = 0.00
        isr_reteido = 0.00
        total_retenciones = 0.00
        tasa = 0.00
        apli_iva = "NO"
        obj_gv_diot = self.pool.get('gv.move.diot')#objeto de los cfdo asociados a la poliza
        obj_invoice = self.pool.get('account.invoice')
        data = self.browse(cr, uid, ids)
        id_invoice = obj_invoice.search(cr, uid, [('move_id','=',data.id)], context=context)
        invoice = obj_invoice.browse(cr, uid, id_invoice, context=context)
        proveedor = self.proveedor(cr, uid , ids, context=context)
        #raise osv.except_osv(('Warning!'), (str(proveedor)))
        for line in data.line_id:
             if line.tax_id_secondary:
                if line.tax_id_secondary.tax_category_id.name=='IVA':
                    if line.debit:importe_iva+=line.debit
                    else:importe_iva+=line.credit
                if line.tax_id_secondary.tax_category_id.name=='IVA-RET':
                    if line.credit: iva_retenido+=line.credit
                    else: iva_retenido+=line.debit
                if line.tax_id_secondary.tax_category_id.name=='ISR-RET':
                    isr_reteido+=line.credit
             if line.date_maturity:
                    if line.credit: importe_base+=line.credit
                    else: importe_base+=line.debit
        if importe_iva>0:apli_iva = "SI"
        total_retenciones = iva_retenido+isr_reteido
        importe_base = (importe_base-importe_iva) + total_retenciones
        total_erogacion = (importe_base + importe_iva) - total_retenciones
        #raise osv.except_osv(('Warning!'), (str(importe_iva)+" -:- "+str(importe_base)))
        if importe_iva > 0 and importe_base > 0 :
            tasa = (importe_iva*100)/importe_base 
        obj_gv_diot.create(cr, uid, {'provedor': proveedor,
                                    'total_erogacion': total_erogacion,
                                    'tasa': tasa,
                                    'importe_base': importe_base,
                                    'importe_iva': importe_iva,
                                    'otras_erogaciones': 0.00,
                                    'iva_retenido': iva_retenido,
                                    'isr_retenido': isr_reteido,
                                    'iva_pag_no_acreditable': 0.00,
                                    'apl_iva': apli_iva,
                                    'periodo_acreditable': data.period_id.id,
                                    'move_id': data.id,
                                    }, context=context)
        
    def _informacion_diot(self, cr, uid, ids, name, args, context):
        """informacion de la diot cuando la poliza es generada directamente
           cuando se valida la factura"""
        if not ids : return {}
        result = {}
        valor = False
        data = self.browse(cr, uid, ids)
        #if not data.gv_move_diot_id:
      
        self.informacion_diot(cr, uid, ids, context=context)
        result[data.id] = valor
        return result

    def _obtiene_uuids(self, cr, uid, ids, name, args, context):
        """Obtiene los folios de las facturas ligadas a esta poliza, en base al campo invoices_ids"""
        if not ids : return {}
        result = {}
        valor = False
        sub = ""
        lista_ivoices_ids = []
        lista_nuevos_invoices_ids = ""
        nuevos_uuids = ""
        move_obj =  self.pool.get('account.move')
        #try:
        for this in self.browse(cr,uid,ids):
            if this.invoices_ids:
                for cad  in this.invoices_ids:
                    if cad!=',': sub+=cad
                    if cad==',':
                        lista_ivoices_ids.append(sub)
                        sub = ""
                for l in lista_ivoices_ids:
                    invoice = self.pool.get('account.invoice').browse(cr, uid, int(l), context=context)
                    if invoice.cfdi_folio_fiscal:
                        nuevos_uuids+=invoice.cfdi_folio_fiscal+","
                        #raise osv.except_osv(('Warning!'), (str(l)))
                    if not invoice.cfdi_folio_fiscal:
                        lista_nuevos_invoices_ids+=l+","
                if this.folio_fiscal_factura:
                     nuevos_uuids = nuevos_uuids+str(this.folio_fiscal_factura)
                    
                #raise osv.except_osv(('Warning!'), (str(nuevos_uuids)))
                move_obj.write(cr, uid, this.id, {'invoices_ids':lista_nuevos_invoices_ids,'folio_fiscal_factura':nuevos_uuids,},context=context)
                
        result[this.id] = valor
        return result
        #except:
          # return {}
        
    _columns = {'folio_fiscal_factura': fields.text("UUID", readonly=True),
                'invoices_ids' :  fields.char('Facturas',size=254),
                'obtener_uuids' : fields.function(_obtiene_uuids,type="boolean"),
                'diot_factura' : fields.function(_informacion_diot,type="boolean"),
                'direccion': fields.char('Direccion:', size=254),
                'rfc_empresa': fields.char('Reg. Fed:', size=254),
                'codigo_postal': fields.char('Código postal:', size=254),
                'moneda': fields.char('Moneda:', size=254),
               
                #relacion al modelo de datos del diot
                'gv_move_diot_id': fields.one2many('gv.move.diot','move_id', 'DIOT'),
                #relacion al modelo de los cfdi asociados a esta poliza
                'gv_cfdi_moves_id': fields.one2many('gv.cfdi.moves','move_id', 'UUIDS')
                }
    
                
    
    
    def _obtiene_direccion(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        obj_company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        return obj_company.street+" "+obj_company.l10n_mx_street3+"-"+obj_company.l10n_mx_street4+", "+obj_company.street2
    
    def _obtiene_rfc(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        obj_company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        return obj_company.vat[2:]
    
    def _obtiene_cod_postal(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        obj_company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        return obj_company.zip
    
    def _obtiene_moneda(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        obj_company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        if obj_company.currency_id.name=='MXN':
            return 'Peso Mexicano'
        elif obj_company.currency_id.name=='USD':
            return 'Dolar'
        
    _defaults={
         'direccion': _obtiene_direccion,
         'rfc_empresa': _obtiene_rfc,
         'codigo_postal': _obtiene_cod_postal,
         'moneda': _obtiene_moneda,
         
    }
               
account_move()


class account_voucher(osv.osv):
    _inherit = 'account.voucher'
     
    def tipo_documento(self, cr, uid, type_doc, context=None):
        """Regresa el tipo de documento ingreso(FAC) o egreso(TC)"""
        if type_doc=='out_refund' or type_doc=='in_refund':
            return 'Egreso'
        if type_doc=='out_invoice' or type_doc=='in_invoice':
            return 'Ingreso'
    
    def uuids_documentos_cliente(self, cr, uid, ids, context=None):
        """uuids facturas y notas credito de cliente cuando se realiza un pago desde la factura"""
        data=self.browse(cr, uid, ids)
        obj_account_voucher = self.pool.get('account.voucher')
        obj_account_move = self.pool.get('account.move')
        active_id = context.get('active_id')
        obj_invoice = self.pool['account.invoice'].browse(cr, uid, active_id, context=context)
        obj_gv_cfd_moves = self.pool.get('gv.cfdi.moves')#objeto de los cfdo asociados a la poliza
        ########################################################################################
        obj_ir_attachment = self.pool.get('ir.attachment') #objteto para leer adjuntos
        ########################################################################################
        tipo = self.tipo_documento(cr, uid,obj_invoice.type, context=context)
        #Facturas de cliente
       
        serie = obj_invoice.journal_id.sequence_id.approval_ids.serie
        if obj_invoice.journal_id.sequence_id:
            if obj_invoice.journal_id.sequence_id.approval_ids.type=='cfdi32_pac_sf':
                attachment_id = self.pool.get('ir.attachment.facturae.mx').search(cr, uid, [('invoice_id','=',obj_invoice.id)], context=context)[0]
                obj_attachment = self.pool.get('ir.attachment.facturae.mx').browse(cr, uid, attachment_id, context=context)
                if obj_attachment.state!='draft' or obj_attachment.state=='confirmed' or obj_attachment.state!='cancel':
                    id_poliza = obj_account_move.search(cr, uid, [('name','=',data.number)], context=context)[0]
                    #xml de la factura
                    obj_ir_attachment = self.pool.get('ir.attachment') #objteto para leer adjuntos
                    id_attachment = obj_ir_attachment.search(cr, uid, [('res_id','=', obj_invoice.id)], context=context)[0]
                    attachment = obj_ir_attachment.browse(cr, uid, id_attachment, context=context)
                    xmldoc = xml.dom.minidom.parseString(attachment.index_content)
                    fecha_emision = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['fecha'].value
                    obj_gv_cfd_moves.create(cr, uid, {
                                        'fecha_emision': fecha_emision,
                                        'tipo': tipo,
                                        'serie':serie,
                                        'folio': obj_invoice.number,
                                        'uuid': obj_invoice.cfdi_folio_fiscal,
                                        'rfc_tax': obj_invoice.partner_id.vat[2:],
                                        'razon_social': obj_invoice.partner_id.name,
                                        'total': obj_invoice.amount_total,
                                        'move_id': id_poliza}, context=context)
                    #obj_account_move.write(cr, uid, id_poliza, {'folio_fiscal_factura':obj_invoice.cfdi_folio_fiscal},context=context)
                else:
                    id_poliza = obj_account_move.search(cr, uid, [('name','=',data.number)], context=context)[0]
                    obj_account_move.write(cr, uid, id_poliza, {'invoices_ids':obj_invoice.id},context=context)
    
    def uuids_documentos_proveedor(self, cr, uid, ids, context=None):
        """uuids facturas y notas credito de proveedor cuando se realiza un pago desde la factura"""
        data=self.browse(cr, uid, ids)
        obj_account_voucher = self.pool.get('account.voucher')
        obj_account_move = self.pool.get('account.move')
        active_id = context.get('active_id')
        obj_invoice = self.pool['account.invoice'].browse(cr, uid, active_id, context=context)
        obj_gv_cfd_moves = self.pool.get('gv.cfdi.moves')#objeto de los cfdo asociados a la poliza
        ########################################################################################
        obj_ir_attachment = self.pool.get('ir.attachment') #objteto para leer adjuntos
        ########################################################################################
        tipo = self.tipo_documento(cr, uid,obj_invoice.type, context=context)
        if obj_invoice.type=='in_invoice' or obj_invoice.type=='in_refund':
            id_poliza = obj_account_move.search(cr, uid, [('name','=',data.number)], context=context)[0]
            id_attachment = obj_ir_attachment.search(cr, uid, [('res_id','=', obj_invoice.id)], context=context)[0]
            attachment = obj_ir_attachment.browse(cr, uid, id_attachment, context=context)
            xmldoc = xml.dom.minidom.parseString(attachment.index_content)
            uuid = xmldoc.getElementsByTagName('tfd:TimbreFiscalDigital')[0].attributes['UUID'].value
            serie = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['serie'].value
            fecha_emision = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['fecha'].value
            folio = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['folio'].value
            obj_gv_cfd_moves.create(cr, uid, {
                                            'fecha_emision': fecha_emision,
                                            'tipo': tipo,
                                            'serie':serie,
                                            'folio': folio,
                                            'uuid': uuid,
                                            'rfc_tax': obj_invoice.partner_id.vat[2:],
                                            'razon_social': obj_invoice.partner_id.name,
                                            'total': obj_invoice.amount_total,
                                            'move_id': id_poliza}, context=context)
            
    def button_proforma_voucher(self, cr, uid, ids, context=None):
        """Guarda el uuid de la factura cuando se regristra un pago desde la factura"""
        super(account_voucher, self).button_proforma_voucher(cr, uid, ids, context=context)
        data = self.browse(cr, uid, ids)
        active_id = context.get('active_id')
        obj_invoice = self.pool['account.invoice'].browse(cr, uid, active_id, context=context)
        if obj_invoice.type=='out_refund' or obj_invoice.type=='out_invoice':
            self.uuids_documentos_cliente(cr, uid, ids, context=context)
        if obj_invoice.type=='in_refund' or obj_invoice.type=='in_invoice':
            self.uuids_documentos_proveedor(cr, uid, ids, context=context)
    
    def uiids_pagos_clientes(self, cr, uid, invoice_id,number, context=None):
        """uuids cuando se realiza un pago desde el modulo de pagos clientes"""
        uuids = ""
        invoices_ids = ""
        obj_gv_cfd_moves = self.pool.get('gv.cfdi.moves')#objeto de los cfdo asociados a la poliza
        obj_account_voucher = self.pool.get('account.voucher')
        obj_account_move = self.pool.get('account.move')
        obj_invoice = self.pool['account.invoice'].browse(cr, uid, invoice_id, context=context)
        id_poliza = obj_account_move.search(cr, uid, [('name','=',number)], context=context)[0]
        serie = obj_invoice.journal_id.sequence_id.approval_ids.serie
        tipo = self.tipo_documento(cr, uid,obj_invoice.type, context=context)
        serie = obj_invoice.journal_id.sequence_id.approval_ids.serie
        obj_ir_attachment = self.pool.get('ir.attachment') #objteto para leer adjuntos
        id_attachment = obj_ir_attachment.search(cr, uid, [('res_id','=', invoice_id)], context=context)[0]
        attachment = obj_ir_attachment.browse(cr, uid, id_attachment, context=context)
        #xmldoc = xml.dom.minidom.parseString(attachment.index_content)
        #fecha_emision = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['fecha'].value
        
        ##
        obj_gv_cfd_moves.create(cr, uid, {
                            'fecha_emision': obj_invoice.date_invoice,
                            'tipo': tipo,
                            'serie':serie,
                            'folio': obj_invoice.number,
                            'uuid':obj_invoice.cfdi_folio_fiscal,
                            'rfc_tax': obj_invoice.partner_id.vat[2:],
                            'razon_social': obj_invoice.partner_id.name,
                            'total': obj_invoice.amount_total,
                            'move_id': id_poliza}, context=context)
        #uuids+=this.move_line_id.invoice.cfdi_folio_fiscal+","
                
    
    def uiids_pagos_provedores(self, cr, uid, invoice_id, number,context=None):
        """uuids cuando se realiza un pago desde el modulo de pagos proveedores"""
       # data=self.browse(cr, uid, ids)
        #raise osv.except_osv(('Warning!'), (str(invoice_id)))
        #raise osv.except_osv(('Warning!'), ("4"))
        obj_account_voucher = self.pool.get('account.voucher')
        obj_account_move = self.pool.get('account.move')
        obj_invoice = self.pool['account.invoice'].browse(cr, uid, invoice_id, context=context)
        obj_gv_cfd_moves = self.pool.get('gv.cfdi.moves')#objeto de los cfdo asociados a la poliza
        ########################################################################################
        obj_ir_attachment = self.pool.get('ir.attachment') #objteto para leer adjuntos
        ########################################################################################
        tipo = self.tipo_documento(cr, uid,obj_invoice.type, context=context)
    
        id_poliza = obj_account_move.search(cr, uid, [('name','=',number)], context=context)[0]
        #cr.execute('select id from ir_attachment where res_id=%s',(invoice_id,))
        #id_ir = cr.fetchone()[0]
        #raise osv.except_osv(('Warning!'), (str(id_attachment)))
        id_attachment = obj_ir_attachment.search(cr, uid, [('res_id','=', invoice_id)], context=context)[0]
        #raise osv.except_osv(('Warning!'), (str(id_attachment)))
        attachment = obj_ir_attachment.browse(cr, uid, id_attachment, context=context)
        
        xmldoc =  xml.dom.minidom.parseString(attachment.index_content)
       
        uuid = xmldoc.getElementsByTagName('tfd:TimbreFiscalDigital')[0].attributes['UUID'].value
        serie = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['serie'].value
        fecha_emision = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['fecha'].value
        folio = xmldoc.getElementsByTagName('cfdi:Comprobante')[0].attributes['folio'].value
        obj_gv_cfd_moves.create(cr, uid, {
                                        'fecha_emision': fecha_emision,
                                        'tipo': tipo,
                                        'serie':serie,
                                        'folio':folio,
                                        'uuid': uuid,
                                        'rfc_tax': obj_invoice.partner_id.vat[2:],
                                        'razon_social': obj_invoice.partner_id.name,
                                        'total': obj_invoice.amount_total,
                                        'move_id': id_poliza}, context=context)
        
        return True
    def proforma_voucher(self, cr, uid, ids, context=None):
        """Guarda el uuid de las facturas o notas de credito timbradas,
           cuando se realiza un pago desde el submenu Pagos de clientes."""
        #hace la llamada al metodo padre para que haga lo normal
        super(account_voucher, self).proforma_voucher(cr, uid, ids, context=context)
        obj_account_move = self.pool.get('account.move')
        invoices_ids = ""
        data =  self.browse(cr, uid, ids)
        for this in data.line_cr_ids:
             if this.move_line_id.invoice:
                if this.move_line_id.invoice.type=='out_invoice':
                     if  this.move_line_id.invoice.journal_id.sequence_id.approval_ids.type=='cfdi32_pac_sf':
                         if this.move_line_id.invoice.cfdi_folio_fiscal:
                            self.uiids_pagos_clientes(cr, uid, this.move_line_id.invoice.id, data.number)
                         else:invoices_ids+=str(this.move_line_id.invoice.id)+","
                if this.move_line_id.invoice.type=='in_refund':
                    self.uiids_pagos_provedores(cr, uid, this.move_line_id.invoice.id,data.number)
                
                            
        for fac in data.line_dr_ids:
           if fac.move_line_id.invoice:
            if fac.move_line_id.invoice.type=='out_refund':
                 if  fac.move_line_id.invoice.journal_id.sequence_id.approval_ids.type=='cfdi32_pac_sf':
                      if fac.move_line_id.invoice.cfdi_folio_fiscal:
                        self.uiids_pagos_clientes(cr, uid, fac.move_line_id.invoice.id,data.number)
                      else:invoices_ids+=str(this.move_line_id.invoice.id)+","
            if fac.move_line_id.invoice.type=='in_invoice':
                self.uiids_pagos_provedores(cr, uid, fac.move_line_id.invoice.id, data.number)
               
        
        #if invoices_ids:
              #  obj_account_move.write(cr, uid, id_poliza, {'invoices_ids':invoices_ids,},context=context)
        return True
account_voucher()








class move_diot(osv.Model):
    
    _name='gv.move.diot'
    
    _table = '_gv_move_diot'
    
    _columns = {#datos del formato de la diot
                'provedor': fields.char("Prov."),
                'total_erogacion': fields.float("Total erogacion"),
                'tasa': fields.float("Tasa"),
                'importe_base': fields.float("Importe base."),
                'importe_iva': fields.float("Importe IVA"),
                'otras_erogaciones': fields.float("Otras erogaciones"),
                'iva_retenido': fields.float("IVA retenido"),
                'isr_retenido': fields.float("ISR retenido"),
                'iva_pag_no_acreditable': fields.float("IVA pag. no acred"),
                'apl_iva': fields.char("Apl. IVA", size=254),
                'periodo_acreditable': fields.many2one('account.period',"Periodo acredit.",),
                #Relacion al modelo de account.move polizas
                'move_id': fields.many2one('account.move','Poliza')}
                
                
    defaults = {}
move_diot()


#Clase para los cfdi's asociados a la poliza
class cfdi_moves(osv.Model):
    
    _name='gv.cfdi.moves'
    
    _table = '_gv_cfdi_moves'
    
    _columns = {#datos del formato de la diot
                'fecha_emision': fields.char("Emision"),
                'tipo': fields.char("Tipo"),
                'serie': fields.char("Serie"),
                'folio': fields.char("Folio"),
                'uuid': fields.char("UUID"),
                'rfc_tax': fields.char("RFC/TaxId"),
                'razon_social': fields.char("Razon Social"),
                'total': fields.float("Total"),
                'move_id': fields.many2one('account.move','Poliza')}
                
                
    defaults = {}
cfdi_moves()
 
 
