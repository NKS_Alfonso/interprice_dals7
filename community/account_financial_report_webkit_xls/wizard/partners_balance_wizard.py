# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import orm, osv
# import logging
# _logger = logging.getLogger(__name__)


class partner_balance_wizard(orm.TransientModel):
    _inherit = 'partner.balance.webkit'

    def xls_export(self, cr, uid, ids, context=None):
        return self.check_report(cr, uid, ids, context=context)#eeeeeeeeeeeeeeeeeee

    def _print_report(self, cr, uid, ids, data, context=None):
        context = context or {}
        if context.get('xls_export'):
            # we update form with display account value
            data = self.pre_print_report(cr, uid, ids, data, context=context)
            cr.execute("""SELECT rcr.rate FROM res_currency AS rc 
                                INNER JOIN res_currency_rate AS rcr 
                                ON rc.id=rcr.currency_id
                                WHERE rc.name='USD' order by rcr.id desc
                            """)
            rate = cr.fetchone()[0]
            tipo_cambio =  round(1/rate,2)
            data['tipocambio'] = tipo_cambio
            #raise osv.except_osv( ('Al!'), (str(data)))
            return {
                'type': 'ir.actions.report.xml',
                'report_name': 'account.account_report_partner_balance_xls',
                'datas': data}
        else:
            return super(partner_balance_wizard, self)._print_report(
                cr, uid, ids, data, context=context)
