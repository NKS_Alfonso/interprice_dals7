#-*- coding:utf-8 -*-
from openerp import models, api, _
from openerp.exceptions import Warning
from openerp.osv import fields, osv


class sale_order(models.Model):
    _inherit = "sale.order"
    
    @api.onchange('property_payment_term')
    def _onchange_property_payment_term(self):
        self.payment_term = self.property_payment_term
        obj_payment_type =self.env['account.payment.term'].browse(int(self.property_payment_term))
        if (self.credit > self.credit_limit) and (self.property_payment_term.id!=1):
          self. property_payment_term = False
          self.payment_term = False
          
        
    @api.one
    def action_wait(self):
        self.check_limit()
        return super(sale_order, self).action_wait()

    # @api.one
    def check_limit(self):
        obj_payment_type =self.env['account.payment.term'].browse(int(self.payment_term))
        if self.payment_term.name=='Immediate Payment':
         return  True
        
        else:
          # We sum from all the sale orders that are aproved, the sale order
          # lines that are not yet invoiced
          domain = [('order_id.partner_id', '=', self.partner_id.id),
                    ('invoiced', '=', False),
                    ('order_id.state', 'not in', ['draft', 'cancel', 'sent'])]
          order_lines = self.env['sale.order.line'].search(domain)
          none_invoiced_amount = sum([x.price_subtotal for x in order_lines])
  
          # We sum from all the invoices that are in draft the total amount
          domain = [ 
              ('partner_id', '=', self.partner_id.id), ('state', '=', 'draft')]
          draft_invoices = self.env['account.invoice'].search(domain)
          draft_invoices_amount = sum([x.amount_total for x in draft_invoices])
  
          available_credit = self.partner_id.credit_limit - \
              self.partner_id.credit - \
              none_invoiced_amount - draft_invoices_amount
          
          if self.amount_total > available_credit:
              msg = 'No se puede confirmar el Pedido ya que el cliente no tiene credito suficiente.\
                    Favor de realizar su Plazo de pago de forma "Pago inmediato".'
              raise Warning(_(msg))
              return False
          return True
    
    def _fnct_get_credit_partner(self, cr, uid, ids, field_name, args, context=None):
        res = {}
        ban = False
        for this in self.browse(cr, uid, ids, context=context):
            
              credit =float(this.partner_id.credit)
              credit_limit = float(this.partner_id.credit_limit)
              cr.execute("""UPDATE sale_order SET credit=%s, credit_limit=%s WHERE id=%s""",(credit,credit_limit,this.id,))
              cr.commit()
              res[this.id] = ban  
        return res
    
    # Nuevos campos agregados para el modelo sale.order
    _columns = {
      'credit': fields.float('Total a cobrar'),
      'credit_limit': fields.float('Crédito concedido'),
      'property_payment_term': fields.many2one('account.payment.term','Plazo de pago', required=True,),
      'update_credit_partner': fields.function(_fnct_get_credit_partner, string="actualizar", type='boolean'),
      
    }
    
sale_order()