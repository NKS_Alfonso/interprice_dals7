# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Copyright (c) 2010 moylop260 - http://www.hesatecnica.com.com/
#    All Rights Reserved.
#    info skype: german_442 email: (german.ponce@hesatecnica.com)
############################################################################
#    Coded by: german_442 email: (german.ponce@hesatecnica.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields
import time
import dateutil
import dateutil.parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, date
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare
import openerp.addons.decimal_precision as dp
from openerp import netsvc
import openerp
import calendar
import tempfile
from xml.dom import minidom
import os
import base64
import hashlib
import tempfile
import os
import codecs

from qrtools import QR
from string import zfill

import xml.dom.minidom
from xml.dom import minidom

class account_invoice(osv.osv):
    _inherit ='account.invoice'

    # def _obtain_qr(self, cr, uid, ids, fields, arg, context=None):
    #     result = {}
    #     for rec in self.browse(cr, uid, ids, context=None):
    #         company_vat = rec.company_emitter_id.address_invoice_parent_company_id.vat
    #         rfc_transmitter = company_vat.replace('MX','') if company_vat else ''
    #         customer_vat = rec.partner_id.vat
    #         rfc_receiver = customer_vat.replace('MX','')
    #         amount_total = zfill("%0.6f"%rec.amount_total,17)
    #         cfdi_folio_fiscal = rec.cfdi_folio_fiscal
    #         # if not cfdi_folio_fiscal:
    #         #     return result

    #         qrstr = "?re="+rfc_transmitter if rfc_transmitter else ''+"&rr="+rfc_receiver if rfc_receiver else ''+"&tt="+str(amount_total) if amount_total else ''+"&id="+cfdi_folio_fiscal if cfdi_folio_fiscal else ''

    #         qr_string = qrstr
    #         qr_code = QR(data=qr_string.encode('utf-8'))
    #         try:
    #             qr_code.encode()
    #         except Exception, e:
    #             raise osv.except_osv(_('Error'), _('No se pudo Crear la Imagen CBB.\n Contacte al Administrador %s') % e)

    #         qr_file = open(qr_code.filename, "rb")
    #         temp_bytes = qr_file.read()
    #         qr_bytes = base64.encodestring(temp_bytes)
    #         qr_file.close()

    #         result[rec.id] = qr_bytes
    #     return result

    _columns = {
    'entry_invoice': fields.selection([('Ingreso','Ingreso'),('Egreso','Egreso')],'Ingreso o Egreso', required=False),
    #'cfdi_cbb': fields.function(_obtain_qr, type="binary", string="CFD-I CBB", store=True),
    'cfdi_cbb': fields.binary("CFD-I CBB"),
    'cfdi_sello': fields.text('CFD-I Sello', help='Sign assigned by the SAT'),
    'cfdi_no_certificado': fields.char('CFD-I Certificado', size=32,
                                       help='Serial Number of the Certificate'),
    'cfdi_cadena_original': fields.text('CFD-I Cadena Original',
                                        help='Original String used in the electronic invoice'),
    'cfdi_fecha_timbrado': fields.datetime('CFD-I Fecha Timbrado',
                                           help='Date when is stamped the electronic invoice'),
    'cfdi_folio_fiscal': fields.char('CFD-I Folio Fiscal', size=64,
                                     help='Folio used in the electronic invoice'),

    }
    
    ######### FUNCION QUE IMPRIME UN REPORTE EN JASPER DESDE UN BOTON CON UNA FUNCION EN PYTHON ######################
    def print_jasper_invoice(self, cr, uid, ids, context=None):
        value = {
            'type': 'ir.actions.report.xml',
            'report_name': 'Factura-2013-Final',
            'datas': {
                        'model' : 'account.invoice',
                        'ids'   : ids,
                        }
                    }

        return value

    def print_jasper_note(self, cr, uid, ids, context=None):
        value = {
            'type': 'ir.actions.report.xml',
            'report_name': 'Nota-2013-Final',
            'datas': {
                        'model' : 'account.invoice',
                        'ids'   : ids,
                        }
                    }

        return value

    def write(self, cr, uid, ids, vals, context=None):
        if vals.has_key('cfdi_cbb'):
            result=super(account_invoice, self).write(cr, uid, ids, vals, context)
            return result
        for rec in self.browse(cr, uid, ids, context=None):
            tipoComprobante = "Egreso"
            if rec.type=='out_invoice':
                tipoComprobante = 'Ingreso'
                company_vat = rec.company_emitter_id.address_invoice_parent_company_id.vat
                rfc_transmitter = company_vat.replace('MX','') if company_vat else ''
                customer_vat = rec.partner_id.vat
                rfc_receiver = customer_vat.replace('MX','')  if customer_vat else ''
                amount_total = zfill("%0.6f"%rec.amount_total,17) if rec.amount_total else ''
                cfdi_folio_fiscal = rec.cfdi_folio_fiscal if rec.cfdi_folio_fiscal else ''
                if not cfdi_folio_fiscal:
                    qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+''

                    qr_string = qrstr
                    qr_code = QR(data=qr_string.encode('utf-8'))
                    try:
                        qr_code.encode()
                    except Exception, e:
                        raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                    
                    qr_file = open(qr_code.filename, "rb")
                    temp_bytes = qr_file.read()
                    qr_bytes = base64.encodestring(temp_bytes)
                    qr_file.close()

                    vals.update({'cfdi_cbb':qr_bytes})
                    return super(account_invoice, self).write(cr, uid, ids, vals, context)

                qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+cfdi_folio_fiscal

                qr_string = qrstr
                qr_code = QR(data=qr_string.encode('utf-8'))
                try:
                    qr_code.encode()
                except Exception, e:
                    raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                
                qr_file = open(qr_code.filename, "rb")
                temp_bytes = qr_file.read()
                qr_bytes = base64.encodestring(temp_bytes)
                qr_file.close()

                vals.update({'cfdi_cbb':qr_bytes,'entry_invoice': tipoComprobante})

                # invoice_obj = self.pool.get('account.invoice')
                # attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',rec.id),('name','like','.xml')], context=context)
                # if attachment_ids:
                #     attach_obj = self.pool.get('ir.attachment')
                #     attach_browse = attach_obj.browse(cr, uid, attachment_ids, context=None)[0]
                #     cfd_data = base64.decodestring(attach_browse.datas)
                #     if cfd_data:
                #         cfdi_minidom = minidom.parseString(cfd_data)
                #         # getElementsByTagName es un metodo que permite extraer partes de codigo
                #         # pasandole como parametro el nombre de la etiqueta <xxx
                #         # getAttribute() metodo que extrae las propiedades de esa etiqueta como 
                #         # Emisor, RFC, etc...
                #         # Todos los Resultados del Minimon son regresados en forma de lista [ '<x id=''></>',]
                #         node = cfdi_minidom.getElementsByTagName('cfdi:Comprobante')[0]
                #         subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')
                #         if subnode:
                #             subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')[0]
                #             cfdi_folio_fiscal = subnode.getAttribute('UUID') if subnode.getAttribute('UUID') else ''
                #             cfdi_fecha_timbrado = subnode.getAttribute('FechaTimbrado') if subnode.getAttribute('FechaTimbrado') else ''
                #             cfdi_no_certificado = subnode.getAttribute('noCertificadoSAT') if subnode.getAttribute('noCertificadoSAT') else ''
                #             cfdi_sello = subnode.getAttribute('selloSAT') if subnode.getAttribute('selloSAT') else ''
                #             sello_CFD = subnode.getAttribute('selloCFD') if subnode.getAttribute('selloCFD') else ''
                #             cfdi_cadena_original = '||1.0|'+cfdi_folio_fiscal+'|'+cfdi_fecha_timbrado+'|'+sello_CFD+'|'+cfdi_no_certificado+'||'
                #             vals.update({ 'cfdi_folio_fiscal':cfdi_folio_fiscal,
                #                             'cfdi_fecha_timbrado':cfdi_fecha_timbrado,
                #                             'cfdi_no_certificado':cfdi_no_certificado,
                #                             'cfdi_sello':cfdi_sello,
                #                             'cfdi_cadena_original':cfdi_cadena_original})

        result=super(account_invoice, self).write(cr, uid, ids, vals, context)
        return result

    def copy(self, cr, uid, id, default={}, context=None):
        if context is None:
            context = {}
        default.update({
            'cfdi_cbb': False,
            'cfdi_folio_fiscal': False,
            'cfdi_fecha_timbrado': False,
            'cfdi_no_certificado': False,
            'cfdi_sello': False,
            'cfdi_cadena_original': False,
        })
        return super(account_invoice, self).copy(cr, uid, id, default, context)
    
    
    def action_invoice_sent_jasper(self, cr, uid, ids, context=None):
    
     return {
        'type': 'ir.actions.act_window',
        'res_model': 'send.massive.invoice',
        'view_mode': 'form',
        'view_type': 'form',
        'views': [(False, 'form')],
        'target': 'new',
        'context':context,
       }
    
    def action_invoice_sent_jasperddd(self, cr, uid, ids, context=None):
        '''
        This function opens a window to compose an email, with the edi invoice template message loaded by default
        '''
        #self.attachment_invoice(cr, uid, ids, context=None)
        
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'hesatec_attachment_cfdi_jasper', 'email_template_edi_invoice_jasper')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False 
        ctx = dict(context)
        #adjuntar por defatul by yoo
        attachment_ids = []
        active_ids = context and context.get('active_ids', False)
        wizard_facturae = self.pool.get('ir.attachment')
        wizard_ids = wizard_facturae.search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',ids[0])])
        if wizard_ids:
                for wizard in wizard_facturae.browse(cr, uid, wizard_ids, context=None):
                    if wizard.id:
                        attachment_ids.append(wizard.id)
                    if not wizard.id:
                      file_xml_sign = wizard.name.split('.')[0] if wizard.name else ''
                      if file_xml_sign:
                          attachment_obj = self.pool.get('ir.attachment')
                          attachment_pdf_id = attachment_obj.search(cr, uid,[('name','=',file_xml_sign+'.pdf')])
                          if attachment_pdf_id:
                              attachment_ids.append(attachment_pdf_id[0])
        attachment_not_repeat_ids = []
        [attachment_not_repeat_ids.append(key) for key in attachment_ids if key not in attachment_not_repeat_ids]
        
        #attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',ids[0])], context=context)
        #raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % compose_form_id)
        
        ctx.update({
            'default_model': 'account.invoice',
            'res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_invoice_as_sent': True,
            'attachment_ids':[x for x in attachment_not_repeat_ids],
            })
        
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

account_invoice()


class  account_invoice_attachment(osv.osv_memory):
    _name = 'account.invoice.attachment'
    _description = 'Wizard para Adjuntar y Enviar CFDI'
    _columns = {

        'cfdi_normal': fields.boolean('Factura CFDI'),
        'cfdi_transp': fields.boolean('Factura CFDI Transporte'),
        'cfdi_nomina': fields.boolean('Nomina CFDI'),
        'cfdi_nota': fields.boolean('Nota de Credito CFDI'),
    }
    _defaults = {  
        'cfdi_transp': True,
        }
    def attachment_invoice_cfdi_transporte(self, cr, uid, ids, factura_id, file_xml_sign, file_xml_sign_name, context=None):
        active_id = factura_id if factura_id else False
        account_obj = self.pool.get('account.invoice')
        ### EJECUTANDO EL METODO QUE PERMITE ESCRIBIR DATOS DESDE EL XML
        
        for factura in account_obj.browse(cr, uid, [active_id], context=None):
            attachment_xml_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',factura.id),('name','like','.xml')], context=context)
            if attachment_xml_ids:
                attach_obj = self.pool.get('ir.attachment')
                attach_browse = attach_obj.browse(cr, uid, attachment_xml_ids, context=None)[0]
                cfd_data = base64.decodestring(attach_browse.datas)
                if cfd_data:
                    cfdi_minidom = minidom.parseString(cfd_data)
                    # getElementsByTagName es un metodo que permite extraer partes de codigo
                    # pasandole como parametro el nombre de la etiqueta <xxx
                    # getAttribute() metodo que extrae las propiedades de esa etiqueta como 
                    # Emisor, RFC, etc...
                    # Todos los Resultados del Minimon son regresados en forma de lista [ '<x id=''></>',]
                    node = cfdi_minidom.getElementsByTagName('cfdi:Comprobante')[0]
                    subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')
                    if subnode:
                        subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')[0]
                        cfdi_folio_fiscal = subnode.getAttribute('UUID') if subnode.getAttribute('UUID') else ''
                        cfdi_fecha_timbrado = subnode.getAttribute('FechaTimbrado') if subnode.getAttribute('FechaTimbrado') else ''
                        cfdi_no_certificado = subnode.getAttribute('noCertificadoSAT') if subnode.getAttribute('noCertificadoSAT') else ''
                        cfdi_sello = subnode.getAttribute('selloSAT') if subnode.getAttribute('selloSAT') else ''
                        sello_CFD = subnode.getAttribute('selloCFD') if subnode.getAttribute('selloCFD') else ''
                        cfdi_cadena_original = '||1.0|'+cfdi_folio_fiscal+'|'+cfdi_fecha_timbrado+'|'+sello_CFD+'|'+cfdi_no_certificado+'||'
                        factura.write({ 'cfdi_folio_fiscal':cfdi_folio_fiscal,
                                        'cfdi_fecha_timbrado':cfdi_fecha_timbrado,
                                        'cfdi_no_certificado':cfdi_no_certificado,
                                        'cfdi_sello':cfdi_sello,
                                        'cfdi_cadena_original':cfdi_cadena_original})
                        if factura.type=='out_invoice':
                            company_vat = factura.company_emitter_id.address_invoice_parent_company_id.vat
                            rfc_transmitter = company_vat.replace('MX','') if company_vat else ''
                            customer_vat = factura.partner_id.vat
                            rfc_receiver = customer_vat.replace('MX','')  if customer_vat else ''
                            amount_total = zfill("%0.6f"%factura.amount_total,17) if factura.amount_total else ''
                            if not cfdi_folio_fiscal:
                                qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+''

                                qr_string = qrstr
                                qr_code = QR(data=qr_string.encode('utf-8'))
                                try:
                                    qr_code.encode()
                                except Exception, e:
                                    raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                                
                                qr_file = open(qr_code.filename, "rb")
                                temp_bytes = qr_file.read()
                                qr_bytes = base64.encodestring(temp_bytes)
                                qr_file.close()

                                factura.write({'cfdi_cbb':qr_bytes})

                            qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+cfdi_folio_fiscal

                            qr_string = qrstr
                            qr_code = QR(data=qr_string.encode('utf-8'))
                            try:
                                qr_code.encode()
                            except Exception, e:
                                raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                            
                            qr_file = open(qr_code.filename, "rb")
                            temp_bytes = qr_file.read()
                            qr_bytes = base64.encodestring(temp_bytes)
                            qr_file.close()

                            factura.write({'cfdi_cbb':qr_bytes})  
                        ### Borrar el Reporte PDF de la Factura
                        # pdf_name = attach_browse.name.split('.')[0]+'.pdf'
                        # attach_id = attach_obj.search(cr, uid, [('name','=',pdf_name)])
                        # if attach_id:
                        #     attach_obj.unlink(cr, uid, attach_id, context=None)

        #### --- DICCCIONARIO VALUE EN DONDE CREAMOS UN ACTION REPORT EN EN EL MODELO TYPE --- ######
        value = {
            'type': 'ir.actions.report.xml', #### INDICAMOS QUE EL RETORNO ES DEL MODELO ir.actions.report.xml
            'report_name': 'Factura-2013-Final', #### NOMBRE DEL REPORTE CON EL QUE LO GUARDAMOS EN LA CARPETA REPORT SIN LA EXTENSION .jrxml EN MI CASO SE LLAMA Factura-2013-Final.jrxml ES IMPORTANTE QUE SE ENCUENTRE DENTRO DE LA CARPETA REPORT DEL MODULO
            'datas': {
                        'model' : 'account.invoice', #### MODELO DEL CUAL OBTENDRA LA INFORMACION EL REPORTE
                        'ids'   : [active_id], ### INDICAMOS LOS IDS DE LOS CUALES TOMARA LA INFORMACION EL REPORTE
                        }
                    }

        #### --- CONSTRUIMOS EL NOMBRE PARA EL REPORTE --- ####
        file_name = ""
        report_name = value['report_name']

        
        file_name = file_xml_sign_name if file_xml_sign_name else "Factura CFDI "

        #### --- BUSCAMOS QUE EL REGISTRO DE LA FACTURA NO TENGA ADJUNTO UN ARCHIVO PDF PREVIAMENTE CARGADO CON ESTA FUNCION --- ####
        attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [("name","=",file_name+'.pdf')], limit=1, context=context)

        #### --- EN CASO DE QUE NO TENGA UN ARCHIVO ADJUNTO ENTONCES PROCEDEMOS A CREARLO PARA ELLO HACEMOS USO DE LA FUNCION CREATE_REPORT_JASPER --- ####
        if not attachment_ids:
            if not context:
                context = {}
            id = ids[0]
            (fileno, fname) = tempfile.mkstemp(
                '.pdf', 'openerp_' + (False or '') + '__facturae__')
            os.close(fileno)
            file = self.create_report_jasper(cr, uid, [active_id], 'Factura-2013-Final', file_name) ### USAMOS LA FUNCION CREATE_REPORT_JASPER Y LOS PARAMETROS A RECIBIR SON IDS A DONDE CREARA EL ADJUNTO,EL NOMBRE DEL REPORTE RML O JASPER Y EL NOMBRE PARA CREAR EL REPORTE
            is_file = file[0]
            fname = file[1]
            if is_file and os.path.isfile(fname):
                f = open(fname, "r")
                data = f.read()
                f.close()
                # datas = data and base64.encodestring(data)
                # print "##################################################### DATASSSSS", datas
                data_attach = {
                    'name': file_name+'.pdf',
                    'datas_fname': file_name+'.pdf',
                    'description': 'Factura-CFDI PDF',
                    'res_model': 'account.invoice',
                    'res_id': active_id,
                    'type': 'binary',
                    'datas': data and base64.encodestring(data) or None,
                    'store_fname': file_name+'.pdf',
                }
                attachment_id = self.pool.get('ir.attachment').create( cr, uid, data_attach, context=context)
                if attachment_id:
                    wizard_active_id = context and context.get('active_id', False)
                    attachment_obj = self.pool.get('ir.attachment.facturae.mx')
                    msj = _("Attached Successfully PDF\n")
                    for facturae in attachment_obj.browse(cr, uid, [wizard_active_id], context=None):
                        facturae.write({
                                    'file_pdf': attachment_id,
                                    'msj': msj,
                                    'last_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                    'file_pdf_index': ''})
                    # attach_obj.write(cr, uid, [wizard_active_id], {'file_pdf':attachment_id}, context=None) 
                    wf_service = netsvc.LocalService("workflow")
                    wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', wizard_active_id, 'action_printable', cr)


        return value

    ##################### NOTA DE CREDITO

    def attachment_invoice_cfdi_nota(self, cr, uid, ids, factura_id, file_xml_sign, file_xml_sign_name, context=None):
        active_id = factura_id if factura_id else False
        account_obj = self.pool.get('account.invoice')
        ### EJECUTANDO EL METODO QUE PERMITE ESCRIBIR DATOS DESDE EL XML
        for factura in account_obj.browse(cr, uid, [active_id], context=None):
            attachment_xml_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',factura.id),('name','like','.xml')], context=context)
            if attachment_xml_ids:
                attach_obj = self.pool.get('ir.attachment')
                attach_browse = attach_obj.browse(cr, uid, attachment_xml_ids, context=None)[0]
                cfd_data = base64.decodestring(attach_browse.datas)
                if cfd_data:
                    cfdi_minidom = minidom.parseString(cfd_data)
                    # getElementsByTagName es un metodo que permite extraer partes de codigo
                    # pasandole como parametro el nombre de la etiqueta <xxx
                    # getAttribute() metodo que extrae las propiedades de esa etiqueta como 
                    # Emisor, RFC, etc...
                    # Todos los Resultados del Minimon son regresados en forma de lista [ '<x id=''></>',]
                    node = cfdi_minidom.getElementsByTagName('cfdi:Comprobante')[0]
                    subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')
                    if subnode:
                        subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')[0]
                        cfdi_folio_fiscal = subnode.getAttribute('UUID') if subnode.getAttribute('UUID') else ''
                        cfdi_fecha_timbrado = subnode.getAttribute('FechaTimbrado') if subnode.getAttribute('FechaTimbrado') else ''
                        cfdi_no_certificado = subnode.getAttribute('noCertificadoSAT') if subnode.getAttribute('noCertificadoSAT') else ''
                        cfdi_sello = subnode.getAttribute('selloSAT') if subnode.getAttribute('selloSAT') else ''
                        sello_CFD = subnode.getAttribute('selloCFD') if subnode.getAttribute('selloCFD') else ''
                        cfdi_cadena_original = '||1.0|'+cfdi_folio_fiscal+'|'+cfdi_fecha_timbrado+'|'+sello_CFD+'|'+cfdi_no_certificado+'||'
                        factura.write({ 'cfdi_folio_fiscal':cfdi_folio_fiscal,
                                        'cfdi_fecha_timbrado':cfdi_fecha_timbrado,
                                        'cfdi_no_certificado':cfdi_no_certificado,
                                        'cfdi_sello':cfdi_sello,
                                        'cfdi_cadena_original':cfdi_cadena_original})
                        if factura.type=='out_refund':
                            company_vat = factura.company_emitter_id.address_invoice_parent_company_id.vat
                            rfc_transmitter = company_vat.replace('MX','') if company_vat else ''
                            customer_vat = factura.partner_id.vat
                            rfc_receiver = customer_vat.replace('MX','')  if customer_vat else ''
                            amount_total = zfill("%0.6f"%factura.amount_total,17) if factura.amount_total else ''
                            if not cfdi_folio_fiscal:
                                qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+''

                                qr_string = qrstr
                                qr_code = QR(data=qr_string.encode('utf-8'))
                                try:
                                    qr_code.encode()
                                except Exception, e:
                                    raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                                
                                qr_file = open(qr_code.filename, "rb")
                                temp_bytes = qr_file.read()
                                qr_bytes = base64.encodestring(temp_bytes)
                                qr_file.close()

                                factura.write({'cfdi_cbb':qr_bytes})

                            qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+cfdi_folio_fiscal

                            qr_string = qrstr
                            qr_code = QR(data=qr_string.encode('utf-8'))
                            try:
                                qr_code.encode()
                            except Exception, e:
                                raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                            
                            qr_file = open(qr_code.filename, "rb")
                            temp_bytes = qr_file.read()
                            qr_bytes = base64.encodestring(temp_bytes)
                            qr_file.close()

                            factura.write({'cfdi_cbb':qr_bytes})  
                        ### Borrar el Reporte PDF de la Factura
                        # pdf_name = attach_browse.name.split('.')[0]+'.pdf'
                        # attach_id = attach_obj.search(cr, uid, [('name','=',pdf_name)])
                        # if attach_id:
                        #     attach_obj.unlink(cr, uid, attach_id, context=None)

        #### --- DICCCIONARIO VALUE EN DONDE CREAMOS UN ACTION REPORT EN EN EL MODELO TYPE --- ######
        value = {
            'type': 'ir.actions.report.xml', #### INDICAMOS QUE EL RETORNO ES DEL MODELO ir.actions.report.xml
            'report_name': 'Nota-2013-Final', #### NOMBRE DEL REPORTE CON EL QUE LO GUARDAMOS EN LA CARPETA REPORT SIN LA EXTENSION .jrxml EN MI CASO SE LLAMA Factura-2013-Final.jrxml ES IMPORTANTE QUE SE ENCUENTRE DENTRO DE LA CARPETA REPORT DEL MODULO
            'datas': {
                        'model' : 'account.invoice', #### MODELO DEL CUAL OBTENDRA LA INFORMACION EL REPORTE
                        'ids'   : [active_id], ### INDICAMOS LOS IDS DE LOS CUALES TOMARA LA INFORMACION EL REPORTE
                        }
                    }

        #### --- CONSTRUIMOS EL NOMBRE PARA EL REPORTE --- ####
        file_name = ""
        report_name = value['report_name']

        
        file_name = file_xml_sign_name if file_xml_sign_name else "Nomina CFDI "

        #### --- BUSCAMOS QUE EL REGISTRO DE LA FACTURA NO TENGA ADJUNTO UN ARCHIVO PDF PREVIAMENTE CARGADO CON ESTA FUNCION --- ####
        attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [("name","=",file_name+'.pdf')], limit=1, context=context)

        #### --- EN CASO DE QUE NO TENGA UN ARCHIVO ADJUNTO ENTONCES PROCEDEMOS A CREARLO PARA ELLO HACEMOS USO DE LA FUNCION CREATE_REPORT_JASPER --- ####
        if not attachment_ids:
            if not context:
                context = {}
            id = ids[0]
            (fileno, fname) = tempfile.mkstemp(
                '.pdf', 'openerp_' + (False or '') + '__facturae__')
            os.close(fileno)
            file = self.create_report_jasper(cr, uid, [active_id], 'Nota-2013-Final', file_name) ### USAMOS LA FUNCION CREATE_REPORT_JASPER Y LOS PARAMETROS A RECIBIR SON IDS A DONDE CREARA EL ADJUNTO,EL NOMBRE DEL REPORTE RML O JASPER Y EL NOMBRE PARA CREAR EL REPORTE
            is_file = file[0]
            fname = file[1]
            if is_file and os.path.isfile(fname):
                f = open(fname, "r")
                data = f.read()
                f.close()
                # datas = data and base64.encodestring(data)
                # print "##################################################### DATASSSSS", datas
                data_attach = {
                    'name': file_name+'.pdf',
                    'datas_fname': file_name+'.pdf',
                    'description': 'Nota-CFDI PDF',
                    'res_model': 'account.invoice',
                    'res_id': active_id,
                    'type': 'binary',
                    'datas': data and base64.encodestring(data) or None,
                    'store_fname': file_name+'.pdf',
                }
                attachment_id = self.pool.get('ir.attachment').create( cr, uid, data_attach, context=context)
                if attachment_id:
                    wizard_active_id = context and context.get('active_id', False)
                    attachment_obj = self.pool.get('ir.attachment.facturae.mx')
                    msj = _("Attached Successfully PDF\n")
                    for facturae in attachment_obj.browse(cr, uid, [wizard_active_id], context=None):
                        facturae.write({
                                    'file_pdf': attachment_id,
                                    'msj': msj,
                                    'last_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                    'file_pdf_index': ''})
                    # attach_obj.write(cr, uid, [wizard_active_id], {'file_pdf':attachment_id}, context=None) 
                    wf_service = netsvc.LocalService("workflow")
                    wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', wizard_active_id, 'action_printable', cr)


        return value

    def attachment_invoice_cfdi_normal(self, cr, uid, ids, factura_id, file_xml_sign, file_xml_sign_name, context=None):
        active_id = factura_id if factura_id else False
        account_obj = self.pool.get('account.invoice')

        for factura in account_obj.browse(cr, uid, [active_id], context=None):
            attachment_xml_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',factura.id),('name','like','.xml')], context=context)
            if attachment_xml_ids:
                attach_obj = self.pool.get('ir.attachment')
                attach_browse = attach_obj.browse(cr, uid, attachment_xml_ids, context=None)[0]
                cfd_data = base64.decodestring(attach_browse.datas)
                if cfd_data:
                    cfdi_minidom = minidom.parseString(cfd_data)
                    # getElementsByTagName es un metodo que permite extraer partes de codigo
                    # pasandole como parametro el nombre de la etiqueta <xxx
                    # getAttribute() metodo que extrae las propiedades de esa etiqueta como 
                    # Emisor, RFC, etc...
                    # Todos los Resultados del Minimon son regresados en forma de lista [ '<x id=''></>',]
                    node = cfdi_minidom.getElementsByTagName('cfdi:Comprobante')[0]
                    subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')
                    if subnode:
                        subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')[0]
                        cfdi_folio_fiscal = subnode.getAttribute('UUID') if subnode.getAttribute('UUID') else ''
                        cfdi_fecha_timbrado = subnode.getAttribute('FechaTimbrado') if subnode.getAttribute('FechaTimbrado') else ''
                        cfdi_no_certificado = subnode.getAttribute('noCertificadoSAT') if subnode.getAttribute('noCertificadoSAT') else ''
                        cfdi_sello = subnode.getAttribute('selloSAT') if subnode.getAttribute('selloSAT') else ''
                        sello_CFD = subnode.getAttribute('selloCFD') if subnode.getAttribute('selloCFD') else ''
                        cfdi_cadena_original = '||1.0|'+cfdi_folio_fiscal+'|'+cfdi_fecha_timbrado+'|'+sello_CFD+'|'+cfdi_no_certificado+'||'
                        factura.write({ 'cfdi_folio_fiscal':cfdi_folio_fiscal,
                                        'cfdi_fecha_timbrado':cfdi_fecha_timbrado,
                                        'cfdi_no_certificado':cfdi_no_certificado,
                                        'cfdi_sello':cfdi_sello,
                                        'cfdi_cadena_original':cfdi_cadena_original})
                        
                        ## Borrar el Reporte PDF de la Factura
                        # pdf_name = attach_browse.name.split('.')[0]+'.pdf'
                        # attach_id = attach_obj.search(cr, uid, [('name','=',pdf_name)])
                        # if attach_id:
                        #     attach_obj.unlink(cr, uid, attach_id, context=None)
                        if factura.type=='out_invoice':
                            company_vat = factura.company_emitter_id.address_invoice_parent_company_id.vat
                            rfc_transmitter = company_vat.replace('MX','') if company_vat else ''
                            customer_vat = factura.partner_id.vat
                            rfc_receiver = customer_vat.replace('MX','')  if customer_vat else ''
                            amount_total = zfill("%0.6f"%factura.amount_total,17) if factura.amount_total else ''
                            if not cfdi_folio_fiscal:
                                qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+''

                                qr_string = qrstr
                                qr_code = QR(data=qr_string.encode('utf-8'))
                                try:
                                    qr_code.encode()
                                except Exception, e:
                                    raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                                
                                qr_file = open(qr_code.filename, "rb")
                                temp_bytes = qr_file.read()
                                qr_bytes = base64.encodestring(temp_bytes)
                                qr_file.close()

                                factura.write({'cfdi_cbb':qr_bytes})

                            qrstr = "?re="+rfc_transmitter+"&rr="+rfc_receiver+"&tt="+str(amount_total)+"&id="+cfdi_folio_fiscal

                            qr_string = qrstr
                            qr_code = QR(data=qr_string.encode('utf-8'))
                            try:
                                qr_code.encode()
                            except Exception, e:
                                raise osv.except_osv(_('Warning'), _('Could not create QR Code image.\nError %s') % e)
                            
                            qr_file = open(qr_code.filename, "rb")
                            temp_bytes = qr_file.read()
                            qr_bytes = base64.encodestring(temp_bytes)
                            qr_file.close()

                            factura.write({'cfdi_cbb':qr_bytes})
        #### --- DICCCIONARIO VALUE EN DONDE CREAMOS UN ACTION REPORT EN EN EL MODELO TYPE --- ######
        value = {
            'type': 'ir.actions.report.xml', #### INDICAMOS QUE EL RETORNO ES DEL MODELO ir.actions.report.xml
            'report_name': 'ir_actions_report_xml_file_75', #### NOMBRE DEL REPORTE CON EL QUE LO GUARDAMOS EN LA CARPETA REPORT SIN LA EXTENSION .jrxml EN MI CASO SE LLAMA Factura-2013-Final.jrxml ES IMPORTANTE QUE SE ENCUENTRE DENTRO DE LA CARPETA REPORT DEL MODULO
            'datas': {
                        'model' : 'account.invoice', #### MODELO DEL CUAL OBTENDRA LA INFORMACION EL REPORTE
                        'ids'   : [active_id], ### INDICAMOS LOS IDS DE LOS CUALES TOMARA LA INFORMACION EL REPORTE
                        }
                    }

        #### --- CONSTRUIMOS EL NOMBRE PARA EL REPORTE --- ####
        file_name = ""
        report_name = value['report_name']

        file_name = file_xml_sign_name if file_xml_sign_name else "Factura CFDI "


        #### --- BUSCAMOS QUE EL REGISTRO DE LA FACTURA NO TENGA ADJUNTO UN ARCHIVO PDF PREVIAMENTE CARGADO CON ESTA FUNCION --- ####
        attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [("name","=",file_name+'.pdf')], limit=1, context=context)

        #### --- EN CASO DE QUE NO TENGA UN ARCHIVO ADJUNTO ENTONCES PROCEDEMOS A CREARLO PARA ELLO HACEMOS USO DE LA FUNCION CREATE_REPORT_JASPER --- ####
        if not attachment_ids:
            if not context:
                context = {}
            id = ids[0]
            (fileno, fname) = tempfile.mkstemp(
                '.pdf', 'openerp_' + (False or '') + '__facturae__')
            os.close(fileno)
            file = self.create_report_jasper(cr, uid, [active_id], 'ir_actions_report_xml_file_75', file_name) ### USAMOS LA FUNCION CREATE_REPORT_JASPER Y LOS PARAMETROS A RECIBIR SON IDS A DONDE CREARA EL ADJUNTO,EL NOMBRE DEL REPORTE RML O JASPER Y EL NOMBRE PARA CREAR EL REPORTE
            is_file = file[0]
            fname = file[1]
            if is_file and os.path.isfile(fname):
                f = open(fname, "r")
                data = f.read()
                f.close()
                # datas = data and base64.encodestring(data)
                # print "##################################################### DATASSSSS", datas
                data_attach = {
                    'name': file_name+'.pdf',
                    'datas_fname': file_name+'.pdf',
                    'description': 'ir_actions_report_xml_file_75 PDF',
                    'res_model': 'account.invoice',
                    'res_id': active_id,
                    'type': 'binary',
                    'datas': data and base64.encodestring(data) or None,
                    'store_fname': file_name+'.pdf',
                }
                
                attachment_id = self.pool.get('ir.attachment').create( cr, uid, data_attach, context=context)
                if attachment_id:
                    wizard_active_id = context and context.get('active_id', False)
                    attachment_obj = self.pool.get('ir.attachment.facturae.mx')
                    msj = _("Attached Successfully PDF\n")
                    for facturae in attachment_obj.browse(cr, uid, [wizard_active_id], context=None):
                        facturae.write({
                                    'file_pdf': attachment_id,
                                    'msj': msj,
                                    'last_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                    'file_pdf_index': ''})
                    # attach_obj.write(cr, uid, [wizard_active_id], {'file_pdf':attachment_id}, context=None) 
                    wf_service = netsvc.LocalService("workflow")
                    wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', wizard_active_id, 'action_printable', cr)

        return value

    def create_report_jasper(self, cr, uid, res_ids, report_name=False, file_name=False):
        #### --- ESTA FUNCION CREA Y PROCESA EL REPORTE PARA PODER TENERLO EN MEMORIA TEMPORALMENTE --- ####
        if not report_name or not res_ids:
            return (False, Exception('Report name and Resources ids are required !!!'))
        ret_file_name = '/tmp/'+file_name+'.pdf' #### NOMBRE TEMPORAL DEL REPORTE
        service = netsvc.LocalService("report."+report_name) #### EJECUTAMOS NETSVC PARA EJECUAL PROCESOS LOCALES DE OPENERP EN ESTE CASO SIMULAMOS LA IMPRESION DEL REPORTE
        (result, format) = service.create(cr, uid, res_ids, {'model' : 'account.invoice'}, {})
        fp = open(ret_file_name, 'wb+');
        fp.write(result);
        fp.close();
        return (True, ret_file_name)

    def attachment_invoice(self, cr, uid, ids, context=None):
        value = {}

        for rec in self.browse(cr, uid, ids, context=context):           
            active_id = context and context.get('active_id', False)
            attachment_obj = self.pool.get('ir.attachment.facturae.mx')
            attach_browse = attachment_obj.browse(cr, uid, [active_id], context=None)[0]
            if attach_browse.model_source == 'account.invoice':
                if rec.cfdi_nomina:
                    raise osv.except_osv(_('Error'), _('Seleccione Una de las Opciones \n -Factura CFDI\n -Factura CFDI Transporte ó\n Contacte al Administrador'))
                factura_id = attach_browse.id_source
                file_xml_sign =  attach_browse.file_xml_sign.id
                file_xml_sign_name = attach_browse.file_xml_sign.name.split('.')[0]
                if not factura_id:
                    xml_name = attach_browse.name
                    account_obj = self.pool.get('account.invoice')
                    try:
                        factura_name = xml_name.split('_')[-1]
                        factura_s = account_obj.search(cr, uid, [('internal_number','=',factura_name)])
                        factura_id = factura_s[0] if factura_s else []
                        if not factura_id:
                            raise osv.except_osv(_('Error'), _('No se pudo Adjuntar la Factura No tiene Relacion con el campo invoice_id.\n Contacte al Administrador'))
                    except:
                        raise osv.except_osv(_('Error'), _('No se pudo Adjuntar la Factura No tiene Relacion con el campo invoice_id.\n Contacte al Administrador'))
                if rec.cfdi_normal:
                    value = rec.attachment_invoice_cfdi_normal(factura_id,file_xml_sign,file_xml_sign_name)
                elif rec.cfdi_transp:
                    value = rec.attachment_invoice_cfdi_transporte(factura_id,file_xml_sign,file_xml_sign_name)
                elif rec.cfdi_nota:
                    value = rec.attachment_invoice_cfdi_nota(factura_id,file_xml_sign,file_xml_sign_name)

            elif attach_browse.model_source == 'hr.payslip':
                if rec.cfdi_normal or rec.cfdi_transp or rec.cfdi_nota:
                    raise osv.except_osv(_('Error'), _('Seleccione Una de las Opciones \n -Factura CFDI\n -Factura CFDI Transporte ó\n Contacte al Administrador'))
                if rec.cfdi_nomina:
                    # attachment_obj.signal_printable(cr, uid, [active_id], context=context)
                    attach_browse.signal_printable()
        return value

account_invoice_attachment()

#### HERENCIA DEL MODELO QUE ENVIA Y GENERA LA FACTURACION ELECTRONICA PARA AGREGAR FUNCIONES
#### COMO ENVIAR POR CORREO, ETC, LIGANDOLO AL FLUJO NORMAL
class ir_attachment_facturae_mx(osv.osv):
    _name = 'ir.attachment.facturae.mx'
    _inherit ='ir.attachment.facturae.mx'
    _columns = {
        }

    def action_invoice_sent_jasper(self, cr, uid, ids, context=None):
        '''
        This function opens a window to compose an email, with the edi invoice template message loaded by default
        '''
        #self.attachment_invoice(cr, uid, ids, context=None)
        attachment_obj = self.pool.get('ir.attachment.facturae.mx')
        attach_browse = self.browse(cr, uid, ids, context=None)[0]
        if attach_browse.model_source == 'account.invoice':
            account_obj = self.pool.get('account.invoice')
            factura_id = [attach_browse.id_source]
            if not attach_browse.id_source:
                xml_name = attach_browse.name
                account_obj = self.pool.get('account.invoice')
                factura_name = xml_name.split('_')[-1]
                factura_s = account_obj.search(cr, uid, [('internal_number','=',factura_name)])
                factura_id = factura_s if factura_s else []
                if not factura_id:
                    raise osv.except_osv(_('Error'), _('No se pudo Enviar la Factura No tiene Relacion con el campo invoice_id o el Nombre no corresponde.\n Contacte al Administrador'))

            assert len(factura_id) == 1, 'This option should only be used for a single id at a time.'
            ir_model_data = self.pool.get('ir.model.data')
            try:
                template_id = ir_model_data.get_object_reference(cr, uid, 'hesatec_attachment_cfdi_jasper_tms', 'email_template_edi_invoice_jasper')[1]
            except ValueError:
                template_id = False
            try:
                compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
            except ValueError:
                compose_form_id = False 
            ctx = dict(context)
            # file_name = ""
            # for rec in account_obj.browse(cr, uid, factura_id, context=context):
            #     vat = rec.company_emitter_id.address_invoice_parent_company_id.vat.replace('MX','')
            #     if vat:
            #         file_name = vat + '_CFDI_'+str(rec.number)
            #     else:
            #         file_name = "Factura CFDI "+str(rec.number)
            # attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',ids[0])], context=context)
            
            ctx.update({
                'default_model': 'account.invoice',
                'default_res_id': factura_id[0],
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'mark_invoice_as_sent': True,
    #            'attachment_ids': [x for x in attachment_ids],
                })
            wf_service = netsvc.LocalService("workflow")
            if attach_browse.state == 'printable':
                wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', ids[0], 'action_send_customer', cr)
            elif attach_browse.state == 'sent_customer':
                wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', ids[0], 'action_send_backup', cr)
            return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'mail.compose.message',
                    'views': [(compose_form_id, 'form')],
                    'view_id': compose_form_id,
                    'target': 'new',
                    'context': ctx,
                    }
        elif attach_browse.model_source == 'hr.payslip':
            payslip_obj = self.pool.get('hr.payslip')
            payslip_id = [attach_browse.id_source]
            if not attach_browse.id_source:
                xml_name = attach_browse.name
                payslip_obj = self.pool.get('hr.payslip')
                factura_name = xml_name
                payslip_s = payslip_obj.search(cr, uid, [('number','=',factura_name)])
                payslip_id = payslip_s if payslip_s else []
                if not payslip_id:
                    raise osv.except_osv(_('Error'), _('No se pudo Enviar el CFDI del Empleado, No se tiene Relacion con el campo invoice_id o el Nombre no corresponde.\n Contacte al Administrador'))

            assert len(payslip_id) == 1, 'This option should only be used for a single id at a time.'
            ir_model_data = self.pool.get('ir.model.data')
            try:
                template_id = ir_model_data.get_object_reference(cr, uid, 'hesatec_attachment_cfdi_jasper_tms', 'email_template_edi_payslip_cfdi')[1]
            except ValueError:
                template_id = False
            try:
                compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
            except ValueError:
                compose_form_id = False 
            ctx = dict(context)

            ctx.update({
                'default_model': 'hr.payslip',
                'default_res_id': payslip_id[0],
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'mark_invoice_as_sent': True,
    #            'attachment_ids': [x for x in attachment_ids],
                })
            wf_service = netsvc.LocalService("workflow")
            if attach_browse.state == 'printable':
                wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', ids[0], 'action_send_customer', cr)
            elif attach_browse.state == 'sent_customer':
                wf_service.trg_validate(uid, 'ir.attachment.facturae.mx', ids[0], 'action_send_backup', cr)
            return {
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'mail.compose.message',
                    'views': [(compose_form_id, 'form')],
                    'view_id': compose_form_id,
                    'target': 'new',
                    'context': ctx,
                    }
            # if attach_browse.state == 'printable':
            #     attach_browse.signal_send_customer()
            # elif attach_browse.state == 'sent_customer':
            #     attach_browse.signal_send_backup()
        return  {'type': 'ir.actions.act_window_close'}

    _default = {
        }
ir_attachment_facturae_mx ()
#### ASISTENTE PARA ACTUALIZAR LOS CAMPOS CFDI DESDE EL XML UTILIZANDO EL MINIDOM PARA LOS ATRIBUTOS Y TAGS

class  invoice_update_cfdi_fields(osv.osv_memory):
    _name = 'invoice.update.cfdi.fields'
    _description = 'Auxiliar para Actualizar los campos CFDI desde el XML'
    _columns = {

        'comment': fields.text('Numeros de Facturas', help='Introduce todos los Numeros de Facturas, separados por comas Ej. 1,2,3,4,5,6,7' ),

    }

    def update_fields(self, cr, uid, ids, context=None):
        ### ESTA FUNCION PRETENDE CORREGIR ALGUNOS ERRORES DEL CFDI LEYENDO
        ### EL XML Y EXTRAYENDO INFORMACION PARA PASARLA A OPENERP

        for rec in self.browse(cr, uid, ids, context=None):
            lista_actualizar = rec.comment
            lista_facturas = []
            for x in lista_actualizar.split(','):
                lista_facturas.append(x)
            tupla_facturas = tuple(lista_facturas)
            invoice_obj = self.pool.get('account.invoice')
            facturas_ids = invoice_obj.search(cr, uid, [('number','in',tupla_facturas)])
            for factura in invoice_obj.browse(cr, uid, facturas_ids, context=None):
                attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('res_model','=','account.invoice'),('res_id','=',factura.id),('name','like','.xml')], context=context)
                if attachment_ids:
                    attach_obj = self.pool.get('ir.attachment')
                    attach_browse = attach_obj.browse(cr, uid, attachment_ids, context=None)[0]
                    cfd_data = base64.decodestring(attach_browse.datas)
                    if cfd_data:
                        cfdi_minidom = minidom.parseString(cfd_data)
                        # getElementsByTagName es un metodo que permite extraer partes de codigo
                        # pasandole como parametro el nombre de la etiqueta <xxx
                        # getAttribute() metodo que extrae las propiedades de esa etiqueta como 
                        # Emisor, RFC, etc...
                        # Todos los Resultados del Minimon son regresados en forma de lista [ '<x id=''></>',]
                        node = cfdi_minidom.getElementsByTagName('cfdi:Comprobante')[0]
                        subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')
                        if subnode:
                            subnode = cfdi_minidom.getElementsByTagName('tfd:TimbreFiscalDigital')[0]
                            cfdi_folio_fiscal = subnode.getAttribute('UUID') if subnode.getAttribute('UUID') else ''
                            cfdi_fecha_timbrado = subnode.getAttribute('FechaTimbrado') if subnode.getAttribute('FechaTimbrado') else ''
                            cfdi_no_certificado = subnode.getAttribute('noCertificadoSAT') if subnode.getAttribute('noCertificadoSAT') else ''
                            cfdi_sello = subnode.getAttribute('selloSAT') if subnode.getAttribute('selloSAT') else ''
                            sello_CFD = subnode.getAttribute('selloCFD') if subnode.getAttribute('selloCFD') else ''
                            cfdi_cadena_original = '||1.0|'+cfdi_folio_fiscal+'|'+cfdi_fecha_timbrado+'|'+sello_CFD+'|'+cfdi_no_certificado+'||'
                            factura.write({ 'cfdi_folio_fiscal':cfdi_folio_fiscal,
                                            'cfdi_fecha_timbrado':cfdi_fecha_timbrado,
                                            'cfdi_no_certificado':cfdi_no_certificado,
                                            'cfdi_sello':cfdi_sello,
                                            'cfdi_cadena_original':cfdi_cadena_original})
                            
                            ## Borrar el Reporte PDF de la Factura
                            pdf_name = attach_browse.name.split('.')[0]+'.pdf'
                            attach_id = attach_obj.search(cr, uid, [('name','=',pdf_name)])
                            # if attach_id:
                            #     attach_obj.unlink(cr, uid, attach_id, context=None)
        return True

invoice_update_cfdi_fields()

############################### EXTENDIENDO EL hr.payslip PARA AGREGAR LAS REDES SOCIALES ######################################
class hr_payslip(osv.osv):
    _name = 'hr.payslip'
    _inherit = ['mail.thread', 'ir.needaction_mixin', 'hr.payslip']
    _columns = {
    }

hr_payslip()
