# -*- coding: utf-8 -*-
{
    'name': "point_og_sale_serial_numbers",

    'summary': """
    """,

    'description': """
        
    """,

    'author': "tantums",
    'website': "http://www.tantums.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'point of sale',
    'version': '8.0',

    # any module necessary for this one to work correctly
    'depends': ['base','point_of_sale','stock_picking_import_serial__numbers'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [],
}