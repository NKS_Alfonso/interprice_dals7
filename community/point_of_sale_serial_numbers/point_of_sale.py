# -*- coding: utf-8 -*-
import logging
import time

from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product

_logger = logging.getLogger(__name__)

class pos_order(osv.osv):
	_inherit = 'pos.order'

	def action_invoice(self, cr, uid, ids, context=None):
		inv_ref = self.pool.get('account.invoice')
		inv_line_ref = self.pool.get('account.invoice.line')
		product_obj = self.pool.get('product.product')
		inv_ids = []

		for order in self.pool.get('pos.order').browse(cr, uid, ids, context=context):
			if order.invoice_id:
				inv_ids.append(order.invoice_id.id)
				continue

			if not order.partner_id:
				raise osv.except_osv(_('Error!'), _('Please provide a partner for the sale.'))

			acc = order.partner_id.property_account_receivable.id
			inv = {
				'name': order.name,
				'picking_name': order.picking_id.name,
				'origin': order.name,
				'account_id': acc,
				'journal_id': order.sale_journal.id or None,
				'type': 'out_invoice',
				'reference': order.name,
				'partner_id': order.partner_id.id,
				'comment': order.note or '',
				'currency_id': order.pricelist_id.currency_id.id, # considering partner's sale pricelist's currency
			}
			inv.update(inv_ref.onchange_partner_id(cr, uid, [], 'out_invoice', order.partner_id.id)['value'])
			if not inv.get('account_id', None):
				inv['account_id'] = acc
			inv_id = inv_ref.create(cr, uid, inv, context=context)

			self.write(cr, uid, [order.id], {'invoice_id': inv_id, 'state': 'invoiced'}, context=context)
			inv_ids.append(inv_id)
			for line in order.lines:
				inv_line = {
					'invoice_id': inv_id,
					'product_id': line.product_id.id,
					'quantity': line.qty,
				}
				inv_name = product_obj.name_get(cr, uid, [line.product_id.id], context=context)[0][1]
				inv_line.update(inv_line_ref.product_id_change(cr, uid, [],
																line.product_id.id,
																line.product_id.uom_id.id,
																line.qty, partner_id = order.partner_id.id,
																fposition_id=order.partner_id.property_account_position.id)['value'])
				inv_line['price_unit'] = line.price_unit
				inv_line['discount'] = line.discount
				inv_line['name'] = inv_name
				inv_line['invoice_line_tax_id'] = [(6, 0, [x.id for x in line.product_id.taxes_id] )]
				inv_line_ref.create(cr, uid, inv_line, context=context)
			inv_ref.button_reset_taxes(cr, uid, [inv_id], context=context)
			self.signal_workflow(cr, uid, [order.id], 'invoice')
			inv_ref.signal_workflow(cr, uid, [inv_id], 'validate')

		if not inv_ids: return {}

		mod_obj = self.pool.get('ir.model.data')
		res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
		res_id = res and res[1] or False
		return {
			'name': _('Customer Invoice'),
			'view_type': 'form',
			'view_mode': 'form',
			'view_id': [res_id],
			'res_model': 'account.invoice',
			'context': "{'type':'out_invoice'}",
			'type': 'ir.actions.act_window',
			'nodestroy': True,
			'target': 'current',
			'res_id': inv_ids and inv_ids[0] or False,
		} 
		