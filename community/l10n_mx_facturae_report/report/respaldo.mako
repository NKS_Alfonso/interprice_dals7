<!DOCTYPE>
<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>
    %for o in objects :
        ${set_global_data(o)}
        <table class="basic_table">
            <tr>
                <td style="vertical-align: top;">
                    ${helper.embed_image('jpeg',str(o.company_emitter_id.logo),180, 85)}
                </td>
                <td>
                    <table class="basic_table">
                        <tr>
                            <td width='50%'>
                                <div class="title">${o.company_emitter_id.address_invoice_parent_company_id.name or ''}</div>
                            </td>
                            <td width='20%'>
                                %if o.type == 'out_invoice':
                                    <div class="invoice">${_("Factura:")}
                                %elif o.type == 'out_refund':
                                    <div class="refund">${_("NOTA DE CREDITO:")}
                                %endif
                                %if o.type in ['out_invoice', 'out_refund']:
                                    %if o.state in ['open', 'paid', 'cancel']:
                                        ${o.number or ''}
                                    %else:
                                        <font size="2">${'SIN FOLIO O ESTATUS NO VALIDO'}</font>
                                    %endif
                                %endif
                                %if o.state == 'cancel':
                                    ${'FACTURA CANCELADA'}
                                %endif
                            </td>
                        </tr>
                        <tr>
                            <td class="td_data_exp">

                            </td>
                            <td class="td_data_exp">
                                <div class="fiscal_address">
                                    %if o.payment_term.name:
                                        Condicion de pago: ${o.payment_term.note or o.payment_term.name or ''}
                                    %endif
                                    %if o.address_issued_id:
                                    <br/>Expedido en:
                                        ${o.address_issued_id.name or ''}
                                        <br/>${o.address_issued_id.street or ''}
                                        ${o.address_issued_id.l10n_mx_street3 or ''}
                                        ${o.address_issued_id.l10n_mx_street4 or ''}
                                        <br/>${o.address_issued_id.street2 or ''}
                                        ${o.address_issued_id.zip or ''}
                                        <br/>Localidad: ${o.address_issued_id.l10n_mx_city2 or ''}
                                        <br/>${o.address_issued_id.city or ''}
                                        ${o.address_issued_id.state_id.name and ',' or ''} ${o.address_issued_id.state_id and o.address_issued_id.state_id.name or ''}
                                        ${o.address_issued_id.country_id.name and ',' or ''} ${o.address_issued_id.country_id and o.address_issued_id.country_id.name or ''}
                                    %endif
                                <div/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="line" width="100%" border="1"></table>
        <table class="basic_table" style="font-size:11;">
            <tr>
                <td width="80%">
                    <table class="basic_table">
                        <tr>
                            <td class="cliente"><b>Receptor:</b></td>
                            <td width="64%" class="cliente">${get_data_partner(o.partner_id)['name'] or ''}</td>
                            <td class="cliente"><b>R. F. C.:</b></td>
                            <td width="16%" class="cliente"><b>${get_data_partner(o.partner_id)['vat'] or ''}</b></td>
                        </tr>
                    </table>
                    <table class="basic_table">
                        <tr>
                            <td width="7%" class="cliente"><b>Calle:</b></td>
                            <td class="cliente">${get_data_partner(o.partner_id)['street'] or ''}</td>
                            <td width="9%" class="cliente"><b>No. Ext:</b></td>
                            <td width="9%" class="cliente">${get_data_partner(o.partner_id)['l10n_mx_street3'] or ''}</td>
                            <td width="9%" class="cliente"><b>No. Int:</b></td>
                            <td width="9%" class="cliente">${get_data_partner(o.partner_id)['l10n_mx_street4'] or ''}</td>
                        </tr>
                    </table>
                    <table class="basic_table">
                        <tr>
                            <td width="10%" class="cliente"><b>Colonia:</b></td>
                            <td class="cliente">${get_data_partner(o.partner_id)['street2'] or ''}</td>
                            <td width="7%" class="cliente"><b>C.P.:</b></td>
                            <td class="cliente">${get_data_partner(o.partner_id)['zip'] or ''}</td>
                            <td width="12%" class="cliente"><b>Localidad:</b></td>
                            <td class="cliente">${get_data_partner(o.partner_id)['l10n_mx_city2'] or ''}</td>
                        </tr>
                    </table>
                    <table class="basic_table">
                        <tr>
                            <td width="9%" class="cliente"><b>Lugar:</b></td>
                            <td class="cliente">
                                ${get_data_partner(o.partner_id)['city'] or ''},
                                ${get_data_partner(o.partner_id)['state'] or ''},
                                ${get_data_partner(o.partner_id)['country'] or ''}
                            </td>
                        </tr>
                    </table>
                    <table class="basic_table" style="border-bottom:1px solid #002966;">
                        <tr>
                            <td width="13%" class="cliente"><b>Telefono(s):</b></td>
                            <td width="55%" class="cliente">
                                ${get_data_partner(o.partner_id)['phone'] or ''}
                                ${get_data_partner(o.partner_id)['fax'] and ',' or ''}
                                ${get_data_partner(o.partner_id)['fax'] or ''}
                                ${get_data_partner(o.partner_id)['mobile'] and ',' or ''}
                                ${get_data_partner(o.partner_id)['mobile'] or ''}</font>
                            <td width="9%" class="cliente"><b>Origen:</b></td>
                            <td width="23%" class="cliente"><b>${o.origin or ''}</b></td>
                        </tr>
                    </table>
                </td>
                <td width="1%"></td>
                <td width="19%" align="center">
                    %if o.address_issued_id:
                        ${o.address_issued_id.city or ''},
                        ${o.address_issued_id.state_id and o.address_issued_id.state_id.name or ''},
                        ${o.address_issued_id.country_id and o.address_issued_id.country_id.name or ''}
                    %endif
                    <br/>${_("a")} ${o.date_invoice_tz or ''}
                </td>
            </tr>
        </table>
        <br/>
        <table class="basic_table" style="color:#121212">
            <tr class="firstrow">
                <th width="10%">${_("Cant.")}</th>
                <th width="10%">${_("Unidad")}</th>
                <th>${_("Descripci&oacute;n")}</th>
                <th width="9%" >${_("P.Unitario")}</th>
                %if has_disc(o.invoice_line):
                    <th width="8%" >${_("Dto. %") or ''}</th>
                %endif
                <th width="15%">${_("Importe")}</th>
            </tr>
            %for line in o.invoice_line:
                %if (line.id%2==0):
                    <tr  class="nonrow">
                %else:
                    <tr>
                %endif
                    <td width="10%" class="number_td">${line.quantity or '0.0'}</td>
                    <td width="10%" class="basic_td">${line.uos_id.name or ''}</td>
                    <td class="basic_td">${line.name or ''}</td>
                    <td width="9%" class="number_td">$ ${formatLang(line.price_unit) or '0.0'}</td>
                    %if has_disc(o.invoice_line):
                        <td width="8%" class="number_td">${formatLang(line.discount) or ''} %</td>
                    %endif
                    <td width="15%" class="number_td">$ ${formatLang(line.price_subtotal) or '0.0'}</td>
                </tr>
            %endfor
        </table>
        <table align="right" width="30%" style="border-collapse:collapse">
            %if get_taxes() or get_taxes_ret():
                <tr>
                    <td class="total_td">${_("Sub Total:")}</td>
                    <td align="right" class="total_td">$ ${formatLang(o.amount_untaxed) or ''}</td>
                </tr>
            %endif
            %for tax in get_taxes():
                <tr>
                    <td class="tax_td">${tax['name2']} (${round(float(tax['tax_percent']))}) % </td>
                    <td class="tax_td" align="right">$ ${formatLang(float( tax['amount'] ) ) or ''}</td>
                </tr>
            %endfor
            %for tax_ret in get_taxes_ret():
                <tr>
                    <td class="tax_td">${tax_ret['name2']} ${_("Ret")} ${round( float( tax_ret['tax_percent'] ), 2 )*-1 } % </td>
                    <td class="tax_td" align="right">$ ${formatLang( float( tax_ret['amount'] )*-1 ) or ''}</td>
                </tr>
            %endfor
            <tr align="left">
                <td class="total_td"><b>${_("Total:")}</b></td>
                <td class="total_td" align="right"><b>$ ${formatLang(o.amount_total) or ''}</b></td>
            </tr>
        </table>
        <br clear="all" />
        <table class="basic_table">
            <tr>
                <td class="tax_td">
                    ${_("IMPORTE CON LETRA:")}
                </td>
            </tr>
            <tr>
                <td class="center_td">
                    <i>${o.amount_to_text or ''}</i>
                </td>
            </tr>
            <tr>
                <td class="center_td">
                    ${_('PAGO EN UNA SOLA EXHIBICI&Oacute;N - EFECTOS FISCALES AL PAGO')}
                </td>
            </tr>
            
        </table>
        <br/>${o.comment or '' }<br/>
        <br clear="all"/>
        <!--code for cfd-->
        <font class="font">"Este documento es una representacion impresa de un CFDI"
        <br/>CFDI, Comprobante Fiscal Digital por Internet</font>
        <!-- bank info-->
        %if o.company_emitter_id.partner_id.bank_ids:
            <table class="basic_table">
                <tr>
                    <td class="center_td">
                        ${_('Datos Bancarios')} ${o.company_emitter_id.address_invoice_parent_company_id.name or ''}
                    </td>
                </tr>
            </table>
            <table class="basic_table" rules="all">
                <tr>
                    <td class="data_bank_label">${_('Banco / Moneda')}</td>
                    <td class="data_bank_label">${_('N&uacute;mero de cuenta')}</td>
                    <td class="data_bank_label" width="30%">${_('Clave Interbancaria Estandarizada (CLABE)')}</td>
                    <td class="data_bank_label">${_('Referencia')}</td>
                </tr>
                %for f in o.company_emitter_id.partner_id.bank_ids:
                    <tr>
                        <td class="center_td">${f.bank.name or ''} ${f.currency2_id and '/' or '' } ${f.currency2_id and f.currency2_id.name or '' }</td>
                        <td class="center_td">${f.acc_number or ''}</td>
                        <td class="center_td">${f.clabe or ''}</td>
                        <td class="center_td">${f.reference or ''}</td>
                    </tr>
                %endfor
            </table>
        %endif
        <!--code for cfd 3.2-->
        <table class="basic_table" rules="cols" style="border:1.5px solid grey;">
            <tr>
                <th width="33%"> ${_('Certificado del SAT')}</th>
                <th> ${_('Fecha de Timbrado')}</th>
                <th width="33%"> ${_('Folio Fiscal')}</th>
            </tr>
            <tr>
                <td width="33%" class="center_td"> ${ o.cfdi_no_certificado or 'No identificado' }</td>
                <td width="34%" class="center_td"> ${ o.cfdi_fecha_timbrado or 'No identificado' }</td>
                <td width="33%" class="center_td"> ${ o.cfdi_folio_fiscal or 'No identificado' }</td>
            </tr>
        </table>

        <table class="basic_table" rules="cols" style="border:1.5px solid grey;">
            <tr>
                <th width="33%">${_('Certificado del emisor')}</th>
                <th width="34%">${_('M&eacute;todo de Pago')}</th>
                <th width="33%">${_('&Uacute;ltimos 4 d&iacute;gitos de la cuenta bancaria')}</th>
            </tr>
            <tr>
                <td class="center_td">${ o.no_certificado or 'No identificado' }</td>
                <td class="center_td">${ o.pay_method_id.name or 'No identificado' }</td>
                <td class="center_td">
                    ${ o.acc_payment and o.acc_payment.bank_name or '' }
                    ${ o.acc_payment.last_acc_number or 'No identificado' }</td>
            </tr>
        </table>
        <!--code for cfd32-->
        %if 'cfdi' in o.invoice_sequence_id.approval_id.type:
            <div style="page-break-inside:avoid; border:1.5px solid grey;">
                <table width="100%" class="datos_fiscales">
                    <tr>
                        %if o.company_emitter_id.cif_file:
                        <td align="left">
                            ${helper.embed_image('jpeg',str(o.company_emitter_id.cif_file), 140, 220)}
                        </td>
                        %endif
                        <td valign="top" align="left">
                            %if o.company_emitter_id.cif_file == False:
                                <p class="cadena_without_cif">
                            %elif o.cfdi_cbb == False:
                                <p class="cadena_without_cbb">
                            %elif o.cfdi_cbb == False and o.company_emitter_id.cif_file == False:
                                <p class="cadena_without_cbb_cfd">
                            %elif o.cfdi_cbb and o.company_emitter_id.cif_file:
                                <p class="cadena_with_cbb_cfd">
                            %endif
                            <b>${_('Sello Digital Emisor:')} </b><br/>
                            ${split_string( o.sello ) or ''}<br/>
                            <b>${_('Sello Digital SAT:')} </b><br/>
                            ${split_string( o.cfdi_sello or '') or ''}<br/>
                            <b>${_('Cadena original:')} </b><br/>
                            ${split_string(o.cfdi_cadena_original) or ''}</br>
                            <b>${_('Enlace al certificado: ')}</b></br>
                            ${o.pac_id and o.pac_id.certificate_link or ''}</p>
                        </td>
                        %if o.cfdi_cbb:
                        <td align="right">
                            ${helper.embed_image('jpeg',str(o.cfdi_cbb), 180, 180)}
                        </td>
                        %endif
                    </tr>
                </table>
                <!--</span> si se activan, forzan un brinco de linea-->
            </div>
        %endif
        <br></br>
        <section>
            %if o.company_id.dinamic_text:
                <table class="basic_table" style="border:1.5px solid grey;">
                    <tr><td class="address"><pre style='pre'>${ get_text_promissory(o.company_id, o.partner_id, address_emitter, o) or '' }</pre></td></tr>
                </table>
            %endif
        </section>
    <p style="page-break-after:always"></p>
    %endfor

</body>
</html>
 
