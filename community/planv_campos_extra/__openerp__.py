# -*- coding: utf-8 -*-
{
    'name': "planv_campos_extra",

    'summary': '',

    'description': '',

    'author': "TANTUMS",
    'website': "http://www.tantums.com",
    'category': 'general',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'purchase'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'planv_fields_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}