# -*- coding: utf-8 -*-
from openerp import models, fields, api

class planv_campos_extra(models.Model):
    _inherit = 'stock.picking'

    pedimento = fields.Char(string="Pedimento")
    aduana = fields.Char(string="Aduana")
    fecha_pedimento = fields.Date(string="Fecha de entrada")
    
