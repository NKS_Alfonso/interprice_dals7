# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 	* l10n_mx_eaccounting
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 7.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-12-23 18:30+0000\n"
"PO-Revision-Date: 2015-01-05 18:01-0800\n"
"Last-Translator: yukie\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"X-Generator: Poedit 1.5.4\n"

#. module: l10n_mx
#: field:account.account,sat_group_id:0 field:account.account.template,
#: sat_group_id:0
msgid "SAT Group"
msgstr ""

#. module: l10n_mx
#: field:account.account.sat_group,sat_group_parent_id:0
msgid "SAT Group Parent"
msgstr ""

#. module: l10n_mx
#: field:account.account.sat_group,code:0
msgid "Code"
msgstr ""

#. module: l10n_mx
#: field:account.account.sat_group,name:0
msgid "Name"
msgstr ""

#. module: l10n_mx
#: view:account.account.sat_group.tree:0 view:account.account.sat_group.form
#: model:ir.actions.act_window,name:l10n_mx.sat_group_action
#: model:ir.ui.menu,name:l10n_mx.sat_group_menu
#: view:account.account.sat_group:0
msgid "Sat Group"
msgstr ""

#. module: l10n_mx
#: view:account.journal:0
msgid "Bank Accounts"
msgstr ""

#. module: l10n_mx
#: help:account.account,nature:0 code:addons/l10n_mx/account_account.py:38
#: code:addons/l10n_mx/account_account.py:68
#, python-format
msgid "Express the account nature (Debitor or Creditor)"
msgstr ""

#. module: l10n_mx
#: help:account.account,sat_group_id:0
#: code:addons/l10n_mx/account_account.py:43
#: code:addons/l10n_mx/account_account.py:73
#, python-format
msgid "Used for express the group code according to SAT catalog"
msgstr ""
