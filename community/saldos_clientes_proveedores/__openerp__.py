# -*- coding: utf-8 -*-

{
	'name': 'Saldos vencidos de empresas',
	'version': '1.0',
	'category': 'Contabilidad',
	'author': 'Israel Cabrera Juarez(gvdeto)',
	'maintainer': '',
	'website': '',
	#'images' : ['ventas/static/images/ventas.jpg'],
	# 'icon': "ventas/static/src/img/icon.png",
	'installable': True,
	'active': False,
	'description': 'Configuracion',
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base','sale','sale_stock','account_financial_report_webkit_xls','account_financial_report_webkit'
	],
	#XML imports
	'data': [

		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#


		#------------------------------Devoluciones ----------------#
		'secciones/saldo_cliente/saldo_cliente.xml',
		'secciones/saldo_cliente/saldo_proveedor.xml',
		'secciones/saldo_cliente/estado_cuenta_cliente.xml',
		'secciones/saldo_cliente/estado_cuenta_proveedor.xml',







	],
	'css': [

	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
