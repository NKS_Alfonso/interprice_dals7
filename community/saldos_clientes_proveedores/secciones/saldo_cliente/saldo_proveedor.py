# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                                #
#  @creacion    :  (aaaa/mm/dd)                                                                                                                      #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
import base64
import cStringIO
from openerp import models, api, _
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _
import logging
from openerp.tools.safe_eval import safe_eval as eval
import pytz
from openerp import SUPERUSER_ID
_logger = logging.getLogger(__name__)
class saldo_proveedor(osv.osv):
    
    def action_wizard(self, cr, uid, ids, context=None):
        """Funcion que exporta a csv"""
        columna = "\"Proveedor\",\"Dias atrasados\",\"Fecha venciemiento\",\"Tipo documento\",\"Folio\",\"Referencia(s)\",\"Saldo\",\"'01/30\",\"31/60\",\"61/90\",\"91/120\",\"'+120\"\n"
        nombre_arch ="Saldos_proveedores"
        lista = []
        state = ""
        total = 0.00
        total_01_30 = 0.00
        total_31_60 = 0.00
        total_61_90 = 0.00
        total_91_120 = 0.00
        total_mas_120 = 0.00
        for this in self.browse(cr, uid, ids):
            for data in this.saldo_cliente_ids:
              columna += "\""+unicode(data.cliente).encode("latin-1")+"\","+"\""+unicode(data.dias_atrasados).encode("latin-1")+"\","\
              +"\""+unicode(data.fecha_vencimiento).encode("latin-1")+"\","+"\""+unicode(data.tipo).encode("latin-1")+"\","\
              +"\""+unicode(data.folio).encode("latin-1")+"\","+"\""+unicode(data.referencias).encode("latin-1")+"\"," +"\""+unicode(data.saldo).encode("latin-1")+"\","\
              +"\""+unicode(data.days_due_01to30).encode("latin-1")+"\","+"\""+unicode(data.days_due_31to60).encode("latin-1")+"\","\
              +"\""+unicode(data.days_due_61to90).encode("latin-1")+"\","+"\""+unicode(data.days_due_91to120).encode("latin-1")+"\","+"\""+unicode(data.days_due_121togr).encode("latin-1")+"\","+"\n"
              total+=data.saldo
              total_01_30+=data.days_due_01to30
              total_31_60+=data.days_due_31to60
              total_61_90+=data.days_due_61to90
              total_91_120+=data.days_due_91to120
              total_mas_120+=data.days_due_121togr
            
            columna += "\""+unicode("TOTALES").encode("latin-1")+"\","+"\""+unicode().encode("latin-1")+"\","+"\""+unicode().encode("latin-1")+"\","+"\""\
           +unicode().encode("latin-1")+"\","+"\""+unicode().encode("latin-1")+"\","+"\""+unicode().encode("latin-1")+"\","+"\""+unicode(total).encode("latin-1")+"\","+"\""\
           +unicode(total_01_30).encode("latin-1")+"\","+"\""+unicode(total_31_60).encode("latin-1")+"\","+"\""\
           +unicode(total_61_90).encode("latin-1")+"\","+"\""+unicode(total_91_120).encode("latin-1")+"\","+"\""\
           +unicode(total_mas_120).encode("latin-1")+"\","+"\n"""
        #--------- termina contenido archivo
        buf = cStringIO.StringIO()
        buf.write(columna)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        filename_psn = nombre_arch+".csv"
        write_vals = {
        'name': filename_psn,
        'data': out,
        'gv_saldos_clientes_id':ids[0]
        }
        id=self.pool.get('wizard.export.saldos').create(cr, uid, write_vals, context= context)
        
        # --------end------------------------
        return {
        'type': 'ir.actions.act_window',
        'res_model': 'wizard.export.saldos',
        'view_mode': 'form',
        'view_type': 'form',
        'res_id': id,
        'views': [(False, 'form')],
        'target': 'new',
    }
    def docopen(self, cr, uid, ids, context=None):
        """
        @Abre el documento factura o nota
        """

        if not context:
            context = {}
        active_id = context.get('active_id')
        models = self.pool.get('ir.model.data')
        #Get this line's invoice id
        folio = self.browse(cr, uid, ids[0]).folio
        inv_id = self.pool.get('account.invoice').search(cr, uid, [('number','=',folio)])[0]
        view = models.get_object_reference(cr, uid, 'account', 'invoice_form')
        view_id = view and view[1] or False
        name = 'Customer Invoices'
        res_model = 'account.invoice'
        ctx = "{'type':'out_invoice'}"
        doc_id = inv_id
    
        if not doc_id:
            return {}
        
        #Open up the document's form
        return {
            'name': (name),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [view_id],
            'res_model': res_model,
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': doc_id,
        }
    
    def tipo_documento(self, cr, uid, tipo, context=None):
        """
        @Regresa el tipo de documento
        """
        if tipo == 'in_invoice' or tipo == 'out_invoice':
            return 'Factura'
        elif tipo =='in_refund' or tipo =='out_refund':
            return 'Nota de credito'
       
    
    def date_due(self, cr, uid, date_due, context=None):
        """
        @Regresa el numero de dias atrasados
        """
        a = datetime.strptime(str(date.today()), "%Y-%m-%d")
        b = datetime.strptime(str(date_due), "%Y-%m-%d")
        delta = b - a
        return abs(delta.days)
    
    def anticipos(self,cr, uid, ids, context=None):
        """@Filtra solo anticipos"""
        data = self.browse(cr, uid, ids)
        anticipos = []
        saldo_anticipo = 0.00
        anticipos_clientes = ['120','120.01','120.02','120.03','120.04']
        obj_voucher = self.pool.get('account.voucher')
        ob_account = self.pool.get('account.account')
        obj_saldo = self.pool.get('gv.saldos.cliente')
        obj_account_voucher_line = self.pool.get('account.voucher.line')
        cr.execute("""SELECT COUNT(sat.code) FROM account_voucher AS av 
                        INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                        INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                        WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and  av.date<=%s and av.state='posted'""",(data.fecha_final,))
        count = cr.fetchone()[0]
        if count>0:
            if data.filter_by=='filtrar_anticipos' and data.select_by=='todos':
                cr.execute("""DELETE FROM _gv_saldos_cliente WHERE id!=%s""",(data.id,))
                cr.execute("""SELECT av.id FROM account_voucher AS av 
                            INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                            INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                            WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and av.date<=%s and av.state='posted'""",(data.fecha_final,))
                anticipos = cr.fetchall()
            elif data.filter_by=='filtrar_anticipos' and data.select_by=='cliente':
                cr.execute("""DELETE FROM _gv_saldos_cliente WHERE id!=%s""",(data.id,))
                cr.execute("""SELECT av.id FROM account_voucher AS av 
                            INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                            INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                            WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and av.date<=%s and av.state='posted' and av.partner_id=%s""",(data.fecha_final,data.partner_id.id,))
                anticipos = cr.fetchall()
            elif data.filter_by=='filtrar_todos' and data.select_by=='todos':
                cr.execute("""SELECT av.id FROM account_voucher AS av 
                            INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                            INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                            WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and av.date<=%s and av.state='posted'""",(data.fecha_final,))
                anticipos = cr.fetchall()
            elif data.filter_by=='filtrar_todos' and data.select_by=='cliente':
                cr.execute("""SELECT av.id FROM account_voucher AS av 
                            INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                            INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                            WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and av.date<=%s and av.state='posted' and av.partner_id=%s""",(data.fecha_final,data.partner_id.id,))
                anticipos = cr.fetchall()
            for voucher in anticipos:
                 saldo_anticipo = 0.00
                 voucher = obj_voucher.browse(cr, uid, voucher,context=context)
                 cr.execute("""SELECT count(av.id) from account_voucher_line AS av INNER JOIN account_move_line AS am ON av.move_line_id=am.id where am.name=%s""",(voucher.number,))
                 count_move_lines = cr.fetchone()[0]
                 if count_move_lines>0:
                     cr.execute("""SELECT av.id from account_voucher_line AS av INNER JOIN account_move_line AS am ON av.move_line_id=am.id where am.name=%s order by av.id desc""",(voucher.number,))
                     voucher_line_id = cr.fetchone()[0]
                    # raise osv.except_osv(_('Alerta!'), _(str(voucher_line_id)))
                     line = obj_account_voucher_line.browse(cr, uid, voucher_line_id, context=context)
                     #if line.amount_unreconciled-line.amount == 0.00:
                     if line.amount_unreconciled == 0.00:
                        saldo_anticipo = line.amount_original - line.amount
                     else:
                        #raise osv.except_osv(_('Alerta!'), _(str(line.amount)))
                        saldo_anticipo = line.amount_unreconciled-line.amount
                 if count_move_lines == 0:
                        saldo_anticipo = voucher.amount
                 #raise osv.except_osv(_('Alerta!'), _(str(line.amount_original)+" : "+str(count_move_lines)))
                
                 if saldo_anticipo > 0.00:
                    obj_saldo.create(cr, uid, {'cliente':voucher.partner_id.name,
                                          'fecha_vencimiento':'',
                                          'folio': voucher.number,
                                          'tipo':'Anticipo',
                                          'referencias':voucher.reference or '',
                                          'dias_atrasados':'',
                                          'saldo':saldo_anticipo,
                                          'saldo_cliente_id':data.id}, context=context)
                 #raise osv.except_osv(_('Alerta!'), _(str(voucher.id)))
                 
                 
    def filtrar(self,cr, uid, ids, context=None):
        """
        @Filtra aquellos docuemnto que cumplan con los filtros de fechas
        """
        invoce_list = []
        data = self.browse(cr, uid, ids)
        if  not data.fecha_final:raise osv.except_osv(_('Alerta!'), _(str('Indique una fecha')))
        cr.execute("""DELETE FROM _gv_saldos_cliente WHERE id!=%s""",(data.id,))
        obj_invoice = self.pool.get('account.invoice')
        obj_saldo = self.pool.get('gv.saldos.proveedor')
        obj_voucher = self.pool.get('account.voucher')
        obj_picking = self.pool.get('stock.picking')
        obj_purchase = self.pool.get('purchase.order')
        dias_atrasados = 0.00
        saldo = 0.00
        if data.filter_by=='filtrar_facturas' and data.select_by=='todos':
            cr.execute("""SELECT id from account_invoice WHERE state='open' and date_invoice<=%s and type=%s""",(data.fecha_final,'in_invoice',))
            invoce_list = cr.fetchall()
        elif data.filter_by=='filtrar_notas' and data.select_by=='todos':
            cr.execute("""SELECT id from account_invoice WHERE state='open' and date_invoice<=%s and type=%s""",(data.fecha_final,'in_refund',))
            invoce_list = cr.fetchall()
        elif data.filter_by=='filtrar_todos' and data.select_by=='todos':
            cr.execute("""SELECT id from account_invoice WHERE state='open' and date_invoice<=%s and (type='in_invoice' or type='in_refund')""",(data.fecha_final,))
            invoce_list = cr.fetchall()
        if data.filter_by=='filtrar_facturas' and data.select_by=='cliente':
            cr.execute("""SELECT id from account_invoice WHERE state='open' and date_invoice<=%s and type=%s and partner_id=%s""",(data.fecha_final,'in_invoice',data.partner_id.id,))
            invoce_list = cr.fetchall()
        elif data.filter_by=='filtrar_notas' and data.select_by=='cliente':
            cr.execute("""SELECT id from account_invoice WHERE state='open' and date_invoice<=%s and type=%s and partner_id=%s""",(data.fecha_final,'in_refund',data.partner_id.id,))
            invoce_list = cr.fetchall()
        elif data.filter_by=='filtrar_todos' and data.select_by=='cliente':
            cr.execute("""SELECT id from account_invoice WHERE state='open' and date_invoice<=%s and partner_id=%s and (type='in_invoice' or type='in_refund')""",(data.fecha_final,data.partner_id.id,))
            invoce_list = cr.fetchall()
        elif data.filter_by=='filtrar_anticipos':
            self.anticipos(cr, uid, ids, context=context)
        #if len(invoce_list) == 0:
            #raise osv.except_osv(_('Alerta!'), _(str('Sin documentos para este periodo')))
            
        for this in invoce_list :
            referencias = ""
            invoice = obj_invoice.browse(cr, uid, this,context=context)
            tipo_documento = self.tipo_documento(cr, uid, invoice.type, context)
            date_due = self.date_due(cr, uid, invoice.date_due, context)
            if invoice.origin:
                if invoice.type =='in_refund':
                    saldo = invoice.residual
                    cr.execute("""SELECT COUNT(*) FROM account_invoice WHERE number=%s""",(invoice.origin,))
                    if cr.fetchone()[0] >0: referencias=invoice.origin
                    if invoice.picking_dev_id:
                        id_pick_dev = obj_picking.search(cr, uid, [('name','=',invoice.picking_dev_id.origin)], context=context)[0]
                        pick_dev = obj_picking.browse(cr, uid, id_pick_dev, context=context)
                        cr.execute("""SELECT COUNT(*) FROM purchase_order WHERE name=%s""",(pick_dev.origin,))
                        if cr.fetchone()[0] >0:
                            id_invoice_dev = obj_invoice.search(cr, uid, [('origin','=',pick_dev.origin)], context=context)[0]
                            invoice_dev = obj_invoice.browse(cr, uid,id_invoice_dev,context=context)
                            referencias=invoice_dev.number
                       # raise osv.except_osv(_('Alerta!'), _(str(pick_dev.name)+"_:"+str(pick_dev.origin)))
                if invoice.type=='in_invoice':
                    saldo = -(invoice.residual)
                    if invoice.payment_ids:
                        for this in invoice.payment_ids:
                            id_voucher = obj_voucher.search(cr, uid, [('number','=',this.move_id.name)])[0]
                            voucher = obj_voucher.browse(cr, uid, id_voucher,context=context) #move_line_id
                            if voucher.amount == 0.00:
                                for v in voucher.line_cr_ids:
                                    if v.move_line_id.invoice:
                                        referencias+=v.move_line_id.invoice.number+","
                                    #raise osv.except_osv(_('Alerta!'), _(str(invoice.number)+" : "+str(referencias)))
            if not invoice.origin and invoice.type=='in_invoice': saldo= -invoice.residual
            if not invoice.origin and invoice.type=='in_refund': saldo= invoice.residual
            new_id = obj_saldo.create(cr, uid, {'cliente':invoice.partner_id.name,
                                       'fecha_vencimiento':invoice.date_due,
                                       'folio': invoice.number,
                                       'tipo':tipo_documento,
                                       'referencias':referencias,
                                       'dias_atrasados':date_due,
                                       'saldo':saldo,
                                       'saldo_cliente_id':data.id}, context=context)
            if date_due>=1 and date_due<=30:
                obj_saldo.write(cr, uid, new_id, { 'days_due_01to30':saldo,}, context=context)
            elif date_due>=31 and date_due<=60:
                obj_saldo.write(cr, uid, new_id, { 'days_due_31to60':saldo,}, context=context)
            elif date_due>=61 and date_due<=90:
                obj_saldo.write(cr, uid, new_id, { 'days_due_61to90':saldo,}, context=context)
            elif date_due>=91 and date_due<=120:
                obj_saldo.write(cr, uid, new_id, { 'days_due_91to120':saldo,}, context=context)
            elif date_due>=121:
                obj_saldo.write(cr, uid, new_id, { 'days_due_121togr':saldo,}, context=context)
                               
        if data.filter_by=='filtrar_todos' and data.select_by=='todos' or data.filter_by=='filtrar_todos' and data.select_by=='cliente':
           self.anticipos(cr, uid, ids, context=context)
    _name='gv.saldos.proveedor'
    _table = '_gv_saldos_cliente'
    _order = 'dias_atrasados desc'
    #rec_name='fecha_inicial'
    _columns={
        'fecha_final': fields.datetime('Fecha:'),
        'cliente': fields.char('Cliente',size=254),
        'fecha_vencimiento': fields.char('Fecha Vencimiento',size=254),
        'folio': fields.char('Folio', size=254),
        'tipo': fields.char('Tipo documento', size=254),
        'referencias': fields.char('Referencia(s)', size=254),
        'saldo': fields.float('Saldo'),
        'dias_atrasados': fields.char('Dias atrasados', size=254),
        
        
        'filter_by': fields.selection([
          ('filtrar_facturas', 'Facturas'),
          ('filtrar_notas', 'Notas'),
          ('filtrar_anticipos', 'Anticipos'),
          ('filtrar_todos', 'Todos'),
          ], 'Filtrar por:', required=False,),
        'select_by': fields.selection([
            ('cliente', 'Proveedor'),
            ('todos', 'Todos'),
            ], 'Proveedores:', required=False,),
        
        'days_due_01to30': fields.float(u'01/30', readonly=True),
        'days_due_31to60': fields.float(u'31/60', readonly=True),
        'days_due_61to90': fields.float(u'61/90', readonly=True),
        'days_due_91to120': fields.float(u'91/120', readonly=True),
        'days_due_121togr': fields.float(u'+121', readonly=True),
        
        'partner_id': fields.many2one('res.partner','Proveedor',),
        'saldo_cliente_ids':fields.one2many('gv.saldos.cliente','saldo_cliente_id','Saldos Vencidos'),
        'saldo_cliente_id':fields.many2one('gv.saldos.cliente','Saldos'),
        }
    
    @api.onchange('filtrar_facturas')
    def _filtrar_facturas(self):
          self.filtrar_notas=False
          self.filtrar_todos=False
    
    @api.onchange('filtrar_notas')
    def _filtrar_notas(self):
          self.filtrar_facturas=False
          self.filtrar_todos=False
    
    @api.onchange('filtrar_todos')
    def _filtrar_todos(self):
          self.filtrar_facturas=False
          self.filtrar_notas=False
    
    @api.onchange('select_by')
    def _select_by(self):
          self.partner_id=False
          
saldo_proveedor()
