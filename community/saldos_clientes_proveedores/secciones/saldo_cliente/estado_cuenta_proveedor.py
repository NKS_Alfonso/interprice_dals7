
# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                                #
#  @creacion    :  (aaaa/mm/dd)                                                                                                                      #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
import base64
import cStringIO
from openerp import models, api,_
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _
import logging
import time
from openerp.tools.safe_eval import safe_eval as eval
import pytz
from openerp import SUPERUSER_ID
_logger = logging.getLogger(__name__)
class saldo_cuentas_proveedor(osv.osv):

    def onchange_datos(self, cr, uid, ids, partner_id):
        data = self.pool.get('res.partner').browse(cr, uid, partner_id)
        """ Devuelve el status del cliente"""
        status=''
        if data.active == True:
           status = 'Activo'

        """ Devuelve la tasa de la moneda MXN """
        paridad=0.0
        cr.execute (""" SELECT id FROM res_currency where name='MXN' """)
        moneda_id = cr.fetchone()[0]
        obj = self.pool.get('res.currency').browse(cr, uid,moneda_id)

        """ Devuelve el saldo de la cuenta"""
        # cr.execute (""" select id from account_account where code = '110-030-000-0000-000' and name='CLIENTES' """)
        # id_cuenta = cr.fetchone()[0]
        # cuenta = self.pool.get('account.account').browse(cr,uid,id_cuenta)

        return{'value':
        {'status':status,
         'paridad':obj.rate_silent,
         'saldo_cuenta':data.credit,
        }
        }

    def docopen(self, cr, uid, ids, context=None):
        """Abre el documento"""
        folio = self.browse(cr, uid, ids[0]).documento.replace('->','')
        cr.execute("""
                   SELECT COUNT(*) FROM account_invoice
                   WHERE type in('out_invoice','out_refund')
                   AND number=%s
                   """,
                   (folio,))
        count = cr.fetchone()[0]
        if count>0:return self.open_invoice(cr, uid, ids, context)
        else:return self.open_anticipo(cr, uid, ids, context)

    def open_anticipo(self, cr, uid, ids, context=None):
        """
        @Abre el documento factura o nota
        """

        if not context:
            context = {}
        active_id = context.get('active_id')
        models = self.pool.get('ir.model.data')
        #Get this line's invoice id
        folio = self.browse(cr, uid, ids[0]).documento.replace('->','')
        #pasar ('state','=','open') al servidor
        inv_id = self.pool.get('account.voucher').search(cr, uid, [('number','=',folio),('state','=','posted')])[0]
        view = models.get_object_reference(cr, uid, 'account_voucher', 'view_vendor_payment_form')
        view_id = view and view[1] or False
        name = 'Voucher supplier'
        res_model = 'account.voucher'
        #ctx = "{'type':'in_invoice'}"
        doc_id = inv_id

        if not doc_id:
            return {}

        #Open up the document's form
        return {
            'name': (name),
            'view_type': 'form',
            'view_mode': 'form',
           'view_id': [view_id],
            'res_model': res_model,
            'context': context,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': doc_id,
        }

    def open_invoice(self, cr, uid, ids, context=None):
        """
        @Abre el documento factura o nota
        """

        if not context:
            context = {}
        active_id = context.get('active_id')
        models = self.pool.get('ir.model.data')
        #Get this line's invoice id
        folio = self.browse(cr, uid, ids[0]).documento.replace('->','').replace('->->','')
        #foli = folio.replace('->','')
        #pasar ('state','=','open') al servidor
        inv_id = self.pool.get('account.invoice').search(cr, uid, [('number','=',folio),('state','in',('open','paid'))])[0]
        view = models.get_object_reference(cr, uid, 'account', 'invoice_form')
        view_id = view and view[1] or False
        name = 'Supplier Invoices'
        res_model = 'account.invoice'
        ctx = "{'type':'in_invoice'}"
        doc_id = inv_id

        if not doc_id:
            return {}

        #Open up the document's form
        return {
            'name': (name),
            'view_type': 'form',
            'view_mode': 'form',
           'view_id': [view_id],
            'res_model': res_model,
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': doc_id,
        }

    def obtiene_iva(self, cr, uid,invoice, context=None):
        """obtiene porcentaje de iva"""
        p_iva = 0.00
        if invoice.tax_line:
            cr.execute("""
                       SELECT tax_percent FROM account_invoice_tax
                       WHERE invoice_id=%s and name2='IVA'
                       """,(invoice.id,))
            iva = cr.fetchone()[0]
            p_iva = (invoice.residual*iva)/100
        return p_iva

    def traduce_estados(self, cr, uid, state, context=None):
        estado = ""
        if state=='draft': estado ='Borrador'
        elif state=='proforma' or state=='proforma2': estado ='Pro-forma'
        elif state=='open': estado ='Abierto'
        elif state=='paid': estado ='Pagado'
        elif state=='posted': estado ='Contabilizado'
        else: estado ='Cancelado'
        return estado

    def tipo_documento(self, cr, uid, tipo, context=None):
        """
        @Regresa el tipo de documento
        """
        if tipo == 'in_invoice' or tipo == 'out_invoice':
            return 'Factura'
        elif tipo =='in_refund' or tipo =='out_refund':
            return 'Nota de credito'


    def date_due(self, cr, uid, date_due, context=None):
        """
        @Regresa el numero de dias atrasados
        """
        a = datetime.strptime(str(date.today()), "%Y-%m-%d")
        b = datetime.strptime(str(date_due), "%Y-%m-%d")
        delta = b - a
        return abs(delta.days)


    def delete_ids(self, cr, uid, ids, context=None):
        """"""
        data = self.browse(cr, uid, ids)
        cr.execute("""DELETE FROM _gv_estado_cuentas WHERE id!=%s""",(data.id,))

    def tipo_cambio_fecha_documento(self, cr, uid, invoice, residual, context=None):
        """Retorna el tipo de cambio que habia a la fecha del documento"""
        obj_currency = self.pool['res.currency']
        rate_id = obj_currency.search(cr, uid, [('name','=','MXN')])[0]
        if invoice._columns.has_key('date_invoice'):
            date = invoice.date_invoice
        elif invoice._columns.has_key('date'):
            date =invoice.date
        saldo_mxn = 0.00
        saldo_usd = 0.00
        cr.execute("""SELECT rate from res_currency_rate WHERE currency_id=%s AND to_char(name,'YYYY-MM-DD')<=%s order by id desc""",(rate_id,date,))
        tipo_cambio = cr.fetchone()[0]
        if invoice._columns.has_key('date_invoice'):
            if invoice.currency_id.name=='MXN':
                 saldo_mxn = invoice.residual
                 saldo_usd = invoice.residual*tipo_cambio
            elif invoice.currency_id.name=='USD':
                  saldo_usd = invoice.residual
                  saldo_mxn =  invoice.residual/tipo_cambio
        elif invoice._columns.has_key('date'):
            if invoice.journal_id.currency:
                if invoice.journal_id.currency.name=='MXN':
                    saldo_mxn = residual
                    saldo_usd = residual*tipo_cambio
                elif invoice.journal_id.currency.name=='USD':
                    saldo_usd = residual
                    saldo_mxn =  residual/tipo_cambio
            else:
                 saldo_mxn = residual
                 saldo_usd = residual*tipo_cambio

        return tipo_cambio,saldo_mxn, saldo_usd

    def facturas_notas_por_emision(self, cr, uid, ids, context=None):
        """FACTURAS Y NOTAS POR EMISION"""
        self.delete_ids(cr, uid, ids, context)
        obj_invoice = self.pool['account.invoice']
        obj_cuentas = self.pool['gv.cuenta.proveedor']
        obj_currency = self.pool['res.currency']
        data = self.browse(cr, uid, ids)
        saldo_acumulado = 0.00
        saldo_mxn = 0.00
        saldo_usd = 0.00
        paridad = 0.00
        cr.execute("""SELECT id FROM account_invoice
                    WHERE type in('out_invoice','out_refund')
                    AND (state='open' AND residual>0.0)
                    AND partner_id=%s AND (date_invoice>=%s AND date_invoice<=%s)""",(data.partner_id.id,data.date_from, data.date_to,))
        invoices = cr.fetchall()
        #raise osv.except_osv(_('Alerta!'), _(str(invoices)))
        if invoices:
            rate_id = obj_currency.search(cr, uid, [('name','=','MXN')])[0]
            for this in invoices:
                invoice = obj_invoice.browse(cr, uid, this,context)
                if invoice.currency_rate_alter>0.00:
                    paridad = invoice.currency_rate_alter
                    if invoice.currency_id.name=='MXN':
                        saldo_mxn = invoice.residual
                        saldo_usd = invoice.residual*invoice.currency_rate_alter
                    else:
                        saldo_usd = invoice.residual
                        saldo_mxn =  invoice.residual/invoice.currency_rate_alter
                else: paridad,saldo_mxn,saldo_usd =self.tipo_cambio_fecha_documento(cr, uid,invoice, invoice.residual, context=context)
                saldo_acumulado+=saldo_mxn
                vals = { 'limite_credito':invoice.partner_id.credit_limit,
                    'dias_credito': invoice.partner_id.property_payment_term.line_ids.days,
                    'paridad':paridad,
                    'documento':invoice.number,
                    'referencia_documento': invoice.origin,
                    'estado': self.traduce_estados(cr, uid, invoice.state, context) ,
                    'emision':invoice.date_invoice ,
                    'vencimiento': invoice.date_due,
                    'importe':invoice.amount_untaxed+invoice.amount_tax,
                    'moneda':invoice.currency_id.id,
                    'saldo_mxn':saldo_mxn,
                    'saldo_usd':saldo_usd,
                    'iva': self.obtiene_iva(cr, uid, invoice, context),
                    'cuenta_contrapartida':0,
                    'tipo_movimiento':'NA',
                    'saldo_acumulado':saldo_acumulado,
                    'estado_cliente_id':data.id,}
                obj_cuentas.create(cr, uid,vals,context)
                #saldo_mxn = 0.00
                #saldo_usd = 0.00
                paridad = 0.00

            self.obtiene_anticpos(cr, uid, data, saldo_acumulado,context)
        #else:raise osv.except_osv(_('Alerta!'), _("No hay coincidencias"))

    def facturas_notas_por_vencimiento(self, cr, uid, ids, context=None):
        """FACTURAS Y NOTAS POR VENCIMIENTO"""
        self.delete_ids(cr, uid, ids, context)
        obj_invoice = self.pool['account.invoice']
        obj_cuentas = self.pool['gv.cuenta.proveedor']
        obj_currency = self.pool['res.currency']
        data = self.browse(cr, uid, ids)
        saldo_mxn = 0.00
        saldo_usd = 0.00
        saldo_acumulado = 0.00
        cr.execute("""
                    SELECT id FROM account_invoice
                    WHERE type in('out_invoice','out_refund')
                    AND (state='open' AND residual>0.0)
                    AND partner_id=%s AND (date_due>=%s AND date_due<=%s)
                    """,(data.partner_id.id,data.date_from, data.date_to,))
        invoices = cr.fetchall()
        if invoices:
            rate_id = obj_currency.search(cr, uid, [('name','=','MXN')])[0]
            for this in invoices:
                invoice = obj_invoice.browse(cr, uid, this,context)
                if invoice.currency_rate_alter>0.00:
                    paridad = invoice.currency_rate_alter
                    if invoice.currency_id.name=='MXN':
                        saldo_mxn = invoice.residual
                        saldo_usd = invoice.residual*invoice.currency_rate_alter
                    else:
                        saldo_usd = invoice.residual
                        saldo_mxn =  invoice.residual/invoice.currency_rate_alter
                else: paridad,saldo_mxn,saldo_usd =self.tipo_cambio_fecha_documento(cr, uid,invoice, invoice.residual, context=context)
                saldo_acumulado+=saldo_mxn
                vals = { 'limite_credito':invoice.partner_id.credit_limit,
                        'dias_credito': invoice.partner_id.property_payment_term.line_ids.days,
                        'paridad':paridad,
                        'documento':invoice.number,
                        'referencia_documento': invoice.origin,
                        'estado': self.traduce_estados(cr, uid, invoice.state, context) ,
                        'emision':invoice.date_invoice ,
                        'vencimiento': invoice.date_due,
                        'importe':invoice.amount_untaxed+invoice.amount_tax,
                        'moneda':invoice.currency_id.id,
                        'saldo_mxn':saldo_mxn,
                        'saldo_usd':saldo_usd,
                        'iva': self.obtiene_iva(cr, uid, invoice, context),
                        'cuenta_contrapartida':0,
                        'tipo_movimiento':'N/A',
                        'saldo_acumulado':saldo_acumulado,
                        'estado_cliente_id':data.id,}
                obj_cuentas.create(cr, uid,vals,context)
               # saldo_mxn = 0.00
               # saldo_usd = 0.00
                paridad = 0.00

            self.obtiene_anticpos(cr, uid, data, saldo_acumulado,context)
        #else:raise osv.except_osv(_('Alerta!'), _("No hay coincidencias"))

    def obtiene_anticpos(self, cr, uid , data,saldo_acumulado, context=None):
        #data = self.browse(cr, uid, ids)
        obj_currency = self.pool['res.currency']
        obj_voucher = self.pool['account.voucher']
        obj_cuentas = self.pool['gv.cuenta.proveedor']
        saldo_mxn = 0.00
        saldo_usd = 0.00
        s_acumulado=saldo_acumulado
        cr.execute("""
                    SELECT av.id FROM account_voucher AS av
                    INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                    INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                    WHERE sat.code in ('206','206.01','206.02','206.04','206.04','206.05')
                    and av.date>=%s and av.date<=%s and av.state='posted' and av.partner_id=%s
                   """,
                   (data.date_from,data.date_to,data.partner_id.id,))
        anticipos = cr.fetchall()
        if anticipos:
            rate_id = obj_currency.search(cr, uid, [('name','=','MXN')])[0]
            for this in anticipos:
                anticipo = obj_voucher.browse(cr, uid, this,context)
                residual = self.get_ammount_voucher(cr, uid, anticipo, context)
                paridad,saldo_mxn,saldo_usd =self.tipo_cambio_fecha_documento(cr,uid,anticipo,residual, context=context)
                s_acumulado+=saldo_mxn
                vals = { 'limite_credito':anticipo.partner_id.credit_limit,
                        'dias_credito': anticipo.partner_id.property_payment_term.line_ids.days,
                        'paridad':paridad,
                        'documento':anticipo.number,
                        'referencia_documento': anticipo.reference,
                        'estado': self.traduce_estados(cr, uid, anticipo.state, context) ,
                        'emision':anticipo.date ,
                        'vencimiento': anticipo.date,
                        'importe':anticipo.net_amount,
                        'moneda':anticipo.journal_id.currency.id or 34,
                        'saldo_mxn':saldo_mxn,
                        'saldo_usd':saldo_usd,
                        'iva': 0.00,
                        'cuenta_contrapartida':0,
                        'tipo_movimiento':'N/A',
                        'saldo_acumulado':s_acumulado,
                        'estado_cliente_id':data.id,}
                obj_cuentas.create(cr, uid,vals,context)
                #saldo_mxn = 0.00
                #saldo_usd = 0.00
                paridad = 0.00

        #else:raise osv.except_osv(_('Alerta!'), _("No hay coincidencias"))

    def get_ammount_voucher(self, cr, uid, voucher, context=None):
        """Retorn el saldo que aun queda pendiente del anticipo"""
        amount_residual = 0.00
        cr.execute("""
                   SELECT COUNT(*) from account_voucher_line AS av INNER JOIN
                   account_move_line AS am ON av.move_line_id=am.id where am.name=%s"""
                   ,(voucher.number,))
        count = cr.fetchone()[0]
        if count == 0:
            amount_residual = voucher.amount
        elif count == 1 :
            cr.execute("""
                       SELECT amount_original-amount from account_voucher_line AS av INNER JOIN
                       account_move_line AS am ON av.move_line_id=am.id where am.name=%s order by av.id desc"""
                   ,(voucher.number,))
            amount_residual = cr.fetchone()[0]
        elif count > 1 :
            cr.execute("""
                       SELECT sum(amount), amount_original from account_voucher_line AS av INNER JOIN
                       account_move_line AS am ON av.move_line_id=am.id where am.name=%s group by amount_original"""
                   ,(voucher.number,))
            resultado = cr.fetchall()
            for (amount, original) in resultado:
                if amount == 0.00:
                    amount_residual = original
                elif amount >0:
                    amount_residual = original-amount
        return amount_residual
     #####################################################################################################
    #pagos
    def total_pago(self, cr, uid, voucher, context=None):
        cr.execute("""SELECT sum(amount) FROM account_voucher_line WHERE voucher_id=%s
                      AND type='cr'""",(voucher.id,))
        return cr.fetchone()[0]

    def get_object(self, cr, uid, document, context=None):
        """retona un objeto de factura o invoice"""
        objeto = False
        residual = 0.0
        estado = ''
        obj_voucher = self.pool['account.voucher']
        obj_invoice = self.pool['account.invoice']
        obj_move_line = self.pool['account.move.line']
        obj_move = self.pool['account.move']
        p_iva = 0.00
        cr.execute("""
                   SELECT COUNT(*) FROM account_voucher WHERE number=%s
                   """,
                   (document,))
        count = cr.fetchone()[0]
        if count>0:
            voucher_id = obj_voucher.search(cr, uid,[('number','=',document),('state','=','posted')],context)[0]
            objeto=obj_voucher.browse(cr, uid,voucher_id,context)
            residual = self.get_ammount_voucher(cr, uid, objeto, context)
            estado = self.traduce_estados(cr, uid, objeto.state, context)
            return objeto, residual, p_iva, estado
        else:
            if 'type_of_line' in context:
                move_line_id = False
                invoice_id = False
                if context.get('type_of_line') == 'cr':
                    if context.get('ref'):
                        move_line = obj_move_line.search(cr, uid, [('ref', '=', document)],None,limit=1)
                        move_line_id = obj_move_line.browse(cr, uid, move_line, context)
                    else:
                        move_line = obj_move_line.search(cr, uid, [('name', '=', document)],None,limit=1)
                        move_line_id = obj_move_line.browse(cr, uid, move_line, context)
                if context.get('type_of_line') == 'dr':
                    invoice = obj_invoice.search(cr, uid, [('number', '=', document)],None,limit=1)
                    invoice_id = obj_invoice.browse(cr, uid, invoice, context)
                if move_line_id or invoice_id:
                    if move_line_id:
                        if move_line_id.invoice:
                            objeto = move_line_id.invoice
                        else:
                            objeto = None
                    if invoice_id:
                        objeto = invoice_id
                    objeto = move_line_id.invoice if move_line_id else invoice_id
                    p_iva = 0.00
                    if objeto.tax_line:
                        cr.execute("""
                                   SELECT tax_percent FROM account_invoice_tax
                                   WHERE invoice_id=%s and name2='IVA'
                                   """,(objeto.id,))
                        iva = cr.fetchone()[0]
                        p_iva = (objeto.residual*iva)/100
                        estado = self.traduce_estados(cr, uid, objeto.state, context)
                    return  objeto, objeto.residual, p_iva, estado
        return objeto, residual, p_iva, estado


    def line_dr_ids(self, cr, uid, anticipo, data,context=None):
        """Notas de credito, anticipos"""
        obj_voucher = self.pool['account.voucher']
        obj_invoice = self.pool['account.invoice']
        obj_cuentas = self.pool['gv.cuenta.proveedor']
        date = ""
        date_due = ""
        for this in anticipo.line_dr_ids:
            if this.amount>0:
                ctx = {}
                saldo = this.amount_unreconciled-this.amount
                if this.move_line_id.name=='/':
                    documento = this.move_line_id.ref
                    referencia_documento = ''
                    ctx.update({'ref': True})
                else:
                    documento = this.move_line_id.name
                    referencia_documento = this.move_line_id.invoice.origin or ''
                ctx.update({'type_of_line': 'dr'})
                objeto, residual, iva , estado= self.get_object(cr, uid, documento, ctx)
                if objeto and estado != 'Cancelado':
                    paridad,saldo_mxn,saldo_usd =self.tipo_cambio_fecha_documento(cr,uid,objeto,residual, context=context)
                else:
                    continue
                if objeto._columns.has_key('date_invoice'):
                 date = objeto.date_invoice
                 date_due = objeto.date_due
                elif objeto._columns.has_key('date'):
                   if saldo ==0.00 or saldo>0.00:
                        date = anticipo.date
                        date_due = anticipo.date
                vals = {
                        'limite_credito':anticipo.partner_id.credit_limit,
                        'dias_credito': anticipo.partner_id.property_payment_term.line_ids.days,
                        'paridad':paridad,#FALTA
                        'documento':"->"+documento,
                        'referencia_documento': referencia_documento,
                        'estado': estado,
                        'emision':time.strftime('%Y-%m-%d'),
                        'vencimiento': time.strftime('%Y-%m-%d'),
                        'importe':this.amount_original,
                        'moneda':objeto.currency_id.id,
                        'saldo_mxn':saldo_mxn,
                        'saldo_usd':saldo_usd,
                        'iva': iva,
                        'estado_cliente_id':data.id,}
                obj_cuentas.create(cr, uid,vals,context)
                date = ""
                date_due = ""

    def line_cr_ids(self, cr, uid, anticipo,data, context=None):
        """Facturas"""
        obj_voucher = self.pool['account.voucher']
        obj_cuentas = self.pool['gv.cuenta.proveedor']
        for this in anticipo.line_cr_ids:
            if this.amount>0:
                ctx = {}
                saldo = this.amount_unreconciled-this.amount
                documento = this.move_line_id.invoice.number
                referencia_documento = this.move_line_id.invoice.origin or ''
                ctx.update({'type_of_line': 'cr'})
                objeto, residual, iva,estado= self.get_object(cr, uid, documento, ctx)
                if objeto and estado != 'Cancelado':
                    paridad,saldo_mxn,saldo_usd =self.tipo_cambio_fecha_documento(cr,uid,objeto,residual, context=context)
                else:
                    continue
                vals = { 'limite_credito':0.00,
                        'dias_credito': 0.00,
                        'paridad':paridad,
                        'documento':"->->"+documento,
                        'referencia_documento': referencia_documento,
                        'estado': estado ,
                        'emision':time.strftime('%Y-%m-%d') ,
                        'vencimiento': time.strftime('%Y-%m-%d'),
                        'importe':this.amount_original,
                        'moneda':this.move_line_id.invoice.currency_id.id,
                        'saldo_mxn':saldo_mxn,
                        'saldo_usd':saldo_usd,
                        'iva': iva,
                        'estado_cliente_id':data.id,}
                obj_cuentas.create(cr, uid,vals,context)


    def payment_by_document(self, cr, uid, ids, context=None):
        """Pagos por documento"""
        obj_voucher = self.pool['account.voucher']
        obj_cuentas = self.pool['gv.cuenta.proveedor']
        self.delete_ids(cr, uid, ids, context)
        data =self.browse(cr,uid, ids)
        cr.execute("""SELECT id FROM account_voucher
                      WHERE partner_id=%s AND
                      date>=%s AND date<=%s
                      AND state='posted'
                    """,
                    (data.partner_id.id,data.date_from, data.date_to,))
        pagos = cr.fetchall()
        if pagos:
            for this in pagos:
                anticipo = obj_voucher.browse(cr, uid, this,context)
                total_anticipo = self.total_pago(cr, uid, anticipo,context)
                if total_anticipo>0:
                    residual = self.get_ammount_voucher(cr, uid, anticipo, context)
                    paridad,saldo_mxn,saldo_usd =self.tipo_cambio_fecha_documento(cr,uid,anticipo,total_anticipo, context=context)
                    vals = { 'limite_credito':anticipo.partner_id.credit_limit,
                            'dias_credito': anticipo.partner_id.property_payment_term.line_ids.days,
                            'paridad':paridad,
                            'documento':anticipo.number,
                            'referencia_documento': anticipo.reference,
                            'estado': self.traduce_estados(cr, uid, anticipo.state, context) ,
                            'emision':anticipo.date ,
                            'vencimiento': anticipo.date,
                            'importe':self.total_pago(cr, uid, anticipo,context),
                            'moneda':anticipo.journal_id.currency.id or 34,
                            'saldo_mxn':saldo_mxn,
                            'saldo_usd':saldo_usd,
                            'estado_cliente_id':data.id,}
                    obj_cuentas.create(cr, uid,vals,context)
                    self.line_dr_ids(cr, uid, anticipo,data, context)
                    self.line_cr_ids(cr, uid, anticipo, data,context)


    def filtrar(self, cr, uid, ids, context):
        data  =self.browse(cr, uid, ids)
        if data.select_by=='a' and data.select_date=='d':
            self.facturas_notas_por_vencimiento(cr,uid, ids, context)
        elif data.select_by=='a' and data.select_date=='c':
            self.facturas_notas_por_emision(cr,uid, ids, context)
        elif data.select_by=='b' and (data.select_date=='c' or data.select_date=='d'):
            self.payment_by_document(cr, uid, ids, context=None)

    def action_wizard(self, cr, uid, ids, context=None):
        columna = "\"Límite de crédito\",\"Días de crédito\",\"Paridad\",\"Documento\",\"Referencia\",\"Estado\",\"Emisión\",\"Vencimiento\",\"Importe\",\"Moneda\",\"Saldo MXN\",\"Saldo USD\",\"IVA\"\n"
        nombre_arch = "Estado de cuentas clientes"
        data = self.browse(cr, uid, ids)
        for cuenta in data.estado_cliente_ids :
         columna += "\""+unicode(cuenta.limite_credito).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.dias_credito).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.paridad).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.documento).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.referencia_documento).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.estado).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.emision).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.vencimiento).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.importe).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.moneda.name).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.saldo_mxn).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.saldo_usd).encode("latin-1")+"\","\
          +"\""+unicode(cuenta.iva).encode("latin-1")+"\","+"\n"


        buf = cStringIO.StringIO()
        buf.write(columna)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        filename_psn = nombre_arch+".csv"
        write_vals = {
        'name': filename_psn,
        'data': out,
        'gv.cuenta.x.clientes_id':ids[0]
        }
        id=self.pool.get('wizard.export.cuenta').create(cr, uid, write_vals, context= context)

        # --------end------------------------
        return {
        'type': 'ir.actions.act_window',
        'res_model': 'wizard.export.cuenta',
        'view_mode': 'form',
        'view_type': 'form',
        'res_id': id,
        'views': [(False, 'form')],
        'target': 'new',
}

    _name='gv.cuenta.cliente'
    _table = '_gv_estado_cuentas'
    #_order = 'dias_atrasados desc'
    _description = 'Estado de cuentas'
    #_rec_name='fecha_inicial'
    _columns={
        'partner_id': fields.many2one('res.partner','Cliente',),
        'date_from': fields.datetime('De:'),
        'date_to': fields.datetime('A:'),
        'status' : fields.char('Status:', size=12),
        'saldo_cuenta' : fields.float('Saldo cuenta:'),
        'select_by' : fields.selection([
           ('a', 'Sólo con saldo'),
           ('b', 'Con cobros'),
           ],'Filtar por:',),
        'select_date' : fields.selection([
           ('c', 'Fecha emisión'),
           ('d', 'Fecha vencimiento'),
           ],'Filtar por:',),
        #columnas del reporte csv
        'limite_credito': fields.float('Límite de crédito'),
        'dias_credito': fields.float('Días de crédito'),
        'paridad': fields.float('Paridad:'),
        'documento': fields.char('Documento'),
        'referencia_documento': fields.char('Referencia de documento'),
        'estado': fields.char('Estado'),
        'emision': fields.char('Emisión'),
        'vencimiento': fields.char('Vencimiento'),
        'importe': fields.float('Importe'),
        'moneda': fields.many2one('res.currency','Moneda'),
        'saldo_mxn': fields.float('Saldo MXN'),
        'saldo_usd': fields.float('Saldo USD'),
        'iva': fields.float('IVA'),
        'cuenta_contrapartida': fields.many2one('account.account','Cuenta contrapartida'),
        'tipo_movimiento': fields.char('Tipo de movimiento'),
        'saldo_acumulado': fields.float('Saldo acumulado'),
        #relaciones
        'estado_cliente_ids':fields.one2many('gv.cuenta.cliente','estado_cliente_id','Saldos Vencidos'),
        'estado_cliente_id':fields.many2one('gv.cuenta.cliente','Estados'),
        }
    _defaults = {
        'saldo_cuenta':0.00,
        'paridad':0.00,
 }

saldo_cuentas_proveedor()

#termina la clase del reporte de estado de cuentas de clientes


##################################################################################
#                    wizard_export                                              #
#################################################################################
class wizard_export(osv.osv_memory):
  _name = "wizard.export.cuenta"

  _columns = {
          'name': fields.char('File Name', readonly=True),
          'data': fields.binary('File', readonly=True),
           #CAMPO PARA EL REPORTE DE ESTADO DE CUENTA CLIENTES
          'gv.cuenta.x.clientes_id' : fields.many2one('gv.cuenta.cliente'),
    }
  _defaults = {}
#TERMINA LA CLASE wizard_export

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
