# -*- coding: utf-8 -*-

from openerp import models, fields, api
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
import datetime
import pdb

class account_bank_transfer(models.Model):
	_name = 'account.account_bank_transfer'
	_rec_name = 'ref'
	#date = fields.Date(string="Date")
	account_origin_id = fields.Many2one('account.account',string="Account Origin", 
		required=True, domain="[('type','!=','view')]")
	account_dest_id = fields.Many2one('account.account',string="Account Destiny", 
		required=True, domain="[('type','!=','view')]")
	#account_origin_balance = fields.Float('Quantity', digits=(16,2))
	#account_origin_balance = fields.Float(compute="_balance_account_origin", digits_compute=dp.get_precision('Account'), string='Balance')
	account_origin_balance = fields.Float('Account Origin Balance', digits=(16,2),)
	account_dest_balance = fields.Float('Account Origin Balance', digits=(16,2),)
	description = fields.Text("Descripction")
	ref = fields.Char(string="Reference", required=True)
	amount = fields.Float(string="Amount", digits=(16,2), required=True)
	account_move_id = fields.Many2one('account.move', string="Account Move")
	account_journal_id = fields.Many2one('account.journal', string="Journal", required=True)
	#account_period_id = fields.Many2one('account.period', string="Period")
	account_currency_id = fields.Many2one('res.currency', string="Type Currency",required=False)
	state = fields.Selection([
                ('draft', 'Draft'),
                ('apply', 'Apply'),
                ], string='Satatus', default="draft")
	# _defaults = {
	# 	'account_currency_id': lambda self, cr, uid, c: self.pool.get('res.currency').search(cr, uid,[('name','=','MXN')])[0],
	# }

	def onchange_accont_origin_id(self, cr, uid, ids, field, context=None):
		balance = 0.0
		for o in self.pool.get('account.account').browse(cr,uid,field):
			balance = o.balance
		return {'value':{'account_origin_balance': balance}}

	def onchange_accont_dest_id(self, cr, uid, ids, field, context=None):
		balance = 0.0
		for o in self.pool.get('account.account').browse(cr,uid,field):
			balance = o.balance
		return {'value':{'account_dest_balance': balance}}

	def apply(self, cr, uid, ids, context=None):
		period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
		account_move = self.pool.get('account.move')
		account_move_line = self.pool.get('account.move.line')
		res_currency_rate = self.pool.get('res.currency')
		res_currency_id = 0
		account_move_id = 0
		account_move_line_id = 0

		for obj in self.browse(cr,uid,ids):
			account_move_id = 0
			res_currency_id = obj.account_currency_id.id
			for u in self.pool.get('res.users').browse(cr,uid,uid):
				company_id = u.company_id.id

			account_move_id = account_move.create(cr,uid,{'name':'/','ref':obj.ref,'journal_id':obj.account_journal_id.id, 'period_id':period_ids[0], 'date':datetime.datetime.now(), 'company_id':company_id})
			account_move_line_id = self.pool.get('account.move.line').create(cr,uid,{'move_id': account_move_id, 'journal_id':obj.account_journal_id.id, 'period_id':period_ids[0], 'date':datetime.datetime.now(), 'name': obj.ref, 'account_id':obj.account_dest_id.id,'debit':abs(obj.amount), 'credit':0, 'currency_id': obj.account_currency_id.id})
			account_move_line_id = self.pool.get('account.move.line').create(cr,uid,{'move_id': account_move_id, 'journal_id':obj.account_journal_id.id, 'period_id':period_ids[0], 'date':datetime.datetime.now(), 'name': obj.ref, 'account_id':obj.account_origin_id.id,'debit':0, 'credit':abs(obj.amount), 'currency_id': obj.account_currency_id.id})
			
		self.write(cr,uid,ids,{'state':'apply','account_move_id':account_move_id})
		return True

account_bank_transfer()

