# -*- coding: utf-8 -*-
{
    'name': "account_bank_transfer",

    'summary': """
        Transfer of movements of accounts bank, multi currency""",

    'description': """
        Transfer of movements of accounts bank, multi currency
    """,

    'author': "tantums",
    'website': "http://www.tantums.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'account',
    'version': '8.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'view/account_bank_transfer_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}