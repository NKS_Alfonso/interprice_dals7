# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 OpenPyme - http://www.openpyme.mx/
#    All Rights Reserved.
#    Coded by: Agustín Cruz (agustin.cruz@openpyme.mx)
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Mexico Account Tax Transaction',
    'version': '1.0.7',
    'category': 'Generic Modules/Account',
    'description': """
Payment Tax Account Transaction
===============================

Set "Auto Tax Transfer" if you want create tax transfer entry lines when bank
entry is created and the conditions set in operation type are fulfilled, else,
only fiscal lines are created,
- "Tax Income Details" operate in bank journal input entries
- "Tax Outcome Details" operate in bank journal output entries

In "Operation Tax" select Tax, Tax Type,
- If you want tax transfer, fill "From Account" with the source account and
"To Account" with the target account,

Open Sale/Address Book/Customer and select a customer, now go to Accounting an
select the "Operation Type" for this customer.

Open Purchase/Address Book/Suppliers and select a supplier, now go to
Accounting an select the "Operation Type" for this supplier.

When a new bank journal entry is created, and a this entry have a partner set,
and Operation Type is set in this partner, and this entry use some account from
the "Accounts Where Operation Apply" set in partner operation type, the model
creates new fiscal lines and if Auto Tax Transfer is set, it create the tax
transfer lines to.


To determine if it's an outcome or income operation, it will check in the entry
the account that being used, if the account is on the tax model and the account
is receivable, it will be consider as income operation, if its payable, it will
be consider as outcome operation.
""",
    'author': 'OpenPyme',
    'website': 'http://www.openpyme.mx',
    'depends': [
        'account'
    ],
    'data': [
        'view/res_partner_view.xml',
        'view/account_view.xml',
        'view/account_move_line_view.xml',
        'security/res.groups.csv',
    ],
    'installable': True,
    'active': False,
}
