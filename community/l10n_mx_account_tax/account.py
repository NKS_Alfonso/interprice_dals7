# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Gerardo García, Agustín Cruz
#    Copyright Fedrojesa S.A. de C.V. 2013 (<http://openpyme.mx>)
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

from openerp.osv import fields, osv
from openerp.tools.translate import _

from .constants import TAX_TYPES

_logger = logging.getLogger(__name__)


class account_move(osv.Model):
    _inherit = "account.move"

    _columns = {
        'tax_reconcile_id': fields.many2one(
            'account.move.reconcile',
            'Account Reconciliation'
        )
    }


class account_tax(osv.Model):
    """
    Extends account.tax object to include information to use on tax payment
    """
    _inherit = "account.tax"

    _columns = {
        'account_reconcile_id': fields.many2one(
            'account.account',
            'Paid Tax Account',
            help='Set the account that will be set by default when reconcile'
            'payments with invoices that include this tax.'),
        'transfer': fields.boolean(
            'Transfer tax on payment',
            help='Set if you want to transfer tax from Invoice Tax Account'
            ' to Pay Tax Account'),
        'tax_categ': fields.selection(TAX_TYPES, 'Tax Category'),
    }


class account_move_reconcile(osv.Model):
    """
    Object that stores the reconcile movements
    """
    _inherit = 'account.move.reconcile'

    _columns = {
        'account_move_tax_ids': fields.one2many(
            'account.move',
            'tax_reconcile_id', 'Tax Movement'
        )
    }

    def _check_no_mix_taxes(self, cr, uid, ids, context=None):
        """
        Check that there is no mixing taxes rates on the lines to reconcile
        this prevents failures later on payment reconcile and VAT
        translation
        """
        _logger.debug('Restriction Check Reconcile Id: %s', ids)
        for reconcile in self.browse(cr, uid, ids, context=context):
            move_lines = []
            taxes = []
            rtaxes = []
            if reconcile.opening_reconciliation:
                continue
            # Get lines to reconcile
            if reconcile.line_id:
                move_lines = reconcile.line_id
            elif reconcile.line_partial_ids:
                move_lines = reconcile.line_partial_ids
            # Get taxes on invoices & refunds to reconcile
            _logger.debug('Reconcile Line Ids: %s', move_lines)
            for line in move_lines:
                if line.journal_id.type in ['sale', 'purchase']:
                    _logger.debug(
                        'Is Invoice: %s', line.invoice and line.invoice.number
                    )

                    # Help determining when an Invoice not have taxes.
                    if not line.invoice.tax_line:
                        continue
                    for tax_line in line.invoice.tax_line:
                        if tax_line.tax_id not in taxes:
                            taxes.append(tax_line.tax_id)
                if line.journal_id.type in ['sale_refund', 'purchase_refund']:
                    _logger.debug(
                        'Is Refund: %s', line.invoice and line.invoice.number
                    )
                    # Help determining when a refund not have taxes.
                    if not line.invoice.tax_line:
                        rtaxes.append(False)
                    for tax_line in line.invoice.tax_line:
                        if tax_line.tax_id not in rtaxes:
                            rtaxes.append(tax_line.tax_id)

            _logger.debug('Taxes: %s and Rtaxes: %s', taxes, rtaxes)
            # If there is no refunds return True
            if len(rtaxes) == 0 or len(taxes) == 0:
                return True
            # Compare taxes and rtaxes
            if (taxes and not rtaxes) or (not taxes and rtaxes):
                return False
            if any(tax not in taxes for tax in rtaxes):
                return False
        return True

    _constraints = [
        (_check_no_mix_taxes,
         _('You cannot reconcile journal items with mixed taxes.'),
         ['line_id'])
    ]


class account_tax_reconcile(osv.Model):
    """
    This objects is a helper for manage tax transfer on partial payments
    """
    _name = "account.tax.reconcile"
    _description = "Account Tax Reconciliation"
    _columns = {
        'name': fields.char('Name', size=64, required=True),
        'tax_line_ids': fields.one2many(
            'account.move.line', 'tax_reconcile_id',
            'Entry Lines'
        ),
        'create_date': fields.date('Creation date', readonly=True),
    }

    def _get_tax_reconcile_name(self, cr, uid, context=None):
        return self.pool.get('ir.sequence').get(
            cr, uid, 'account.tax.reconcile', context=context
        ) or '/'

    _defaults = {
        'name': _get_tax_reconcile_name
    }
