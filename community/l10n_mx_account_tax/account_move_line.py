# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

from openerp.osv import fields, orm

from .constants import JOURNAL_INVOICES, JOURNAL_PAYMENTS

_logger = logging.getLogger(__name__)


class account_move_line(orm.Model):
    """
    Extend account move line object for allow create vat movements on payments
    """
    _inherit = "account.move.line"

    _columns = {
        'tax_reconcile_id': fields.many2one(
            'account.tax.reconcile',
            'Tax Reconciliation', readonly=True, select=2),
        'tax2_id': fields.many2one('account.tax', 'Tax'),
        'tax2_base': fields.float('Tax Base', help='Tax base amount'),
        'tax_ret': fields.float('Tax Retained', help='Amount of tax retained')
    }

    def _create_tax_move(self, cr, uid, ids, r_id, context=None):
        """
        Create an account.move for transfer taxes when needed

        :param ids: Account Move Line ids to evaluate
        :param r_id: id for Account Move Reconcile for this op
        """
        lcontext = {
            'tax_lines': [],
            'pay_lines': [],
            'op_lines': [],
        }
        partner = None
        for line in self.browse(cr, uid, ids, context):
            # Process sales & purchase lines for get taxes
            if line.partner_id and line.journal_id.type in JOURNAL_INVOICES:
                # Process sales & purchase lines of payment terms for get taxes
                for line_part in line.move_id.line_id:
                    if line_part.account_id.type in ['receivable', 'payable']:
                        lcontext['op_lines'].append(line_part)
                # Get partner for account.movement
                partner = line.partner_id
                # Iterate taxes on invoice for validate
                for tax in line.invoice.tax_line:
                    if tax.tax_id.transfer:
                        lcontext['tax_lines'].append(tax)
            # Process cash & bank lines for get payments
            if line.partner_id and line.journal_id.type in JOURNAL_PAYMENTS:
                lcontext['pay_lines'].append(line)

        # Skip the tax move creation if:
        # 1.- There are not cash or bank lines
        # 2.- Partner is market as not vat subjected
        if (
            not len(lcontext['pay_lines']) or not len(lcontext['tax_lines']) or
            not partner.vat_subjected
        ):
            return
        # Create the actual moves
        tax_ids = self._prepare_move(cr, uid, partner, context=lcontext)
        self._tax_reconcile(
            cr, uid, lcontext['pay_lines'], tax_ids, context=context
        )
        return

    def _prepare_move(self, cr, uid, partner, context=None):
        """
        Calculate the tax transfer movement

        :param partner: The partner to attach to the movements
        """
        acc_mov_line_obj = self.pool.get('account.move.line')
        line_ids = []
        date = move_line_id = None

        # Process taxes
        taxes = {}
        ttax = {}
        for line in context['tax_lines']:
            t_tax = taxes.setdefault(
                line.tax_id.id, {'amount': 0, 'base': 0, 'tax_ret': 0}
            )
            # Init tax transfered for this tax at 0
            ttax.setdefault(
                line.tax_id.id, {'amount': 0, 'base': 0, 'tax_ret': 0}
            )
            t_tax['tax_name'] = line.tax_id.name
            t_tax['tax_amount'] = line.tax_id.amount
            t_tax['account_reconcile_id'] = line.tax_id.account_reconcile_id.id
            t_tax['account_collected_id'] = line.tax_id.account_collected_id.id
            t_tax['tax_id'] = line.tax_id.id
            t_tax['amount'] += line.tax_amount
            t_tax['base'] += line.base_amount

        # Totalize all operations
        total = payed = 0
        for line in context['op_lines']:
            total += line.debit - line.credit
        # Totalize all payments
        for line in context['pay_lines']:
            payed += line.credit - line.debit
            move_line_id = line if line.move_id.date > date else move_line_id
            date = line.move_id.date
            # Get the amount of tax already transfered
            if line.tax_reconcile_id:
                # This payment line already transfer tax
                for tline in line.tax_reconcile_id.tax_line_ids:
                    if tline.journal_id.type not in JOURNAL_PAYMENTS:
                        if tline.tax2_id:
                            ttax[tline.tax2_id.id]['amount'] += tline.credit - tline.debit
                            ttax[tline.tax2_id.id]['base'] += tline.tax2_base
                            ttax[tline.tax2_id.id]['tax_ret'] += tline.tax_ret

        # Get factor
        factor = payed / total

        def _get_debit_credit(tax=0, transfered=0):
            """
            Helper function to calculate the correct debit & credit for
            account move lines

            @param tax: Total amount of taxes on invoice
            @param transfered: Total amount of already transfered taxes

            @return: Float amount of credit or debit for line
            """
            amount = tax * factor - transfered
            if amount > 0:
                credit = amount
                debit = 0
            else:
                credit = 0
                debit = -1 * amount
            return debit, credit

        for tax_id in taxes:
            debit, credit = _get_debit_credit(
                taxes[tax_id]['amount'], ttax[tax_id]['amount']
            )
            # Line for tax transfered
            base = taxes[tax_id]['base'] * factor - ttax[tax_id]['base']
            tax_ret = (
                taxes[tax_id]['tax_ret'] * factor - ttax[tax_id]['tax_ret']
            )
            vals = {
                'name': taxes[tax_id]['tax_name'],
                'move_id': move_line_id.move_id.id,
                'date': move_line_id.move_id.date,
                'period_id': move_line_id.move_id.period_id.id,
                'partner_id': partner.id,
                'state': 'valid',
                'currency_id': False,
            }
            line = {
                'credit': credit,
                'debit': debit,
                'account_id': taxes[tax_id]['account_reconcile_id'],
                # This data is used for DIOT report
                'tax2_id': tax_id,
                'tax2_base': base,
                'tax_ret': tax_ret,
            }
            line.update(vals)
            line_id = acc_mov_line_obj.create(
                cr, uid, line, context=None
            )
            line_ids.append(line_id)
            # Line for tax collected
            line = {
                # This is a balance line so we interchange credit & debit
                'credit': debit,
                'debit': credit,
                'account_id': taxes[tax_id]['account_collected_id'],
            }
            line.update(vals)
            line_id = acc_mov_line_obj.create(
                cr, uid, line, context=None
            )
            line_ids.append(line_id)
        return line_ids

    def _tax_reconcile(self, cr, uid, pay_lines, tax_ids, context=None):
        """
        Creates the account.tax.reconcile object for relate payment lines
        with tax transfer lines for handle partial payments on invoices
        """
        pay_ids = [line.id for line in pay_lines if not line.tax_reconcile_id]
        tax_rec_obj = self.pool.get('account.tax.reconcile')
        tax_rec_obj.create(cr, uid, {
            'tax_line_ids': map(lambda x: (4, x, False), tax_ids + pay_ids),
        })

    def reconcile_partial(
        self, cr, uid, ids, type='auto', context=None, writeoff_acc_id=False,
        writeoff_period_id=False, writeoff_journal_id=False
    ):
        """
        Extend account.move.line reconcile_partial to write tax transfer
        when account.move.reconcile needed
        :param ids: account.move.line ids to reconcile
        :type ids: List
        :param writeoff_acc_id: Account where writeoff will to be charged
        :param writeoff_period_id: Period for writeoff move
        :param writeoff_journal_id: Journal where writeoff will be posted
        """
        super(account_move_line, self).reconcile_partial(
            cr, uid, ids=ids,
            type=type, writeoff_acc_id=writeoff_acc_id,
            writeoff_period_id=writeoff_period_id,
            writeoff_journal_id=writeoff_journal_id, context=context
        )
        # Check if there was a partial reconcile
        acc_mov_line = self.browse(cr, uid, ids, context)[0]
        if acc_mov_line.reconcile_partial_id:
            # Call the function that create the account_move with tax transfer
            # and save tax_move_id onto reconcile object
            r_id = acc_mov_line.reconcile_partial_id.id
            self._create_tax_move(cr, uid, ids, r_id, context=None)

        return True

    def reconcile(
        self, cr, uid, ids, type='auto', writeoff_acc_id=False,
        writeoff_period_id=False, writeoff_journal_id=False, context=None
    ):
        """
        Extend account.move.line reconcile to write tax transfer when needed
        :param ids: account.move.line ids to reconcile
        :type ids: List
        :param writeoff_acc_id: Account where writeoff will be charged
        :param writeoff_period_id: Period for writeoff move
        :param writeoff_journal_id: Journal where writeoff will be posted

        :res r_id: account_move_reconcile object id
        """
        r_id = super(account_move_line, self).reconcile(
            cr, uid, ids=ids,
            type=type, writeoff_acc_id=writeoff_acc_id,
            writeoff_period_id=writeoff_period_id,
            writeoff_journal_id=writeoff_journal_id, context=context
        )

        # Call the function that create the account_move with tax transfer
        # and save tax_move_id onto reconcile object
        self._create_tax_move(cr, uid, ids, r_id, context=None)
