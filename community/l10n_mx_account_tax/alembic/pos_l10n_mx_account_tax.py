"""pos_l10n_mx_account_tax

Revision ID: pos_l10n_mx_account_tax
Revises:
Create Date: 2015-10-31 12:40:31.791194

"""

# revision identifiers, used by Alembic.
revision = 'pos_l10n_mx_account_tax'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy.sql import select, table, column, update, text
from sqlalchemy import String, Integer, Date, Boolean, Float

conn = op.get_bind()


def into_dictionary(result):
    lista = []
    for row in conn.execute(result):
        item = dict(zip(row.keys(), row))
        lista.append(item)
    return lista


def upgrade():
    account_tax = table(
        'account_tax',
        column('id', Integer),
        column('name', String),
        column('company_id', Integer),
        column('base_sign', Float),
        column('amount', Float),
    )
    account_invoice_tax = table(
        'account_invoice_tax',
        column('id', Integer),
        column('tax_id', Integer),
        column('name', String),
        column('company_id', Integer),
        column('base_amount', Float),
        column('tax_amount', Float),
    )
    account_move_line = table(
        'account_move_line',
        column('id', Integer),
        column('tax2_id', Integer),
        column('tax2_base', Float),
        column('credit', Float),
        column('debit', Float),
    )
    result_accout_tax = select(
        [account_tax.c.id, account_tax.c.name,
         account_tax.c.company_id, account_tax.c.amount,
         account_tax.c.base_sign]
    )
    list_taxes = into_dictionary(result_accout_tax)

    result_account_invoice_tax = select(
        [account_invoice_tax.c.id, account_invoice_tax.c.name,
         account_invoice_tax.c.company_id, account_invoice_tax.c.base_amount,
         account_invoice_tax.c.tax_amount]
    )
    invoice_taxes = into_dictionary(result_account_invoice_tax)

    for count, invoice_tax in enumerate(invoice_taxes):
        for tax in list_taxes:
            if (
                invoice_tax['name'] == tax['name'] and
                invoice_tax['company_id'] == tax['company_id']
            ):
                tax_id = tax['id']
                base_amount = invoice_tax['base_amount'] * tax['base_sign']
                amount_tax = invoice_tax['tax_amount'] * tax['base_sign']
                print (
                    'Updating account_move_invoice table {0}/{1}'
                    ' id {2} ---- tax_id {3} ---'
                    ' base_amount {4} ---- tax_amount {5}'
                ).format(
                    count+1, len(invoice_taxes),
                    invoice_tax['id'], tax['id'], base_amount,
                    amount_tax,
                )
                udapte_account_invoice_tax = account_invoice_tax.update().where(
                    account_invoice_tax.c.id == invoice_tax['id']
                ).values(
                    tax_id=tax_id,
                    base_amount=base_amount,
                    tax_amount=amount_tax,
                )
                conn.execute(udapte_account_invoice_tax)
                break

    # Update tax2_base in accoout_move_line
    account_move_lines = conn.execute(text(
        """
        SELECT tax2_id, id, debit, credit
        FROM account_move_line
        WHERE tax2_id IS NOT NULL
        """
    ))
    move_line_ids = []
    for row in account_move_lines:
        move_line_id = dict(zip(row.keys(), row))
        move_line_ids.append(move_line_id)

    for count, move_line_id in enumerate(move_line_ids):
        for list_taxe in list_taxes:
            if move_line_id['tax2_id'] == list_taxe['id']:
                tax_amount = (
                    move_line_id['debit'] if move_line_id['debit'] else
                    move_line_id['credit']
                )
                tax2_base = (
                    float(tax_amount) / list_taxe['amount'] * list_taxe['base_sign'] if
                    list_taxe['amount'] else tax_amount
                )
                print (
                    'Updating account_move_line table {3}/{4}'
                    ' id {0} ---- tax2_base {1} ---'
                    ' Tax name {2}'
                ).format(
                    move_line_id['id'], tax2_base,
                    list_taxe['name'], count, len(move_line_ids)
                )
                udapte_account_move_line = account_move_line.update().where(
                    account_move_line.c.id == move_line_id['id']
                ).values(tax2_base=tax2_base)
                conn.execute(udapte_account_move_line)
                continue


def downgrade():
    # TODO
    pass
