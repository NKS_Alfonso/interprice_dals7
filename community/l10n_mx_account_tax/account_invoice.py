# -*- coding: utf-8 -*-
##############################################################################
#
#    Authors: Gerardo García, Agustín Cruz
#    Copyright Fedrojesa S.A. de C.V. 2013 (<http://openpyme.mx>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp import api


class AccountInvoiceTax(osv.Model):
    """
    Add reference to tax object on account.invoice.tax
    """
    _inherit = 'account.invoice.tax'

    _columns = {
        'tax_id': fields.many2one(
            'account.tax', 'Tax',
            required=False, ondelete='set null'
        )
    }

    @api.v8
    def compute(self, invoice):
        """
        Add tax_id to account invoice tax object for be used on
        tax cash payment method computations
        """
        res = super(AccountInvoiceTax, self).compute(invoice)
        for key in res:
            tax_name = res[key]['name']
            taxes = self.env['account.tax'].search(
                [('name', '=', tax_name),
                 ('company_id', '=', invoice.company_id.id)]
            )
            for tax in taxes:
                res[key]['tax_id'] = tax.id
        return res

