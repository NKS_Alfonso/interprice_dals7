# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2014 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#import time
import base64, StringIO, csv
from openerp.osv import orm, fields
from openerp.addons.decimal_precision import decimal_precision as dp
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)
import pdb

header_fields = ['serie', 'producto']

class picking_serial_import(orm.TransientModel):
	_name = 'picking.serial.import'
	_description = 'Import serial numbers'

	_columns = {
		'aml_data': fields.binary('File', required=True, readonly = True),
		'aml_fname': fields.char('Filename', size=128, required=False),
		'csv_separator': fields.selection([(',',','),(';',';')], 'CSV Separator', required=True),
		'decimal_separator': fields.selection([('.','.'),(',',',')], 'Decimal Separator', required=False),
		'note':fields.text('Log'),
		'picking_id': fields.many2one('stock.picking','Albarán'),
	}
	_defaults = {
		'aml_fname': 'archivo.csv',
		'csv_separator': ',',
		'picking_id': lambda self, cr, uid, context=None: self.pool.get('stock.picking').browse(cr, uid, context['active_id'], context=context)
		
		
	}
	
	

	