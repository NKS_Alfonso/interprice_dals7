# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.odoo.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import except_orm
from openerp.tools.float_utils import float_compare, float_round
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)
import time
import pdb

class stock_production_lot(models.Model):
	_inherit = 'stock.production.lot'
	origin_document = fields.Char(string="Documento Origen")
	pedimento = fields.Char(string="Pedimento")
	fecha_pedimento = fields.Date(string="Fecha Pedimento")
	aduana = fields.Char(string="aduana")
	codigo = fields.Char(String="Codigo")
	piking_id = fields.Many2one('stock.picking')

class stock_picking(models.Model):
	_inherit = 'stock.picking'

	def import_serial(self, cr, uid, ids, context=None):
		mod_obj = self.pool.get('ir.model.data')
		wiz_view = mod_obj.get_object_reference(cr, uid, 'stock_picking_import_serial__numbers', 'picking_serial_import_view')
		for move in self.browse(cr, uid, ids, context=context):
			if not move.move_lines:
				raise except_orm(_('Unsupported Function :'),
					_("Import not allowed when not exist move lines. \nPlease add move lines first."))
			
			total_movs = 0
			product_name_list = []

			for ml in move.move_lines:
				total_movs = total_movs + ml.product_uom_qty
				product_name_list.append(ml.product_id.default_code.strip())

			ctx = {
				'stock_picking_id': move.id,
				'pedimento': move.pedimento,
				'fecha_pedimento': move.fecha_pedimento,
				'aduana': move.aduana,
				'total_movs': total_movs,
				'stock_picking_name': move.name,
				'product_name_list': product_name_list,
				'picking_type_code': move.picking_type_id.code,
			}
			act_import = {
				'name':'Import File',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'picking.serial.import',
				'view_id': [wiz_view[1]],
				'nodestroy': True,
				'target': 'new',
				'type': 'ir.actions.act_window',
				'context': ctx,
			}
			return act_import
		

class stock_transfer_details(models.TransientModel):
	_inherit = 'stock.transfer_details'

	def default_get(self, cr, uid, fields, context=None):
		if context is None: context = {}
		res = super(stock_transfer_details, self).default_get(cr, uid, fields, context=context)
		picking_ids = context.get('active_ids', [])
		active_model = context.get('active_model')

		if not picking_ids or len(picking_ids) != 1:
		    # Partial Picking Processing may only be done for one picking at a time
		    return res
		assert active_model in ('stock.picking'), 'Bad context propagation'
		picking_id, = picking_ids
		picking = self.pool.get('stock.picking').browse(cr, uid, picking_id, context=context)
		items = []
		packs = []
		
		if not picking.pack_operation_ids:
		    picking.do_prepare_partial()
		for op in picking.pack_operation_ids:
			#print picking.name
			stock_product_lot_ids = self.pool.get('stock.production.lot').search(cr,uid,[('product_id','=',op.product_id.id),('origin_document','=',picking.name)])
			op_id = op.id
			op_product_id = op.product_id.id
			op_product_uom_id = op.product_uom_id.id
			op_package_id_id = op.package_id.id
			op_location_id = op.location_id.id
			op_location_dest_id = op.location_dest_id.id
			op_result_package_id = op.result_package_id.id
			op_date = op.date
			op_owner_id = op.owner_id.id
			print picking.picking_type_id.code
			if stock_product_lot_ids: #and picking.picking_type_id.code == 'incoming':
				for lot_id in stock_product_lot_ids:
					item = {
			        	'packop_id': False,
			        	'product_id': op_product_id,
				        'product_uom_id': op_product_uom_id,
				        'quantity': 1,
				        'package_id': op_package_id_id,
				        'lot_id': lot_id,
				        'sourceloc_id': op_location_id,
				        'destinationloc_id': op_location_dest_id,
				        'result_package_id': op_result_package_id,
				        'date': op_date, 
				        'owner_id': op_owner_id,
			    	}

					if op.product_id:
						items.append(item)
					elif op.package_id:
						packs.append(item)
			else:
			    item = {
			        'packop_id': op.id,
			        'product_id': op.product_id.id,
			        'product_uom_id': op.product_uom_id.id,
			        'quantity': op.product_qty,
			        'package_id': op.package_id.id,
			        'lot_id': op.lot_id.id,
			        'sourceloc_id': op.location_id.id,
			        'destinationloc_id': op.location_dest_id.id,
			        'result_package_id': op.result_package_id.id,
			        'date': op.date, 
			        'owner_id': op.owner_id.id,
			    }
			    if op.product_id:
			        items.append(item)
			    elif op.package_id:
			        packs.append(item)
		res.update(item_ids=items)
		res.update(packop_ids=packs)
		return res