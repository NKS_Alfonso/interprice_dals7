from openerp.osv import fields, osv
from openerp import models, fields, api
#from openerp.exceptions import except_orm
import pdb

class account_invoice(models.Model):
	_inherit = 'account.invoice'

	picking_name = fields.Char(string="Albaran de salida")
	#subir al servidor 
	def write_lotes(self, cr, uid, ids, context=None):
		"""Esta funcion agrega los lotes a las lineas de la factura,de acuerdo a los lotes asignados
		   por el almacenista en el momento de transferir para darles salida, cuando el proceso
		   general es una VENTA.
		"""
		invoice_lines = self.pool.get('account.invoice.line')
		lotes_ids = ""
		for this in self.browse(cr, uid, ids):
			if this.origin:
				picking_id = self.pool.get('stock.picking').search(cr, uid, [('name','=',this.origin)])[0]
				cr.execute("""SELECT spo.id FROM stock_pack_operation AS spo
								INNER JOIN stock_picking AS sp ON spo.picking_id=sp.id WHERE sp.id=%s""",(picking_id,))
				if cr.rowcount:
					operations_ids = cr.fetchall()
				for invoice_id in this.invoice_line:
					for pack in operations_ids:
						lote = self.pool.get('stock.pack.operation').browse(cr, uid,pack, context=context)
						if lote.product_id.id == invoice_id.product_id.id:
							lotes_ids = lotes_ids + str(lote.lot_id.id)+","
					invoice_lines.write(cr, uid, invoice_id.id, {'lotes': lotes_ids}, context=context)
					lotes_ids = ""
									
			return True
	
	#@api.multi
	def button_dummy(self):
		return True

	def set_serial_by_line(self,cr,uid,ids,context=None):
		for this in self.browse(cr,uid,ids):
			picking_ids = self.pool.get('stock.picking').search(cr,uid,[('name','=',this.picking_name)])
			picking_obj = self.pool.get('stock.picking').browse(cr,uid,picking_ids)
			inv_line = self.pool.get('account.invoice.line')

			if not picking_obj:
				return {
    				'type': 'ir.actions.act_window.message',
    				'title':'Informacion',
    				'message': 'El albaran de salida ' + this.picking_name + 'no existe',
				}

			if not picking_obj.pack_operation_ids:
				return {
    				'type': 'ir.actions.act_window.message',
    				'title':'Informacion',
    				'message': 'El albaran de salida no contiene series',
				}

			serial_list = []
			inv_list = []
			serial_pick_dict = {}
			inv_dict = {}

			for lin in this.invoice_line:
				lin.split_quantities_by_serial()

			for p in picking_obj.pack_operation_ids:
				serial_pick_dict = {'product_id': p.product_id.id, 'serial_id': p.lot_id.id,'pedimento':p.lot_id.pedimento}
				serial_list.append(serial_pick_dict)
			serial_list.sort(key=lambda x: x['product_id'])

			for lin in this.invoice_line:
				inv_dict = {'id': lin.id,'product_id': lin.product_id.id}
				inv_list.append(inv_dict)
			inv_list.sort(key=lambda x: x['product_id'])

			for lin in inv_list:
				indx = 0
				for p in serial_list:
					if p['product_id'] == lin['product_id']:
						inv_line.write(cr,uid,lin['id'],{'serie_id': p['serial_id'],'pedimento': p['pedimento']})
						del serial_list[indx]
						break
					indx += 1
		return True

class account_invoice_line(models.Model):
	_inherit = 'account.invoice.line'
	lotes = fields.Text('Lotes')
	

	@api.multi
	def split_quantities(self):
		for lin in self:
			if lin.quantity>1:
				lin.quantity = (lin.quantity-1)
				new_id = lin.copy(context=self.env.context)
				new_id.quantity = 1
		if self and self[0]:
			return lin.invoice_id.button_dummy()

	@api.multi
	def split_quantities_by_serial(self):
		cont = 0
		for lin in self:
			cont = lin.quantity
			for c in range(int(cont)):
				if lin.quantity>1:
					lin.quantity = (lin.quantity-1)
					new_id = lin.copy(context=self.env.context)
					new_id.quantity = 1
				else:
					break
		if self and self[0]:
			return lin.invoice_id.button_dummy()

	