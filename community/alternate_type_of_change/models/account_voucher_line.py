# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import osv, fields
import openerp.addons.decimal_precision as dp

from openerp import models, _
from openerp import fields as fields8


class AccountVoucherLineV8(models.Model):
    _inherit = 'account.voucher.line'

    original_amount = fields8.Float(
        string=_('Original Amount'),
        required=False,
        readonly=True,
        index=False,
        default=0.0,
        digits=dp.get_precision('todo_dp_payments'),
        help=False,
    )
    original_currency = fields8.Many2one(
        string=_('Original Currency'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='res.currency',
        domain=[],
        context={},
        auto_join=False,
    )
    original_balance = fields8.Float(
        string=_('Original Balance'),
        required=False,
        readonly=True,
        index=False,
        default=0.0,
        digits=dp.get_precision('todo_dp_payments'),
        help=False,
    )


class AccountVoucherLine(osv.osv):
    _inherit = 'account.voucher.line'

    def _compute_balance(self, cr, uid, ids, name, args, context=None):
        currency_pool = self.pool.get('res.currency')
        rs_data = {}
        uid_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company_sale_tax = uid_obj.company_id.tax_provision_customer.amount
        account_apply = """
            SELECT ap.code FROM account_account_apply AS ap
            INNER JOIN account_journal AS aj
            ON aj.account_account_apply_id = ap.id
            INNER JOIN account_voucher as av
            ON av.journal_id = aj.id
            INNER JOIN account_voucher_line as avl
            ON avl.voucher_id = av.id
            WHERE avl.id = {}""".format(ids[0])
        cr.execute(account_apply)
        apply_code = cr.fetchone()[0]

        for line in self.browse(cr, uid, ids, context=context):
            if line.amount > 0:
                ctx = context.copy()
                ctx.update({'date': line.voucher_id.date,
                            'todo_ttype': line.voucher_id.type})
                ttype_rate = line.voucher_id.currency_id.rate
                if line.voucher_id.type == u'receipt':
                    ttype_rate = line.voucher_id.currency_id.rate_sale

                elif line.voucher_id.type == u'payment':
                    ttype_rate = line.voucher_id.currency_id.rate
                ctx.update({
                    'voucher_special_currency': line.voucher_id.payment_rate_currency_id and line.voucher_id.payment_rate_currency_id.id or False,
                    'voucher_special_currency_rate': line.voucher_id.payment_rate * ttype_rate})
                res = {}
                company_currency = line.voucher_id.journal_id.company_id.currency_id.id
                voucher_currency = line.voucher_id.currency_id and line.voucher_id.currency_id.id or company_currency
                move_line = line.move_line_id or False
                if apply_code == 'advance' and (len(move_line.move_id.advance_payment) > 0 or
                                                len(move_line.move_id.advance_payment_supplier) > 0):
                    amount_total_currency = 0.0
                    amount_balance = 0.0
                    has_tax = False
                    for record in move_line.move_id.line_id:
                        if record.account_id.id == uid_obj.company_id.tax_provision_customer.account_paid_voucher_id.id and record.account_id.active:
                            if record.credit > 0:
                                has_tax = True
                    if not move_line:
                        res['amount_original'] = 0.0
                        res['amount_unreconciled'] = 0.0
                    elif voucher_currency == move_line.currency_id.id:
                        if has_tax:
                            amount_total_currency = (move_line.amount_currency) + (
                            move_line.amount_currency * company_sale_tax)
                        else:
                            amount_total_currency = move_line.amount_currency
                        res['amount_original'] = round(abs(amount_total_currency), 1)
                        if move_line.reconcile_partial_id:
                            for payment_line in move_line.reconcile_partial_id.line_partial_ids:
                                if payment_line.id == move_line.id:
                                    continue
                                amount_balance += payment_line.amount_currency
                            if amount_balance > 0:
                                if has_tax:
                                    amount_currency_tax = abs(move_line.amount_currency) * company_sale_tax
                                    amount_currency_with_tax = abs(move_line.amount_currency) + amount_currency_tax
                                    amount_balance = amount_currency_with_tax - amount_balance
                                else:
                                    amount_balance = abs(move_line.amount_currency) - amount_balance
                            res['amount_unreconciled'] = round(abs(amount_balance), 1)
                        else:
                            res['amount_unreconciled'] = round(abs(amount_total_currency), 1)
                    elif not move_line.currency_id.id and voucher_currency == company_currency:
                        if has_tax:
                            amount_total_original = (
                                move_line.credit or move_line.debit) + (
                                (move_line.credit or move_line.debit) * company_sale_tax)
                        else:
                            amount_total_original = move_line.credit or move_line.debit
                        res['amount_original'] = round(abs(amount_total_original), 1)
                        if move_line.reconcile_partial_id:
                            for payment_line in move_line.reconcile_partial_id.line_partial_ids:
                                if payment_line.id == move_line.id:
                                    continue
                                amount_balance += payment_line.debit
                            if amount_balance > 0:
                                if has_tax:
                                    credit_tax = abs(move_line.credit) * company_sale_tax
                                    credit_with_tax = abs(move_line.credit) + credit_tax
                                    amount_balance = credit_with_tax - amount_balance
                                else:
                                    amount_balance = abs(move_line.credit) - amount_balance
                            res['amount_unreconciled'] = round(abs(amount_balance), 1)
                        else:
                            res['amount_unreconciled'] = round(abs(amount_total_original), 1)
                    else:
                        # TODO:UPDATE:BEGIN
                        payment_rate = line.voucher_id.payment_rate
                        # This is going to convert only non local currency on save in order to re render
                        # on the payment view after saved.
                        if line.currency_id.id and voucher_currency == company_currency:
                            if payment_rate != 1 and payment_rate > 0:
                                if has_tax:
                                    amount_total_currency = (
                                        move_line.amount_currency) + (
                                        move_line.amount_currency * company_sale_tax)
                                else:
                                    amount_total_currency = move_line.amount_currency
                                res['amount_original'] = round(abs(
                                    amount_total_currency * payment_rate), 1)
                                if move_line.reconcile_partial_id:
                                    for payment_line in move_line.reconcile_partial_id.line_partial_ids:
                                        if payment_line.id == move_line.id:
                                            continue
                                        amount_balance += payment_line.amount_currency
                                    if amount_balance > 0:
                                        if has_tax:
                                            amount_currency_tax = abs(move_line.amount_currency) * company_sale_tax
                                            amount_currency_with_tax = abs(move_line.amount_currency) + amount_currency_tax
                                            amount_balance = amount_currency_with_tax - amount_balance
                                        else:
                                            amount_balance = abs(move_line.amount_currency) - amount_balance
                                    res['amount_unreconciled'] = round(abs(amount_balance * payment_rate), 1)
                                else:
                                    res['amount_unreconciled'] = round(abs(amount_total_currency * payment_rate), 1)
                        elif not line.currency_id.id and voucher_currency != company_currency:
                            if payment_rate != 1 and payment_rate > 0:
                                if has_tax:
                                    amount_total_original = (
                                        move_line.credit or move_line.debit) + (
                                        (move_line.credit or move_line.debit) * company_sale_tax)
                                else:
                                    amount_total_original = move_line.credit or move_line.debit
                                res['amount_original'] = round(abs(
                                    amount_total_original) / payment_rate, 1)
                                if move_line.reconcile_partial_id:
                                    for payment_line in move_line.reconcile_partial_id.line_partial_ids:
                                        if payment_line.id == move_line.id:
                                            continue
                                        amount_balance += payment_line.debit
                                    if amount_balance > 0:
                                        if has_tax:
                                            credit_tax = abs(move_line.credit) * company_sale_tax
                                            credit_with_tax = abs(move_line.credit) + credit_tax
                                            amount_balance = credit_with_tax - amount_balance
                                        else:
                                            amount_balance = abs(move_line.credit) - amount_balance
                                    res['amount_unreconciled'] = round(abs(amount_balance) / payment_rate, 1)
                                else:
                                    res['amount_unreconciled'] = round(abs(amount_total_original) / payment_rate, 1)
                else:
                    if not move_line:
                        res['amount_original'] = 0.0
                        res['amount_unreconciled'] = 0.0
                    elif voucher_currency == move_line.currency_id.id:
                        res['amount_original'] = abs(move_line.amount_currency)
                        res['amount_unreconciled'] = abs(move_line.amount_residual_currency)
                    elif not move_line.currency_id.id and voucher_currency == company_currency:
                        res['amount_original'] = abs(move_line.credit or move_line.debit or 0.0)
                        res['amount_unreconciled'] = abs(move_line.amount_residual)
                    else:
                        # TODO:UPDATE:BEGIN
                        payment_rate = line.voucher_id.payment_rate
                        # This is going to convert only non local currency on save in order to re render
                        # on the payment view after saved.
                        if line.currency_id.id and voucher_currency == company_currency:
                            if payment_rate != 1 and payment_rate > 0:
                                res['amount_original'] = abs(move_line.amount_currency * payment_rate)
                                res['amount_unreconciled'] = abs(move_line.amount_residual_currency * payment_rate)
                            else:
                                res['amount_original'] = currency_pool.compute(cr, uid, line.currency_id.id, voucher_currency, abs(move_line.amount_currency), round=False, context=ctx)
                                res['amount_unreconciled'] = currency_pool.compute(cr, uid, line.currency_id.id, voucher_currency, abs(move_line.amount_residual_currency), round=False, context=ctx)
                        elif not line.currency_id.id and voucher_currency != company_currency:
                            if payment_rate != 1 and payment_rate > 0:
                                res['amount_original'] = abs(move_line.credit or move_line.debit) / payment_rate
                                res['amount_unreconciled'] = abs(move_line.amount_residual) / payment_rate
                            else:
                                res['amount_original'] = currency_pool.compute(cr, uid, company_currency, voucher_currency, abs(move_line.credit or move_line.debit or 0.0), round=False, context=ctx)
                                res['amount_unreconciled'] = currency_pool.compute(cr, uid, company_currency, voucher_currency, abs(move_line.amount_residual), round=False, context=ctx)
                            # TODO:UPDATE:END
                            # TODO:UPDATE:END
                rs_data[line.id] = res
        return rs_data

    _columns = {
        'amount_original': fields.function(_compute_balance, multi='dc', type='float', string='Original Amount', store=True, digits_compute=dp.get_precision('todo_dp_payments')),
        'amount_unreconciled': fields.function(_compute_balance, multi='dc', type='float', string='Open Balance', store=True, digits_compute=dp.get_precision('todo_dp_payments')),
        'amount': fields.float('Amount', digits_compute=dp.get_precision('todo_dp_payments')),
    }

    def onchange_amount(self, cr, uid, ids, amount=False, amount_unreconciled=False, context={}):
        """
        This will validate amount entered not been bigger than the amount left to be payed.
        """
        if amount > amount_unreconciled:
            amount = 0.0
            res = super(AccountVoucherLine, self).onchange_amount(
                cr, uid, ids, amount, amount_unreconciled, context)
            res['value'].update({'amount': 0.0})
        else:
            res = super(AccountVoucherLine, self).onchange_amount(
                cr, uid, ids, amount, amount_unreconciled, context)
        res['value'].update({'reconcile': (amount == amount_unreconciled)})
        return res
