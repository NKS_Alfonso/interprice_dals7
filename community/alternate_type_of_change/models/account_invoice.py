# -*- encoding: utf-8 -*-
from openerp import models, fields, api
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    currency_rate_alter = fields.Float(string='TC Alternative', digits=(10, 4))
    hide_currency_rate_alter_field = fields.Boolean(
        string='',
        required=False,
        readonly=True,
        index=False,
        default=False,
        help=False
    )

    # @api.model
    def create(self, cr, uid, vals, context=None):
        try:
            pool_currency = self.pool.get('res.currency')
            obj_currency_id = pool_currency.browse(cr, uid, vals['currency_id'])
            pool_company = self.pool.get('res.company')
            obj_company_id = pool_company.browse(cr, uid, vals['company_id'])
            if obj_currency_id == obj_company_id.currency_id:
                vals['hide_currency_rate_alter_field'] = True
            else:
                vals['hide_currency_rate_alter_field'] = False
            currency_rate_alter = 0.0
            doc_name = ""
            obj_purchase_order = self.pool.get('purchase.order')

            if vals['name']:
                doc_name = vals['name']
            elif vals['origin']:
                doc_name = vals['origin']
            #
            if vals['origin']:
                obj_purchase_order_ids = self.pool.get('purchase.order').search(cr, uid, [('name', '=', doc_name)])
                for p in obj_purchase_order.browse(cr, uid, obj_purchase_order_ids):
                    currency_rate_alter = p.currency_rate_alter

                vals['currency_rate_alter'] = currency_rate_alter
        except Exception, e:
            _logger.error(e)
        finally:
            return super(AccountInvoice, self).create(cr, uid, vals, context=context)

    @api.one
    def write(self, vals):
        if type(vals) is dict and 'currency_id' in vals:
            pool_currency = self.env['res.currency']
            obj_currency_id = pool_currency.browse(vals['currency_id'])
            obj_company_id = self.company_id
            if obj_currency_id == obj_company_id.currency_id:
                vals['hide_currency_rate_alter_field'] = True
            else:
                vals['hide_currency_rate_alter_field'] = False
        return super(AccountInvoice, self).write(vals)

    @api.multi
    def compute_invoice_totals(self, company_currency, ref, invoice_move_lines):
        total = 0
        total_currency = 0
        for line in invoice_move_lines:
            if self.currency_id != company_currency:
                currency = self.currency_id.with_context(date=self.date_invoice or fields.Date.context_today(self))
                line['currency_id'] = currency.id
                line['amount_currency'] = currency.round(line['price'])
                if self.currency_rate_alter or self.currency_rate_alter > 0:
                    line['price'] = self.currency_id.round(line['price'] * self.currency_rate_alter)
                else:
                    line['price'] = currency.compute(line['price'], company_currency)
            else:
                line['currency_id'] = False
                line['amount_currency'] = False
                line['price'] = self.currency_id.round(line['price'])
            line['ref'] = ref
            if self.type in ('out_invoice', 'in_refund'):
                total += line['price']
                total_currency += line['amount_currency'] or line['price']
                line['price'] = - line['price']
            else:
                total -= line['price']
                total_currency -= line['amount_currency'] or line['price']
        return total, total_currency, invoice_move_lines

    def invoice_pay_customer(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account_voucher',
                                                                             'view_vendor_receipt_dialog_form')

        inv = self.browse(cr, uid, ids[0], context=context)
        return {
            'name': _("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
                'currency_rate_alter': inv.currency_rate_alter
            }
        }

    @api.onchange('currency_id')
    def hide_currency_rate_alter(self):
        if self.journal_id.company_id.currency_id.id == self.currency_id.id:
            self.hide_currency_rate_alter_field = True
            self.currency_rate_alter = 0.0
        else:
            self.hide_currency_rate_alter_field = False
