import time

from openerp import workflow
from openerp.osv import osv
from openerp.tools.translate import _


class account_move_line(osv.osv):
    _inherit = 'account.move.line'

    def reconcile(self, cr, uid, ids, type='auto',
                  writeoff_acc_id=False, writeoff_period_id=False,
                  writeoff_journal_id=False, context=None):
        """
        Core function completely overwritten in order to skip writeoff account move creation.

        It will create writeoff account move only if 'todo_exec_writeoff' = True is in context and
        a writeoff_acc_id is given, one can assign a writeoff_acc_id when compleating an Invoice
        payment (on the last payment in multiple payments or just one big payment) then selecting
        the payment option such as "with writeoff" and finally select the the counterpart account
        in matter.
        """
        if ids:
          with_writeoff = context.get('with_writeoff', True)
          fix_writeoff_amounts = context.get('fix_writeoff_amounts', False)
          account_obj = self.pool.get('account.account')
          move_obj = self.pool.get('account.move')
          move_rec_obj = self.pool.get('account.move.reconcile')
          partner_obj = self.pool.get('res.partner')
          currency_obj = self.pool.get('res.currency')
          #lines = self.browse(cr, uid, ids, context=context)

          cr.execute("""
                         SELECT 
                             AML.id line0,
                             AML.partner_id partner_id1,
                             CASE WHEN AML.partner_id is not null THEN True ELSE False END AS partner2,
                             AML.company_id company_id3
                         FROM account_move_line AML
                         WHERE AML.id in %s
                         GROUP BY AML.id
                     """, (tuple(ids),))

          select_lines_sql = cr.fetchall()

          cr.execute("""
                         SELECT 
                             AML.company_id
                         FROM account_move_line AML
                         WHERE AML.id in %s
                         GROUP BY AML.company_id
                     """, (tuple(ids),))

          select_lines_company_sql = cr.fetchall()
          #print(select_lines_sql)

          #unrec_lines = filter(lambda x: not x['reconcile_id'], lines)
          cr.execute("""
                         SELECT 
                             AML.id line0,
                             AML.ref line_ref1,
                             AML.state line_state2,
                             AML.name line_name3,
                             CASE WHEN AI.id  is not null THEN True ELSE False END AS line_invoice4,
                             COMPANY.currency_id line_company_id_currency_id5,
                             COALESCE(AML.currency_id,0) line_currency_id6,
                             CASE WHEN AML.tax_code_id is not null THEN True ELSE False END AS line_tax_code7,
                             COALESCE(AML.tax_code_id,0) line_tax_code_id8,
                             COALESCE(AML.credit,0) line_credit9,
                             COALESCE(AML.debit,0) line_debit10,
                             COALESCE(AML.amount_currency,0) line_amount_currency11,
                             AML.account_id line_account_id12,
                             CASE WHEN AML.partner_id is not null THEN True ELSE False END AS line_partner13,
                             AML.partner_id line_partner_id14,
                             COALESCE(AA.reconcile,False) account_reconcile15,
                             CASE WHEN AML.currency_id is not null THEN True ELSE False END AS line_currency16,
                             AML.date line_date17,
                             COALESCE(COMPANY_AA.currency_id,0) line_account_id_company_id_currency_id18,
                             COALESCE(AA.currency_id,0) account_currency_id19
                         FROM account_move_line AML
                         LEFT JOIN account_move AM ON AML.move_id=AM.id
                         LEFT JOIN account_invoice AI ON AM.id=AI.move_id
                         LEFT JOIN res_company COMPANY ON AML.company_id=COMPANY.id
                         LEFT JOIN account_account AA ON AML.account_id=AA.id
                         LEFT JOIN res_company COMPANY_AA ON AA.company_id=COMPANY_AA.id
                         WHERE AML.id in %s and AML.reconcile_id is null
                         GROUP BY AML.id,AI.id,COMPANY.id,AA.id,COMPANY_AA.id
                     """, (tuple(ids),))
          select_unrec_lines_sql = cr.fetchall()
          #print(select_unrec_lines_sql)

          credit = debit = 0.0
          currency = 0.0
          writeoff_covert = False
          account_id = False
          partner_id = False
          reconcile = False
          writeoff_for_tax = False
          ref = ''
          if context is None:
              context = {}

          #print ('Parte 1.1 reconcile')
          company_list = []
          for line in select_lines_sql:
              if company_list and not line[3] in company_list:
                  raise osv.except_osv(_('Warning!'), _('To reconcile the entries company should \
                                                        be the same for all entries.'))
              company_list.append(line[3])

          #print ('Parte 1.2 reconcile')

          #print ('Parte 2.1 reconcile')
          for line in select_unrec_lines_sql:
              # Uncomment this for debugging
              # print "---------------------------"
              # print "Name: ", line.name
              # print "Ref: ", line.ref
              # print "Debit: ", line.debit
              # print "Credit: ", line.credit
              # print "amount currency: ", line.amount_currency
              ref = line[1]
              if line[2] <> 'valid':
                  raise osv.except_osv(_('Error!'),
                          _('Entry "%s" is not valid !') % line[3])
              if line[4] and line[5] != line[6]:
                  writeoff_covert = True
                  writeoff_covert_currency = line[6]
                  writeoff_covert_company_currency = line[5]
              if line[7]:
                  writeoff_for_tax = True
              credit += line[9]
              debit += line[10]
              currency += line[11]
              account_id = line[12]
              partner_id = (line[13] and line[14]) or False
              reconcile = line[15]

              account_company_id_currency_id = line[5]
              account_currency_id=line[19]

          #print ('Parte 2.2 reconcile')

          #print ('Parte 3.1 reconcile')
          if context.get('todo_writeoff_amount', False):
              writeoff = context.get('todo_writeoff_amount')
          elif writeoff_covert and currency != 0.0 and not writeoff_for_tax:
              sing = 1 if writeoff_covert > 0.0 else -1
              writeoff = currency_obj.compute(
                  cr, uid, writeoff_covert_currency, writeoff_covert_company_currency,
                  abs(currency), context=context) * sing
          elif writeoff_covert and currency == 0.0:
              writeoff = 0.0
          else:
              writeoff = debit - credit
          #print ('Parte 3.2 reconcile')

          #print ('Parte 4.1 reconcile')
          # Ifdate_p in context => take this date
          if context.has_key('date_p') and context['date_p']:
              date = context['date_p']
          else:
              date = time.strftime('%Y-%m-%d')
          #print ('Parte 4.2 reconcile')

          #print ('Parte 5.1 reconcile')
          cr.execute('SELECT account_id, reconcile_id '\
                     'FROM account_move_line '\
                     'WHERE id IN %s '\
                     'GROUP BY account_id,reconcile_id',
                     (tuple(ids), ))
          r = cr.fetchall()
          # TODO: move this check to a constraint in the account_move_reconcile object
          if len(r) != 1:pass
              # raise osv.except_osv(_('Error'), _('Entries are not of the same account or \
              # already reconciled ! '))
          if len(select_unrec_lines_sql)==0:
              raise osv.except_osv(_('Error!'), _('Entry is already reconciled.'))
          #account = account_obj.browse(cr, uid, account_id, context=context)
          if not reconcile:
              raise osv.except_osv(_('Error'), _('The account is not defined to be reconciled !'))
          if r[0][1] != None:
              raise osv.except_osv(_('Error!'), _('Some entries are already reconciled.'))
          #print ('Parte 5.2 reconcile')

          #print ('Parte 6.1 reconcile')
          if with_writeoff:
              account_currency_obj=currency_obj.browse(cr, uid, account_currency_id, context=context)
              account_company_currency_obj=currency_obj.browse(cr, uid, account_company_id_currency_id, context=context)
              if (not currency_obj.is_zero(cr, uid, account_company_currency_obj, writeoff)) or \
                 (account_currency_obj and (not currency_obj.is_zero(cr,
                                                                    uid,
                                                                    account_currency_obj,
                                                                    currency))):
                  if not writeoff_acc_id:
                      raise osv.except_osv(_('Warning!'), _('You have to provide an account for the \
                                                            write off/exchange difference entry.'))
                  if writeoff > 0:
                      debit = writeoff
                      credit = 0.0
                      self_credit = writeoff
                      self_debit = 0.0
                  else:
                      debit = 0.0
                      credit = -writeoff
                      self_credit = 0.0
                      self_debit = -writeoff
                  # If comment exist in context, take it
                  if 'comment' in context and context['comment']:
                      libelle = context['comment']
                  else:
                      libelle = _('Write-Off')

                  #cur_obj = self.pool.get('res.currency')
                  cur_id = False
                  amount_currency_writeoff = 0.0

                  if not writeoff_covert_currency and context.get('company_currency_id', False):
                      writeoff_covert_currency = context['company_currency_id']

                  if fix_writeoff_amounts:
                      # USD payment, this will fix values for writeoff
                      if context.get('company_currency_id', False) != context.get('currency_id', False) and writeoff:
                          cur_id = context['currency_id']
                          amount_currency_writeoff = writeoff
                          todo_writeoff = currency_obj.compute(cr, uid,
                                                          context.get('currency_id', False),
                                                          context.get('company_currency_id', False),
                                                          writeoff,
                                                          context=context)
                          if todo_writeoff > 0:
                              debit = self_credit = todo_writeoff
                          else:
                              credit = self_debit = -todo_writeoff
                      elif context.get('company_currency_id', False) == writeoff_covert_currency:
                          amount_currency_writeoff = 0.0
                      else:
                          # MXN Payment
                          amount_currency_writeoff = currency_obj.compute(cr, uid,
                                                                     context.get('company_currency_id', False),
                                                                     writeoff_covert_currency,
                                                                     writeoff,
                                                                     context=context)

                  else:
                      # This is orinal behaviour
                      if context.get('company_currency_id', False) != context.get('currency_id', False):
                          cur_id = context.get('currency_id', False)
                          for line in select_unrec_lines_sql:
                              if line[16] and line[6] == \
                                      context.get('currency_id', False):
                                  amount_currency_writeoff += line[11]
                              else:
                                  tmp_amount = currency_obj.compute(cr, uid,
                                                               line[18],
                                                               context.get('currency_id', False),
                                                               abs(line[10] - line[9]),
                                                               context={'date': line[17]})
                                  amount_currency_writeoff += (line[10] > 0) and tmp_amount or \
                                      -tmp_amount

                  writeoff_lines = [
                      (0, 0, {
                          'name': libelle,
                          'debit': self_debit,
                          'credit': self_credit,
                          'account_id': account_id,
                          'date': date,
                          'ref': ref,
                          'partner_id': partner_id,
                          'currency_id': cur_id or (account_currency_id or False),
                          'amount_currency': amount_currency_writeoff and -1 *
                          amount_currency_writeoff or (account_currency_id and -1 *
                                                       currency or 0.0)
                      }),
                      (0, 0, {
                          'name': libelle,
                          'debit': debit,
                          'credit': credit,
                          'account_id': writeoff_acc_id,
                          'analytic_account_id': context.get('analytic_id', False),
                          'ref': ref,
                          'partner_id': partner_id,
                          'currency_id': cur_id or (account_currency_id or False),
                          'amount_currency': amount_currency_writeoff and
                          amount_currency_writeoff or (account_currency_id and currency or 0.0)
                      })
                  ]

                  writeoff_move_id = move_obj.create(cr, uid, {
                      'period_id': writeoff_period_id,
                      'journal_id': writeoff_journal_id,
                      'ref': ref,
                      'date': date,
                      'state': 'draft',
                      'line_id': writeoff_lines
                  })

                  writeoff_line_ids = self.search(cr, uid, [('move_id', '=', writeoff_move_id),
                                                  ('account_id', '=', account_id)])
                  if account_id == writeoff_acc_id:
                      writeoff_line_ids = [writeoff_line_ids[1]]
                  ids += writeoff_line_ids
          #print ('Parte 6.2 reconcile')
          # marking the lines as reconciled does not change their validity, so there is no need
          # to revalidate their moves completely.
          #print ('Parte 7.1 reconcile')
          reconcile_context = dict(context, novalidate=True)
          """
          create_uid=uid
          name=self.pool.get('ir.sequence').get(cr, uid, 'account.reconcile') or '/'
          opening_reconciliation=False
          write_uid=uid
          insert_sql=(('''insert into account_move_reconcile (create_uid,create_date,name,opening_reconciliation,write_uid,write_date,type) 
                          values (%s,CURRENT_DATE,'%s',%s,%s,now() at time zone 'UTC','%s') 
                          RETURNING id;
                      ''')% (create_uid,name,opening_reconciliation,write_uid,type))
          print(insert_sql)
          cr.execute(insert_sql)
          insert_id=cr.fetchall()
          r_id=insert_id[0][0]
          print(r_id)
          """
          r_id = move_rec_obj.create(cr, uid, {
              'type': type,
          }, context=reconcile_context)
          #print(r_id)
          r_id_obj=self.pool('account.move.reconcile').browse(cr, uid, r_id, context=context)
          name=r_id_obj.name
          """
          r_id = move_rec_obj.create(cr, uid, {
              'type': type,
              'line_id': map(lambda x: (4, x, False), ids),
              'line_partial_ids': map(lambda x: (3, x, False), ids)
          }, context=reconcile_context)
          """
          update_sql=(('''update account_move_line set reconcile_partial_id=null,reconcile_id=%s,reconcile_ref='%s' where id  in %s''')% (r_id,name,tuple(ids)))
          #print(update_sql)
          cr.execute(update_sql)

          #print ('Parte 7.2 reconcile')
          # the id of the move.reconcile is written in the move.line (self)
          # by the create method above because of the way the line_id are defined: (4, x, False)
          #print ('Parte 8.1 reconcile')
          for id in ids:
              workflow.trg_trigger(uid, 'account.move.line', id, cr)
          #print ('Parte 8.2 reconcile')
          #import pdb; pdb.set_trace()
          if select_lines_sql and select_lines_sql[0][0]:
          #if lines and lines[0]:
              #print ('Parte 9.1 reconcile')
              partner_id = select_lines_sql[0][2] and select_lines_sql[0][1] or False
              if partner_id and not partner_obj.has_something_to_reconcile(cr, uid, partner_id,
                                                                           context=context):
                  partner_obj.mark_as_reconciled(cr, uid, [partner_id], context=context)
              #print ('Parte 9.2 reconcile')
          return r_id

        with_writeoff = context.get('with_writeoff', True)
        fix_writeoff_amounts = context.get('fix_writeoff_amounts', False)
        account_obj = self.pool.get('account.account')
        move_obj = self.pool.get('account.move')
        move_rec_obj = self.pool.get('account.move.reconcile')
        partner_obj = self.pool.get('res.partner')
        currency_obj = self.pool.get('res.currency')
        lines = self.browse(cr, uid, ids, context=context)
        unrec_lines = filter(lambda x: not x['reconcile_id'], lines)
        credit = debit = 0.0
        currency = 0.0
        writeoff_covert = False
        account_id = False
        partner_id = False
        writeoff_for_tax = False
        ref = ''
        if context is None:
            context = {}
        company_list = []
        for line in lines:
            if company_list and not line.company_id.id in company_list:
                raise osv.except_osv(_('Warning!'), _('To reconcile the entries company should \
                                                      be the same for all entries.'))
            company_list.append(line.company_id.id)
        for line in unrec_lines:
            # Uncomment this for debugging
            # print "---------------------------"
            # print "Name: ", line.name
            # print "Ref: ", line.ref
            # print "Debit: ", line.debit
            # print "Credit: ", line.credit
            # print "amount currency: ", line.amount_currency
            ref = line.ref
            if line.state <> 'valid':
                raise osv.except_osv(_('Error!'),
                        _('Entry "%s" is not valid !') % line.name)
            if line.invoice and line.company_id.currency_id.id != line.currency_id.id:
                writeoff_covert = True
                writeoff_covert_currency = line.currency_id.id
                writeoff_covert_company_currency = line.company_id.currency_id.id
            if line.tax_code_id.id:
                writeoff_for_tax = True
            credit += line['credit']
            debit += line['debit']
            currency += line['amount_currency'] or 0.0
            account_id = line['account_id']['id']
            partner_id = (line['partner_id'] and line['partner_id']['id']) or False

        if context.get('todo_writeoff_amount', False):
            writeoff = context.get('todo_writeoff_amount')
        elif writeoff_covert and currency != 0.0 and not writeoff_for_tax:
            sing = 1 if writeoff_covert > 0.0 else -1
            writeoff = currency_obj.compute(
                cr, uid, writeoff_covert_currency, writeoff_covert_company_currency,
                abs(currency), context=context) * sing
        elif writeoff_covert and currency == 0.0:
            writeoff = 0.0
        else:
            writeoff = debit - credit

        # Ifdate_p in context => take this date
        if context.has_key('date_p') and context['date_p']:
            date = context['date_p']
        else:
            date = time.strftime('%Y-%m-%d')

        cr.execute('SELECT account_id, reconcile_id '\
                   'FROM account_move_line '\
                   'WHERE id IN %s '\
                   'GROUP BY account_id,reconcile_id',
                   (tuple(ids), ))
        r = cr.fetchall()
        # TODO: move this check to a constraint in the account_move_reconcile object
        if len(r) != 1:pass
            # raise osv.except_osv(_('Error'), _('Entries are not of the same account or \
            # already reconciled ! '))
        if not unrec_lines:
            raise osv.except_osv(_('Error!'), _('Entry is already reconciled.'))
        account = account_obj.browse(cr, uid, account_id, context=context)
        if not account.reconcile:
            raise osv.except_osv(_('Error'), _('The account is not defined to be reconciled !'))
        if r[0][1] != None:
            raise osv.except_osv(_('Error!'), _('Some entries are already reconciled.'))

        if with_writeoff:
            if (not currency_obj.is_zero(cr, uid, account.company_id.currency_id, writeoff)) or \
               (account.currency_id and (not currency_obj.is_zero(cr,
                                                                  uid,
                                                                  account.currency_id,
                                                                  currency))):
                if not writeoff_acc_id:
                    raise osv.except_osv(_('Warning!'), _('You have to provide an account for the \
                                                          write off/exchange difference entry.'))
                if writeoff > 0:
                    debit = writeoff
                    credit = 0.0
                    self_credit = writeoff
                    self_debit = 0.0
                else:
                    debit = 0.0
                    credit = -writeoff
                    self_credit = 0.0
                    self_debit = -writeoff
                # If comment exist in context, take it
                if 'comment' in context and context['comment']:
                    libelle = context['comment']
                else:
                    libelle = _('Write-Off')

                cur_obj = self.pool.get('res.currency')
                cur_id = False
                amount_currency_writeoff = 0.0

                if not writeoff_covert_currency and context.get('company_currency_id', False):
                    writeoff_covert_currency = context['company_currency_id']

                if fix_writeoff_amounts:
                    # USD payment, this will fix values for writeoff
                    if context.get('company_currency_id', False) != context.get('currency_id', False) and writeoff:
                        cur_id = context['currency_id']
                        amount_currency_writeoff = writeoff
                        todo_writeoff = cur_obj.compute(cr, uid,
                                                        context.get('currency_id', False),
                                                        context.get('company_currency_id', False),
                                                        writeoff,
                                                        context=context)
                        if todo_writeoff > 0:
                            debit = self_credit = todo_writeoff
                        else:
                            credit = self_debit = -todo_writeoff
                    elif context.get('company_currency_id', False) == writeoff_covert_currency:
                        amount_currency_writeoff = 0.0
                    else:
                        # MXN Payment
                        amount_currency_writeoff = cur_obj.compute(cr, uid,
                                                                   context.get('company_currency_id', False),
                                                                   writeoff_covert_currency,
                                                                   writeoff,
                                                                   context=context)

                else:
                    # This is orinal behaviour
                    if context.get('company_currency_id', False) != context.get('currency_id', False):
                        cur_id = context.get('currency_id', False)
                        for line in unrec_lines:
                            if line.currency_id and line.currency_id.id == \
                                    context.get('currency_id', False):
                                amount_currency_writeoff += line.amount_currency
                            else:
                                tmp_amount = cur_obj.compute(cr, uid,
                                                             line.account_id.company_id.currency_id.id,
                                                             context.get('currency_id', False),
                                                             abs(line.debit - line.credit),
                                                             context={'date': line.date})
                                amount_currency_writeoff += (line.debit > 0) and tmp_amount or \
                                    -tmp_amount

                writeoff_lines = [
                    (0, 0, {
                        'name': libelle,
                        'debit': self_debit,
                        'credit': self_credit,
                        'account_id': account_id,
                        'date': date,
                        'ref': ref,
                        'partner_id': partner_id,
                        'currency_id': cur_id or (account.currency_id.id or False),
                        'amount_currency': amount_currency_writeoff and -1 *
                        amount_currency_writeoff or (account.currency_id.id and -1 *
                                                     currency or 0.0)
                    }),
                    (0, 0, {
                        'name': libelle,
                        'debit': debit,
                        'credit': credit,
                        'account_id': writeoff_acc_id,
                        'analytic_account_id': context.get('analytic_id', False),
                        'ref': ref,
                        'partner_id': partner_id,
                        'currency_id': cur_id or (account.currency_id.id or False),
                        'amount_currency': amount_currency_writeoff and
                        amount_currency_writeoff or (account.currency_id.id and currency or 0.0)
                    })
                ]

                writeoff_move_id = move_obj.create(cr, uid, {
                    'period_id': writeoff_period_id,
                    'journal_id': writeoff_journal_id,
                    'ref': ref,
                    'date': date,
                    'state': 'draft',
                    'line_id': writeoff_lines
                })

                writeoff_line_ids = self.search(cr, uid, [('move_id', '=', writeoff_move_id),
                                                ('account_id', '=', account_id)])
                if account_id == writeoff_acc_id:
                    writeoff_line_ids = [writeoff_line_ids[1]]
                ids += writeoff_line_ids

        # marking the lines as reconciled does not change their validity, so there is no need
        # to revalidate their moves completely.
        reconcile_context = dict(context, novalidate=True)
        r_id = move_rec_obj.create(cr, uid, {
            'type': type,
            'line_id': map(lambda x: (4, x, False), ids),
            'line_partial_ids': map(lambda x: (3, x, False), ids)
        }, context=reconcile_context)
        # the id of the move.reconcile is written in the move.line (self)
        # by the create method above because of the way the line_id are defined: (4, x, False)
        for id in ids:
            workflow.trg_trigger(uid, 'account.move.line', id, cr)

        if lines and lines[0]:
            partner_id = lines[0].partner_id and lines[0].partner_id.id or False
            if partner_id and not partner_obj.has_something_to_reconcile(cr, uid, partner_id,
                                                                         context=context):
                partner_obj.mark_as_reconciled(cr, uid, [partner_id], context=context)
        return r_id
