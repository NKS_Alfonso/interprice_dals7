# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import osv
from openerp.tools.translate import _


class ResCurrency(osv.osv):
    _inherit = "res.currency"

    def _get_conversion_rate(self, cr, uid, from_currency, to_currency, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        from_currency = self.browse(cr, uid, from_currency.id, context=ctx)
        to_currency = self.browse(cr, uid, to_currency.id, context=ctx)

        if from_currency.rate <= 0 or to_currency.rate <= 0:
            raise osv.except_osv(_('Wrong exchange rate'), _(
                "The exchange rate should be bigger than zero.\n Please review currency configuration."))
            return 0.0

        if ctx.get('todo_ttype', False) == u'receipt' and hasattr(from_currency, 'rate_sale'):
            if from_currency.rate_sale <= 0 or to_currency.rate_sale <= 0:
                raise osv.except_osv(_('Wrong exchange rate'), _(
                    "The exchange rate should be bigger than zero.\n Please review currency configuration."))
                return 0.0
            else:
                _from_currency_rate = from_currency.rate_sale
                _to_currency_rate = to_currency.rate_sale
        elif ctx.get('todo_ttype', False) == u'payment':
            _from_currency_rate = from_currency.rate
            _to_currency_rate = to_currency.rate
        else:
            _from_currency_rate = from_currency.rate
            _to_currency_rate = to_currency.rate

        return _to_currency_rate / _from_currency_rate
        # FINAL (SE AGREGA PARA EL FUCINONAMIENTO CORRECTO DEL MODULO DNI (Proveedores = TC Compra, Clientes = TC Venta))
