# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, api


class account_invoice_line(models.Model):
        _inherit = "account.invoice.line"

        @api.model
        def move_line_get(self, invoice_id):
            inv = self.env['account.invoice'].browse(invoice_id)
            currency = inv.currency_id.with_context(date=inv.date_invoice)
            company_currency = inv.company_id.currency_id
            res = []
            for line in inv.invoice_line:
                mres = self.move_line_get_item(line)
                mres['invl_id'] = line.id
                res.append(mres)
                tax_code_found = False
                taxes = line.invoice_line_tax_id.compute_all(
                    (line.price_unit * (1.0 - (line.discount or 0.0) / 100.0)),
                    line.quantity, line.product_id, inv.partner_id)['taxes']
                for tax in taxes:
                    if inv.type in ('out_invoice', 'in_invoice'):
                        tax_code_id = tax['base_code_id']
                        tax_amount = tax['price_unit'] * line.quantity * tax['base_sign']
                    else:
                        tax_code_id = tax['ref_base_code_id']
                        tax_amount = tax['price_unit'] * line.quantity * tax['ref_base_sign']

                    if tax_code_found:
                        if not tax_code_id:
                            continue
                        res.append(dict(mres))
                        res[-1]['price'] = 0.0
                        res[-1]['account_analytic_id'] = False
                    elif not tax_code_id:
                        continue
                    tax_code_found = True

                    res[-1]['tax_code_id'] = tax_code_id
                    if inv.currency_id.id != inv.company_id.currency_id.id and \
                            line.invoice_id.currency_rate_alter and line.invoice_id.currency_rate_alter > 0:
                        res[-1]['tax_amount'] = tax_amount * line.invoice_id.currency_rate_alter
                    else:
                        res[-1]['tax_amount'] = currency.compute(tax_amount, company_currency)

            return res
