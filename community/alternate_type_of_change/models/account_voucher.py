# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp import models
import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round


class AccountVoucher(models.Model):
    _inherit = 'account.voucher'

    def _get_writeoff_amount(self, cr, uid, ids, name, args, context=None):
        if not ids: return {}
        # currency_obj = self.pool.get('res.currency')
        res = {}
        for voucher in self.browse(cr, uid, ids, context=context):
            debit = credit = 0.0
            sign = voucher.type == 'payment' and -1 or 1
            for l in voucher.line_dr_ids:
                debit += l.amount
            for l in voucher.line_cr_ids:
                credit += l.amount
            # currency = voucher.currency_id or voucher.company_id.currency_id
            # writeoff amount
            res[voucher.id] = voucher.amount - sign * (credit - debit)
        return res

    _columns = {
        'currency_rate_alter': fields.float(string='TC Alternative',
                                            digits=(10, 4)),
        'net_amount': fields.float('Amount',
                                   digits=dp.get_precision('todo_dp_payments'),
                                   required=True,
                                   default=0.0,    # for compatibility with other modules
                                   readonly=True,
                                   states={'draft': [('readonly', False)]},
                                   help='Amount Paid With Journal Method'),
        'paylines_amount': fields.float(_('Paylines Amount'),
                                        compute='_get_paylines_amount',
                                        digits=dp.get_precision('todo_dp_payments'),
                                        help=_('Amount Paid With Paylines: checks, withholdings, etc.')),
        'amount_readonly': fields.float(related='amount',
                                        string='Total Amount',
                                        digits=dp.get_precision('todo_dp_payments'),
                                        readonly=True),
        'amount': fields.float(string='Total Amount',
                               compute='_get_amount',
                               digits=dp.get_precision('todo_dp_payments'),
                               inverse='_set_net_amount',
                               help='Total Amount Paid',
                               copy=False,
                               store=True),
        'writeoff_amount': fields.function(_get_writeoff_amount,
                                           string='Difference Amount',
                                           type='float',
                                           digits=dp.get_precision('todo_dp_payments'),
                                           readonly=True,
                                           help="Computed as the difference between the amount stated in the voucher and the sum of allocation on the voucher lines.")
    }

    def get_currency_rate(self, cr, uid, currency_id, context={}):
        currency_id_obj = self.pool.get('res.currency').browse(cr, uid, currency_id, context=context)
        if context.get('type', False) == u'receipt' or context.get('todo_ttype', False) == u'receipt':
            ttype_rate = currency_id_obj.rate_sale

        elif context.get('type', False) == u'payment' or context.get('todo_ttype', False) == u'payment':
            ttype_rate = currency_id_obj.rate
        else:
            ttype_rate = 0.0
        return ttype_rate

    def _convert_amount(self, cr, uid, amount, voucher_id, context=None):
        '''
        This function convert the amount given in company currency. It takes either the rate in the voucher (if the
        payment_rate_currency_id is relevant) either the rate encoded in the system.

        :param amount: float. The amount to convert
        :param voucher: id of the voucher on which we want the conversion
        :param context: to context to use for the conversion. It may contain the key 'date' set to the voucher date
            field in order to select the good rate to use.
        :return: the amount in the currency of the voucher's company
        :rtype: float
        '''
        voucher = self.browse(cr, uid, voucher_id, context=context)
        if context is None:
            context = {}
        currency_obj = self.pool.get('res.currency')
        ctx = dict(context or {})
        ctx.update({'todo_ttype': voucher.type})
        currency_evaluation = currency_obj.browse(cr, uid, voucher.currency_id.id, context=ctx)
        rate_customer = currency_evaluation.rate_sale
        rate_supplier = currency_evaluation.rate
        if voucher.type == u'receipt':
            if not rate_customer:
                raise osv.except_osv(_('Wrong exchange rate'), _(
                    "The exchange rate for customer should be bigger than zero.\n Please review currency configuration."))
        elif voucher.type == u'payment':
            if not rate_supplier:
                raise osv.except_osv(_('Wrong exchange rate'), _(
                    "The exchange rate for supplier should be bigger than zero.\n Please review currency configuration."))
        if voucher.payment_rate != 1 and voucher.payment_rate > 0 and voucher.company_id.currency_id != voucher.currency_id:
            amount_converted = float_round(amount * voucher.payment_rate, precision_rounding=currency_evaluation.rounding)
        else:
            amount_converted = currency_obj.compute(cr, uid, voucher.currency_id.id, voucher.company_id.currency_id.id, amount, context=ctx)
        return amount_converted

    def onchange_rate(self, cr, uid, ids, rate, amount, currency_id, payment_rate_currency_id, company_id,
                      context=None):
        res = {'value': {'paid_amount_in_company_currency': amount,
                         'currency_help_label': self._get_currency_help_label(cr, uid, currency_id, rate,
                                                                              payment_rate_currency_id,
                                                                              context=context)}}
        amount_in_company_currency = 0
        ctx = dict(context)
        if rate and amount and currency_id:
            for voucher in self.browse(cr, uid, ids, context=context):
                ctx.update({'todo_ttype': voucher.type})
                company_currency = self.pool.get('res.company').browse(cr, uid, company_id,
                                                                       context=context).currency_id
                if voucher.currency_rate_alter or voucher.currency_rate_alter > 0:
                    amount_in_company_currency = float_round(amount * voucher.currency_rate_alter, precision_rounding=company_currency.rounding)
                else:
                    # context should contain the date, the payment currency and the payment rate specified on the voucher
                    amount_in_company_currency = self.pool.get('res.currency').compute(cr, uid, currency_id,
                                                                                       company_currency.id, amount,
                                                                                       context=context)
            res['value']['paid_amount_in_company_currency'] = amount_in_company_currency
        return res

    def _paid_amount_in_company_currency(self, cr, uid, ids, name, args, context=None):
        if context is None:
            context = {}
        res = {}
        ctx = context.copy()
        for v in self.browse(cr, uid, ids, context=context):
            ctx.update({'date': v.date, 'todo_ttype': v.type})
            # make a new call to browse in order to have the right date in the context, to get the right currency rate
            voucher = self.browse(cr, uid, v.id, context=ctx)
            ctx.update({
                'voucher_special_currency': voucher.payment_rate_currency_id and voucher.payment_rate_currency_id.id or False,
                'voucher_special_currency_rate': voucher.currency_id.rate * voucher.payment_rate, })
            if v.currency_rate_alter or v.currency_rate_alter > 0:
                res[voucher.id] = float_round(voucher.amount * v.currency_rate_alter, precision_rounding=voucher.payment_rate_currency_id.rounding)
            else:
                res[voucher.id] = self.pool.get('res.currency').compute(cr, uid, voucher.currency_id.id,
                                                                        voucher.company_id.currency_id.id,
                                                                        voucher.amount, context=ctx)
        return res

    def recompute_voucher_lines(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date,
                                context=None):
        def _remove_noise_in_o2m():
            if line.reconcile_partial_id:
                if line.currency_id.id:
                    type_avl = line.credit and 'dr' or 'cr'
                    if ttype == u'receipt' and type_avl == 'cr' or ttype == u'payment' and type_avl == 'dr': #cr - dr
                        if line.amount_residual_currency <= 0 or ttype == u'receipt' and line.amount_currency <= 0:
                            return True
                    else: #dr - cr
                        if line.amount_residual_currency <= 0 or ttype == u'receipt' and line.amount_currency == 0:
                            return True
                else:
                    if line.amount_residual <= 0:
                        return True
            return False
        # TODO:BKP:BEGIN
        """
        Next the original function.
        """

        # def _remove_noise_in_o2m():
        #     if line.reconcile_partial_id:
        #         if currency_id == line.currency_id.id:
        #             if line.amount_residual_currency <= 0:
        #                 return True
        #         else:
        #             if line.amount_residual <= 0:
        #                 return True
        #     return False
        # TODO:BKP:END

        if context is None:
            context = {}
        context_multi_currency = context.copy()
        currency_pool = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        journal_pool = self.pool.get('account.journal')
        line_pool = self.pool.get('account.voucher.line')
        # set default values
        default = {
            'value': {'line_dr_ids': [], 'line_cr_ids': [], 'pre_line': False},
        }

        # drop existing lines
        line_ids = ids and line_pool.search(cr, uid, [('voucher_id', '=', ids[0])])
        for line in line_pool.browse(cr, uid, line_ids, context=context):
            if line.type == 'cr':
                default['value']['line_cr_ids'].append((2, line.id))
            else:
                default['value']['line_dr_ids'].append((2, line.id))

        if not partner_id or not journal_id:
            return default

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        currency_id = currency_id or journal.company_id.currency_id.id

        total_credit = 0.0
        total_debit = 0.0
        account_type = None
        if context.get('account_id'):
            account_type = self.pool['account.account'].browse(cr, uid, context['account_id'], context=context).type
        if ttype == 'payment':
            if not account_type:
                account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            if not account_type:
                account_type = 'receivable'

        if not context.get('move_line_ids', False):
            ids = move_line_pool.search(cr, uid, [('state', '=', 'valid'), ('account_id.type', '=', account_type),
                                                  ('reconcile_id', '=', False), ('partner_id', '=', partner_id)],
                                        context=context)
        else:
            ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_lines_found = []

        voucher_line_pool = self.pool.get('account.voucher.line')
        vlines = voucher_line_pool.browse(
            cr, uid, voucher_line_pool.search(
                cr, uid, [
                    ('move_line_id', 'in', ids),
                    ('amount_original', '=', 0)
                ]
            )
        )

        move_ids = [vl.move_line_id.id for vl in vlines]
        ids = list(set(ids) - set(move_ids))

        # order the lines by most old first
        ids.reverse()
        account_move_lines = move_line_pool.browse(cr, uid, ids, context=context)
        tax_amount = 0.00
        # compute the total debit/credit and look for a matching open amount or invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue

            if invoice_id:
                if line.invoice.id == invoice_id:
                    # if the invoice linked to the voucher line is equal to the invoice_id in context
                    # then we assign the amount on that line, whatever the other voucher lines
                    move_lines_found.append(line.id)
            elif currency_id == company_currency:
                # otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    # if the amount residual is equal the amount voucher, we assign it to that voucher
                    # line, whatever the other voucher lines
                    move_lines_found.append(line.id)
                    break
                    # otherwise we will split the voucher amount on each line (by most old first)
                total_credit += line.credit or 0.0
                total_debit += line.debit or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_lines_found.append(line.id)
                    break
                total_credit += line.credit and line.amount_currency or 0.0
                total_debit += line.debit and line.amount_currency or 0.0
        account_apply = """
            SELECT ap.code FROM account_account_apply AS ap
            INNER JOIN account_journal AS aj
            ON aj.account_account_apply_id = ap.id
            WHERE aj.id = {}""".format(journal_id)
        cr.execute(account_apply)
        apply_code = cr.fetchone()[0]

        remaining_amount = price
        # voucher line creation
        for line in account_move_lines:

            if _remove_noise_in_o2m():
                continue
            has_tax, tax_value = self.eval_tax_account(cr, uid, line, context)
            if has_tax:
                tax_amount = tax_value
            else:
                tax_amount = tax_value
            if line.currency_id.id and currency_id == line.currency_id.id:
                # validation document advance
                if apply_code == 'advance' and (len(line.move_id.advance_payment) > 0 or
                                                len(line.move_id.advance_payment_supplier) > 0):
                    amount_total_original = (line.amount_currency) + (
                        line.amount_currency * tax_amount)
                    amount_original = abs(amount_total_original)
                    amount_total_residual_currency = (
                        line.amount_residual_currency) + (
                        line.amount_residual_currency * tax_amount)
                    amount_unreconciled = abs(amount_total_residual_currency)
                else:
                    amount_original = abs(line.amount_currency)
                    amount_unreconciled = abs(line.amount_residual_currency)
            elif not line.currency_id.id and currency_id == company_currency:
                # validation document advance
                if apply_code == 'advance' and (len(line.move_id.advance_payment) > 0 or
                                                len(line.move_id.advance_payment_supplier) > 0):
                    amount_total_original = (
                        line.credit or line.debit) + (
                        (line.credit or line.debit) * tax_amount)
                    amount_original = abs(amount_total_original)
                    amount_residual = (
                        line.amount_residual) + (
                        line.amount_residual * tax_amount)
                    amount_unreconciled = abs(amount_residual)
                else:
                    amount_original = abs(line.credit or line.debit or 0.0)
                    amount_unreconciled = abs(line.amount_residual)
            else:
                # TODO:UPDATE:BEGIN
                payment_rate = context.get('voucher_special_currency_rate', False)
                context_multi_currency.update({'todo_ttype': ttype})
                # validate advance lines
                if apply_code == 'advance' and (len(line.move_id.advance_payment) > 0 or
                                                len(line.move_id.advance_payment_supplier) > 0):
                    if line.currency_id.id and currency_id == company_currency:
                        # get rate from policy advance
                        for row in line.move_id.line_id:
                            if row.amount_currency:
                                _rate = (
                                    abs(row.debit) or abs(
                                        row.credit)) / abs(row.amount_currency)
                                break
                        if payment_rate != 1 and payment_rate > 0:
                            # include the total of the advance
                            amount_total_original = (
                                line.amount_currency) + (
                                line.amount_currency * tax_amount)
                            amount_original = abs(
                                amount_total_original * payment_rate)
                            amount_total_unreconciled = (
                                line.amount_residual_currency) + (
                                line.amount_residual_currency * tax_amount)
                            amount_unreconciled = abs(
                                amount_total_unreconciled * payment_rate)
                        else:
                            amount_total_original = (
                                line.amount_currency) + (
                                line.amount_currency * tax_amount)
                            amount_original = abs(amount_total_original) * abs(
                                _rate)
                            amount_total_unreconciled = (
                                line.amount_residual_currency) + (
                                line.amount_residual_currency * tax_amount)
                            amount_unreconciled = abs(
                                amount_total_unreconciled) * abs(_rate)
                    elif not line.currency_id.id and currency_id != company_currency:
                        if payment_rate != 1 and payment_rate > 0:
                            amount_total_original = (
                                line.credit or line.debit) + (
                                (line.credit or line.debit) * tax_amount)
                            amount_original = (
                                abs(amount_total_original)) / payment_rate
                            amount_residual = (line.amount_residual) + (
                                line.amount_residual * tax_amount)
                            amount_unreconciled = abs(
                                amount_residual) / payment_rate
                        else:
                            amount_total_original = (
                                line.credit or line.debit) + (
                                (line.credit or line.debit) * tax_amount)
                            amount_original = currency_pool\
                                .compute(
                                    cr, uid,
                                    company_currency,
                                    currency_id, abs(amount_total_original),
                                    round=False,
                                    context=context_multi_currency)
                            amount_residual = (line.amount_residual) + (
                                line.amount_residual * tax_amount)
                            amount_unreconciled = currency_pool.compute(
                                cr, uid, company_currency,
                                currency_id, abs(amount_residual),
                                round=False, context=context_multi_currency)
                else:
                    # This is going to convert only non local currency
                    if line.currency_id.id and currency_id == company_currency:
                        if payment_rate != 1 and payment_rate > 0:
                            amount_original = abs(line.amount_currency * payment_rate)
                            amount_unreconciled = abs(line.amount_residual_currency * payment_rate)
                        else:
                            amount_original = currency_pool.compute(cr, uid, line.currency_id.id, currency_id, abs(line.amount_currency), round=False, context=context_multi_currency)
                            amount_unreconciled = currency_pool.compute(cr, uid, line.currency_id.id, currency_id, abs(line.amount_residual_currency), round=False, context=context_multi_currency)
                    elif not line.currency_id.id and currency_id != company_currency:
                        if payment_rate != 1 and payment_rate > 0:
                            amount_original = abs(line.credit or line.debit) / payment_rate
                            amount_unreconciled = abs(line.amount_residual) / payment_rate
                        else:
                            amount_original = currency_pool.compute(cr, uid, company_currency, currency_id, abs(line.credit or line.debit or 0.0), round=False, context=context_multi_currency)
                            amount_unreconciled = currency_pool.compute(cr, uid, company_currency, currency_id, abs(line.amount_residual), round=False, context=context_multi_currency)
                # TODO:UPDATE:END
            # Original fields of the document
            if not line.currency_id.id or line.currency_id.id == company_currency:
                if apply_code == 'advance' and (len(line.move_id.advance_payment) > 0 or
                                                len(line.move_id.advance_payment_supplier) > 0):
                    original_amount = (line.credit or line.debit) + (
                        (line.credit or line.debit) * tax_amount)
                    original_currency = company_currency
                    original_balance = line.amount_residual + (
                        line.amount_residual * tax_amount)
                else:
                    original_amount = line.credit or line.debit
                    original_currency = company_currency
                    original_balance = line.amount_residual
            else:
                if apply_code == 'advance' and (len(line.move_id.advance_payment) > 0 or
                                                len(line.move_id.advance_payment_supplier) > 0):
                    original_amount = line.amount_currency + (
                        line.amount_currency * tax_amount)
                    original_currency = line.currency_id.id
                    original_balance = line.amount_residual_currency + (
                        line.amount_residual_currency * tax_amount)
                else:
                    original_amount = line.amount_currency
                    original_currency = line.currency_id.id
                    original_balance = line.amount_residual_currency

            line_currency_id = line.currency_id and line.currency_id.id or\
                company_currency

            rs = {
                'name': line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id': line.id,
                'account_id': line.account_id.id,
                'amount_original': amount_original,
                'amount': (line.id in move_lines_found) and min(abs(remaining_amount), amount_unreconciled) or 0.0,
                'date_original': line.date,
                'date_due': line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
                'original_amount': abs(original_amount),
                'original_currency': abs(original_currency),
                'original_balance': abs(original_balance)
            }
            remaining_amount -= rs['amount']
            # in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            # on existing invoices: we split voucher amount by most old first, but only for lines in the same currency
            # if not move_lines_found:
            #     if currency_id == line_currency_id:
            #         if line.credit:
            #             amount = min(amount_unreconciled, abs(total_debit))
            #             rs['amount'] = amount
            #             total_debit -= amount
            #         else:
            #             amount = min(amount_unreconciled, abs(total_credit))
            #             rs['amount'] = amount
            #             total_credit -= amount
            # TODO:UPDATE
            # if rs['amount_unreconciled'] == rs['amount']:
            # 	rs['reconcile'] = True
            rs['amount'] = 0.0
            # TODO:END:UPDATE

            if rs['type'] == 'cr':
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(cr, uid,
                                                                                default['value']['line_dr_ids'],
                                                                                default['value']['line_cr_ids'], price,
                                                                                ttype)
        return default

    def eval_tax_account(self, cr, uid, account_move_line, context=None):
        res = False
        tax_value = 0.00
        if account_move_line:
            move_obj = self.pool.get('account.move')
            tax_obj = self.pool.get('account.tax')
            tax_ids = tax_obj.search(cr, uid, [], None)
            taxes = tax_obj.browse(cr, uid, tax_ids, None)
            move_id = move_obj.browse(cr, uid, account_move_line.move_id.id, context)
            for line in move_id.line_id:
                for tax in taxes:
                    if line.account_id.id == tax.account_paid_voucher_id.id\
                    or line.account_id.id == tax.account_collected_voucher_id.id:
                        res = True
                        tax_value = tax.amount
        return res, tax_value


    def voucher_move_line_create(self, cr, uid, voucher_id, line_total, move_id, company_currency, current_currency, context=None):
        #print('Sobreescrito 1 -voucher_move_line_create')
        #import pdb; pdb.set_trace()
        if context is None:
            context = {}
        _prec = 4
        move_line_obj = self.pool.get('account.move.line')
        currency_obj = self.pool.get('res.currency')
        company_currency_obj = currency_obj.browse(cr, uid, company_currency)
        tax_obj = self.pool.get('account.tax')
        tot_line = line_total
        rec_lst_ids = []
        date = self.read(cr, uid, [voucher_id], ['date'], context=context)[0]['date']
        ctx = context.copy()
        ctx.update({'date': date})
        voucher = self.pool.get('account.voucher').browse(cr, uid, voucher_id, context=ctx)
        # TODO:UPDATE:BEGIN
        """
        This update fix invoice creation specifically the amount conversion.
        works together with update in module 'alternate_type_of_change' in side
        'class ResCurrenc' definition.
        """
        ctx.update({'todo_ttype': voucher.type})
        # TODO:UPDATE:END
        voucher_currency = voucher.journal_id.currency or company_currency_obj
        # INICIO (SE MODIFICA PARA EL FUCINONAMIENTO CORRECTO DEL MODULO DNI (Proveedores = TC Compra, Clientes = TC Venta))
        # ctx.update({
        #  'voucher_special_currency_rate': voucher_currency.rate * voucher.payment_rate ,
        #  'voucher_special_currency': voucher.payment_rate_currency_id and voucher.payment_rate_currency_id.id or False,})
        if voucher.type == u'payment':
            rate = voucher_currency.rate
        if voucher.type == u'receipt':
            if hasattr(voucher_currency, 'rate_sale'):
                rate = voucher_currency.rate_sale
            else:
                rate = voucher_currency.rate
        vprc_id = voucher.payment_rate_currency_id
        ctx.update({
            'voucher_special_currency_rate': rate * voucher.payment_rate,
            'voucher_special_currency': vprc_id and vprc_id.id or False,
            'todo_ttype': voucher.type
        })
        # FINAL (SE AGREGA PARA EL FUCINONAMIENTO CORRECTO DEL MODULO DNI (Proveedores = TC Compra, Clientes = TC Venta))
        prec = self.pool.get('decimal.precision').precision_get(cr, uid, 'Account')
        for line in voucher.line_ids:
            #print('recorre detalles facturas - pago')
            # create one move line per voucher line where amount is not 0.0
            # AND (second part of the clause) only if the original move line was not having debit = credit = 0 (which is a legal value)
            if not line.amount and not (line.move_line_id and
                                        not float_compare(line.move_line_id.debit,
                                                          line.move_line_id.credit,
                                                          precision_digits=prec) and
                                        not float_compare(line.move_line_id.debit,
                                                          0.0,
                                                          precision_digits=prec)):
                continue
            # convert the amount set on the voucher line into the currency of the voucher's company
            # this calls res_curreny.compute() with the right context, so that it will take either the rate on the voucher if it is relevant or will use the default behaviour
            amount = self._convert_amount(cr, uid, line.untax_amount or line.amount, voucher.id, context=ctx)
            currency_rate_difference = 0.0
            currency_rate_difference_to_zero = False
            # if the amount encoded in voucher is equal to the amount unreconciled, we need to compute the
            # currency rate difference
            if line.amount == line.amount_unreconciled:
                if not line.move_line_id:
                    raise osv.except_osv(_('Wrong voucher line'),
                                         _("The invoice you are willing to pay is not valid anymore."))
                sign = line.type == 'dr' and -1 or 1
                # currency_rate_difference = sign * (line.move_line_id.amount_residual - amount)
            else:
                sign = line.type == 'dr' and -1 or 1
            if line.move_line_id.invoice.currency_rate_alter > 0:
                rate_current = voucher.payment_rate if voucher.payment_rate != 1 and voucher.payment_rate > 0 else 1 / self.get_currency_rate(cr, uid, line.move_line_id.currency_id.id, context=ctx)
                invoice_rate = line.move_line_id.invoice.currency_rate_alter
                if line.move_line_id.currency_id.id:
                    if voucher.currency_id.id != company_currency:
                        amount_on_invoice_creation = (line.untax_amount or line.amount) * invoice_rate
                    else:
                        amount_on_invoice_creation = ((line.untax_amount or line.amount) / rate_current) * invoice_rate
                else:
                    amount_on_invoice_creation = (line.untax_amount or line.amount)
            else:
                # The payment of the invoice must get the correct exchange 'rate'
                # if currency_rate_alter was not used. If there are other than invoice
                # type of documents one can add them in this validation.
                if line.move_line_id.currency_id.id:
                    invoice_rate = (line.move_line_id.debit or line.move_line_id.credit) /\
                        abs(line.move_line_id.amount_currency)
                    if voucher.currency_id.id != company_currency:
                        amount_on_invoice_creation = (line.untax_amount or line.amount) * invoice_rate
                    else:
                        rate_current = voucher.payment_rate if voucher.payment_rate != 1 and voucher.payment_rate > 0 else 1 / self.get_currency_rate(cr, uid, line.move_line_id.currency_id.id, context=ctx)
                        amount_on_invoice_creation = ((line.untax_amount or line.amount) / rate_current) * invoice_rate

                else:
                    amount_on_invoice_creation = (line.untax_amount or line.amount)

                if not line.move_line_id.currency_id.id or line.move_line_id.currency_id.id == company_currency:
                    currency_rate_difference_to_zero = True
            amount_on_invoice_creation = float_round(amount_on_invoice_creation, precision_rounding=line.move_line_id.currency_id.rounding or company_currency_obj.rounding)
            # Posible invertir la resta y verificar los resultados
            currency_rate_difference = float_round(sign * (amount_on_invoice_creation - amount), precision_rounding=line.move_line_id.currency_id.rounding)

            if currency_rate_difference_to_zero or \
                    line.currency_id.rounding >= currency_rate_difference >= line.currency_id.rounding * -1:
                currency_rate_difference = 0.0

            move_line = {
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'name': line.name or '/',
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                'currency_id': line.move_line_id and
                (company_currency <> line.move_line_id.currency_id.id and
                 line.move_line_id.currency_id.id) or False,
                'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                'quantity': 1,
                'credit': 0.0,
                'debit': 0.0,
                'date': voucher.date
            }
            if amount < 0:
                amount = -amount
                if line.type == 'dr':
                    line.type = 'cr'
                else:
                    line.type = 'dr'

            if (line.type == 'dr'):
                tot_line += amount
                move_line['debit'] = amount
            else:
                tot_line -= amount
                move_line['credit'] = amount

            if voucher.tax_id and voucher.type in ('sale', 'purchase'):
                move_line.update({
                    'account_tax_id': voucher.tax_id.id,
                })

            if move_line.get('account_tax_id', False):
                tax_data = tax_obj.browse(cr, uid, [move_line['account_tax_id']], context=context)[0]
                if not (tax_data.base_code_id and tax_data.tax_code_id):
                    raise osv.except_osv(_('No Account Base Code and Account Tax Code!'),
                                         _("You have to configure account base code and account \
                                           tax code on the '%s' tax!") % (tax_data.name))

            # compute the amount in foreign currency
            foreign_currency_diff = 0.0
            amount_currency = False
            if line.move_line_id:
                # We want to set it on the account move line as soon as the original line had a foreign currency
                if line.move_line_id.currency_id and line.move_line_id.currency_id.id != company_currency:
                    # we compute the amount in that foreign currency.
                    # USD-USD | MXN-MXN
                    if line.move_line_id.currency_id.id == current_currency:
                        # if the voucher and the voucher line share the same currency, there is no computation to do
                        sign = (move_line['debit'] - move_line['credit']) < 0 and -1 or 1
                        amount_currency = sign * (line.amount)
                    else:
                        sign = (move_line['debit'] - move_line['credit']) < 0 and -1 or 1
                        # TODO:UPDATE:BEGIN
                        # USD-MXN
                        if line.move_line_id.currency_id.id and current_currency == voucher.company_id.currency_id.id:
                            if voucher.payment_rate != 1 and voucher.payment_rate > 0:
                                amount_currency = float_round(sign * (line.amount / voucher.payment_rate), precision_rounding=line.move_line_id.currency_id.rounding or company_currency_obj.rounding)
                            else:
                                amount_currency = sign * currency_obj.compute(cr, uid, current_currency, line.move_line_id.currency_id.id, line.amount, context=ctx)
                        # MXN-USD
                        elif not line.move_line_id.currency_id.id and current_currency != voucher.company_id.currency_id.id:
                            if voucher.payment_rate != 1 and voucher.payment_rate > 0:
                                amount_currency = float_round(sign * (line.amount * voucher.payment_rate), precision_rounding=line.move_line_id.currency_id.rounding or company_currency_obj.rounding)
                            else:
                                amount_currency = sign * currency_obj.compute(cr, uid, current_currency, voucher.company_id.currency_id.id, (line.amount), context=ctx)
                        # TODO:UPDATE:END
                # TODO:UPDATE:BEGIN
                """
                In order to generate exchange rate gains or losses lines, the diff
                always has to be calculated, so it's moved out of the validation,
                insted a flag is raised.
                """
                reconcile_compleat = False
                foreign_currency_diff = line.move_line_id.amount_residual_currency - abs(amount_currency)
                if line.amount == line.amount_unreconciled:
                    # foreign_currency_diff = line.move_line_id.amount_residual_currency - abs(amount_currency)
                    reconcile_compleat = True

            move_line['amount_currency'] = amount_currency


            v_create_uid=uid
            v_write_uid=uid
            v_ref=self.pool('account.move').browse(cr, uid, move_id, context=context).ref

            if voucher.cost_center_id:
                v_cost_center_id=voucher.cost_center_id.id
            else:
                v_cost_center_id='''null'''

            if move_line['date']:
                v_date=move_line['date']
            else:
                v_date='''null'''

            v_company_id='''null'''
            if move_line['account_id']:
                v_account_id=move_line['account_id']
                v_company_obj=self.pool('account.account').browse(cr, uid, move_line['account_id'], context=context).company_id
                if v_company_obj:
                    v_company_id=v_company_obj.id
            else:
                v_account_id='''null'''

            if move_line['currency_id']:
                v_currency_id=move_line['currency_id']
            else:
                v_currency_id='''null'''

            if move_line['partner_id']:
                v_partner_id=move_line['partner_id']
            else:
                v_partner_id='''null'''

            if move_line['journal_id']:
                v_journal_id=move_line['journal_id']
            else:
                v_journal_id='''null'''

            if move_line['period_id']:
                v_period_id=move_line['period_id']
            else:
                v_period_id='''null'''

            if move_line['move_id']:
                v_move_id=move_line['move_id']
            else:
                v_move_id='''null'''

            if move_line['name']:
                v_name=move_line['name']
            else:
                v_name='''null'''

            v_quantity=move_line['quantity']
            v_credit=move_line['credit']
            v_debit=move_line['debit']

            if move_line['amount_currency']:
                v_amount_currency=move_line['amount_currency']
            else:
                v_amount_currency=0

            v_analytic_account_id='''null'''
            if "analytic_account_id" in move_line:
                if move_line['analytic_account_id']:
                    v_analytic_account_id=move_line['analytic_account_id']


            #import pdb; pdb.set_trace()
            insert_sql=(('''insert into account_move_line
                            (
                                create_uid,write_uid,create_date,write_date,date_created,date,account_id,company_id,centralisation,
                                state,blocked,not_move_diot,is_tax_voucher,partner_id,journal_id,period_id,move_id,name,
                                credit,debit,amount_currency,quantity,cost_center_id,currency_id,analytic_account_id,ref
                            )
                            values (
                                        %s,%s,now() at time zone 'UTC',now() at time zone 'UTC',CURRENT_DATE,'%s',%s,%s,'normal',
                                        'valid',False,False,False,%s,%s,%s,%s,'%s','%s','%s','%s','%s',%s,%s,%s,'%s'
                                    )
                            RETURNING id;
                        ''')% (
                                v_create_uid,v_write_uid,v_date,v_account_id,v_company_id,v_partner_id,v_journal_id,v_period_id,v_move_id,
                                v_name,v_credit,v_debit,v_amount_currency,v_quantity,v_cost_center_id,v_currency_id,v_analytic_account_id,v_ref
                        ))
            #print(insert_sql)
            cr.execute(insert_sql)
            insert_id=cr.fetchall()
            voucher_line=insert_id[0][0]
            #print(voucher_line)

            #voucher_line = move_line_obj.create(cr, uid, move_line, context=ctx)
            rec_ids = [voucher_line, line.move_line_id.id]
            if not currency_obj.is_zero(cr, uid, voucher.company_id.currency_id, currency_rate_difference):
                # Change difference entry in company currency
                exch_lines = self._get_exchange_lines(cr,
                                                      uid,
                                                      line,
                                                      move_id,
                                                      currency_rate_difference,
                                                      company_currency,
                                                      current_currency,
                                                      context=context)
                v_create_uid=uid
                v_write_uid=uid
                v_ref=self.pool('account.move').browse(cr, uid, move_id, context=context).ref

                if voucher.cost_center_id:
                    v_cost_center_id=voucher.cost_center_id.id
                else:
                    v_cost_center_id='''null'''

                if exch_lines[0]['date']:
                    v_date0="'"+exch_lines[0]['date']+"'"
                else:
                    v_date0='null'


                if exch_lines[0]['account_id']:
                    v_account_id0=exch_lines[0]['account_id']
                else:
                    v_account_id0='''null'''

                v_company_id0='''null'''
                if "company_id" in exch_lines[0]:
                    if exch_lines[0]['company_id']:
                        v_company_id0=exch_lines[0]['company_id']
                else:
                    v_company_obj0=self.pool('account.account').browse(cr, uid,exch_lines[0]['account_id'],context=context).company_id
                    if v_company_obj0:
                        v_company_id0=v_company_obj0.id

                if exch_lines[0]['currency_id']:
                    v_currency_id0=exch_lines[0]['currency_id']
                else:
                    v_currency_id0='''null'''

                if exch_lines[0]['partner_id']:
                    v_partner_id0=exch_lines[0]['partner_id']
                else:
                    v_partner_id0='''null'''

                if exch_lines[0]['journal_id']:
                    v_journal_id0=exch_lines[0]['journal_id']
                else:
                    v_journal_id0='''null'''

                if exch_lines[0]['period_id']:
                    v_period_id0=exch_lines[0]['period_id']
                else:
                    v_period_id0='''null'''

                if exch_lines[0]['move_id']:
                    v_move_id0=exch_lines[0]['move_id']
                else:
                    v_move_id0='''null'''

                if exch_lines[0]['name']:
                    v_name0=exch_lines[0]['name']
                else:
                    v_name0='''null'''

                v_quantity0=exch_lines[0]['quantity']
                v_credit0=exch_lines[0]['credit']
                v_debit0=exch_lines[0]['debit']

                if exch_lines[0]['amount_currency']:
                    v_amount_currency0=exch_lines[0]['amount_currency']
                else:
                    v_amount_currency0=0

                insert_sql0=(('''insert into account_move_line
                                (
                                    create_uid,write_uid,create_date,write_date,date_created,blocked,centralisation,
                                    state,not_move_diot,is_tax_voucher,currency_id,partner_id,journal_id,account_id,
                                    period_id,move_id,name,amount_currency,quantity,date,credit,debit,ref,cost_center_id,company_id

                                )
                                values (
                                        %s,%s,now() at time zone 'UTC',now() at time zone 'UTC',CURRENT_DATE,False,'normal',
                                        'valid',False,False,%s,%s,%s,%s,%s,%s,'%s','%s','%s',%s,'%s','%s','%s',%s,%s
                                        )
                                RETURNING id;
                            ''')% (
                                    v_create_uid,v_write_uid,v_currency_id0,v_partner_id0,v_journal_id0,v_account_id0,v_period_id0,v_move_id0,
                                    v_name0,v_amount_currency0,v_quantity0,v_date0,v_credit0,v_debit0,v_ref,v_cost_center_id,v_company_id0
                            ))
                #print(insert_sql0)
                cr.execute(insert_sql0)
                insert_id0=cr.fetchall()
                new_id=insert_id0[0][0]
                #print(new_id)


                if exch_lines[1]['date']:
                    v_date1="'"+exch_lines[1]['date']+"'"
                else:
                    v_date1='null'


                if exch_lines[1]['account_id']:
                    v_account_id1=exch_lines[1]['account_id']
                else:
                    v_account_id1='''null'''

                v_company_id1='''null'''
                if "company_id" in exch_lines[1]:
                    if exch_lines[1]['company_id']:
                        v_company_id1=exch_lines[1]['company_id']
                else:
                    v_company_obj1=self.pool('account.account').browse(cr, uid,exch_lines[1]['account_id'],context=context).company_id
                    if v_company_obj1:
                        v_company_id1=v_company_obj1.id

                if exch_lines[1]['currency_id']:
                    v_currency_id1=exch_lines[1]['currency_id']
                else:
                    v_currency_id1='''null'''

                if exch_lines[1]['partner_id']:
                    v_partner_id1=exch_lines[1]['partner_id']
                else:
                    v_partner_id1='''null'''

                if exch_lines[1]['journal_id']:
                    v_journal_id1=exch_lines[1]['journal_id']
                else:
                    v_journal_id1='''null'''

                if exch_lines[1]['period_id']:
                    v_period_id1=exch_lines[1]['period_id']
                else:
                    v_period_id1='''null'''

                if exch_lines[1]['move_id']:
                    v_move_id1=exch_lines[1]['move_id']
                else:
                    v_move_id1='''null'''

                if exch_lines[1]['name']:
                    v_name1=exch_lines[1]['name']
                else:
                    v_name1='''null'''

                v_quantity1=exch_lines[1]['quantity']
                v_credit1=exch_lines[1]['credit']
                v_debit1=exch_lines[1]['debit']

                if exch_lines[1]['amount_currency']:
                    v_amount_currency1=exch_lines[1]['amount_currency']
                else:
                    v_amount_currency1=0

                insert_sql1=(('''insert into account_move_line
                                (
                                    create_uid,write_uid,create_date,write_date,date_created,blocked,centralisation,
                                    state,not_move_diot,is_tax_voucher,currency_id,partner_id,journal_id,account_id,
                                    period_id,move_id,name,amount_currency,quantity,date,credit,debit,ref,cost_center_id,company_id

                                )
                                values (
                                        %s,%s,now() at time zone 'UTC',now() at time zone 'UTC',CURRENT_DATE,False,'normal',
                                        'valid',False,False,%s,%s,%s,%s,%s,%s,'%s','%s','%s',%s,'%s','%s','%s',%s,%s
                                        )
                                RETURNING id;
                            ''')% (
                                    v_create_uid,v_write_uid,v_currency_id1,v_partner_id1,v_journal_id1,v_account_id1,v_period_id1,v_move_id1,
                                    v_name1,v_amount_currency1,v_quantity1,v_date1,v_credit1,v_debit1,v_ref,v_cost_center_id,v_company_id1
                            ))
                #print(insert_sql1)
                cr.execute(insert_sql1)
                insert_id1=cr.fetchall()
                new_id1=insert_id1[0][0]



                """
                new_id = move_line_obj.create(cr, uid, exch_lines[0], context)
                move_line_obj.create(cr, uid, exch_lines[1], context)
                """
                rec_ids.append(new_id)
            # if line.move_line_id and line.move_line_id.currency_id and \
            #         not currency_obj.is_zero(cr,
            #                                  uid,
            #                                  line.move_line_id.currency_id,
            #                                  foreign_currency_diff) and reconcile_compleat:
            #     # TODO:UPDATE:END
            #     # Change difference entry in voucher currency
            #     move_line_foreign_currency = {
            #         'journal_id': line.voucher_id.journal_id.id,
            #         'period_id': line.voucher_id.period_id.id,
            #         'name': _('change') + ': ' + (line.name or '/'),
            #         'account_id': line.account_id.id,
            #         'move_id': move_id,
            #         'partner_id': line.voucher_id.partner_id.id,
            #         'currency_id': line.move_line_id.currency_id.id,
            #         'amount_currency': -1 * foreign_currency_diff,
            #         'quantity': 1,
            #         'credit': 0.0,
            #         'debit': 0.0,
            #         'date': line.voucher_id.date,
            #     }
            #     new_id = move_line_obj.create(cr, uid, move_line_foreign_currency, context=context)
            #     rec_ids.append(new_id)
            if line.move_line_id.id:
                rec_lst_ids.append(rec_ids)
        return (round(tot_line, _prec), rec_lst_ids)

    # def recompute_payment_rate(self, cr, uid, ids, vals, currency_id, date, ttype, journal_id, amount, context=None):
    #     journal_obj = self.pool.get('account.journal')
    #     journal = journal_obj.browse(cr, uid, [journal_id], context={})
    #
    #     res = super(AccountVoucher, self).recompute_payment_rate(cr, uid, ids, vals, currency_id, date, ttype, journal_id, amount, context=context)
    #     if (journal.currency.id or journal.company_id.currency_id.id) == journal.company_id.currency_id.id:
    #         res['value']['payment_rate'] = 1 / res['value']['payment_rate']
    #     return res

    def onchange_partner_id(self, cr, uid, ids, partner_id, journal_id, amount, currency_id, ttype, date, context=None):
        """
        :method overwrite:
        """

        res = super(AccountVoucher, self).onchange_partner_id(cr, uid, ids, partner_id,
                                                              journal_id, amount,
                                                              currency_id, ttype, date,
                                                              context=context)
        return self.show_documentos_by_type_application(cr, res, journal_id, ttype)

    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id,
                         context=None):
        """
        :method overwrite:
        """
        res = super(AccountVoucher, self).onchange_journal(cr, uid, ids, journal_id,
                                                           line_ids, tax_id, partner_id,
                                                           date, amount, ttype,
                                                           company_id, context=context)
        # Every time when the journal change, amounts are reset
        if isinstance(res, dict) and res.get('value', False):
            res['value']['net_amount'] = 0
            res['value']['amount_readonly'] = 0

        return self.show_documentos_by_type_application(cr, res, journal_id, ttype)

    def onchange_amount(self, cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date,
                        payment_rate_currency_id, company_id, context=None):
        res = super(AccountVoucher, self).onchange_amount(cr, uid, ids, amount, rate, partner_id, journal_id,
                                                          currency_id, ttype, date, payment_rate_currency_id,
                                                          company_id, context=context)
        return self.show_documentos_by_type_application(cr, res, journal_id, ttype)

    def show_documentos_by_type_application(self, cr, res, journal_id, ttype):
        if isinstance(res, dict) and 'value' in res:
            account_account_apply = 'line_cr_ids'
            if ttype == 'receipt':  # Customer
                account_account_apply = 'line_dr_ids'
            # journal_id must be selected in order to exec the query
            if account_account_apply in res['value'] and len(res['value'][account_account_apply]) > 0 and journal_id:
                qs_account_journal_apply = """
                  SELECT COALESCE(account_account_apply_id, 0) id  from account_journal where id ={};
                """.format(journal_id)
                cr.execute(qs_account_journal_apply)
                if cr.rowcount > 0:
                    _account_apply_id = cr.fetchone()[0]
                    if _account_apply_id > 0:
                        account_ids = [line_cr_or_dr['account_id']\
                                       for line_cr_or_dr in res['value'][account_account_apply]\
                                       if isinstance(line_cr_or_dr, dict)]
                        qs_account_account_apply = """
                            SELECT id from account_account where id in ({}) and account_account_apply_id = {}
                        """.format(str(account_ids)[1:-1], _account_apply_id)
                        cr.execute(qs_account_account_apply)
                        if cr.rowcount > 0:
                            account_apply_ids = [line[0] for line in cr.fetchall()]
                            cr_dr_tmp = []
                            for line_cr_or_dr in res['value'][account_account_apply]:
                                if isinstance(line_cr_or_dr, dict):
                                    if line_cr_or_dr['account_id'] in account_apply_ids:
                                        cr_dr_tmp.append(line_cr_or_dr)
                                else:
                                    cr_dr_tmp.append(line_cr_or_dr)
                            res['value'][account_account_apply] = cr_dr_tmp
                        else:
                            res['value'][account_account_apply] = []
                    else:
                        res['value'][account_account_apply] = []
                else:
                    res['value'][account_account_apply] = []
        return res
