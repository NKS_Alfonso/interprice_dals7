��    #      4  /   L           	  5        R     [     u     �     �     �     �     �     �  *   �               -     ?     K     [  $   j     �  -   �  a   �  a   .  T   �  8   �  z        �     �     �  	   �     �     �  x   �  �   _  E  �     +	  5   B	     x	     �	     �	     �	     �	     �	     �	     �	     
  *   
     B
  	   S
     ]
     d
     p
     �
  $   �
  
   �
  -   �
  a   �
  a   P  T   �  8     z   @     �     �     �  	   �     �     �  x     �   �     !                
                                            "                                  #                                            	              Accounting Voucher Amount Paid With Paylines: checks, withholdings, etc. Currency Entry "%s" is not valid ! Entry is already reconciled. Error Error! Hide currency rate alter field Invoice Invoice Line Journal Items No Account Base Code and Account Tax Code! Original Amount Original Balance Original Currency Pay Invoice Paylines Amount Purchase Order Some entries are already reconciled. TC Alternative The account is not defined to be reconciled ! The exchange rate for customer should be bigger than zero.
 Please review currency configuration. The exchange rate for supplier should be bigger than zero.
 Please review currency configuration. The exchange rate should be bigger than zero.
 Please review currency configuration. The invoice you are willing to pay is not valid anymore. To reconcile the entries company should                                                       be the same for all entries. True Voucher Lines Warning! Write-Off Wrong exchange rate Wrong voucher line You have to configure account base code and account                                            tax code on the '%s' tax! You have to provide an account for the                                                           write off/exchange difference entry. Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-09 18:01+0000
PO-Revision-Date: 2017-08-09 13:55-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
 Comprobantes contables Amount Paid With Paylines: checks, withholdings, etc. Dinero corriente Entry "%s" is not valid ! Entry is already reconciled. Error Error! Hide currency rate alter field Factura Línea de factura Elementos diario No Account Base Code and Account Tax Code! Importe original Saldo doc Moneda Pay Invoice Paylines Amount Orden de Compra Some entries are already reconciled. TC Alterno The account is not defined to be reconciled ! The exchange rate for customer should be bigger than zero.
 Please review currency configuration. The exchange rate for supplier should be bigger than zero.
 Please review currency configuration. The exchange rate should be bigger than zero.
 Please review currency configuration. The invoice you are willing to pay is not valid anymore. To reconcile the entries company should                                                       be the same for all entries. True Líneas de comprobante Warning! Write-Off Wrong exchange rate Wrong voucher line You have to configure account base code and account                                            tax code on the '%s' tax! You have to provide an account for the                                                           write off/exchange difference entry. 