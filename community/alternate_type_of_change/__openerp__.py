# -*- coding: utf-8 -*-
{
    'name': "Alternate type of change",

    'summary': """
        """,

    'description': """

    """,

    'author': "tantums",
    'website': "http://www.tantums.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '8.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'purchase',
        'sale',
        'account',
        'account_voucher_tax',
        'account_invoice_tax',
        'account_check',
        'account_voucher_payline',
        'configuracion',
    ],
    # always loaded
    'data': [
        'view/purchase_view.xml',
        'view/account_view.xml',
        # 'view/account_voucher_view.xml',
        # 'security/ir.model.access.csv',
        # 'templates.xml',
        'view/invoice_view.xml',
        'view/payments_decimal_prec.xml',
        'view/payments_view.xml',
    ],
    # Custom CSS styles
    'css': [
        'static/src/css/styles.css'
    ],

    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
}
