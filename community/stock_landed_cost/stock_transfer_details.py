# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.odoo.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64, StringIO, csv
from openerp.osv import orm, fields
from openerp.osv import osv, fields
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime
import pdb
import logging
_logger = logging.getLogger(__name__)
import time
import warnings
from openerp import api
header_fields = ['SKU', 'Serie']


class stock_transfer_details(osv.osv_memory):
	_inherit = 'stock.transfer_details'
	### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
	###                                     decoradores                                                                                              ###
	### ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  ###
	
	###########history.stock###########################################################
	def history_purchase(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de albaranes de entrada(compras)"""
		#product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.picking_id.origin
		warehouse_id = data.picking_id.picking_type_id.warehouse_id.id
		location_id = data.picking_id.picking_type_id.default_location_dest_id.id
		picking_origin = data.picking_id.name
		picking_id = data.picking_id.id
		for this in data.item_ids:
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,location_id,))
			for (qty, sm) in cr.fetchall():
				if qty==0: product_qty = 0
				elif(qty>0): product_qty = sm
				write_vals = {
							  'product_id':this.product_id.id,
							  'warehouse_id':warehouse_id,
							  'location_id':location_id,
							  'product_qty_move':this.quantity,
							  'existencia_anterior': product_qty,
							  'existencia_posterior': this.quantity+product_qty,
							  'document_origin': document_origen,
							  'picking_origin':picking_origin,
							  'picking_id':picking_id,
							  }
				obj_history_stock.create(cr, uid, write_vals, context=context)
	
	def history_sale(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de albaranes de salida(ventas)"""
		#product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.picking_id.origin
		warehouse_id = data.picking_id.picking_type_id.warehouse_id.id
		location_id = data.picking_id.picking_type_id.default_location_src_id.id
		picking_origin = data.picking_id.name
		picking_id = data.picking_id.id
		for this in data.item_ids:
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,location_id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								  'product_id':this.product_id.id,
								  'warehouse_id':warehouse_id,
								  'location_id':location_id,
								  'product_qty_move':-(this.quantity),
								  'existencia_anterior': product_qty,
								  'existencia_posterior':product_qty-this.quantity,
								  'document_origin': document_origen,
								  'picking_origin':picking_origin,
								  'picking_id':picking_id,
								  }
					obj_history_stock.create(cr, uid, write_vals, context=context)
					
	def history_transfer(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de albaranes de transferencias(internas)"""
		#product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.picking_id.origin
		picking_origin = data.picking_id.name
		picking_id = data.picking_id.id
		for this in data.item_ids:
			#historial origen (salida)
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,this.sourceloc_id.id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								  'product_id':this.product_id.id,
								  'warehouse_id':self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id', '=', this.sourceloc_id.id)])[0],
								  'location_id':this.sourceloc_id.id,
								  'product_qty_move':-(this.quantity),
								  'existencia_anterior': product_qty,
								  'existencia_posterior':product_qty-this.quantity,
								  'document_origin': document_origen,
								  'picking_origin':picking_origin,
								  'picking_id':picking_id,
								  }
					obj_history_stock.create(cr, uid, write_vals, context=context)
			#historial destino (entrada)
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,this.destinationloc_id.id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								  'product_id':this.product_id.id,
								  'warehouse_id':self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id', '=', this.destinationloc_id.id)])[0],
								  'location_id':this.destinationloc_id.id,
								  'product_qty_move':this.quantity,
								  'existencia_anterior': product_qty,
								  'existencia_posterior':product_qty+this.quantity,
								  'document_origin': document_origen,
								  'picking_origin':picking_origin,
								  'picking_id':picking_id,
								  }
					obj_history_stock.create(cr, uid, write_vals, context=context)
	###################################################################################
	@api.multi
	def out_lotes(self):
		for lstits in [self.item_ids]:
			for det in lstits:
				while det.quantity>1:
					det.quantity = (det.quantity-1)
					new_id = det.copy(context=self.env.context)
					new_id.quantity = 1
					new_id.packop_id = False
			return self[0].wizard_view()
			return True
	
		
	def _get_unit_cost(self, cr, uid, ids, name, args, context):
		stock_transfer_details = self.pool.get('stock.transfer_details_items')
		if not ids : return {}
		result = {}
		cost_unit = 0.0
		for transfer in self.browse(cr, uid, ids):
			# if transfer.picking_id.computation_cost == 'per_unit':
				for moves in transfer.picking_id.move_lines:	
					for items in transfer.item_ids:
						stock_transfer_details.write(cr,uid,items,{'unit_cost':moves.price_init})
				cost_unit = 0.01
				result[line.id] = cost_unit
		return result
	
	
	def _get_quantity_total(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		quantity_total = 0.0
		for line in self.browse(cr, uid, ids):
			for i in line.item_ids:
				quantity_total += i.quantity
			result[line.id] = quantity_total
		return result

	def _amount_total(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		amount_total = 0.0
		for line in self.browse(cr, uid, ids):
			if line.item_ids:
				for i in line.item_ids:
					amount_total += i.sub_total
			result[line.id] = amount_total
		return result

	def _landed_cost(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_cost = 0.0
		for line in self.browse(cr, uid, ids):
			if line.picking_id:
				landed_cost = line.picking_id.cost_amount
			result[line.id] = landed_cost
		return result
	
	def _lote_automatico(self, cr, uid, ids, name, args, context):
			"""Genera el lote en automatico con los datos del pedimento"""
			if not ids : return {}
			result = {}
			lote = ""
			for this in self.browse(cr,uid,ids):
				lote = this.picking_id.lot
			result[this.id] = lote
			return result

	def _rfc_proveedor(self, cr, uid, ids, name, args, context):
		"""Verifica si el proveedor tiene un rfc o no"""
		if not ids : return {}
		result = {}
		rfc = False
		for this in self.browse(cr,uid,ids):
				#Para compras nacioales verifica si el partner tiene rfc
				if (this.picking_id.origen_nacional_internacional == False):
					if (this.picking_id.partner_id.vat == '' or this.picking_id.partner_id.vat == False):
						rfc = True
					else:
						rfc = False
		result[this.id] = rfc
		return result
	def _compra_o_venta(self, cr, uid, ids, name, args, context):
		"""Verifica si el albaran proviene de una venta o de una compra"""
		if not ids : return {}
		result = {}
		compra_venta= False
		try:
			for this in self.browse(cr,uid,ids):
					if  this.picking_id.compra_o_venta == True:
						compra_venta= True
				
			result[this.id] = compra_venta
			return result
		except:
			 return {}
	_columns = {
		'quantity_total': fields.function(_get_quantity_total,digits_compute=dp.get_precision('Account'), string='Quantity Total'),
		'total_amount' : fields.function(_amount_total, digits_compute=dp.get_precision('Account'), string='Total Amount'),
		'landed_cost' : fields.function(_landed_cost, digits_compute=dp.get_precision('Account'), string='Landed Cost Product'),
		'lote' : fields.function(_lote_automatico,type="char"),
		'rfc_proveedor' : fields.function(_rfc_proveedor,type="boolean", string="RFC"),
		'compra_o_venta': fields.function(_compra_o_venta, type="boolean"),
		#quitar
		'aml_data': fields.binary('File', required=False),
		
		
	}
	
	_defaults = {
	
		
	}

	def update(self, cr, uid, ids, context=None):
		return self.wizard_view(cr,uid,ids,context=context)


stock_transfer_details()

class stock_transfer_details_items(osv.osv_memory):
	_inherit = 'stock.transfer_details_items'
	#subir al servidor
	def _compra_o_venta(self, cr, uid, ids, name, args, context):
		"""Verifica si el albaran proviene de una venta o de una compra"""
		try:
			if not ids : return {}
			result = {}
			compra_venta= False
			
			for this in self.browse(cr,uid,ids):
					if  this.transfer_id.picking_id.compra_o_venta == True:
						compra_venta= True
				
			result[this.id] = compra_venta
			return result
		except:
			 return {}
			
	def _get_product_price(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		for line in self.browse(cr, uid, ids):
			for m in line.transfer_id.picking_id.move_lines:
				if m.product_id == line.product_id:
					result[line.id] = m.price_unit
		return result

	def _get_subtotal(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		for line in self.browse(cr,uid,ids):
			for m in line.transfer_id.picking_id.move_lines:
				if m.product_id == line.product_id:
					result[line.id] = line.quantity * m.price_unit
		return result

	def _get_landing_cost(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		for line in self.browse(cr, uid, ids):
			if line.transfer_id.picking_id.computation_cost == 'per_unit':
				# landed_costs = (line.transfer_id.picking_id.cost_amount / line.transfer_id.quantity_total) * line.quantity
				for l in line.transfer_id.picking_id.move_lines:
					if line.product_id == l.product_id:
						#landed_costs = line.unit_cost + l.landing_cost
						landed_costs = line.unit_cost * line.quantity
			elif line.transfer_id.picking_id.computation_cost == 'by_percent':
				landed_costs = ((line.product_price / line.transfer_id.picking_id.total_amount) * line.transfer_id.picking_id.cost_amount) * line.quantity
			else:
				landed_costs = line.transfer_id.picking_id.cost_amount / line.transfer_id.quantity_total
			
			result[line.id] = landed_costs
		return result

	def get_quantity_by_line(self, cr, uid, ids, product_id, context=None):
		qty_by_product = 0.0
		for line in self.browse(cr, uid, ids):
			trans = self.pool.get('stock.transfer_details').browse(cr,uid,line.transfer_id.id)
			for t in trans:
				for i in t.item_ids:
					if product_id == i.product_id.id:
						qty_by_product = i.quantity
		return qty_by_product

	def _get_unit_cost(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		cost_unit = 0.0
		#items
		for line in self.browse(cr, uid, ids):
			if line.transfer_id.picking_id.computation_cost == 'per_unit':
				#move_lines que se encuentran el albaran
				for l in line.transfer_id.picking_id.move_lines:
					if line.product_id == l.product_id:
						cost_unit = l.price_unit
					
					
			elif line.transfer_id.picking_id.computation_cost == 'by_percent':
				cost_unit = (line.landing_cost / line.quantity) #self.get_quantity_by_line(cr,uid,ids,line.product_id.id,context=context))
			else:
				cost_unit = line.transfer_id.picking_id.cost_amount / line.transfer_id.quantity_total
			
			result[line.id] = cost_unit
		return result
	
	def onchange_lote_quants(self, cr, uid, ids, lot_id,product_id):
		"""Funcion que retorna un warning y limpia el campo de lote si este ya no tiene existencias"""
		if not lot_id or not product_id:
			return {}
		cr.execute("""SELECT id FROM stock_quant WHERE qty > 1 and (lot_id=%s and product_id=%s)""",(lot_id,product_id,))
		if cr.rowcount:
			return {'value': {},}

		else:
			objec_t = self.pool.get('stock.production.lot').browse(cr, uid, lot_id)
			return {'value': {'lot_id':0,},
				'warning': {'title': 'Alerta!', 'message': "El lote %s Ya no tiene existencias"%(objec_t.name)}
				}
	
	_columns = {
		'product_price':fields.function(_get_product_price, digits_compute=dp.get_precision('Account'), string="Price Product"),
		'sub_total': fields.function(_get_subtotal, digits_compute=dp.get_precision('Account'), string="Subtotal"),
		'landing_cost': fields.function(_get_landing_cost, digits_compute=dp.get_precision('Account'), string='Landing Cost'),
		'unit_cost' : fields.function(_get_unit_cost,digits_compute=dp.get_precision('Account'), string='Unit Cost'),
		'compra_o_venta': fields.function(_compra_o_venta, type="boolean"),
		#--borrrar
		'data': fields.binary('File', readonly=True),
		
		
	}
		
		
	_defaults = {
		'csv_separator': ',',
	}
	
stock_transfer_details_items()