# -*- coding: utf-8 -*-
from openerp.osv import osv,fields

class purchase_order(osv.osv):
	_inherit = 'purchase.order'

	_columns = {
		'applied_cost': fields.boolean(string="Costo Aplicado"),
	}

	_defaults = {
		'applied_cost': False,
	}

