��    *      l  ;   �      �     �     �     �     �     �     �     �       
     
     P   '     x     {     �     �     �     �  U   �     D     Q     _     o          �     �     �     �     �     �     �  	   �     �        
          $        A     N     b     q     �  V  �                     2     8     I     Z     i  
   u  
   �  V   �     �     �     �     �     	     +	  ;   ?	     {	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	  "   
     )
  	   9
     C
     R
     [
     t
  )   }
     �
     �
     �
     �
     �
           $                 (         *              	   "                  #      '                          !                                       
          %                            )             &    (Update) Absolute Value Additional Info Amount Amount Type Calculo de Costeo Cantidad total Costo total Created by Created on Defines if the amount is to be calculated for each quantity or an absolute value ID Landed Cost Name Landed Cost Product Landed Costs Base Quantity Landed Costs Base Value Landed Costs Total Untaxed Landed cost for stock valuation. It will be added to the price of the supplier price. Landing Cost Landing Costs Last Updated by Last Updated on Monto total Partner Per Quantity Picking Picking List Picking wizard Picking wizard items Price Product Proveedor Quantity Total Release Stock Move Subtotal The supplier of this cost component. Total Amount Total Product Price Total Quantity Total costo por cantidad Total costo por valor absoluto Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-26 20:18+0000
PO-Revision-Date: 2015-06-23 13:12-0600
Last-Translator: Cesar equihua <cequihua@tantums.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_MX
X-Generator: Poedit 1.5.4
 (Actualizar) Valor Completo Informacion adicional Monto Metodo de Costeo Metodo de Costeo Cantidad total Costo total Created by Created on Define si el monto es calculado entre la cantidad total de piezas o por el valor total ID Nombre Costo del Producto Costo por cantidad Costo por el valor total Costo total sin IVA Costo revaluado, este sera agregado al precio del proveedor Costo Adicional Costos Adicionales Last Updated by Last Updated on Monto total Proveedor Por Cantidad Albaran Albaran Asistente de albaran Elementos del asistente de albaran Precio Producto Proveedor Cantidad Total Liberado Movimiento de inventario Subtotal El proveedor de este costo complementado. Monto Total Total Precio Producto Cantidad Total Total costo por cantidad Total costo por valor absoluto 