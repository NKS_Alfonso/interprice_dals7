# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#    Copyright (C) 2010-2012 ChriCar Beteiligungs- und Beratungs- GmbH (<http://www.camptocamp.at>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv,fields
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)
import pdb
#----------------------------------------------------------
# Stock Picking
#----------------------------------------------------------
class stock_picking(osv.osv):
	_inherit = "stock.picking"

	def get_currency_company(self,cr,uid,ids,context=None):
		currency_id = 0
		res_company = self.pool.get('res.company')
		res_user = self.pool.get('res.users')
		for u in res_user.browse(cr,uid,uid):
			for c in res_company.browse(cr,uid,u.company_id.id):
				currency_id = c.currency_id.id

		return currency_id

	def _landed_cost_base_value(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs_base_value = 0.0
		currency_company_id = 0
		currency_obj = self.pool.get('res.currency')
		currency_rate = 0.0
		
		for line in self.browse(cr, uid, ids):
			if line.aditionals_costs_ids:
				for costs in line.aditionals_costs_ids:
					if line.computation_cost == 'by_percent':
						currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
						if costs.currency_id.id != currency_company_id:
							if costs.currency_rate_alter or costs.currency_rate_alter > 0:
								landed_costs_base_value += costs.amount_total * costs.currency_rate_alter
							else:
								landed_costs_base_value += currency_obj.compute(cr, uid, costs.currency_id.id, currency_company_id, costs.amount_total, context=context)
						else:
							landed_costs_base_value += costs.amount_total
			result[line.id] = landed_costs_base_value
		return result

	def _landed_cost_base_quantity(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs_base_quantity = 0.0
		currency_company_id = 0
		currency_obj = self.pool.get('res.currency')
		currency_rate = 0.0

		for line in self.browse(cr, uid, ids):
			if line.aditionals_costs_ids:
				for costs in line.aditionals_costs_ids:
					if line.computation_cost == 'per_unit':
						currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
						if costs.currency_id.id != currency_company_id:
							if costs.currency_rate_alter or costs.currency_rate_alter > 0:
								landed_costs_base_quantity += costs.amount_total * costs.currency_rate_alter
							else:
								landed_costs_base_quantity += currency_obj.compute(cr, uid, costs.currency_id.id, currency_company_id, costs.amount_total, context=context)
						else:
							landed_costs_base_quantity += costs.amount_total
			result[line.id] = landed_costs_base_quantity
		return result

	def _quantity_total(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		quantity_total = 0.0
		for line in self.browse(cr, uid, ids):
			if line.move_lines:
				for ml in line.move_lines:
					if ml.product_qty > 0.0:
						quantity_total += ml.product_qty
			result[line.id] = quantity_total
		return result

	def _amount_total(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		amount_total = 0.0
		for line in self.browse(cr, uid, ids):
			if line.move_lines:
				for ml in line.move_lines:
					if ml.product_qty > 0.0 and ml.price_unit: #ml.product_id.standard_price:
						#amount_total += (ml.product_qty * ml.product_id.standard_price)
						amount_total += (ml.product_qty * ml.price_unit)
			result[line.id] = amount_total
		return result

	def _landed_cost(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		# landed costss for the line
		for line in self.browse(cr, uid, ids):
			if line.aditionals_costs_ids:
				landed_costs += line.cost_amount #line.landed_cost_base_value + line.landed_cost_base_quantity
			result[line.id] = landed_costs
		return result
	
	#
	def _suma_total(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		suma = 0.0
		try:
			for line in self.browse(cr, uid, ids):
					#move_lines que se encuentran el albaran
					for l in line.move_lines:
						suma+=l.sub_total
				
					result[line.id] = suma
			return result
		except:
			return {}

	_columns = {
		'landed_cost_base_value' : fields.function(_landed_cost_base_value, digits_compute=dp.get_precision('Account'), string='Landed Costs Base Value'),
		'landed_cost_base_quantity' : fields.function(_landed_cost_base_quantity, digits_compute=dp.get_precision('Account'), string='Landed Costs Base Quantity'),
		'landed_cost' : fields.function(_landed_cost, digits_compute=dp.get_precision('Account'), string='Landed Costs Total Untaxed'),
		'total_amount' : fields.function(_amount_total, digits_compute=dp.get_precision('Account'), string='Total Product Price'),
		'quantity_total' : fields.function(_quantity_total, digits_compute=dp.get_precision('Product UoM'), string='Total Quantity'),
		'is_release': fields.boolean(string="Release"),
		#'aditional_cost_ids': fields.one2many('purchase.order','aditional_cost_id', 'Aditional Costs'),
		'aditionals_costs_ids': fields.many2many('purchase.order','purchase_picking_rel','order_id','picking_id','Aditional Costs'),
		'computation_cost': fields.selection([('per_unit','Por Cantidad'), ('by_percent','By Percent'),('equal','Equal')], 'Method Cost'),
		'cost_amount': fields.float(string="Amount", digits_compute=dp.get_precision('Account Cost')),
		'balance': fields.float(string="Saldo",digits_compute=dp.get_precision('Account')),
		'suma_total' : fields.function(_suma_total, digits_compute=dp.get_precision('Product UoM'), string='Monto Total(MXN)'),
		'cost_order_id': fields.many2one('added.cost.order','Albaran'),
	}

	_defaults = {
		'is_release': False,
	}

	def do_enter_transfer_details(self, cr, uid, picking, context=None):
		try:
			for pick in self.browse(cr,uid,picking):
				if pick.picking_type_id.code == 'incoming' and pick.origen_nacional_internacional == True:
					if pick.is_release == True:
						return super(stock_picking,self).do_enter_transfer_details(cr,uid,picking,context=context)
					else:
						return {
							'type': 'ir.actions.act_window.message',
							'title':'Informacion',
							'message': 'El albaran no ha sido liberado por el departamento de compras',
						}
						#raise osv.except_osv(_('Error :'), _("El albaran no ha sido liberado por el departamento de compras"))
				else:
					return super(stock_picking,self).do_enter_transfer_details(cr,uid,picking,context=context)
		except Exception, e:
			print str(e)

stock_picking()

class stock_move(osv.osv):
	_inherit = "stock.move"
	#subir al servidor se modifico esta funcion
	def _get_product_price(self, cr, uid, ids, name, args, context):
		try:
			if not ids : return {}
			result = {}
			price = 0.0
			for line in self.browse(cr, uid, ids):
				if line.picking_id.compra_o_venta == False :
					price = line.price_unit #line.product_id.standard_price
					result[line.id] = price
				elif(line.picking_id.compra_o_venta == True):
					result[line.id] = price
			return result
		except:return {}
	#subir al servidor se modifico esta funcion
	def _get_subtotal(self, cr, uid, ids, name, args, context):
		try:
			if not ids : return {}
			result = {}
			subtotal = 0.0
			for line in self.browse(cr,uid,ids):
				if line.picking_id.compra_o_venta == False :
					subtotal = (line.product_qty * line.price_unit) #line.product_id.standard_price
					result[line.id] = subtotal
				elif(line.picking_id.compra_o_venta == True):
					result[line.id] = subtotal
			return result
		except:return {}
	def _get_landing_cost(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		for line in self.browse(cr, uid, ids):
			if line.picking_id.computation_cost == 'per_unit':
				#-----------------------------calculando el costo adicional que le corresponde  a cada producto-#
				landed_costs = (line.picking_id.cost_amount / line.picking_id.quantity_total) * line.product_qty
				

				#-----------------------------------<<<<<<<<<>>>>>>>>>>>>>>-------------------------------------#
			elif line.picking_id.computation_cost == 'by_percent':
				landed_costs = ((line.product_price / line.picking_id.total_amount) * line.picking_id.cost_amount) * line.product_qty
			else:
				landed_costs = line.picking_id.cost_amount / line.picking_id.quantity_total
			result[line.id] = landed_costs
		return result

	_columns = {
		'product_price': fields.function(_get_product_price, digits_compute=dp.get_precision('Account'), string="Price Product"),
		'sub_total': fields.function(_get_subtotal, digits_compute=dp.get_precision('Account'), string="Subtotal"),
		# 'landing_cost' : fields.function(_get_landing_cost, digits_compute=dp.get_precision('Account'), string='Landing Cost'),
		'landing_cost' : fields.float(digits_compute=dp.get_precision('Account'), string='Landing Cost'),
		'copy_price_unit': fields.float(digits_compute=dp.get_precision('Account'), string='Copia del precio unitario')
	}

stock_move()