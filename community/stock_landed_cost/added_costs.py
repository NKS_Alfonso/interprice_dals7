from openerp.osv import osv,fields
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
import pdb
import time

class added_cost_order(osv.osv):
	_name = 'added.cost.order'
	_rec_name = 'cost_order_id'


	#FUNCIONES ORM 
	def create(self, cr, uid, vals, context=None):
		sufbijo = "CA" #PO
		sufbijo10 = "CA0" #PO0
		nuevo_id = None
		#--------------------secuencias----------------------------------------------------
		cr.execute("""SELECT COUNT(cost_order_id) FROM added_cost_order""")
		count = cr.fetchone()[0]
		if( count == 0 ) :
		  vals['cost_order_id'] = "CA001"
		  
		else:
			cr.execute('select substr(cost_order_id,3,1 ) from added_cost_order order by id desc')
			pos_3 =  cr.fetchone()[0]
			cr.execute('select substr(cost_order_id,4,1 ) from added_cost_order order by id desc')
			pos_4 =  cr.fetchone()[0]
			if ( pos_3 == '0' and pos_4 == '0') :
				cr.execute('select substr(cost_order_id,5,100 ) from added_cost_order order by id desc')
				pos_5 =  cr.fetchone()[0]
				if( pos_5 == '9'):
					vals['cost_order_id'] = "CA010"
				else:
					pos_6 = int(pos_5) + 1
					vals['cost_order_id'] = sufbijo + str(pos_3) + str(pos_4) +str(pos_6)
			elif(pos_3 == '0' and pos_4 != '0'):
				cr.execute('select substr(cost_order_id,4,100 ) from added_cost_order order by id desc')
				pos_7 =  cr.fetchone()[0]
				if( pos_7 == '99'):
					vals['cost_order_id'] = "CA100"
				else:
					pos_8 = int(pos_7) + 1
					vals['cost_order_id'] = sufbijo10 + str(pos_8)
			else:
				 cr.execute('select substr(cost_order_id,3,100 ) from added_cost_order order by id desc')
				 pos_9 =  int(cr.fetchone()[0]) + 1
				 vals['cost_order_id'] = sufbijo + str(pos_9)
		
		nuevo_id = super( added_cost_order, self).create( cr, uid, vals, context = context )
		return nuevo_id
	
	def get_currency_company(self,cr,uid,ids,context=None):
		currency_id = 0
		res_company = self.pool.get('res.company')
		res_user = self.pool.get('res.users')
		for u in res_user.browse(cr,uid,uid):
			for c in res_company.browse(cr,uid,u.company_id.id):
				currency_id = c.currency_id.id

		return currency_id
	#--------------------------------------campo landed_cost-------------------------------------------------------
	# def _landed_cost_by_quantity(self, cr, uid, ids, name, args, context):
	# 	if not ids : return {}
	# 	result = {}
	# 	landed_costs = 0.0
	# 	currency_company_id = 0
	# 	currency_obj = self.pool.get('res.currency')
	# 
	# 	for this in self.browse(cr,uid,ids):
	# 		for costs in this.aditionals_costs_ids:
	# 			if this.computation_cost and this.computation_cost == 'per_unit':
	# 				currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
	# 				if costs.currency_id.id != currency_company_id:
	# 					if costs.currency_rate_alter or costs.currency_rate_alter > 0:
	# 						landed_costs += costs.amount_total * costs.currency_rate_alter
	# 					else:
	# 						landed_costs += currency_obj.compute(cr, uid, costs.currency_id.id, currency_company_id, costs.amount_total, context=context)
	# 				else:
	# 					landed_costs += costs.amount_total
	# 		result[this.id] = landed_costs
	# 	return result
	
	def _landed_cost_by_quantity(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		currency_obj = self.pool.get('res.currency')
		currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
		for this in self.browse(cr,uid,ids):
			
			for costs in this.aditionals_costs_ids:
				if (costs.currency_id.name == 'MXN'):
					landed_costs += costs.amount_untaxed
				elif(costs.currency_id.name == 'USD'):
					if (costs.currency_rate_alter > 0):
						landed_costs+= (costs.amount_untaxed*costs.currency_rate_alter)
						
					elif(costs.currency_rate_alter == 0):
						cr.execute("""SELECT rate FROM res_currency_rate AS r INNER JOIN res_currency As c ON r.currency_id=c.id where c.name='USD'
								   ORDER BY r.name desc LIMIT 1""")
						rate = cr.fetchone()[0]
						landed_costs +=(costs.amount_untaxed/rate)
						
						
			result[this.id] = landed_costs
		return result
	#--------------------------------------termina campo landed_cost---------------------------------------------------
	def _landed_cost_by_percent(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		currency_company_id = 0
		currency_obj = self.pool.get('res.currency')

		for this in self.browse(cr,uid,ids):
			for costs in this.aditionals_costs_ids:
				if this.computation_cost and this.computation_cost == 'by_percent':
					currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
					if costs.currency_id.id != currency_company_id:
						if costs.currency_rate_alter or costs.currency_rate_alter > 0:
							landed_costs += costs.amount_total
						else:
							landed_costs += currency_obj.compute(cr, uid, costs.currency_id.id, currency_company_id, costs.amount_total, context=context)
					else:
						landed_costs += costs.amount_total
			result[this.id] = landed_costs
		return result
	
	def _landed_cost_by_equity(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		currency_company_id = 0
		currency_obj = self.pool.get('res.currency')
	
		for this in self.browse(cr,uid,ids):
			for costs in this.aditionals_costs_ids:
				if this.computation_cost and this.computation_cost == 'equal':
					currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
					if costs.currency_id.id != currency_company_id:
						if costs.currency_rate_alter or costs.currency_rate_alter > 0:
							landed_costs += costs.amount_total
						else:
							landed_costs += currency_obj.compute(cr, uid, costs.currency_id.id, currency_company_id, costs.amount_total, context=context)
					else:
						landed_costs += costs.amount_total
			result[this.id] = landed_costs
		return result
	
	# def _landed_cost(self, cr, uid, ids, name, args, context):
	# 	if not ids : return {}
	# 	result = {}
	# 	landed_costs = 0.0
	# 	currency_company_id = 0
	# 	currency_obj = self.pool.get('res.currency')
	# 
	# 	for this in self.browse(cr,uid,ids):
	# 		landed_costs += this.landed_cost_by_quantity + this.landed_cost_by_percent + this.landed_cost_by_equity
	# 		result[this.id] = landed_costs
	# 	return result
	
	def _landed_cost(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		currency_company_id = 0
		currency_obj = self.pool.get('res.currency')

		for this in self.browse(cr,uid,ids):
			landed_costs += this.landed_cost_by_quantity
			result[this.id] = landed_costs
		return result
	
	#----------------Convierte a pesos mexicanos las ordenes de compras internacioales-------------------------
	def _compras_internacionales(self, cr, uid, ids, name, args, context):
		if not ids : return {}
		result = {}
		landed_costs = 0.0
		currency_obj = self.pool.get('res.currency')
		currency_company_id = self.get_currency_company(cr,uid,ids,context=context)
		monto_total_compra = 0.0
		for this in self.browse(cr,uid,ids):

			for costs in this.picking_ids:
					cr.execute("""SELECT amount_total FROM purchase_order WHERE name=%s""",(costs.origin,))
					total = cr.fetchone()[0]
					cr.execute("""SELECT currency_rate_alter FROM purchase_order WHERE name=%s""",(costs.origin,))
					currency_rate_alter = cr.fetchone()[0]
					if (currency_rate_alter > 0):
						landed_costs+= (total*currency_rate_alter)
						
					elif(currency_rate_alter == 0):
						cr.execute("""SELECT rate FROM res_currency_rate AS r INNER JOIN res_currency As c ON r.currency_id=c.id where c.name='USD'
								   ORDER BY r.name desc LIMIT 1""")
						rate = cr.fetchone()[0]
						landed_costs +=(total/rate)
						
						
			result[this.id] = landed_costs
		return result

	_columns = {
		'cost_order_id': fields.char(string="Nombre", readonly=True),
		'name': fields.char(string="Name"),
		'date': fields.date(string="Fecha"),
		'state': fields.selection([('draft','Borrador'),('applied_cost','Aplicado'),('done','Hecho'),('cancel','Cancelado')], string="Estado"),
		'aditionals_costs_ids': fields.many2many('purchase.order','cost_order_purchase_order_rel','purchase_order_id','cos_added_order_id','Costs'),
		'picking_ids': fields.many2many('stock.picking','cost_order_picking_rel','order_id','picking_id', string='Pickings'),
		'landed_cost' : fields.function(_landed_cost, digits_compute=dp.get_precision('Account'), string='Total Costos Adicionales(MXN)'),
		'costos_compras' : fields.function(_compras_internacionales, digits_compute=dp.get_precision('Account'), string='Total Compras(MXN)'),
		'landed_cost_by_quantity' : fields.function(_landed_cost_by_quantity, digits_compute=dp.get_precision('Account'), string='Landed Costs Quantity'),
		'landed_cost_by_percent' : fields.function(_landed_cost_by_percent, digits_compute=dp.get_precision('Account'), string='Landed Costs Percent'),
		'landed_cost_by_equity' : fields.function(_landed_cost_by_equity, digits_compute=dp.get_precision('Account'), string='Landed Costs Equal'),
		'computation_cost': fields.selection([('per_unit','Por Cantidades'), ('by_percent','By Percent')], 'Metodo de coste'),
		
		#ids de nuevos albaranes de salida
		'stock_pickings_reverse_ids': fields.one2many('stock.picking','cost_order_id','Albaranes'),
	}

	_defaults = {
		# 'cost_order_id': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').get(cr, uid, 'cost.order.number'),
		'state': 'draft',
		'date': lambda *a: time.strftime('%Y-%m-%d'),
	}

	def dividing_expense(self,cr,uid,ids,context=None):
		for this in self.browse(cr,uid,ids):
			if this.computation_cost == 'per_unit':
				self.dividing_expenses_by_quantity(cr,uid,ids,context=context)
			# elif this.computation_cost == 'by_percent':
			# 	self.dividing_expenses_by_percent(cr,uid,ids,context=context)
			# else:
			# 	self.dividing_expenses_by_equity(cr,uid,ids,context=context)
		return True

	def dividing_expenses_by_quantity(self,cr,uid,ids,context=None):
		#----------------porcentajes----------------------------------------------------------------------------------inicio
		picking_obj = self.pool.get('stock.picking')
		stock_move = self.pool.get('stock.move')
		data = self.browse(cr,uid,ids)[0]
		landed_cost = data.landed_cost
		monto_total = data.costos_compras
		cantidad_productos = ""
		suma_porcentajes = 0
		suma_costos = 0
		cantidad_x_picking = 0.00
		cantidad_x_picking_final = 0.00
		monto_qty_percent=0
		monto_qty_percent_costadd = 0
		formulas = """ Monto total = Sum(montos totales de las compras)
					   Costos adicionales = 600
					   Total(por cada cantidad de productos) = Precio unitario x Cantidad
					   Porcentaje del monto total = ( Total x 100 ) / Monto total
					   Cantidad del % con respecto a los costos Ad = ( Porcentaje del monto total x  Costos adicionales ) / 100
					   Nuevo costo = ( Total + Cantidad del % con respecto a los costos Ad ) / Cantidad"""
		for id in ids:
			cr.execute("""SELECT sp.id FROM stock_picking AS sp
								INNER JOIN cost_order_picking_rel ca ON sp.id=ca.picking_id
								INNER JOIN added_cost_order AS ado  ON ca.order_id=ado.id WHERE ado.id=%s""",(id,))
			list_picking_ids = cr.fetchall()
			#obtiene los totales de cada cantidad de producto
			for i in list_picking_ids:
				cr.execute("""SELECT sm.id FROM stock_move AS sm INNER JOIN stock_picking AS sp ON sm.picking_id=sp.id WHERE sp.id=%s""",(i,))
				stock_move_id = cr.fetchall()
				for j in stock_move_id:
					#cantidad
					cr.execute("""SELECT product_uom_qty FROM stock_move WHERE id=%s""",(j,))
					qty = cr.fetchone()[0]
					#precio 
					cr.execute("""SELECT price_unit FROM stock_move WHERE id=%s""",(j,))
					price_unit = cr.fetchone()[0]
					#raise osv.except_osv( ('Alerta!'), ('No')+str(price_unit) )
					#monto por cantidad
					monto_qty = qty * price_unit
					#que porcentaje monto_qty es del monto total
					monto_qty_percent = ( monto_qty * 100 ) / monto_total
					#cantidad del % de monto_qty_percent con respecto a los costos adicionales
					monto_qty_percent_costadd = ( monto_qty_percent * landed_cost ) / 100
					stock_move.write(cr,uid,j,{'landing_cost':monto_qty_percent_costadd})
					#nuevo costo del producto
					cost_add_product = ( monto_qty + monto_qty_percent_costadd ) / qty
					suma_porcentajes = suma_porcentajes + monto_qty_percent
					suma_costos = suma_costos + monto_qty_percent_costadd
					cantidad_productos = cantidad_productos+" Cantidad: "+str(qty)+", Precio unitario "+str(price_unit)+", Total "+str(monto_qty)+", % del monto total "+str(monto_qty_percent)+", Cantidad costos Add. "+str(monto_qty_percent_costadd)+", Nuevo costo "+str(cost_add_product)+"\n\n"
					#cantidad por cada albaran
					cantidad_x_picking = cantidad_x_picking + monto_qty_percent_costadd
				cantidad_x_picking_final = cantidad_x_picking_final + cantidad_x_picking
				#raise osv.except_osv( ('Alerta!'), ('No')+str(cantidad_x_picking_final) )
				picking_obj.write(cr,uid,i,{'cost_amount':cantidad_x_picking_final,'computation_cost':'per_unit'})
				cantidad_x_picking = 0
				cantidad_x_picking_final = 0
		return True

	def dividing_expenses_by_percent(self, cr, uid, ids, context=None):
		picking_obj = self.pool.get('stock.picking')
		qty_piks = 0
		cost_by_qty = 0

		for this in self.browse(cr,uid,ids):
			qty_piks = len(this.picking_ids)
			cost_by_qty = this.landed_cost_by_percent / qty_piks
			for pik in this.picking_ids:
				picking_obj.write(cr,uid,pik.id,{'cost_amount':cost_by_qty,'computation_cost':'by_percent'})
		return True

	def dividing_expenses_by_equity(self, cr, uid, ids, context=None):
		picking_obj = self.pool.get('stock.picking')
		qty_piks = 0
		cost_by_qty = 0

		for this in self.browse(cr,uid,ids):
			qty_piks = len(this.picking_ids)
			cost_by_qty = this.landed_cost_by_equity / qty_piks
			for pik in this.picking_ids:
				picking_obj.write(cr,uid,pik.id,{'cost_amount':cost_by_qty,'computation_cost':'equal'})
		return True

	def apply(self,cr,uid,ids,context=None):
		picking_obj = self.pool.get('stock.picking')
		cost_obj = self.pool.get('purchase.order')
		stock_move = self.pool.get('stock.move')
		price_unit = 0.0
		for this in self.browse(cr,uid,ids,context=context):
			for pik in this.picking_ids:
				picking_obj.write(cr,uid,pik.id,{'is_release':True})
				#------aplicando los costos adicioales al precio unitario-------------
				for move in pik.move_lines:
					price_unit = (move.landing_cost / move.product_uom_qty) + move.price_unit
					copy_price_unit = move.price_unit
					# move.price_unit + move.landing_cost
					stock_move.write(cr,uid,move.id,{'price_unit':float(price_unit),'copy_price_unit':copy_price_unit,})
					price_unit = 0.0
				#---------------------------------------------------------------------
			for cost in this.aditionals_costs_ids:
				cost_obj.write(cr,uid,cost.id,{'applied_cost': True})
			
			self.write(cr,uid,this.id,{'state':'applied_cost'},context=context)
		return True

	def done(self,cr,uid,ids,context=None):
		cost_obj = self.pool.get('purchase.order')
		for this in self.browse(cr,uid,ids,context=context):
			for cost in this.aditionals_costs_ids:
				cost_obj.write(cr,uid,cost.id,{'applied_cost': True})
			self.write(cr,uid,this.id,{'state':'done'},context=context)
		return True


	def cancelar_polizas_first_pick(self, cr, uid, ids, context=None):
		for pik in ids: picking = pik
		account_move = self.pool.get('account.move')
		picking_obj = self.pool.get('stock.picking')
		pick = picking_obj.browse(cr, uid, pik, context=context)
		raise osv.except_osv(_('Warning !'), _(str(pick.id)))
		cr.execute("""SELECT id FROM account_move WHERE ref=%s""",(pick.name,))
		d_accoun_move = cr
		for ac in ids:
				account_move.button_cancel(cr, uid, [ac], context=context)
				
		
	def reset(self,cr,uid,ids,context=None):
		picking_obj = self.pool.get('stock.picking')
		move_obj = self.pool.get('stock.move')
		account_move = self.pool.get('account.move')
		qty_piks = 0
		cost_by_qty = 0
		data = self.browse(cr, uid, ids)
		for this in self.browse(cr,uid,ids):
			qty_piks = len(this.picking_ids)
			cost_by_qty = this.landed_cost_by_quantity / qty_piks
			for pik in this.picking_ids:
				if pik.state == 'done':
					cr.execute("""SELECT id FROM account_move WHERE ref=%s""",(pik.name,))
					id_accoun_move = cr.fetchall()
					#raise osv.except_osv(_('Warning !'), _(str(id_accoun_move)))
					for ac in id_accoun_move:
						account_move.button_cancel(cr, uid, ac, context=context)
						account_move.unlink(cr, uid, ac, context=context)
					#self.cancelar_polizas_first_pick(cr, uid, [pik], context=context)
					for mov in pik.move_lines:
						cr.execute("""SELECT count(pc.id) FROM stock_picking AS pc
								   INNER JOIN stock_move AS sm ON pc.id = sm.picking_id
								   WHERE pc.state='done'
								   and product_id=%s and pc.create_date>%s and pc.name!=%s""",(mov.product_id.id,pik.create_date,pik.name,))
						count = cr. fetchone()[0]
						#raise osv.except_osv(_('Warning !'), _(str(count)))
						if count==0:
							#raise osv.except_osv(_('Warning !'), _(str(count)))
						#move_obj.write(cr, uid,mov.id,{'landing_cost':0,'price_unit':mov.copy_price_unit,})
							cr.execute("""select cost from product_price_history
										where create_date<%s and product_template_id=%s
										order by create_date desc""",(pik.create_date,
																	  mov.product_id.product_tmpl_id.id))
							cost = cr.fetchone()[0]
							self.pool.get('product.template').write(cr, uid, mov.product_id.product_tmpl_id.id, {'standard_price': cost})
						elif(count>0):pass
					#picking_obj.write(cr,uid,pik.id,{'cost_amount':0,'is_release':False})
					#pasando id del albaran a la funcion _create_returns para revertir la trasferencia
					self._create_returns(cr, uid, [pik.id], this.id,context=None)
					for m in pik.move_lines:
						move_obj.write(cr, uid,m.id,{'landing_cost':0,'price_unit':m.copy_price_unit,})
					picking_obj.write(cr,uid,pik.id,{'cost_amount':0,'is_release':False})
					new_picking = picking_obj.copy(cr, uid, pik.id, context=context)
					picking_obj.action_confirm(cr, uid, [new_picking], context=context)
					picking_obj.action_assign(cr, uid, [new_picking], context)
					cr.execute("""update cost_order_picking_rel set picking_id=%s where order_id=%s""",(new_picking,data.id,))
				
				else:
					for movi in pik.move_lines:
						move_obj.write(cr, uid,movi.id,{'landing_cost':0,'price_unit':movi.copy_price_unit,})
					picking_obj.write(cr,uid,pik.id,{'cost_amount':0,'is_release':False})
					#raise osv.except_osv(_('Warning !'), _(str(lista_pik_ids)))
			self.write(cr,uid,this.id,{'state':'cancel'},context=context)
			#self.write(cr,uid,this.id,{'state':'draft'},context=context)
		return True
	
	def reset_draft(self, cr, uid, ids, context=None):
		account_move = self.pool.get('account.move')
		data = self.browse(cr, uid, ids)
		if data.stock_pickings_reverse_ids:
			for this in data.stock_pickings_reverse_ids:
				if this.state !='done':
					raise osv.except_osv(_('Warning !'), _("Favor de finalizar los albaranes"))	
				elif(this.state=='done'):
						cr.execute("""SELECT id FROM account_move WHERE ref=%s""",(this.name,))
						id_accoun_move = cr.fetchall()
						for ac in id_accoun_move:
							account_move.button_cancel(cr, uid, ac, context=context)
							account_move.unlink(cr, uid, ac, context=context)
			self.write(cr, uid, data.id, {'state':'draft'}, context=context)
		#self.write(cr,uid,this.id,{'state':'draft'},context=context)
		
	
	def _create_returns(self, cr, uid, ids, this,context=None):
		if context is None:
			context = {}
		for picking in ids:
			record_id = picking
		#raise osv.except_osv(_('Warning !'), _(str(this)))
		#record_id = context and context.get('active_id', False) or False
		move_obj = self.pool.get('stock.move')
		pick_obj = self.pool.get('stock.picking')
		uom_obj = self.pool.get('product.uom')
		data_obj = self.pool.get('stock.return.picking.line')
		pick = pick_obj.browse(cr, uid, record_id, context=context)
		data = self.read(cr, uid, ids[0], context=context)
		returned_lines = 0
		move=0
		# Cancel assignment of existing chained assigned moves
		moves_to_unreserve = []
		for move in pick.move_lines:
			to_check_moves = [move.move_dest_id] if move.move_dest_id.id else []
			while to_check_moves:
				current_move = to_check_moves.pop()
				if current_move.state not in ('done', 'cancel') and current_move.reserved_quant_ids:
					moves_to_unreserve.append(current_move.id)
				split_move_ids = move_obj.search(cr, uid, [('split_from', '=', current_move.id)], context=context)
				if split_move_ids:
					to_check_moves += move_obj.browse(cr, uid, split_move_ids, context=context)
	
		if moves_to_unreserve:
			move_obj.do_unreserve(cr, uid, moves_to_unreserve, context=context)
			#break the link between moves in order to be able to fix them later if needed
			move_obj.write(cr, uid, moves_to_unreserve, {'move_orig_ids': False}, context=context)
	
		#Create new picking for returned products
		pick_type_id = pick.picking_type_id.return_picking_type_id and pick.picking_type_id.return_picking_type_id.id or pick.picking_type_id.id
		new_picking = pick_obj.copy(cr, uid, pick.id, {
			'move_lines': [],
			'picking_type_id': pick_type_id,
			'state': 'draft',
			'origin': pick.name,
			'cost_order_id':this,
		}, context=context)
		move=0
		#for data_get in data_obj.browse(cr, uid, data['product_return_moves'], context=context):
		for  data_get in pick.move_lines:
				lot_id = self.pool.get('stock.production.lot').search(cr, uid, [('product_id','=',data_get.product_id.id),('name','=',pick.lot)])[0]
				#raise osv.except_osv(_('Warning!'), _(str(lot_id)))
				move = data_get
				if not move:
					raise osv.except_osv(_('Warning !'), _("You have manually created product lines, please delete them to proceed"))
				new_qty = data_get.product_uom_qty
				if new_qty:
					# The return of a return should be linked with the original's destination move if it was not cancelled
					if move.origin_returned_move_id.move_dest_id.id and move.origin_returned_move_id.move_dest_id.state != 'cancel':
						move_dest_id = move.origin_returned_move_id.move_dest_id.id
					else:
						move_dest_id = False
		
					returned_lines += 1
					move_obj.copy(cr, uid, move.id, {
						'product_id': move.product_id.id,
						'price_unit': move.price_unit,
						'product_uom_qty': new_qty,
						'product_uos_qty': new_qty * move.product_uos_qty / move.product_uom_qty,
						'picking_id': new_picking,
						'state': 'draft',
						'location_id': move.location_dest_id.id,
						'location_dest_id': move.location_id.id,
						'picking_type_id': pick_type_id,
						'warehouse_id': pick.picking_type_id.warehouse_id.id,
						'origin_returned_move_id': move.id,
						'procure_method': 'make_to_stock',
						'restrict_lot_id': lot_id,
						'move_dest_id': move_dest_id,
					})
		
		if not returned_lines:
			raise osv.except_osv(_('Warning!'), _("Please specify at least one non-zero quantity."))
	
		pick_obj.action_confirm(cr, uid, [new_picking], context=context)
		pick_obj.action_assign(cr, uid, [new_picking], context)
			#return new_picking, pick_type_id
