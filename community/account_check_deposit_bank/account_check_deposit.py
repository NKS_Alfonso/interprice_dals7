# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import pdb

class account_bank_deposit(models.Model):
	_inherit = 'account.check.deposit'

	account_id = fields.Many2one('account.account',string="Cuenta")

	def _prepare_counterpart_move_lines_vals(self, cr, uid, deposit, total_debit, total_amount_currency,context=None):
		account_id = 0
		#pdb.set_trace()
		account_id = deposit.account_id.id

		return {
			'name': _('Check Deposit %s') % deposit.name,
			'debit': total_debit,
			'credit': 0.0,
			'account_id': account_id,
			'partner_id': False,
			'currency_id': deposit.currency_none_same_company_id.id or False,
			'amount_currency': total_amount_currency,
		}