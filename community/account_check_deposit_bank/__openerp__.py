# -*- coding: utf-8 -*-
{
    'name': "account_check_deposit_bank",

    'summary': """""",

    'description': """
    """,

    'author': "tantums",
    'website': "http://www.tantums.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'bank',
    'version': '8.0',

    # any module necessary for this one to work correctly
    'depends': ['base','account','account_check_deposit'],

    # always loaded
    'data': ['view/account_check_deposit_view.xml',
        # 'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}