#!/usr/bin/python
# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#    Copyright (C) Vauxoo (<http://vauxoo.com>).
#    All Rights Reserved
# Credits######################################################
#    Coded by: Carlos Funes(juan@vauxoo.com)
#############################################################################
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################
from openerp.osv import fields as fieldsv7, osv
from openerp import models, fields, _


class account_tax_category(models.Model):
    _name = 'account.tax.category'
    _inherit = 'mail.thread'

    company_id = fields.Many2one('res.company', 'Company', required=True, help='Company where will add this category')
    name = fields.Char('Name', size=64, required=True, help='Name for this category')
    code = fields.Char('Code', size=32, required=True, help='Code for this category')
    active = fields.Boolean('Active', help='Indicate if this category is active')
    sign = fields.Integer('Sign')
    category_ids = fields.One2many('account.tax', 'tax_category_id', 'Category',
                                   help='Tax that belong of this category')
    value_tax = fields.Float('Value Tax', help='Amount of Tax Original')
    factor_type = fields.Selection(string=_('Factor type'), selection=[
        ('Tasa', _('Rate')),
        ('Couta', _('Fee')),
        ('Exento', _('Exempt'))
    ], required=True)

    _defaults = {

        'active': 1,
        'company_id': lambda s, cr, uid, c:
        s.pool.get('res.company')._company_default_get
        (cr, uid, 'account.tax.category', context=c),
    }


class account_tax(osv.Model):
    _inherit = 'account.tax'

    _columns = {
        'tax_category_id': fieldsv7.many2one('account.tax.category',
                                             'Tax Category', required=True, help='Category of this tax'),
    }
