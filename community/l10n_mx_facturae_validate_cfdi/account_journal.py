# -*- encoding: utf-8 -*-
# ##########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Openpyme (<http://openpyme.mx>)
#
#    Coded by: Miguel Angel Villafuerte Ojeda (mikeshot@gmail.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv


class account_journal(osv.Model):
    _inherit = 'account.journal'

    _columns = {
        'validate_sat': fields.boolean(
            'Validate SAT',
            help="If this field is enabled, then validates through the"
            " webservice of the Mexican SAT that the info of "
            " CFDI is valid (attached or provided in the form)"
        ),
        'validate_xml': fields.boolean(
            'Validate XML',
            help="If this field is enabled, then validates that"
            " the supplier invoices has a valid XML file attached"
        ),
        'max_diff': fields.float(
            'Max difference',
            help="Max difference between total calculated on invoice form"
            " and total indicated on XML file"
        )
    }
