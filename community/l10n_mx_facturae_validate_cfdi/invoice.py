# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Vaouxoo (<http://www.vauxoo.com/>)
#             Openpyme (<http://openpyme.mx>)
#
#
#    Coded by: moylop260 (moylop260@vauxoo.com)
#              Agustín Cruz Lozano (agustin.cruz@openpyme.mx)
#              Miguel Angel Villafuerte Ojeda (mikeshot@gmail.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public Licens
#
##############################################################################

from openerp.osv import orm
from openerp.tools.translate import _
import base64
import logging
from suds.client import Client

_logger = logging.getLogger(__name__)


try:
    import xmltodict
except:
    _logger.error(
        'Execute "sudo pip install xmltodict" to use '
        'l10n_mx_facturae_report module.')


class AccountInvoice(orm.Model):
    _inherit = 'account.invoice'

    _sql_constraints = [
        ('cfdi_company_uniq', 'unique (cfdi_folio_fiscal, company_id)',
         'The CFDI must be unique per company !'),
    ]

    def invoice_validate(self, cr, uid, ids, context=None):
        def validate_cfdi_folio_fiscal(data):
            """ Validate an UUID on SAT servers"""
            # Validates if "data" exists
            if not data:
                raise orm.except_orm(
                    _('Error'),
                    _('CFDI Data not found\n'
                      'You must attach a valid XML invoice file or '
                      'provide a valid CFDI number!')
                )

            # Webservice parameters
            url = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl'
            client = Client(url)
            # Consume webservice
            result = client.service.Consulta(
                """"?re=%s&rr=%s&tt=%s&id=%s""" %
                (data['vat_emitter'] or '', data['vat_receiver'] or '',
                 data['amount'] or 0.0, data['uuid'] or '')
            )
            result = result and result.Estado or ''

            # If webservice does not return 'Vigente' state,
            # raises an exception
            if result != 'Vigente':
                raise orm.except_orm(
                    _('Error'),
                    _('The CFDI provided did not passed the SAT validation\n'
                      'The UUID sate on SAT servers is %s') % result)

            # If CFDI is valid:
            return True

        # Main function
        if context is None:
            context = {}

        for invoice in self.browse(cr, uid, ids, context=context):
            # If the user provides the field "cfdi_folio_fiscal" then
            # validates the info of the invoice, not the xml file
            if invoice.cfdi_folio_fiscal and invoice.journal_id.validate_sat:
                if not invoice.partner_id.vat_split:
                    raise orm.except_orm(
                        _('Error'),
                        _('Not VAT number provided for this supplier!')
                    )
                data = {
                    'vat_emitter': invoice.partner_id.vat_split,
                    'vat_receiver': invoice.company_id.partner_id.vat_split,
                    'amount': invoice.amount_total,
                    'uuid': invoice.cfdi_folio_fiscal
                }
                # Validates the CFDI info if the journal has enabled the
                # "validate_sat" option
                validate_cfdi_folio_fiscal(data)
                continue

            # Validates the XML info if the journal has enabled the
            # "validate_xml" option
            if invoice.journal_id.validate_xml:
                att_obj = self.pool.get('ir.attachment')
                att_ids = att_obj.search(
                    cr, uid,
                    [('res_id', '=', invoice.id),
                     ('res_model', '=', 'account.invoice'),
                     '|',
                     ('file_type', '=', 'application/xml'),
                     ('file_type', '=', 'text/plain')],
                    context=context)

                # Process and stores the info of the attachments
                count = 0
                for att in att_obj.browse(cr, uid, att_ids, context=context):
                    try:
                        db_datas = att.db_datas or att.datas
                        data_xml = base64.decodestring(db_datas)
                        dict_data = dict(
                            xmltodict.parse(data_xml).get(
                                'cfdi:Comprobante', {})
                        )
                    except:
                        # Skip file if any error
                        continue

                    complemento = dict_data.get('cfdi:Complemento', {})
                    emitter = dict_data.get('cfdi:Emisor', {})
                    receiver = dict_data.get('cfdi:Receptor', {})
                    data = {
                        'amount': float(dict_data.get('@total', 0.0)),
                        'uuid': complemento.get(
                            'tfd:TimbreFiscalDigital', {}
                        ).get('@UUID', ''),
                        'vat_emitter': emitter.get('@rfc', ''),
                        'vat_receiver': receiver.get('@rfc', '')
                    }
                    count = count + 1

                # Verify that exists only 1 xml attachment related
                # to the invoice
                if count == 0:
                    raise orm.except_orm(
                        _('Error'),
                        _('Not found XML invoice information.\n'
                          'You must attach a valid XML invoice'
                          ' file or provide a valid CFDI number!')
                    )
                elif count > 1:
                    raise orm.except_orm(
                        _('Error'),
                        _('Multiple XML file not valid.\n'
                          'You must provide only one XML '
                          'invoice file!')
                    )
                # Verify that VAT number on XML file match company VAT number
                if invoice.type in ['in_invoice', 'in_refund']:
                    actor = _('Receiver')
                    vat = data['vat_receiver']
                else:
                    actor = _('Emitter')
                    vat = data['vat_emitter']
                if vat != invoice.company_id.partner_id.vat_split:
                    raise orm.except_orm(
                        _('Error'),
                        _('%s VAT number (%s) on XML file not match company VAT'
                          % (actor, vat))
                    )
                # Verify that total on XML file match total on current
                # invoice
                invoice_diff = abs(data['amount'] - invoice.amount_total)
                if invoice_diff > invoice.journal_id.max_diff:
                    raise orm.except_orm(
                        _('Error'),
                        _('Amount on XML file not match current '
                          'invoice total')
                    )

                # Validates the CFDI info if the journal has enabled the
                # "validate_sat" option
                if invoice.journal_id.validate_sat:
                    validate_cfdi_folio_fiscal(data)

                # Update the invoice adding uuid info
                self.write(
                    cr, uid, ids[0],
                    {'cfdi_folio_fiscal': data['uuid']},
                    context=context
                )

        return super(AccountInvoice, self).invoice_validate(
            cr, uid, ids, context=context
        )
