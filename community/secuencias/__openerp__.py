# -*- coding: utf-8 -*-
{
    'name': "secuencias",

    'summary': """
        Secuencias""",

    'description': """
        Arregla los huecos que quedan en las secuencias cuando se borra un registro de la base
        de los modulos de Ventas y Compras
    """,

    'author': "Israel Cabrera --gvadeto",
    

    # for the full list
    'category': 'PV',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        #     'campos_extra/purchase_order_view.xml',
            # 'campos_extra/stock_picking_view.xml',
            # 'campos_extra/stock_inventory_view.xml',
            #'campos_extra/stock_return_picking.xml'


    ],
}
