# -*- coding: utf-8 -*-
#!/usr/bin/env python
#by Israel Cabrera Juarez
from openerp.osv import fields, osv
import base64, StringIO, csv
from openerp.osv import orm, fields
from openerp.addons.decimal_precision import decimal_precision as dp
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)
import pdb

header_fields = ['serie', 'producto']

class serial_product(orm.TransientModel):
	_name = 'serial.product'
	_description = 'Numeros de series'

	_columns = {
		'archivo_csv': fields.binary('Archivo', required=True),#file
		'nombre_archivo': fields.char('Filename', size=128, required=True),
		'csv_separator': fields.selection([(',',','),(';',';')], 'CSV Separator', required=True),
		'decimal_separator': fields.selection([('.','.'),(',',',')], 'Decimal Separator', required=False),
	}
	_defaults = {
		'nombre_archivo': '',
		'csv_separator': ',',
	}

	def asignar_series(self, cr, uid, ids, context=None):
		data = self.browse(cr,uid,ids)[0]
		serie = ""
		codigo = ""
		archivo_csv = data.archivo_csv
		contenido=base64.decodestring(archivo_csv)
		csv_separator = str(data.csv_separator)
		reader = csv.reader(StringIO.StringIO(contenido), delimiter=csv_separator)
		for index in reader:
			# process header line
			serial_number = index[0].strip()
			product_code = index[1].strip()
			if serial_number == header_fields[0]:
				pass
			else:
				serie = serie+str(index[0].strip())
				codigo = codigo+str(index[1].strip())
		#nombre_archivo=base64.decodestring(aml_file)
		#reader = csv.reader(StringIO.StringIO(lines), delimiter=csv_separator)
serial_product()

