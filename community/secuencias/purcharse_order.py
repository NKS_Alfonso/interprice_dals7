# -*- coding: utf-8 -*-
#by Israel Cabrera Juarez
from openerp.osv import fields, osv
from openerp.tools.translate import _


class purchase_order(osv.osv):
    _inherit = 'purchase.order'
    _columns = {

    }

    def create(self, cr, uid, vals, context=None):

        _sequence_obj = self.pool['ir.sequence']
        next_number = _sequence_obj.next_by_code(cr, uid, 'purchase.order', context=None)
        order = None
        if next_number:
            vals['name'] = next_number
            context = dict(context or {}, mail_create_nolog=True)
            order = super(purchase_order, self).create(cr, uid, vals, context=context)
            self.message_post(cr, uid, [order], body=_("RFQ created"), context=context)
        else:
            raise osv.except_osv(_('Error!'), (_('No se pudo generar la secuencia, favor de verificar la configuración!')))
        return order

         
purchase_order()

