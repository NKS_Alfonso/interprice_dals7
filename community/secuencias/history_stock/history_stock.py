# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                                #
#  @creacion    :  (aaaa/mm/dd)                                                                                                                      #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
from openerp import models, fields


from openerp.osv import fields, osv
from datetime import datetime, date

#Modulo :: historial del stock: Para reportes
class historial_inventario( osv.osv ) :

  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                         Variables Privadas y Publicas (No variables del API de OPENERP)                                          #
  #                                                                                                                                                  #
  ####################################################################################################################################################  
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Privados (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                                 Metodos Publicos (No metodos del API de OPENERP)                                                 #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  
  #---------------------------------------------------------------------------------------------------------------------------------------------------
  
  ####################################################################################################################################################
  #                                                                                                                                                  #
  #                                          Metodos OPENERP de Procesos Publicos (No metodos para reportes)                                         #
  #                                                                                                                                                  #
  ####################################################################################################################################################
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                          OPENERP Metodos ORM                                                                 ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
 
  #---------------------------------------------------------------------------------------------------------------------------------------------------

  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                            Metodos OnChange                                                                  ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###

  #---------------------------------------------------------------------------------------------------------------------------------------------------

  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                      Metodos para campos "function"                                                          ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                                  Atributos basicos de un modelo OPENERP                                                      ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Nombre del modelo
  _name = 'history.stock'
  
  #Nombre de la tabla
  _table = '_history_stock'
  
  #Nombre de la descripcion al usuario en las relaciones m2o hacia este módulo
  _rec_name = 'document_origin'
  
  #Clausula SQL "ORDER BY"
  #_order = 'res_users_id'

  #Columnas y/o campos Tree & Form
  
  _columns = {
    
    # =========================================  OpenERP Campos Basicos (integer, char, text, float, etc...)  ====================================== #
    'product_qty_move': fields.float('Cantidad',help='Cantida del producto que entra o sale (ventas o compras etc.)'),
    'document_origin':fields.char('Documento Origen',size=254, help='Gurada el documento origen, PO,SO, ETC.'),
    'picking_origin': fields.char('Albaran Origen',size=254,help='Guarda el albaran origen'),
    'existencia_anterior':fields.integer('Existencia Antes del movimiento', help='Guarda la existencia que hay un deteminado almacen antes del movimiento'),
    'existencia_posterior':fields.integer('Existencia despues del movimiento', help='Guarda la existencia de un determinado almacen despues del movimiento'),
    # ========================================================  Relaciones [many2one](m2o) ========================================================= #
    'product_id': fields.many2one('product.product','Producto',help='Guarda el id del producto'),
    'warehouse_id': fields.many2one('stock.warehouse','Almacen',help='Guarda el id del almacen donde se encuentra el producto'),
    'location_id':fields.many2one('stock.location','ubicacion',help='Guarda el id de la ubicacion del almacen donde se encuentra el producto'),
    'ajuste_id': fields.many2one('stock.inventory','AjusteInventario', help='guarda el id del ajuste de inventario'),
    'picking_id':fields.many2one('stock.picking','Id albaran',help='Guarda el id del albaran'),
    'mrp_picking_type_code': fields.char('Tipo albaran',help='Tipo albaran en manufactura'),
    
    # ========================================================  Relaciones [one2many](o2m) ========================================================= #
  
    # ========================================================  Relaciones [many2many](m2m) ======================================================== #
    
    # ======================================================== Campos "function" (function) ======================================================== #
    
  }
  
  #Valores por defecto de los campos del diccionario [_columns]
  _defaults = {
  }
  
  #Restricciones de BD (constraints)
  _sql_constraints = []
  
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  ###                                                                                                                                              ###
  ###                                              Metodos para validacion de la lista: [_constraints]                                             ###
  ###                                                                                                                                              ###
  ### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
  
  #Restricciones desde codigo
  _constraints = []
  
historial_inventario()


class stock_transfer_details(osv.osv_memory):
	_inherit = 'stock.transfer_details'
	### //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ###
	###                                     decoradores                                                                                              ###
	### ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  ###
	
	###########history.stock###########################################################
	def history_purchase(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de albaranes de entrada(compras)"""
		#product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.picking_id.origin
		warehouse_id = data.picking_id.picking_type_id.warehouse_id.id
		location_id = data.picking_id.picking_type_id.default_location_dest_id.id
		picking_origin = data.picking_id.name
		picking_id = data.picking_id.id
		for this in data.item_ids:
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,location_id,))
			for (qty, sm) in cr.fetchall():
				if qty==0: product_qty = 0
				elif(qty>0): product_qty = sm
				write_vals = {
							  'product_id':this.product_id.id,
							  'warehouse_id':warehouse_id,
							  'location_id':location_id,
							  'product_qty_move':this.quantity,
							  'existencia_anterior': product_qty,
							  'existencia_posterior': this.quantity+product_qty,
							  'document_origin': document_origen,
							  'picking_origin':picking_origin,
							  'picking_id':picking_id,
							  }
				obj_history_stock.create(cr, uid, write_vals, context=context)
	
	def history_sale(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de albaranes de salida(ventas)"""
		#product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.picking_id.origin
		warehouse_id = data.picking_id.picking_type_id.warehouse_id.id
		location_id = data.picking_id.picking_type_id.default_location_src_id.id
		picking_origin = data.picking_id.name
		picking_id = data.picking_id.id
		for this in data.item_ids:
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,location_id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								  'product_id':this.product_id.id,
								  'warehouse_id':warehouse_id,
								  'location_id':location_id,
								  'product_qty_move':-(this.quantity),
								  'existencia_anterior': product_qty,
								  'existencia_posterior':product_qty-this.quantity,
								  'document_origin': document_origen,
								  'picking_origin':picking_origin,
								  'picking_id':picking_id,
								  }
					obj_history_stock.create(cr, uid, write_vals, context=context)
					
	def history_transfer(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de albaranes de transferencias(internas)"""
		#product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.picking_id.origin
		picking_origin = data.picking_id.name
		picking_id = data.picking_id.id
		for this in data.item_ids:
			#historial origen (salida)
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,this.sourceloc_id.id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								  'product_id':this.product_id.id,
								  'warehouse_id':self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id', '=', this.sourceloc_id.id)])[0],
								  'location_id':this.sourceloc_id.id,
								  'product_qty_move':-(this.quantity),
								  'existencia_anterior': product_qty,
								  'existencia_posterior':product_qty-this.quantity,
								  'document_origin': document_origen,
								  'picking_origin':picking_origin,
								  'picking_id':picking_id,
								  }
					obj_history_stock.create(cr, uid, write_vals, context=context)
			#historial destino (entrada)
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,this.destinationloc_id.id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								  'product_id':this.product_id.id,
								  'warehouse_id':self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id', '=', this.destinationloc_id.id)])[0],
								  'location_id':this.destinationloc_id.id,
								  'product_qty_move':this.quantity,
								  'existencia_anterior': product_qty,
								  'existencia_posterior':product_qty+this.quantity,
								  'document_origin': document_origen,
								  'picking_origin':picking_origin,
								  'picking_id':picking_id,
								  }
					obj_history_stock.create(cr, uid, write_vals, context=context)

stock_transfer_details()

###################################################################################

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
