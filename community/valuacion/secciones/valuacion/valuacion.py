# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : ICJ                                                                                                                                #
#  @creacion    :  (aaaa/mm/dd)                                                                                                                      #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
#from osv import fields, osv
import base64
import cStringIO
from openerp import models, api, _
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _
import logging
from openerp.tools.safe_eval import safe_eval as eval
import pytz
import time
from datetime import date
import datetime
from openerp import SUPERUSER_ID
from openerp.addons.payment.models.payment_acquirer import ValidationError
import openerp.addons.decimal_precision as dp
_logger = logging.getLogger(__name__)

class valuacion(osv.osv):
    
    _name='gv.valuacion'
    _table = '_gv_valuacion'
    _order = 'partner_id'
    #rec_name='fecha_inicial'
    
    
    def activar_historial(self, cr, uid, ids, context=None):
        """Activa el campo que hace visible los campos de fechas para
            Porder consultar un historial de las valuaciones pasadas"""
        self.write(cr, uid, ids, {'historial':True},context)
    
    
    def cuentas_proevedor_nacional_internacional(self, cr, uid, account_id,context=None):
        """Regresa el tipo de proveedor (Nacional o Internacional),y el id del proveedor"""
        cr.execute("""SELECT ac.id FROM account_account AS ac INNER JOIN account_account_sat_group AS acg ON ac.sat_group_id=acg.id
                            WHERE acg.code='201.01' and ac.code='200-1000'""")
        cuenta_proveedor_nacional = cr.fetchone()[0]
        cr.execute("""SELECT ac.id FROM account_account AS ac INNER JOIN account_account_sat_group AS acg ON ac.sat_group_id=acg.id
                            WHERE acg.code='201.02' and ac.code='200-2000'""")
        cuenta_proveedor_internacional = cr.fetchone()[0]
        if cuenta_proveedor_nacional == account_id:
            return 'nacional',cuenta_proveedor_nacional
        if cuenta_proveedor_internacional ==account_id:
            return 'internacional',cuenta_proveedor_internacional
    
    def perdida_o_ganancia(self, cr, uid, m_facturas,m_notas, m_anticipos,impuestos, context=None):
        """Regresa el tipo de cuenta segun sea ganancia o perdida"""
        cr.execute("""SELECT ac.id FROM account_account AS ac INNER JOIN account_account_sat_group AS acg ON ac.sat_group_id=acg.id
                            WHERE acg.code='701.01' and ac.code='502-0200'""")
        account_perdida_id = cr.fetchone()[0]
        cr.execute("""SELECT ac.id FROM account_account AS ac INNER JOIN account_account_sat_group AS acg ON ac.sat_group_id=acg.id
                            WHERE acg.code='701.01' and ac.code='407-0000'""")
        account_ganancia_id = cr.fetchone()[0]
        if m_facturas>0 and m_notas==0 and m_anticipos==0:
            return 'Perdida', account_perdida_id,'factura', impuestos,m_facturas
        elif m_facturas<0 and m_notas==0 and m_anticipos==0:
            return 'Ganancia',account_ganancia_id,'factura',impuestos,m_facturas
        elif m_facturas==0 and m_notas>0 and m_anticipos==0:
            return 'Perdida',account_perdida_id,'nota',impuestos,m_notas
        elif m_facturas==0 and m_notas<0 and m_anticipos==0:
            return 'Ganancia',account_ganancia_id,'nota',impuestos,m_notas
        elif m_facturas==0 and m_notas==0 and m_anticipos>0:
            return 'Perdida',account_perdida_id,'anticipo',impuestos,m_anticipos
        elif m_facturas==0 and m_notas==0 and m_anticipos<0:
            return 'Ganancia',account_ganancia_id,'anticipo',impuestos,m_anticipos
        elif m_facturas==0 and m_notas==0 and m_anticipos==0:
            return 'NA',0,'NA',0.00,0.00
    
    
    def _crear_lineas_polizas_perdida(self, cr,uid, id_poliza, perdida_o_ganancia_id,impuestos,monto,tipo_doc,context=None):
        #Perdida o ganancia
        impuesto = 0.00
        cr.execute("""SELECT id FROM account_account WHERE code='107-0000'""")
        iva_id = cr.fetchone()[0]
        for u in self.pool.get('res.users').browse(cr,uid,uid):
               company_id = u.company_id.id
        period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
        if tipo_doc !='anticipo':
            self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':tipo_doc,
                                            'account_id':perdida_o_ganancia_id,
                                            'debit':monto,
                                            'credit':0.00,
                                            })
            if impuestos > 0: impuesto = impuestos
            if impuestos < 0: impuesto = abs(impuestos)
            if impuesto >0:
                 self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':'IVA',
                                            'account_id':iva_id,
                                            'debit':impuesto,
                                            'credit':0.00,
                                            })
        elif tipo_doc=='anticipo':
            self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':tipo_doc,
                                            'account_id':perdida_o_ganancia_id,
                                            'debit':monto,
                                            'credit':0.00,
                                            })
    def _crear_lineas_polizas_ganancia(self, cr,uid,id_poliza, perdida_o_ganancia_id,impuestos,monto,tipo_doc, context=None):
        #Perdida o ganancia
        impuesto = 0.00
        cr.execute("""SELECT id FROM account_account WHERE code='107-0000'""")
        iva_id = cr.fetchone()[0]
        for u in self.pool.get('res.users').browse(cr,uid,uid):
               company_id = u.company_id.id
        period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
        if tipo_doc !='anticipo':
            self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':tipo_doc,
                                            'account_id':perdida_o_ganancia_id,
                                            'debit':0.00,
                                            'credit':monto
                                            })
            if impuestos > 0: impuesto = impuestos
            if impuestos < 0: impuesto = abs(impuestos)
            if impuesto > 0:
                 self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':'IVA',
                                            'account_id':iva_id,
                                            'debit':0.00,
                                            'credit':impuesto
                                            })
        elif tipo_doc=='anticipo':
            self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':tipo_doc,
                                            'account_id':perdida_o_ganancia_id,
                                            'debit':0.00,
                                            'credit':monto
                                            })
    
    def _crear_lineas_polizas(self, cr, uid, id_poliza,data, context=None):
        """Crea los asiientos contables(Lineas ligadas a la poliza)
        1.- Facturas--Si el resultado es negativo significa que es una ganancia(siempre se abona),
                      Si el resultado es positivo siginifica que hay una perdida(siempre se carga)
        2.- Notas de credito y antipos-- Si el resultado es negativo significa que
                                         hay una ganancia(siempre se abona)
                                         Si el resultado es positivo significa que
                                         hay una Perdida(siempre se carga)"""
        impuesto = 0.00
        monto_final = 0.00
        for u in self.pool.get('res.users').browse(cr,uid,uid):
               company_id = u.company_id.id
        period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
        for this in data.gv_valuacion_partner_id:
            tipo_proveedor,cuenta_id = self.cuentas_proevedor_nacional_internacional(cr, uid, this.partner_id.property_account_payable.id, context)
            perdida_o_ganancia, perdida_o_ganancia_id,tipo_doc,impuestos,monto = self.perdida_o_ganancia(cr,uid,this.resultado,this.resultado_notas,this.resultado_anticipos,this.impuestos,context)
            if impuestos>0:impuesto = impuestos
            if impuestos<0:impuesto = abs(impuestos)
            if monto > 0: monto_final = monto
            if monto<0:monto_final = abs(monto)
            if perdida_o_ganancia == 'Perdida' and tipo_doc=='antipo':
                self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':'Proveedores',
                                            'account_id':cuenta_id,
                                            'debit':0.00,
                                            'credit':monto_final
                                            })
                self._crear_lineas_polizas_perdida(cr, uid, id_poliza,perdida_o_ganancia_id,impuestos,monto,tipo_doc, context)
            
            elif perdida_o_ganancia == 'Perdida' and tipo_doc!='antipo':
               self.pool.get('account.move.line').create(cr, uid, {
                                           'partner_id':company_id,
                                           'move_id':id_poliza,
                                           'period_id':period_ids[0],
                                           'date':datetime.datetime.now(),
                                           'name':'Proveedores',
                                           'account_id':cuenta_id,
                                           'debit':0.00,
                                           'credit':monto_final+impuesto
                                           })
               self._crear_lineas_polizas_perdida(cr, uid, id_poliza,perdida_o_ganancia_id,impuestos,monto,tipo_doc, context)
            ######------------------------------------------------------------------------------                     
            elif perdida_o_ganancia == 'Ganancia' and tipo_doc=='anticipo':
                self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':'Proveedores',
                                            'account_id':cuenta_id,
                                            'debit':monto_final,
                                            'credit':0.00,
                                            })
                self._crear_lineas_polizas_ganancia(cr, uid, id_poliza,perdida_o_ganancia_id,impuestos,monto_final,tipo_doc, context)
            elif perdida_o_ganancia == 'Ganancia' and tipo_doc!='anticipo':
                self.pool.get('account.move.line').create(cr, uid, {
                                            'partner_id':company_id,
                                            'move_id':id_poliza,
                                            'period_id':period_ids[0],
                                            'date':datetime.datetime.now(),
                                            'name':'Proveedores',
                                            'account_id':cuenta_id,
                                            'debit':monto_final+impuesto,
                                            'credit':0.00,})
                self._crear_lineas_polizas_ganancia(cr, uid, id_poliza,perdida_o_ganancia_id,impuestos,monto_final,tipo_doc, context)
            impuesto = 0.00
            monto_final = 0.00
        #raise osv.except_osv( ('Alerta!'), ('El ajuste %s no tiene lineas con productos'+str(this.partner_id.name)+" : "+str(tipo_proveedor)+" : "+str(cuenta_id)+" : "+str(perdida_o_ganancia)+" : "+str(perdida_o_ganancia_id)))    
    def crear_poliza(self, cr, uid, ids, context=None):
      """Crea una poliza con las cuentas de proveedor nacional e internacional
      ,con las cuentas de perdida, ganancia y con las cuentas de los ivas acreditables
      si en algun documento se aplica un iva.
      """
      move_line_id = 0
      data = self.browse(cr, uid, ids)
      obj_move_line = self.pool.get('account.move.line')
      period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
      
      journal_id= self.pool.get('account.journal').search(cr, uid, [('code','=',"VALU")])[0]
      cr.execute("""SELECT ac.id FROM account_account AS ac INNER JOIN account_account_sat_group AS acg ON ac.sat_group_id=acg.id
                            WHERE acg.code='701.01' and ac.code='502-0200'""")
      account_perdida_id = cr.fetchone()[0]
      cr.execute("""SELECT ac.id FROM account_account AS ac INNER JOIN account_account_sat_group AS acg ON ac.sat_group_id=acg.id
                        WHERE acg.code='701.01' and ac.code='407-0000'""")
      account_ganancia_id = cr.fetchone()[0]
      for u in self.pool.get('res.users').browse(cr,uid,uid):
         company_id = u.company_id.id
      poliza_id = self.pool.get('account.move').create(cr, uid, {'journal_id':journal_id,
                                                                 'ref':'Valuacion',
                                                                 'date':datetime.datetime.now(),
                                                                 'company_id':company_id}, context=context)
      self._crear_lineas_polizas(cr, uid, poliza_id, data, context)
      
      #obj_poliza = self.pool.get('account.move').browse(cr, uid, poliza_id,context=context)
      
        
    def tipo_cambio_fecha_documento(self, cr, uid, fecha, rate_id, context=None):
        """Retorna el tipo de cambio que habia a la fecha del documento"""
        cr.execute("""SELECT rate from res_currency_rate WHERE currency_id=%s AND to_char(name,'YYYY-MM-DD')<=%s order by id desc""",(rate_id,fecha,))
        tipo_cambio = cr.fetchone()[0]
        return tipo_cambio
    
    def calcular_valuacion_proveedor(self, cr, uid, ids, context=None):
        """Calcula el monto total que le corresponde a cada proveedor,
        en base a la lista de documentos que se tiene(Facturas,
        Notas de credito y anticipos)"""
        resultado = 0.00
        abs_tax = 0.00
        obj_valuacion_partner = self.pool.get('gv.valuacion.partner')
        data = self.browse(cr, uid, ids)
        cr.execute('''SELECT partner_id,tipo_doc, sum(suma_tipo_cambio_doc), sum(suma_tipo_cambio_sis),sum(impuestos)
                        FROM _gv_valuacion  WHERE gv_valuacion_id=%s
                        GROUP BY partner_id,tipo_doc''',(data.id,))
        total_amount = cr.fetchall()
        for (partner,tipo,sum1, sum2,tax) in total_amount:
            if tipo == 'Factura' or tipo=='Nota cred.':
                if tipo == 'Factura': 
                    obj_valuacion_partner.create(cr, uid,{'fecha':time.strftime('%Y-%m-%d'),
                                                  'partner_id':partner,
                                                  'total_valuacion_doc':sum1,
                                                  'total_valuacion_sis':sum2,
                                                  'resultado': sum2-sum1,
                                                  'resultado_notas':0.00,
                                                  'resultado_anticipos':0.00,
                                                  'impuestos':tax,
                                                  'gv_valuacion_id':data.id,}, context)
                elif tipo == 'Nota cred.':
                    #raise osv.except_osv( ('Alerta!'), ('El ajuste %s no tiene lineas con productos'%tipo))
                    obj_valuacion_partner.create(cr, uid,{'fecha':time.strftime('%Y-%m-%d'),
                                                  'partner_id':partner,
                                                  'total_valuacion_doc':sum1,
                                                  'total_valuacion_sis':sum2,
                                                  'resultado': 0.00,
                                                  'resultado_anticipos':0.00,
                                                  'resultado_notas':sum1-sum2,
                                                  'impuestos':tax,
                                                  'gv_valuacion_id':data.id,}, context)
                    
            elif tipo=='Anticipo':
                    obj_valuacion_partner.create(cr, uid,{'fecha':time.strftime('%Y-%m-%d'),
                                             'partner_id':partner,
                                             'total_valuacion_doc':sum1,
                                             'total_valuacion_sis':sum2,
                                             'resultado': 0.00,
                                             'resultado_anticipos':sum1-sum2,
                                             'resultado_notas':0.00,
                                             'gv_valuacion_id':data.id,}, context)
                    
    
    def get_ammount_voucher(self, cr, uid, voucher, context=None):
        """Retorn el saldo que aun queda pendiente del anticipo"""
        amount_residual = 0.00
        cr.execute("""SELECT COUNT(*) from account_voucher_line AS av INNER JOIN
                   account_move_line AS am ON av.move_line_id=am.id where am.name=%s""",(voucher.number,))
        count = cr.fetchone()[0]
        if count == 0:
            amount_residual = voucher.amount
        elif count == 1 :
            cr.execute("""SELECT amount_original-amount from account_voucher_line AS av INNER JOIN
                   account_move_line AS am ON av.move_line_id=am.id where am.name=%s order by av.id desc""",(voucher.number,))
            amount_residual = cr.fetchone()[0]
        elif count > 1 :
            cr.execute("""SELECT sum(amount), amount_original from account_voucher_line AS av INNER JOIN
                   account_move_line AS am ON av.move_line_id=am.id where am.name=%s group by amount_original""",(voucher.number,))
            resultado = cr.fetchall()
            for (amount, original) in resultado:
                if amount == 0.00:
                    amount_residual = original
                elif amount >0:
                    amount_residual = original-amount
        return amount_residual
    
    def anticipos(self, cr, uid, ids, context=None):
        """Retorna la lista de anticipos  de cada proveedor
        """
        data =self.browse(cr, uid, ids)
        anticpos = ""
        obj_anticipo = self.pool['account.voucher']
        obj_valuacion = self.pool['gv.valuacion']
        obj_currency = self.pool['res.currency']
        obj_valuacion_partner = self.pool['gv.valuacion.partner']
        cr.execute("""SELECT COUNT(av.id) FROM account_voucher AS av 
                        INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                        INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                        WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and av.state='posted'""")
        count = cr.fetchone()[0]
        if count >0 :
            rate_id = obj_currency.search(cr, uid, [('name','=','MXN')])[0]
            rate =  obj_currency.browse(cr, uid, rate_id, context=context)
            cr.execute("""SELECT av.id FROM account_voucher AS av 
                        INNER JOIN account_account AS ac ON av.advance_account_id=ac.id
                        INNER JOIN account_account_sat_group AS sat on ac.sat_group_id=sat.id
                        WHERE sat.code in ('120','120.01','120.02','120.03','120.04') and av.state='posted'""")
            lista_anticipos = cr.fetchall()
            for this in lista_anticipos:
                anticipo = obj_anticipo.browse(cr, uid, this,context)
                amount_voucher = self.get_ammount_voucher(cr, uid, anticipo, context)
                tcambio_fecha = self.tipo_cambio_fecha_documento(cr, uid, anticipo.date, rate_id,context=context)
                if amount_voucher>0:
                    obj_valuacion.create(cr, uid, {'fecha':time.strftime('%Y-%m-%d'),
                                                  'partner_id':anticipo.partner_id.id,
                                                  'saldo_proveedor': anticipo.partner_id.debit,
                                                  'saldo_actual':amount_voucher/tcambio_fecha,#anticipo.amount/tcambio_fecha,
                                                  'suma_tipo_cambio_doc': amount_voucher,
                                                  'suma_tipo_cambio_sis':(amount_voucher/tcambio_fecha)*rate.rate_silent,
                                                  'folio_doc':anticipo.number or '',
                                                  'tipo_doc': 'Anticipo',
                                                  'tipo_cambio_doc':tcambio_fecha,
                                                  'gv_valuacion_id':data.id,})
    
    def get_iva(self, cr, uid, resultado, invoice,context=None):
        """Retorna el iva del resultado final de cada documento"""
        iva = 0.00
        if invoice.tax_line:
            cr.execute("""SELECT tax_percent FROM account_invoice_tax WHERE invoice_id=%s AND name2='IVA'""",(invoice.id,))
            tax_percent = cr.fetchone()[0]
            iva = (resultado*tax_percent)/100
            return iva
            
    def update(self, cr, uid, ids, context=None):
        """Resetea el saldo del proveedor a cero para volver a calcular la valuacion
        en base a los documentos que se tienen"""
        data  = self.browse(cr, uid, ids)
        for this in data.gv_valuacion_ids:
            cr.execute("""UPDATE res_partner SET debit_copy_valuation=0.00 where id=%s""",(this.partner_id.id,))
    
    def set_balance_supplier(self, cr, uid, ids, context=None):
        """Actualiza el saldo del proveedor en base a la lista de documentos
        que se tiene (Facturas, Notas de credito y Anticipos)"""
        data = self.browse(cr, uid, ids)
        new_saldo = 0.00
        obj_partner = self.pool['res.partner']
        resultado = 0.00
        for this in data.gv_valuacion_ids:
            debit_copy_valuation = this.partner_id.debit_copy_valuation
            if this.tipo_doc == 'Factura':#B-A
              if this.partner_id.debit_copy_valuation==0:
                resultado = this.suma_tipo_cambio_sis-this.suma_tipo_cambio_doc
                obj_partner.write(cr, uid, this.partner_id.id,{'debit_copy_valuation':this.saldo_proveedor+resultado},context )
              else:
                resultado = this.suma_tipo_cambio_sis-this.suma_tipo_cambio_doc
                obj_partner.write(cr, uid, this.partner_id.id,{'debit_copy_valuation':debit_copy_valuation+resultado},context )
                
            elif this.tipo_doc == 'Nota cred.':#A-B
                if this.partner_id.debit_copy_valuation==0:
                    resultado = this.suma_tipo_cambio_doc-this.suma_tipo_cambio_sis
                    obj_partner.write(cr, uid, this.partner_id.id,{'debit_copy_valuation':this.saldo_proveedor+resultado},context )
                else:
                    resultado = this.suma_tipo_cambio_doc-this.suma_tipo_cambio_sis
                    obj_partner.write(cr, uid, this.partner_id.id,{'debit_copy_valuation':debit_copy_valuation+resultado},context )
                    
            else: #this.tipo_doc == 'Anticipo': #A-B
                if this.partner_id.debit_copy_valuation==0:
                    resultado = this.suma_tipo_cambio_doc-this.suma_tipo_cambio_sis
                    obj_partner.write(cr, uid, this.partner_id.id,{'debit_copy_valuation':this.saldo_proveedor+resultado},context )
                else:
                    resultado = this.suma_tipo_cambio_doc-this.suma_tipo_cambio_sis
                    obj_partner.write(cr, uid, this.partner_id.id,{'debit_copy_valuation':debit_copy_valuation+resultado},context )
            resultado = 0.00
        
     
    def calcular(self, cr, uid, ids, context=None):
        """Retorna la lista de documentos que tengan un saldo en dolares
        (Facturas y Notas de credito) de cada provedor"""
        data =self.browse(cr, uid, ids)
        tipo_doc = ""
        #cr.execute('''DELETE FROM _gv_valuacion WHERE id!=%s''',(data.id,)) 
        cr.execute("""SELECT COUNT(*) FROM account_invoice AS ai INNER JOIN  res_currency as rc ON ai.currency_id=rc.id 
                      WHERE rc.name='USD' AND (ai.type='in_invoice' or ai.type='in_refund') AND ai.residual>0""")
        count = cr.fetchone()[0]
        saldo1 = 0.00
        saldo2 = 0.00
        obj_partner = self.pool.get('res.partner')
        obj_invoice = self.pool.get('account.invoice')
        obj_valuacion = self.pool.get('gv.valuacion')
        obj_currency = self.pool.get('res.currency')
        if count>1:
            cr.execute("""SELECT ai.id FROM account_invoice AS ai INNER JOIN  res_currency as rc ON ai.currency_id=rc.id 
                          WHERE rc.name='USD' AND (ai.type='in_invoice' or ai.type='in_refund') AND ai.residual>0""")
            list_invoices = cr.fetchall()
            rate_id = obj_currency.search(cr, uid, [('name','=','MXN')])[0]
            rate =  obj_currency.browse(cr, uid, rate_id, context=context)
            self.write(cr,uid,ids,{'tipo_cambio':rate.rate_silent}, context)
            for this in list_invoices:
               invoice = obj_invoice.browse(cr, uid, this, context=context)
               if invoice.currency_rate_alter>0:
                saldo1=invoice.residual*round(invoice.currency_rate_alter,2)
               else :
                    tcambio_fecha = self.tipo_cambio_fecha_documento(cr, uid, invoice.date_invoice, rate_id,context=context)
                    saldo1=tcambio_fecha*invoice.residual
                    #raise osv.except_osv( ('Alerta!'), ('Verifique que los datos sean corectos')+str(saldo1))
               saldo2 = invoice.residual*rate.rate_silent
               if invoice.type=="in_refund":tipo_doc = 'Nota cred.'
               else: tipo_doc = 'Factura'
               iva = self.get_iva(cr, uid, saldo1-saldo2,invoice,context)
               obj_valuacion.create(cr, uid, {'fecha':time.strftime('%Y-%m-%d'),
                                              'partner_id':invoice.partner_id.id,
                                              'saldo_proveedor': invoice.partner_id.debit,
                                              'saldo_actual':invoice.residual,
                                              'suma_tipo_cambio_doc': saldo1 or 0.00,
                                              'suma_tipo_cambio_sis':saldo2 or 0.00,
                                              'impuestos': iva,
                                              'folio_doc':invoice.number or '',
                                              'tipo_doc': tipo_doc,
                                              'tipo_cambio_doc': invoice.currency_rate_alter or 0.00,
                                              'gv_valuacion_id':data.id,})
               saldo1 = 0.00
               saldo2 = 0.00
               #raise ValidationError(str(invoice.number))
            self.anticipos(cr, uid, ids, context=context)
            self.calcular_valuacion_proveedor(cr, uid, ids, context=context)
           
            
    _columns={
        #campos para filtros
        'date_from': fields.date('DE:'),
        'date_to':fields.date('A:'),
        'fecha':fields.date('Fecha'),
        'partner_id': fields.many2one('res.partner','Proveedor'),
        'saldo_proveedor': fields.float('Saldo proveedor(MXN)'),
        'folio_doc': fields.char('Folio'),
        'tipo_cambio_doc': fields.float('Tipo de cambio doc.'),
        'saldo_actual': fields.float('Saldo doc(USD)'),
        'suma_tipo_cambio_doc': fields.float('Saldo doc(USD) x Tipo de cambio doc.', digits_compute= dp.get_precision('Product Price')),
        'suma_tipo_cambio_sis': fields.float('Saldo doc(USD) x Tipo de cambio sistema', digits_compute= dp.get_precision('Product Price')),
        'tipo_doc':fields.char('Tipo doc'),
        'historial': fields.boolean('Ver historial'),
        'tipo_cambio': fields.float('Tipo de cambio sistema'),
        'impuestos': fields.float('IVA'),
        'gv_valuacion_ids':fields.one2many('gv.valuacion','gv_valuacion_id','Valuacion'),
        'gv_valuacion_id':fields.many2one('gv.valuacion','Valuacion'),
        'gv_valuacion_partner_id':fields.one2many('gv.valuacion.partner','gv_valuacion_id','Totales'),
        'gv_valuacion_partner_id_historial':fields.one2many('gv.valuacion.partner','gv_valuacion_id_historial','Historial'),
       
    }
    
 
    def filtrar(self, cr, uid, ids, context=None):
        """Historial de las valuaciones por fechas"""
        data = self.browse(cr, uid, ids)
        if not data.date_from or not data.date_to:raise osv.except_osv( ('Alerta!'), ('Verifique que los datos sean corectos'))
        cr.execute('UPDATE _gv_valuacion_partner_history SET gv_valuacion_id_historial=null')
        cr.commit()
        cr.execute('UPDATE _gv_valuacion_partner_history SET gv_valuacion_id_historial=%s WHERE fecha>=%s AND fecha<=%s',(data.id,data.date_from,data.date_to,))
        cr.commit()
    _defaults = {  
 
        }
valuacion()

class valuacion_partner(osv.osv):
    
    _name = 'gv.valuacion.partner'
    _table = '_gv_valuacion_partner_history'
    _order = 'partner_id'
    _columns = {
        'fecha':fields.date('Fecha'),
        'partner_id': fields.many2one('res.partner','Proveedor'),
        'total_valuacion_doc': fields.float('Total 1'),
        'total_valuacion_sis': fields.float('Total 2'),
        'resultado':fields.float('Monto Facturas'),
        'resultado_notas':fields.float('Monto Notas Cred.'),
        'resultado_anticipos':fields.float('Monto Anticipos'),
        'impuestos': fields.float('IVA'),
        'gv_valuacion_id': fields.many2one('gv.valuacion','Totales'),
        'gv_valuacion_id_historial': fields.many2one('gv.valuacion','Historial')}
    
valuacion_partner()

class res_partner(osv.osv):
    _inherit='res.partner'
    def _get_debit(self, cr, uid, ids, name, arg, context = {} ):
        #try:
            result = {}
            valor = 0.00
            data = self.browse(cr, uid, ids)
            if data.debit_copy==0.00: 
                for record in self.browse( cr, uid, ids, context = context):
                    result[record.id] = mostrar
                return	result
            if data.debit_copy!=0.00:
                pass
        
       #except :
            #return result
    _columns = {
        #'debit_copy': fields.function(_get_debit, string='Total Payable', store=True),
        'debit_copy_valuation': fields.float('saldo Proveedor')}
res_partner()
    
          

