# -*- coding: utf-8 -*-

{
	'name': 'Valuacion',
	'version': '1.0',
	'category': 'Contabilidad',
	'author': 'Israel Cabrera Juarez(gvdeto)',
	'maintainer': '',
	'website': '',
	#'images' : ['ventas/static/images/ventas.jpg'],
	# 'icon': "ventas/static/src/img/icon.png",
	'installable': True, 
	'active': False,
	'description': 'Calcula de valuacion',
	#This model depends of BASE OpeneERP model...
	'depends': [
		'base','account'
	],
	#XML imports
	'data': [
		
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		#:::::::::::::::::::::::::::::::::::::::::::::::::::::::: XML PARA MODELOS DEL SISTEMA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
		#------------------------------------------------------------------------------------------------------------------------------------------------#
		
		
		#------------------------------Devoluciones ----------------#
		'secciones/valuacion/valuacion.xml',
		
		
		
		
		

		
	],
	'css': [
		
	],
	'js': [
		'static/src/js/main.js'
	],
	'application': True,
	'installable': True,
	'auto_install': False,
}
