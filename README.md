![Blackpcs](http://blackpcs.to-do.mx/web/binary/company_logo?dbname=BLKPCprod00)![Qian](http://qian.to-do.mx/web/binary/company_logo?dbname=DIPePROD00)![Todo.Cheap](http://cheap.to-do.mx/web/binary/company_logo?dbname=cheap)

---------

### Introduction
To-do it's a hard fork of [Odoo version 8.0](https://github.com/odoo/odoo/tree/8.0).

###  Companies
[Qian](), [Blackpcs](), [Todo Cheap]() ...

### Contributing
* [See CONTRIBUTING](https://bitbucket.org/pchm/wiki)

### Wiki
* [See WIKI](https://bitbucket.org/pchm/wiki)

### Release
* [See RELEASE](RELEASE)

### Authors
* [See AUTHORS](https://bitbucket.org/pchm/wiki)

### License
* [See LICENSE](LICENSE)

Copyright © 2015-Present, To-Do Developers Authors. All Rights Reserved.
