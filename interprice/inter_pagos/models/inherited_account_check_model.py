# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from openerp import models, fields, api, _


class Check(models.Model):
    """
    Creates new field to 'account.check' model.
    """
    _inherit = ['account.check']

    inter_pago_ids = fields.Many2one(
        string=_('Transferencia Bancaria'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='inter_pagos.pago',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )


class account_check_action(models.TransientModel):
    _inherit = 'account.check.action'

    @api.model
    def _get_inter_pago_id(self):
        active_ids = self._context.get('active_ids', [])
        checks = self.env['account.check'].browse(active_ids)
        return checks.inter_pago_ids

    inter_pago_ids = fields.Many2one(
        string=_('Transferencia Bancaria'),
        default=_get_inter_pago_id,
        comodel_name='inter_pagos.pago',
        ondelete='cascade'
    )

    @api.model
    def get_vals(self, action_type, check, date):
        period = self.env['account.period'].find(
            date)
        if not period:
            raise Warning(_('Not period found for this date'))
        period_id = period.id

        # <Update TODO: Account check improvements to accept other docs.>
        # vou_journal = check.voucher_id.journal_id
        check_journal = check.journal_id
        # <EndUpdate TODO>
        # TODO improove how we get vals, get them in other functions
        if self.action_type == 'deposit':
            ref = _('Deposit Check Nr. ')
            check_move_field = 'deposit_account_move_id'
            journal = self.journal_id
            debit_account_id = self.account_id
            partner = check.source_partner_id.id,
            # <Update TODO: Account check improvements to accept other docs.>
            # credit_account_id = vou_journal.default_credit_account_id.id
            credit_account_id = check_journal.default_credit_account_id
            # <EndUpdate TODO>
            signal = 'holding_deposited'
        elif self.action_type == 'debit':
            ref = _('Debit Check Nr. ')
            check_move_field = 'debit_account_move_id'
            journal = check.checkbook_id.debit_journal_id
            partner = check.destiny_partner_id.id
            credit_account_id = journal.default_debit_account_id
            # <Update TODO: Account check improvements to accept other docs.>
            # debit_account_id = vou_journal.default_credit_account_id.id
            debit_account_id = check_journal.default_credit_account_id
            if self.inter_pago_ids:
                journal = self.journal_id
                credit_account_id = self.inter_pago_ids.account_bridge
                debit_account_id = self.account_id
            # <EndUpdate TODO>
            signal = 'handed_debited'
        elif self.action_type == 'return':
            ref = _('Return Check Nr. ')
            check_move_field = 'return_account_move_id'
            # <Update TODO: Account check improvements to accept other docs.>
            # journal = vou_journal
            journal = check_journal
            # <EndUpdate TODO>
            debit_account_id = (
                check.source_partner_id.property_account_receivable)
            partner = check.source_partner_id.id,
            # <Update TODO: Account check improvements to accept other docs.>
            # credit_account_id = vou_journal.default_credit_account_id.id
            credit_account_id = check_journal.default_credit_account_id
            # <EndUpdate TODO>
            signal = 'holding_returned'

        name = self.env['ir.sequence'].next_by_id(
            journal.sequence_id.id)
        ref += check.name
        currency_rate = 0
        if self.action_type == 'debit':
            if check.inter_pago_ids and self.journal_id.currency.id:
                if self.inter_pago_ids.exchange_rate <= 0:
                    id_obj_rate = self.pool.get('res.currency.rate').search(self._cr, self._uid,
                                                                            [('name', '<=',
                                                                              self.inter_pago_ids.validate_date),
                                                                             ('currency_id', '=', journal.currency.id)],
                                                                            order='name desc')
                    if len(id_obj_rate) > 0:
                        id_rate = self.pool.get('res.currency.rate').browse(self._cr, self._uid, id_obj_rate[0])
                        currency_rate = 1 / id_rate.rate
                else:
                    currency_rate = self.inter_pago_ids.exchange_rate

            elif check.voucher_id and check.currency_id:
                id_obj_rate = self.pool.get('res.currency.rate').search(self._cr, self._uid,
                                                                        [('name', '<=', fields.Datetime.now()),
                                                                         ('currency_id', '=',
                                                                          check.currency_id.id)],
                                                                        order='name desc')
                if len(id_obj_rate) > 0:
                    id_rate = self.pool.get('res.currency.rate').browse(self._cr, self._uid, id_obj_rate[0])
                    currency_rate = 1 / id_rate.rate_sale

            else:
                currency_rate = 1
        elif self.action_type in ('deposit', 'return'):
            if credit_account_id.currency_id.id or debit_account_id.currency_id.id:
                id_obj_rate = self.pool.get('res.currency.rate').search(self._cr, self._uid,
                                                                        [('name', '<=', fields.Datetime.now()),
                                                                         ('currency_id', '=',
                                                                          credit_account_id.currency_id.id or
                                                                          debit_account_id.currency_id.id)],
                                                                        order='name desc')
                if len(id_obj_rate) > 0:
                    id_rate = self.pool.get('res.currency.rate').browse(self._cr, self._uid, id_obj_rate[0])
                    currency_rate = 1 / id_rate.rate
            else:
                currency_rate = 1

        move_vals = {
            'name': name,
            'journal_id': journal.id,
            'period_id': period_id,
            'date': self.date,
            'ref': ref,
        }
        debit_line_vals = {
            'name': name,
            'account_id': debit_account_id.id,
            'partner_id': partner,
            'debit': check.company_currency_amount or check.amount,
            'ref': ref,
            'currency_id': debit_account_id.currency_id.id or False,
            'amount_currency': debit_account_id.currency_id.id or 0,
            'docto_referencia': check.inter_pago_ids.folio or False,
        }
        credit_line_vals = {
            'name': name,
            'account_id': credit_account_id.id,
            'partner_id': partner,
            'credit': check.company_currency_amount or check.amount,
            'ref': ref,
            'currency_id': credit_account_id.currency_id.id or False,
            'amount_currency': credit_account_id.currency_id.id or 0,
            'docto_referencia': check.inter_pago_ids.folio or False,
        }

        if credit_account_id.currency_id:
            if credit_account_id.currency_id.id != credit_account_id.company_id.currency_id.id:
                _amount = 0
                if check.company_currency_amount:
                    _amount = check.amount
                else:
                    if not check.company_currency_amount:
                        _amount = check.amount / currency_rate
                    else:
                        _amount = credit_account_id.company_id.currency_id.compute(check.amount,
                                                                                   credit_account_id.currency_id, True)
                credit_line_vals.update(
                    {'amount_currency': (-1 * _amount),
                     'currency_id': credit_account_id.currency_id.id, })

        if debit_account_id.currency_id:
            if debit_account_id.currency_id.id != debit_account_id.company_id.currency_id.id:
                _amount = 0
                if check.company_currency_amount:
                    _amount = check.amount
                else:
                    if not check.company_currency_amount:
                        _amount = check.amount / currency_rate
                    else:
                        _amount = debit_account_id.company_id.currency_id.compute(check.amount,
                                                                                  debit_account_id.currency_id, True)
                debit_line_vals.update({'amount_currency': _amount,
                                        'currency_id': debit_account_id.currency_id.id, })

        return {
            'move_vals': move_vals,
            'debit_line_vals': debit_line_vals,
            'credit_line_vals': credit_line_vals,
            'check_move_field': check_move_field,
            'signal': signal,
        }
