# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from openerp import models, fields, api, exceptions, _
from datetime import datetime as dt


PAYMENTS = [
    ('TE', _('Transferencia Electrónica')),
    ('CQ', _('Cheque'))
]

STATUS = [
    ('draft', _('Borrador')),
    ('completed', _('Completado')),
    ('canceled', _('Cancelado'))
]

ACCOUNT_BRIDGE_FILTER = [
                        '&',
                        ('type', '!=', 'consolidation'),
                        ('type', '!=', 'view')
                        ]

JOURNAL_FILTER = [
                '&',
                ('default_debit_account_id.id', '!=', False),
                ('default_credit_account_id.id', '!=', False),
                '|',
                ('type', '=', 'bank'),
                ('type', '=', 'cash')
                ]


class Pago(models.Model):
    """
    Base class for modeling payments:
    Electronic Transfers (TE) and Checks (CQ), using a Poliza (account.move).
    """
    _name = 'inter_pagos.pago'
    _description = _('Transferencia bancaria')
    _rec_name = 'folio'
    _inherit = ['mail.thread']

    def _set_currency(self, account):
        if account.default_debit_account_id.currency_id:
            return account.default_debit_account_id.currency_id.id
        else:
            return account.default_debit_account_id.company_currency_id.id

    def get_current_period(self):
        """
        Define the period (month/year) base on the current date.
        """
        date = dt.now().date()
        date_fixed = date.strftime("%m/%Y")
        return self.env['account.period'].search([
                                                 ('name', '=', date_fixed)
                                                 ])

    def get_journals(self, vals):
        #import pdb; pdb.set_trace()
        """
        Search for the journals origin and destiny recorsets.

        Keyword arguments:
        vals -- dictionary, should include {'journal_origin': id(int),
        'journal_destiny': id(int)} at minimum
        """
        if 'journal_origin' in vals:
            journal_origin = self.journal_origin.search([(
                                                        'id',
                                                        '=',
                                                        vals['journal_origin']
                                                        )])
        else:
            journal_origin = self.journal_origin

        if 'journal_destiny' in vals:
            journal_destiny = self.journal_destiny.search([(
                                                        'id',
                                                        '=',
                                                        vals['journal_destiny']
                                                        )])
        else:
            journal_destiny = self.journal_destiny

        return journal_origin, journal_destiny

    def get_accounts(self, vals):
        #~ import pdb; pdb.set_trace()
        """
        Returns the default debit account id of the given journals.

        Keyword arguments:
        vals -- dictionary, should include {'journal_origin': id(int),
        'journal_destiny': id(int)} at minimum
        """
        journal_origin, journal_destiny = self.get_journals(vals)

        return journal_origin.default_debit_account_id, \
            journal_destiny.default_debit_account_id

    def convert_currency_on_change_origin(self, vals, account_origin):
        """
        Calculate a float given in vals['amount'] in the accout_origin
        currency.

        Keyword arguments:
        vals -- Dictionary with should include {'amount': int, 'exchange_rate':
        float, 'currency': id(recorset)} at minimum.
        account_origin -- singleton recordset
        """
        currency = self.env['res.currency'].browse(vals['currency'])

        if currency.name == 'MXN':
            if account_origin.currency_id.name:
                vals['amount'] *= (1/vals['exchange_rate'])
            if not account_origin.currency_id.name:
                vals['amount'] = vals['amount']
        else:
            if account_origin.currency_id.name:
                vals['amount'] = vals['amount']
            if not account_origin.currency_id.name:
                vals['amount'] *= vals['exchange_rate']

        return vals['amount']

    def create_movement(self, vals, process_state='draft'):
        """
        Creates object account.move base on vals['payment_type'].

        Keyword arguments:
        vals -- dictionary, should include {'journal_origin': id(int),
                'journal_destiny': id(int), 'payment_type': 'TE' | 'CQ',
                'folio': 'str'(sequence)  } at minimum,
        process_state -- if given define the status of the object in matter
        """
        movement = self.env['account.move']

        # Create movement headers for Electronic Transfer (account.move)
        if vals['payment_type'] == 'TE':
            movement_created = movement.create({
                       'create_uid': self.env.user.id,
                       'date': fields.datetime.now(),
                       'journal_id': vals['journal_origin'],
                       'period_id': self.get_current_period().id,
                       'ref': vals['folio'],
                       'state': process_state
                       })
            return movement_created
        # Create movement headers for check payment (account.move)
        elif vals['payment_type'] == 'CQ' and vals['journal_origin_checkbook']:
            movement_created = movement.create({
                       'create_uid': self.env.user.id,
                       'date': fields.datetime.now(),
                       'journal_id': vals['journal_origin'],
                       'period_id': self.get_current_period().id,
                       'ref': vals['folio'],
                       'state': process_state
                       })
            return movement_created
        else:
            return None

    def set_moveline_fields(self,
                            vals,
                            movement_created_id,
                            movement_created_name):
        """
        Creates a dictionary with all fields to create an object type
        'move.line'.

        This will also handle all logic involved with currencies and
        process_state
        Keyword arguments:
        vals -- All the values of the form
        movement_created_id -- ID of the 'account.move' object created to link
                            this new possible object,
        movement_created_name -- name of 'account.move' object created to link
                            this new possible object
        """
        account_origin, account_destiny = self.get_accounts(vals)

    # Overwrite the accounts if need to use 'account_bridge'

        # Payments with CQ on generation should use and account bridge dst-bri
        if vals.get('generation', False) and vals['account_bridge']:
            account_destiny = self.env['account.account'].browse(
                                                        vals['account_bridge']
                                                        )
            # Amount currency should be defined by account_origin
            vals['amount'] = self.convert_currency_on_change_origin(
                                                                vals,
                                                                account_origin
                                                                )

        # Payments with CQ on canceling should use the bridge account org-bri
        if vals['account_bridge'] and vals.get('canceling', False) == \
                'org-bri':
            account_destiny = account_origin
            account_origin = self.env['account.account'].browse(
                                                        vals['account_bridge']
                                                        )
            # Amount currency should be defined by account_origin
            vals['amount'] = self.convert_currency_on_change_origin(
                                                                vals,
                                                                account_origin
                                                                )

        line_credit = {
                    'account_id': account_origin.id,
                    'move_id': movement_created_id,
                    'name': movement_created_name,
                    'period_id': self.get_current_period().id,
                    'docto_referencia': self.folio,
                    }

        line_debit = {
                    'account_id': account_destiny.id,
                    'move_id': movement_created_id,
                    'name': '/',
                    'period_id': self.get_current_period().id,
                    'docto_referencia': self.folio,
                    }

        if account_origin.currency_id.name:
            line_credit['credit'] = vals['amount'] * vals['exchange_rate']
            line_credit['amount_currency'] = vals['amount'] * -1
            line_credit['currency_id'] = account_origin.currency_id.id
        else:
            line_credit['credit'] = vals['amount']
            if account_destiny.currency_id.name:
                line_credit['credit'] = vals['amount']
                line_credit['amount_currency'] = vals['amount'] * (1/vals['exchange_rate']) * -1
                line_credit['currency_id'] = account_destiny.currency_id.id

        if account_destiny.currency_id.name:
            if not account_origin.currency_id.name:
                line_debit['debit'] = vals['amount']
                line_debit['amount_currency'] = vals['amount'] * 1/vals['exchange_rate']
                line_debit['currency_id'] = account_destiny.currency_id.id
            else:
                line_debit['debit'] = vals['amount'] * vals['exchange_rate']
                line_debit['amount_currency'] = vals['amount']
                line_debit['currency_id'] = account_destiny.currency_id.id
        else:
            if not account_origin.currency_id.name:
                line_debit['debit'] = vals['amount']
            else:
                line_debit['debit'] = vals['amount'] * vals['exchange_rate']
                line_debit['amount_currency'] = vals['amount']
                line_debit['currency_id'] = account_origin.currency_id.id

        return line_debit, line_credit

    def _set_balance(self, account):
        """
        Sets balance for a given account
        """
        if type(self.env['account.journal']) == type(account):
            account_debit = account.default_debit_account_id

        elif type(self.env['account.account']) == type(account):
            account_debit = account

        else:
            return None

        if not account_debit.currency_id.name:
            balance = account_debit.balance
        else:
            move_lines = self.env['account.move.line'].search([
                                                 ('account_id',
                                                  '=',
                                                  account_debit.name)
                                                 ])
            credit_total = 0.0
            debit_total = 0.0
            for line in move_lines:
                if line.amount_currency:
                    if line.credit:
                        credit_total += abs(line.amount_currency)
                    elif line.debit:
                        debit_total += abs(line.amount_currency)
                # This lines calculate the amount_currency to USD with the
                # correct exchage rate, base on the last modification date ( .write_date)
                # could it be the best way to do it with .create_date?

                # line_date = dt.strptime(line.write_date, '%Y-%m-%d %H:%M:%S')
                # for i, rate_id in enumerate(account_debit.currency_id.rate_ids):
                #     if i == 0:
                #         rate_id_date = dt.now()
                #         rate_id_date_prev = dt.strptime(rate_id.name, '%Y-%m-%d %H:%M:%S')
                #     else:
                #         rate_id_date = dt.strptime(account_debit.currency_id.rate_ids[i-1].name,
                #                                    '%Y-%m-%d %H:%M:%S')
                #         rate_id_date_prev = dt.strptime(rate_id.name, '%Y-%m-%d %H:%M:%S')

                #     if line_date <= rate_id_date and line_date >= rate_id_date_prev:
                #         credit += line.credit * rate_id.rate
                #         debit += line.debit * rate_id.rate
                #         break
            balance = debit_total - credit_total
        return balance

    def set_account_journal(self, vals):
        #import pdb; pdb.set_trace()
        if 'account_bridge' in vals:
            account_bridge_balance = self._set_balance(self.env['account.account'].browse(
                                            vals['account_bridge']))
            account_bridge_currency = self.env['account.account'].browse(
                                            vals['account_bridge']).currency_id.id or \
                                            self.env['account.account'].browse(
                                            vals['account_bridge']).company_currency_id.id
        else:
            account_bridge_balance = self._set_balance(self.env['account.account'].browse(
                                            self.account_bridge.id))
            account_bridge_currency = self.env['account.account'].browse(
                                            self.account_bridge.id).currency_id.id or \
                                            self.env['account.account'].browse(
                                            self.account_bridge.id).company_currency_id.id
#~
        if 'journal_origin' in vals:
            journal_origin_currency = self._set_currency(
                                                        self.env['account.journal'].
                                                        browse(vals['journal_origin']))
        else:
            journal_origin_currency = self._set_currency(
                                                        self.env['account.journal'].
                                                        browse(self.journal_origin.id))
#~
        if 'journal_destiny' in vals:
            journal_destiny_currency = self._set_currency(
                                                        self.env['account.journal'].
                                                        browse(vals['journal_destiny']))
        else:
            journal_destiny_currency = self._set_currency(
                                                        self.env['account.journal'].
                                                        browse(self.journal_destiny.id))

        return account_bridge_balance, account_bridge_currency,\
            journal_origin_currency, journal_destiny_currency

    @api.model
    def create(self, vals):
        #~ import pdb; pdb.set_trace()
        """
        Processes missed fields.

        Keyword arguments:
        vals -- dictionary to super original create
        """
        vals['payment_date'] = vals.get('payment_date', False) or fields.datetime.now()

        vals['folio'] = self.env['ir.sequence'].next_by_code(
                                                'inter_pagos.pago.folio'
                                                )

        vals['comments'] = vals.get('comments', False) or ''

        vals['period_id'] = self.get_current_period().id

        account_origin, account_destiny = self.get_accounts(vals)

        # Balances aren't passed to vals 'cos marked as read only on view
        vals['journal_origin_diary_balance'] = self._set_balance(account_origin)
        vals['journal_destiny_diary_balance'] = self._set_balance(account_destiny)

        account_bridge_balance, account_bridge_currency, journal_origin_currency,\
            journal_destiny_currency = self.set_account_journal(vals)

        vals['account_bridge_balance'] = account_bridge_balance
        vals['account_bridge_currency'] = account_bridge_currency
        vals['currency'] = account_origin.currency_id.id or \
                            account_origin.company_id.currency_id.id
        vals['journal_origin_currency'] = journal_origin_currency
        vals['journal_destiny_currency'] = journal_destiny_currency


        # Draft status has to be updated
        vals['process_state'] = 'draft'
        return super(models.Model, self).create(vals)

    @api.one
    def write(self, vals):
        #~ import pdb; pdb.set_trace()

        account_origin, account_destiny = self.get_accounts(vals)

        # Balances aren't passed to vals 'cos marked as read only on view
        vals['journal_origin_diary_balance'] = self._set_balance(account_origin)
        vals['journal_destiny_diary_balance'] = self._set_balance(account_destiny)

        account_bridge_balance, account_bridge_currency, journal_origin_currency,\
            journal_destiny_currency = self.set_account_journal(vals)

        vals['account_bridge_balance'] = account_bridge_balance
        vals['account_bridge_currency'] = account_bridge_currency
        vals['journal_origin_currency'] = journal_origin_currency
        vals['journal_destiny_currency'] = journal_destiny_currency

        vals['currency'] = account_origin.currency_id.id or \
                             account_origin.company_id.currency_id.id

        #~ # Draft status has to be updated
        return super(Pago, self).write(vals)


    @api.onchange('journal_origin')
    def _set_journal_origin_info(self):
        """
        Sets default values on changes at origin account.
        """
        # Resetting values
        self.payment_type = 'TE'
        self.journal_origin_checkbook = False

        # Move currency needs to be shown in the view
        # account.account.company_currency
        self.currency = self._set_currency(self.journal_origin)

        # The exchange rated should be show in view
        self.exchange_rate = 1 / self.env['res.currency'].search(
                    [('name', '=', 'USD')]).rate_silent

        # Journal currency should be shown
        self.journal_origin_currency = self._set_currency(self.journal_origin)

        self.journal_origin_diary_balance = self._set_balance(self.journal_origin)

        self.journal_origin_checkbook = False

        # If account has not checkbooks TE should be the default payment
        if not self.journal_origin.checkbook_ids or \
                self.journal_origin.payment_subtype != u'issue_check':

            self.payment_type = 'TE'
            self.journal_origin_checkbook = False

        _journal_filter = list(JOURNAL_FILTER)
        _account_bridge_filter = list(ACCOUNT_BRIDGE_FILTER)
        # if self.journal_origin.default_debit_account_id.currency_id.id:
        #     _journal_filter.insert(0, ('default_debit_account_id.currency_id.id', '=',
        #                                self.journal_origin.default_debit_account_id.currency_id.id))
        #     _account_bridge_filter.insert(0, ('currency_id.id', '=',
        #                                       self.journal_origin.default_debit_account_id.currency_id.id))
        #
        # elif self.currency.name and self.currency.name == self.journal_origin.default_debit_account_id.company_currency_id.name:
        #     _journal_filter.insert(0, ('default_debit_account_id.currency_id', '=', False))
        #     _account_bridge_filter.insert(0, ('currency_id', '=', False))

        return {'domain': {
                'journal_destiny': _journal_filter,
                'account_bridge': _account_bridge_filter
                }}

    @api.onchange('journal_origin_checkbook')
    def _set_journal_origin_checkbooks_check_number(self):
        """
        Sets the number of the next check avalable in the view.
        """
        checkbook = self.env['account.checkbook'].\
            browse(self.journal_origin_checkbook.id)

        self.check_number_label = checkbook.next_check_number
        self.payment_date = False

    @api.onchange('payment_type')
    def _set_journal_origin_checkbooks(self):
        """
        Sets checkbooks for the journal in matter.

        if not journal checkbook available, going back to electronic transfer.
        """
        # Account bridge values should be reset
        self.account_bridge_balance = False
        self.account_bridge = False

        self.journal_destiny = False
        self.journal_destiny_diary_balance = False

        if self.payment_type == 'CQ' and \
                self.journal_origin.payment_subtype == u'issue_check':

            check_ids = [check.id for check in self.journal_origin.
                        checkbook_ids if check.id]

            return {'domain': {'journal_origin_checkbook': [
                        ('id', 'in', check_ids), ('state', '=', 'active')
                    ]
                }
            }
        else:
            self.payment_type = 'TE'
            self.journal_origin_checkbook = False

    @api.onchange('journal_destiny')
    def _set_journal_destiny_info(self):
        # Journal currency should be shown
        self.journal_destiny_currency = self._set_currency(self.journal_destiny)

        self.journal_destiny_diary_balance = self._set_balance(self.journal_destiny)

    @api.onchange('account_bridge')
    def _set_account_bridge_info(self):
        # Account currency should be shown
        if self.account_bridge.currency_id.id:
            self.account_bridge_currency = self.account_bridge.currency_id.id
        else:
            self.account_bridge_currency = self.account_bridge.\
                                            company_currency_id.id
        self.account_bridge_balance = self._set_balance(self.account_bridge)

    # Actions for validate button
    @api.one
    def validate_process_state(self):
        #~ import pdb; pdb.set_trace()
        """
        Creates 'account.move', 'move.line' objects and associate them.

        Requirements:
        self._context -- need to receive all values from the associated
                        form view context attribute.
        Note:
        some values are not passed in self._context 'cos read-only property,
        those need to be recalculated
        """
        vals = dict(self._context)

        movement_created = self.create_movement(vals)
        # account.move has to posted (change status)
        movement_created.post()
        vals['movement'] = movement_created.id

        # Custom actions should be executed depending on the payment type
        # For check (CQ)...
        if vals['journal_origin_checkbook'] and vals['payment_type'] == 'CQ':
            vals['generation'] = True
            # Check number has to be recalculated 'cos isn't pass in the vals
            # dict.
            checkbook = self.env['account.checkbook'].\
                browse(vals['journal_origin_checkbook'])

            check = self.env['account.check']

            # If the Journal destiny has forgin currency
            # 'amount' should be given in the forgin currency
            account_origin, account_destiny = self.get_accounts(vals)
            vals['amount'] = self.amount
            # A check must be genereted to link in
            check_created = check.create({
                'inter_pago_ids': self.id,
                'checkbook_id': vals['journal_origin_checkbook'],
                'amount': vals['amount'],
                'company_currency_amount': vals['amount'] *
                vals['exchange_rate'] if account_origin.currency_id.name
                else 0,

                'number': checkbook.next_check_number,
                'payment_date': vals['payment_date'] or fields.datetime.now(),
                'validate_date': fields.datetime.now(),

                # 'voucher_id': 1  # This has been ovewrite check
                # commits 5926688 and 7b50ce0 on 'test' branch.
                # Now'other_partner_id' Needed if no voucher given
                'other_partner_id': self.env.user.company_id.partner_id.id,
                #'type': 'issue_check',
                'issue_date': fields.Date.context_today(self),
                'issue_check_subtype': checkbook.issue_check_subtype,
                'journal_id': self.journal_origin.id,
                'currency_id': account_origin.currency_id.id or False,
                'company_id': self.env['res.company']._company_default_get(
                                                        'inter_pagos.pago'
                                                        )
                        })

            # Check should be in status 'handed' (entregado)
            # check_created.action_hand()
            check_created.signal_workflow("draft_router")
            vals['check_number'] = check_created.id
            self.check_number = check_created.id
            check_created.type = self.journal_origin.payment_subtype

        line_debit, line_credit = self.set_moveline_fields(
                                                  vals,
                                                  movement_created.id,
                                                  movement_created.name)
        movement_line = self.env['account.move.line']
        movement_line.create(line_debit)
        movement_line.create(line_credit)

        # Folio is not pass in self._context
        self.update({'movement': movement_created.id,
                     'folio': self.folio,
                     'process_state': 'completed',
                     'payment_date': fields.datetime.now(),
                     'validate_date': fields.datetime.now()})

    @api.multi
    def cancel_process_state(self):
        """
        Creates a Poliza from the destiny journal to the origin journal or
        bridge account.

        Requirements:
        self._context -- need to receive all values from the associated
                        form view context attribute.
        Note:
        some values are not passed in self._context 'cos read-only property,
        those need to be recalculated
        """
        vals = dict(self._context)
        journal_origin, journal_destiny = self.get_journals(vals)

        if vals['payment_type'] == 'TE':
            vals['folio'] = self.folio + ' Transferencia cancelada ' + self.movement.name
            movement_created = self.create_movement(vals)
            movement_created.post()
            vals['movement'] = movement_created.id
            # Inverting origin vs destiny
            vals['journal_origin'] = journal_destiny.id
            vals['journal_destiny'] = journal_origin.id

            # If there are diferent currencies between origin and destiny
            # from the _context, amount currency should be converted too
            # in order to complain with odoo constrains.
            if journal_origin.default_debit_account_id.currency_id.name \
                and not journal_destiny.default_debit_account_id.\
                    currency_id.name:

                vals['amount'] = vals['amount'] * vals['exchange_rate']

            if not journal_origin.default_debit_account_id.currency_id.name \
                    and journal_destiny.default_debit_account_id.currency_id.\
                    name:

                vals['amount'] = vals['amount'] * (1/vals['exchange_rate'])

            line_debit, line_credit = self.set_moveline_fields(
                                                        vals,
                                                        movement_created.id,
                                                        movement_created.name)

            movement_line = self.env['account.move.line']
            movement_line.create(line_debit)
            movement_line.create(line_credit)

        if vals['payment_type'] == 'CQ':
            # Check cancel poliza shoul be only created if status
            # is 'draft' or 'handed'
            check_issued = self.env['account.check'].browse(
                                    self.check_number.id
                                    )
            if check_issued.state not in ['draft', 'handed']:
                raise exceptions.ValidationError(_("El cheque no puede ser \
                                    cancelado, debido a su estado actual"))
                return None

            # Genereting Poliza from bridge set on form to origin set on form
            vals = dict(self._context)

            vals['folio'] = self.folio + ' Transferencia cancelada ' + self.movement.name
            vals['canceling'] = 'org-bri'
            movement_created = self.create_movement(vals)
            movement_created.post()
            vals['movement'] = movement_created.id

            line_debit, line_credit = self.set_moveline_fields(
                                                        vals,
                                                        movement_created.id,
                                                        movement_created.name)
            movement_line = self.env['account.move.line']
            movement_line.create(line_debit)
            movement_line.create(line_credit)

        self.process_state = 'canceled'
        self.canceled = movement_created.id
        view_id = self.env.ref('inter_pagos.pago_iuv_canceled_form_view').id

        context = self._context.copy()

        return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'inter_pagos.pago',
                'target': 'current',
                'flags': {'initial_mode': 'edit'},
                'res_id': self.id,
                'readonly': False,
                'type': 'ir.actions.act_window',
                'views': [(view_id, 'form')],
                'view_id': view_id,
                'context': context
            }

    @api.one
    @api.constrains('amount')
    def _validated_amount_(self):
        """
        Amount should be flot or int and bigger than cero.
        """
        if type(self.amount) != float:
            raise exceptions.ValidationError(
                                        _("El monto debe ser tipo numérico")
                                        )

        if self.amount <= 0.00:
            raise exceptions.ValidationError(
                                        _("El monto debe ser mayor a cero")
                                        )

    @api.one
    @api.constrains('journal_origin')
    def _validated_accounts(self):
        """
        Amount should be flot or int and bigger than cero.
        """
        if self.journal_origin == self.journal_destiny:
            raise exceptions.ValidationError(
                _("Por favor seleccione diario origen y destino diferentes")
                )
        elif self.journal_origin.default_debit_account_id == \
                self.account_bridge:

            raise exceptions.ValidationError(
                _("Por favor seleccione diario origen y cuenta puente \
                   diferentes")
                )

    @api.one
    @api.constrains('exchange_rate')
    def _validated_exchange_rate_(self):
        """
        Exchange_rate should be flot and bigger than cero.
        """
        if type(self.exchange_rate) != float:
            raise exceptions.ValidationError(
                                _("El tipo de cambio debe ser tipo numérico")
                                )

        if self.amount <= 0.00:
            raise exceptions.ValidationError(
                                    _("El monto debe ser mayor a cero")
                                    )

        if self.exchange_rate <= 0.00:
            raise exceptions.ValidationError(
                                _("El tipo de cambio debe ser mayor a cero")
                                )

    @api.depends('journal_origin')
    def _compute_hide_exchange_rate_field(self):
        if self.journal_origin.default_debit_account_id.currency_id.name:
            self.hide_field_exchange_rate = False
        else:
            self.hide_field_exchange_rate = True

    @api.one
    def unlink(self):
        if not self.process_state == 'draft':
            raise exceptions.Warning(
                _("No puede eliminar una transferencia bancaria "
                  "después de que ha sido validada o cancelada, "
                  "(está relacionada con una póliza)."))

        return super(Pago, self).unlink()

    folio = fields.Char(
        string=_('Folio'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        size=50,
        translate=True,
        copy=False,
    )
    journal_origin = fields.Many2one(
        string=_('Diario origen'),
        required=True,
        readonly=False,
        index=True,
        default=None,
        help=False,
        comodel_name='account.journal',
        domain=JOURNAL_FILTER,
        context={},
        ondelete='cascade',
        auto_join=False
    )
    journal_origin_diary_balance = fields.Float(
        string=_('Saldo'),
        required=False,
        readonly=True,
        index=False,
        default=0.0,
        digits=(16, 2),
        help=False
    )
    journal_origin_currency = fields.Many2one(
        string=_('Moneda'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='res.currency',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
    payment_type = fields.Selection(
        string=_('Tipo de pago'),
        required=True,
        readonly=False,
        index=False,
        default='TE',
        help=False,
        selection=PAYMENTS
    )
    journal_origin_checkbook = fields.Many2one(
        string=_('Cuenta de cheques'),
        required=False,
        readonly=False,
        index=True,
        default=None,
        help=False,
        comodel_name='account.checkbook',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
    journal_destiny = fields.Many2one(
        string=_('Diario destino'),
        required=False,
        readonly=False,
        index=True,
        default=None,
        help=False,
        comodel_name='account.journal',
        domain=JOURNAL_FILTER,
        context={},
        ondelete='cascade',
        auto_join=False
    )
    journal_destiny_diary_balance = fields.Float(
        string=_('Saldo'),
        required=False,
        readonly=True,
        index=False,
        default=0.0,
        digits=(16, 2),
        help=False
    )
    journal_destiny_currency = fields.Many2one(
        string=_('Moneda'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='res.currency',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
    account_bridge = fields.Many2one(
        string=_('Cuenta puente'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='account.account',
        domain=ACCOUNT_BRIDGE_FILTER,
        context={},
        ondelete='cascade',
        auto_join=False
    )
    account_bridge_balance = fields.Float(
        string=_('Saldo'),
        required=False,
        readonly=True,
        index=False,
        default=0.0,
        digits=(16, 2),
        help=False
    )
    account_bridge_currency = fields.Many2one(
        string=_('Moneda'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='res.currency',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
    exchange_rate = fields.Float(
        string=_('Tipo de Cambio'),
        required=True,
        readonly=False,
        index=False,
        default=0.1234,
        digits=(8, 4),
        help='Peso a Dollar'
    )
    amount = fields.Float(
        string=_('Monto'),
        required=True,
        readonly=False,
        index=False,
        default=0,
        digits=(26, 2),
        help=False
    )
    comments = fields.Text(
        string=_('Observaciones'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=_('Por favor ingresa los comentarios...'),
        translate=True,
        copy=False,
    )
    process_state = fields.Selection(
        string=_('Estado'),
        required=True,
        readonly=False,
        index=False,
        default='draft',
        help=False,
        selection=STATUS,
        track_visibility='onchange'
    )
    payment_date = fields.Date(
        string=_('Fecha de pago'),
        required=False,
        readonly=False,
        index=False,
        default=fields.datetime.now(),
        help=False,
    )

    period_id = fields.Many2one(
        string=_('Periodo contable'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='account.period',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False,
    )
    movement = fields.Many2one(
        string=_('Póliza'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='account.move',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False,
        track_visibility='onchange',
        copy=False
    )
    currency = fields.Many2one(
        string=_('Moneda'),
        required=True,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='res.currency',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
    canceled = fields.Many2one(
        string=_('Cancelado'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='account.move',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False,
        track_visibility='onchange',
        copy=False,
    )
    check_number = fields.Many2one(
        string=_('Cheque usado'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        comodel_name='account.check',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False,
        copy=False
    )
    check_number_label = fields.Char(
        string=_('Siguiente cheque'),
        required=False,
        readonly=True,
        index=False,
        default=None,
        help=False,
        size=50,
        translate=True
    )
    hide_field_exchange_rate = fields.Boolean(
        string='hide_field_exchange_rate',
        required=False,
        readonly=True,
        index=False,
        default=True,
        compute='_compute_hide_exchange_rate_field'
    )

    validate_date = fields.Datetime(
        string=_('Fecha de validación'),
        required=False,
        readonly=False,
        index=False,
        help=False
    )

