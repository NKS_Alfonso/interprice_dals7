# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation
{
    'name': "Transferencias Bancarias",

    'summary': """
        Modulo de control de pagos multi cuenta y multi moneda.
        """,
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account_check', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/inter_pagos_view.xml',
        'views/inter_pagos_sequence_folio.xml',
        'views/inherited_account_check_model_view.xml'
    ]
}
