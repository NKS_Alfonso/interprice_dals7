Transferencias bancarias
------------------------

Modulo de control de pagos por transferencia electrónica y chequera,
entre cuentas locales o externas, multi moneda.


Ubicación del menu

.. figure:: ../inter_pagos/static/img/transference_menu.png
    :alt: Menu impresión de póliza
    :width: 80%

Transferencia electrónica

.. figure:: ../inter_pagos/static/img/transference.png
    :alt: Menu impresión de póliza
    :width: 80%

Póliza transferencia electrónica

.. figure:: ../inter_pagos/static/img/transference_policy.png
    :alt: Menu impresión de póliza
    :width: 80%

Transferencia por cheque

.. figure:: ../inter_pagos/static/img/check.png
    :alt: Menu impresión de póliza
    :width: 80%

Póliza transferencia por cheque

.. figure:: ../inter_pagos/static/img/check_policy.png
    :alt: Menu impresión de póliza
    :width: 80%