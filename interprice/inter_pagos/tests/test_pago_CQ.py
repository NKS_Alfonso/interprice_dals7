# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from openerp.tests.common import TransactionCase


class TestPagoCQ(TransactionCase):
    """ This class contains the unit tests for 'test.pago.CQ'.

        Tests:
          - test_create_pago: Checks if 'create' works properly
    """

    def setUp(self):
        super(TestPagoCQ, self).setUp()
        pago_model = self.env['inter_pagos.pago'].sudo(
            self.ref('base.user_demo')
            )
        vals = {
            'payment_date': False,
            'process_state': 'draft',
            'comments': 'Blah',
            'journal_origin': 12,
            'exchange_rate': 20.732300000000002,
            'amount': 100,
            'journal_destiny': False,
            'account_bridge': 415,
            'journal_origin_checkbook': 2,
            'payment_type': 'CQ'
        }
        self.pago_te = pago_model.create(vals)

    def test_create_pago(self):
        """ Checks if the create works properly """
        self.assertEqual(self.pago_te.process_state,
                         'generated', "CQ Pago can't be created")
