# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from openerp.tests.common import TransactionCase


class TestPagoTE(TransactionCase):
    """ This class contains the unit tests for 'test.pago.TE'.

        Tests:
          - test_create_pago: Checks if 'create' works properly
    """

    def setUp(self):
        super(TestPagoTE, self).setUp()
        pago_model = self.env['inter_pagos.pago'].sudo(
            self.ref('base.user_demo')
            )
        vals = {
            'payment_date': False,
            'process_state': 'draft',
            'comments': 'Blah...',
            'journal_origin': 26,
            'exchange_rate': 20.732300000000002,
            'amount': 100, 'journal_destiny': 27,
            'account_bridge': False,
            'journal_origin_checkbook': False,
            'payment_type': 'TE'
        }
        self.pago_te = pago_model.create(vals)

    def test_create_pago(self):
        """ Checks if the create works properly """
        self.assertEqual(self.pago_te.process_state,
                         'generated', "TE Pago can't be created")
