# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import _, api, exceptions, fields, models
from datetime import datetime, timedelta
from dateutil.parser import parse


class LockUnlockReportClientsWizard(models.TransientModel):
    """
    """
    # _inherit = 'lock.unlock.client'
    _name = 'lock.unlock.clients.report.wizard'

    # Fields
    status_client_parameter = fields.Selection([
        ('locked', _("Locked")),
        ('unlocked', _("Activated")),
        # ('returned_check', _("Returned check"))
        ],
        string=_('Status parameter'),
        help=_('State of client to consider for the filter')
    )

    start_date = fields.Datetime(
        string=_('Start date'),
        required=True,
        help=_('Start date to consider for the filter')
    )

    end_date = fields.Datetime(
        string=_('End date'),
        required=True,
        help=_('End date to consider for the filter')
    )

    no_client_start = fields.Integer(
        default='',
        string=_('From:'),
        help=_("Start of the range to apply the blockade"),
    )
    no_client_end = fields.Integer(
        default='',
        string=_('To:'),
        help=_("End of the range to apply the blockade"),
    )

    def _get_ids(self):
        # Get filter values
        s_date = self.start_date
        e_date = self.end_date
        client_start = self.no_client_start
        client_end = self.no_client_end
        status_client = self.status_client_parameter
        lock_unlock_ids = []

        query = "SELECT luc.id FROM lock_unlock_client luc\n"

        if s_date:
            query += "\tWHERE luc.lock_unlock_date " +\
            ">= CAST('%s' AS TIMESTAMP)\n" % (s_date)
        if e_date:
            query += "\tAND luc.lock_unlock_date "+\
            "<= CAST('%s' AS TIMESTAMP)\n" % (e_date)
        if client_start:
            query += "\tAND CAST(luc.no_client as INT) "+\
            "BETWEEN %s\n" % (client_start)
        if client_end:
            query += "\tAND %s\n" % (client_end)
        if status_client:
            query += "\tAND luc.state = '%s'\n" % (status_client)
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            lock_unlock_ids = [ids[0] for ids in self.env.cr.fetchall()]
        else:
            raise exceptions.Warning(_("No records found!"))

        return lock_unlock_ids

    @api.multi
    def screen(self):
        ids = self._get_ids()
        view_ref = self.env['ir.model.data'].get_object_reference(
            'clients_administration', 'view_tree_to_lock_unlock_client')
        view_id = view_ref and view_ref[1] or False
        return {
            'name': 'Reporte de clientes',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': 'lock.unlock.client',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', ids)],
            'editable': False,
            'target': 'current',
            'view_id': view_id,
            'context': '{"start_date": "%s", "end_date": "%s"}' %\
             (self.start_date,self.end_date),
        }

    @api.multi
    def show_pdf(self):
        return self.env['report'].get_action(
            self.env['lock.unlock.client'].browse(self._get_ids()),
            'clients_administration.locked_clients_massive_report_pdf'
        )

    @api.multi
    def show_xls(self):
        ids = self._get_ids()
        context = {'start_date': self.start_date, 'end_date': self.end_date}
        csv_id = self.csv_by_ids(ids, context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
            'context': '{"start_date":"%s", "end_date":"%s"}' %\
            (self.start_date, self.end_date),
        }

    def csv_by_ids(self, _ids, _context):
        data = self.env['lock.unlock.client'].\
                with_context(start_date=_context.get('start_date'),
                             end_date=_context.get('end_date')).browse(_ids)
        data_csv = [
            [
                _('No client'),
                _('Folio'),
                _('Client'),
                _('Status'),
                _('Date'),
                _('Cause lock'),
                _('Cause Activated'),
                _('User'),
            ]
        ]
        for row in data:
            data_csv.append([
                unicode(
                    self._set_default(
                        row.no_client)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.internal_folio)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.partner_id.name)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.state)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.lock_unlock_date)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.lock_cause.name)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.unlock_cause.name)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.user)).encode('utf8')
            ])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'LockUnlockClientsReport.csv',
                'type_file': 'csv',
                'file': file,
                })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
