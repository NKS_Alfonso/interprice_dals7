# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import _, api, exceptions, fields, models
from datetime import datetime, timedelta
from dateutil.parser import parse


class LockUnlockReportClientsWizard(models.TransientModel):
    """
    """
    # _inherit = 'lock.unlock.client'
    _name = 'print.lock.unlock.clients.report.wizard'

    def _get_ids(self):
        # Get filter values
        lock_unlock_ids = []
        if self._context.get('active_ids'):
            active_ids = self._context.get('active_ids')
            for ids in active_ids:
                lock_unlock_ids.append(ids)

        return lock_unlock_ids


    @api.multi
    def show_xls(self):
        ids = self._get_ids()
        csv_id = self.csv_by_ids(ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    def csv_by_ids(self, _ids):
        data = self.env['lock.unlock.client'].browse(_ids)
        data_csv = [
            [
                _('No client'),
                _('Folio'),
                _('Client'),
                _('Status'),
                _('Date'),
                _('Cause lock'),
                _('Cause of Activation'),
                _('User'),
            ]
        ]


        for row in data:
            if row.state == 'unlocked':
                state = 'Activo'
            else:
                state = 'Bloqueado'
            data_csv.append([
                unicode(
                    self._set_default(
                        row.no_client)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.internal_folio)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.partner_id.name)).encode('utf8'),
                unicode(
                    self._set_default(
                        state)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.lock_unlock_date)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.lock_cause.name)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.unlock_cause.name)).encode('utf8'),
                unicode(
                    self._set_default(
                        row.user)).encode('utf8')
            ])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'LockUnlockClientsReport.csv',
                'type_file': 'csv',
                'file': file,
                })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
