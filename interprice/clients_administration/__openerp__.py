# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
{
    'name': "Administration of clients (Lock/Unlock)",

    'summary': """
        Module to help on lock and unlock of clients.
    """,
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': '',
    'version': '0.1',

    # Module necessary for this one to work correctly
    'depends': [
        'base',
        'mail',
        'account',
        'sale',
    ],

    # always loaded
    'data': [
        'security/menu_lock_client_access.xml',
        'security/ir.model.access.csv',
        'data/catalogue_of_causes_data.xml',
        'data/document_sequence_lock.xml',
        'views/lock_or_unlock_client_view.xml',
        'views/catalogue_causes_lock_unlock_view.xml',
        'views/add_status_field_res_partner_inherit_view.xml',
        'wizard/views/lock_unlock_massive_clients_wizard.xml',
        'reports/wizard/views/locked_clients_massive_report_pdf.xml',
        'reports/wizard/views/lock_unlock_report_wizard.xml',
        'reports/wizard/views/print_report_csv.xml',
    ],

}
