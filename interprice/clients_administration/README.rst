=======================================================
Bloqueo de clientes
=======================================================
Este modulo tiene la funcionalidad para bloquear y desbloquear clientes
quienes tienen adeudos que no han pagado hacia la empresa, cuenta con la
caracteristica de bloqueo masivo, opciones para diferentes tipos de reportes
PDF, csv, pantalla.

******************************
Modulo
******************************
    * Nombre tecnico del modulo
    .. figure:: ../clients_administration/static/description/module_clients_administration.png
        :width: 100%

******************************
 Menu y vistas
******************************
    * Menu
    .. figure:: ../clients_administration/static/description/menu_lock_unlock_clients.png
        :width: 100%

    * Vista formulario
    .. figure:: ../clients_administration/static/description/view_form_block_client.png
        :width: 100%

    * Vista bloqueo masivo
    .. figure:: ../clients_administration/static/bloque_masivo.png
        :width: 100%

    * Vista reportes
    .. figure:: ../clients_administration/static/view_report.png
        :width: 100%

    * Vista reportes
    .. figure:: ../clients_administration/static/causas_de_bloqueo.png
        :width: 100%

    * Configuración de permisos de usaurio
    .. figure:: ../clients_administration/static/access.png
        :width: 100%
