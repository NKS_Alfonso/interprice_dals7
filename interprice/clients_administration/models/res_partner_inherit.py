# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import _, api, fields, exceptions, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def _get_default_name(self):
        return False

    status_client = fields.Boolean(
        default=lambda self: self._get_default_name(),
        string=_('Locked'),
        help=_('If the field is checked, the client is blocked'),
    )

    @api.model
    def create(self, vals):
        vals.update({'status_client': False})
        result = super(ResPartner, self).create(vals)
        return result
