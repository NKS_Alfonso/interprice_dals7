# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _, exceptions

class lockOrUnlockClient(models.Model):
    _inherit = 'mail.thread'
    _name = 'lock.unlock.client'
    _description = _('Locking or activation of clients')
    _rec_name = 'state'
    _defaults = {
        'state': 'unlocked',
    }

    internal_folio = fields.Char(string='')
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string=_('Client'),
        required=True,
    )
    credit_line = fields.Char(
        string=_('Limit credit line'),
        compute='get_values_partner',
        store=True
    )
    payment_type = fields.Char(
        string=_('Payment type'),
        compute='get_values_partner',
        store=True,
    )
    lock_unlock_date = fields.Datetime(
        default=fields.datetime.now(),
        string=_('Date'),
        required=True
    )
    observations = fields.Text(
        string=_('Observations'),

    )
    action_type_ul = fields.Selection(
        [('lock', _('Lock')),
        ('unlock', _('Activate'))],
        string=_('Action Type'),
        required=True,
    )
    lock_cause = fields.Many2one(
        comodel_name='catalogue.causes',
        string=_('Reason'),
        required=True,
    )
    payment_deadline = fields.Char(
        string=_('Payment deadline'),
        compute='get_values_partner',
        store=True,
        # readonly=True,
    )
    state = fields.Selection([
        ('draft', _('Draft')),
        ('locked', _('Locked')),
        ('unlocked', _('Activated'))],
        string='Status',
        required=True,
        # readonly=True,
    )
    no_client = fields.Char(
        _('No. Client'),
        compute='get_values_partner',
        store=True,
    )
    user = fields.Many2one(
        comodel_name='res.users',
        string=_('User'),
        default=lambda self: self.env.user.id,
        required=False
    )
    client_is_saved = fields.Boolean(
        default=False,
        string="Is saved",
        readonly=True,
    )
    history_ids = fields.One2many(
        'lock.unlock.history',
        'document_id',
        string=_('History'),
        readonly=True,
        copy=False
    )
    
    @api.model
    def exists_document(self, partner_id):
        unlock_id = False
        if partner_id:
            self.env.cr.execute("""
                SELECT partner_id FROM lock_unlock_client
                """)
            if self.env.cr.rowcount:
                lock_unlock_ids = [j[0] for j in self.env.cr.fetchall()]
                if partner_id in lock_unlock_ids:
                    unlock_id = self.env['lock.unlock.client'].search(
                        [('partner_id', '=', partner_id)], limit=1)
        return unlock_id

    @api.onchange('action_type_ul')
    def onchange_action_type(self):
        res = {}
        client_ids = []
        causes_lock_ids = []
        causes_unlock_ids = []
        self.lock_cause = False
        
    @api.model
    @api.depends('partner_id')
    def get_values_partner(self):
        self.payment_type = self.partner_id.forma_pago.str_nombre
        self.payment_deadline = self.partner_id.property_payment_term.name
        self.credit_line = self.partner_id.company_credit_limit
        self.no_client = self.partner_id.client_number
        self.user = self.env.user.id

    @api.model
    def create(self, vals):
        if not vals.get('is_massive'):
            unlock_id = self.exists_document(vals.get('partner_id'))
            if unlock_id:
                raise exceptions.Warning(_("""The client already has a
                    lock/unlock document[' {} '], please follow up on it""".format(
                        unlock_id.internal_folio)))
            else:
                sequence_id = self.env['ir.sequence'].search(
                    [('code', '=', 'lock.unlock.client.folio')])
                vals['internal_folio'] = self.env['ir.sequence'].next_by_id(
                    sequence_id.id)
        return super(lockOrUnlockClient, self).create(vals)

    @api.multi
    def write(self, vals):
        unlock_id = self.exists_document(vals.get('partner_id'))
        if unlock_id:
            raise exceptions.Warning(_("""The client already has a
                lock/unlock document['{}'], please follow up on it""".format(
                    unlock_id.internal_folio)))
        return super(lockOrUnlockClient, self).write(vals)

    @api.one
    def button_lock(self):
        vals = {
            'state': 'locked',
            'user': self.env.user.id,
            'client_is_saved': True,
            'lock_unlock_date': fields.datetime.now()
            }
        if self.state in ['unlocked','draft'] and self.action_type_ul == 'lock':
            client_id = self.partner_id
            client_id.status_client = True
            unlock_id = self.exists_document(client_id.id)
            self.update(vals)
        else:
            raise exceptions.Warning(_("""To block the user you must change
                    the type of action and choose a corresponding cause"""))
        vals['document_id'] = self.id
        vals['partner_id'] = self.partner_id.id
        vals['action_type_ul'] = self.action_type_ul
        vals['lock_cause'] = self.lock_cause.id
        self.history_ids.create(vals)
        

    @api.one
    def button_unlock(self):
        vals = {
            'state': 'unlocked',
            'lock_cause': self.lock_cause.id,
            'user': self.env.user.id,
            'client_is_saved': True,
            'lock_unlock_date': fields.datetime.now()
            }
        if self.state in ['locked','draft'] and self.action_type_ul == 'unlock':
            client_id = self.partner_id
            client_id.status_client = False
            self.update(vals)
        else:
            raise exceptions.Warning(_("""To Activate the user you must change
                    the type of action and choose a corresponding cause"""))
        vals['document_id'] = self.id
        vals['partner_id'] = self.partner_id.id
        vals['action_type_ul'] = self.action_type_ul
        vals['lock_cause'] = self.lock_cause.id
        self.history_ids.create(vals)

class LockUnlockHistory(models.Model):
    _inherit = 'mail.thread'
    _name = 'lock.unlock.history'
    _description = _('Document history of customers')
    _rec_name = 'document_id'
    _order = 'lock_unlock_date desc'

    document_id = fields.Many2one(
        comodel_name='lock.unlock.client',
        string=_('Customer Document'),
        readonly=True
    )
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string=_('Client'),
        readonly=True,
    )
    lock_unlock_date = fields.Datetime(
        default=fields.datetime.now(),
        string=_('Date'),
        readonly=True
    )
    observations = fields.Text(
        string=_('Observations'),
        readonly=True
    )
    action_type_ul = fields.Selection(
        [('lock', _('Lock')),
        ('unlock', _('Activate'))],
        string=_('Action Type'),
        readonly=True
    )
    lock_cause = fields.Many2one(
        comodel_name='catalogue.causes',
        string=_('Reason'),
        readonly=True
    )
    state = fields.Selection([
        ('draft', _('Draft')),
        ('locked', _('Locked')),
        ('unlocked', _('Activated'))],
        string='Status',
        readonly=True
        # readonly=True,
    )
    user = fields.Many2one(
        comodel_name='res.users',
        string=_('User'),
        default=lambda self: self.env.user.id,
        readonly=True
    )
