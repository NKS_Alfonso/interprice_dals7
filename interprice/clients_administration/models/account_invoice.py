# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class AccountInvoice(models.Model):
    """ Model extend"""

    # Odoo properties
    _inherit = 'account.invoice'

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice,
                            payment_term, partner_bank_id, company_id):
        result = super(AccountInvoice, self).onchange_partner_id(
            type, partner_id, date_invoice, payment_term,
            partner_bank_id, company_id)
        if type == 'out_invoice':
            if partner_id:
                partner_obj_id = self.env['res.partner'].browse(partner_id)
                if partner_obj_id.customer == True and partner_obj_id.is_company == True and partner_obj_id.status_client == True:
                    raise exceptions.Warning(_("Lock partner!"))
        return result

    def create(self, cr, uid, vals, context=None):
        res = super(AccountInvoice, self).create(cr, uid, vals, context=context)
        doc_obj = self.pool.get('account.invoice').browse(cr, uid, res, context=context)
        if doc_obj.partner_id and doc_obj.type == 'out_invoice':
            partner_id = self.pool.get('res.partner').browse(
                cr, uid, doc_obj.partner_id.id, context=context)
            if partner_id.status_client == True:
                 raise exceptions.Warning(_("Unable to create invoice for this client because it is blocked!"))
        return res
