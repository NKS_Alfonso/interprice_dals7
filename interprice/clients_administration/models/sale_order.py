# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class SaleOrder(models.Model):
    """ Model extend"""
    # Odoo properties
    _inherit = "sale.order"

    @api.multi
    def onchange_partner_id(self, partner_id):
        result = super(SaleOrder, self).onchange_partner_id(partner_id)
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)
            if partner.status_client != False and partner.customer == True:
                raise exceptions.Warning(_("Blocked partner!"))
        return result

    def create(self, cr, uid, vals, context=None):
        if vals.get('partner_id'):
            partner_id = self.pool.get('res.partner').browse(
                cr, uid, vals.get('partner_id'), context=context)
            if partner_id.status_client == True:
                 raise exceptions.Warning(_("Unable to create invoice for this client because it is blocked!"))
        return super(SaleOrder, self).create(cr, uid, vals, context=context)

    def action_button_confirm(self, cr, uid, vals, context=None):
        """Function to validate if the client si blocked before
        to continue with the quote.

        This function is inherit to the model sale.orde to can validate
        before to continue with the quote order.

       Raises:
            exceptions -- This exception catch if the client is in
            status Locked.
        """
        if vals:
            sale_order = self.pool.get('sale.order').browse(
                cr, uid, vals[0], context=context)
            if sale_order.partner_id.status_client:
                raise exceptions.Warning(_("It is not possible to continue with the quote, the client is blocked"))
        return super(SaleOrder, self).action_button_confirm(cr, uid, vals, context=context)
