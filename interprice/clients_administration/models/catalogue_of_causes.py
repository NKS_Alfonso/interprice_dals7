# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _, exceptions

class catalogueCauses(models.Model):
    _inherit = 'mail.thread'
    _name = 'catalogue.causes'
    _description = _('Catalogue of causes')
    _defaults = {
        'action_type': 'lock',
    }


    name = fields.Char(
        string=_('Name'),
        required=True
    )
    action_type = fields.Selection(
        selection=[
            ('lock', _('Lock')),
            ('unlock', _('Activate'))],
        string=_('Action Type'),
        required=True
    )
    description = fields.Text(
        copy=False,
        help=_("Type of cause description"),
        string=_("Description"),
    )

    to_massive = fields.Boolean(
        default=False,
        string=_('To massive'),
    )

    lock_parameter_catalogue = fields.Selection([
        ('amount_credit', _("Amount credit")),
        ('expired_debt', _("Expired debt")),
        # ('returned_check', _("Returned check"))
        ],
        string=_('Lock parameter'),
        required=False,
     )

