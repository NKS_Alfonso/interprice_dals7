# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
from olib.oCsv.oCsv import OCsv
from openerp import _, api, exceptions, fields, models
from datetime import date
from dateutil.parser import parse


class LockUnlockMassiveClientsWizard(models.TransientModel):
    """
    """
    # _inherit = 'lock.unlock.client'
    _name = 'lock.unlock.massive.clients.wizard'

    # Fields
    lock_parameter = fields.Many2one(
        comodel_name='catalogue.causes',
        domain=[
                '&',
                ('action_type', '=', 'lock'),
                ('to_massive','=',True)
            ],
        string=_('Lock Cause'),
        required=True,
        )

    tolerance_amount = fields.Float(
        string=_('Amount'),
        default=0,
        required=True,
        help=_("Tolerance amount ")
    )
    no_client_start = fields.Integer(
        default='',
        string=_('From:'),
        help=_("Start of the range to apply the blockade"),
        )
    no_client_end = fields.Integer(
        default='',
        string=_('To:'),
        help=_("End of the range to apply the blockade"),
        )
    is_massive = fields.Boolean(
        default=True,
        string=_('Is massive lock'),
    )


    @api.multi
    def massive_lock(self):
        lock_client_ids = []
        lock_client = self.env['lock.unlock.client']
        client_ids = self.env['res.partner'].search(
                [('customer', '=', True),
                 ('status_client', '=', False),
                 ('is_company', '=', True)]
                )
        parameter = self.lock_parameter
        amount = self.tolerance_amount
        client_start = self.no_client_start
        client_end = self.no_client_end
        if parameter.to_massive:
            if parameter.lock_parameter_catalogue == 'amount_credit':
                for client_id in client_ids:
                    if client_id.company_credit_limit > amount:
                        lock_id = lock_client.exists_document(client_id.id)
                        if lock_id and lock_id.state == 'unlocked':
                            if client_start > 0 and client_end > 0:
                                if int(client_id.client_number) in range(
                                    client_start, client_end):
                                    lock_id.action_type_ul = 'lock'
                                    lock_id.state = 'locked'
                                    lock_id.lock_cause = parameter.id
                                    lock_id.lock_unlock_date = fields.datetime.now()
                                    lock_id.partner_id.status_client = True
                                    lock_id.unlock_cause = False
                                    lock_client_ids.append(lock_id.id)
                                    post_vars = {
                                        'subject': _("Information"),
                                        'body': _("""
                                            <p>The Client <b>{}</b> has been locked</p>
                                            <p>Reason: <b>{}</b></p>
                                            <p>Observations: <b>{}</b>""").format(
                                                client_id.name,
                                                parameter.name and parameter.name or '',
                                                lock_id.observations and  lock_id.observations or '')
                                    }
                                    lock_id.message_post(type="notification",
                                                      subtype="mt_comment", **post_vars)
                        elif client_start > 0 and client_end > 0:
                            if int(client_id.client_number) in range(
                                client_start, client_end):
                                values = {
                                    'state': 'locked',
                                    'action_type_ul': 'lock',
                                    'partner_id': client_id.id,
                                    'lock_cause': parameter.id,
                                    'lock_unlock_date': fields.datetime.now(),
                                    'payment_type': client_id.\
                                                forma_pago.str_nombre,
                                    'payment_deadline': client_id.\
                                                property_payment_term.name,
                                    'credit_line': client_id.\
                                                company_credit_limit,
                                    'no_client': client_id.client_number,
                                    'is_massive': self.is_massive,
                                    'user': self.env.user.name
                                    }
                                new_lock_id = lock_client.create(values)
                                sequence_id = self.env['ir.sequence'].search(
                                    [('code', '=', 'lock.unlock.client.folio')])
                                new_lock_id.internal_folio = self.env[
                                    'ir.sequence'].next_by_id(
                                    sequence_id.id)
                                lock_client_ids.append(new_lock_id.id)
                                client_id.update({'status_client': True})
                                post_vars = {
                                    'subject': _("Information"),
                                    'body': _("""
                                        <p>The Client <b>{}</b> has been locked</p>
                                        <p>Reason: <b>{}</b></p>""").format(
                                            client_id.name,
                                            parameter.name and parameter.name or '')
                                }
                                new_lock_id.message_post(
                                    type="notification",
                                    subtype="mt_comment", **post_vars)
                        else:
                            values = {
                                'state': 'locked',
                                'action_type_ul': 'lock',
                                'partner_id': client_id.id,
                                'lock_cause': parameter.id,
                                'lock_unlock_date': fields.datetime.now(),
                                'payment_type': client_id.\
                                                forma_pago.str_nombre,
                                'payment_deadline': client_id.\
                                                property_payment_term.name,
                                'credit_line': client_id.\
                                                company_credit_limit,
                                'no_client': client_id.client_number,
                                'is_massive': self.is_massive,
                                'user': self.env.user.name
                                }
                            new_lock_id = lock_client.create(values)
                            sequence_id = self.env['ir.sequence'].search(
                                [('code', '=', 'lock.unlock.client.folio')])
                            new_lock_id.internal_folio = self.env[
                                'ir.sequence'].next_by_id(
                                sequence_id.id)
                            lock_client_ids.append(new_lock_id.id)
                            client_id.update({'status_client': True})
                            post_vars = {
                                'subject': _("Information"),
                                'body': _("""
                                    <p>The Client <b>{}</b> has been locked</p>
                                    <p>Reason: <b>{}</b></p>""").format(
                                        client_id.name,
                                        parameter.name and parameter.name or '')
                            }
                            new_lock_id.message_post(
                                type="notification",
                                subtype="mt_comment", **post_vars)
                    else:
                        pass
            elif parameter.lock_parameter_catalogue == 'expired_debt':
                for client_id in client_ids:
                    saldo_vencido = 0.0
                    invoice_ids = self.env['account.invoice'].search(
                            [('partner_id', '=', client_id.id),
                             ('state', '=', 'open')])
                    if invoice_ids:
                        for invoice_id in invoice_ids:
                            today = date.today()
                            date_due_invoice = parse(
                                invoice_id.date_due).date()
                            if today > date_due_invoice:
                                saldo_vencido += invoice_id.residual
                    if saldo_vencido > amount:
                        lock_id = lock_client.exists_document(client_id.id)
                        if lock_id and lock_id.state == 'unlocked':
                            lock_id.action_type_ul = 'lock'
                            lock_id.state = 'locked'
                            lock_id.lock_cause = parameter.id
                            lock_id.unlock_cause = False
                            lock_id.lock_unlock_date = fields.datetime.now()
                            lock_id.partner_id.status_client = True
                            lock_client_ids.append(lock_id.id)
                            post_vars = {
                                'subject': _("Information"),
                                'body': _("""
                                    <p>The Client <b>{}</b> has been locked</p>
                                    <p>Reason: <b>{}</b></p>
                                    <p>Observations: <b>{}</b>""").format(
                                        client_id.name,
                                        parameter.name and parameter.name or '',
                                        lock_id.observations and lock_id.observations or '')
                            }
                            lock_id.message_post(type="notification",
                                              subtype="mt_comment", **post_vars)
                        elif client_start > 0 and client_end > 0:
                            if int(client_id.client_number) in\
                             range(client_start, client_end):
                                values = {
                                    'state': 'locked',
                                    'action_type_ul': 'lock',
                                    'partner_id': client_id.id,
                                    'lock_cause': parameter.id,
                                    'lock_unlock_date': fields.datetime.now(),
                                    'payment_type': client_id.\
                                                forma_pago.str_nombre,
                                    'payment_deadline': client_id.\
                                                property_payment_term.name,
                                    'credit_line': client_id.\
                                                company_credit_limit,
                                    'no_client': client_id.client_number,
                                    'is_massive':self.is_massive,
                                    'user': self.env.user.name
                                    }
                                new_lock_id =  lock_client.create(values)
                                sequence_id = self.env['ir.sequence'].search(
                                    [('code', '=', 'lock.unlock.client.folio')])
                                new_lock_id.internal_folio = self.env[
                                    'ir.sequence'].next_by_id(
                                    sequence_id.id)
                                lock_client_ids.append(new_lock_id.id)
                                client_id.update({'status_client':True})
                                post_vars = {
                                    'subject': _("Information"),
                                    'body': _("""
                                        <p>The Client <b>{}</b> has been locked</p>
                                        <p>Reason: <b>{}</b></p>""").format(
                                            client_id.name,
                                            parameter.name and parameter.name or '')
                                }
                                new_lock_id.message_post(
                                    type="notification",
                                    subtype="mt_comment", **post_vars)
                        else:
                            values = {
                                'state': 'locked',
                                'action_type_ul': 'lock',
                                'partner_id': client_id.id,
                                'lock_cause': parameter.id,
                                'lock_unlock_date': fields.datetime.now(),
                                'payment_type': client_id.\
                                            forma_pago.str_nombre,
                                'payment_deadline': client_id.\
                                            property_payment_term.name,
                                'credit_line': client_id.\
                                            company_credit_limit,
                                'no_client': client_id.client_number,
                                'is_massive':self.is_massive,
                                'user': self.env.user.name
                                }
                            new_lock_id = lock_client.create(values)
                            sequence_id = self.env['ir.sequence'].search(
                                [('code', '=', 'lock.unlock.client.folio')])
                            new_lock_id.internal_folio = self.env[
                                'ir.sequence'].next_by_id(
                                sequence_id.id)
                            lock_client_ids.append(new_lock_id.id)
                            client_id.update({'status_client':True})
                            post_vars = {
                                'subject': _("Information"),
                                'body': _("""
                                    <p>The Client <b>{}</b> has been locked</p>
                                    <p>Reason: <b>{}</b></p>""").format(
                                        client_id.name,
                                        parameter.name and parameter.name or '',)
                            }
                            new_lock_id.message_post(
                                type="notification",
                                subtype="mt_comment", **post_vars)
                    else:
                        pass
        else:
            raise exceptions.Warning(
                _("""The cause of blocking is not valid
                    for the massive blockade!"""))

        if len(lock_client_ids) > 0:
            return {
                'name': _('Locked clients'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'lock.unlock.client',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', lock_client_ids)],
            }
        else:
            raise exceptions.Warning(_("No records found!"))
