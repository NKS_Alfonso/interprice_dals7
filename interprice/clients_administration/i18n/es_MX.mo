��    n      �  �   �      P	  �   Q	  �   6
  �   �
  �   Z  �   '  �   �     X     d     �     �     �     �  	   �     �     �     �     �     �               -  
   =     H     \     x       
   �  
   �     �  .   �     �     �     �  #   �  &     
   =     H     U  	   [     e     k  �   |       /     .   5     d     p          �     �     �  D   �     �               %     7     <     H     ]     v     �     �     �     �     �     �     �      �          !  "   *     M  	   R     \  
   n     y     �     �     �     �     �     �     �     �     �  
   �  %     (   -  *   V     �     �     �  P   �  a   �  [   T  l   �  i     
   �     �  �   �     V     h  '   �  ?   �     �     �     �       B    �   `  �   E  �   �  �   m  �   =  �   �     p  %   �  *   �     �     �     �     �                    (     ;     D     X     l     |     �     �     �     �  
   �  	   �     �  3   �     *     7     @  2   L  %        �     �     �  
   �     �     �  �   �     {  =   ~  5   �     �     �                       /   F   8            �      �      �      �      �      !     !     8!     N!     a!      v!      �!  	   �!     �!     �!      �!     "     1"  %   :"     `"     g"     z"     �"     �"     �"     �"     �"     �"     �"     #     '#     .#     >#     G#  .   W#  (   �#  .   �#     �#     �#     �#  M   $  k   R$  `   �$  n   %  o   �%     �%     
&  �   &     �&     �&  &   '  I   +'     u'     �'     �'     �'     .   V   %   =          X   >   h          g   m              C           [             H   U                       a   i      B   3   	   J                 !             O   P   <      #      (       S   7   )   9   D               \   _          `   -   W   A   @              +   Y                G           /   5   Z           f   ]   k       K   e       ^      &   d       c   '   j   8   ,               I   2   l       E   :   N          b   F   M   0   *   "                 T   ;   ?       R       1       $      4   
      L   Q   n   6                   
                                            <p>The Client <b>{}</b> has been locked</p>
                                            <p>Reason: <b>{}</b></p>
                                            <p>Observations: <b>{}</b> 
                                        <p>The Client <b>{}</b> has been locked</p>
                                        <p>Reason: <b>{}</b></p> 
                                    <p>The Client <b>{}</b> has been locked</p>
                                    <p>Reason: <b>{}</b></p> 
                                    <p>The Client <b>{}</b> has been locked</p>
                                    <p>Reason: <b>{}</b></p>
                                    <p>Observations: <b>{}</b> 
                    <p>The Client <b>{}</b> has been activated</p>
                    <p>Reason: <b>{}</b></p>
                    <p>Observations: <b>{}</b> 
                <p>The Client <b>{}</b> has been locked</p>
                <p>Reason: <b>{}</b></p>
                <p>Observations: <b>{}</b> Action Type Action lock massive clients Action lock report clients Action type Activate Activate client Activated Administrator Amount Amount credit Blocked partner! Cancel Catalogue of Causes Catalogue of causes Cause Activated Cause lock Cause of Activation Cause of lock or Activating Client Clients Range Created by Created on Date Date of the last message posted on the record. Description Draft End date End date to consider for the filter End of the range to apply the blockade Excel File Expired debt Folio Followers From: History of locks Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. If the field is checked, the client is blocked Information Internal folio Invoice Is a Follower Is massive lock Is saved It is not possible to continue with the quote, the client is blocked Last Message Date Last Updated by Last Updated on Limit credit line Lock Lock client Lock massive clients Lock or Activate clients Lock parameter Lock partner! Lock/Unlock Lock/Unlock clients Lock/unlock Locked Locked clients Locked clients report Locking or activation of clients Massive clients lock Messages Messages and communication history Name No client No records found! No. Client Observations PDF File Partner Payment deadline Payment type Print Report csv Print report locked clients Reason Sales Order Screen Start date Start date to consider for the filter Start of the range to apply the blockade State of client to consider for the filter Status Status parameter Summary The cause of blocking is not valid
                    for the massive blockade! The client already has a
                    lock/unlock document[' {} '], please follow up on it The client already has a
                lock/unlock document['{}'], please follow up on it To Activate the user you must change
                    the type of action and choose a corresponding cause To block the user you must change
                    the type of action and choose a corresponding cause To massive To: Todos los pedidos que sean realizados por medio del ecommerce               via API, activara este campo para distinguirlos de los pedidos               realizados por algun operador interno. Tolerance amount  Type of cause description Type of cause of blocking or activation Unable to create invoice for this client because it is blocked! Unread Messages User Via Web {'invisible' : False} Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-06-26 22:46+0000
PO-Revision-Date: 2018-10-19 12:25-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es
X-Generator: Poedit 1.8.7.1
 
                                            <p>The Client <b>{}</b> has been locked</p>
                                            <p>Reason: <b>{}</b></p>
                                            <p>Observations: <b>{}</b> 
                                        <p>El cliente <b>{}</b> ha sido bloqueado</p>
                                        <p>Razón: <b>{}</b></p> 
                                    <p>El cliente <b>{}</b> ha sido bloqueado</p>
                                    <p>Razón: <b>{}</b></p> 
                                    <p>El cliente <b>{}</b> ha sido bloqueado</p>
                                    <p>Razón: <b>{}</b></p>
                                    <p>Observaciones: <b>{}</b> 
                    <p>El cliente <b>{}</b> ha sido activado</p>
                    <p>Razón: <b>{}</b></p>
                    <p>Observaciones: <b>{}</b> 
                <p>El cliente <b>{}</b> ha sido bloqueado</p>
                <p>Razón: <b>{}</b></p>
                <p>Observaciones: <b>{}</b> Tipo de acción Acción de bloqueo masivo de clientes Acción de informe de bloqueo de clientes  Tipo de acción Activar Activar cliente Activado Administrador Monto Monto de crédito Cliente bloqueado! Cancelar Catálogo de causas Catálogo de causas Cause Activated Causa de bloqueo Causa de Activación Causa de Bloqueo o Activación Cliente Rango de clientes Creado por Creado en Fecha Fecha del último mensaje publicado en el registro. Descripción Borrador Fecha final Fecha de finalización a considerar para el filtro Fin del rango para aplicar el bloqueo Archivo csv Deuda vencida Folio Seguidores Desde: Historial de bloqueos Contiene el resumen de Mensajes (número de mensajes, ...). Este resumen está directamente en formato html para insertarse en vistas kanban. ID Si está marcado, los mensajes nuevos requieren su atención. Si el campo está marcado, el cliente está bloqueado Información Folio Factura Es un seguidor Es bloqueo masivo? Is saved No es posible continuar con la cotización, el cliente está bloqueado Fecha del último mensaje Última actualización por Ultima actualizacion en Límite de línea de crédito Bloquear Bloquear cliente Bloqueo masivo clientes Bloquear o Activar clientes Parámetro de bloqueo Cliente bloqueado! Bloqueo y Activacion Bloqueo o activacion de clientes Bloqueo o activacion de clientes Bloqueado Clientes bloqueados Reporte de clientes bloqueados Bloqueo o activavion de clientes Bloqueo masivo de clientes Mensajes Mensajes e historial de comunicación Nombre Número de cliente ¡No se encontrarón registros! Número de cliente Observaciones Archivo PDF Empresa Tiempo límite de pago Tipo de pago Imprimir reporte csv Reporte de clientes bloqueados Motivo Pedido de venta Pantalla Fecha de inicio Fecha de inicio para considerar para el filtro Inicio del rango para aplicar el bloqueo Estado del cliente a considerar para el filtro Estado Estatus del Parámetro Resumen La causa del bloqueo no es válida
                    para el bloqueo masivo El cliente ya tiene un
                    documento de bloqueo/activación[' {} '], por favor use el mismo El cliente ya tiene un
                    documento de bloqueo ['{}'], por favor, sigue en ello Para Activar al cliente debes cambiar
                    el tipo de acción y elige una causa correspondiente Para bloquear al cliente debes cambiar
                    el tipo de acción y elige una causa correspondiente Para masivo Hasta: Todos los pedidos que sean realizados por medio del ecommerce               via API, activara este campo para distinguirlos de los pedidos               realizados por algun operador interno. Monto de tolerancia Descripción del tipo de causa Tipo de causa de bloqueo o activación ¡No se puede crear una factura para este cliente porque está bloqueado! Mensajes no leídos Usuario Via Web {'invisible' : False} 