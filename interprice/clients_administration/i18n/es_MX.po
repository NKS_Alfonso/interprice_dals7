# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* clients_administration
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-26 22:46+0000\n"
"PO-Revision-Date: 2018-10-19 12:25-0500\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"Language: es\n"
"X-Generator: Poedit 1.8.7.1\n"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:83
#, python-format
msgid ""
"\n"
"                                            <p>The Client <b>{}</b> has been "
"locked</p>\n"
"                                            <p>Reason: <b>{}</b></p>\n"
"                                            <p>Observations: <b>{}</b>"
msgstr ""
"\n"
"                                            <p>The Client <b>{}</b> has been "
"locked</p>\n"
"                                            <p>Reason: <b>{}</b></p>\n"
"                                            <p>Observations: <b>{}</b>"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:122
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:233
#, python-format
msgid ""
"\n"
"                                        <p>The Client <b>{}</b> has been locked</"
"p>\n"
"                                        <p>Reason: <b>{}</b></p>"
msgstr ""
"\n"
"                                        <p>El cliente <b>{}</b> ha sido bloqueado</"
"p>\n"
"                                        <p>Razón: <b>{}</b></p>"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:158
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:269
#, python-format
msgid ""
"\n"
"                                    <p>The Client <b>{}</b> has been locked</p>\n"
"                                    <p>Reason: <b>{}</b></p>"
msgstr ""
"\n"
"                                    <p>El cliente <b>{}</b> ha sido bloqueado</p>\n"
"                                    <p>Razón: <b>{}</b></p>"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:194
#, python-format
msgid ""
"\n"
"                                    <p>The Client <b>{}</b> has been locked</p>\n"
"                                    <p>Reason: <b>{}</b></p>\n"
"                                    <p>Observations: <b>{}</b>"
msgstr ""
"\n"
"                                    <p>El cliente <b>{}</b> ha sido bloqueado</p>\n"
"                                    <p>Razón: <b>{}</b></p>\n"
"                                    <p>Observaciones: <b>{}</b>"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:239
#: code:addons/clients_administration/models/lock_or_unlock_client.py:261
#, python-format
msgid ""
"\n"
"                    <p>The Client <b>{}</b> has been activated</p>\n"
"                    <p>Reason: <b>{}</b></p>\n"
"                    <p>Observations: <b>{}</b>"
msgstr ""
"\n"
"                    <p>El cliente <b>{}</b> ha sido activado</p>\n"
"                    <p>Razón: <b>{}</b></p>\n"
"                    <p>Observaciones: <b>{}</b>"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:213
#, python-format
msgid ""
"\n"
"                <p>The Client <b>{}</b> has been locked</p>\n"
"                <p>Reason: <b>{}</b></p>\n"
"                <p>Observations: <b>{}</b>"
msgstr ""
"\n"
"                <p>El cliente <b>{}</b> ha sido bloqueado</p>\n"
"                <p>Razón: <b>{}</b></p>\n"
"                <p>Observaciones: <b>{}</b>"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:24
#: code:addons/clients_administration/models/lock_or_unlock_client.py:44
#: field:catalogue.causes,action_type:0 field:lock.unlock.client,action_type_ul:0
#: field:lock.unlock.history,action_type_ul:0
#, python-format
msgid "Action Type"
msgstr "Tipo de acción"

#. module: clients_administration
#: model:ir.actions.act_window,name:clients_administration.action_to_lock_unlock_massive_clients
msgid "Action lock massive clients"
msgstr "Acción de bloqueo masivo de clientes"

#. module: clients_administration
#: model:ir.actions.act_window,name:clients_administration.action_to_lock_unlock_report_clients
msgid "Action lock report clients"
msgstr "Acción de informe de bloqueo de clientes "

#. module: clients_administration
#: view:catalogue.causes:clients_administration.view_tree_catalogue_causes_to_lock_unlock
msgid "Action type"
msgstr "Tipo de acción"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:23
#: code:addons/clients_administration/models/lock_or_unlock_client.py:43
#: selection:catalogue.causes,action_type:0
#: selection:lock.unlock.client,action_type_ul:0
#, python-format
msgid "Activate"
msgstr "Activar"

#. module: clients_administration
#: view:lock.unlock.client:clients_administration.view_form_to_lock_unlock_client
msgid "Activate client"
msgstr "Activar cliente"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:66
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:22
#: selection:lock.unlock.client,state:0
#: selection:lock.unlock.clients.report.wizard,status_client_parameter:0
#, python-format
msgid "Activated"
msgstr "Activado"

#. module: clients_administration
#: model:res.groups,name:clients_administration.manager
msgid "Administrator"
msgstr "Administrador"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:31
#: field:lock.unlock.massive.clients.wizard,tolerance_amount:0
#, python-format
msgid "Amount"
msgstr "Monto"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:39
#: selection:catalogue.causes,lock_parameter_catalogue:0
#, python-format
msgid "Amount credit"
msgstr "Monto de crédito"

#. module: clients_administration
#: code:addons/clients_administration/models/sale_order.py:19
#, python-format
msgid "Blocked partner!"
msgstr "Cliente bloqueado!"

#. module: clients_administration
#: view:lock.unlock.clients.report.wizard:clients_administration.lock_unlock_clients_report_wizard
#: view:lock.unlock.massive.clients.wizard:clients_administration.lock_unlock_massive_clients_wizard
#: view:print.lock.unlock.clients.report.wizard:clients_administration.print_lock_unlock_clients_report_wizard
msgid "Cancel"
msgstr "Cancelar"

#. module: clients_administration
#: view:catalogue.causes:clients_administration.view_form_catalogue_causes_to_lock_unlock
#: model:ir.actions.act_window,name:clients_administration.action_catalogue_of_causes
msgid "Catalogue of Causes"
msgstr "Catálogo de causas"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:10
#: model:ir.model,name:clients_administration.model_catalogue_causes
#: model:ir.ui.menu,name:clients_administration.menu_catalogue_of_causes
#, python-format
msgid "Catalogue of causes"
msgstr "Catálogo de causas"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:136
#, python-format
msgid "Cause Activated"
msgstr "Cause Activated"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:135
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:49
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "Cause lock"
msgstr "Causa de bloqueo"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:54
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:50
#: field:lock.unlock.client,unlock_cause:0
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "Cause of Activation"
msgstr "Causa de Activación"

#. module: clients_administration
#: view:catalogue.causes:clients_administration.view_tree_catalogue_causes_to_lock_unlock
msgid "Cause of lock or Activating"
msgstr "Causa de Bloqueo o Activación"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:19
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:132
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:46
#: field:lock.unlock.client,partner_id:0 field:lock.unlock.history,partner_id:0
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "Client"
msgstr "Cliente"

#. module: clients_administration
#: view:lock.unlock.massive.clients.wizard:clients_administration.lock_unlock_massive_clients_wizard
msgid "Clients Range"
msgstr "Rango de clientes"

#. module: clients_administration
#: field:catalogue.causes,create_uid:0 field:lock.unlock.client,create_uid:0
#: field:lock.unlock.clients.report.wizard,create_uid:0
#: field:lock.unlock.massive.clients.wizard,create_uid:0
#: field:print.lock.unlock.clients.report.wizard,create_uid:0
msgid "Created by"
msgstr "Creado por"

#. module: clients_administration
#: field:catalogue.causes,create_date:0 field:lock.unlock.client,create_date:0
#: field:lock.unlock.clients.report.wizard,create_date:0
#: field:lock.unlock.massive.clients.wizard,create_date:0
#: field:print.lock.unlock.clients.report.wizard,create_date:0
msgid "Created on"
msgstr "Creado en"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:34
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:134
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:48
#: field:lock.unlock.client,lock_unlock_date:0
#: field:lock.unlock.history,lock_unlock_date:0
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "Date"
msgstr "Fecha"

#. module: clients_administration
#: help:catalogue.causes,message_last_post:0
#: help:lock.unlock.client,message_last_post:0
msgid "Date of the last message posted on the record."
msgstr "Fecha del último mensaje publicado en el registro."

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:30
#: field:catalogue.causes,description:0
#, python-format
msgid "Description"
msgstr "Descripción"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:64
#: selection:lock.unlock.client,state:0
#, python-format
msgid "Draft"
msgstr "Borrador"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:36
#: field:lock.unlock.clients.report.wizard,end_date:0
#, python-format
msgid "End date"
msgstr "Fecha final"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:38
#: help:lock.unlock.clients.report.wizard,end_date:0
#, python-format
msgid "End date to consider for the filter"
msgstr "Fecha de finalización a considerar para el filtro"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:49
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:44
#: help:lock.unlock.clients.report.wizard,no_client_end:0
#: help:lock.unlock.massive.clients.wizard,no_client_end:0
#, python-format
msgid "End of the range to apply the blockade"
msgstr "Fin del rango para aplicar el bloqueo"

#. module: clients_administration
#: view:lock.unlock.clients.report.wizard:clients_administration.lock_unlock_clients_report_wizard
#: view:print.lock.unlock.clients.report.wizard:clients_administration.print_lock_unlock_clients_report_wizard
msgid "Excel File"
msgstr "Archivo csv"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:40
#: selection:catalogue.causes,lock_parameter_catalogue:0
#, python-format
msgid "Expired debt"
msgstr "Deuda vencida"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:131
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:45
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "Folio"
msgstr "Folio"

#. module: clients_administration
#: field:catalogue.causes,message_follower_ids:0
#: field:lock.unlock.client,message_follower_ids:0
msgid "Followers"
msgstr "Seguidores"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:43
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:38
#: field:lock.unlock.clients.report.wizard,no_client_start:0
#: field:lock.unlock.massive.clients.wizard,no_client_start:0
#, python-format
msgid "From:"
msgstr "Desde:"

#. module: clients_administration
#: help:catalogue.causes,message_summary:0 help:lock.unlock.client,message_summary:0
msgid ""
"Holds the Chatter summary (number of messages, ...). This summary is directly in "
"html format in order to be inserted in kanban views."
msgstr ""
"Contiene el resumen de Mensajes (número de mensajes, ...). Este resumen está "
"directamente en formato html para insertarse en vistas kanban."

#. module: clients_administration
#: field:catalogue.causes,id:0 field:lock.unlock.client,id:0
#: field:lock.unlock.clients.report.wizard,id:0
#: field:lock.unlock.massive.clients.wizard,id:0
#: field:print.lock.unlock.clients.report.wizard,id:0
msgid "ID"
msgstr "ID"

#. module: clients_administration
#: help:catalogue.causes,message_unread:0 help:lock.unlock.client,message_unread:0
msgid "If checked new messages require your attention."
msgstr "Si está marcado, los mensajes nuevos requieren su atención."

#. module: clients_administration
#: code:addons/clients_administration/models/res_partner_inherit.py:17
#: help:res.partner,status_client:0
#, python-format
msgid "If the field is checked, the client is blocked"
msgstr "Si el campo está marcado, el cliente está bloqueado"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:212
#: code:addons/clients_administration/models/lock_or_unlock_client.py:238
#: code:addons/clients_administration/models/lock_or_unlock_client.py:260
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:82
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:121
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:157
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:193
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:232
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:268
#, python-format
msgid "Information"
msgstr "Información"

#. module: clients_administration
#: field:lock.unlock.client,internal_folio:0
msgid "Internal folio"
msgstr "Folio"

#. module: clients_administration
#: model:ir.model,name:clients_administration.model_account_invoice
msgid "Invoice"
msgstr "Factura"

#. module: clients_administration
#: field:catalogue.causes,message_is_follower:0
#: field:lock.unlock.client,message_is_follower:0
msgid "Is a Follower"
msgstr "Es un seguidor"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:48
#: field:lock.unlock.massive.clients.wizard,is_massive:0
#, python-format
msgid "Is massive lock"
msgstr "Es bloqueo masivo?"

#. module: clients_administration
#: field:lock.unlock.client,client_is_saved:0
msgid "Is saved"
msgstr "Is saved"

#. module: clients_administration
#: code:addons/clients_administration/models/sale_order.py:46
#, python-format
msgid "It is not possible to continue with the quote, the client is blocked"
msgstr "No es posible continuar con la cotización, el cliente está bloqueado"

#. module: clients_administration
#: field:catalogue.causes,message_last_post:0
#: field:lock.unlock.client,message_last_post:0
msgid "Last Message Date"
msgstr "Fecha del último mensaje"

#. module: clients_administration
#: field:catalogue.causes,write_uid:0 field:lock.unlock.client,write_uid:0
#: field:lock.unlock.clients.report.wizard,write_uid:0
#: field:lock.unlock.massive.clients.wizard,write_uid:0
#: field:print.lock.unlock.clients.report.wizard,write_uid:0
msgid "Last Updated by"
msgstr "Última actualización por"

#. module: clients_administration
#: field:catalogue.causes,write_date:0 field:lock.unlock.client,write_date:0
#: field:lock.unlock.clients.report.wizard,write_date:0
#: field:lock.unlock.massive.clients.wizard,write_date:0
#: field:print.lock.unlock.clients.report.wizard,write_date:0
msgid "Last Updated on"
msgstr "Ultima actualizacion en"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:23
#: field:lock.unlock.client,credit_line:0
#, python-format
msgid "Limit credit line"
msgstr "Límite de línea de crédito"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:22
#: code:addons/clients_administration/models/lock_or_unlock_client.py:42
#: selection:catalogue.causes,action_type:0
#: selection:lock.unlock.client,action_type_ul:0
#: view:lock.unlock.massive.clients.wizard:clients_administration.lock_unlock_massive_clients_wizard
#, python-format
msgid "Lock"
msgstr "Bloquear"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:48
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:26
#: field:lock.unlock.client,lock_cause:0 field:lock.unlock.history,lock_cause:0
#: field:lock.unlock.massive.clients.wizard,lock_parameter:0
#, python-format
msgid "Reason"
msgstr "Motivo"

#. module: clients_administration
#: view:lock.unlock.client:clients_administration.view_form_to_lock_unlock_client
msgid "Lock client"
msgstr "Bloquear cliente"

#. module: clients_administration
#: model:ir.ui.menu,name:clients_administration.menu_lock_or_unlock_massive_client
#: view:lock.unlock.massive.clients.wizard:clients_administration.lock_unlock_massive_clients_wizard
msgid "Lock massive clients"
msgstr "Bloqueo masivo clientes"

#. module: clients_administration
#: model:ir.actions.act_window,name:clients_administration.action_to_lock_unlock_clients
msgid "Lock or Activate clients"
msgstr "Bloquear o Activar clientes"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:43
#: field:catalogue.causes,lock_parameter_catalogue:0
#, python-format
msgid "Lock parameter"
msgstr "Parámetro de bloqueo"

#. module: clients_administration
#: code:addons/clients_administration/models/account_invoice.py:24
#, python-format
msgid "Lock partner!"
msgstr "Cliente bloqueado!"

#. module: clients_administration
#: model:ir.ui.menu,name:clients_administration.menu_lock_or_unlock_client
#: view:lock.unlock.client:clients_administration.view_form_to_lock_unlock_client
msgid "Lock/Unlock"
msgstr "Bloqueo y Activacion"

#. module: clients_administration
#: model:ir.ui.menu,name:clients_administration.menu_lockunlock_analisys
#: view:lock.unlock.client:clients_administration.view_tree_lockunlock_history
msgid "History of locks"
msgstr "Historial de bloqueos"

#. module: clients_administration
#: model:ir.ui.menu,name:clients_administration.menu_clients_administration
msgid "Lock/Unlock clients"
msgstr "Bloqueo o activacion de clientes"

#. module: clients_administration
#: model:ir.module.category,name:clients_administration.lock_unlock_client_categ
msgid "Lock/unlock"
msgstr "Bloqueo o activacion de clientes"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:65
#: code:addons/clients_administration/models/res_partner_inherit.py:16
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:21
#: selection:lock.unlock.client,state:0
#: selection:lock.unlock.clients.report.wizard,status_client_parameter:0
#: field:res.partner,status_client:0
#, python-format
msgid "Locked"
msgstr "Bloqueado"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:287
#, python-format
msgid "Locked clients"
msgstr "Clientes bloqueados"

#. module: clients_administration
#: model:ir.ui.menu,name:clients_administration.menu_lock_or_unlock_report_client
#: view:lock.unlock.clients.report.wizard:clients_administration.lock_unlock_clients_report_wizard
#: view:print.lock.unlock.clients.report.wizard:clients_administration.print_lock_unlock_clients_report_wizard
msgid "Locked clients report"
msgstr "Reporte de clientes bloqueados"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:10
#: model:ir.model,name:clients_administration.model_lock_unlock_client
#, python-format
msgid "Locking or activation of clients"
msgstr "Bloqueo o activavion de clientes"

#. module: clients_administration
#: view:lock.unlock.massive.clients.wizard:clients_administration.lock_unlock_massive_clients_wizard
msgid "Massive clients lock"
msgstr "Bloqueo masivo de clientes"

#. module: clients_administration
#: field:catalogue.causes,message_ids:0 field:lock.unlock.client,message_ids:0
msgid "Messages"
msgstr "Mensajes"

#. module: clients_administration
#: help:catalogue.causes,message_ids:0 help:lock.unlock.client,message_ids:0
msgid "Messages and communication history"
msgstr "Mensajes e historial de comunicación"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:17
#: field:catalogue.causes,name:0
#, python-format
msgid "Name"
msgstr "Nombre"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:130
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:44
#, python-format
msgid "No client"
msgstr "Número de cliente"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:80
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:295
#, python-format
msgid "No records found!"
msgstr "¡No se encontrarón registros!"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:72
#: field:lock.unlock.client,no_client:0
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "No. Client"
msgstr "Número de cliente"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:38
#: field:lock.unlock.client,observations:0
#, python-format
msgid "Observations"
msgstr "Observaciones"

#. module: clients_administration
#: view:lock.unlock.clients.report.wizard:clients_administration.lock_unlock_clients_report_wizard
msgid "PDF File"
msgstr "Archivo PDF"

#. module: clients_administration
#: model:ir.model,name:clients_administration.model_res_partner
msgid "Partner"
msgstr "Empresa"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:58
#: field:lock.unlock.client,payment_deadline:0
#, python-format
msgid "Payment deadline"
msgstr "Tiempo límite de pago"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:28
#: field:lock.unlock.client,payment_type:0
#, python-format
msgid "Payment type"
msgstr "Tipo de pago"

#. module: clients_administration
#: model:ir.actions.act_window,name:clients_administration.action_to_print_report
msgid "Print Report csv"
msgstr "Imprimir reporte csv"

#. module: clients_administration
#: model:ir.actions.report.xml,name:clients_administration.locked_unlocked_report_pdf_action
msgid "Print report locked clients"
msgstr "Reporte de clientes bloqueados"

#. module: clients_administration
#: model:ir.model,name:clients_administration.model_sale_order
msgid "Sales Order"
msgstr "Pedido de venta"

#. module: clients_administration
#: view:lock.unlock.clients.report.wizard:clients_administration.lock_unlock_clients_report_wizard
msgid "Screen"
msgstr "Pantalla"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:30
#: field:lock.unlock.clients.report.wizard,start_date:0
#, python-format
msgid "Start date"
msgstr "Fecha de inicio"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:32
#: help:lock.unlock.clients.report.wizard,start_date:0
#, python-format
msgid "Start date to consider for the filter"
msgstr "Fecha de inicio para considerar para el filtro"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:44
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:39
#: help:lock.unlock.clients.report.wizard,no_client_start:0
#: help:lock.unlock.massive.clients.wizard,no_client_start:0
#, python-format
msgid "Start of the range to apply the blockade"
msgstr "Inicio del rango para aplicar el bloqueo"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:26
#: help:lock.unlock.clients.report.wizard,status_client_parameter:0
#, python-format
msgid "State of client to consider for the filter"
msgstr "Estado del cliente a considerar para el filtro"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:133
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:47
#: field:lock.unlock.client,state:0 field:lock.unlock.history,state:0
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "Status"
msgstr "Estado"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:25
#: field:lock.unlock.clients.report.wizard,status_client_parameter:0
#, python-format
msgid "Status parameter"
msgstr "Estatus del Parámetro"

#. module: clients_administration
#: field:catalogue.causes,message_summary:0
#: field:lock.unlock.client,message_summary:0
msgid "Summary"
msgstr "Resumen"

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:282
#, python-format
msgid ""
"The cause of blocking is not valid\n"
"                    for the massive blockade!"
msgstr ""
"La causa del bloqueo no es válida\n"
"                    para el bloqueo masivo"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:165
#, python-format
msgid ""
"The client already has a\n"
"                    lock/unlock document[' {} '], please follow up on it"
msgstr ""
"El cliente ya tiene un\n"
"                    documento de bloqueo/activación[' {} '], por favor use el mismo"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:183
#, python-format
msgid ""
"The client already has a\n"
"                lock/unlock document['{}'], please follow up on it"
msgstr ""
"El cliente ya tiene un\n"
"                    documento de bloqueo ['{}'], por favor, sigue en ello"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:272
#, python-format
msgid ""
"To Activate the user you must change\n"
"                    the type of action and choose a corresponding cause"
msgstr ""
"Para Activar al cliente debes cambiar\n"
"                    el tipo de acción y elige una causa correspondiente"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:209
#, python-format
msgid ""
"To block the user you must change\n"
"                    the type of action and choose a corresponding cause"
msgstr ""
"Para bloquear al cliente debes cambiar\n"
"                    el tipo de acción y elige una causa correspondiente"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:35
#: field:catalogue.causes,to_massive:0
#, python-format
msgid "To massive"
msgstr "Para masivo"

#. module: clients_administration
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:48
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:43
#: field:lock.unlock.clients.report.wizard,no_client_end:0
#: field:lock.unlock.massive.clients.wizard,no_client_end:0
#, python-format
msgid "To:"
msgstr "Hasta:"

#. module: clients_administration
#: help:sale.order,order_is_web:0
msgid ""
"Todos los pedidos que sean realizados por medio del ecommerce               via "
"API, activara este campo para distinguirlos de los pedidos               "
"realizados por algun operador interno."
msgstr ""
"Todos los pedidos que sean realizados por medio del ecommerce               via "
"API, activara este campo para distinguirlos de los pedidos               "
"realizados por algun operador interno."

#. module: clients_administration
#: code:addons/clients_administration/wizard/models/lock_unlock_massive_clients_wizard.py:34
#: help:lock.unlock.massive.clients.wizard,tolerance_amount:0
#, python-format
msgid "Tolerance amount "
msgstr "Monto de tolerancia"

#. module: clients_administration
#: code:addons/clients_administration/models/catalogue_of_causes.py:29
#: help:catalogue.causes,description:0
#, python-format
msgid "Type of cause description"
msgstr "Descripción del tipo de causa"

#. module: clients_administration
#: view:catalogue.causes:clients_administration.view_form_catalogue_causes_to_lock_unlock
msgid "Type of cause of blocking or activation"
msgstr "Tipo de causa de bloqueo o activación"

#. module: clients_administration
#: code:addons/clients_administration/models/account_invoice.py:34
#: code:addons/clients_administration/models/sale_order.py:27
#, python-format
msgid "Unable to create invoice for this client because it is blocked!"
msgstr "¡No se puede crear una factura para este cliente porque está bloqueado!"

#. module: clients_administration
#: field:catalogue.causes,message_unread:0 field:lock.unlock.client,message_unread:0
msgid "Unread Messages"
msgstr "Mensajes no leídos"

#. module: clients_administration
#: code:addons/clients_administration/models/lock_or_unlock_client.py:77
#: code:addons/clients_administration/models/lock_or_unlock_client.py:234
#: code:addons/clients_administration/reports/wizard/models/lock_unlock_clients_report_wizard.py:137
#: code:addons/clients_administration/reports/wizard/models/print_report_csv.py:51
#: field:lock.unlock.client,user:0 field:lock.unlock.history,user:0
#: view:website:clients_administration.locked_clients_massive_report_pdf
#, python-format
msgid "User"
msgstr "Usuario"

#. module: clients_administration
#: field:sale.order,order_is_web:0
msgid "Via Web"
msgstr "Via Web"

#. module: clients_administration
#: view:res.partner:clients_administration.todo_add_res_partner_status_field_form_view
msgid "{'invisible' : False}"
msgstr "{'invisible' : False}"
