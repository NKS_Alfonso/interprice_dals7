# -*- coding: utf-8 -*-
import ast
from openerp import models, fields, api
import cStringIO
from base64 import encodestring



class AccountAccountingPlanReport(models.Model):
    _inherit = 'account.chart'
    file = ''

    def account_chart_export_xls(self, cr, uid, ids, context=None):
        _filter = self.account_chart_open_window(cr, uid, ids, context)
        _context_open_chart = _filter['context']
        context.update(ast.literal_eval(_context_open_chart))
        context.update({'search_disable_custom_filters': True})
        ids_accounts = [0]
        self.file = ',Nombre,,,,,Debe,Haber\n'
        self.isTwoLevel = 0
        account_account = self.pool['account.account']
        domain = [('parent_id', '=', ids_accounts), ('active', '=', 1)]
        final_result = self._account_financial(cr, uid, context=context, account=account_account, domain=domain, num=0)
        buf = cStringIO.StringIO()
        buf.write(self.file)
        out = encodestring(buf.getvalue())
        buf.close()
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download_accounting_plan?model=wizard.account.chart.report&field=%s&filename=accounting_plan.csv' % (out),
            'target': 'self',
        }

    def _account_financial(self, cr, uid, context, account, domain, num=0):
        ids_accounts = account.search(cr, uid, domain, context=None)
        accounts = None
        self.isTwoLevel += 1
        num += 1
        if len(ids_accounts) > 0:
            accounts = self.pool.get('account.account').compute_account_account(
                cr,
                uid,
                ids_accounts,
                ['credit', 'debit', 'balance'],
                arg=None,
                context=context)

            for id in accounts:
                if self.isTwoLevel == 1:
                    domain = [('parent_id', '=', id), ('active', '=', 1), ('code', 'in', [1, 2, 3])]
                else:
                    domain = [('parent_id', '=', id), ('active', '=', 1)]
                #  5 is the max level account
                j = 5-num
                s = ''
                if j != 0:
                    s = ',' * j
                g = ',' * num

                acc_name = self.pool.get('account.account').browse(cr, uid, id, context=None)
                # if num == 2:
                #     self.file =self.file + ',,,\n'
                self.file = self.file + g + unicode(acc_name.display_name).encode("latin-1")+','+ s + str(accounts[id]['debit']) + ',' + str(accounts[id]['credit']) + "\n"

                v = self._account_financial(
                    cr=cr,
                    uid=uid,
                    context=context,
                    account=account,
                    domain=domain,
                    num=num,
                    # file=file
                )
                accounts[id].update({'hijo': v})
        # if accounts == None:
        #     c  = 10
        return accounts