# -*- coding: utf-8 -*-
from openerp import http
from openerp.addons.web.controllers.main import content_disposition
from openerp.http import request
import base64


class AccountAccountingPlanReport(http.Controller):
    @http.route('/web/binary/download_accounting_plan', auth='public')
    def index(self, model, field, filename, **kw):
        filecontent = base64.b64decode(field)
        return request.make_response(filecontent,
                                     [('Content-Type', 'application/octet-stream'),
                                      ('Content-Disposition', content_disposition(filename))])
