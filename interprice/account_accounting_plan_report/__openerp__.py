# -*- coding: utf-8 -*-
{
    'name': "Plan contable | Posicion financiera",

    'description': """
        Report
    """,

    'author': "Jose Juan Hernanez Bautista (GVADETO)",
    'website': "http://www.grupovadeto.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Reporte',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'account'
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'account_chart_form.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo.xml',
    ],
}