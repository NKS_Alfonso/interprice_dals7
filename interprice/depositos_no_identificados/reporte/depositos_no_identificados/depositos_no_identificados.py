# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
from openerp.osv import osv
import openerp.tools as tools
from olib.oCsv.oCsv import OCsv


class ReportsDepositosNoIdentificados(osv.osv_memory):
    _name = 'r.depositos_no_identificados'

    date_from = fields.Datetime(string=_('Fecha inicio'))
    date_to = fields.Datetime(string=_('Fecha fin'))
    state = fields.Selection([
        ('Generado', 'Generado'),
        ('Asociado', 'Asociado'),
        ('Aplicado parcial', 'Aplicado parcial'),
        ('Aplicado', 'Aplicado'),
        ('Cancelado', 'Cancelado')],
        string='Estado')
    bank_id = fields.Many2one(
        'res.partner.bank', string='Banco', domain=[])
    partner_id = fields.Many2one(
        'res.partner', string='Cliente',
        domain=[('customer', '=', True),
                ('is_company', '=', True)]
    )

    def _apply_filter(self, cr, uid, ids, context=None, type=None):
        dataset_ids = []
        data = self.browse(cr, uid, ids, context=None)
        istate_id = ''
        ibank_id = ''
        and_partner_id = ''
        if data.state:
            istate_id = "and state = '%s' " % (data.state,)
        if data.bank_id:
            ibank_id = "and bank_name_id = %s " % (data.bank_id.id,)
        if data.partner_id:
            and_partner_id = "and partner_id = %s " % (data.partner_id.id,)
        query = "select id from r_depositos_no_identificados_data where registration_date IS NOT NULL and registration_date >='%s' " \
                "and registration_date <= '%s' %s %s %s" \
                % (data.date_from, data.date_to, istate_id, ibank_id,
                   and_partner_id
                   )
        cr.execute(query)
        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            if type == 'show':
                cr.execute(
                    'delete from r_depositos_no_identificados where id=%s',
                    (ids[0],)
                )
        else:
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids

    def show(self, cr, uid, ids, context=None):
        data_ids = self._apply_filter(cr, uid, ids, context, type='show')
        return {
            'name': 'Depósitos no Identificados',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.depositos.no.identificados.data',
            'target': 'current',
            'domain': [('id', 'in', data_ids)],
        }

    def csv(self, cr, uid, ids, context=None):
        dataset_ids = self._apply_filter(cr, uid, ids, context, type='csv')
        id = self.pool['r.depositos.no.identificados.data'].to_csv_by_ids(
            cr, uid, dataset_ids, context=context
        )
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id),
            'target': 'self',
        }

    def onchange_partner_id(
            self, cr, uid, ids, state, partner_id, context=None
    ):
        values = {}
        user = uid
        company = self.pool.get('res.users').browse(
            cr, uid, user, context=context
        )
        if company:
            company_id = company.company_id.id
        else:
            company_id = []
        values.update({
            'domain':
                {
                    'bank_id': [('company_id', '=', company_id)]
                },
                       })
        if state == 'Generado':
            values.update({
                'value':
                    {
                        'state': 0,
                        'partner_id': 0,
                    },
            })
        return values

    def onchange_state(self, cr, uid, ids, state, partner_id, context=None):
        values = {}
        if state:
            if state in ('Generado') and partner_id:
                values.update({
                    'value':
                        {
                            'state': 0,
                            'partner_id': 0,
                        },
                })
        return values


class ReportsDepositosNoIdentificadosData(models.Model):
    _name = 'r.depositos.no.identificados.data'
    _table = 'r_depositos_no_identificados_data'
    _auto = False
    _order = 'id desc'

    dni_name = fields.Char(string=_('Folio'))
    bank_name = fields.Char(string=_('Banco'))
    account_bank_name = fields.Char(string=_('Cuenta'))
    amount = fields.Float(string=_('Importe'), digits=(10, 2))
    currency = fields.Char(string=_('Moneda'))
    deposit_date = fields.Datetime(string=_('Fecha de Depósito'))
    registration_date = fields.Datetime(string=_('Fecha de Registro'))
    user = fields.Char(string=_('Usuario'))
    state = fields.Char(string=_('Estado'))
    create_poliza = fields.Char(string=_('Póliza de creación'))
    cancel_poliza = fields.Char(string=_('Póliza de cancelación'))
    partner = fields.Char(string=_('Cliente'))
    apply_pay = fields.Char(string=_('Aplicado a'))
    apply_user = fields.Char(string=_('Usuario Aplico'))
    apply_date = fields.Datetime(string=_('Fecha de aplicación'))
    cancel_user = fields.Char(string=_('Usuario Cancelo'))
    cancel_date = fields.Datetime(string=_('Fecha de cancelación'))
    cancel_reason = fields.Char(string=_('Motivo Cancelación'))

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_depositos_no_identificados_data')
        cr.execute("""
            create or replace view r_depositos_no_identificados_data as (
                SELECT
                  row_number() over (order by T1.registration_date) as id,
                  T1.dni_name,
                  T1.bank_name,
                  T1.bank_name_id,
                  T1.account_bank_name,
                  T1.amount,
                  T1.currency,
                  T1.deposit_date,
                  T1.registration_date,
                  T1.user,
                  T1.user_id,
                  CASE T1.state
                    WHEN 'darft' THEN 'Borrador'
                    WHEN 'generated' THEN 'Generado'
                    WHEN 'deassociated' THEN 'Desasociado'
                    WHEN 'associated' THEN 'Asociado'
                    WHEN 'partial_applied' THEN 'Aplicado parcial'
                    WHEN 'applied' THEN 'Aplicado'
                    WHEN 'cancel' THEN 'Cancelado'
                  ELSE '' END state,
                  T1.create_poliza,
                  T1.cancel_poliza,
                  T1.partner,
                  T1.partner_id,
                  T1.apply_pay,
                  T1.apply_user,
                  T1.apply_user_id,
                  T1.apply_date,
                  T1.cancel_user,
                  T1.cancel_user_id,
                  T1.cancel_date,
                  T1.cancel_reason
                 FROM (
                        SELECT
                         dd.name AS dni_name,
                         rpb.bank_name AS bank_name,
                         rpb.id AS bank_name_id,
                         rpb.acc_number AS account_bank_name,
                         dd.amount AS amount,
                         rc.name AS currency,
                         dd.deposit_date AS deposit_date,
                         dd.registration_date AS registration_date,
                         CASE
                            WHEN dd.state != 'draft' THEN (SELECT rp.name FROM res_partner rp WHERE rp.id =
                            (SELECT ru.partner_id FROM res_users ru WHERE ru.id =
                            (SELECT ddm.user_id FROM dni_dni_movements AS ddm WHERE ddm.dni_id = dd.id AND ddm.state = dd.state ORDER BY ddm.create_date LIMIT 1)))
                         ELSE '' END AS user,
                         CASE
                            WHEN dd.state != 'draft' THEN
                            (SELECT ru.id FROM res_users ru WHERE ru.id =
                            (SELECT ddm.user_id FROM dni_dni_movements AS ddm WHERE ddm.dni_id = dd.id AND ddm.state = dd.state ORDER BY ddm.create_date LIMIT 1))
                         ELSE NULL END AS user_id,
                         dd.state AS state,
                         CASE
                            WHEN dd.partner_id IS NOT NULL THEN (SELECT rp.name FROM res_partner rp WHERE rp.id = dd.partner_id)
                         ELSE '' END AS partner,
                         dd.partner_id AS partner_id,
                         CASE
                            WHEN dda.account_voucher_id IS NOT NULL THEN (SELECT av.number FROM account_voucher av WHERE av.id = dda.account_voucher_id)
                         ELSE '' END AS apply_pay,
                         CASE
                            WHEN dd.policy_create_id IS NOT NULL THEN (SELECT am.name FROM account_move am WHERE am.id = dd.policy_create_id)
                         ELSE '' END AS create_poliza,
                         CASE
                            WHEN dd.policy_cancel_id IS NOT NULL THEN (SELECT am.name FROM account_move am WHERE am.id = dd.policy_cancel_id)
                         ELSE '' END AS cancel_poliza,
                         CASE
                            WHEN dda.user_id IS NOT NULL AND dda.state in ('aplicated', 'completed') THEN (SELECT rp.name FROM res_partner rp WHERE rp.id =
                            (SELECT ru.partner_id FROM res_users ru WHERE ru.id = dda.user_id))
                         ELSE '' END AS apply_user,
                         CASE
                            WHEN dda.state = 'aplicated' THEN dda.user_id
                         ELSE null END AS apply_user_id,
                         CASE
                            WHEN dda.create_date IS NOT NULL AND dda.state in ('aplicated', 'completed') THEN dda.write_date
                         ELSE null END AS apply_date,
                         CASE
                            WHEN dda.user_id IS NOT NULL AND dda.state = 'cancel' THEN (SELECT rp.name FROM res_partner rp WHERE rp.id =
                            (SELECT ru.partner_id FROM res_users ru WHERE ru.id = dda.user_id))
                         ELSE '' END AS cancel_user,
                         CASE
                            WHEN dda.user_id IS NOT NULL AND dda.state = 'cancel' THEN dda.user_id
                         ELSE null END AS cancel_user_id,
                         CASE
                            WHEN dda.create_date IS NOT NULL AND dda.state = 'cancel' THEN dda.write_date
                         ELSE null END AS cancel_date,
                         CASE
                            WHEN dd.state = 'cancel' THEN dd.note_cancel
                         ELSE '' END AS cancel_reason

                        FROM
                         dni_dni AS dd
                         INNER JOIN res_partner_bank AS rpb ON rpb.id = dd.bank_id
                         INNER JOIN res_currency AS rc ON rc.id = rpb.currency2_id
                         INNER JOIN account_move AS am_create ON am_create.id = dd.policy_create_id
                         LEFT JOIN account_move AS am_cancel ON am_cancel.id = dd.policy_create_id
                         LEFT JOIN res_partner AS rp ON rp.id = dd.partner_id
                         LEFT JOIN dni_dni_aplicated AS dda ON dda.dni_id = dd.id
                        WHERE (dd.state NOT IN ('draft') AND dda.account_voucher_id IS NULL)
                UNION ALL
                       SELECT
                         dd.name AS dni_name,
                         rpb.bank_name AS bank_name,
                         rpb.id AS bank_name_id,
                         rpb.acc_number AS account_bank_name,
                         dd.amount AS amount,
                         rc.name AS currency,
                         dd.deposit_date AS deposit_date,
                         dd.registration_date AS registration_date,
                         CASE
                            WHEN dd.state != 'draft' THEN (SELECT rp.name FROM res_partner rp WHERE rp.id =
                            (SELECT ru.partner_id FROM res_users ru WHERE ru.id =
                            (SELECT ddm.user_id FROM dni_dni_movements AS ddm WHERE ddm.dni_id = dd.id AND ddm.state = dd.state ORDER BY ddm.create_date LIMIT 1)))
                         ELSE '' END AS user,
                         CASE
                            WHEN dd.state != 'draft' THEN
                            (SELECT ru.id FROM res_users ru WHERE ru.id =
                            (SELECT ddm.user_id FROM dni_dni_movements AS ddm WHERE ddm.dni_id = dd.id AND ddm.state = dd.state ORDER BY ddm.create_date LIMIT 1))
                         ELSE NULL END AS user_id,
                         dd.state AS state,
                         CASE
                            WHEN dd.partner_id IS NOT NULL THEN (SELECT rp.name FROM res_partner rp WHERE rp.id = dd.partner_id)
                         ELSE '' END AS partner,
                         dd.partner_id AS partner_id,
                         CASE
                            WHEN dda.account_voucher_id IS NOT NULL THEN (SELECT av.number FROM account_voucher av WHERE av.id = dda.account_voucher_id)
                         ELSE '' END AS apply_pay,
                         CASE
                            WHEN dd.policy_create_id IS NOT NULL THEN (SELECT am.name FROM account_move am WHERE am.id = dd.policy_create_id)
                         ELSE '' END AS create_poliza,
                         CASE
                            WHEN dd.policy_cancel_id IS NOT NULL THEN (SELECT am.name FROM account_move am WHERE am.id = dd.policy_cancel_id)
                         ELSE '' END AS cancel_poliza,
                         CASE
                            WHEN dda.user_id IS NOT NULL AND dda.state in ('aplicated','completed') THEN (SELECT rp.name FROM res_partner rp WHERE rp.id =
                            (SELECT ru.partner_id FROM res_users ru WHERE ru.id = dda.user_id))
                         ELSE '' END AS apply_user,
                         CASE
                            WHEN dda.state = 'aplicated' THEN dda.user_id
                         ELSE null END AS apply_user_id,
                         CASE
                            WHEN dda.create_date IS NOT NULL AND dda.state in ('aplicated','completed') THEN dda.write_date
                         ELSE null END AS apply_date,
                         CASE
                            WHEN dda.user_id IS NOT NULL AND dda.state = 'cancel' THEN (SELECT rp.name FROM res_partner rp WHERE rp.id =
                            (SELECT ru.partner_id FROM res_users ru WHERE ru.id = dda.user_id))
                         ELSE '' END AS cancel_user,
                         CASE
                            WHEN dda.user_id IS NOT NULL AND dda.state = 'cancel' THEN dda.user_id
                         ELSE null END AS cancel_user_id,
                         CASE
                            WHEN dda.create_date IS NOT NULL AND dda.state = 'cancel' THEN dda.write_date
                         ELSE null END AS cancel_date,
                         CASE
                            WHEN dd.state = 'cancel' THEN dd.note_cancel
                         ELSE '' END AS cancel_reason

                        FROM
                         dni_dni_aplicated AS dda
                         INNER JOIN dni_dni AS dd ON dd.id = dda.dni_id
                         INNER JOIN res_partner_bank AS rpb ON rpb.id = dd.bank_id
                         INNER JOIN res_currency AS rc ON rc.id = rpb.currency2_id
                         LEFT JOIN res_partner AS rp ON rp.id = dd.partner_id
                        WHERE dda.account_voucher_id IS NOT NUll
                     )AS T1
            )
        """)

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    def to_csv_by_ids(self, cr, uid, ids, context=None):
        dataset = self.pool['r.depositos.no.identificados.data'].browse(
            cr, uid, ids, context=context
        )
        dataset_csv = [
            [
                'Folio',
                'Cliente',
                'Banco',
                'Cuenta',
                'Importe',
                'Moneda',
                unicode('Fecha de Ingreso').encode('utf-8'),
                unicode('Fecha de Registro').encode('utf-8'),
                'Usuario',
                'Estado',
                unicode('Póliza de creación').encode('utf-8'),
                unicode('Póliza de cancelación').encode('utf-8'),
                unicode('Motivo de Cancelación').encode('utf-8'),
                'Aplicado a',
                'Usuario Aplico',
                unicode('Fecha de Aplicación').encode('utf-8'),
                'Usuario Cancelo',
                unicode('Fecha de Cancelación').encode('utf-8')
            ]
        ]
        for row in dataset:

            dataset_csv.append(
                [
                    unicode(self.set_default(row.dni_name)).encode('utf-8'),
                    unicode(self.set_default(row.partner)).encode('utf-8'),
                    unicode(self.set_default(row.bank_name)).encode('utf-8'),
                    unicode(self.set_default(row.account_bank_name)).encode('utf-8'),
                    unicode(self.set_default(row.amount, 0)).encode('utf-8'),
                    unicode(self.set_default(row.currency)).encode('utf8'),
                    str(self.utc_mexico_city(row.deposit_date) if row.deposit_date else '').encode('utf-8'),
                    str(self.utc_mexico_city(row.registration_date) if row.registration_date else '').encode('utf-8'),
                    unicode(self.set_default(row.user)).encode('utf-8'),
                    unicode(self.set_default(row.state)).encode('utf-8'),
                    unicode(self.set_default(row.create_poliza)).encode('utf-8'),
                    unicode(self.set_default(row.cancel_poliza)).encode('utf-8'),
                    unicode(self.set_default(row.cancel_reason)).encode('utf-8'),
                    unicode(self.set_default(row.apply_pay)).encode('utf-8'),
                    unicode(self.set_default(row.apply_user)).encode('utf-8'),
                    str(self.utc_mexico_city(row.apply_date) if row.apply_date else '').encode('utf-8'),
                    unicode(self.set_default(row.cancel_user)).encode('utf-8'),
                    str(self.utc_mexico_city(row.cancel_date) if row.cancel_date else '').encode('utf-8')
                    ]
            )
        obj_csv = OCsv()
        file = obj_csv.csv_base64(dataset_csv)
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': 'Depositos_no_identificados.csv',
            'type_file': 'csv',
            'file': file,
        }, context=None)

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
