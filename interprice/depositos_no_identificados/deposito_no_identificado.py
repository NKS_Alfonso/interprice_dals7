# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
from openerp.exceptions import Warning
from datetime import datetime
from openerp.osv import osv
import time


class DepositoNoIdentificado(models.Model):

    # odoo model properties
    _name = "dni.dni"
    _description = "Depositos no identificados"

    # initial domain methods
    @api.onchange('amount')
    @api.depends('amount')
    def _compute_tax(self):
        """Simple method"""
        self.tax = round(self.amount - (self.amount / (1 + self._get_tax().amount)), 4)

    @api.depends('tax', 'amount')
    def _compute_balance(self):
        """Simple method"""
        self.initial_balance = self.amount - self.tax

    def get_company(self):
        """Returns the company."""
        return self.env[
            'res.users'].browse(
            self._uid).company_id

    def _get_tax(self):
        """Returns the tax established in the accounting
        configuration of the company."""
        sale_tax = self.env['account.config.settings'].search([
            ('company_id', '=', self.get_company().id)],
            limit=1, order='create_date desc').default_sale_tax
        if len(sale_tax) == 0:
            raise Warning(
                _("Es necesario guardar los ajustes de contabilidad en configuración")
            )
        return sale_tax

    @api.depends('tax', 'amount')
    def _compute_current_balance(self):
        for dni in self:
            total_amount = 0.0
            for line in dni.dni_aplicated_ids:
                if line.state == 'aplicated':
                    total_amount += abs(line.amount)
            dni.current_balance = round(dni.amount - total_amount, 4)

    # custom fields
    name = fields.Char(string="Número",
                       default="Borrador")
    bank_id = fields.Many2one(
        'res.partner.bank', string="Banco",
        readonly=True, required=True
        )
    partner_id = fields.Many2one(
        'res.partner', string="Cliente",
        domain=[('customer', '=', True),
                ('is_company', '=', True)],
        readonly=True, required=False
        )
    currency_id = fields.Many2one(
        'res.currency', string="Moneda",
        readonly=True, required=True
        )
    journal_id = fields.Many2one(
        'account.journal', string="Diario",
        readonly=True, required=True
    )
    amount = fields.Float(
        string="Monto", readonly=True, required=True
        )
    deposit_date = fields.Datetime(
        string="Fecha de depósito", readonly=True,
        required=True, default=fields.Datetime.now
        )
    registration_date = fields.Datetime(
        string="Fecha de registro"
        )
    validated_date = fields.Datetime(
        string="Fecha de validación"
        )
    dni_movements_ids = fields.One2many(
        'dni.dni.movements', 'dni_id',
        readonly=True, string="Movimientos"
        )
    dni_aplicated_ids = fields.One2many(
        'dni.dni.aplicated', 'dni_id',
        readonly=True, string="Aplicado a"
        )
    policy_create_id = fields.Many2one(
        'account.move', string="Póliza de creación",
        readonly=True
        )
    policy_cancel_id = fields.Many2one(
        'account.move', string="Póliza de cancelación",
        readonly=True
        )
    note_cancel = fields.Text(
        size=300, string="Nota",
        readonly=True
        )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('generated', 'Generado'),
        ('associated', 'Asociado'),
        ('partial_applied', 'Aplicado parcial'),
        ('applied', 'Aplicado'),
        ('cancel', 'Cancelado')],
        'Estado',
        default="draft",
        readonly=True
        )
    tax = fields.Float(
        digits=(12, 2),
        readonly=True,
        compute=_compute_tax,
        store=True,
        string="- Impuesto",
        help="Impuesto del Depósito.",
    )
    initial_balance = fields.Float(
        digits=(12, 2),
        readonly=True,
        compute=_compute_balance,
        string="Saldo Inicial",
        help="Saldo inicial del Depósito.",
    )
    current_balance = fields.Float(
        readonly=True,
        compute=_compute_current_balance,
        string=_("Current Balance"),
        help=_("Current balance, this is a result of debiting "
               "the applications of the unidentified deposit to the opening balance."),
    )
    usd_rate = fields.Float(
        string="Saved usd rate"
    )

    _order = 'create_date desc, id desc'

    def draft(self, cr, uid, ids, context=None):
        self.write(
            cr, uid, ids, {'state': 'draft'},
            context=context
        )
        return True

    def generated(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)
        if data.amount <= 0.00:
            raise osv.except_osv(
                _('¡ERROR!'),
                _('¡El monto no puede ser menor o igual a 0.00!')
            )
        new_name = self.generate(cr, uid, ids, context=context)
        if new_name:
            if data:
                self.write(
                    cr, uid, ids, {'registration_date': data.create_date},
                    context=context
                )
                values = {}
                values.update({'state': 'generated'})
                values = self.values_movements(cr, uid, ids, values,
                                               context=context
                                               )
                if values:
                    new_dni_movements = self.pool.get(
                        'dni.dni.movements').create(
                        cr, uid, values, context=context
                        )
                    if new_dni_movements:
                        self.write(
                            cr, uid, ids, {'state': 'generated',
                                           'name': new_name},
                            context=context
                        )
                        value = self.create_policy(
                            cr, uid, ids, context=context
                        )
                        if value:
                            self.write(
                                cr, uid, ids, {
                                    'policy_create_id': value['res_id'][0]
                                },
                                context=context
                            )
                            if value['res_id']:
                                account_move = self.pool.get(
                                    'account.move').browse(
                                    cr, uid, value['res_id'][0],
                                    context=context
                                )
                                account_move_line = self.pool.get(
                                    'account.move.line').search(
                                    cr, uid, [
                                        ('move_id', '=', account_move.id),
                                        ('name', '!=', '/')
                                    ], context=context
                                )
                                for move_line in self.pool.get('account.move.line').browse(
                                        cr, uid, account_move_line, context=context):
                                    move_line.write(
                                        {
                                            'name': account_move.name
                                        }
                                    )
                                return True
        return False

    def associated(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)
        if not data.partner_id:
            raise osv.except_osv('¡ERROR!', 'Debes seleccionar el Cliente')
        if data:
            self.write(cr, uid, ids, {
                'registration_date': data.create_date
                }, context=context
                       )
            values = {}
            values.update({'state': 'associated'})
            values = self.values_movements(
                cr, uid, ids, values, context=context
            )
            if values:
                new_dni_movements = self.pool.get(
                    'dni.dni.movements').create(
                    cr, uid, values, context=context
                )
                if new_dni_movements:
                    self.write(cr, uid, ids, {'state': 'associated'},
                               context=context
                               )
                    account_move_line_ids = self.pool.get(
                        'account.move.line').search(
                        cr, uid, [
                            ('move_id', '=', data.policy_create_id.id)
                        ],
                        context=context
                    )
                    if account_move_line_ids:
                        for line in account_move_line_ids:
                            self.pool.get('account.move.line').write(
                                cr, uid, line, {
                                    'partner_id': data.partner_id.id
                                },
                                context=context
                            )
                        return True
        return False

    def disassociate(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)
        if data:
            self.write(cr, uid, ids, {
                'registration_date': data.create_date
            }, context=context
                       )
            values = {}
            values.update({'state': 'generated'})
            values = self.values_movements(cr, uid, ids, values,
                                           context=context
                                           )
            if values:
                new_dni_movements = self.pool.get(
                    'dni.dni.movements').create(cr, uid, values,
                                                context=context
                                                )
                if new_dni_movements:
                    self.write(cr, uid, ids, {'state': 'generated'},
                               context=context
                               )
                    account_move_line_ids = self.pool.get(
                        'account.move.line').search(
                        cr, uid, [
                            ('move_id', '=', data.policy_create_id.id)],
                        context=context
                    )
                    if account_move_line_ids:
                        for line in account_move_line_ids:
                            self.pool.get('account.move.line').write(
                                cr, uid, line, {'partner_id': 0},
                                context=context
                            )
                        self.write(cr, uid, ids, {'partner_id': 0},
                                   context=context
                                   )
                    return True
        return False

    def applied(self, cr, uid, ids, value, context=None):
        data = self.browse(cr, uid, ids, context=context)
        if data:
            values = {}
            values.update({'state': 'applied'})
            values = self.values_movements(cr, uid, ids, values,
                                           context=context
                                           )
            if values:
                if 'ids' in value and 'reconcile' in value:
                    if 'amount' and 'currency' in value:
                        voucher_id = value['ids']
                        amount = self.converted_rate(cr, uid, ids,
                                                     value, context=context
                                                     )
                        if amount:
                            new_dni_aplicated = self.pool.get(
                                'dni.dni.aplicated').create(
                                cr, uid, {
                                    'dni_id': ids[0], 'user_id': uid,
                                    'account_voucher_id': voucher_id,
                                    'state': 'aplicated',
                                    'amount': -(amount)}, context=context
                            )
                            if new_dni_aplicated:
                                if value['reconcile']:
                                    self.write(cr, uid, ids, {
                                        'state': 'applied'},
                                               context=context
                                               )
                                    self.write(cr, uid, ids, {
                                        'validated_date': fields.Datetime.now()
                                        }, context=context
                                        )
                                    self.pool.get(
                                        'dni.dni.movements').create(
                                        cr, uid, values, context=context
                                    )
                                else:
                                    values.update(
                                        {'state': 'partial_applied'}
                                    )
                                    self.write(cr, uid, ids, {
                                        'state': 'partial_applied'
                                        }, context=context
                                               )
                                    self.write(cr, uid, ids, {
                                        'validated_date': fields.Datetime.now()
                                        }, context=context
                                               )
                                    self.pool.get('dni.dni.movements').create(
                                        cr, uid, values, context=context
                                    )
                                return True
        return False

    def converted_rate(self, cr, uid, ids, value, context=None):
        dni_data = self.browse(cr, uid, ids, context=context)
        if value['currency']:
            currency_pay = value['currency']
        else:
            currency_pay = dni_data.bank_id.company_id.currency_id.name

        if dni_data.currency_id.name != currency_pay:
            if value['alter_currency']:
                rate = value['alter_currency']
            else:
                cr.execute("""SELECT rcr.rate_sale
                            FROM res_currency_rate rcr
                            INNER JOIN res_currency rc on rc.id = rcr.currency_id
                            WHERE rc.name = 'USD' AND rcr.name <= %s
                            order by rcr.name desc LIMIT 1""",
                           (value['date_pay'],)
                           )
                print value['date_pay']
                if cr.rowcount:
                    rate = cr.fetchone()[0]
            if dni_data.currency_id.name == 'MXN' and currency_pay == 'USD':
                converted = round(round(1 / rate, 4) * value['amount'], 2)
            if dni_data.currency_id.name == 'USD' and currency_pay == 'MXN':
                converted = round(value['amount'] / round(1 / rate, 4), 2)
        if dni_data.currency_id.name == currency_pay:
            converted = round(value['amount'], 2)
        return converted

    def cancel(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)
        if data:
            dni_dni_aplicated_ids = self.pool.get('dni.dni.aplicated').search(
                cr, uid, [('dni_id', '=', data.id)], context=context
            )
            if dni_dni_aplicated_ids:
                dni_dni_aplicated_data_ids = self.pool.get(
                    'dni.dni.aplicated').browse(
                    cr, uid, dni_dni_aplicated_ids, context=context
                )
                for dni_dni_aplicated_data_id in dni_dni_aplicated_data_ids:
                    state = dni_dni_aplicated_data_id.account_voucher_id.state
                    if state == 'posted':
                        raise osv.except_osv(
                            _('¡ERROR!'),
                            _('¡No puedes cancelar un Depósito no Identificado que tenga pagos aplicados! Debes primero cancelar los pagos relacionados.')
                                             )
            if data.note_cancel:
                self.write(cr, uid, ids, {
                    'registration_date': data.create_date
                    }, context=context
                           )
                values = {}
                values.update({'state': 'cancel'})
                values = self.values_movements(cr, uid, ids, values,
                                               context=context
                                               )
                if values:
                    new_dni_movements = self.pool.get(
                        'dni.dni.movements').create(
                        cr, uid, values, context=context
                    )
                    if new_dni_movements:
                        self.write(cr, uid, ids, {'state': 'cancel'},
                                   context=context
                                   )
                        move_pool = self.pool.get('account.move')
                        new_cancel_policy = data.policy_create_id.copy()
                        move_id = self.create_cancel_policy(
                            cr, uid, new_cancel_policy, data.name,
                            data.policy_create_id.name, context=context
                        )
                        res = {'ref': move_pool.browse(
                            cr, uid, data.policy_create_id.id).ref + " Cancelado "
                               }
                        move_pool.write(cr, uid, data.policy_create_id.id, res)

                        if move_id:
                            self.write(cr, uid, ids, {
                                'policy_cancel_id': move_id
                                }, context=context
                                       )
                            account_move = self.pool.get(
                                'account.move').browse(
                                cr, uid, move_id,
                                context=context
                            )
                            account_move_line = self.pool.get(
                                'account.move.line').search(
                                cr, uid, [
                                    ('move_id', '=', account_move.id),
                                    ('name', '!=', '/')
                                ], context=context
                            )
                            self.pool.get('account.move.line').write(
                                cr, uid, account_move_line[0],
                                {'name': account_move.name}
                            )
                            return True
            else:
                raise osv.except_osv(
                    _('¡ERROR!'),
                    _('¡Rellena la nota con el motivo de cancelación!')
                    )
        return False

    def create_cancel_policy(self, cr, uid, move_id, number, policy, context=None):
        today = time.strftime('%Y-%m-%d')
        if datetime.now().month < 10:
            code_period = '0' + str(
                datetime.now().month
            ) + '/' + str(
                datetime.now().year
            )
        else:
            code_period = str(
                datetime.now().month
            ) + '/' + str(
                datetime.now().year
            )
        account_period = self.pool.get('account.period')
        period_ids = account_period.search(cr, uid, [
            ('name', '=', code_period),
            ('special', '=', False)
        ])
        move_pool = self.pool.get('account.move')
        move_pool.button_cancel(cr, uid, [move_id.id])
        object_move = move_pool.browse(cr, uid, move_id.id)
        for i in object_move.line_id:
            debit = i.debit
            credit = i.credit
            amount_currency = 0
            if i.amount_currency < 0:
                amount_currency = abs(i.amount_currency)
            elif i.amount_currency > 0:
                amount_currency = -i.amount_currency
            cr.execute(
                "update account_move_line set debit=%s,"
                "credit=%s, amount_currency=%s WHERE move_id=%s and id=%s",
                (credit, debit, amount_currency, move_id.id, i.id)
            )
        cad = "Cancelado " + number + " " + policy
        cr.execute('update account_move set ref=%s, period_id=%s, '
                   'date=%s where id=%s',
                   (cad, period_ids[0], today, move_id.id)
                   )
        move_pool.button_validate(cr, uid, [move_id.id])
        return move_id.id

    def values_movements(self, cr, uid, ids, values, context=None):
        if values:
            data = self.browse(cr, uid, ids, context=context)
            values.update({
                'user_id': uid,
                'dni_id': data.id
            })
        return values

    @api.multi
    def generate(self):
        value = self.env['ir.sequence'].get('dni.dni') or '/'
        if value:
            return value
        return False

    @api.multi
    def create_policy(self):
        res_cur_obj = self.env['res.currency']
        usd_rate = res_cur_obj.search([('name','=','USD')]).rate
        self.write({'usd_rate': usd_rate})
        account_move_obj = self.env['account.move']
        ref = self.name
        new_ids = []
        value = {}
        if datetime.now().month < 10:
            code_period = '0' + str(
                datetime.now().month
            ) + '/' + str(
                datetime.now().year
            )
        else:
            code_period = str(
                datetime.now().month
            ) + '/' + str(
                datetime.now().year
            )

        period_id = self.env['account.period'].search(
            [('name', '=', code_period)]
        )
        if period_id:
            lines = self.prepare_account_move_lines()
            if lines:
                move_vals = {
                    'journal_id': self.bank_id.journal_id.id,
                    'period_id': period_id.id,
                    'date': fields.Datetime.now(),
                    'ref': ref,
                    'line_id': [(0, 0, x) for x in lines],
                }
                new_id = account_move_obj.create(move_vals)
                # Asentar la poliza
                account_move_data = self.env['account.move'].browse(new_id.id)
                if account_move_data.journal_id.entry_posted:
                    new_id.post()
                new_ids.append(new_id.id)
                value = {
                        'domain': str([('id', 'in', new_ids)]),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'res_model': 'account.move',
                        'view_id': False,
                        'type': 'ir.actions.act_window',
                        'name': 'Póliza',
                        'res_id': new_ids,
                        }

        return value

    def prepare_account_move_lines(self):
        lines_vals = []
        company_user = self.env['res.users'].browse(self.write_uid.id)
        currency_company = company_user.company_id.currency_id.name
        debit = self.bank_id.journal_id.default_debit_account_id.id
        debit_account_id = debit
        credit = self.journal_id.default_credit_account_id.id
        credit_account_id = credit
        tax_account = self._get_tax().account_collected_voucher_id.id
        if currency_company != self.currency_id.name:
            self.env.cr.execute("""SELECT rcr.rate
                                FROM res_currency_rate rcr
                                INNER JOIN res_currency rc on rc.id = rcr.currency_id
                                WHERE rc.name = %s AND rcr.name <= %s
                                order by rcr.name desc LIMIT 1""",
                                (self.currency_id.name, self.create_date)
                                )
            if self.env.cr.rowcount:
                rate = self.env.cr.fetchone()
                if rate:
                    amount = round(round(1 / rate[0], 4) * self.amount, 2)
                    tax = round(round(1 / rate[0], 4) * self.tax, 2)
                else:
                    amount = 1 * self.amount
                    tax = 1 * self.tax
                lines_vals.append({
                    'name': '/',
                    'partner_id': self.partner_id.id,
                    'account_id': debit_account_id,
                    'debit': amount,
                    'credit': 0.00,
                    'amount_currency': self.amount
                    }
                )

                lines_vals.append({
                    'name': '',
                    'partner_id': self.partner_id.id,
                    'account_id': credit_account_id,
                    'debit': 0.00,
                    'credit': amount - tax,
                    'amount_currency': -(self.amount - self.tax)
                }
                )

                lines_vals.append({
                    'name': '',
                    'partner_id': self.partner_id.id,
                    'account_id': tax_account,
                    'debit': 0.00,
                    'credit': tax,
                    'amount_currency': - self.tax
                    }
                )
        if currency_company == self.currency_id.name:
            amount = self.amount
            tax = self.tax
            lines_vals.append({
                'name': '/',
                'partner_id': self.partner_id.id,
                'account_id': debit_account_id,
                'debit': amount,
                'credit': 0.00,
            }
            )

            lines_vals.append({
                'name': '',
                'partner_id': self.partner_id.id,
                'account_id': credit_account_id,
                'debit': 0.00,
                'credit': amount - tax,
            }
            )

            lines_vals.append({
                'name': '',
                'partner_id': self.partner_id.id,
                'account_id': tax_account,
                'debit': 0.00,
                'credit': tax,
            }
            )
        return lines_vals

    def onchange_partner_id(self, cr, uid, ids, context=None):
        values = {}
        user = uid
        company = self.pool.get('res.users').browse(
            cr, uid, user, context=context
        )
        if company:
            company_id = company.company_id.id
        else:
            company_id = []
        values.update({
            'value':
                {
                    'user_id': uid,
                },
            'domain':
                {
                    'user_id': [('id', '=', uid)],
                    'dni_movements_ids': [('dni_id', '=', ids)],
                    'currency_id': [('id', 'in', [])],
                    'policy_create_id': [('id', 'in', [])],
                    'journal_id': [('id', 'in', [])],
                    'bank_id': [('company_id', '=', company_id)]
                },
                       })
        return values

    def onchange_bank_id(self, cr, uid, ids, bank_id, context=None):
        values = {}
        user = uid
        if bank_id:
            bank = self.pool.get('res.partner.bank').browse(
                cr, uid, bank_id, context=context
            )

            account_apply_ids = self.pool.get('account.account.apply').search(
                cr, uid,
                [('code', '=', 'unidentified_deposit')],
                context=context
            )
            currency_bank = bank.currency2_id.id
            if currency_bank:
                user_company = self.pool.get('res.users').browse(
                    cr, uid, user, context=context
                )
                if user_company:
                    company = self.pool.get('res.company').browse(
                        cr, uid, user_company.company_id.id, context=context
                    )
                    currency_company = company.currency_id.id
                    journal_currency = currency_bank
                    if currency_company == currency_bank:
                        journal_currency = False
                    values.update({
                        'value':
                            {
                                'currency_id': currency_bank,
                                'journal_id': False
                            },
                        'domain':
                            {
                                'journal_id': [('type', '=', 'bank'),
                                               ('currency', '=', journal_currency),
                                               ('account_account_apply_id', 'in', account_apply_ids)]
                            }
                     })
                else:
                    if user_company:
                        company = self.pool.get('res.company').browse(
                            cr, uid, user_company.company_id.id, context=context
                        )
                        currency_company = company.currency_id.id
                        if currency_company:
                            values.update({
                                'value':
                                    {
                                        'currency_id': currency_company,
                                        'journal_id': False
                                    },
                                'domain':
                                    {
                                        'journal_id': [('type', '=', 'bank'),
                                                       ('type', '=', False),
                                                       ('account_account_apply_id', 'in', account_apply_ids)],

                                    }
                            })
            return values


class DepositoNoIdentificadoMovements(models.Model):
    _name = "dni.dni.movements"
    _description = "Movimientos de Depositos no identificados"

    dni_id = fields.Many2one('dni.dni', string="Número de DNI")
    user_id = fields.Many2one('res.users', string="Responsable")
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('generated', 'Generado'),
        ('associated', 'Asociado'),
        ('partial_applied', 'Aplicado parcial'),
        ('applied', 'Aplicado'),
        ('cancel', 'Cancelado')],
        'Estado'
    )

    _defaults = {

    }
    _order = 'create_date desc, id desc'


class DepositoNoIdentificadoAplicatedTo(models.Model):
    _name = "dni.dni.aplicated"
    _description = "Aplicacion de depositos no identificados"

    dni_id = fields.Many2one('dni.dni', string="Número de DNI")
    user_id = fields.Many2one('res.users', string="Responsable")
    account_voucher_id = fields.Many2one('account.voucher',
                                         string='Número de pago'
                                         )
    amount = fields.Float(string="Monto", digits=(10, 2))
    state = fields.Selection([
        ('aplicated', 'Aplicado'),
        ('cancel', 'Cancelado')],
        'Estado'
    )

    _defaults = {

    }
    _order = 'write_date desc, id desc'


class AccountVoucher(models.Model):
    _inherit = "account.voucher"
    _description = ""

    @api.model
    def action_move_line_create(self):
        res = super(AccountVoucher, self).action_move_line_create()
        if res:
            account_voucher_line_ids = self.env[
                'account.voucher.line'].search([
                    ('voucher_id', '=', self._ids[0]),
                    ('type', '=', 'dr'), ('amount', '>', 0)
                ])
            if account_voucher_line_ids:
                for data in account_voucher_line_ids:
                    if data:
                        move_id = data.move_line_id.move_id.id
                        dni_ids = self.env['dni.dni'].search(
                            [('policy_create_id', '=', move_id)]
                        )
                        if dni_ids:
                            if dni_ids.state != 'cancel':
                                currency = data.voucher_id.journal_id.currency
                                rate_alt = data.voucher_id.currency_rate_alter
                                value = {}
                                value.update({
                                    'ids': self._ids[0],
                                    'reconcile': data.reconcile,
                                    'currency': currency.name,
                                    'amount': data.amount,
                                    'date_pay': fields.Datetime.now(),
                                    'alter_currency': rate_alt,
                                })
                                dni_ids.applied(value)
        return res

    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id,
                        partner_id, date, amount, ttype, company_id,
                        context=None):
        res = super(AccountVoucher, self). \
                        onchange_journal(cr, uid, ids, journal_id, line_ids,
                        tax_id, partner_id, date, amount, ttype, company_id, context)
        dni_obj = self.pool.get('dni.dni')

        if partner_id:
            move_ids = []
            move_line_ids = []
            account_move_obj = self.pool.get('account.move')
            if res:
                for line in res['value']['line_dr_ids']:
                    if 'move_line_id' in line:
                        move_line_id = line.get('move_line_id')
                        move_line_ids.append(move_line_id)
            move_line_ids_obj = self.pool.get('account.move.line').browse(cr, uid, move_line_ids, context=context)
            for move_line in move_line_ids_obj:
                move_ids.append(move_line.move_id.id)
            dni_data = dni_obj.search(cr, uid, [('policy_create_id','in', move_ids)], context=context)
            # dni_data = dni_obj.search(cr, uid, [('partner_id','=',partner_id),('state','=','associated')], context=context)
            dni_obj_ids = dni_obj.browse(cr, uid, dni_data, context=context)
            if dni_obj_ids:
                journal = self.pool.get('account.journal').browse(cr, uid, res['value']['journal_id'], context=context)
                # Validate if the active journal currency is False (MXN)
                if not journal.currency.name:
                    if res['value']['line_dr_ids']:
                        x = 0
                        # Loop from dni object -> account_move -> account_move_lines
                        for record in dni_obj_ids:
                            if record.journal_id.account_account_apply_id.code == 'unidentified_deposit':
                                for move in record.policy_create_id:
                                    for line in move.line_id:
                                        if line.name == '/' and not line.currency_id.name:
                                            res['value']['line_dr_ids'][x].update(
                                            {
                                                'original_amount': line.debit, # 1st column
                                                'amount_original': line.debit  # 2nd column
                                            })
                                        # If the DNI comes in dollar currency then we place the original $ amount in the first column
                                        # and place the conversion in the 2nd column
                                        elif line.name == '/' and line.currency_id.name == 'USD':
                                            rate = record.usd_rate
                                            if rate > 0:
                                                conversion = round(round(1 / rate, 4) * line.amount_currency, 2)
                                            else:
                                                continue
                                            res['value']['line_dr_ids'][x].update(
                                            {
                                                'original_amount': line.amount_currency,
                                                'amount_original': conversion
                                            })
                                x = x + 1
                elif journal.currency.name == 'USD':
                    if res['value']['line_dr_ids']:
                        x = 0
                        for record in dni_obj_ids:
                            if record.journal_id.account_account_apply_id.code == 'unidentified_deposit':
                                for move in record.policy_create_id:
                                    for line in move.line_id:
                                        if line.name == '/' and line.currency_id.name:
                                            res['value']['line_dr_ids'][x].update(
                                            {
                                                'original_amount': line.amount_currency,
                                                'amount_original': line.amount_currency
                                            })
                                        elif line.name == '/' and not line.currency_id.name:
                                            rate = record.usd_rate
                                            if rate > 0:
                                                conversion = round(line.debit / round(1 / rate, 4), 2)
                                            else:
                                                continue
                                            res['value']['line_dr_ids'][x].update(
                                            {
                                                'original_amount': line.debit,
                                                'amount_original': conversion
                                            })
                            x = x + 1
        return res


    # def action_move_line_create(self, cr, uid, ids, context=None):
    #     res = super(AccountVoucher, self).action_move_line_create(
    #         cr, uid, ids, context=context
    #     )
    #     if res:
    #         account_voucher_line_ids = self.pool.get(
    #             'account.voucher.line').search(
    #             cr, uid, [
    #                 ('voucher_id', '=', ids),
    #                 ('type', '=', 'dr')
    #             ], context=context
    #         )
    #         if account_voucher_line_ids:
    #             data_ids = self.pool.get('account.voucher.line').browse(
    #                 cr, uid, account_voucher_line_ids, context=context
    #             )
    #             for data in data_ids:
    #                 if data:
    #                     move_id = data.move_line_id.move_id.id
    #                     dni_ids = self.pool.get('dni.dni').search(
    #                         cr, uid,
    #                         [('policy_create_id', '=', move_id)],
    #                         context=context
    #                     )
    #                     if dni_ids:
    #                         dni_dni = self.pool.get('dni.dni')
    #                         dni_data = dni_dni.browse(cr, uid, dni_ids[0],
    #                                                   context=context
    #                                                   )
    #                         if dni_data.state != 'cancel':
    #                             account_voucher = self.pool.get(
    #                                 'account.voucher').browse(
    #                                 cr, uid, data.voucher_id.id,
    #                                 context=context
    #                             )
    #                             currency = account_voucher.journal_id.currency
    #                             rate_alt = account_voucher.currency_rate_alter
    #                             value = {}
    #                             value.update({
    #                                 'ids': ids,
    #                                 'reconcile': data.reconcile,
    #                                 'currency': currency.name,
    #                                 'amount': data.amount,
    #                                 'date_pay': fields.Datetime.now(),
    #                                 'alter_currency': rate_alt,
    #                                 })
    #                             self.pool.get('dni.dni').applied(
    #                                 cr, uid, dni_ids[0], value,
    #                                 context=context
    #                             )
    #     return res

    @api.multi
    def cancel_voucher(self):
        res = super(AccountVoucher, self).cancel_voucher()
        if res:
            account_voucher_line_ids = self.env[
                'account.voucher.line'].search([
                    ('voucher_id', '=', self._ids),
                    ('type', '=', 'dr')
                ])
            if account_voucher_line_ids:
                dni_ids = []
                for data in account_voucher_line_ids:
                    if data:
                        move_id = data.move_line_id.move_id.id
                        dni_ids = self.env['dni.dni'].search([
                            ('policy_create_id', '=', move_id)
                        ])
                        if dni_ids:
                            dni_aplicated_id = self.env[
                                'dni.dni.aplicated'].search(
                                [
                                    ('account_voucher_id', '=', self._ids),
                                    ('dni_id', '=', dni_ids.id)
                                ]
                            )
                            dni_aplicated_id.write(
                                {
                                    'state': 'cancel',
                                    'user_id': self._uid,
                                    'amount': 0.0
                                }
                            )
                            dni_aplicated_ids = self.env[
                                'dni.dni.aplicated'].search([
                                    ('dni_id', '=', dni_ids.id)
                                ])
                            values = {}
                            for dni_aplicated_data in dni_aplicated_ids:
                                if dni_aplicated_data:
                                    if dni_aplicated_data.state == 'aplicated':
                                        values.update(
                                            {'state': 'partial_applied'}
                                        )
                                        state = 'partial_applied'
                                        break
                                    else:
                                        values.update({'state': 'associated'})
                                        state = 'associated'
                            values_movements = dni_ids.values_movements(values)
                            if values_movements:
                                new_dni_movements = self.env[
                                    'dni.dni.movements'].create(
                                    values_movements
                                )
                                if new_dni_movements:
                                    dni_ids.write(
                                        {'state': state}
                                    )
        return res

    # def cancel_voucher(self, cr, uid, ids, context=None):
    #     res = super(AccountVoucher, self).cancel_voucher(
    #         cr, uid, ids, context=context
    #     )
    #     if res:
    #         account_voucher_line_ids = self.pool.get(
    #             'account.voucher.line').search(
    #             cr, uid, [('voucher_id', '=', ids),
    #                       ('type', '=', 'dr')], context=context
    #         )
    #         if account_voucher_line_ids:
    #             data_ids = self.pool.get('account.voucher.line').browse(
    #                 cr, uid, account_voucher_line_ids, context=context
    #             )
    #             dni_ids = []
    #             for data in data_ids:
    #                 if data:
    #                     move_id = data.move_line_id.move_id.id
    #                     dni_ids = self.pool.get('dni.dni').search(
    #                         cr, uid, [('policy_create_id', '=', move_id)],
    #                         context=context
    #                     )
    #                     if dni_ids:
    #                         dni_aplicated_id = self.pool.get(
    #                             'dni.dni.aplicated').search(
    #                             cr, uid, [('account_voucher_id', '=', ids),
    #                                       ('dni_id', '=', dni_ids[0])],
    #                             context=context
    #                         )
    #                         self.pool.get('dni.dni.aplicated').write(
    #                             cr, uid, dni_aplicated_id, {
    #                                 'state': 'cancel',
    #                                 'user_id': uid,
    #                                 'amount': 0.0
    #                             }, context=context
    #                         )
    #
    #                         dni_aplicated_ids = self.pool.get(
    #                             'dni.dni.aplicated').search(
    #                             cr, uid, [('dni_id', '=', dni_ids[0])],
    #                             context=context
    #                         )
    #                         dni_aplicated_data_ids = self.pool.get(
    #                             'dni.dni.aplicated').browse(
    #                             cr, uid, dni_aplicated_ids, context=context
    #                         )
    #                         values = {}
    #                         for dni_aplicated_data in dni_aplicated_data_ids:
    #                             if dni_aplicated_data:
    #                                 if dni_aplicated_data.state == 'aplicated':
    #                                     values.update(
    #                                         {'state': 'partial_applied'}
    #                                     )
    #                                     state = 'partial_applied'
    #                                     break
    #                                 else:
    #                                     values.update({'state': 'associated'})
    #                                     state = 'associated'
    #                         values_movements = self.pool.get(
    #                             'dni.dni').values_movements(
    #                             cr, uid, dni_ids[0], values,
    #                             context=context
    #                         )
    #                         if values_movements:
    #                             new_dni_movements = self.pool.get(
    #                                 'dni.dni.movements').create(
    #                                 cr, uid, values_movements,
    #                                 context=context
    #                             )
    #                             if new_dni_movements:
    #                                 self.pool.get('dni.dni').write(
    #                                     cr, uid, dni_ids[0], {'state': state},
    #                                     context=context
    #                                 )
    #     return res
