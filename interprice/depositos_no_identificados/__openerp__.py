# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Depósitos no identificados',
    'version': '1.0',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'category': 'Contabilidad',
    'description': """
        Nuevo modulo para la identificación y asignación de depósitos a clientes..
         """,
    'website': 'http://www.grupovadeto.com',
    'license': 'AGPL-3',
    'depends': ['account', 'account_voucher', 'tc','configuracion'],
    'init_xml': [],
    'demo_xml': [],
    'update_xml': [
                'view/deposito_no_identificado_view.xml',
                'reporte/depositos_no_identificados/depositos_no_identificados_view.xml',
                 ],
    'installable': True,
    'active': False,
}
