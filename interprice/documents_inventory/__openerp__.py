# -*- coding: utf-8 -*-
{
    'name': "Documents of inventory",

    'description': """
        Manejo de documentos de inventario para realizar distintas operaciones de entradas y salidas adecuadas al proceso de inventario de PCH
    """,

    'author': "Easy Process and Solutions",
    'website': "http://www.easyprocess-solutions.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'stock',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','account','account_cost_center','purchase','fleet','delivery','l10n_mx_cities'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/document_view.xml',
        'views/stock_view.xml',
        'views/stock_picking_view.xml',
        'document_sequence.xml',
        'reports/document_inventory.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
}
