# -*- coding: utf-8 -*-
from datetime import datetime
from openerp import (
	_, api, exceptions, fields, models, tools
)


class document_inventory_type(models.Model):
	_name = 'document.inventory.type'
	_rec_name = 'name'

	prefix = fields.Char(string="Prefix")
	name = fields.Char(string="Name")
	type_operation = fields.Selection(
		[('incoming','Incoming'), ('internal','Internal'),
		('outgoing','Outgoing')],
		string='Type Operation')
	journal_id = fields.Many2one('account.journal',string="Account Journal")
	default_location_id = fields.Many2one('stock.location',string="Location by default")
	edit_cost = fields.Boolean(default=True)
	is_ref = fields.Boolean('Es referenciado por documento')
	#is_ref_purchase_or_invoice = fields.Boolean('es referenciado por Compra/Factura')

	inter_company = fields.Boolean('Inter Sucursal')
	state_document_to_confirm = fields.Selection(
		[('generated','Generated'),('supplied','Supplied'),('transit','Transit'),('process','Process')],
		string='State Document to confirm', default='generated', help="Indica el estatus que se debe cambiar al generar el documento")
	state_document_to_transfer = fields.Selection(
		[('generated','Generated'),('supplied','Supplied'),('transit','Transit'),('process','Process')],
		string='State of Document to transfer', default='supplied', help="Indica el estatus que se debe cambiar al transferir el albaran")

	type_document_form_transit_to_dest_id = fields.Many2one('document.inventory.type',string="Type document of incoming in transit", help="Indica el tipo de documento de entrada que se genera al transferir el primer albaran intersucursal a transito, en el almacen destino")
	apply_account_move = fields.Boolean(string="Create account move", default=True)
	reference_by_default_id = fields.Many2one('document.reference', string="Referencia por default")
	return_stock = fields.Boolean(string="Retorna Inventario", help="Define si se debe de retornar, la mercancia que salio por una salida por defectuoso a proveedor, la mercancia que entre en este documento se debera de regresar a la factura o compra relacionada en la salida por defectuosos a proveedor")
	allow_modify_locations = fields.Boolean(string="Permitir modificar ubicaciones",default=True)

	# selection field
	movement_classify = fields.Selection(
		help=_('Field to identify the movement type of document'),
		required=True,
		selection=[
			('input',_('Input')),
			('output',_('Output')),
			('internal_uses',_('Internal Uses')),
			('defective',_('Defective')),
			('intercompany',_('Inter Company')),
			('interwarehouse',_('Inter Warehouse'))
		],
		string=_('Movement classify')
	)



class document_inventory(models.Model):
	_name = 'document.inventory'
	_inherit = ['mail.thread', 'ir.needaction_mixin']
	_rec_name = 'number'

	origin = fields.Char(string='Traspaso Origen')

	@api.one
	def _get_total(self):
		total = 0
		for l in self.lines_ids:
			total = total + l.total
		self.total = total

	def _get_current_date(self):
		print fields.Date.context_today(self)
		return fields.Date.context_today(self)

	def _get_user(self):
		return self.env.user

	def _get_partner(self):
		return self.env.user.partner_id

	prefix_to_validate = fields.Char(string="Prefix to validate")
	state_to_validate = state_to_validate = fields.Selection(
		[('generated','Generated'),('supplied','Supplied'),('transit','Transit'),('process','Process')],
		string='State to Validate')
	partner_id = fields.Many2one('res.partner',string="Cliente", default='_get_partner')
	user_id = fields.Many2one('res.users',string="User", default=_get_user)
	edit_cost = fields.Boolean()
	type_document = fields.Many2one('document.inventory.type',string="Type document")
	is_ref = fields.Boolean()
	#is_ref_purchase_or_invoice = fields.Boolean()
	reference_by = fields.Selection([('none','Sin Referencia'),('document','Documento'),('purchase','Orden de Compra'),('invoice','Factura')],string="Referenciado Por")
	inter_company = fields.Boolean()
	type_operation = fields.Selection(
		[('incoming','Incoming'), ('internal','Internal'),
		('outgoing','Outgoing')],
		string='Type Operation')
	reference_id = fields.Many2one('document.reference', string="Reference")
	reference_default_id = fields.Many2one('document.reference')
	location_id = fields.Many2one('stock.location',string="Warehouse")
	stock_picking_type_id = fields.Many2one('stock.picking.type',string="Warehouse")
	number = fields.Char(copy=False)
	number_ref = fields.Char(string="Number Ref")
	document_ref_id = fields.Many2one('document.inventory')
	purchase_order_ref_id = fields.Many2one('purchase.order',string="Orden de Compra",domain="[('state','in',('confirmed','approved','done'))]")
	account_invoice_ref_id = fields.Many2one('account.invoice',string="Factura de Compra",domain="[('type','=','in_invoice'),('state','in',('open','paid')),('origin','!=',False)]")

	date = fields.Date(string="Date",default=_get_current_date)
	state = fields.Selection(
		[('draft','Draft'),('generated','Generated'),('transit','Transit'),('process','Process'),('supplied','Supplied'),('canceled','Canceled')],
		string='State',default='draft')
	note = fields.Text(string="Notes")
	lines_ids = fields.One2many(comodel_name='document.inventory.line', inverse_name='document_id',copy=True)
	total = fields.Float(string="Total",compute=_get_total)
	location_dest_id = fields.Many2one('stock.location',string="Warehouse Dest")
	stock_picking_dest_type_id = fields.Many2one('stock.picking.type',string="Warehouse Dest")
	supplier_id = fields.Many2one('res.partner',string="Supplier")
	warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse origin")
	warehouse_dest_id = fields.Many2one('stock.warehouse', string="Warehouse Dest")
	picking_id = fields.Many2one('stock.picking')

	account_move_id = fields.Many2one('account.move')
	account_move_cancel_id = fields.Many2one('account.move')
	document_in_transit_id = fields.Many2one('document.inventory',string="Document in transit")
	account_cost_center_id = fields.Many2one('account.cost.center',string="Sucursal Origen")
	account_cost_center_dest_id = fields.Many2one('account.cost.center',string="Sucursal Destino")

	allow_modify_locations = fields.Boolean(string="Permitir modificar ubicaciones",default=True)
	movement_classify = fields.Selection(
		help=_('Field to identify the movement type of document'),
		required=True,
		selection=[
			('input',_('Input')),
			('output',_('Output')),
			('internal_uses',_('Internal Uses')),
			('defective',_('Defective')),
			('intercompany',_('Inter Company')),
			('interwarehouse',_('Inter Warehouse'))
		],
		string=_('Movement classify')
	)
	#doc_date = fields.Date(string="Date",default=fields.Date.context_today(self))
	#is_transit = fields.Boolean(string="Is Transit")

	# @api.onchange('delivery_form')
	# def on_change_delivery_form(self):
	# 	if self.delivery_form == 'at_home':
	# 		if self.warehouse_dest_id:
	# 			'partner_id'
	# 		self.update({'street':})

	def domain_to_validate_initial_filters(self):
		"""[Returning function the domain especific at moment to select the
		 specific type of document, if this is requeried.]

		[This function receives argument self, return a domain, this only for
		some fields, accord the type of document and only if is required an
		specific domain]

		Returns:
			[dictionary] -- [return the domain accord at type of document
			selected.]
		"""
		res = {}
		if self.type_document.movement_classify == 'defective' and \
			self.is_ref:
			res['domain'] = {
				'purchase_order_ref_id':[('shipped', '=', True)]
			}
		elif self.type_document.movement_classify == 'interwarehouse':
			res['domain'] = {
				'warehouse_id':[],
				'warehouse_dest_id':[]
			}
		return res

	def domain_to_validate_internal_defective(self, warehouse_id, supplier_id,
											location_id, location_dest_id):
		"""[This function evaluate the type of document and prepare a domain
		to validate the fields accord to this clasification.]

		[This function receives the warehouse_id, supplier_id, location_id
		and location_dest_id to can create the correct validation on the
		fields.]

		Arguments:
			warehouse_id {[many2one]} -- [field related to stock.warehouse
										model, object document.inventory]
			supplier_id {[many2one]} -- [field related to res.partner model,
										object document.inventory]
			location_id {[many2one]} -- [field related to stock.location
										model, object document.inventory]
			location_dest_id {[many2one]} -- [field related to stock.location
											model, object document.inventory]

		Returns:
			[dictionary] -- [return a domain accord to field type]

		Raises:
			exceptions -- [the function return an exception when on
							location_dest_id field don't get warehouse
							defective type]
		"""
		res = {}
		dest_loc_ids = []
		if self.type_document:
			if self.type_document.movement_classify == 'internal_uses':
				warehouse_id = warehouse_id if self.is_ref else \
					warehouse_id.id
				location_id = location_id if self.is_ref else \
					location_id.id
				location_dest_id = location_dest_id if self.is_ref else \
					location_dest_id.id

				domain_warehouse = [('id', '=', warehouse_id)]
				domain_loc_id = [('id', '=', location_id)]
				domain_loc_dest_id = [('id', '=', location_dest_id)]

			elif self.type_document.movement_classify == 'defective':
				if self.reference_by == 'purchase':
					"""these lines are commented and refactored since
					the sentence brought a list of ids that were of
					the defective type, but in doing so and no such
					store has been configured and this is what
					threw the error.
					"""
					# self._cr.execute(
					# 	"""
					# 	SELECT sl.id
					# 	FROM stock_location sl
					# 	JOIN stock_warehouse sw
					# 	ON sw.id = sl.warehouse_id
					# 	JOIN stock_warehouse_type swt
					# 	ON swt.id = sw.stock_warehouse_type_id
					# 	WHERE swt.code = 'def'
					# 	"""
					# )
					# if self._cr.rowcount:
					# 	lista = self._cr.dictfetchall()
					# dest_loc_ids = [id['id'] for id in lista]
					domain_warehouse = [('id', '=', warehouse_id)]
					domain_supplier = [('id', '=', supplier_id)]
					domain_loc_dest_id = [('id', '=', location_dest_id)]
					domain_loc_id = [('id', '=', location_id)]
					# else:
					# 	raise exceptions.Warning(
					# 			_("Defective warehouse type not exist")
					# 		)
				elif self.reference_by == 'document':
					domain_warehouse = [('id', '=', warehouse_id)]
					domain_loc_id = [('id', '=', location_id)]
					domain_loc_dest_id = [('id', '=', location_dest_id)]

			res['domain'] = {
				'warehouse_id': domain_warehouse,
				'location_dest_id': domain_loc_dest_id,
				'location_id': domain_loc_id
			}

			if self.type_document.movement_classify == 'defective':
				if self.reference_by == 'purchase':
					res['domain'].update({'supplier_id': domain_supplier})
		return res

	def domain_to_validate(self, location_id, location_dest_id):
		"""[Returning function the espedific domain on fields location_id
			and location_dest_id]

		[This function receives the arguments location_id and
		location_des_id to can make the corresponding validation]

		Arguments:
			location_id {[many2one]} -- [field related to
				stock.location model]
			location_dest_id {[many2one]} -- [field related to
				stock.location model]

		Returns:
			[dict] -- [res is a dictionary, this contain the right validation
				to the fields location_id and location_des_id, to the especific
				types input and output movement]
		"""
		res = {}
		dest_loc_ids = []
		if self.type_document:
			if self.type_document.movement_classify in ('input','output'):
				domain_loc_id = [('id', '=', location_id.id)]
				domain_loc_dest_id = [('id', '=', location_dest_id.id)]
				res['domain'] = {
					'location_id': domain_loc_id,
					'location_dest_id': domain_loc_dest_id
				}
		return res

	def domain_to_validate_transfer(self):
		"""[This function make the validation on fields, when the movement
			is interwarehouse or intercompany ]

		[The function receives the argument self, and return a dict with the
		domain, on fields for this movement type]

		Returns:
			[dict] -- [dict with the domain to validate the fields active
			when this document type is selected]

		Raises:
			exceptions -- [return an exception when not find a warehouse
			related at account cost center]
		"""

		res = {}
		if self.type_document.movement_classify == 'intercompany':
			if self.type_document.state_document_to_transfer == 'transit':
				domain_acc_cost_center = [
						('id', '!=', self.account_cost_center_dest_id.id)
					]
				domain_acc_cost_center_dest = [
						('id', '!=', self.account_cost_center_id.id)
					]
				res['domain'] = {
					'account_cost_center_id': domain_acc_cost_center,
					'account_cost_center_dest_id': domain_acc_cost_center_dest
				}
			else:
				res = self.domain_to_validate(self.location_id,
					self.location_dest_id)

		elif self.type_document.movement_classify == 'interwarehouse':
			account_cost_center_id = self.warehouse_id.centro_costo_id.id
			res = {}
			if self.warehouse_id:
				self._cr.execute(
						"""
						SELECT sw.id from stock_warehouse sw
						JOIN account_cost_center acc
						ON acc.id = sw.centro_costo_id
						WHERE sw.centro_costo_id = %s
						""",
						(account_cost_center_id,)
					)
				if self._cr.rowcount:
					lista = self._cr.dictfetchall()
					warehouse_ids = [id['id'] for id in lista]
					if self.warehouse_id.id in warehouse_ids:
						warehouse_ids.remove(self.warehouse_id.id)
					domain_warehouse_id = [('id', 'in', warehouse_ids)]
					domain_loc_id = [('id', '=', self.location_id.id)]
					domain_loc_dest_id = [('id', '=', self.location_dest_id.id)]

					res['domain'] = {
						'warehouse_dest_id': domain_warehouse_id,
						'location_id': domain_loc_id,
						'location_dest_id': domain_loc_dest_id
					}
				else:
					raise exceptions.Warning(
							_("Don't exist warehouse related to the cost center")
						)
		return res


	@api.onchange('purchase_order_ref_id')
	def on_change_purchase_order_ref_id(self):
		domain = {}
		res = self._fill_form()
		if 'domain' in res:
			domain['domain'] = res.pop('domain')

		self.update(res)
		return domain

	@api.onchange('account_invoice_ref_id')
	def on_change_account_invoice_ref_id(self):
		return self.update(self._fill_form())

	@api.onchange('document_ref_id')
	def on_change_document_ref_id(self):
		domain = {}
		if self.reference_id.type_document_to_validate_id:
			type_doc_name = self.reference_id.type_document_to_validate_id.name
			state_doc_name = self.reference_id.state_to_validate
			#print state_doc_name
			if self.reference_id.type_document_to_validate_id.id != self.document_ref_id.type_document.id:
				raise exceptions.ValidationError("Document is not type " + type_doc_name)
			if self.document_ref_id.state != state_doc_name:
				raise exceptions.ValidationError("Document is not state " + state_doc_name)

		res = self._fill_form()
		if 'domain' in res:
			domain['domain'] = res.pop('domain')

		self.update(res)

		return domain

	def _fill_form(self):
		lines = False
		lines_form = []
		location_id = False
		location_dest_id = False
		supplier_id = False
		warehouse_id = False
		domain = {}

		if self.document_ref_id and self.reference_by == 'document':
			lines = self.document_ref_id.lines_ids
			lines_form = self._fill_lines(lines,'document')
			warehouse_id = self.document_ref_id.warehouse_id.id
			location_id = self.document_ref_id.location_dest_id.id
			location_dest_id = self.document_ref_id.location_id.id
			domain = self.domain_to_validate_internal_defective(warehouse_id,
				supplier_id, location_id, location_dest_id)
		elif self.purchase_order_ref_id  and self.reference_by == 'purchase':
			lines = self.purchase_order_ref_id.order_line
			lines_form = self._fill_lines(lines,'purchase')
			warehouse_id = self.purchase_order_ref_id.picking_type_id.warehouse_id.id
			location_id = self.purchase_order_ref_id.picking_type_id.default_location_dest_id.id
			location_dest_id = self.purchase_order_ref_id.picking_type_id.default_location_src_id.id
			supplier_id = self.purchase_order_ref_id.partner_id.id
			domain = self.domain_to_validate_internal_defective(warehouse_id,
				supplier_id, location_id, location_dest_id)
		elif self.account_invoice_ref_id and self.reference_by == 'invoice':
			lines = self.account_invoice_ref_id.invoice_line
			lines_form = self._fill_lines(lines,'invoice')
			purchase_obj = self.env['purchase.order'].search([('name','=',self.account_invoice_ref_id.origin)])
			for po in purchase_obj:
				warehouse_id = po.picking_type_id.warehouse_id.id
				location_id = po.picking_type_id.default_location_dest_id.id
				location_dest_id = po.picking_type_id.default_location_src_id.id
				supplier_id = po.partner_id.id

		self.lines_ids = False
		form = {'location_id':location_id,'location_dest_id':location_dest_id,'supplier_id':supplier_id,
				'lines_ids':[(0, 0, x) for x in lines_form],
				'warehouse_id':warehouse_id,'warehouse_dest_id':self.document_ref_id.warehouse_dest_id,
				'note':self.document_ref_id.note
			}
		form.update(domain)
		return form

	def _fill_lines(self,lines,type_lines):
		lines_list = []
		line_dict = {}
		quantity_remaining = 0

		if type_lines == 'document':
			for l in lines:
				line_dict = {'product_id':l.product_id.id,'description':l.product_id.name,
				'quantity':l.quantity_remaining,'cost':l.product_id.standard_price,'is_ref':True,'edit_cost':self.edit_cost}
				lines_list.append(line_dict)
		elif type_lines == 'invoice':
			for l in lines:
				line_dict = {'product_id':l.product_id.id,'description':l.product_id.name,
				'quantity':l.quantity_remaining,'cost':l.product_id.standard_price,'is_ref':True,'edit_cost':self.edit_cost}
				lines_list.append(line_dict)
		elif type_lines == 'purchase':
			for l in lines:
				line_dict = {'product_id':l.product_id.id,'description':l.product_id.name,
				'quantity':l.quantity_remaining,'cost':l.product_id.standard_price,'is_ref':True,'edit_cost':self.edit_cost}
				lines_list.append(line_dict)

		return lines_list

	@api.onchange('reference_id')
	def on_change_reference_id(self):
		prefix_to_validate = ''
		state_to_validate = ''
		if self.reference_id.type_document_to_validate_id:
			prefix_to_validate = self.reference_id.type_document_to_validate_id.prefix
			state_to_validate = self.reference_id.state_to_validate

		return self.update({'reference_by':self.reference_id.reference_by,'prefix_to_validate':prefix_to_validate,'state_to_validate':state_to_validate})

	@api.onchange('type_document')
	def on_change_type_document(self):
		reference_default_id = self.type_document.reference_by_default_id.id
		onchange_defaults = {'warehouse_id':False,'warehouse_dest_id':False,
			'type_operation':self.type_document.type_operation,
			'reference_id':self.type_document.reference_by_default_id.id,
			'is_ref':self.type_document.is_ref,'inter_company':self.type_document.inter_company,
			'edit_cost':self.type_document.edit_cost,'allow_modify_locations':self.type_document.allow_modify_locations,
			'location_id':False,'location_dest_id':False,'document_ref_id':False,'reference_default_id':reference_default_id,
			'purchase_order_ref_id':False,'account_invoice_ref_id':False, 'supplier_id':False,
			'movement_classify': self.type_document.movement_classify}
		self.update(onchange_defaults)

		res = self.domain_to_validate_initial_filters()

		return res

	@api.onchange('warehouse_id')
	def on_change_warehouse_id(self):
		domain = {}
		if not self.document_ref_id:
			if self.type_document.type_operation == 'incoming':
				self.update(
					{
						'location_dest_id':self.warehouse_id.lot_stock_id,
						'location_id':self.type_document.default_location_id
					})
			else:
				self.update(
					{
						'location_id':self.warehouse_id.lot_stock_id,
						'location_dest_id':self.type_document.default_location_id
					})
		if self.type_document.movement_classify in ('intercompany','interwarehouse'):
			self.update({
					'warehouse_dest_id': False,
					'location_dest_id': False
				})
			domain = self.domain_to_validate_transfer()
		elif self.type_document.movement_classify not in ('intercompany',
			'interwarehouse'):

			domain = self.domain_to_validate(self.location_id,
				self.location_dest_id)

		return domain


	@api.onchange('warehouse_dest_id')
	def on_change_warehouse_dest_id(self):
		return self.update({'location_dest_id':self.warehouse_dest_id.lot_stock_id})

	@api.model
	def create(self, vals):
		ctx = dict(self.env.context or {}, mail_create_nolog=True)
		new_id = super(document_inventory, self).create(vals, context=ctx)
		post_vars = {'subject': "Información", 'body': "Documento creado"}
		new_id.message_post(type="notification", subtype="mt_comment", **post_vars)
		return new_id

	@api.multi
	def generate(self):
		if not self.lines_ids:
			raise exceptions.ValidationError(
				_("You can not generate an empty movement"))

		number = ''
		state = self.type_document.state_document_to_confirm

		result = {'state':state,'prefix_to_validate':self.type_document.prefix}

		if self.type_document.apply_account_move:
			if not self.type_document.journal_id.entry_posted:
				number = self.env['ir.sequence'].next_by_id(self.type_document.journal_id.sequence_id.id)

			account_move_id = self.create_account_move(number,False)
			if number == '':
				number = account_move_id.name

			result.update({'account_move_id':account_move_id.id,'number':number})
		else:
			number = self.env['ir.sequence'].next_by_id(self.type_document.journal_id.sequence_id.id)
			#number = self.env['ir.sequence'].get('document.inventory') or '/'
			result.update({'number':number})

		picking_id = self.create_stock_picking(number)
		if picking_id:
			stock_picking_obj = self.env['stock.picking'].browse(picking_id)
			stock_picking_obj.sudo().action_assign()
			result.update({'picking_id':picking_id})

		post_vars = {'subject': "Información", 'body': "Documento generado"}
		self.message_post(type="notification", subtype="mt_comment", **post_vars)
		self.update(result)
		return True

	@api.multi
	def cancel(self):
		if self.state == 'draft':
			raise exceptions.ValidationError(
				_("It is not possible to cancel a document if it has not generated a picking"))

		result = {'state':'canceled'}
		picking_name = self.picking_id.name
		if self.picking_id.state != 'cancel':
			raise exceptions.ValidationError(
				_("It is not possible to cancel the document because the picking has not been canceled"))

		if self.purchase_order_ref_id and self.reference_by == 'purchase':
			for line in self.lines_ids:
				for line_po in self.purchase_order_ref_id.order_line:
					if line.product_id.id == line_po.product_id.id:
						if self.picking_id.state == 'cancel':
							line_po.update(
								{
									'accumulated_remaining':line_po.accumulated_remaining - line.quantity
								}
							)

		if self.account_move_id.name:
			ref = self.account_move_id.name + ' Cancelado'
			account_move_id = self.create_account_move(ref,True)
			result.update({'account_move_cancel_id':account_move_id.id})
			post_vars = {'subject': "Información", 'body': "Documento cancelado"}
		else:
			post_vars = {'subject': "Información", 'body': "El Documento %s ha sido cancelado" % self.number}

		self.message_post(type="notification", subtype="mt_comment", **post_vars)
		return self.update(result)

	@api.multi
	def change_state(self, state):
		return self.update({'state':state})

	def create_stock_picking(self,doc):
		stock_picking_obj = self.env['stock.picking']
		lines = self._get_stock_moves()
		# if not self.type_document.type_operation in ('internal','outgoing','incoming'):
		# 	operation_type = 'incoming'
		operation_type = self.type_document.type_operation
		#print operation_type, self.warehouse_id.name

		values = {
			#'partner_id':self.partner_id.id or None,
			'date': fields.Datetime.now(),
			'min_date': fields.Datetime.now(),
			'origin': doc,
			'move_type':'direct',
			'invoice_state': 'none',
			'picking_type_id': self.env['stock.picking.type'].search([('code','=',operation_type),('warehouse_id','=',self.warehouse_id.id)])[0].id,
			'move_lines': [(0, 0, x) for x in lines],
			'document_inventory_id':self.id,
		}

		new_id = stock_picking_obj.create(values)
		new_id.action_confirm()
		if operation_type == 'incoming':
			new_id.action_assign()

		return new_id.id

	def _get_stock_moves(self):
		lines_vals = []
		location_id = False
		location_dest_id = False
		cost = 0

		location_id = self.location_id.id
		location_dest_id = self.location_dest_id.id

		if self.type_document.state_document_to_transfer == 'transit':
			location_dest_id = self.type_document.default_location_id.id

		operation_type = self.type_document.type_operation

		for l in self.lines_ids:
			if operation_type == 'incoming':
				cost = l.cost

			line = {
				'product_id':l.product_id.id,
				'product_uom_qty':l.quantity,
				'product_uom':l.product_id.uom_id.id,
				'name':l.product_id.display_name,
				'date':fields.Date.context_today(self),
				'date_expected':fields.Date.context_today(self),
				'invoice_state':'none',
				'procure_method':'make_to_stock',
				'location_id': location_id,
				'location_dest_id':location_dest_id,
				'price_unit':cost
			}

			lines_vals.append(line)
		return lines_vals

	def create_account_move(self, ref, cancel):
		account_move_obj = self.env['account.move']

		code_period = str("%02d" % (datetime.now().month,)) + '/' + str(datetime.now().year)
		#print code_period
		period_id = self.env['account.period'].search([('code','=',code_period)])[0]

		lines = self._get_account_move_lines(cancel)

		move_vals = {
			'journal_id':self.type_document.journal_id.id,
			'period_id':period_id.id,
			'date':fields.Date.context_today(self),
			'ref':ref,
			'line_id': [(0, 0, x) for x in lines],
		}

		new_id = account_move_obj.create(move_vals)
		#Asentar la poliza
		#new_id.post()
		if not cancel:
			new_id.write({'ref':ref})

		return new_id

	def _get_account_move_lines(self, cancel):
		lines_vals = []
		move_line = {}

		debit_account_id = self.type_document.journal_id.default_debit_account_id
		credit_account_id = self.type_document.journal_id.default_credit_account_id

		account_cost_center_id = 0

		if self.warehouse_id.centro_costo_id:
			account_cost_center_id = self.warehouse_id.centro_costo_id.id

		if not debit_account_id or not credit_account_id:
			raise exceptions.ValidationError('Please configure the account of debit and credit in te journal ' +  self.type_document.journal_id.name)

		if not cancel:
			move_line = {'name':debit_account_id.name,'account_id':debit_account_id.id,'debit':self.total,'credit':0,'cost_center_id':account_cost_center_id}
			lines_vals.append(move_line)

			move_line = {'name':credit_account_id.name,'account_id':credit_account_id.id,'debit':0,'credit':self.total, 'cost_center_id':account_cost_center_id}
			lines_vals.append(move_line)
		else:
			move_line = {'name':debit_account_id.name,'account_id':debit_account_id.id,'debit':0,'credit':self.total, 'cost_center_id':account_cost_center_id}
			lines_vals.append(move_line)

			move_line = {'name':credit_account_id.name,'account_id':credit_account_id.id,'debit':self.total,'credit':0, 'cost_center_id':account_cost_center_id}
			lines_vals.append(move_line)

		return lines_vals

	@api.onchange('account_cost_center_id')
	def on_change_account_cost_center_id(self):
		wh_ids = self.env['stock.warehouse'].search([('centro_costo_id','=',self.account_cost_center_id.id)])
		wh_id = 0
		for w in wh_ids:
			if w.stock_warehouse_type_id.code == 'fac':
				wh_id = w.id

		self.update({'warehouse_id':wh_id})

		domain = self.domain_to_validate_initial_filters()

		return domain

	@api.onchange('account_cost_center_dest_id')
	def on_change_account_cost_center_dest_id(self):
		wh_dest_ids = self.env['stock.warehouse'].search([('centro_costo_id','=',self.account_cost_center_dest_id.id)])
		wh_dest_id = 0
		for w in wh_dest_ids:
			if w.stock_warehouse_type_id.code == 'fac':
				wh_dest_id = w.id

		self.update({'warehouse_dest_id':wh_dest_id})
		domain = self.domain_to_validate_initial_filters()

		return domain

	@api.one
	def unlink(self):
		if not self.state == 'draft':
			raise exceptions.Warning(
				_("You can't delete moves that are not in draft status."))

		return super(document_inventory, self).unlink()

class document_inventory_line(models.Model):
	_name = 'document.inventory.line'

	@api.one
	def _get_total(self):
		total = 0
		total = self.quantity * self.cost
		self.total = total

	@api.one
	def _get_quantity_remaining(self):
		total = 0
		self.quantity_remaining = self.quantity - self.accumulated_remaining

	document_id = fields.Many2one('document.inventory',string="Document",ondelete='cascade')
	product_id = fields.Many2one('product.product',string="Product")
	lot_id = fields.Many2one('stock.production.lot','Lot/Serial')
	description = fields.Char(string="Description")
	quantity = fields.Float(string="Quantity", default=1)
	quantity_remaining = fields.Float(string="Quantity remaining", compute=_get_quantity_remaining)
	accumulated_remaining = fields.Float(string="accumulated remaining", default=0)
	cost = fields.Float(string="Cost")
	total = fields.Float(string="Total",compute=_get_total)
	is_ref = fields.Boolean(default=False)
	edit_cost = fields.Boolean()
	#completed = fields.Boolean(default=False)

	@api.onchange('product_id','quantity')
	def on_change(self):
		desc = ""
		cost = 0
		edit_cost = False

		if self.product_id:
			if self.quantity <= 0:
				raise exceptions.ValidationError(
					_('Please enter quantities greater than 0'))
			if self.is_ref:
				if self.quantity > self.quantity_remaining:
					raise exceptions.ValidationError(
						_('The amount that income exceeds the remaining amount of the document'))

			if self.document_id.type_document.type_operation != 'incoming':
				var = self.validate_available()
				if var != '':
					stock_warning = var
					qty = stock_warning.get('qty_update')
					if qty:
						self.quantity = qty
					else:
						self.product_id = False
					return {'warning': stock_warning}

			if self.cost == 0:
				cost = self.product_id.standard_price
			else:
				cost = self.cost

			if self.document_id.type_document:
				edit_cost = self.document_id.type_document.edit_cost

		self.description = desc
		self.cost = cost
		self.edit_cost = edit_cost

	def validate_available(self):
		result = ''
		msg = ''
		quantity = self.quantity
		product = self.env['product.product'].browse(self.product_id.id)
		if product.type == 'service':
			result = _("The product %s ") % self.product_id.name +\
				_("is a service")
			return result

		if self.document_id.location_id.usage == 'transit':
			result = _("The location is transit type")
			return result

		new_qty = 0
		product_qty_by_loc = []
		product_qty_by_loc = product.get_product_available_by_location(
			self.document_id.location_id.id)[product.id]['virtual_available']
		for location, existencias in product_qty_by_loc:
			qty_available = product.get_product_available_by_location(
				self.document_id.location_id.id)[product.id]['qty_available']
			real_qty = qty_available[0][1]
			if quantity:
				self._cr.execute("""
					SELECT SUM(sq.qty) FROM stock_quant sq
					JOIN product_product pp
					ON pp.id = sq.product_id
					JOIN stock_move sm
					ON sq.reservation_id = sm.id
					JOIN stock_location sl
					ON sq.location_id = sl.id
					WHERE sq.product_id = %s
					AND sl.id = %s""", (product.id, location.id))
				if self._cr.rowcount:
					apartado = self._cr.fetchone()[0]
					if apartado is None or apartado is False:
						apartado = 0.0
						if quantity > real_qty:
							msg = _("\nPlan to transfer %.2f %s(s)") % \
								(self.quantity, self.product_id.uom_id.name) +\
								_(" but it only has %.2f %s(s)") % \
								(real_qty - apartado, self.product_id.uom_id.name)\
								+ _(" available in this location!") +\
								_("\nThe actual stock (without reserves) is %.2f %s (s)") % \
								(real_qty, self.product_id.uom_id.name)\
								+ _("\nReserves %.2f %s (s)") % \
								(apartado, product.uom_id.name)
					else:
						real_qty = real_qty - apartado
						if real_qty <= 0:
							real_qty=0
						if quantity > real_qty:
							msg = _("\nPlan to transfer %.2f %s(s)") % \
								(self.quantity, self.product_id.uom_id.name) +\
								_(" but it only has %.2f %s(s)") % \
								(real_qty, self.product_id.uom_id.name)\
								+ _(" available in this location!") +\
								_("\nThe actual stock (without reserves) is %.2f %s (s)") % \
								(real_qty, self.product_id.uom_id.name)\
								+ _("\nReserves %.2f %s (s)") % \
								(apartado, product.uom_id.name)
				else:
					msg = _("\nPlan to transfer %.2f %s(s)") % \
						(self.quantity, self.product_id.uom_id.name) +\
						_(" but it only has %.2f %s(s)") % \
						(real_qty, self.product_id.uom_id.name)\
						+ _(" available in this location!") +\
						_("\nThe actual stock (without reserves) is %.2f %s (s)") % \
						(real_qty, product.uom_id.name)
		if msg:
			result += _("Product [%s] has not enough stock!: "%self.product_id.name) + msg + "\n"

		if result:
			if apartado:
				new_qty = real_qty - apartado
				if new_qty <= 0:
					new_qty=0
			else:
				new_qty = 0
			warning = {
				'title': _('Warning!'),
				'message': result,
				'qty_update': new_qty
			}
			result = warning
		return result

	@api.one
	@api.constrains('quantity')
	def _check_quantity(self):
		self.on_change()


class document_reference(models.Model):
	_name = 'document.reference'
	_rec_name = 'name'

	name = fields.Char(string="Name")
	reference_by = fields.Selection([('none','Sin Referencia'),('document','Documento'),('purchase','Orden de Compra'),('invoice','Factura de Compra')],string="Referenciado Por",default="none")
	type_document_to_validate_id = fields.Many2one('document.inventory.type',string="Type Document to Validate")
	state_to_validate = fields.Selection(
		[('generated','Generated'),('supplied','Supplied'),('transit','Transit'),('process','Process')],
		string='State to Validate')