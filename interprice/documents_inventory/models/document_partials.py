# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp import exceptions

class document_inventory(models.Model):
	_inherit = 'document.inventory'

	partials_completed = fields.Boolean(copy=False)

	@api.multi
	def generate(self):
		res = super(document_inventory,self).generate()
		#l.write({'accumulated_remaining':l.accumulated_remaining + l.quantity})

		if self.type_document.apply_account_move:
			#add_costcenter_line function add cost center id in the each account.move.lines equals cost center of account.invoice
			move_id = self.account_move_id
			cost_center_id = self.warehouse_id.centro_costo_id.id
			result = self.env['account.move.line'].add_costcenter_line(move_ids=move_id, cost_center_id=cost_center_id)

		if self.reference_by == 'document':
			if self.document_ref_id:
				for l in self.lines_ids:
					for l2 in self.document_ref_id.lines_ids:
						if l.product_id.id == l2.product_id.id:
							l2.write({'accumulated_remaining':l2.accumulated_remaining + l.quantity})
					if self.type_document.return_stock:
						if self.document_ref_id.purchase_order_ref_id:
							for lc in self.document_ref_id.purchase_order_ref_id.order_line:
								if lc.product_id == l.product_id:
									lc.write({'accumulated_remaining':lc.accumulated_remaining - l.quantity})
						elif self.document_ref_id.account_invoice_ref_id:
							for li in self.document_ref_id.account_invoice_ref_id.invoice_line:
								if li.product_id == l.product_id:
									li.write({'accumulated_remaining':li.accumulated_remaining - l.quantity})
						else:
							raise exceptions.ValidationError('Para ingresar la mercancia al almacen es necesario que la referencia relacionada a este documento, contenga como referencia una compra o factura')
		elif self.reference_by == 'purchase':
			if self.purchase_order_ref_id:
				for l in self.lines_ids:
					for l2 in self.purchase_order_ref_id.order_line:
						if l.product_id.id == l2.product_id.id:
							l2.write({'accumulated_remaining':l2.accumulated_remaining + l.quantity})
		elif self.reference_by == 'invoice':
			if self.account_invoice_ref_id:
				for l in self.lines_ids:
					for l2 in self.account_invoice_ref_id.invoice_line:
						if l.product_id.id == l2.product_id.id:
							l2.write({'accumulated_remaining':l2.accumulated_remaining + l.quantity})
		sum1 = 0
		sum2 = 0

		if self.reference_by == 'document':
			if self.document_ref_id:
				for l in self.document_ref_id.lines_ids:
					sum1 = sum1 + l.quantity
					sum2 = sum2 + l.accumulated_remaining
				if sum1 == sum2:
					self.document_ref_id.write({'partials_completed':True})
		elif self.reference_by == 'purchase':
			if self.purchase_order_ref_id:
				for l in self.purchase_order_ref_id.order_line:
					sum1 = sum1 + l.product_qty
					sum2 = sum2 + l.accumulated_remaining
				if sum1 == sum2:
					self.purchase_order_ref_id.write({'partials_completed':True})
		elif self.reference_by == 'invoice':
			if self.account_invoice_ref_id:
				for l in self.account_invoice_ref_id.invoice_line:
					sum1 = sum1 + l.quantity
					sum2 = sum2 + l.accumulated_remaining
				if sum1 == sum2:
					self.account_invoice_ref_id.write({'partials_completed':True})
		return res

	@api.multi
	def cancel(self):
		res = super(document_inventory,self).cancel()
		if self.reference_by == 'document' and self.type_document.movement_classify == 'internal_uses':
		    # if self.document_ref_id.partials_completed:
			self._dui_to_ui()

		return res

	@api.multi
	def _dui_to_ui(self):
		"""This function is used to change the status of the completed_partials
			field to make the change in the accumulated field.

		Decorators:
			api.multi
		"""
		if self.reference_by == 'document':
			if self.document_ref_id:
				for selfline in self.lines_ids:
					for uiline in self.document_ref_id.lines_ids:
						if selfline.product_id.id == uiline.product_id.id:
							uiline.write(
								{'accumulated_remaining': uiline.accumulated_remaining - selfline.quantity}
							)
		sum1 = 0
		sum2 = 0
		if self.reference_by == 'document':
			if self.document_ref_id:
				for uiline in self.document_ref_id.lines_ids:
					sum1 = sum1 + uiline.quantity
					sum2 = sum2 + uiline.accumulated_remaining
				if sum1 == sum2:
					self.document_ref_id.partials_completed = True
				else:
					self.document_ref_id.partials_completed = False

	def button_dummy(self):
		return True


	def create_stock_picking_inter_wh(self,doc,location_id, location_dest_id):
		stock_picking_obj = self.env['stock.picking']
		lines = self._get_stock_moves_inter_wh(location_id, location_dest_id)
		# if not self.type_document.type_operation in ('internal','outgoing','incoming'):
		# 	operation_type = 'incoming'
		operation_type = self.type_document.type_operation

		values = {
			#'partner_id':self.partner_id.id or None,
			'date': fields.Datetime.now(),
			'min_date': fields.Datetime.now(),
			'origin': doc,
			'move_type':'direct',
			'invoice_state': 'none',
			'picking_type_id': self.env['stock.picking.type'].search([('code','=',operation_type),('warehouse_id','=',self.warehouse_id.id)])[0].id,
			'move_lines': [(0, 0, x) for x in lines],
			'document_inventory_id':self.id,
		}

		new_id = stock_picking_obj.create(values)
		new_id.action_confirm()
		if operation_type == 'incoming':
			new_id.action_assign()

		return new_id.id

	@api.multi
	def _get_stock_moves_inter_wh(self,location_id,location_dest):
		lines_vals = []

		for l in self.lines_ids:
			line = {
				'product_id':l.product_id.id,
				'product_uom_qty':l.quantity,
				'product_uom':l.product_id.uom_id.id,
				'name':l.product_id.name,
				'date':fields.Datetime.now(),
				'date_expected':fields.Datetime.now(),
				'invoice_state':'none',
				'procure_method':'make_to_stock',
				'location_id': location_id,
				'location_dest_id':location_dest_id,
			}

			lines_vals.append(line)
		return lines_vals

class document_inventory_line(models.Model):
	_inherit = 'document.inventory.line'

	@api.multi
	def split_quantities(self):
		if self.quantity>1:
			self.quantity = (self.quantity-1)
			new_id = self.copy(context=self.env.context)
			new_id.quantity = 1
		if self and self[0]:
			return self.document_id.button_dummy()
