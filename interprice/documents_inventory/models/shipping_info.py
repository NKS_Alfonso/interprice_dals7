# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.tools.translate import _
from datetime import datetime
from openerp import exceptions
import pdb

class shipping_info(models.Model):
	_name = 'shipping.info'

	document_inventory_id = fields.Many2one('document.inventory',string="Document")
	shipping_method = fields.Selection(
		[('local','Local'),('shipping_service','Shipping service')],
		string='Shipping method')
	name_shipping_service_company_id = fields.Many2one('delivery.carrier',string='Name shipping service company')
	delivery_form = fields.Selection(
		[('at_home','At Home'),('happen','Happen')],
		string="Delivery form")
	shipping_company = fields.Char(string="Shipping Company")
	#shipping_guide = fields.Char(string="Shipping guide")
	shipping_guide_ids = fields.One2many(comodel_name='shipping.guide', inverse_name='shipping_id',string="Shipping guide")
	number_of_packages = fields.Integer(string="Number of packages")
	vehicle_id = fields.Many2one('fleet.vehicle',string="Vehicle")
	driver_id = fields.Many2one('res.users',string="Driver")
	
	street = fields.Char()
	street2 = fields.Char()
	city = fields.Char()
	city_id = fields.Many2one('res.country.state.city',string="Ciudad")
	state_id = fields.Many2one('res.country.state')
	zip = fields.Char()
	country_id = fields.Many2one('res.country')

	# @api.one
	# @api.constrains('name_shipping_service_company_id','shipping_guide')
	# def _check_shipping_data(self):
	# 	doct_ids = self.env['document.inventory'].search([('id','!=',self.document_inventory_id.id),('name_shipping_service_company','=',self.name_shipping_service_company),('name_shipping_service_company','!=',False),('shipping_guide','=',self.shipping_guide),('shipping_guide','!=',False)])
	# 	#print 'doct_ids',doct_ids
	# 	if doct_ids:
	# 		raise exceptions.ValidationError('La guia y paqueteria ya han sido registrados')

	@api.onchange('delivery_form')
	def on_change_delivery_form(self):
		if self.delivery_form == 'at_home':
			if self.document_inventory_id.warehouse_dest_id:
				self.update({
					'street':self.document_inventory_id.warehouse_dest_id.partner_id.street,
					'street2':self.document_inventory_id.warehouse_dest_id.partner_id.street2,
					'city':self.document_inventory_id.warehouse_dest_id.partner_id.city,
					'state_id':self.document_inventory_id.warehouse_dest_id.partner_id.state_id.id,
					'zip':self.document_inventory_id.warehouse_dest_id.partner_id.zip,
					'country_id':self.document_inventory_id.warehouse_dest_id.partner_id.country_id.id
				})
		else:
			self.update({
				'street':False,
				'street2':False,
				'city':False,
				'state_id':False,
				'zip':False,
				'country_id':False
			})

class guide(models.Model):
	_name = 'shipping.guide'

	number = fields.Char(string="Number")
	shipping_id = fields.Many2one('shipping.info')
		