# -*- coding: utf-8 -*-
from openerp import models, fields, api

class picking(models.Model):
	_inherit = 'stock.picking'

	document_inventory_id = fields.Many2one('document.inventory')

class stock_transfer_details(models.TransientModel):
	_inherit = 'stock.transfer_details'

	@api.one
	def do_detailed_transfer(self):
		picking_type = self.picking_id.get_picking_type()
		if picking_type == 'entrada':
			purchase = self.env['purchase.order'].search([('name', '=', self.picking_id.origin)])
			if purchase:
				current_rate = round(1 / purchase.currency_id.rate_silent, 4)
				for move in self.picking_id.move_lines:
					price_unit = purchase.order_line.search([
						('product_id', '=', move.product_id.id), ('order_id', '=', purchase.id)], limit=1).price_unit
					move.price_unit = current_rate * price_unit
		res = super(stock_transfer_details,self).do_detailed_transfer()

		if self.picking_id.document_inventory_id:
			self.picking_id.document_inventory_id.change_state(self.picking_id.document_inventory_id.type_document.state_document_to_transfer)

			if self.picking_id.document_inventory_id.document_in_transit_id:
				self.picking_id.document_inventory_id.document_in_transit_id.change_state('supplied')
			
			if self.picking_id.document_inventory_id.type_document.state_document_to_transfer == 'transit':
				document = self.picking_id.document_inventory_id

				type_document_id = document.type_document.type_document_form_transit_to_dest_id.id
				warehouse_id = document.warehouse_id.id
				warehouse_dest_id = document.warehouse_dest_id.id
				location_id = document.type_document.default_location_id.id
				location_dest_id = document.location_dest_id.id

				#new_document = document.copy()
				lines = document._fill_lines(document.lines_ids,'document')
				#print type_document_id,warehouse_id,warehouse_dest_id,location_id,location_dest_id
				new_document = self.env['document.inventory'].create({
					'type_document': type_document_id,
					'document_in_transit_id':document.id,
					'warehouse_id': warehouse_id,
					'warehouse_dest_id': warehouse_dest_id,
					'location_id': location_id,
					'location_dest_id': location_dest_id,
					'lines_ids':[(0, 0, x) for x in lines]
				})

				new_document.generate()
				#new_document.generate_doc_inter_wh(location_id,location_dest_id)
				#document.write({'state':'supplied'})

		return res

class location(models.Model):
	_inherit = 'stock.location'

	warehouse_id = fields.Many2one('stock.warehouse',string="Warehouse")