# -*- coding: utf-8 -*-
from openerp import models, fields, api

class purchase(models.Model):
	_inherit = 'purchase.order'

	partials_completed = fields.Boolean(copy=False)

class purchase_line(models.Model):
	_inherit = 'purchase.order.line'

	@api.one
	def _get_quantity_remaining(self):
		total = 0
		self.quantity_remaining = self.product_qty - self.accumulated_remaining

	quantity_remaining = fields.Float(string="Quantity remaining", compute=_get_quantity_remaining,copy=False)
	accumulated_remaining = fields.Float(string="accumulated remaining", default=0,copy=False)