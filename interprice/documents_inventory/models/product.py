# coding: utf-8

from openerp import api, models

class ProductProduct(models.Model):
	_inherit = "product.product"

	@api.multi
	def get_product_available_by_warehouse(self,warehouse_id):
		ctx = dict(self._context)
		res = {}.fromkeys(self.ids, {})
		for product in self:
			qty_available = []
			virtual_available = []
			incoming_qty = []
			outgoing_qty = []
			for warehouse in self.env['stock.warehouse'].sudo().search([('id','=',warehouse_id)]):
				ctx.update({'warehouse': warehouse.id, 'location': False})
				product_qty = product.with_context(ctx).\
					_product_available()[product.id]
				qty_available += [(warehouse, product_qty['qty_available'])]
				virtual_available += [
					(warehouse, product_qty['virtual_available'])]
				incoming_qty += [(warehouse, product_qty['incoming_qty'])]
				outgoing_qty += [(warehouse, product_qty['outgoing_qty'])]
			res[product.id] = {
				'qty_available': qty_available,
				'virtual_available': virtual_available,
				'incoming_qty': incoming_qty,
				'outgoing_qty': outgoing_qty,
			}
		return res

	@api.multi
	def get_product_available_by_location(self,location_id):
		ctx = dict(self._context)
		res = {}.fromkeys(self.ids, {})
		for product in self:
			qty_available = []
			virtual_available = []
			incoming_qty = []
			outgoing_qty = []
			for location in self.env['stock.location'].sudo().search([('id','=',location_id)]):
				ctx.update({'warehouse': False,'location': location.id})
				product_qty = product.with_context(ctx).\
					_product_available()[product.id]
				qty_available += [(location, product_qty['qty_available'])]
				virtual_available += [
					(location, product_qty['virtual_available'])]
				incoming_qty += [(location, product_qty['incoming_qty'])]
				outgoing_qty += [(location, product_qty['outgoing_qty'])]
			res[product.id] = {
				'qty_available': qty_available,
				'virtual_available': virtual_available,
				'incoming_qty': incoming_qty,
				'outgoing_qty': outgoing_qty,
			}
		return res
