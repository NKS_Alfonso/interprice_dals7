# -*- coding: utf-8 -*-
from openerp import models, fields, api

class invoice(models.Model):
	_inherit = 'account.invoice'

	partials_completed = fields.Boolean(copy=False)

class invoice_line(models.Model):
	_inherit = 'account.invoice.line'

	@api.one
	def _get_quantity_remaining(self):
		total = 0
		self.quantity_remaining = self.quantity - self.accumulated_remaining
		
	quantity_remaining = fields.Float(string="Quantity remaining", compute=_get_quantity_remaining,copy=False)
	accumulated_remaining = fields.Float(string="accumulated remaining", default=0,copy=False)