# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, _, tools


class DepositFileVoucherView(models.Model):
    """Model for vouchers used on 'Piramide de pago',
    posted and with application of cash, bank or check."""

    # odoo model properties
    _name = 'deposit.file.voucher.view'
    _table = 'deposit_file_voucher_view'
    _description = 'Deposit file voucher view'
    _auto = False  # Don't let odoo create table
    _rec_name = 'folio'
    # _order = 'id DESC'

    # View fields
    folio = fields.Char(
        help=_("Deposit file voucher folio"),
        readonly=True,
        string=_("Folio")
    )
    voucher_id = fields.Many2one(
        comodel_name='account.voucher',
        help=_("Deposit file voucher"),
        readonly=True,
        string=_("Payment"),
    )
    advance_id = fields.Many2one(
        comodel_name='advance.payment',
        help=_("Deposit file voucher advance"),
        readonly=True,
        string=_("Advance"),
    )
    voucher_date = fields.Datetime(
        help=_("Deposit file voucher date"),
        readonly=True,
        string=_("Voucher Date")
    )
    journal_id = fields.Many2one(
        comodel_name='account.journal',
        help=_("Deposit file journal"),
        readonly=True,
        string=_("Journal"),
    )
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        help=_("Deposit file journal currency"),
        readonly=True,
        string=_("Currency"),
    )
    cost_center_id = fields.Many2one(
        comodel_name='account.cost.center',
        help=_("Deposit file journal cost center"),
        readonly=True,
        string=_("Cost center"),
    )
    account_apply_id = fields.Many2one(
        comodel_name='account.account.apply',
        help=_("Deposit file journal apply"),
        readonly=True,
        string=_("Account apply"),
    )
    user_id = fields.Many2one(
        comodel_name='res.users',
        help=_("Deposit file cost center user"),
        readonly=True,
        string=_("User"),
    )

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'deposit_file_voucher_view')

        # Create view
        cr.execute("""
            CREATE OR REPLACE VIEW deposit_file_voucher_view AS
            (SELECT
                DISTINCT
                    ROW_NUMBER() OVER() AS id,
                    dfTable.folio,
                    CASE WHEN dfTable.voucher_id = 0
                                        THEN NULL
                                    ELSE dfTable.voucher_id
                                        END AS voucher_id,
                    CASE WHEN dfTable.advance_id = 0
                                        THEN NULL
                                    ELSE dfTable.advance_id
                                        END AS advance_id,
                    dfTable.voucher_date,
                    dfTable.journal_id,
                    dfTable.currency_id,
                    dfTable.cost_center_id,
                    dfTable.account_apply_id,
                    dfTable.user_id
                FROM
                (WITH check_voucher AS (
                    SELECT DISTINCT ac.voucher_id
                    FROM account_check ac
                    WHERE ac.type = 'third_check'
                    ORDER BY ac.voucher_id ASC)
                SELECT
                    DISTINCT
                    av.number AS folio,
                    av.id AS voucher_id,
                    0 AS advance_id,
                    av.date AS voucher_date,
                    aj.id AS journal_id,
                    rcur.id AS currency_id,
                    accaj.account_cost_center_id AS cost_center_id,
                    aaa.id AS account_apply_id,
                    av.write_uid           AS user_id
                    FROM account_voucher av
                    JOIN account_journal_piramide_de_pago_rel ajpdp
                    ON (ajpdp.account_journal_id = av.journal_id)
                    JOIN account_journal aj
                    ON (aj.id = ajpdp.account_journal_id)
                    JOIN res_company rcom
                    ON (rcom.id = aj.company_id)
                    JOIN res_currency rcur
                    ON (rcur.id = COALESCE(aj.currency, rcom.currency_id))
                    JOIN account_cost_center_account_journal_rel accaj
                    ON (accaj.account_journal_id = aj.id)
                    JOIN account_cost_center_res_users_rel accru
                    ON (accru.account_cost_center_id = accaj.account_cost_center_id)
                    JOIN account_account_apply aaa
                    ON (aaa.id = aj.account_account_apply_id)
                    LEFT JOIN check_voucher cv
                    ON (cv.voucher_id = av.id)
                    WHERE av.state in ('posted', 'cancel')
                    AND aaa.code IN ('cash', 'bank', 'check', 'credit_card', 'deposit')
            UNION ALL
                SELECT
                    DISTINCT
                    ap.number AS folio,
                    0 AS voucher_id,
                    ap.id AS advance_id,
                    ap.register_date AS voucher_date,
                    aj.id AS journal_id,
                    rcur.id AS currency_id,
                    accaj.account_cost_center_id AS cost_center_id,
                    aaa.id AS account_apply_id,
                    ap.user_generated AS user_id
                    FROM advance_payment ap
                    INNER JOIN account_journal aj
                    ON (aj.id = ap.journal_id)
                    INNER JOIN res_currency rcur
                    ON (rcur.id = ap.currency_id)--COALESCE(aj.currency, rcom.currency_id))
                    INNER JOIN account_cost_center_account_journal_rel accaj
                    ON (accaj.account_journal_id = aj.id)
                    INNER JOIN account_account_apply aaa
                    ON (aaa.id = ap.payment_type_id)
                    WHERE ap.state in( 'generated', 'applied partial','applied','canceled')
                    AND aaa.code IN ('cash', 'check', 'credit_card', 'deposit')
                ) AS dfTable
            )
        """)
        # cr.execute(
        #     """
        #     CREATE OR REPLACE VIEW deposit_file_voucher_view
        #         AS (
        #             WITH check_voucher AS (
        #                 SELECT DISTINCT ac.voucher_id
        #                 FROM account_check ac
        #                 WHERE ac.type = 'third_check'
        #                 ORDER BY ac.voucher_id ASC
        #             )
        #             SELECT
        #                 DISTINCT av.id AS voucher_id,
        #                 av.date AS voucher_date,
        #                 aj.id AS journal_id,
        #                 rcur.id AS currency_id,
        #                 accaj.account_cost_center_id AS cost_center_id,
        #                 aaa.id AS account_apply_id,
        #                 accru.res_users_id AS user_id
        #             FROM account_voucher av
        #             JOIN account_journal_piramide_de_pago_rel ajpdp
        #                 ON (ajpdp.account_journal_id = av.journal_id)
        #             JOIN account_journal aj
        #                 ON (aj.id = ajpdp.account_journal_id)
        #             JOIN res_company rcom
        #                 ON (rcom.id = aj.company_id)
        #             JOIN res_currency rcur
        #                 ON (rcur.id = COALESCE(aj.currency, rcom.currency_id))
        #             JOIN account_cost_center_account_journal_rel accaj
        #                 ON (accaj.account_journal_id = aj.id)
        #             JOIN account_cost_center_res_users_rel accru
        #                 ON (accru.account_cost_center_id = accaj.account_cost_center_id)
        #             JOIN account_account_apply aaa
        #                 ON (aaa.id = aj.account_account_apply_id)
        #             LEFT JOIN check_voucher cv
        #                 ON (cv.voucher_id = av.id)
        #             WHERE av.state = 'posted'
        #                 AND aaa.code IN ('cash', 'bank', 'check', 'credit_card', 'deposit')
        #             ORDER BY av.id ASC
        #         )
        #     """
        # )
