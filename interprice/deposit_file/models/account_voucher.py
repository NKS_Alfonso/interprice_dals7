# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, exceptions, _


class AccountVoucher(models.Model):
    _inherit = 'account.voucher'

    currency = fields.Many2one(
        comodel_name='res.currency',
        compute='_get_currency',
        help=_("Voucher currency"),
        string=_("Currency"),
    )
    doc_amount = fields.Float(
        compute='_get_doc_amount',
        help=_("Voucher amount"),
        string=_("Amount"),
    )
    deposit_file = fields.Many2one(
        comodel_name='deposit.file',
        help=_("Deposit file reference"),
        string=_("Deposit file"),
    )

    @api.one
    def _get_currency(self):
        self.currency = self.journal_id.currency \
            or self.journal_id.company_id.currency_id

    @api.one
    def _get_doc_amount(self):
        amount = self.amount
        if self.amount == 0.00 and len(self.line_dr_ids) > 0:
            for line in self.line_dr_ids:
                amount += line.amount
        self.doc_amount = amount

    @api.multi
    def cancel_voucher(self):
        deposit_file_voucher = self.env[
            'deposit.file.voucher'].search(
            [('voucher', '=', self.id)])
        if deposit_file_voucher:
            if deposit_file_voucher.state == "accepted":
                raise exceptions.Warning(
                    _("You can not cancel a payment that is on an accepted deposit slip, you must first cancel the deposit slip.")
                )
        return super(AccountVoucher, self).cancel_voucher()
