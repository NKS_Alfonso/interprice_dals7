# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    deposit_file = fields.Many2one(
        comodel_name='deposit.file',
        help=_("Deposit file reference"),
        string=_("Deposit file"),
    )
