# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
from datetime import datetime
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, exceptions, _


class DepositFile(models.Model):
    """Deposit file model"""

    # odoo model properties
    _name = 'deposit.file'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'folio'
    _order = "write_date desc"

    # field selection options
    deposit_file_state = [
        ('draft', _("Draft")),
        ('normal', _("Normal")),
        ('accepted', _("Accepted")),
        ('partial_cancelled', _("Partially cancelled")),
        ('cancelled', _("Cancelled"))
    ]

    # initial domain methods
    @api.model
    def bank_domain(self):
        """Returns all bank ids where its journals are used on payments.
        'deposit_file_voucher_view' has all journals used on payments
        as well as journals used on 'piramide_de_pago' and journals with
        applications of cash, bank or check.
        """
        bank_ids = []
        self.env.cr.execute(
            """
            SELECT DISTINCT rpb.id
            FROM res_partner_bank rpb
            JOIN deposit_file_voucher_view dfv
                ON (dfv.journal_id = rpb.journal_id)
            --WHERE dfv.user_id = CAST(%s AS INT)
            ORDER BY rpb.id ASC;
            """
            # ,
            # (self.env.user.id,)
        )
        if self.env.cr.rowcount:
            bank_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', bank_ids)]

    @api.model
    def journal_domain(self):
        """Returns all journal ids filtered by currency of the bank journal.
        """
        journal_ids = []
        self.env.cr.execute(
            """
            WITH bank_journal_currency AS (
                SELECT
                    DISTINCT rcur.id AS currency_id
                FROM res_partner_bank rpb
                JOIN res_company rcom
                    ON (rcom.id = rpb.company_id)
                JOIN res_currency rcur
                    ON (rcur.id = COALESCE(rpb.currency2_id, rcom.currency_id))
                WHERE rpb.id = CAST(%s AS INT)
            )
            SELECT
                DISTINCT dfv.journal_id
            FROM deposit_file_voucher_view dfv
            JOIN bank_journal_currency bjc
                ON (bjc.currency_id = dfv.currency_id)
            --WHERE dfv.user_id = CAST( AS INT)
            ORDER BY dfv.journal_id ASC;
            """,
            (
                self.bank.id,
                # self.env.user.id,
            )
        )
        if self.env.cr.rowcount:
            journal_ids = [j[0] for j in self.env.cr.fetchall()]
        return [('id', 'in', journal_ids)]

    @api.model
    def cost_center_domain(self):
        cost_center_ids = []
        self.env.cr.execute(
            """
            SELECT
                DISTINCT dfv.cost_center_id
            FROM deposit_file_voucher_view dfv
            --WHERE dfv.user_id = CAST(%s AS INT)
            ORDER BY dfv.cost_center_id ASC;.
            """
            # ,
            # (self.env.user.id,)
        )
        if self.env.cr.rowcount:
            cost_center_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', cost_center_ids)]

    @api.model
    def application_domain(self):
        application_ids = []
        self.env.cr.execute(
            """
            SELECT
                DISTINCT dfv.account_apply_id
            FROM deposit_file_voucher_view dfv
            ORDER BY dfv.account_apply_id ASC;
            """
        )
        if self.env.cr.rowcount:
            application_ids = [j[0] for j in self.env.cr.fetchall()]
        return [('id', 'in', application_ids)]

    # fields
    folio = fields.Char(
        copy=False,
        default='_',
        help=_("Deposit file folio"),
        readonly=True,
        required=True,
        store=True,
        string=_("Folio"),
    )
    deposit_file_folio = fields.Char(
        copy=False,
        help=_("User deposit file folio"),
        required=True,
        states={
            'accepted': [('readonly', True)],
            'cancelled': [('readonly', True)],
        },
        string=_("Reference folio"),
    )
    observations = fields.Text(
        copy=False,
        help=_("Deposit file observations"),
        states={
            'cancelled': [('readonly', True)]
        },
        string=_("Observations"),
    )
    date = fields.Date(
        copy=False,
        default=fields.Date.today(),
        help=_("Deposit file normal state date"),
        readonly=True,
        store=True,
        string=_("Date"),
    )
    date_bank_accepted = fields.Date(
        copy=False,
        # default=fields.Date.today(),
        help=_("Deposit file date bank accepted"),
        readonly=True,
        store=True,
        string=_("Date bank accepted"),
    )
    normal_date = fields.Date(
        copy=False,
        help=_("Deposit file normal state date"),
        string=_("Normal state date"),
    )
    accepted_date = fields.Date(
        copy=False,
        help=_("Deposit file accepted state date"),
        string=_("Accepted state date"),
    )
    partial_cancelled_date = fields.Date(
        copy=False,
        help=_("Deposit file partial cancelled state date"),
        string=_("Partial cancelled state date"),
    )
    cancelled_date = fields.Date(
        copy=False,
        help=_("Deposit file cancelled state date"),
        string=_("Cancelled state date"),
    )
    state = fields.Selection(
        copy=False,
        default='draft',
        help=_("Deposit file state"),
        required=True,
        readonly=True,
        selection=deposit_file_state,
        string=_("state"),
        track_visibility='always'
    )
    prev_state = fields.Selection(
        copy=False,
        default='draft',
        help=_("Deposit file state"),
        required=True,
        selection=deposit_file_state,
        string=_("state"),
    )
    normal_user = fields.Many2one(
        comodel_name='res.users',
        copy=False,
        help=_("state changed user"),
        string=_("Normal state user"),
    )
    accepted_user = fields.Many2one(
        comodel_name='res.users',
        copy=False,
        help=_("state changed user"),
        string=_("Accepted state user"),
    )
    partial_cancelled_user = fields.Many2one(
        comodel_name='res.users',
        copy=False,
        help=_("state changed user"),
        string=_("Partial cancel state user"),
    )
    cancelled_user = fields.Many2one(
        comodel_name='res.users',
        copy=False,
        help=_("state changed user"),
        string=_("Cancel state user"),
    )
    bank = fields.Many2one(
        comodel_name='res.partner.bank',
        copy=False,
        domain=bank_domain,
        help=_("Deposit file bank"),
        readonly=True,
        required=True,
        states={
            'draft': [('readonly', False)],
        },
        string=_("Bank"),
    )
    journal = fields.Many2one(
        comodel_name='account.journal',
        copy=False,
        domain=journal_domain,
        help=_("Same bank currency journal"),
        readonly=True,
        required=True,
        states={
            'draft': [('readonly', False)],
        },
        string=_("Payment journal"),
    )
    changed_fields = fields.Text(
        copy=False,
        default="{}",
        help=_("Changed fields and values dictionary"),
        required=True,
        string=_("Changed fields")
    )
    # filter fields
    start_date = fields.Date(
        copy=False,
        help=_("Initial date to filter payments"),
        readonly=True,
        states={
            'draft': [('readonly', False)],
        },
        string=_("Initial date"),
    )
    end_date = fields.Date(
        copy=False,
        help=_("Final date to filter payments"),
        readonly=True,
        states={
            'draft': [('readonly', False)],
        },
        string=_("Final date"),
    )
    cost_center = fields.Many2one(
        comodel_name='account.cost.center',
        copy=False,
        #domain=cost_center_domain,
        help=_("Cost centers allowed by user"),
        readonly=True,
        required=True,
        states={
            'draft': [('readonly', False)],
        },
        string=_("Cost center"),
    )
    application = fields.Many2one(
        comodel_name='account.account.apply',
        copy=False,
        domain=application_domain,
        help=_("Account application"),
        readonly=True,
        required=True,
        states={
            'draft': [('readonly', False)],
        },
        string=_("Application"),
    )
    deposit_file_vouchers = fields.One2many(
        comodel_name='deposit.file.voucher',
        copy=False,
        help=_("Deposit file voucher(Payment)."),
        inverse_name='deposit_file',
        readonly=True,
        states={
            'accepted': [('readonly', False)],
            'partial_cancelled': [('readonly', False)],
            'draft': [('readonly', False)]
        },
        string=_("Payment")
    )
    policy_ids = fields.One2many(
        comodel_name='account.move',
        help="Deposit file policies",
        inverse_name='deposit_file',
        readonly=True,
        store=True,
        string="Policies",
    )

    # Print report
    @api.multi
    def show_pdf(self):
        return self.env['report'].get_action(
            self.deposit_file_vouchers,
            'deposit_file.deposit_file_report_pdf'
        )

    # onchange methods
    @api.onchange('bank')
    def onchange_bank(self):
        """Changes the domain of the journal field with the journal ids
        that has the same currency as the bank's journal currency,
        and only journals used on payments and inside a 'piramide de pago'.
        """
        res = {}
        res['domain'] = {
            'journal': self.journal_domain(),
            'bank': self.bank_domain(),
        }
        self.journal = False  # Clean journal field
        return res

    @api.onchange(
        'journal', 'cost_center', 'application', 'start_date', 'end_date'
    )
    def onchange_voucher(self):
        """Changes the domain of the vouchers field with the voucher ids
        that has the same currency as the journal currency and has the
        same applications assigned and cost_center,
        and only journals used on payments and inside a 'piramide de pago'.
        """
        self.deposit_file_vouchers = False

    @api.onchange('journal')
    def onchange_journal(self):
        res = {}
        res['domain'] = {}
        cost_center_list = []
        for cost_center in self.journal.centro_costos:
            cost_center_list.append(cost_center.id)
        res['domain'].update({'cost_center': [('id', 'in', cost_center_list)]})
        application_list = []
        for application in self.journal.account_account_apply_id:
            application_list.append(application.id)
        res['domain'].update({'application': [('id', 'in', application_list)]})
        self.update(
            {
                'application': application_list[0] if len(application_list) > 0 else False,
                'cost_center': cost_center_list[0] if len(cost_center_list) > 0 else False,
            }
        )
        return res

    # overriden methods
    @api.model
    def create(self, vals):
        """Override original create method"""
        self.validate_filters()
        state = vals.get('state', False)
        if state:
            vals['prev_state'] = state

        vals['folio'] = '/'
        return super(DepositFile, self).create(vals)

    @api.one
    def write(self, vals):
        """Override original write method"""
        self.validate_filters()
        vals['changed_fields'] = vals
        old_dfv_ids = [
            dfv.id
            for dfv in self.deposit_file_vouchers
            if dfv.state == 'cancelled'
        ]
        super(DepositFile, self).write(vals)
        vals = {}
        new_dfv_ids = [
            cv.id for cv
            in self.deposit_file_vouchers if cv.cancel
        ]
        amount_ids = set(new_dfv_ids) - set(old_dfv_ids)
        another_currency = False
        if self.journal.currency.id:
            another_currency = True
        if self.state in ('accepted', 'partial_cancelled'):
            if len(new_dfv_ids) < len(self.deposit_file_vouchers) and len(new_dfv_ids) > 0:
                if amount_ids:
                    vals['state'] = 'partial_cancelled'
                    vals['partial_cancelled_user'] = self.env.user.id
                    vals['partial_cancelled_date'] = fields.Date.context_today(
                        self)
                    policy_ids = [p.id for p in self.policy_ids]
                    # if self.application.code != 'check':
                    policy_ids.append(
                        self.create_policy(reverse=True, amount_ids=amount_ids, another_currency=another_currency).id
                    )
                    vals['policy_ids'] = [(6, 0, policy_ids)]
            elif len(new_dfv_ids) == len(self.deposit_file_vouchers):
                vals['state'] = 'cancelled'
                vals['cancelled_user'] = self.env.user.id
                vals['cancelled_date'] = fields.Date.context_today(self)
                policy_ids = [p.id for p in self.policy_ids]
                # if self.application.code != 'check':
                policy_ids.append(
                    self.create_policy(reverse=True, amount_ids=amount_ids, another_currency=another_currency).id
                )
                vals['policy_ids'] = [(6, 0, policy_ids)]
        elif self.state == 'cancelled' and self.prev_state in ('accepted', 'partial_cancelled'):
            amount_ids = [
                dfv.id
                for dfv in self.deposit_file_vouchers
                if dfv.state == 'accepted'
            ]
            if amount_ids:
                policy_ids = [p.id for p in self.policy_ids]
                # if self.application.code != 'check':
                # Create a policy for Each line of payment if is not company currency
                if self.journal.currency.id or self.application.code == 'check':
                    for amount in amount_ids:
                        policy_ids.append(self.create_policy(reverse=True, amount_ids=[amount], another_currency=another_currency).id)
                else:
                    policy_ids.append(self.create_policy(reverse=True, amount_ids=amount_ids, another_currency=another_currency).id)
                vals['policy_ids'] = [(6, 0, policy_ids)]
                self.cancel_deposit_file_vouchers()
                self.prev_state = 'cancelled'
        return super(DepositFile, self).write(vals)

    @api.multi
    def unlink(self):
        """Override original unlink (delete) method"""
        for deposit_file in self:
            if deposit_file.state != 'draft':
                raise exceptions.Warning(
                    _("Deposit file(s) must be on draft state to delete"))
        return super(DepositFile, self).unlink()

    # button methods
    @api.one
    def button_normal(self):
        self.validate_lines_used()
        self.folio = self.env['ir.sequence'].get('deposit.file')
        # Set state and send message
        self.set_workflow_state('normal')
        self.deposit_file_vouchers.write({'state': 'normal'})
        """return self.set_message(
            _name='button_normal',
            _subject=_("state changed to normal"),
            _body=_("Deposit file can be updated without adding more payments")
        )"""

    @api.one
    def button_accept(self):
        # Create deposit file policy
        # if self.application.code != 'check':
        # Create policy split in currency distinct of company
        self.validate_lines_used()
        if self.journal.currency.id:
            for dfv in self.deposit_file_vouchers:
                self.policy_ids += self.create_policy(reverse=False, amount_ids=dfv.id, another_currency=True)
        else:
            self.policy_ids += self.create_policy()
        # Set state and send message
        self.set_workflow_state('accepted')
        # Set deposit file status on voucher lines
        self.deposit_file_vouchers.write({'state': 'accepted'})
        """return self.set_message(
            _name='button_accept',
            _subject=_("state changed to accepted"),
            _body=_("New deposit file policy created")
        )"""

    @api.one
    def button_cancel(self):
        # Set state and send message
        self.set_workflow_state('cancelled')
        # Set deposit file status on voucher lines
        # self.deposit_file_vouchers.write(
        #     {
        #         'state': 'cancelled',
        #         'cancel': True
        #     }
        # )
        """return self.set_message(
            _name='button_cancel',
            _subject=_("state changed to cancelled"),
            _body=_("Some lines are cancelled, please take a look"
                    " on 'cancellation' checkbox of each line")
        )"""

    @api.one
    def button_draft(self):
        """Document return to draft state"""
        self.set_workflow_state('draft')

    @api.one
    def cancel_deposit_file_vouchers(self):
        """Cancel the lines of deposit file"""
        self.deposit_file_vouchers.write(
            {
                'state': 'cancelled',
                'cancel': True
            }
        )

    # workflow methods
    @api.one
    def set_workflow_state(self, _state):
        self.validate_data()
        # Set prev, current and user state
        self.prev_state = self.state
        self.state = _state
        if _state == 'normal':
            self.normal_user = self.env.user.id
            self.normal_date = fields.Date.context_today(self)
        elif _state == 'accepted':
            self.accepted_user = self.env.user.id
            self.accepted_date = fields.Date.context_today(self)
        elif _state == 'cancelled':
            self.cancelled_user = self.env.user.id
            self.cancelled_date = fields.Date.context_today(self)
        ###################################

    # filter validation methods
    @api.multi
    def validate_lines_used(self):
        """This method validate the lines used in the deposit file"""
        used_line = []
        warning = False
        payments = ''
        for line in self.deposit_file_vouchers:
            if line.advance and not line.voucher:
                used_line = self.env['deposit.file.voucher'].search([
                    ('advance', '=', line.advance.id)
                ])
            elif line.voucher and not line.advance:
                used_line = self.env['deposit.file.voucher'].search([
                    ('voucher', '=', line.voucher.id)
                ])
            for l in used_line:
                if l.deposit_file.application.code == 'check':
                    if l.state in ('accepted', 'cancelled'):
                        warning = True
                        payments += l.folio.folio + '\n'
                else:
                    if l.state == 'accepted':
                        warning = True
                        payments += l.folio.folio + '\n'
        if warning:
            raise exceptions.Warning(
                _("The next payments or advances already this applied in other deposit file.") + "\n" + payments
            )

    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        ds = self.start_date
        de = self.end_date

        if ds and de and ds > de:
            self.start_date = None
            self.end_date = None
            raise exceptions.Warning(
                _("'Date to' must be higher than 'Date from'.")
            )

    def validate_data(self):
        if len(self.deposit_file_vouchers) == 0:
            raise exceptions.Warning(
                _("Add payments to change the deposit file state"))

    # Methods
    """def set_message(self, _name, _subject, _body):
        post_vars = {
            'subject': _subject,
            'body': _body
        }
        self.message_post(type="notification",
                          subtype="mt_comment", **post_vars)
        return {
            'type': 'ir.actions.client',
            'tag': 'action_info',
            'name': _name,
            'params': {
                'title': _subject,
                'text': _body,
                'sticky': False
            }
        }"""

    def get_amount(self, cancel, ids=False, another_currency=False):
        amount = 0
        if cancel:
            if ids:
                query = "SELECT SUM(dfv.amount)\n"\
                        "FROM deposit_file_voucher dfv\n"
                if len(ids) > 1:
                    query += "WHERE dfv.id IN %s" % (str(tuple(ids)))
                elif len(ids) == 1:
                    query += "WHERE dfv.id = CAST(%s AS INT)" % (list(ids)[0])

                self.env.cr.execute(query)
            else:
                self.env.cr.execute(
                    """
                    SELECT SUM(dfv.amount)
                    FROM deposit_file_voucher dfv
                    WHERE dfv.deposit_file = CAST(%s AS INT)
                        AND dfv.state = 'accepted';
                    """,
                    (self.id,)
                )
        else:
            if another_currency:
                self.env.cr.execute(
                    """
                    SELECT dfv.amount
                    FROM deposit_file_voucher dfv
                    WHERE dfv.id = CAST(%s AS INT);
                    """,
                    (ids,)
                )
            else:
                self.env.cr.execute(
                    """
                    SELECT SUM(dfv.amount)
                    FROM deposit_file_voucher dfv
                    WHERE dfv.deposit_file = CAST(%s AS INT);
                    """,
                    (self.id,)
                )

        if self.env.cr.rowcount:
            amount = [a[0] for a in self.env.cr.fetchall()][0]
        return amount

    def get_currency(self):
        currency_id = False
        self.env.cr.execute(
            """
            SELECT
                DISTINCT rcur.id AS currency_id
            FROM res_partner_bank rpb
            JOIN res_company rcom
                ON (rcom.id = rpb.company_id)
            JOIN res_currency rcur
                ON (rcur.id = COALESCE(rpb.currency2_id, rcom.currency_id))
            WHERE rpb.id = CAST(%s AS INT)
            """,
            (self.bank.id,)
        )
        if self.env.cr.rowcount:
            currency_id = [a[0] for a in self.env.cr.fetchall()]
            currency_id = self.env['res.currency'].browse(currency_id)
        return currency_id

    def get_current_period(self):
        """Returns fiscal period of date with only
        month and year."""
        month_year = datetime.now().strftime("%m/%Y")
        return self.env['account.period'].search([('name', '=', month_year)])

    def get_company_partner_id(self):
        partner_id = False
        self.env.cr.execute(
            """
            SELECT rp.id
            FROM res_company rc
            JOIN res_partner rp
                ON (rp.company_id = rp.id)
            LIMIT 1;
            """
        )
        if self.env.cr.rowcount:
            partner_id = [j[0] for j in self.env.cr.fetchall()][0]
        return partner_id

    def get_amount_currency(self, from_currency, from_amount, to_currency, date, alter_rate, round=False):
        """ Convert `from_amount` from currency to `to_currency`. """
        # apply conversion rate
        if from_currency.id == to_currency.id:
            to_amount = from_amount
        else:
            if alter_rate > 1:
                to_amount = from_amount * alter_rate
            else:
                to_amount = from_amount * self.get_conversion_rate(from_currency, to_currency, date)
        # apply rounding and return
        return to_currency.round(to_amount) if round else to_amount

    def get_conversion_rate(self, from_currency, to_currency, date):
        """ Convert `from_amount` from currency to `to_currency`. """
        from_currency_rate = self.env['res.currency.rate'].search(
            [('currency_id', '=', from_currency.id),
             ('write_date', '<=', date)],
            order='write_date desc', limit=1)
        if not from_currency_rate:
            return 0.00
        todo_ttype = self._context.get('todo_ttype', False)
        if todo_ttype and todo_ttype == 'advance':
            from_rate = from_currency_rate.rate or 1
            to_rate = to_currency.rate or 1
        else:
            from_rate = from_currency_rate.rate_sale or 1
            to_rate = to_currency.rate_sale or 1
        return to_rate / from_rate

    def create_policy(self, reverse=False, amount_ids=False, another_currency=False):
        """Creates a deposit file policy for the accepted payments"""
        period = self.get_current_period()
        # company_partner_id = self.get_company_partner_id()
        currency = self.get_currency()
        company_currency = self.bank.company_id.currency_id
        amount = self.get_amount(cancel=reverse, ids=amount_ids, another_currency=another_currency)
        deposit_file_voucher = self.env['deposit.file.voucher'].browse(amount_ids)
        if another_currency:
            date = deposit_file_voucher.voucher.move_id.create_date or deposit_file_voucher.advance.register_date
            alter_rate = deposit_file_voucher.voucher.payment_rate or 1
            # amount_currency = currency.with_context(
            #     todo_ttype='payment'
            # ).compute(
            #     amount,
            #     company_currency,
            #     True,
            # )
            amount_currency = self.with_context(
                todo_ttype=('advance' if deposit_file_voucher.advance and not
                            deposit_file_voucher.voucher else 'payment')
            ).get_amount_currency(
                currency,
                amount,
                company_currency,
                date,
                alter_rate,
                True,
            )
        ext_cur = currency.id != company_currency.id
        if reverse:
            if self.application.code == 'check':
                ref = _("Returned check Deposit file ") + self.folio
                default_credit_account = (deposit_file_voucher.voucher.partner_id.property_account_receivable.id
                                          if deposit_file_voucher.voucher and not deposit_file_voucher.advance
                                          else deposit_file_voucher.advance.partner_id.property_account_receivable.id)
                company_partner_id = (deposit_file_voucher.voucher.partner_id.id
                                      if deposit_file_voucher.voucher and not deposit_file_voucher.advance
                                      else deposit_file_voucher.advance.partner_id.id)
            else:
                ref = _("Deposit file cancelled ") + self.folio
                default_credit_account = self.journal.default_credit_account_id.id
                company_partner_id = self.get_company_partner_id()
            amount_credit_debit = amount_currency if ext_cur else amount
            amount_credit_credit = 0
            amount_debit_debit = 0
            amount_debit_credit = amount_currency if ext_cur else amount
            amount_currency_credit = amount if ext_cur else False
            amount_currency_debit = -amount if ext_cur else False
            default_debit_account = self.bank.journal_id.default_debit_account_id.id
        else:
            ref = self.folio
            amount_credit_debit = 0
            amount_credit_credit = amount_currency if ext_cur else amount
            amount_debit_debit = amount_currency if ext_cur else amount
            amount_debit_credit = 0
            amount_currency_credit = -amount if ext_cur else False
            amount_currency_debit = amount if ext_cur else False
            default_credit_account = self.journal.default_credit_account_id.id
            default_debit_account = self.bank.journal_id.default_debit_account_id.id
            company_partner_id = self.get_company_partner_id()

        # ref = _("Deposit file cancelled ") + \
        #     self.folio if reverse and else self.folio
        # Create policy header
        header = {
            'state': 'draft',
            'journal_id': self.bank.journal_id.id,
            'period_id': period.id,
            'ref': ref,
            'date': fields.Date.context_today(self),
            'partner_id': company_partner_id
        }
        policy = self.env['account.move'].create(header)
        # Create policy credit line
        line_credit = {
            'name': "/",
            'partner_id': company_partner_id,
            'docto_referencia': self.folio,
            'cost_center_id': self.cost_center.id,
            'account_id': default_credit_account,
            'debit': amount_credit_debit,
            'credit': amount_credit_credit,
            'currency_id': currency.id if ext_cur else False,
            'amount_currency': amount_currency_credit,
            'period_id': period.id,
            'journal_id': self.journal.id,
            'move_id': policy.id,
            'date': self.date,
        }
        line_credit = self.env['account.move.line'].create(line_credit)
        # Create policy debit line
        line_debit = {
            'name': self.folio,
            'partner_id': company_partner_id,
            'docto_referencia': self.folio,
            'cost_center_id': self.cost_center.id,
            'account_id': default_debit_account,
            'debit': amount_debit_debit,
            'credit': amount_debit_credit,
            'currency_id': currency.id if ext_cur else False,
            'amount_currency': amount_currency_debit,
            'period_id': period.id,
            'journal_id': self.bank.journal_id.id,
            'move_id': policy.id,
            'date': self.date,
        }
        line_debit = self.env['account.move.line'].create(line_debit)
        policy.ensure_one()
        policy.post()
        return policy


class DepositFileVoucher(models.Model):
    """Deposit file payment(account.voucher) lines model."""

    # odoo model properties
    _name = 'deposit.file.voucher'
    _rec_name = 'voucher'

    # field selection options
    deposit_file_voucher_state = [
        ('draft', _("Draft")),
        ('normal', _("Normal")),
        ('accepted', _("Accepted")),
        ('cancelled', _("Cancelled"))
    ]

    # initial domain methods

    @api.model
    def folio_domain(self):
        """Browse record object if not loaded already"""
        deposit_file = self.deposit_file
        voucher_ids = []
        if deposit_file._context.get('params', False):
            deposit_file_id = deposit_file._context.get(
                'params', False).get('id', False)
            if not deposit_file and deposit_file_id:
                deposit_file = deposit_file.browse([deposit_file_id])
        if deposit_file.state == 'draft':
            deposit_file.env.cr.execute(
                """
                SELECT
                    DISTINCT
                        dfvw.id as id
                    FROM
                        deposit_file_voucher_view dfvw
                    WHERE
                    dfvw.currency_id = (SELECT
                                rcur.id AS currency_id
                                FROM
                                account_journal aj
                                JOIN res_company rcom
                                ON (rcom.id = aj.company_id)
                                JOIN res_currency rcur
                                ON (rcur.id = COALESCE(aj.currency, rcom.currency_id))
                                WHERE aj.id = CAST(%s AS INT)
                                ORDER BY aj.id ASC)
                    --AND user_id = CAST( AS INT)
                    AND dfvw.voucher_date >=
                    COALESCE(%s, (SELECT
                            MIN(voucher_date)
                            FROM
                            deposit_file_voucher_view)
                            )
                    AND dfvw.voucher_date <=
                    COALESCE(%s, (SELECT
                                MAX(voucher_date)
                                FROM
                                deposit_file_voucher_view)
                                )
                    AND dfvw.cost_center_id = CAST(%s AS INT)
                    AND dfvw.account_apply_id = CAST(%s AS INT)
                    AND (dfvw.advance_id NOT IN (SELECT
                                DISTINCT
                                dfv.advance
                                FROM deposit_file df
                                INNER JOIN deposit_file_voucher dfv
                                ON df.id = dfv.deposit_file
                                INNER JOIN account_account_apply aaa
                                ON df.application = aaa.id
                                WHERE
                                (dfv.advance IS NOT NULL AND aaa.code != 'check' AND dfv.state IN ('accepted')) OR
                                (dfv.advance IS NOT NULL AND aaa.code = 'check' AND dfv.state IN ('accepted', 'cancelled'))
                            )
                            OR dfvw.advance_id IN (SELECT
                                DISTINCT
                                    dfv.advance
                                FROM
                                    deposit_file df
                                INNER JOIN deposit_file_voucher dfv
                                ON dfv.deposit_file = df.id
                                INNER JOIN account_account_apply acc
                                ON acc.id = df.application
                                WHERE (dfv.advance IS NOT NULL AND acc.code IN ('check', 'credit_card')
                                ))
                    OR dfvw.voucher_id NOT IN (SELECT
                                DISTINCT
                                dfv.voucher
                                FROM deposit_file df
                                INNER JOIN deposit_file_voucher dfv
                                ON df.id = dfv.deposit_file
                                INNER JOIN account_account_apply aaa
                                ON df.application = aaa.id
                                WHERE
                                (dfv.voucher IS NOT NULL AND aaa.code != 'check' AND dfv.state IN ('accepted')) OR
                                (dfv.voucher IS NOT NULL AND aaa.code = 'check' AND dfv.state IN ('accepted', 'cancelled'))
                                )
                        )
                    ORDER BY
                        dfvw.id ASC;
                """,
                (
                    deposit_file.journal.id,
                    # deposit_file.env.user.id,
                    deposit_file.start_date or None,
                    deposit_file.end_date or None,
                    deposit_file.cost_center.id,
                    deposit_file.application.id
                )
            )
            if self.env.cr.rowcount:
                voucher_ids = [v[0] for v in self.env.cr.fetchall()]

        return [('id', 'in', voucher_ids)]

    # @api.model
    # def voucher_domain(self):
    #     deposit_file = self.deposit_file
    #     voucher_ids = []
    #     """Browse record object if not loaded already"""
    #     deposit_file_id = False
    #     if deposit_file._context.get('params', False):
    #         deposit_file_id = deposit_file._context.get(
    #             'params', False).get('id', False)
    #         if not deposit_file and deposit_file_id:
    #             deposit_file = deposit_file.browse([deposit_file_id])
    #     """"""""""""""""""""""""""""""""""""""""""""""""
    #     if deposit_file.state == 'draft':
    #         deposit_file.env.cr.execute(
    #             """
    #
    #             WITH voucher_allowed AS (
    #                   WITH journal_currency AS (
    #                     SELECT
    #                         rcur.id AS currency_id
    #                     FROM account_journal aj
    #                     JOIN res_company rcom
    #                         ON (rcom.id = aj.company_id)
    #                     JOIN res_currency rcur
    #                         ON (rcur.id = COALESCE(aj.currency, rcom.currency_id))
    #                     WHERE aj.id = CAST(%s AS int)
    #                     ORDER BY aj.id ASC
    #                 )
    #             SELECT
    #                 DISTINCT dfv.voucher_id as id
    #             FROM deposit_file_voucher_view dfv
    #             JOIN journal_currency jc
    #                 ON (jc.currency_id = dfv.currency_id)
    #             WHERE user_id = CAST(%s AS INT)
    #                 AND dfv.voucher_date >= CAST(
    #                 COALESCE(
    #                     %s,
    #                     (SELECT MIN(voucher_date)
    #                         FROM deposit_file_voucher_view)
    #                     ) AS DATE
    #                 )
    #                 AND dfv.voucher_date <= CAST(
    #                     COALESCE(
    #                         %s,
    #                         (SELECT MAX(voucher_date)
    #                             FROM deposit_file_voucher_view)
    #                         ) AS DATE
    #                     )
    #                 AND dfv.cost_center_id = CAST(%s AS INT)
    #                 AND dfv.account_apply_id = CAST(%s AS INT)
    #                 AND dfv.voucher_id NOT IN (
    #                     SELECT DISTINCT dfv.voucher
    #                     FROM deposit_file_voucher dfv
    #                     JOIN account_voucher av
    #                         ON (av.id = dfv.voucher)
    #                     JOIN account_journal aj
    #                         ON (aj.id = av.journal_id)
    #                     JOIN account_account_apply aaa
    #                         ON (aaa.id = aj.account_account_apply_id)
    #                     WHERE dfv.state NOT IN ('draft', 'cancelled')
    #                         AND aaa.code != 'check'
    #                     ORDER BY dfv.voucher ASC
    #                 )
    #             ORDER BY dfv.voucher_id ASC)
    #             SELECT * FROM voucher_allowed where id not in (
    #                 select
    #                   --df.id, df.folio, df.state, aaa.code , dfv.state,
    #                   dfv.voucher
    #                 from deposit_file df INNER JOIN deposit_file_voucher dfv
    #                   on df.id = dfv.deposit_file
    #                 INNER JOIN account_account_apply aaa on df.application = aaa.id
    #                 where (aaa.code != 'check' and dfv.state='accepted') or
    #                       (aaa.code ='check' and dfv.state in ('accepted', 'cancelled'))
    #             )
    #             ;
    #             """,
    #             (
    #                 deposit_file.journal.id,
    #                 deposit_file.env.user.id,
    #                 deposit_file.start_date or None,
    #                 deposit_file.end_date or None,
    #                 deposit_file.cost_center.id,
    #                 deposit_file.application.id
    #             )
    #         )
    #         if self.env.cr.rowcount:
    #             voucher_ids = [v[0] for v in self.env.cr.fetchall()]
    #
    #     return [('id', 'in', voucher_ids)]

    # fields
    deposit_file = fields.Many2one(
        comodel_name='deposit.file',
        copy=False,
        ondelete='cascade',
        help=_("Deposit file"),
        readonly=True,
        required=True,
        string=_("Deposit file"),
    )
    folio = fields.Many2one(
        comodel_name='deposit.file.voucher.view',
        copy=False,
        domain=folio_domain,
        store=True,
        help=_("Deposit file advance"),
        required=False,
        string=_("Folio"),
        states={
            'draft': [('required', True)]
        }
    )
    voucher = fields.Many2one(
        comodel_name='account.voucher',
        copy=False,
        # domain=voucher_domain,
        help=_("Deposit file voucher"),
        # readonly=True,
        required=False,
        string=_("Account voucher"),
        states={
            'normal': [('readonly', True)],
            'accepted': [('readonly', True)],
            'cancelled': [('readonly', True)]
        },
    )
    advance = fields.Many2one(
        comodel_name='advance.payment',
        copy=False,
        help=_("Deposit file advance"),
        required=False,
        string=_("Advance"),
        states={
            'normal': [('readonly', True)],
            'accepted': [('readonly', True)],
            'cancelled': [('readonly', True)]
        },
    )
    currency = fields.Many2one(
        comodel_name='res.currency',
        copy=False,
        help=_("Deposit file voucher currency"),
        readonly=True,
        string=_("Currency"),
    )
    amount = fields.Float(
        copy=False,
        help=_("Voucher amount"),
        readonly=True,
        string=_("Amount"),
    )
    state = fields.Selection(
        copy=False,
        default='draft',
        help=_("Deposit file voucher state"),
        readonly=True,
        selection=deposit_file_voucher_state,
        string=_("State"),
    )
    cancel = fields.Boolean(
        default=False,
        help=_("Payment rejected"),
        string=_("Cancel"),
        states={
            'cancelled': [('readonly', True)]
        }
    )

    used_vouchers = set()
    used_advances = set()

    # onchange methods
    # @api.onchange('voucher')
    # def onchange_voucher(self):
    #     voucher = self.voucher
    #     if voucher:
    #         # user interface info (saved on create/write overriden methods)
    #         self.currency = voucher.currency
    #         self.amount = voucher.doc_amount
    #
    #         # update list domain taking out this voucher ids
    #         self.used_vouchers.add(voucher.id)
    #     res = {}
    #     # Take out used_vouchers [0][2] -> ids list
    #     voucher_ids = set(self.voucher_domain()[0][2])
    #     # voucher_ids = voucher_ids - self.used_vouchers
    #     res['domain'] = {
    #         'voucher': [('id', 'in', list(voucher_ids))]
    #     }
    #     return res

    @api.onchange('folio')
    def onchange_folio(self):
        if self.folio:
            if self.folio.voucher_id and not self.folio.advance_id:
                self.currency = self.folio.voucher_id.currency
                if self.folio.voucher_id.received_third_check_ids:
                    self.amount = self.folio.voucher_id.checks_amount
                else:
                    self.amount = self.folio.voucher_id.doc_amount
                self.voucher = self.folio.voucher_id.id
                self.advance = False
                self.used_vouchers.add(self.folio.voucher_id.id)
            if not self.folio.voucher_id and self.folio.advance_id:
                self.currency = self.folio.advance_id.currency_id
                self.amount = self.folio.advance_id.amount
                self.advance = self.folio.advance_id.id
                self.voucher = False
                self.used_advances.add(self.folio.advance_id.id)
        res = {}
        # Take out used_vouchers [0][2] -> ids list
        folio_ids = set(self.folio_domain()[0][2])
        res['domain'] = {
            'folio': [('id', 'in', list(folio_ids))]
        }
        return res

    @api.model
    def create(self, vals):
        """Override original create method"""
        if vals.get('advance', 0) and not vals.get('voucher', 0):
            advance = self.env['advance.payment'].browse(vals.get('advance', 0))
            if advance:
                vals['currency'] = advance.currency_id.id
                vals['amount'] = advance.amount
        if vals.get('voucher', 0) and not vals.get('advance', 0):
            voucher = self.env['account.voucher'].browse(vals.get('voucher', 0))
            if voucher:
                vals['currency'] = voucher.currency.id
                if voucher.received_third_check_ids:
                    vals['amount'] = voucher.checks_amount
                else:
                    vals['amount'] = voucher.doc_amount
        # voucher = self.env['account.voucher'].browse(vals.get('voucher', 0))
        # if voucher:
        #     vals['currency'] = voucher.currency.id
        #     vals['amount'] = voucher.doc_amount
        return super(DepositFileVoucher, self).create(vals)

    @api.one
    def write(self, vals):
        """Override original write method"""
        voucher = self.env['account.voucher'].browse(vals.get('voucher', 0))
        if voucher:
            vals['currency'] = voucher.currency.id
            vals['amount'] = voucher.doc_amount
            self.env.cr.execute(
                """
                SELECT voucher
                FROM deposit_file_voucher dfv
                WHERE voucher = CAST(%s AS INT)
                    AND state IN ('normal', 'accepted')
                LIMIT 1;
                """,
                (voucher.id,)
            )
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _("Payment already used on deposit file.")
                )
        # Change to cancelled if cancel checkbox is marked
        if vals.get('state', False) == 'accepted':
            if self.voucher and not self.advance:
                check = self.env['account.check'].search(
                    [('voucher_id', '=', self.voucher.id)]
                )
            if not self.voucher and self.advance:
                check = self.env['account.check'].search(
                    [('advance_payment', '=', self.advance.id)]
                )
            # if check:
            for chk in check:
                if chk.state == 'holding':
                    check_action = self.env['account.check.action'].create(
                        {
                            'company_id': chk.company_id.id,
                            'action_type': 'deposit'
                        }
                    )
                    check_action.action_confirm()
                    chk.action_deposit()

        if vals.get('cancel', False):
            vals['state'] = 'cancelled'
            if self.voucher and not self.advance:
                check = self.env['account.check'].search(
                    [('voucher_id', '=', self.voucher.id)]
                )
            if not self.voucher and self.advance:
                check = self.env['account.check'].search(
                    [('advance_payment', '=', self.advance.id)]
                )
            # if check:
            for chk in check:
                check_action = self.env['account.check.action'].create(
                    {
                        'company_id': chk.company_id.id,
                        'action_type': 'return'
                    }
                )
                check_action.action_confirm()
                chk.action_return()
        return super(DepositFileVoucher, self).write(vals)
