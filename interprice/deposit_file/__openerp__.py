# -*- coding: utf-8 -*-
{
    'name': """Deposit file""",
    'summary': """Deposit files of cash, cards and checks""",
    'category': "Accounting",
    'version': "1.0.0",
    'application': True,

    'author': "Copyright © 2016 TO-DO - All Rights Reserved",
    'website': "http://www.grupovadeto.com",

    'depends': ['base',
                'account',
                'account_check',
                'account_cost_center',
                'centro_de_costos',
                'cotizaciones',
                'piramide_de_pago',
                'client_advance_payment',
                'price_list',
                'l10n_mx_facturae',
                'cfdi33'],
    'data': [
        'views/deposit_file.xml',
        'views/deposit_file_sequence.xml',
        'views/deposit_file_workflow.xml',
        'reports/views/deposit_files_report.xml',
        'reports/views/deposit_file_report_pdf.xml',
        'reports/views/deposit_files_report_pdf.xml',
        'reports/wizard/views/deposit_files_report_wizard.xml'
    ]
}
