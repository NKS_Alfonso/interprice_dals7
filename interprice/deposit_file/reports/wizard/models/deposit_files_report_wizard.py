# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import _, api, exceptions, fields, models


class DepositFilesReportWizard(models.TransientModel):
    """This wizard is meant to filter the results for the
    Payment reception report (Show info about client payments).
    """
    _name = 'deposit_files_report_wizard'

    # field selection options
    deposit_file_state = [
        ('draft', _("Draft")),
        ('normal', _("Normal")),
        ('accepted', _("Accepted")),
        ('partial_cancelled', _("Partially cancelled")),
        ('cancelled', _("Cancelled"))
    ]

    # domain methods
    @api.model
    def bank_domain(self):
        """Returns all bank ids where its journals are used on payments.
        'deposit_file_voucher_view' has all journals used on payments
        as well as journals used on 'piramide_de_pago' and journals with
        applications of cash, bank or check.
        """
        bank_ids = []
        self.env.cr.execute(
            """
            SELECT DISTINCT dfrv.bank_id
            FROM deposit_files_report_view dfrv
            --WHERE dfrv.user_id = CAST(%s AS INT)
            ORDER BY dfrv.bank_id ASC;
            """
            # ,
            # (self.env.user.id,)
        )
        if self.env.cr.rowcount:
            bank_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', bank_ids)]

    @api.model
    def journal_domain(self):
        """Returns all journal ids filtered by currency of the bank journal.
        """
        journal_ids = []
        self.env.cr.execute(
            """
            SELECT
                DISTINCT dfrv.journal_id
            FROM deposit_files_report_view dfrv
            --WHERE dfrv.user_id = CAST(%s AS INT)
            ORDER BY dfrv.journal_id ASC;
            """
            # ,
            # (self.env.user.id,)
        )
        if self.env.cr.rowcount:
            journal_ids = [j[0] for j in self.env.cr.fetchall()]
        return [('id', 'in', journal_ids)]

    @api.model
    def cost_center_domain(self):
        cost_center_ids = []
        self.env.cr.execute(
            """
            SELECT
                DISTINCT dfrv.cost_center_id
            FROM deposit_files_report_view dfrv
            --WHERE dfrv.user_id = CAST(%s AS INT)
            ORDER BY dfrv.cost_center_id ASC;
            """
            # ,
            # (self.env.user.id,)
        )
        if self.env.cr.rowcount:
            cost_center_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', cost_center_ids)]

    @api.model
    def application_domain(self):
        application_ids = []
        self.env.cr.execute(
            """
            SELECT
                DISTINCT dfrv.application_id
            FROM deposit_files_report_view dfrv
            --WHERE dfrv.user_id = CAST(%s AS INT)
            ORDER BY dfrv.application_id ASC;
            """
            # ,
            # (self.env.user.id,)
        )
        if self.env.cr.rowcount:
            application_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', application_ids)]

    # Fields
    bank = fields.Many2one(
        comodel_name='res.partner.bank',
        domain=bank_domain,
        help=_("Deposit file bank"),
        string=_("Bank")
    )
    start_date = fields.Date(
        help=_("Initial date to filter deposit files"),
        string=_("Initial date")
    )
    end_date = fields.Date(
        help=_("Final date to filter deposit files"),
        string=_("Final date")
    )
    journal = fields.Many2one(
        comodel_name='account.journal',
        domain=journal_domain,
        help=_("Deposit file policy journals"),
        string=_("Journal")
    )
    cost_center = fields.Many2one(
        comodel_name='account.cost.center',
        domain=cost_center_domain,
        help=_("Deposit file cost centers"),
        string=_("Cost center")
    )
    application = fields.Many2one(
        comodel_name='account.account.apply',
        domain=application_domain,
        help=_("Deposit file account application"),
        string=_("Application"),
    )
    state = fields.Selection(
        help=_("Deposit file state"),
        selection=deposit_file_state,
        string=_("state"),
    )

    @api.multi
    def screen(self):
        ids = self._get_ids()
        return {
            'name': _('Deposit files report'),
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': 'deposit.files.report.view',
            'target': 'current',
            'context': {},
            'domain': [('id', 'in', ids)]
        }

    @api.multi
    def show_pdf(self):
        return self.env['report'].get_action(
            self.env['deposit.files.report.view'].browse(self._get_ids()),
            'deposit_file.deposit_files_report_pdf'
        )

    @api.multi
    def show_xls(self):
        ids = self._get_ids()
        csv_id = self.csv_by_ids(ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        df = self.start_date
        dt = self.end_date

        if df and dt and df > dt:
            self.start_date = False
            self.end_date = False
            raise exceptions.Warning(
                "'Date to' must be higher than 'Date from'.")

    def _get_ids(self):
        # Get filter values
        bank = self.bank
        s_date = self.start_date
        e_date = self.end_date
        journal = self.journal
        cost_center = self.cost_center
        application = self.application
        state = self.state

        query = "SELECT DISTINCT dfrv.id\n"\
                "FROM deposit_files_report_view dfrv\n"\
                "WHERE dfrv.id IS NOT NULL\n"

        if bank:
            query += "\tAND dfrv.bank_id = CAST(% s AS INT)\n" % (bank.id)
        if s_date:
            query += "\tAND dfrv.date >= CAST('%s' AS date)\n" % (s_date)
        if e_date:
            query += "\tAND dfrv.date <= CAST('%s' AS date)\n" % (e_date)
        if journal:
            query += "\tAND dfrv.journal_id = CAST(%s AS INT)\n" % (
                journal.id)
        if cost_center:
            query += "\tAND dfrv.cost_center_id = CAST(%s AS INT)\n" % (
                cost_center.id)
        if application:
            query += "\tAND dfrv.application_id = CAST(%s AS INT)\n" % (
                application.id)
        if state:
            query += "\tAND dfrv.filter_state = CAST('%s' AS TEXT)\n" % (state)

        query += "\tORDER BY dfrv.id DESC;"

        self.env.cr.execute(query)

        if self.env.cr.rowcount:
            ids = [id[0] for id in self.env.cr.fetchall()]
        else:
            raise exceptions.Warning(_("No records found!"))

        return ids

    def csv_by_ids(self, _ids):
        data = self.env['deposit.files.report.view'].browse(_ids)

        data_csv = [
            [
                _('Folio'),
                _('Reference folio'),
                _('Amount'),
                _('Bank'),
                _('Creation date'),
                _('Creation user'),
                _('Application'),
                _('State'),
                _('Acceptation date'),
                _('Acceptation user'),
                _('Partial cancellation date'),
                _('Partial cancellation user'),
                _('Cancellation date'),
                _('Cancellation user'),
                _('Observations')
            ]
        ]

        for row in data:
            data_csv.append([
                unicode(self._set_default(row.folio)).encode('utf8'),
                unicode(self._set_default(
                    row.deposit_file_folio)).encode('utf8'),
                unicode(self._set_default(row.amount, 0)).encode('utf8'),
                unicode(self._set_default(row.bank_name)).encode('utf8'),
                unicode(self._set_default(row.create_date)).encode('utf8'),
                unicode(self._set_default(row.create_user)).encode('utf8'),
                unicode(self._set_default(row.application_name)).encode('utf8'),
                unicode(self._set_default(row.state)).encode('utf8'),
                unicode(self._set_default(row.accepted_date)).encode('utf8'),
                unicode(self._set_default(row.accepted_user)).encode('utf8'),
                unicode(self._set_default(row.partial_cancelled_date)).encode('utf8'),
                unicode(self._set_default(row.partial_cancelled_user)).encode('utf8'),
                unicode(self._set_default(row.cancelled_date)).encode('utf8'),
                unicode(self._set_default(row.cancelled_user)).encode('utf8'),
                unicode(self._set_default(row.observations)).encode('utf8')
            ])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'DepositFilesReport.csv',
                'type_file': 'csv',
                'file': file,
            })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default

    @api.onchange('bank')
    def onchange_bank(self):
        """Changes the domain of the journal field with the journal ids
        that has the same currency as the bank's journal currency,
        and only journals used on payments and inside a 'piramide de pago'.
        """
        res = {}
        journal_ids = []
        if self.bank:
            self.env.cr.execute(
                """
                 WITH bank_journal_currency AS (
                    SELECT
                        DISTINCT rcur.id AS currency_id
                    FROM res_partner_bank rpb
                    JOIN res_company rcom
                        ON (rcom.id = rpb.company_id)
                    JOIN res_currency rcur
                        ON (rcur.id = COALESCE(rpb.currency2_id, rcom.currency_id))
                    WHERE rpb.id = CAST(%s AS INT)
                )
                SELECT
                    DISTINCT dfrv.journal_id
                FROM deposit_files_report_view dfrv
                JOIN bank_journal_currency bjc
                    ON (bjc.currency_id = dfrv.currency_id)
                --WHERE dfrv.user_id = CAST( AS INT)
                ORDER BY dfrv.journal_id ASC;
                """,
                (
                    self.bank.id,
                    # self.env.user.id,
                )
            )
            if self.env.cr.rowcount:
                journal_ids = [j[0] for j in self.env.cr.fetchall()]
            res['domain'] = {
                'journal': [('id', 'in', journal_ids)],
            }
            self.journal = False  # Clean journal field
        return res
