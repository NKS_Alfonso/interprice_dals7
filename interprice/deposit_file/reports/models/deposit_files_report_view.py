# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, tools


class DepositFileReportView(models.Model):
    """Cash back report by date / bank Wizard"""

    # odoo model properties
    _name = 'deposit.files.report.view'
    _table = 'deposit_files_report_view'
    _description = 'Deposit files report'
    _auto = False  # Don't let odoo create table
    _order = 'id DESC'

    # View fieds
    bank_id = fields.Many2one(
        comodel_name='res.partner.bank',
        help=_("Deposit file bank"),
        readonly=True,
        string=_("Bank"),
    )
    create_date = fields.Date(
        help=_("Deposit file create date"),
        readonly=True,
        string=_("Create date"),
    )
    create_user = fields.Char(
        help=_("Deposit file create user"),
        readonly=True,
        string=_("Create user"),
    )
    normal_date = fields.Date(
        help=_("Deposit file normal date"),
        readonly=True,
        string=_("Normal date"),
    )
    normal_user = fields.Char(
        help=_("Deposit file normal user"),
        readonly=True,
        string=_("Normal user"),
    )
    accepted_date = fields.Date(
        help=_("Deposit file accepted date"),
        readonly=True,
        string=_("Accepted date"),
    )
    accepted_user = fields.Char(
        help=_("Deposit file accepted user"),
        readonly=True,
        string=_("Accepted user"),
    )
    partial_cancelled_date = fields.Date(
        help=_("Deposit file partial_cancelled date"),
        readonly=True,
        string=_("Partial cancelled date"),
    )
    partial_cancelled_user = fields.Char(
        help=_("Deposit file partial_cancelled user"),
        readonly=True,
        string=_("Partial cancelled user"),
    )
    cancelled_date = fields.Date(
        help=_("Deposit file cancelled date"),
        readonly=True,
        string=_("Cancelled date"),
    )
    cancelled_user = fields.Char(
        help=_("Deposit file cancelled user"),
        readonly=True,
        string=_("Cancelled user"),
    )
    journal_id = fields.Many2one(
        comodel_name='account.journal',
        help=_("Same bank currency journal"),
        readonly=True,
        string=_("Payment journal"),
    )
    cost_center_id = fields.Many2one(
        comodel_name='account.cost.center',
        help=_("Cost centers allowed by user"),
        readonly=True,
        string=_("Cost center"),
    )
    application_id = fields.Many2one(
        comodel_name='account.account.apply',
        help=_("Account application"),
        readonly=True,
        string=_("Application"),
    )
    folio = fields.Char(
        help=_("Deposit file folio"),
        readonly=True,
        string=_("Folio"),
    )
    deposit_file_folio = fields.Char(
        help=_("User deposit file folio"),
        readonly=True,
        string=_("Folio"),
    )
    amount = fields.Float(
        help=_("Voucher amount"),
        readonly=True,
        string=_("Amount"),
    )
    bank_name = fields.Char(
        help=_("Deposit file bank name"),
        readonly=True,
        string=_("Bank name"),
    )
    create_user = fields.Char(
        help=_("Deposit file creation user"),
        readonly=True,
        string=_("Create user"),
    )
    application_name = fields.Char(
        help=_("Deposit file application"),
        readonly=True,
        string=_("Application name"),
    )
    state = fields.Char(
        help=_("Deposit file state"),
        readonly=True,
        string=_("State"),
    )
    observations = fields.Text(
        help=_("Deposit file observations"),
        readonly=True,
        string=_("Observations")
    )

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'deposit_files_report_view')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW deposit_files_report_view
                AS(
                    SELECT
                        df.id,
                        df.folio,
                        df.deposit_file_folio,
                        ROUND(CAST(SUM(dfv.amount) AS NUMERIC), 4) AS amount,
                        CONCAT_WS(
                            ': ',
                            CAST(rpb.bank_name AS TEXT),
                            CAST(rpb.acc_number AS TEXT)
                        ) AS bank_name,
                        df.bank AS bank_id,
                        df.journal AS journal_id,
                        df.cost_center AS cost_center_id,
                        df.application AS application_id,
                        df.date AS create_date,
                        rp_create.name AS create_user,
                        df.normal_date,
                        rp_normal.name AS normal_user,
                        df.accepted_date,
                        rp_accepted.name AS accepted_user,
                        df.partial_cancelled_date,
                        rp_partial_cancelled.name AS partial_cancelled_user,
                        df.cancelled_date,
                        rp_cancelled.name AS cancelled_user,
                        CASE WHEN aaa.code = 'cash'
                                THEN 'Efectivo'
                            WHEN aaa.code = 'bank'
                                THEN 'Tarjeta'
                            WHEN aaa.code = 'check'
                                THEN 'Cheque'
                            END AS application_name,
                        CASE WHEN df.state = 'draft'
                                THEN 'Borrador'
                            WHEN df.state = 'normal'
                                THEN 'Normal'
                            WHEN df.state = 'accepted'
                                THEN 'Aceptado'
                            WHEN df.state = 'partial_cancelled'
                                THEN 'Cancelación parcial'
                            WHEN df.state = 'cancelled'
                                THEN 'Cancelado'
                            END AS state,
                        df.observations,
                        df.state AS filter_state,
                        dfvv.currency_id,
                        dfvv.user_id
                    FROM deposit_file df
                    JOIN res_partner_bank rpb
                        ON (rpb.id = df.bank)
                    JOIN deposit_file_voucher dfv
                        ON (dfv.deposit_file = df.id)
                    LEFT JOIN deposit_file_voucher_view dfvv
                      ON CASE WHEN dfv.voucher IS NOT NULL
                      THEN
                        dfv.voucher = dfvv.voucher_id --and dfvv.cost_center_id=df.cost_center
                         WHEN dfv.advance IS NOT NULL THEN
                           dfv.advance = dfvv.advance_id --and dfvv.cost_center_id=df.cost_center
                         END
                    JOIN account_account_apply aaa
                        ON (aaa.id = df.application)
                    JOIN res_users ru_create
                        ON (ru_create.id = df.create_uid)
                    JOIN res_partner rp_create
                        ON (rp_create.id = ru_create.partner_id)
                    LEFT JOIN res_users ru_normal
                        ON (ru_normal.id = df.normal_user)
                    LEFT JOIN res_partner rp_normal
                        ON (rp_normal.id = ru_normal.partner_id)
                    LEFT JOIN res_users ru_accepted
                        ON (ru_accepted.id = df.accepted_user)
                    LEFT JOIN res_partner rp_accepted
                        ON (rp_accepted.id = ru_accepted.partner_id)
                    LEFT JOIN res_users ru_partial_cancelled
                        ON (ru_partial_cancelled.id = df.partial_cancelled_user)
                    LEFT JOIN res_partner rp_partial_cancelled
                        ON (rp_partial_cancelled.id = ru_partial_cancelled.partner_id)
                    LEFT JOIN res_users ru_cancelled
                        ON (ru_cancelled.id = df.cancelled_user)
                    LEFT JOIN res_partner rp_cancelled
                        ON (rp_cancelled.id = ru_cancelled.partner_id)
                    GROUP BY df.id,
                            rpb.id,
                            dfvv.user_id,
                            dfvv.currency_id,
                            rp_create.id,
                            rp_normal.id,
                            rp_accepted.id,
                            rp_partial_cancelled.id,
                            rp_cancelled.id,
                            aaa.id
                    ORDER BY df.id DESC
                )
            """
        )
