# -*- encoding: utf-8 -*-
{
        "name": "Partner Risk Analysis",
        "version": "0.1",
        "description": """This module adds a new button in the partner form to
        analyze current state of a partner risk.
        It reports current information regarding amount of debt in invoices,
        orders, etc.
        It also modifies the workflow of sale orders by adding a new step when
        partner's risk is exceeded.

Developed for Trod y Avia, S.L.""",
        "author": "NaN·tic",
        "website": "http://www.NaN-tic.com",
        "depends": ['base',
                    'account',
                    'sale',
                    'sale_stock',
                    'account_payment',
                    'partner_risk_insurance',
                    'alternate_type_of_change',
                    'price_list'
                    ],
        "category": "Custom Modules",
        "data": [
            'wizard/open_risk_window_view.xml',
            'risk_view.xml',
            'sale_view.xml',
            'security/nan_partner_risk_groups.xml',
            'security/ir.model.access.csv',
            'sale_workflow.xml',
            'wizard/credit_extension_wizard_view.xml',
            'wizard/credit_extension_report_wizard_view.xml',
            'reports/credit_extension_line_report.xml',
        ],
        "active": False,
        "installable": True
}
