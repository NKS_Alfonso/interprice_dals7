# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models
from time import gmtime, strftime


class CreditExtensionWizard(models.TransientModel):
    _name = 'credit.extension.wizard'
    partner_id = fields.Many2one('res.partner', 'Client', readonly=True)
    amount = fields.Float('Extension of Credit', digits=(10, 2))
    credit_limit = fields.Float('Credit Limit', digits=(10, 2), readonly=True)
    payment_method = fields.Char('Payment method', size=32, readonly=True)

    @api.one
    def extend_credit(self):
        extension_lines = self.env['credit.extension.line']
        amount = self.amount
        balance_extension = self.partner_id.balance_extension
        credit_limit = self.credit_limit
        date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        lines = extension_lines.search([(
                'partner_id.id', '=', self.partner_id.id)],
                limit=1, order='date_line desc'
            )
        if not lines:
            extension_lines.create({
                'partner_id': self.partner_id.id,
                'date_line': date,
                'amount': amount,
            })
            self.partner_id.write({'balance_extension': amount})
        else:
            if amount < balance_extension:
                negative_amount = amount - balance_extension
                extension_lines.create({
                    'partner_id': self.partner_id.id,
                    'date_line': date,
                    'amount': negative_amount,
                })
                self.partner_id.write({'balance_extension': amount})
            else:
                extension_lines.create({
                    'partner_id': self.partner_id.id,
                    'date_line': date,
                    'amount': amount - balance_extension,
                })
                self.partner_id.write({
                    'balance_extension': amount - balance_extension
                })

        return {}
