# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models, _
from time import strftime, gmtime


class CreditExtensionReportWizard(models.TransientModel):
    _name = 'credit.extension.report.wizard'
    start_date = fields.Date(
        'Start date',
        required=True,
        default=lambda self: strftime('%Y-%m-%d', gmtime()),
    )
    end_date = fields.Date(
        'End date',
        required=True,
        default=lambda self: strftime('%Y-%m-%d', gmtime()),
    )
    partner_id = fields.Many2one('res.partner', 'Client')

    @api.multi
    def print_extension_lines_report(self):
        report_obj = self.env['credit.extension.line'].search([
            ('partner_id.id', '=', self.partner_id.id),
            ('date_line', '>=', self.start_date),
            ('date_line', '<=', self.end_date)
        ]).ids
        if report_obj:
            return self.env['report'].get_action(
                self.env['credit.extension.line'].browse(report_obj),
                'nan_partner_risk.nan_partner_risk_lines_report'
            )
        else:
            raise exceptions.Warning(_('No records found'))
