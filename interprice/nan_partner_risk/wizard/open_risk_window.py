# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009 Albert Cervera i Areny (http://www.nan-tic.com). All Rights Reserved
#    Copyright (c) 2011 Pexego Sistemas Informáticos. All Rights Reserved
#                       Alberto Luengo Cabanillas <alberto@pexego.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


""" Open Risk Window and show Partner relative information """

from openerp.osv import osv, fields
from openerp.tools.translate import _


class open_risk_window(osv.osv_memory):
    """ Open Risk Window and show Partner relative information """

    _name = "open.risk.window"
    _description = "Partner's risk information"

    def _get_account_invoice(self, cr, uid, context=None):
        invoices = []
        for partner in self.pool.get('res.partner').browse(
                cr, uid, context['active_id'], context):
            account_invoice_list = self.pool.get('account.invoice').search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', 'in', ('open', 'draft')),
                    ('type', '=', 'out_invoice')
                ], order='number asc')
            if account_invoice_list:
                for i in self.pool.get('account.invoice').browse(
                        cr, uid, account_invoice_list, context):
                    amount_total = 0.0
                    balance = 0.0
                    if i.state == 'open':
                        if i.currency_id.name == i.company_id.currency_id.name:
                            amount_total += i.amount_total
                            balance += i.residual
                        elif i.currency_id.name != i.company_id.currency_id.name:
                            amount_total += i.amount_total * i.rate
                            balance += i.residual * i.rate
                    elif i.state == 'draft':
                        if i.picking_dev_id:
                            if i.currency_id.name == i.company_id.currency_id.name:
                                amount_total += i.amount_total
                                balance += i.amount_total
                            elif i.currency_id.name != i.company_id.currency_id.name:
                                from_currency_id = i.currency_id.id
                                to_currency_id = i.company_id.currency_id.id
                                currency_obj = self.pool.get('res.currency')
                                amount_total += currency_obj.compute(
                                    cr, uid, from_currency_id, to_currency_id,
                                    i.amount_total, round=True, context=context)
                                balance += currency_obj.compute(
                                    cr, uid, from_currency_id, to_currency_id,
                                    i.amount_total, round=True, context=context)
                    if amount_total or balance:
                        invoices.append({'number': i.number if i.number else _('draft'),
                                         'amount_total': amount_total,
                                         'residual': balance})
        return invoices

    def _get_sale_order(self, cr, uid, context=None):
        orders = []
        for partner in self.pool.get('res.partner').browse(
                cr, uid, context['active_id'], context):
            sale_orders = self.pool.get('sale.order').search(
                cr, uid, [
                    ('partner_id', 'child_of', [partner.id]),
                    ('state', 'not in', [
                        'draft', 'cancel', 'wait_risk', 'invoice_except'
                    ]
                     )
                ], context=context)
            if sale_orders:
                for o in self.pool.get('sale.order').browse(
                        cr, uid, sale_orders, context):
                    cr.execute("""
                                SELECT ai.id FROM account_invoice AS ai
                                INNER JOIN stock_picking AS sp ON ai.origin=sp.name
                                INNER JOIN sale_order AS so ON sp.origin=so.name
                                WHERE so.id=%s""", (o.id,))
                    if not cr.rowcount:
                        amount_total = 0.0
                        if o.currency.id == o.company_id.currency_id.id:
                            amount_total += o.amount_total
                        elif o.currency.id != o.company_id.currency_id.id:
                            from_currency_id = o.currency.id
                            to_currency_id = o.company_id.currency_id.id
                            currency_obj = self.pool.get('res.currency')
                            amount_total = currency_obj.compute(
                                cr, uid, from_currency_id, to_currency_id,
                                o.amount_total, round=True, context=context
                            )
                        orders.append({'name': o.name,
                                       'amount_total': amount_total})
        return orders

    _columns = {
        'unpayed_amount': fields.float('Expired Unpaid Payments', digits=(4,2), readonly="True"),
        'pending_amount': fields.float('Unexpired Unpaid Payments', digits=(4,2), readonly="True"),
        'draft_invoices_amount': fields.float('Draft Invoices', digits=(4,2), readonly="True"),
        'pending_orders_amount': fields.float('Uninvoiced Orders', digits=(4,2), readonly="True"),
        'total_debt': fields.float('Debt', digits=(4,2), readonly="True"),
        'credit_note': fields.float('Credit Notes', digits=(4, 2), readonly="True"),
        'unidentified_deposits': fields.float('Unidentified deposits', digits=(4, 2), readonly="True"),
        'advance_payment': fields.float('Advance payment', digits=(4, 2), readonly="True"),
        'total_positive_balance': fields.float('Positive balance', digits=(4, 2), readonly="True"),
        'total_balance': fields.float('Total Debt', digits=(4, 2), readonly="True"),
        'credit_limit': fields.float('Credit Limit', digits=(4,2), readonly="True"),
        'available_risk': fields.float('Available Credit', digits=(4,2), readonly="True"),
        'total_risk_percent': fields.float('Credit Usage (%)', digits=(4,2), readonly="True"),
        'account_invoice': fields.one2many('account.invoice', 'partner_id', string='Invoices', readonly="True"),
        'sale_order': fields.one2many('sale.order', 'partner_id', string='Sale Orders', readonly="True"),
        'balance_extension': fields.float(
                'Extension balance',
                digits=(10, 2),
                readonly=True,
            )
        }
    _defaults = {
        'unpayed_amount': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).unpayed_amount or 0.0,
        'pending_amount': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).pending_amount or 0.0,
        'draft_invoices_amount': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).draft_invoices_amount or 0.0,
        'pending_orders_amount': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).pending_orders_amount or 0.0,
        'total_debt': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).total_debt or 0.0,
        'credit_note': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr, uid, context['active_id'],context).credit_note or 0.0,
        'unidentified_deposits': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr, uid, context['active_id'],context).unidentified_deposits or 0.0,
        'advance_payment': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr, uid, context['active_id'], context).advance_payment or 0.0,
        'total_positive_balance': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr, uid, context['active_id'], context).total_positive_balance or 0.0,
        'total_balance': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr, uid, context['active_id'], context).total_balance or 0.0,
        'credit_limit': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).credit_limit or 0.0,
        'available_risk': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).available_risk or 0.0,
        'total_risk_percent': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).total_risk_percent or 0.0,
        'balance_extension': lambda self, cr, uid, context: self.pool.get('res.partner').browse(cr,uid,context['active_id'],context).balance_extension or 0.0,
        'account_invoice': _get_account_invoice,
        'sale_order': _get_sale_order,
    }


open_risk_window()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
