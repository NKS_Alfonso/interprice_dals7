# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009 Albert Cervera i Areny (http://www.nan-tic.com). All Rights Reserved
#    Copyright (c) 2011 Pexego Sistemas Informáticos. All Rights Reserved
#                       Alberto Luengo Cabanillas <alberto@pexego.es>
#    Copyright (c) 2015 Comunitea Servicios Tecnológicos. All Rights Reserved
#                       Omar Castiñeira Saavedra <omar@comunitea.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
import time
from openerp.tools.translate import _
from datetime import datetime
from dateutil import tz


class sale_order_line(osv.osv):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    def _amount_invoiced(self, cr, uid, ids, field_name, arg, context):
        result = {}
        for line in self.browse(cr, uid, ids, context):
            # Calculate invoiced amount with taxes included.
            # Note that if a line is only partially invoiced we consider
            # the invoiced amount 0.
            # The problem is we can't easily know if the user changed amounts
            # once the invoice was created
            if line.invoiced:
                result[line.id] = line.price_subtotal + self._tax_amount(cr, uid, line)
            else:
                result[line.id] = 0.0
        return result

    def _tax_amount(self, cr, uid, line):
        val = 0.0
        for c in self.pool.get('account.tax').compute_all(cr, uid, line.tax_id, line.price_unit * (1-(line.discount or 0.0)/100.0), line.product_uos_qty, line.order_id.partner_invoice_id.id, line.product_id, line.order_id.partner_id)['taxes']:
            val += c['amount']
        return val

    _columns = {
        'amount_invoiced': fields.function(_amount_invoiced, method=True, string='Invoiced Amount', type='float'),
    }
sale_order_line()


class sale_order(osv.osv):
    _inherit = 'sale.order'

    # Inherited onchange function
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        if context is None:
            context=context
        result = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)
        if part:
            partner = self.pool.get('res.partner').browse(cr, uid, part).commercial_partner_id
            if partner.credit_limit and partner.available_risk < 0.0:
                result['warning'] = {
                    'title': _('Credit Limit Exceeded'),
                    'message': _('''Warning: Credit Limit Exceeded.\n\nThis
                    partner has a credit limit of %(limit).2f and already has
                    a debt of %(debt).2f.''') % {
                        'limit': partner.credit_limit,
                        'debt': partner.total_debt,
                    }
                }
        return result

    def _amount_invoiced(self, cr, uid, ids, field_name, arg, context):
        result = {}
        for order in self.browse(cr, uid, ids, context):
            if order.invoiced:
                amount = order.amount_total
            else:
                amount = 0.0
                for line in order.order_line:
                    amount += line.amount_invoiced
            result[order.id] = amount
        return result

    def amount_risk_total(self, cr, uid, ids, context=None):
        order = self.browse(cr, uid, ids, context)
        currency_pool = self.pool.get('res.currency')
        amount_total = order.amount_total
        if order.currency != order.company_id.currency_id:
            context = dict(context)
            context.update({'todo_ttype': 'receipt'})
            amount_total = currency_pool.compute(
                cr, uid, order.currency.id, order.company_id.currency_id.id,
                order.amount_total, round=True, context=context
            )
        if not order.partner_id.credit_limit or order.partner_id.available_risk - amount_total >= 0.0:
            return False
        if order.partner_id.credit_limit and order.partner_id.available_risk - amount_total < 0.0:
            return True

    _columns = {
        'amount_invoiced': fields.function(_amount_invoiced, method=True, string='Invoiced Amount', type='float'),
        'state': fields.selection([
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ('wait_risk', 'Waiting Risk Approval'),
            ], 'Order State', readonly=True, help=_("Gives the state of the quotation or sale order. The exception state is automatically set when a cancel operation occurs in the invoice validation (Invoice Exception) or in the packing list process (Shipping Exception). The 'Waiting Schedule' state is set when the invoice is confirmed but waiting for the scheduler to run on the date 'Date Ordered'."), select=True),
    }
sale_order()


class partner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def _unpayed_amount(self, cr, uid, ids, name, arg, context=None):
        res = {}
        today = time.strftime('%Y-%m-%d')
        balance = 0.0
        for partner in self.browse(cr, uid, ids, context):
            account_invoice_obj = self.pool.get('account.invoice')
            account_invoice_list = account_invoice_obj.search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', '=', 'open'),
                    ('type', '=', 'out_invoice'),
                    ('date_due', '<', today)
                ])
            account_invoice = account_invoice_obj.browse(
                cr, uid, account_invoice_list, context)
            for invoice in account_invoice:
                if invoice.currency_id.name == invoice.company_id.currency_id.name:
                    balance += invoice.residual
                elif invoice.currency_id.name != invoice.company_id.currency_id.name:
                    balance += invoice.residual * invoice.rate
            res[partner.id] = balance
        return res

    # def _unpayed_amount(self, cr, uid, ids, name, arg, context=None):
    #     res = {}
    #     today = time.strftime('%Y-%m-%d')
    #     for partner in self.browse(cr, uid, ids, context):
    #         accounts = []
    #         if partner.property_account_receivable:
    #             accounts.append(partner.property_account_receivable.id)
    #         if partner.property_account_payable:
    #             accounts.append(partner.property_account_payable.id)
    #         line_ids = self.pool.get('account.move.line').search(cr, uid, [
    #             ('partner_id','=',partner.id),
    #             ('account_id', 'in', accounts),
    #             ('reconcile_id','=',False),
    #             ('date_maturity','<',today),
    #         ], context=context)
    #         # Those that have amount_residual == 0, will mean that they're circulating. The payment request has been sent
    #         # to the bank but have not yet been reconciled (or the date_maturity has not been reached).
    #         amount = 0.0
    #         amount_return = 0.0
    #         for line in self.pool.get('account.move.line').browse(
    #                 cr, uid, line_ids, context):
    #             if line.currency_id:
    #                 sign = line.amount_currency < 0 and -1 or 1
    #             else:
    #                 sign = (line.debit - line.credit) < 0 and -1 or 1
    #             amount += sign * line.amount_residual
    #             """
    #             Invoices pending payment...Return without invoicing
    #             """
    #             cr.execute("""SELECT dev.id FROM _gv_devolucion AS dev
    #                         INNER JOIN stock_picking AS sp ON sp.origin=dev.order_id
    #                         INNER JOIN account_invoice AS ai ON ai.origin=sp.name
    #                         INNER JOIN account_move AS am ON am.ref=ai.number
    #                         INNER JOIN account_move_line AS aml ON aml.move_id=am.id
    #                         WHERE aml.id=%s""", ([line.id]))
    #             if cr.rowcount:
    #                 returns = cr.fetchall()
    #                 for gv_dev_obj in self.pool.get('gv.devolucion').browse(
    #                         cr, uid, [x[0] for x in returns],
    #                         context=context):
    #                     if not gv_dev_obj.nota_id:
    #                         move_return_ids = self.pool.get('stock.move').search(
    #                             cr, uid, [
    #                                 ('picking_id', '=', gv_dev_obj.picking_id.id)
    #                             ], context=context
    #                         )
    #                         for move_return in self.pool.get('stock.move').browse(
    #                                 cr, uid, move_return_ids, context=context):
    #                             invoice_line_ids = self.pool.get('account.invoice.line').search(
    #                                 cr, uid, [
    #                                     ('invoice_id', '=', gv_dev_obj.invoice_id.id),
    #                                     ('product_id', '=', move_return.product_id.id)
    #                                 ]
    #                             )
    #                             for invoice_line in self.pool.get('account.invoice.line').browse(
    #                                     cr, uid, invoice_line_ids, context):
    #                                 rate = 0.0
    #                                 if line.amount_currency and line.debit:
    #                                     rate = line.debit / line.amount_currency
    #                                 price_unit = invoice_line.price_unit * rate
    #                                 taxes = invoice_line.invoice_line_tax_id.compute_all(
    #                                     price_unit, move_return.product_qty,
    #                                     move_return.product_id.id,
    #                                     partner=invoice_line.invoice_id.partner_id
    #                                 )
    #                                 amount_return += taxes['total_included']
    #         res[partner.id] = amount - amount_return
    #     return res

    def _pending_amount(self, cr, uid, ids, name, arg, context=None):
        res = {}
        today = time.strftime('%Y-%m-%d')
        balance = 0.0
        for partner in self.browse(cr, uid, ids, context):
            account_invoice_obj = self.pool.get('account.invoice')
            account_invoice_list = account_invoice_obj.search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', '=', 'open'),
                    ('type', '=', 'out_invoice'),
                    ('date_due', '>=', today)
                ])
            account_invoice = account_invoice_obj.browse(
                cr, uid, account_invoice_list, context)
            for invoice in account_invoice:
                if invoice.currency_id.name == invoice.company_id.currency_id.name:
                    balance += invoice.residual
                elif invoice.currency_id.name != invoice.company_id.currency_id.name:
                    balance += invoice.residual * invoice.rate
            res[partner.id] = balance
        return res

    # def _pending_amount_(self, cr, uid, ids, name, arg, context=None):
    #     res = {}
    #     today = self.tz_user(str(fields.datetime.now()), context.get('tz', False))
    #     for partner in self.browse(cr, uid, ids, context):
    #         accounts = []
    #         if partner.property_account_receivable:
    #             accounts.append(partner.property_account_receivable.id)
    #         if partner.property_account_payable:
    #             accounts.append(partner.property_account_payable.id)
    #         line_ids = self.pool.get('account.move.line').search(cr, uid, [
    #             ('partner_id', '=', partner.id),
    #             ('account_id', 'in', accounts),
    #             ('reconcile_id', '=', False),
    #             '|',
    #             ('date_maturity', '>=', today),
    #             ('date_maturity', '=', False)
    #         ], context=context)
    #         # Those that have amount_residual == 0, will mean that they're circulating. The payment request has been sent
    #         # to the bank but have not yet been reconciled (or the date_maturity has not been reached).
    #         amount = 0.0
    #         amount_return = 0.0
    #         for line in self.pool.get('account.move.line').browse(
    #                 cr, uid, line_ids, context):
    #             if line.currency_id:
    #                 sign = line.amount_currency < 0 and -1 or 1
    #             else:
    #                 sign = (line.debit - line.credit) < 0 and -1 or 1
    #             amount += sign * line.amount_residual
    #             """
    #             invoice pending payment...Return without invoicing
    #             """
    #             cr.execute("""SELECT dev.id FROM _gv_devolucion AS dev
    #                         INNER JOIN stock_picking AS sp ON sp.origin=dev.order_id
    #                         INNER JOIN account_invoice AS ai ON ai.origin=sp.name
    #                         INNER JOIN account_move AS am ON am.ref=ai.number
    #                         INNER JOIN account_move_line AS aml ON aml.move_id=am.id
    #                         WHERE aml.id=%s""", ([line.id]))
    #             if cr.rowcount:
    #                 returns = cr.fetchall()
    #                 for gv_dev_obj in self.pool.get('gv.devolucion').browse(
    #                         cr, uid, [x[0] for x in returns],
    #                         context=context):
    #                     if not gv_dev_obj.nota_id:
    #                         move_return_ids = self.pool.get('stock.move').search(
    #                             cr, uid, [
    #                                 ('picking_id', '=', gv_dev_obj.picking_id.id)
    #                             ], context=context
    #                         )
    #                         for move_return in self.pool.get('stock.move').browse(
    #                                 cr, uid, move_return_ids, context=context):
    #                             invoice_line_ids = self.pool.get('account.invoice.line').search(
    #                                 cr, uid, [
    #                                     ('invoice_id', '=', gv_dev_obj.invoice_id.id),
    #                                     ('product_id', '=', move_return.product_id.id)
    #                                 ]
    #                             )
    #                             for invoice_line in self.pool.get('account.invoice.line').browse(
    #                                     cr, uid, invoice_line_ids, context):
    #                                 rate = 0.0
    #                                 if line.amount_currency and line.debit:
    #                                     rate = line.debit / line.amount_currency
    #                                 price_unit = invoice_line.price_unit * rate
    #                                 taxes = invoice_line.invoice_line_tax_id.compute_all(
    #                                     price_unit, move_return.product_qty,
    #                                     move_return.product_id.id,
    #                                     partner=invoice_line.invoice_id.partner_id
    #                                 )
    #                                 amount_return += taxes['total_included']
    #         res[partner.id] = amount - amount_return
    #     return res

    def tz_user(self, datetime_from, tzinfo):
        if not tzinfo:
            return time.strftime('%Y-%m-%d')
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz(tzinfo)
        utc = datetime.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = datetime.strftime(utc.astimezone(to_zone), '%Y-%m-%d')
        return central

    def _draft_invoices_amount(self, cr, uid, ids, name, arg, context=None):
        res = {}
        balance = 0.0
        for partner in self.browse(cr, uid, ids, context):
            account_invoice_obj = self.pool.get('account.invoice')
            account_invoice_list = account_invoice_obj.search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', '=', 'draft'),
                    ('type', '=', 'out_invoice')
                ])
            account_invoice = account_invoice_obj.browse(
                cr, uid, account_invoice_list, context)
            for invoice in account_invoice:
                if invoice.picking_dev_id:
                    if invoice.currency_id.name == invoice.company_id.currency_id.name:
                        balance += invoice.amount_total
                    elif invoice.currency_id.name != invoice.company_id.currency_id.name:
                        from_currency_id = invoice.currency_id.id
                        to_currency_id = invoice.company_id.currency_id.id
                        currency_obj = self.pool.get('res.currency')
                        balance += currency_obj.compute(
                            cr, uid, from_currency_id, to_currency_id,
                            invoice.amount_total, round=True, context=context
                        )
            res[partner.id] = balance
        return res

    # def _draft_invoices_amount(self, cr, uid, ids, name, arg, context=None):
    #     res = {}
    #     # print fields.datetime.now(), type(fields.datetime.now)
    #     today = self.tz_user(str(fields.datetime.now()), context.get('tz',False))
    #     for id in ids:
    #         invids = self.pool.get('account.invoice').search(cr, uid, [
    #             ('partner_id', 'child_of', [id]),
    #             ('state', '=', 'draft'),
    #             '|',
    #             ('date_due', '>=', today),
    #             ('date_due', '=', False)
    #         ], context=context)
    #         val = 0.0
    #         amount_return = 0.0
    #         for invoice in self.pool.get('account.invoice').browse(cr, uid, invids, context):
    #             # Note that even if the invoice is in 'draft' state it can have an account.move because it
    #             # may have been validated and brought back to draft. Here we'll only consider invoices with
    #             # NO account.move as those will be added in other fields.
    #             if invoice.currency_id != invoice.company_id.currency_id:
    #                 from_currency_id = invoice.currency_id.id
    #                 to_currency_id = invoice.company_id.currency_id.id
    #                 # customer = self.browse(cr, uid, id, context=context).customer
    #                 # supplier = self.browse(cr, uid, id, context=context).supplier
    #                 context = dict(context)
    #                 # context.update({'customer': customer, 'supplier': supplier})
    #                 context.update({'todo_ttype': 'receipt'})
    #                 currency_pool = self.pool.get('res.currency')
    #                 amount_total = currency_pool.compute(
    #                     cr, uid, from_currency_id, to_currency_id,
    #                     invoice.amount_total, round=True,
    #                     context=context)
    #                 if invoice.currency_rate_alter > 0.0:
    #                     amount_total = invoice.currency_rate_alter * invoice.amount_total
    #                 if invoice.type in ('out_invoice', 'in_refund'):
    #                     val += amount_total
    #                 else:
    #                     val -= amount_total
    #             else:
    #                 if invoice.move_id:
    #                     continue
    #                 if invoice.type in ('out_invoice', 'in_refund'):
    #                     val += invoice.amount_total
    #                 else:
    #                     val -= invoice.amount_total
    #             """
    #             draft invoices... Return without invoicing
    #             """
    #             cr.execute("""SELECT dev.id FROM _gv_devolucion AS dev
    #                         INNER JOIN stock_picking AS sp ON sp.origin=dev.order_id
    #                         INNER JOIN account_invoice AS ai ON ai.origin=sp.name
    #                         WHERE ai.id=%s""", ([invoice.id]))
    #             if cr.rowcount:
    #                 returns = cr.fetchall()
    #                 for gv_dev_obj in self.pool.get('gv.devolucion').browse(
    #                         cr, uid, [x[0] for x in returns],
    #                         context=context):
    #                     if not gv_dev_obj.nota_id and gv_dev_obj.invoice_id.state == 'draft':
    #                         move_ids = self.pool.get('stock.move').search(
    #                             cr, uid, [
    #                                 ('picking_id', '=', gv_dev_obj.picking_id.id)
    #                             ], context=context
    #                         )
    #                         for move_return in self.pool.get('stock.move').browse(
    #                                 cr, uid, move_ids, context=context):
    #                             invoice_line_ids = self.pool.get('account.invoice.line').search(
    #                                 cr, uid, [
    #                                     ('invoice_id', '=', gv_dev_obj.invoice_id.id),
    #                                     ('product_id', '=', move_return.product_id.id)
    #                                 ], context=context)
    #                             for line in self.pool.get('account.invoice.line').browse(
    #                                     cr, uid, invoice_line_ids, context=context):
    #                                 if line.invoice_id.currency_id.id != line.invoice_id.company_id.currency_id.id:
    #                                     taxes = line.invoice_line_tax_id.compute_all(
    #                                         line.price_unit, move_return.product_qty,
    #                                         move_return.product_id.id,
    #                                         partner=line.invoice_id.partner_id)
    #                                     amount_return += currency_pool.compute(
    #                                         cr, uid, line.invoice_id.currency_id.id,
    #                                         line.invoice_id.company_id.currency_id.id,
    #                                         taxes['total_included'],
    #                                         round=True, context=context)
    #                                 else:
    #                                     taxes = line.invoice_line_tax_id.compute_all(
    #                                         line.price_unit, move_return.product_qty,
    #                                         move_return.product_id.id,
    #                                         partner=line.invoice_id.partner_id)
    #                                     amount_return += taxes['total_included']
    #         res[id] = val - amount_return
    #     return res

    def _pending_orders_amount(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context):
            sale_orders = self.pool.get('sale.order').search(
                cr, uid, [
                    ('partner_id', 'child_of', [partner.id]),
                    ('state', 'not in', [
                        'draft', 'cancel', 'wait_risk', 'invoice_except'
                    ])], context=context)
            total = 0.0
            for order in self.pool.get('sale.order').browse(
                    cr, uid, sale_orders, context):
                cr.execute("""
                    SELECT ai.id FROM account_invoice AS ai
                    INNER JOIN stock_picking AS sp ON ai.origin=sp.name
                    INNER JOIN sale_order AS so ON sp.origin=so.name
                    WHERE so.id=%s""", (order.id,))
                if not cr.rowcount:
                    if order.currency.id == order.company_id.currency_id.id:
                        total += order.amount_total
                    elif order.currency.id != order.company_id.currency_id.id:
                        from_currency_id = order.currency.id
                        to_currency_id = order.company_id.currency_id.id
                        currency_obj = self.pool.get('res.currency')
                        amount_total = currency_obj.compute(
                            cr, uid, from_currency_id, to_currency_id,
                            order.amount_total, round=True, context=context
                        )
                        total += amount_total
            res[partner.id] = total
        return res

    # def _pending_orders_amount(self, cr, uid, ids, name, arg, context=None):
    #     res = {}
    #     for id in ids:
    #         sids = self.pool.get('sale.order').search(cr, uid, [
    #             ('partner_id', 'child_of', [id]),
    #             ('state', 'not in', ['draft', 'cancel', 'wait_risk'])
    #         ], context=context)
    #         orders = 'sale.order'
    #         total = 0.0
    #         amount_return = 0.0
    #         for order in self.pool.get(orders).browse(cr, uid, sids, context):
    #             context = dict(context)
    #             if orders == 'sale.order':
    #                 from_currency_id = order.currency
    #                 context.update({'todo_ttype': 'receipt'})
    #             else:
    #                 from_currency_id = order.currency_id
    #             if from_currency_id.id != order.company_id.currency_id.id:
    #                 from_currency_id = from_currency_id.id
    #                 to_currency_id = order.company_id.currency_id.id
    #                 currency_pool = self.pool.get('res.currency')
    #                 amount_total = currency_pool.compute(
    #                     cr, uid, from_currency_id, to_currency_id,
    #                     order.amount_total, round=True, context=context
    #                 )
    #                 if sids:
    #                     amount_invoiced = currency_pool.compute(
    #                         cr, uid, from_currency_id, to_currency_id,
    #                         order.amount_invoiced, round=True, context=context
    #                     )
    #                 total += amount_total - amount_invoiced
    #             else:
    #                 total += order.amount_total - order.amount_invoiced
    #             """
    #             sale order confirmed...
    #             """
    #             cr.execute("""SELECT dev.id FROM _gv_devolucion AS dev
    #                         INNER JOIN stock_picking AS sp ON sp.origin=dev.order_id
    #                         INNER JOIN sale_order AS so ON so.name=sp.origin
    #                         WHERE so.id=%s""", ([order.id]))
    #             if cr.rowcount:
    #                 returns = cr.fetchall()
    #                 for gv_dev_obj in self.pool.get('gv.devolucion').browse(
    #                         cr, uid, [x[0] for x in returns],
    #                         context=context):
    #                     if not gv_dev_obj.nota_id and not gv_dev_obj.invoice_id:
    #                         move_ids = self.pool.get('stock.move').search(
    #                             cr, uid, [
    #                                 ('picking_id', '=', gv_dev_obj.picking_id.id)
    #                             ], context=context
    #                         )
    #                         for move_return in self.pool.get('stock.move').browse(
    #                                 cr, uid, move_ids, context=context):
    #                             order_line_ids = self.pool.get('sale.order.line').search(
    #                                 cr, uid, [
    #                                     ('order_id', '=', order.id),
    #                                     ('product_id', '=', move_return.product_id.id)
    #                                 ], context=context)
    #                             for line in self.pool.get('sale.order.line').browse(
    #                                     cr, uid, order_line_ids, context=context):
    #                                 if line.order_id.currency != order.company_id.currency_id:
    #                                     taxes = line.tax_id.compute_all(
    #                                         line.price_unit, move_return.product_qty,
    #                                         move_return.product_id.id,
    #                                         partner=line.order_id.partner_id)
    #                                     amount_return += currency_pool.compute(
    #                                         cr, uid, line.order_id.currency.id,
    #                                         order.company_id.currency_id.id,
    #                                         taxes['total_included'],
    #                                         round=True, context=context)
    #                                 else:
    #                                     taxes = line.tax_id.compute_all(
    #                                         line.price_unit, move_return.product_qty,
    #                                         move_return.product_id.id,
    #                                         partner=line.order_id.partner_id)
    #                                     amount_return += taxes['total_included']
    #         res[id] = total - amount_return
    #     return res

    def _credit_note(self, cr, uid, ids, namw, arg, context=None):
        res = {}
        balance = 0.0
        for partner in self.browse(cr, uid, ids, context):
            account_invoice_obj = self.pool.get('account.invoice')
            credit_note_list = account_invoice_obj.search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', '=', 'open'),
                    ('type', '=', 'out_refund')
                ])
            credit_note = account_invoice_obj.browse(
                cr, uid, credit_note_list, context)
            for note in credit_note:
                if note.currency_id.name == note.company_id.currency_id.name:
                    balance += note.residual
                elif note.currency_id.name != note.company_id.currency_id.name:
                    from_currency_id = note.currency_id.id
                    to_currency_id = note.company_id.currency_id.id
                    currency_obj = self.pool.get('res.currency')
                    balance += currency_obj.compute(
                        cr, uid, from_currency_id, to_currency_id,
                        note.residual, round=True, context=context
                    )
            res[partner.id] = balance
        return res

    def _unidentified_deposits(self, cr, uid, ids, namw, arg, context=None):
        res = {}
        balance = 0.0
        for partner in self.browse(cr, uid, ids, context):
            unidentified_deposit_obj = self.pool.get('dni.dni')
            unidentified_deposit_list = unidentified_deposit_obj.search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', 'in', ('associated', 'partial_applied')),
                ])
            unidentified_deposit = unidentified_deposit_obj.browse(
                cr, uid, unidentified_deposit_list, context)
            for deposit in unidentified_deposit:
                if deposit.currency_id.name == partner.company_id.currency_id.name:
                    balance += deposit.current_balance
                elif deposit.currency_id.name != partner.company_id.currency_id.name:
                    from_currency_id = deposit.currency_id.id
                    to_currency_id = partner.company_id.currency_id.id
                    currency_obj = self.pool.get('res.currency')
                    balance += currency_obj.compute(
                        cr, uid, from_currency_id, to_currency_id,
                        deposit.current_balance, round=True, context=context
                    )
            res[partner.id] = balance
        return res

    def _advance_payment(self, cr, uid, ids, namw, arg, context=None):
        res = {}
        balance = 0.0
        for partner in self.browse(cr, uid, ids, context):
            advance_payment_obj = self.pool.get('advance.payment')
            advance_payment_list = advance_payment_obj.search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', 'in', ('generated', 'applied_partial')),
                ])
            advance_payment = advance_payment_obj.browse(
                cr, uid, advance_payment_list, context)
            for advance in advance_payment:
                if advance.currency_id.name == partner.company_id.currency_id.name:
                    balance += advance.current_balance
                elif advance.currency_id.name != partner.company_id.currency_id.name:
                    from_currency_id = advance.currency_id.id
                    to_currency_id = partner.company_id.currency_id.id
                    currency_obj = self.pool.get('res.currency')
                    balance += currency_obj.compute(
                        cr, uid, from_currency_id, to_currency_id,
                        advance.current_balance, round=True, context=context
                    )
            res[partner.id] = balance
        return res

    def _total_debt(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context):
            pending_orders = partner.pending_orders_amount or 0.0
            unpayed = partner.unpayed_amount or 0.0
            pending = partner.pending_amount or 0.0
            draft_invoices = partner.draft_invoices_amount or 0.0
            res[partner.id] = pending_orders + unpayed + pending + draft_invoices
        return res

    def _total_positive_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context):
            credit_note = partner.credit_note or 0.0
            u_deposit = partner.unidentified_deposits or 0.0
            a_payment = partner.advance_payment or 0.0
            res[partner.id] = credit_note + u_deposit + a_payment
        return res

    def _total_balance(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context):
            res[partner.id] = partner.total_debt - partner.total_positive_balance
        return res

    def _available_risk(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context):

            cr.execute("""
                SELECT SUM(amount_extension) AS monto FROM
                credit_extension_line_log
                WHERE order_status='A' AND partner_id=%s
            """ % str(partner.id))
            balance_extension_positive = cr.dictfetchone()

            cr.execute("""
                SELECT SUM(amount_extension) AS monto FROM
                credit_extension_line_log
                WHERE order_status in ('P','C') AND partner_id=%s
            """ % str(partner.id))
            balance_extension_negative = cr.dictfetchone()

            total = partner.credit_limit - partner.total_balance
            if balance_extension_positive['monto']:
                total += balance_extension_positive['monto']
            # if balance_extension_negative['monto']:
            #         total -= balance_extension_negative['monto']

            res[partner.id] = total

        return res

    def _total_risk_percent(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context):
            if partner.credit_limit:
                res[partner.id] = 100.0 * partner.total_balance / partner.credit_limit
            else:
                res[partner.id] = 100
        return res

    _columns = {
        'unpayed_amount': fields.function(_unpayed_amount, method=True, string=_('Expired Unpaid Payments'), type='float'),
        'pending_amount': fields.function(_pending_amount, method=True, string=_('Unexpired Pending Payments'), type='float'),
        'draft_invoices_amount': fields.function(_draft_invoices_amount, method=True, string=_('Draft Invoices'), type='float'),
        'pending_orders_amount': fields.function(_pending_orders_amount, method=True, string=_('Uninvoiced Orders'), type='float'),
        'total_debt': fields.function(_total_debt, method=True, string=_('Debt'), type='float'),
        'credit_note': fields.function(_credit_note, method=True, string=_('Credit Notes'), type='float'),
        'unidentified_deposits': fields.function(_unidentified_deposits, method=True, string=_('Unidentified deposits'), type='float'),
        'advance_payment': fields.function(_advance_payment, method=True, string=_('Advance payment'), type='float'),
        'total_positive_balance': fields.function(_total_positive_balance, method=True, string=_('Positive balance'), type='float'),
        'total_balance': fields.function(_total_balance, method=True, string=_('Total Debt'), type='float'),
        'available_risk': fields.function(_available_risk, method=True, string=_('Available Credit'), type='float'),
        'total_risk_percent': fields.function(_total_risk_percent, method=True, string=_('Credit Usage (%)'), type='float')
    }
partner()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
