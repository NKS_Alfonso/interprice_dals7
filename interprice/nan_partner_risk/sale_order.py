# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, exceptions, _
import logging


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def create(self, values):
        record = super(SaleOrder, self).create(values)

        if record.partner_id.forma_pago.str_nombre == 'Firma Factura':
            credit = record.partner_id.available_risk
            if record.partner_id.company_credit_limit:
                total = credit + record.partner_id.balance_extension
                amount_total = record.amount_total
                if record.currency.name == 'USD':
                    amount_total = record.convert_usd_to_mxn()[0]
                if credit < amount_total:
                    raise exceptions.Warning(
                        _('Available credit: %(balance).2f M.N. exceeded') % {
                            'balance': total
                        })

        return record

    @api.one
    def convert_usd_to_mxn(self):
        usd_currency = self.env['res.currency'].search([('name', '=', 'USD')])
        tc = usd_currency.rate_silent
        amount_untaxed = self.amount_untaxed
        taxes = self.amount_tax
        usd_price = 1 / tc
        amount_in_mxn = usd_price * (amount_untaxed + taxes)
        return amount_in_mxn

    @api.one
    def write(self, values):
        if 'state' in values:
            if self.partner_id.forma_pago.str_nombre == 'Firma Factura':
                amount_total = self.amount_total
                log_obj = self.env['credit.extension.line.log']
                if self.currency.name == 'USD':
                    amount_total = self.convert_usd_to_mxn()[0]

                if values['state'] == 'progress':
                    extension = self.partner_id.balance_extension
                    if self.partner_id.company_credit_limit:
                        if ((self.partner_id.available_risk + extension) <
                                amount_total):
                            raise exceptions.Warning(
                                _('''
                                Available credit: %(balance).2f M.N.
                                exceeded''') % {
                                    'balance': self.partner_id.available_risk +
                                    extension
                                })
                        else:
                            if extension >= amount_total:
                                extension -= amount_total
                                self.partner_id.balance_extension = extension
                                log_obj.create({
                                    'amount_extension': amount_total,
                                    'order_id': self.id,
                                    'order_status': 'A',
                                    'partner_id': self.partner_id.id
                                })
                            else:
                                diff = amount_total - extension
                                self.partner_id.balance_extension = 0
                                log_obj.create({
                                    'amount_extension': extension,
                                    'order_id': self.id,
                                    'order_status': 'A',
                                    'partner_id': self.partner_id.id
                                })

                if values['state'] == 'cancel' or values['state'] == 'done':
                    ids = log_obj.search([('order_id', '=', self.id)])
                    status = 'C' if values['state'] == 'cancel' else 'P'
                    if ids:
                        ids.write({'order_status': status})

        return super(SaleOrder, self).write(values)
