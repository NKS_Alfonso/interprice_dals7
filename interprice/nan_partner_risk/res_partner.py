# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    balance_extension = fields.Float(
        _('Extension balance'),
        default=0,
        digits=(10, 2),
        readonly=True,
    )

    credit_extension_lines = fields.One2many(
        'credit.extension.line',
        'partner_id'
    )

    @api.multi
    def show_wizard_credit_extension_report(self):
        self.ensure_one()
        wizard_form = self.env.ref(
            'nan_partner_risk.credit_extension_report_view',
            False
        )

        view_id = self.env['credit.extension.report.wizard']
        vals = {
            'partner_id': self.id,
        }
        new = view_id.create(vals)
        return {
            'name': 'Report Credit Extension',
            'type': 'ir.actions.act_window',
            'res_model': 'credit.extension.report.wizard',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    @api.multi
    def show_wizard_credit_extension(self):
        self.ensure_one()
        wizard_form = self.env.ref(
            'nan_partner_risk.credit_extension_wizard_form', False)

        view_id = self.env['credit.extension.wizard']
        vals = {
            'partner_id': self.id,
            'amount': 0,
            'credit_limit': self.credit_limit,
            'payment_method': self.forma_pago.str_nombre
        }
        new = view_id.create(vals)
        return {
            'name': 'Credit Extension',
            'type': 'ir.actions.act_window',
            'res_model': 'credit.extension.wizard',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }
