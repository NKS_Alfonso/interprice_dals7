# -*- coding: utf-8 -*-

from openerp import models, fields, api


class AccountAccount(models.Model):
    _inherit = 'account.account'
    isRevertOperation = fields.Boolean(string='Revertir operación')