# -*- coding: utf-8 -*-
{
    'name': "Amounts and balances",

    'description': """
        Everything accounts is checked Reverse Operation apply.
    """,

    'author': "Jose Juan Hernandez Bautista, Viridiana Cruz (GVADETO)",
    'website': "http://www.grupovadeto.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Account',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'account',
        'l10n_mx_p'
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
}