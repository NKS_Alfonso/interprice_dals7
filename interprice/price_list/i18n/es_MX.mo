��    H      \  a   �            !     0     <  0   N          �     �  M   �     �       (        C  
   R  
   ]     h     y  .   �     �  	   �  �   �     N  /   Q     �     �     �     �     �     �     �     �     �     	     &	      2	     S	     X	     n	     �	  "   �	     �	     �	     �	     �	     �	  
   �	     �	     �	     
     '
     7
     E
     M
     ^
     r
     �
  	   �
  
   �
     �
     �
     �
     �
     �
     �
          
     "     *     /  C   ?  F   �  
   �  E  �          ,     C  7   Z     �  !   �     �  U   �       #   $  $   H     m  
   �  
   �     �     �  .   �     �  	   �  �   �     �  /   �     �     �     �     �     �       )        =  '   C     k     x  ,   �     �  #   �     �     �  "        *     1     9     @     Q     b  !   r  #   �  (   �     �     �               +     H     ]     }     �     �     �     �  &   �     �                     <     D     I  C   Y  F   �     �     >   A       D   B   	               1      ,   2                    .       /   E       %   F         9           5   "                       6       3              =      :                 
       4       H      -   G          ;      &   '           *   )   @         +       (   ?          !                   $   C      <   7   #             8      0    % Average cost % Last cost % Reposition cost 'Final date' must be higher than 'Initial date'. Average cost Average cost - standard price Base Base price to compute the customer price. Sometimes called the catalog price. Brand Brand product category Can't be duplicate value for this field! Client segment Created by Created on Credit Insurance Currency Date of the last message posted on the record. Email Thread Followers Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. Initial date Invoice Line Is a Follower Last Message Date Last Updated by Last Updated on Last purchase cost percentage Level Level average cost percentage Level id Level price Level reposition cost percentage Line Line product category List price currency Messages Messages and communication history Name Partner Price Price brand Price level Price list Price list final date Price list initial date Price list rule name Price list type Price segment Product Product Template Product price level Reposition cost Reposition cost currency Rule name Sale Price Sales Order Sales Order Line Section Section product category Segment Segment name Serial Serial product category Summary Type Unread Messages calc_price_unit(parent.partner_id, product_id, "", parent.currency) calc_price_unit(parent.partner_id, product_id, "", parent.currency_id) final date Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-02-21 18:15+0000
PO-Revision-Date: 2017-02-21 12:16-0600
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
 % Costo promedio % Costo última compra % Costo de reposición La 'Fecha final' debe ser mayor que la 'Fecha inicial'. Costo promedio Costo promedio - Precio estándar Base Costo base para calcular el precio del cliente. A veces llamado precio del catálogo. Marca Categoría de la marca del producto ¡Este campo no puede ser duplicado! Segmento del cliente Created by Created on Seguro de riesgo Moneda Date of the last message posted on the record. Hilo de mensajes Followers Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. Fecha inicial Línea de factura Is a Follower Last Message Date Last Updated by Last Updated on Porcentaje del costo de la última compra Nivel Porcentaje del costo promedio del nivel Id del nivel Precio del nivel Porcentaje de costo de reposición del nivel Línea Categoría de la línea de producto Moneda de la lista de precio Messages Messages and communication history Nombre Empresa Precio Marca del precio NIvel del precio LIsta de precio Fecha final de la lista de precio Fecha inicial de la lista de precio Nombre de la regla de la lista de precio Tipo de lista de precio Segmento de precio Producto Plantilla de producto NIvel de precio del producto Costo de reposición Moneda del costo de reposición Nombre de la regla Precio de venta Pedidos de Venta Línea pedido de venta Sección Categoría de la sección del producto Segmento Nombre del segmento Serie Categoría de serie del producto Resumen Tipo Unread Messages calc_price_unit(parent.partner_id, product_id, "", parent.currency) calc_price_unit(parent.partner_id, product_id, "", parent.currency_id) Fecha final 