# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from models import (product_template,
                    price_level,
                    product_price_level,
                    product_segment,
                    price_list,
                    res_partner,
                    sale_order_line,
                    account_invoice_line,
                    product_category,
                    res_users,
                    # stock_picking
                    )
