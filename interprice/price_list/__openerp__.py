# -*- coding: utf-8 -*-
{
    'name': 'Price List',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Sales',
    'description': 'Price list product/client extension',
    'depends': ['base', 'product', 'sale', 'sale_stock'],
    'application': True,
    'data': [
        'views/product_template_view.xml',
        'views/product_price_level_view.xml',
        'views/product_segment_view.xml',
        'views/price_list_view.xml',
        'views/res_partner_view.xml',
        'views/sale_order_line_view.xml',
        'views/account_invoice_line_view.xml',
        'views/product_category_view.xml',
        'views/res_users_view.xml',
        'security/ir.model.access.csv',
        'data/default_level_data.xml',
        'data/default_type_data.xml',
    ],
    'demo': [
    ]
}
