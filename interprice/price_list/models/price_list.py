# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, exceptions, api, fields, _


class price_list(models.Model):
    """Price list model"""

    # odoo model properties
    _name = "price.list"
    _inherit = ['mail.thread']
    _rec_name = 'name'

    # selection field options
    list_types = [
        ('segment', _('Segment')),
        ('brand', _('Brand'))
    ]

    # custom fields
    name = fields.Char(
        help="Price list rule name",
        required=True,
        string="Rule name",
    )
    list_type = fields.Selection(
        help="Price list type",
        required=True,
        selection=list_types,
        string="Type",
    )
    brand = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Marca')]",
        help="Price brand",
        string="Brand",
    )
    segment = fields.Many2one(
        comodel_name='product.segment',
        help="Price segment",
        string="Segment",
    )
    level = fields.Many2one(
        string="Level",
        help="Level",
        comodel_name="price.level",
        required=True,
    )
    initial_date = fields.Date(
        help="Price list initial date",
        required=True,
        string="Initial date",
    )
    final_date = fields.Date(
        help="Price list final date",
        required=True,
        string="final date",
    )

    @api.onchange('initial_date', 'final_date', 'segment', 'brand', 'type')
    def validate_filters(self):
        """Validates dates range"""
        df = self.initial_date
        dt = self.final_date
        if df and dt and df > dt:
            return {
                'value': {'initial_date': False, 'final_date': False},
                'warning': {
                    'title': 'warning',
                    'message': _("'Final date' must be higher than 'Initial date'.")
                }
            }
