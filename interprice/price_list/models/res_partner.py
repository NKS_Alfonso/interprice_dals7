# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT
from datetime import datetime


class res_partner(models.Model):
    """Extended model to add new fields
    for product related to price level"""

    # odoo model properties
    _inherit = "res.partner"

    # custom fields
    client_segment = fields.Many2one(
        comodel_name='product.segment',
        help="Client segment",
        # required=True,
        string="Segment",
    )
    base = fields.Many2one(
        string="Base level",
        help="Base level",
        comodel_name="price.level",
        # required=True,
    )
    brand = fields.Many2many(
        comodel_name='price.list',
        relation='client_price_list_rel',
        column1='partner',
        column2='price_list',
        help="Price brand",
        string="Brand",
        domain=[
            ('list_type', '=', 'brand'),
            ('initial_date', '<=', datetime.now().strftime(DATE_FORMAT)),
            ('final_date', '>=', datetime.now().strftime(DATE_FORMAT))
        ]
    )
    segment = fields.Many2one(
        comodel_name='price.list',
        help="Price segment",
        string="Segment",
        # domain=[
        #     ('list_type', '=', 'segment'),
        #     ('initial_date', '<=', datetime.now().strftime(DATE_FORMAT)),
        #     ('final_date', '>=', datetime.now().strftime(DATE_FORMAT))
        # ]
    )

    @api.onchange('client_segment')
    def get_segment_price_list(self):
    #     res = {}
    #     res['domain'] = {
    #         'segment': [
    #             ('list_type', '=', 'segment'),
    #             ('segment', '=', self.client_segment.id),
    #             ('initial_date', '<=', datetime.now().strftime(DATE_FORMAT)),
    #             ('final_date', '>=', datetime.now().strftime(DATE_FORMAT))
    #         ],
    #     }
        self.segment = False
    #     return res
