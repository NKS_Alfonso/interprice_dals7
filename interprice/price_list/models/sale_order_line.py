# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, exceptions, _
from openerp.osv import osv

import logging
_logger = logging.getLogger(__name__)


class sale_order(models.Model):
    """Extended model to retrieve price list on Sales Order lines"""

    # odoo model properties
    _inherit = 'sale.order'

    def _default_currency(self):
        company = self.env['res.company']._company_default_get('sale.order')
        company = self.env['res.company'].browse(company)
        return company.currency_id

    currency = fields.Many2one(
        comodel_name='res.currency',
        help="Currency",
        required=True,
        string="Currency",
        default=_default_currency,
    )

    @api.onchange('currency')
    def calc_line_price(self):
        if self.currency and self.order_line:
            self.order_line = False

    def action_button_confirm(self):
        user = self.env['res.users'].browse(self.env.uid)
        for sale_line in self.order_line:
            sale_line.check_under_cost_price(user, sale_line, self)

        return super(sale_order, self).action_button_confirm()


class sale_order_line(models.Model):
    """Extended model to retrieve price list on Sales Order lines"""

    # odoo model properties
    _inherit = 'sale.order.line'

    # Methods
    def calc_price_unit(self, cr, uid, ids,
                        client_id=None,
                        product_id=None,
                        product_code="",
                        currency_id=None,
                        currency_name="",
                        context=None):
        """Retrieves price_list price from product"""
        res = {'value': {}}
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        if product:
            prod_price = product.product_tmpl_id.calc_price_unit(
                client_ids=client_id,
                product_ids=product_id,
                # product_code,
                currency_ids=currency_id,
                # currency_name
            ).get(product.default_code, False)
            if prod_price:
                res['value']['price_unit'] = prod_price.get('price', 0.00)
        return res

    @api.onchange('purchase_price')
    def change_purchase_price(self):
        company_currency_id = self.order_id.company_id.currency_id
        standard_price = company_currency_id.with_context(
            todo_ttype='receipt'
        ).compute(
            self.product_id.product_tmpl_id.average_cost,
            self.order_id.currency,
            True,
        )
        purchase_price = company_currency_id.with_context(
            todo_ttype='receipt'
        ).compute(
            self.purchase_price,
            self.order_id.currency,
            True,
        )
        if standard_price == purchase_price:
            if self.order_id.currency != company_currency_id:
                self.purchase_price = purchase_price


class sale_order_line_osv(osv.osv):
    _inherit = "sale.order.line"

    def product_id_change_with_wh(self, cr, uid, ids,
                                  pricelist, product, qty=0,
                                  uom=False, qty_uos=0, uos=False,
                                  name='', partner_id=False,
                                  lang=False, update_tax=True,
                                  date_order=False, packaging=False,
                                  fiscal_position=False,
                                  flag=False, warehouse_id=False,
                                  context=None):
        res = super(sale_order_line_osv, self).product_id_change_with_wh(
            cr, uid, ids, pricelist, product, qty,
            uom, qty_uos, uos, name, partner_id,
            lang, update_tax, date_order,
            packaging, fiscal_position,
            flag, warehouse_id, context)
        value = res.get('value', False)
        if value:
            price_unit = value.get('price_unit', False)
            qty = value.get('product_uos_qty', False)
            if not qty:
                qty = value.get('product_uom_qty', False)
            if price_unit and qty:
                value.pop('price_unit', False)
        return res

    @api.model
    def create(self, vals):
        user = self.env['res.users'].browse(self.env.uid)
        order = self.env['sale.order'].browse(vals.get('order_id', 0))
        self.check_under_cost_price(user, None, order, vals)
        return super(sale_order_line_osv, self).create(vals)

    @api.multi
    def write(self, vals):
        user = self.env['res.users'].browse(self.env.uid)
        if(len(self) == 1):
            self.check_under_cost_price(user, self, self.order_id, vals)
        return super(sale_order_line_osv, self).write(vals)

    def check_under_cost_price(self, user, sale_line=None, sale_order=None, vals={}):
        margin = 0.09
        if sale_line:
            is_delivery = sale_line.is_delivery
            product_id = sale_line.product_id
            price_unit = vals.get('price_unit', 0.00) or sale_line.price_unit
        else:
            is_delivery = vals.get('is_delivery', False)
            product_id = self.env['product.product'].browse(
                vals.get('product_id', 0))
            price_unit = vals.get('price_unit', 0.00)

        if not is_delivery and not user.under_cost_price:
            product_tmpl_id = product_id.product_tmpl_id
            level_price = product_tmpl_id.calc_price_unit(
                client_ids=self.check_fields(sale_order.partner_id.id, 'Client'),
                product_ids=self.check_fields(product_id.id, 'Product'),
                # product_code,
                # currency_ids=sale_order.currency.id,
                currency_ids=self.check_fields(product_tmpl_id.list_price_currency.id, 'Currency')
                # currency_name
            ).get(product_id.default_code, False)
            price_unit = sale_order.currency.with_context(
                todo_ttype='receipt'
            ).compute(
                price_unit,
                product_tmpl_id.list_price_currency,
                True,
            )
            if level_price:

                if level_price.get('price', 0.00) != price_unit:
                    self.env.cr.execute(
                        """
                        SELECT EXISTS(
                            SELECT true
                                FROM product_price_level ppl
                                JOIN price_level pl
                                    ON (pl.id = ppl.price_level)
                                WHERE pl.level_id <= CAST(%s as integer)
                                    AND ppl.price <= %s
                                    AND ppl.product = %s
                        );
                        """,
                        (user.price_level.level_id,
                         price_unit,
                         product_tmpl_id.id)
                    )
                    if self.env.cr.rowcount:
                        res = [result[0] for result in self.env.cr.fetchall()][0]
                        if not res:
                            leve_price_tmp = abs(level_price.get('price', 0.00) - price_unit)
                            price_integer = leve_price_tmp // 1
                            price_decimal = leve_price_tmp % 1
                            if price_integer != 0 or price_decimal > margin:
                                raise exceptions.Warning(
                                    _(
                                        "Sales executive: %s, can't sell "
                                        "product: %s, with price: %s."
                                        "\n Check user's price list level"
                                        % (user.name,
                                            product_id.name,
                                            price_unit)
                                    )
                                )

    def check_fields(self, field, string):
        if not field and string == 'Currency':
            raise exceptions.Warning(
                _("The %s field is empty in product template" % string))