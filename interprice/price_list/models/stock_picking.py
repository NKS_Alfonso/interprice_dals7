# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def _get_invoice_vals(self, cr, uid, key, inv_type,
                          journal_id, move, context=None):
        if context is None:
            context = {}
        partner, currency_id, company_id, user_id = key
        res = super(StockPicking, self).\
            _get_invoice_vals(cr, uid, key, inv_type,
                              journal_id, move, context=context)
        if inv_type in ('out_invoice', 'out_refund'):
            so_pool = self.pool.get('sale.order')
            sale_order = so_pool.search(cr, uid, [('name', '=', move.origin)])
            sale_order = so_pool.browse(cr, uid, sale_order)
            if 'currency_id' not in res:
                res['currency_id'] = sale_order.currency.id,
            else:
                res.update({'currency_id': sale_order.currency.id})
        return res
