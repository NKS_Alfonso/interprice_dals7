# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
import time
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, exceptions, _
import openerp.addons.decimal_precision as dp


prev_price_cur = False
prev_reposition_cur = False


class product_template(models.Model):
    """Extended model to add new fields
    for product related to price level"""
    i = 0
    # odoo model properties
    _inherit = "product.template"

    def _default_price_level(self):
        product_price_levels_nopop = []
        price_levels = [pl.id for pl in self.env[
            'price.level'].search([('id', '!=', False)])]
        for price_level in price_levels:
            product_price_levels_nopop.append(
                (0, False, {'price_level': price_level,
                            'product': self.id,
                            'price': self.reposition_cost,
                            'reposition_cost': 0.00,
                            'average_cost': 0.00,
                            'last_po_cost': 0.00,
                            })
            )
        return product_price_levels_nopop

    # Overriden fields
    list_price = fields.Float(
        digits_compute=dp.get_precision('Product Price'),
        help="Base price to compute the customer price."
        " Sometimes called the catalog price.",
        readonly=True,
        # related='reposition_cost',
        string='Sale Price',
    )
    # custom fields
    reposition_cost = fields.Float(
        default=0.01,
        digits=(12, 2),
        help="Reposition cost",
        required=True,
        string="Reposition cost",
    )

    list_price_ro = fields.Float(
        store=True,
        digits=(12, 2),
        help="Base price to compute the customer price."
        " Sometimes called the catalog price.",
        readonly=True,
        related='list_price',
        string="Sale Price",
    )
    reposition_cost_ro = fields.Float(
        digits=(12, 2),
        help="Reposition cost",
        readonly=True,
        related='reposition_cost',
        string="Reposition cost",
    )
    average_cost = fields.Float(
        compute='_compute_average_cost',
        digits=(12, 2),
        help="Average cost - standard price",
        # store=True,
        string="Average cost",
    )
    last_po_cost = fields.Float(
        compute='_compute_last_po_cost',
        digits=(12, 2),
        help="Last purchase order price unit",
        # store=True,
        string="Last PO. Cost",
    )

    # relation fields
    list_price_currency = fields.Many2one(
        comodel_name='res.currency',
        help="List price currency",
        required=True,
        string="List price currency",
    )
    reposition_cost_currency = fields.Many2one(
        comodel_name='res.currency',
        help="Reposition cost currency",
        required=True,
        string="Reposition cost currency",
    )

    section = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Sección')]",
        help="Section product category",
        required=True,
        string="Section",
    )
    brand = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Marca')]",
        help="Brand product category",
        required=True,
        string="Brand",
    )
    line = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Línea')]",
        help="Line product category",
        required=True,
        string="Line",
    )
    serial = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Serie')]",
        help="Serial product category",
        required=True,
        string="Serial",
    )
    # relation / computed
    list_price_currency_ro = fields.Many2one(
        comodel_name='res.currency',
        help="List price currency",
        readonly=True,
        related='list_price_currency',
        string="List price currency",
    )
    reposition_cost_currency_ro = fields.Many2one(
        comodel_name='res.currency',
        help="Reposition cost currency",
        readonly=True,
        related='reposition_cost_currency',
        string="Reposition cost currency",
    )
    average_cost_currency_ro = fields.Many2one(
        comodel_name='res.currency',
        help="Average cost currency",
        readonly=True,
        related='company_id.currency_id',
        string="Average cost currency",
    )
    last_po_cost_currency_ro = fields.Many2one(
        compute='_compute_last_po_currency',
        comodel_name='res.currency',
        help="Last purchase order cost currency",
        string="Last PO. Cost currency",
    )

    product_price_levels_nopop = fields.One2many(
        comodel_name='product.price.level',
        help="Product price level",
        inverse_name="product",
        required=False,
        string="Price level",
    )

    # This field is used to avoid default computed problems
    compute_levels = fields.Char(
        compute='_compute_product_price_levels_nopop',
    )

    @api.onchange('reposition_cost')
    def onchange_reposition_cost(self):
        # Change level price if it's lower than reposition_cost
        company_currency = self.env.user.company_id.currency_id
        if self.reposition_cost <= 0:
            return {
                    'warning': {
                        'title': _('Warning'),
                        'message': _("Reposition cost must be positive, please add another value"),
                    },
                    'value': {
                        'reposition_cost': 0.01
                    },
                }
        elif self.list_price_currency.id == company_currency.id and self.list_price_currency.id != self.reposition_cost_currency.id:
            rate = self.env['res.currency'].search([('name', '=', 'USD')]).rate_sale
            reposition_cost_real = round((self.reposition_cost * (1 / rate)),2)
            if reposition_cost_real > self.list_price:
                self.reposition_cost = 0
                return {
                    'warning':{
                        'title': _('Warning'),
                        'message': _("Reposition cost must be smaller than list price.\n"
                            "The actual reposition cost is %s and \n"
                             "The price list is %s ") % (reposition_cost_real, self.list_price)
                    }
                }
        elif self.list_price_currency.id != company_currency.id and self.list_price_currency.id != self.reposition_cost_currency.id:
            rate = self.env['res.currency'].search([('name', '=', 'USD')]).rate_sale
            reposition_cost_real = round((self.reposition_cost / (1 / rate)),2)
            if reposition_cost_real > self.list_price:
                self.reposition_cost = 0
                return {
                    'warning':{
                        'title': _('Warning'),
                        'message': _("Reposition cost must be smaller than list price.\n"
                            "The actual reposition cost is %s and \n"
                             "the price list is %s ") % (reposition_cost_real, self.list_price)
                    },
                }

        elif self.list_price_currency.id == self.reposition_cost_currency.id:
            if not self.product_price_levels_nopop:
                for price_level in self.product_price_levels_nopop:
                    if price_level.price < self.reposition_cost:
                        price_level.set_reposition_cost()
            else:
                if self.list_price < self.reposition_cost:
                # self.reposition_cost = 0
                    return{
                            'warning':{
                                'title': _('Warning'),
                                'message': _("Reposition cost must be smaller than list price.\n"
                                    "The actual reposition cost is %s and \n"
                                     "the price list is %s ") % (self.reposition_cost, self.list_price)
                            },
                            # 'value': {
                            # 'price_list': self.reposition_cost
                            # },
                        }

    @api.one
    @api.depends('standard_price')
    def _compute_average_cost(self):
        cost = 0.00
        if self.id:
            self.env.cr.execute(
                """
                SELECT cost FROM product_price_history
                    WHERE product_template_id = %s
                        AND cost != 0
                ORDER BY datetime DESC
                LIMIT 1;
                """, (self.id,)
            )
            if self.env.cr.rowcount:
                cost = [c[0] for c in self.env.cr.fetchall()]
                cost = cost[0] or 0.00
        self.average_cost = cost

    @api.one
    @api.depends('last_po_cost')
    def _compute_last_po_cost(self):
        last_cost = 0.00
        if self.product_variant_ids.id:
            self.env.cr.execute(
                """
                SELECT price_unit FROM purchase_order_line pol
                LEFT JOIN purchase_order po
                    ON (pol.order_id = po.id)
                WHERE po.shipped = TRUE
                    AND pol.product_id = %d
                    AND pol.invoiced = TRUE
                ORDER BY pol.write_date DESC
                LIMIT 1; """ % (self.product_variant_ids.id,)
            )
            if self.env.cr.rowcount:
                last_cost = [price_unit[0]
                             for price_unit in self.env.cr.fetchall()]
                last_cost = last_cost[0]

        self.last_po_cost = last_cost or 0.00

    @api.one
    @api.depends('last_po_cost')
    def _compute_last_po_currency(self):
        last_cost_currency = False
        self.env.cr.execute(
            """
            SELECT currency_id FROM purchase_order_line pol
            LEFT JOIN purchase_order po
                ON (pol.order_id = po.id)
            WHERE po.shipped = TRUE
                AND pol.product_id = CAST(%s AS integer)
                AND pol.invoiced = TRUE
            ORDER BY pol.write_date DESC
            LIMIT 1;
            """,
            (self.product_variant_ids.id,)
        )
        if self.env.cr.rowcount:
            last_cost_currency = [currency[0]
                                  for currency in self.env.cr.fetchall()]
            last_cost_currency = last_cost_currency[0]

        self.last_po_cost_currency_ro = last_cost_currency or False

    @api.depends('list_price')
    def _compute_product_price_levels_nopop(self):
        if(not self.product_price_levels_nopop):
            self.write(
                {
                    'product_price_levels_nopop': self._default_price_level()
                }
            )

    @api.model
    def calc_price_unit(self,
                        client_ids=None,
                        product_codes=None,
                        currency_names=None,
                        product_ids=None,
                        currency_ids=None):
        """This method is used to retrieve the price of a product
        by client price lists
        and brands or segments, this method can be accessed
        outside odoo environment"""
        code_price = {}
        # CONVERT VALUES TO TUPLES
        if not isinstance(client_ids, tuple) and\
                not isinstance(client_ids, list):
            client_ids = 0 if client_ids is False else client_ids,
        if not isinstance(product_ids, tuple) and\
                not isinstance(product_ids, list):
            product_ids = 0 if product_ids is False else product_ids,
        if not isinstance(product_codes, tuple) and\
                not isinstance(product_codes, list):
            product_codes = "" if product_codes is False else product_codes,
        if not isinstance(currency_ids, tuple) and\
                not isinstance(currency_ids, list):
            currency_ids = 0 if currency_ids is False else currency_ids,
        if not isinstance(currency_names, tuple) and\
                not isinstance(currency_names, list):
            currency_names = "" if currency_names is False else currency_names,

        self.env.cr.execute("""
            SELECT DISTINCT ON(prod_template.id)
                product.default_code,
                CASE
                WHEN prod_template.list_price_currency != res_currency.id
                    AND rate_prod.rate_sale < rate_sale.rate_sale
                    THEN prod_price_level.price / rate_prod.rate_sale
                WHEN prod_template.list_price_currency != res_currency.id
                    AND rate_prod.rate_sale > rate_sale.rate_sale
                    THEN prod_price_level.price * rate_sale.rate_sale
                ELSE prod_price_level.price
                END
            FROM product_template prod_template
            INNER JOIN product_product product
                ON (product.product_tmpl_id = prod_template.id)
            INNER JOIN res_partner
                ON (res_partner.id IN %s)
            LEFT JOIN LATERAL (SELECT rel.price_list
                                FROM client_price_list_rel rel
                                JOIN price_list pl
                                ON (pl.id = rel.price_list)
                                WHERE partner = res_partner.id
                                    AND pl.brand = prod_template.brand
                                ORDER BY price_list DESC
                                LIMIT 1)
                AS brand_pl
                ON (brand_pl IS NOT NULL)
            LEFT JOIN price_list
                ON (price_list.id =
                    CASE WHEN brand_pl IS NULL
                        THEN res_partner.segment
                    ELSE brand_pl.price_list
                    END AND (price_list.initial_date <= %s
                    AND price_list.final_date >= %s))
            INNER JOIN price_level
                ON (price_level.id =
                    CASE WHEN price_list.id IS NOT NULL
                        THEN price_list.level
                    ELSE res_partner.base
                    END)
            INNER JOIN product_price_level prod_price_level
                ON (prod_price_level.product = prod_template.id
                    AND prod_price_level.price_level = price_level.level_id)
            JOIN res_currency
                ON (res_currency.id IN %s
                    OR res_currency.name IN %s)
            LEFT JOIN res_currency_rate rate_sale
                ON (rate_sale.id = (SELECT id FROM res_currency_rate rcr
                                    WHERE rcr.currency_id = res_currency.id
                                        AND rcr.name <= %s
                                        AND rcr.rate_sale > 0
                                    ORDER BY name DESC
                                    LIMIT 1)
                    )
            LEFT JOIN res_currency_rate rate_prod
                ON (rate_prod.id = (SELECT id
                                    FROM res_currency_rate rcr
                                    WHERE rcr.currency_id = prod_template.list_price_currency
                                        AND rcr.name <= %s
                                        AND rcr.rate_sale > 0
                                    ORDER BY name DESC
                                    LIMIT 1))
            WHERE product.id IN %s
                OR product.default_code IN %s;
            """, (tuple(client_ids),
                  time.strftime('%Y-%m-%d'),
                  time.strftime('%Y-%m-%d'),
                  tuple(currency_ids),
                  tuple(currency_names),
                  time.strftime('%Y-%m-%d'),
                  time.strftime('%Y-%m-%d'),
                  tuple(product_ids), tuple(product_codes)))

        if self.env.cr.rowcount:
            code_price = {prod_price[0]: {'price': prod_price[1] or 0.00}
                          for prod_price in self.env.cr.fetchall()}

        return code_price

    @api.onchange('list_price_currency')
    def convert_list_price(self):
        if self.list_price_currency and self.product_price_levels_nopop:
            self.list_price = 0
            return {
                'warning': {
                    'title': _('Warning'),
                    'message': _("Currency changed: \n"
                                 "Level prices not changed, change manually."),
                },
                'value': {
                    'list_price_currency_ro': self.list_price_currency,
                    'list_price': 0
                },
            }

    @api.onchange('reposition_cost_currency')
    def convert_reposition_cost(self):
        global prev_reposition_cur
        company_currency_id = self.env.user.company_id.currency_id
        if not prev_reposition_cur and\
                prev_reposition_cur != self.reposition_cost_currency:
            prev_reposition_cur = self.reposition_cost_currency
        else:
            try:
                if self.reposition_cost_currency.id == company_currency_id.id and self.list_price_currency.id != company_currency_id.id:
                    # rate = self.env['res.currency'].search(
                    #     [('name', '=', 'USD')]).rate_sale
                    rate = self.list_price_currency.rate_sale
                    self.reposition_cost = round((self.reposition_cost / rate),2)
                elif self.reposition_cost_currency.id != company_currency_id.id and self.list_price_currency.id == company_currency_id.id:
                    rate = self.reposition_cost_currency.rate_sale
                    self.reposition_cost = round((self.reposition_cost * rate),2)
                elif self.reposition_cost_currency.id == self.list_price_currency.id:
                    rate = self.reposition_cost_currency.rate_sale
                    self.reposition_cost = round((self.reposition_cost * rate),2)
                #     pass
            except ZeroDivisionError:
                self.reposition_cost = 0.00

    @api.model
    def create(self, vals):
        if vals.get('reposition_cost', 0.01) <= 0.00:
            self.reposition_cost = 0.01
            raise exceptions.Warning(
                _("Reposition cost must be positive, please add another value"))
        return super(product_template, self).create(vals)

    @api.multi
    def write(self, vals):
        # import pdb; pdb.set_trace()
        company_currency_id = self.env.user.company_id.currency_id
        if vals.get('list_price_currency'):
            list_price =  vals.get('list_price', self.product_price_levels_nopop[0].price)
            currency_obj = self.env['res.currency'].\
                search([('id', '=', vals.get('list_price_currency'))])
            # if currency_obj.id == company_currency_id.id and self.reposition_cost_currency.id != company_currency_id:
            #     pass
            # elif currency_obj.id != company_currency_id.id and self.reposition_cost_currency.name == company_currency_id.id:
            #     pass
            if vals.get('list_price_currency') == self.reposition_cost_currency.id:
                if list_price < vals.get('reposition_cost', self.reposition_cost):
                    raise exceptions.Warning(
                        _("List price should be higher than reposition cost"))
        # elif vals.get('reposition_cost_currency'):
        #     reposition_cost =  vals.get('reposition_cost', self.reposition_cost)
        #     currency_obj = self.env['res.currency'].\
        #         search([('id', '=', vals.get(vals.get('reposition_cost_currency')))])
        #     if currency_obj.id == company_currency_id.id and self.list_price_currency.id != company_currency_id.id:
        #         pass
        #     elif currency_obj.id != company_currency_id.id and self.list_price_currency.id == company_currency_id.id:
        #         pass
        #     elif vals.get('list_price_currency') == self.list_price_currency.id:
        #         pass
        else:
            # if self.list_price_currency.id == company_currency_id.id  and self.reposition_cost_currency.id != company_currency_id.id:
            #     pass
            # elif self.list_price_currency.id != company_currency_id.id and self.reposition_cost_currency.id == company_currency_id.id:
            #     pass
            if self.list_price_currency.id == self.reposition_cost_currency.id:
                if self.product_price_levels_nopop:
                    if (self.product_price_levels_nopop[0].price < vals.get('reposition_cost', self.reposition_cost)):
                        precio_lista = self.product_price_levels_nopop[0].price
                        # raise exceptions.Warning(
                        #     _("List price should be higher than reposition cost.\n"
                        #         "The actual reposition cost is %s and \n"
                        #         "The price list is %s ") % (vals.get('reposition_cost', self.reposition_cost), precio_lista))
                else:
                    if vals.get('reposition_cost', self.reposition_cost) < vals.get('list_price', self.list_price):
                        pass
                    # else: raise exceptions.Warning(
                    #         _("List price should be higher than reposition cost."))

        if vals.get('reposition_cost', 0.01) <= 0.00:
            self.reposition_cost = 0.01
            raise exceptions.Warning(
                _("Reposition cost must be positive, please add another value"))
        else:
            for price_level in self.product_price_levels_nopop:
            # Set list_price with price of level 1
                if price_level.price_level.level_id == 1:
                    vals['list_price'] = price_level.price
                    # self.list_price = price_level.price
                # Check levels for negative percentages
                """if price_level.reposition_cost < 0 or \
                        price_level.average_cost < 0 or \
                        price_level.last_po_cost < 0:
                    raise exceptions.Warning(
                        _("Negative costs:\n"
                          "Please check your sales price and reposition cost")
                    )"""
        return super(product_template, self).write(vals)

    @api.onchange('product_price_levels_nopop')
    def onchange_first_level(self):
        for price_level in self.product_price_levels_nopop:
            if price_level.price_level.level_id == 1:
                self.list_price = price_level.price
