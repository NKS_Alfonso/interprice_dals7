# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, exceptions


class product_price_level(models.Model):
    """Product price level model"""

    # odoo model properties
    _name = "product.price.level"
    _rec_name = "price_level"

    # custom fields
    price = fields.Float(
        digits=(12, 2),
        help="Level price",
        required=True,
        string="Price",
    )
    reposition_cost = fields.Float(
        compute='compute_cost',
        digits=(12, 2),
        help="Level reposition cost percentage",
        readonly=True,
        string="% Reposition cost",
        # store=True,
    )
    average_cost = fields.Float(
        compute='compute_cost',
        digits=(12, 2),
        help="Level average cost percentage",
        readonly=True,
        string="% Average cost",
        # store=True,
    )
    last_po_cost = fields.Float(
        compute='compute_cost',
        digits=(12, 2),
        help="Last purchase cost percentage",
        readonly=True,
        string="% Last cost",
        # store=True,
    )

    # relation fields
    product = fields.Many2one(
        comodel_name='product.template',
        help="Product",
        ondelete='cascade',
        # required=True,
        string="Product",
    )
    price_level = fields.Many2one(
        comodel_name='price.level',
        help="Price level",
        ondelete='cascade',
        readonly=True,
        required=True,
        string="Price level",
    )

    # Methods
    def set_reposition_cost(self):
        self.price = self.product.reposition_cost
        """self.env['product.price.level'].browse([self.id]).write(
            {'price': self.product.reposition_cost})"""

    @api.onchange('price')
    def _calc_price(self):
        """This method computes all price_level lines in a product
        and validates its price to change bellow lines prices"""
        reposition_cost = self.product.reposition_cost
        price = self.price #Level price
        product_id = self._origin.product.id
        company_currency_id = self.env.user.company_id.currency_id
        price_levels = []

        if self.product.list_price_currency != self.product.reposition_cost_currency:
            if self.product.list_price_currency.id != company_currency_id.id:
                rate_usd = self.product.list_price_currency.rate
                price_rated = price * (1/rate_usd)
                if price_rated < reposition_cost:
                    self.price =  reposition_cost / (1/rate_usd)
                else:
                    self.price = price
            else:
                rate_usd = self.product.reposition_cost_currency.rate
                price_rated = price / (1/rate_usd)
                if price_rated < reposition_cost:
                    self.price =  reposition_cost * (1/rate_usd)
                else:
                    self.price = price
        else:
            # # Set price if reposition cost is greater
            self.price = reposition_cost if price < reposition_cost else price

        if product_id:
            # Update and validate next price levels
            price_levels = self.env['product.price.level'].search(
                [
                    ('product', '=', product_id),
                    ('price_level.level_id', '>=', self.price_level.level_id),

                ]
            )
            if self.price_level.level_id > 1:
                prev_level = self.env['product.price.level'].search(
                    [
                        ('product', '=', product_id),
                        ('price_level.level_id', '=',
                         self.price_level.level_id - 1)
                    ]
                )
                if prev_level and price > prev_level.price:
                    self.price = prev_level.price
            if price_levels:
                price_levels.write({'price': self.price})

    @api.one
    @api.depends('price', 'product')
    # @api.onchange('price')
    def compute_cost(self):
        """This method computes all percentages inside producte_price_levels"""
        if self.price > 0:
            reposition_cost, average_cost, last_po_cost = self.calc_cost()
            self.reposition_cost = reposition_cost
            self.average_cost = average_cost
            self.last_po_cost = last_po_cost

    def calc_cost(self):
        """This method returns all percentages inside producte_price_levels"""
        reposition_cost = 0.00
        average_cost = 0.00
        last_po_cost = 0.00
        if self.price > 0:
            reposition_cost = self.calc_reposition_cost_percent(
                _price=self.price,
                _cost=self.product.reposition_cost,
                _currency=self.product.reposition_cost_currency)
            average_cost = self.calc_average_cost_percent(
                _price=self.price,
                _cost=self.product.average_cost,
                _currency=self.product.average_cost_currency_ro)
            last_po_cost = self.calc_cost_percent_last_po(
                _price=self.price,
                _cost=self.product.last_po_cost,
                _currency=self.product.last_po_cost_currency_ro)
        return reposition_cost, average_cost, last_po_cost

    def calc_reposition_cost_percent(self, _price, _cost, _currency):
        sale_price_list = float(_price)
        reposition_cost = float(_cost)  # Costo de reposicion
        reposition_cost_currency = _currency  # Moneda costo reposicion
        sale_price_list_currency = self.product.list_price_currency  # Moneda costo venta
        company_currency_id = self.env.user.company_id.currency_id
        result = 0.00
        try:
            if reposition_cost_currency != sale_price_list_currency:
                if reposition_cost_currency.id != company_currency_id.id:
                    # reposition_cost *= (1 / rate_po)
                    rate_usd = self.product.reposition_cost_currency.rate
                    reposition_cost = reposition_cost * (1 / rate_usd)
                else:
                    # reposition_cost /= sale_price_list_currency.rate_silent
                    rate_usd = self.product.list_price_currency.rate
                    sale_price_list = sale_price_list * (1 / rate_usd)
                    # cost *= rate_po
                # result = abs((price / cost) - 1)
            result = (sale_price_list / reposition_cost) - 1
            # return math.ceil(result * 100)  # Round 2 dec
            return result * 100
        except ZeroDivisionError:
            return result

    def calc_average_cost_percent(self, _price, _cost, _currency):
        price = _price # Precio de lista
        average_cost = _cost # costo promedio
        average_cost_currency = _currency  # Moneda costo promedio
        list_price_currency = self.product.list_price_currency  # Moneda costo venta
        company_currency_id = self.env.user.company_id.currency_id
        result = 0.00
        try:
            if average_cost_currency != list_price_currency:
                if average_cost_currency.id != company_currency_id.id:
                    # average_cost *= (1 / rate_po)
                    rate_usd = average_cost_currency.rate
                    price = price / (1 / rate_usd)
                else:
                    # average_cost /= list_price_currency.rate_silent
                    rate_usd = list_price_currency.rate
                    price = price * (1 / rate_usd) 
                # result = abs((price / average_cost) - 1)
            result = (price / average_cost) - 1
            # return math.ceil(result * 100)  # Round 2 dec
            return result * 100
        except ZeroDivisionError:
            return result

    def calc_cost_percent_last_po(self, _price, _cost, _currency):
        list_price = float(_price)  # precio de venta
        last_purchase_cost = float(_cost)  # costo de UC
        last_purchase_currency = _currency  # moneda de UC
        last_purchase_rate = self.get_last_po_rate(_currency=last_purchase_currency)  # tipo cambio ultima compra
        sale_currency = self.product.list_price_currency
        company_currency_id = self.env.user.company_id.currency_id
        # moneda venta
        result = 0.00
        try:
            if last_purchase_currency != sale_currency:
                if self.product.last_po_cost_currency_ro.id != company_currency_id.id:
                    list_price = list_price * last_purchase_rate
                else:
                    last_purchase_cost *= last_purchase_rate                
            result = (list_price / last_purchase_cost) - 1
            # return math.ceil(result * 100)  # Round 2 dec
            return result * 100
        except ZeroDivisionError:
            return result

    def get_last_po_rate(self, _currency):
        rate = 0.00
        currency_id = _currency.id
        try:
            product_id = self.product.product_variant_ids.id or \
                self.product._origin.product_variant_ids.id
        except:
            product_id = None
        if product_id:
            self.env.cr.execute(
                """
                SELECT ai.currency_rate_alter
                FROM purchase_order_line pol
                JOIN purchase_order po
                    ON (pol.order_id = po.id)
                JOIN purchase_order_line_invoice_rel polir
                    ON (polir.order_line_id = pol.id)
                JOIN account_invoice_line ail
                    ON (ail.id = polir.invoice_id)
                JOIN account_invoice ai
                    ON (ai.id = ail.invoice_id)
                WHERE po.shipped = TRUE
                    AND pol.product_id = %s
                    AND pol.invoiced = TRUE
                ORDER BY pol.write_date DESC
                LIMIT 1;
                """,
                (product_id,)
            )

            if self.env.cr.rowcount:
                rate = [rate_alter[0]
                        for rate_alter in self.env.cr.fetchall()]
                rate = rate[0] or 0.00

            if rate == 0.00:
                self.env.cr.execute(
                    """
                    SELECT rate.rate
                    FROM purchase_order_line pol
                    JOIN purchase_order po
                        ON (pol.order_id = po.id)
                    JOIN purchase_order_line_invoice_rel polir
                    ON polir.order_line_id = pol.id
                    JOIN account_invoice_line ail
                        ON (ail.id = polir.invoice_id)
                    JOIN account_invoice ai
                        ON (ai.id = ail.invoice_id)
                    JOIN account_move am
                        ON (am.id = ai.move_id)
                    JOIN stock_picking sp
                    ON sp.id = ai.picking_dev_id
                    JOIN res_currency_rate rate
                        ON (rate.id = (
                                SELECT id FROM res_currency_rate rcr
                                    WHERE rcr.currency_id = CAST(%s AS integer)
                                        AND rcr.name <= sp.date_done
                                        AND rcr.rate > 0
                                    ORDER BY name DESC
                                    LIMIT 1
                            )
                        )
                    WHERE po.shipped = TRUE
                        AND pol.product_id = CAST(%s AS integer)
                        AND pol.invoiced = TRUE
                    ORDER BY pol.write_date DESC
                    LIMIT 1;
                    """,
                    (currency_id, product_id))
                if self.env.cr.rowcount:
                    rate = [rate_alter[0]
                            for rate_alter in self.env.cr.fetchall()]
                    rate = rate[0] or 0.00
        return rate

    """@api.model
    def create(self, vals):
        if self.price > 0:
            reposition_cost, average_cost, last_po_cost = self.calc_cost()
            vals['reposition_cost'] = reposition_cost
            vals['average_cost'] = average_cost
            vals['last_po_cost'] = last_po_cost
        return super(product_price_level, self).create(vals)

    @api.multi
    def write(self, vals):
        if self.price > 0:
            reposition_cost, average_cost, last_po_cost = self.calc_cost()
            vals['reposition_cost'] = reposition_cost
            vals['average_cost'] = average_cost
            vals['last_po_cost'] = last_po_cost
        return super(product_price_level, self).write(vals)"""
