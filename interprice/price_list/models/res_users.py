# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields


class product_price_level(models.Model):
    """Price level addon to users (sales executive)"""

    # odoo model properties
    _inherit = "res.users"

    # custom fields
    price_level = fields.Many2one(
        comodel_name="price.level",
        help="Level",
        string="Level",
    )

    under_cost_price = fields.Boolean(
        default=False,
        help="Under cost price",
        string="Under cost price",
    )
