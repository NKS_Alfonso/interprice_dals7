# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, _, exceptions, api


class product_category(models.Model):
    """Extended model to add type to the product categories"""

    # odoo model properties
    _inherit = 'product.category'

    # Methods
    types = fields.Many2many(
        comodel_name='type.product',
        relation='product_category_type_rel',
        column1='product_category',
        column2='type_product',
        help="Type",
        string="Type",
        required=True
    )

    @api.multi
    def unlink(self):
        self.env.cr.execute(
            """
            SELECT EXISTS(
                SELECT 1 FROM product_template pt
                    WHERE pt.line=%s
                    OR pt.brand=%s
                    OR pt.serial=%s
                    OR pt.section = %s
            );
            """,
            (self.id, self.id, self.id, self.id)
        )
        if self.env.cr.rowcount:
            exists = [res[0] for res in self.env.cr.fetchall()]
            exists = exists[0]
            if exists:
                raise exceptions.Warning(
                    _("Can't delete product category(ies)\n"
                        "Category currently used on product(s).")
                )
        return super(product_category, self).unlink()


class type_product(models.Model):
    """Type category model"""

    # odoo model properties
    _name = 'type.product'
    _rec_name = 'type'
    _sql_constraints = [
        ('type_uniq', 'unique(type)',
            _("Can't be duplicate value for this field!"))
    ]

    # Methods
    type = fields.Char(
        help="Type",
        required=True,
        string="Type",
    )
