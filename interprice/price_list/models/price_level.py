# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, _


class product_price_level(models.Model):
    """Price level model"""

    # odoo model properties
    _name = "price.level"
    _rec_name = 'level_id'
    _sql_constraints = [
        ('level_id_uniq', 'unique(level_id)',
            _("Can't be duplicate value for this field!"))
    ]

    # custom fields
    level_id = fields.Integer(
        help="Level id",
        required=True,
        string="Level id",
    )
