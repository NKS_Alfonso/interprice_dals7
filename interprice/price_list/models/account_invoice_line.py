# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models


class account_invoice_line(models.Model):
    """Extended model to retrieve price list on Sales Order lines"""

    # odoo model properties
    _inherit = 'account.invoice.line'

    # Methods
    def calc_price_unit(self, cr, uid, ids,
                        client_id=None,
                        product_id=None,
                        product_code="",
                        currency_id=None,
                        currency_name="",
                        context=None):
        """Retrieves price_list price from product"""
        res = {'value': {}}
        product = self.pool.get('product.product').browse(cr, uid, product_id)
        if product:
            prod_price = product.product_tmpl_id.calc_price_unit(
                client_ids=client_id,
                product_ids=product_id,
                # product_code,
                currency_ids=currency_id,
                # currency_name
            ).get(product.default_code, False)
            if prod_price:
                res['value']['price_unit'] = prod_price.get('price', 0.00)
        return res
