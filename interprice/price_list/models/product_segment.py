# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, _, exceptions


class ProductSegment(models.Model):
    """Model for segments, this model is used for
    segment product price catalog"""

    # odoo model properties
    _name = 'product.segment'
    _inherit = ['mail.thread']
    _rec_name = 'name'
    _sql_constraints = [
        ('name_uniq', 'unique(name)',
            _("Can't be duplicate value for this field!"))
    ]

    name = fields.Char(
        copy=False,
        help="Segment name",
        required=True,
        string="Name",
    )

    @api.multi
    def unlink(self):
        self.env.cr.execute(
            """
            SELECT EXISTS(
                SELECT 1 FROM res_partner rp
                    WHERE rp.client_segment=%s
            );
            """,
            (self.id,)
        )
        if self.env.cr.rowcount:
            exists = [res[0] for res in self.env.cr.fetchall()]
            exists = exists[0]
            if exists:
                raise exceptions.Warning(
                    _("Can't delete segment(s)\n"
                        "Segment(s) currently assigned to client(s).")
                )

        self.env.cr.execute(
            """
            SELECT EXISTS(
                SELECT 1 FROM price_list pl
                    WHERE pl.segment=%s
            );
            """,
            (self.id,)
        )
        if self.env.cr.rowcount:
            exists = [res[0] for res in self.env.cr.fetchall()]
            exists = exists[0]
            if exists:
                raise exceptions.Warning(
                    _("Can't delete segment(s)\n"
                        "Segment(s) currently assigned to price list(s).")
                )
        return super(ProductSegment, self).unlink()
