# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class CancelButton(models.TransientModel):
    _name = 'price.protection.cancel'

    reason = fields.Text(
        string="Reason",
        required=True
    )

    @api.one
    def save_reason(self):
        active_id = self.env.context.get('active_id')
        claim = self.env['price.protection.claim'].search(
            [('id', '=', active_id)]
        )
        claim.update({
            'comment': _('Cancellation reason: \n') + self.reason
        })
        claim.set_workflow('cancelled')
