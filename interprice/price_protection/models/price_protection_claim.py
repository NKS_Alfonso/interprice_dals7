# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved

from openerp import models, fields, api, _, exceptions
from openerp.tools.misc import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

states_list = [
    ('draft', _("Draft")),
    ('generated', _("Generated")),
    ('assigned', _("Assigned")),
    ('applied', _("Applied")),
    ('cancelled', _("Cancelled")),
]


class PriceProtectionClaim(models.Model):
    _name = 'price.protection.claim'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'folio'

    state = fields.Selection(string=_("State"), track_visibility="always", copy=False, default="draft",
                             selection=states_list)

    folio = fields.Char(string=_("Folio"), readonly=True, default='_')
    supplier_id = fields.Many2one(string=_("Supply"), comodel_name='res.partner', domain=[('supplier', '=', True)],
                                  required=True)
    date = fields.Datetime(string=_("Date to move"), default=lambda self: fields.datetime.now(), required=True,
                           help="Date time create transaction")
    product_ids = fields.One2many('price.protection.claim.details', 'parent_price_protection_claim_id', 'Products')

    amount_untaxed = fields.Float(string=_("Amount"), compute="cal_amount_and_tax_total", store=True)
    amount_tax = fields.Float(string=_("Tax"), compute="cal_amount_and_tax_total", store=True)
    amount_total = fields.Float(string=_("Total"), compute="cal_amount_and_tax_total", store=True,
                                track_visibility="always")

    invoice_count = fields.Integer(compute="_compute_invoice_count")
    down_price = fields.Datetime(string=_("Down price"), default=lambda self: fields.datetime.now(), required=True)
    # amount_total_usd = fields.Float(string=_("Total usd"), compute="cal_amount_and_tax_total", store=True,
    #                                track_visibility="always")

    currency_id = fields.Many2one(comodel_name='res.currency', string=_("Currency"), required=True)
    supplier_folio = fields.Char(string=_("Supplier folio"), )
    supplier_amount = fields.Char(string=_("Supplier amount"), )
    # account_invoice_id = fields.Many2one(comodel_name='account.invoice', string=_("Refund"))
    supplier_currency_id = fields.Many2one(comodel_name='res.currency', string=_("Supplier Currency"))

    # Log data

    user_assigned = fields.Many2one(comodel_name='res.users')
    user_cancelled = fields.Many2one(comodel_name='res.users')
    datetime_cancelled = fields.Datetime()
    comment = fields.Text(string=_("Comment"))
    user_created = fields.Many2one(comodel_name='res.users')
    user_generated = fields.Many2one(comodel_name='res.users')
    datetime_generated = fields.Datetime()
    user_applied = fields.Many2one(comodel_name='res.users')
    datetime_applied = fields.Datetime()

    @api.multi
    @api.depends('product_ids')
    def cal_amount_and_tax_total(self):
        for row in self.product_ids:
            # if row.currency_name == 'USD':
            #    self.amount_untaxed_usd += row.amount
            #    self.amount_tax_usd += row.tax
            # else:  # MXN
            self.amount_untaxed += row.amount
            self.amount_tax += row.tax
        self.amount_total += self.amount_untaxed + self.amount_tax
        # self.amount_total_usd += self.amount_untaxed_usd + self.amount_tax_usd

    @api.one
    def button_generate(self):
        self.set_workflow('generated')
        self.folio = self.env['ir.sequence'].get('price.protection')

    @api.one
    def button_assign(self):
        self.set_workflow('assigned')

    @api.multi
    def button_cancel(self):
        wizard_form = self.env.ref(
            'price_protection.button_cancel_popup',
            False
        )
        return {
            'name': 'Reason?',
            'type': 'ir.actions.act_window',
            'res_model': 'price.protection.cancel',
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }
        # self.set_workflow('cancelled')

    @api.one
    def button_apply(self):
        if self.create_account_invoice():
            self.set_workflow('applied')
            self.cal_avg_cost()

    @api.one
    def set_workflow(self, _state):
        self.validation(kind='at_least_one_product')
        self.validation(kind='products_same_currency')
        if _state == 'generated':
            self.user_generated = self.env.user
            self.datetime_generated = fields.datetime.now()
        elif _state == 'assigned':
            self.user_assigned = self.env.user
        elif _state == 'applied':
            self.user_applied = self.env.user
            self.datetime_applied = fields.datetime.now()
        elif _state == 'cancelled':
            self.user_cancelled = self.env.user
            self.datetime_cancelled = fields.datetime.now()

        self.state = _state

    def get_journal_id(self):
        _currency = self.currency_id.id
        if self.currency_id.name != 'USD':
            _currency = None
        _journal = self.env['account.journal'].search([('type', '=', 'purchase_refund'), ('currency', '=', _currency)])
        if len(_journal) > 1:
            _journal_id = _journal[0]
        elif len(_journal) == 1:
            _journal_id = _journal
        else:
            raise exceptions.MissingError(
                _("Not found journal type:%s with currency:" % ('purchase_refund', self.currency_id.name)))
        _account_id = _journal_id.default_credit_account_id.id or self.supplier_id.property_account_payable.id
        if not _account_id:
            raise exceptions.MissingError(_("Please check account payable customer or journal account."))
        return _journal_id.id, _account_id

    def get_account_cost_center_id(self):
        _account_cost_center_id = 0
        _account_cost_center = self.env.user.centro_costo_id
        if len(_account_cost_center) > 1:
            _account_cost_center_id = _account_cost_center[0].id
        elif len(_account_cost_center) == 1:
            _account_cost_center_id = _account_cost_center.id
        else:
            raise exceptions.MissingError(_("Please define account cost center of the user"))
        return _account_cost_center_id

    def get_account_invoice_lines(self):
        _invoice_lines = []
        for row in self.product_ids:
            _line = [0, False, {
                'asset_category_id': False,
                'product_id': row.product_id,
                'price_unit': row.diff_cost,
                'account_id': row.product_id.categ_id.property_account_expense_categ.id or False,
                'invoice_line_tax_id': [[6, False, [row.tax_id.id]]],
                'discount': 0,
                'account_analytic_id': False,
                'quantity': row.stock,
                'name': row.product_id.name,
                'uos_id': row.product_id.uom_id.id
            }]
            _invoice_lines.append(_line)

        return _invoice_lines

    @api.one
    def create_account_invoice(self):
        # make refund
        invoice_datetime = fields.datetime.today()
        journal_id, account_id = self.get_journal_id()

        account_invoice = {
            'origin': self.folio or False,
            'comment': False,
            'date_due': invoice_datetime.date().strftime(DEFAULT_SERVER_DATE_FORMAT),
            'check_total': 0,
            'reference': False,
            'supplier_invoice_number': self.supplier_folio or False,
            'company_id': self.env.user.company_id.id or 1,
            'currency_id': self.currency_id.id,
            # 'acc_payment': 3,
            'tax_line': [],
            'message_ids': False,
            'hide_currency_rate_alter_field': False,
            'fiscal_position': False,
            'user_id': self.env.user.id,
            # 'partner_bank_id': 3,
            'partner_id': self.supplier_id.id,
            'payment_mode_id': False,
            'reference_type': 'none',
            'journal_id': journal_id,
            'pay_method_id': False,
            'invoice_line': [],  # self.get_account_invoice_lines(),
            'currency_rate_alter': 0,
            'account_id': account_id,
            'date_invoice': invoice_datetime.date().strftime(DEFAULT_SERVER_DATE_FORMAT),
            'invoice_datetime': invoice_datetime.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
            'period_id': False,
            'message_follower_ids': False,
            'name': False,
            'cost_center_id': self.get_account_cost_center_id(),
            'payment_term': self.supplier_id.property_payment_term.id,
            'type': 'in_refund'
        }
        account_invoice_id = self.env['account.invoice'].create(vals=account_invoice)
        account_invoice_id.onchange_partner_id(type=account_invoice.get('in_refund'),
                                               partner_id=account_invoice.get('partner_id'),
                                               date_invoice=account_invoice.get('date_invoice'),
                                               payment_term=account_invoice.get('payment_term'),
                                               partner_bank_id=False, company_id=account_invoice.get('company_id'))
        account_invoice_id.onchange_journal_id(journal_id=account_invoice.get('journal_id'))
        account_invoice_id.update({'invoice_line': self.get_account_invoice_lines()})
        account_invoice_id.button_reset_taxes()
        self.cal_avg_cost()
        return True

    @api.model
    def create(self, vals):
        if type(vals) is dict:
            vals.update({'user_created': self.env.user.id})
            self.validation(kind='at_least_one_product_at_create', params=vals)
        return super(PriceProtectionClaim, self).create(vals)

    def _compute_invoice_count(self):
        for r in self:
            r.invoice_count = self.env['account.invoice'].search_count([('origin', '=', r.folio),
                                                                        ('partner_id', '=', r.supplier_id.id)])

    @api.multi
    def invoice_open(self):
        res = self.env['ir.model.data'].get_object_reference('account', 'invoice_supplier_form')
        res_id = res and res[1] or False
        inv_ids = self.env['account.invoice'].search([('origin', '=', self.folio),
                                                      ('partner_id', '=', self.supplier_id.id)], limit=1) or []
        return {
            'name': _('Supplier Invoices'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.invoice',
            'context': "{'type':'in_refund', 'journal_type': 'purchase'}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': inv_ids and inv_ids[0].id or False,
        }

    # @api.one
    def validation(self, kind, params=None):
        if kind == 'at_least_one_product':
            if len(self.product_ids) == 0:
                raise exceptions.ValidationError(msg=_("In this claim there not any products, Please add one."))
            product_list = [p.product_id.id for p in self.product_ids]
            self.validation(kind='products_no_duplicate', params=product_list)
        elif kind == 'at_least_one_product_at_create':
            if not params.get('product_ids', False) and not len(params.get('product_ids', [])):
                raise exceptions.ValidationError(msg=_("In this claim there not any products, Please add one."))
            product_list = [p[2].get('product_id') for p in params.get('product_ids')]
            self.validation(kind='products_no_duplicate', params=product_list)
        elif kind == 'products_same_currency':
            for row in self.product_ids:
                if self.currency_id.id != row.currency_id.id:
                    raise exceptions.ValidationError(
                        _("Not is permitted combine two currencies in product lines with the base currency."))
        elif kind == 'products_no_duplicate':
            for p in params:
                if params.count(p) > 1:
                    raise exceptions.ValidationError(_("Not is permitted add two product same."))

    @api.one
    def cal_avg_cost(self):
        product_tmpl = self.pool.get('product.template')
        for p in self.product_ids:
            avg = p.product_id.product_tmpl_id.average_cost
            # avg_currency = p.product_templ_id.average_cost_currency_ro
            stock = p.stock
            amount = avg * stock
            new_cost = p.diff_cost
            if p.currency_id.name != 'MXN':
                qry_get_rate = """ select get_rate('purchase', 'USD', '{}'); """.format(self.date)
                self.env.cr.execute(qry_get_rate)
                cr_data = self.env.cr.fetchone() or [0]
                new_cost = cr_data[0] * new_cost
            new_amount = new_cost * stock
            diff_amount_and_new_amount = amount - new_amount
            new_avg = abs(diff_amount_and_new_amount / stock)
            print 'Current avg', avg, 'New avg', new_avg
            # p.product_id.standard_price = new_avg
            #Call wizard function here
            # Old api function...
            product_tmpl.do_change_standard_price(
                self._cr,
                self.env.uid,
                [p.product_id.product_tmpl_id.id],
                new_avg,
                self.env.context
            )
