# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved

from openerp import models, fields, api, _
import openerp.tools as tools


class PriceProtectionProduct(models.Model):
    _name = 'price.protection.product'
    _table = 'price_protection_product'
    _auto = False
    _order = 'id desc'
    _rec_name = 'product_name'

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'price_protection_product')

        cr.execute("""
        create or replace view price_protection_product as (
  SELECT
    row_number()
    over (
      order by t2.product_id ) id,
    t2.*
  from (
         select
           pt.id                       product_templ_id,
           pt.name                     product_name,
           pp.id             product_id,
           pt.reposition_cost_currency currency_id,
           rc.name                     currency_name,
           ps.name                     partner_id,
           rp.name                     partner_name,
           pq.qty,
           pq.reservation_qty
         from product_template pt
           inner join product_product pp on pt.id = pp.product_tmpl_id
           inner join product_supplierinfo ps on pt.id = ps.product_tmpl_id
           inner join res_currency rc on pt.reposition_cost_currency = rc.id
           inner join res_partner rp on ps.name = rp.id
           inner join (
                        select
                          t1.sp_product_id,
                          sum(t1.sp_product_qty)             qty,
                          sum(t1.sp_product_reservation_qty) reservation_qty
                        from (
                               select
                                 sq.product_id sp_product_id,
                                 sum(sq.qty)   sp_product_qty,
                                 0             sp_product_reservation_qty
                               from stock_warehouse sw inner join stock_location sl on sw.lot_stock_id = sl.id
                                 inner join stock_warehouse_type swt
                                   on sw.stock_warehouse_type_id = swt.id and swt.code = 'fac'
                                 inner join stock_quant sq on sl.id = sq.location_id and sq.reservation_id is null
                               where sl.usage = 'internal'
                               group by sq.product_id
                               union all
                               select
                                 sq.product_id sp_product_id,
                                 0             sp_product_qty,
                                 sum(sq.qty)   sp_product_reservation_qty
                               from stock_warehouse sw inner join stock_location sl on sw.lot_stock_id = sl.id
                                 inner join stock_warehouse_type swt
                                   on sw.stock_warehouse_type_id = swt.id and swt.code = 'fac'
                                 inner join stock_quant sq on sl.id = sq.location_id and sq.reservation_id is not null
                               where sl.usage = 'internal'
                               group by sq.product_id

                             ) t1
                        group by t1.sp_product_id) pq on pt.id = pq.sp_product_id) t2
);
        """)

    product_id = fields.Many2one(comodel_name='product.product')
    product_templ_id = fields.Many2one(comodel_name='product.template')
    product_name = fields.Char(related='product_templ_id.name')
    #product_code = fields.Char()
    currency_id = fields.Many2one(comodel_name='res.currency')
    currency_name = fields.Char(related='currency_id.name')
    partner_id = fields.Many2one(comodel_name='res.partner')
    #partner_name = fields.Char(related='partner_id.name')
    qty = fields.Float(string='Qty')
    reservation_qty = fields.Float(string='Reservation Qty')

    #display_name = fields.Char(compute='_display_name')

    @api.multi
    def _display_name(self):
        for row in self:
            row.display_name = "{code} - {name}".format(code=row.product_code, name=row.product_name)
