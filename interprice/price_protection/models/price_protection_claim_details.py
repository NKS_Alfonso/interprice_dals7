# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved

from openerp import models, fields, api, _, exceptions


class PriceProtectionClaimDetails(models.Model):
    _name = 'price.protection.claim.details'

    parent_price_protection_claim_id = fields.Many2one('price.protection.claim', 'Parent price protection claim',
                                                       ondelete='cascade')
    #price_protection_id = fields.Many2one(comodel_name='price.protection.product', string=_("Product"))
    product_id = fields.Many2one(comodel_name='product.product', string=_("Product "))
    currency_id = fields.Many2one(comodel_name='res.currency', string=_("Currency"))
    tax_id = fields.Many2one(comodel_name='account.tax', string=_("Account Tax"))
    available_qty = fields.Float(string=_("Available qty"), )
    reservation_qty = fields.Float(string=_("Reservation qty"))
    traffic = fields.Float(string=_("Traffic"))
    stock = fields.Float(string=_("Stock"), compute="sum_qty_available_reservation_traffic", store=True)

    last_cost = fields.Float(string=_("Last cost"))
    new_cost = fields.Float(string=_("New cost"))

    diff_cost = fields.Float(string=_("Diff cost"), compute='diff_cost_between_last_and_new', store=True)

    amount = fields.Float(_("Amount"), compute="cal_amount_and_tax", store=True)
    tax = fields.Float(_("Tax"), compute="cal_amount_and_tax", store=True)

    @api.multi
    @api.depends('available_qty', 'reservation_qty', 'traffic')
    def sum_qty_available_reservation_traffic(self):
        for row in self:
            row.stock = abs(row.available_qty) + abs(row.reservation_qty) + abs(row.traffic)

    @api.multi
    @api.depends('last_cost', 'new_cost')
    def diff_cost_between_last_and_new(self):
        for row in self:
            row.diff_cost = abs(abs(row.last_cost) - abs(row.new_cost))

    @api.multi
    @api.depends('diff_cost', 'stock')
    def cal_amount_and_tax(self):
        for row in self:
            row.amount = row.stock * row.diff_cost
            row.tax = row.amount * row.tax_id.amount or 0.16

    @api.onchange('product_id')
    def price_protection_onchange(self):
        if len(self.product_id):
            product = self.env['price.protection.product'].search([
                ('partner_id', '=', self._context.get('supplier_id', False)),
                ('product_id', '=', self.product_id.id),
                ('currency_id', '=', self._context.get('currency_id', False)),
            ])
            self.currency_id = product.currency_id
            if len(product.product_id.supplier_taxes_id) > 0:
                self.tax_id = product.product_id.supplier_taxes_id[0]
            else:
                self.tax_id = product.product_id.supplier_taxes_id
        #  self.product_id = self.price_protection_id.product_id
            self.available_qty = product.qty
            self.reservation_qty = product.reservation_qty
            self.get_last_cost(product.product_templ_id.id, self.currency_id.name)

        pp = self.env['price.protection.product'].search([
            ('partner_id', '=', self._context.get('supplier_id', False)),
            ('currency_id', '=', self._context.get('currency_id', False))
        ])
        pp_list = [row.product_id.id for row in pp]
        return{'domain': {'product_id': [('id', 'in', pp_list)]}}

    @api.one
    def get_last_cost(self, product_id, currency_name):
        qry_get_last_cost = """
        select
          po.name,
          --  po.date_approve, po.currency_id , 
          rc.name, pol.price_unit, po.currency_rate_alter,
          get_rate('purchase', 'USD', po.date_approve)
        from purchase_order po inner join purchase_order_line pol on po.id = pol.order_id and pol.invoiced =TRUE 
        and po.shipped = TRUE 
        inner join res_currency rc on po.currency_id = rc.id
        inner join product_product pp on pp.id = pol.product_id
        where pp.product_tmpl_id={} order by po.date_approve desc limit 1;
        """.format(int(product_id))
        self.env.cr.execute(qry_get_last_cost)
        new_last_cost = 0.0
        cr_data = self.env.cr.fetchone() or False
        if product_id is False or cr_data is False:
            return
        p_name, c_name, p_price_unit, c_rate_alter, c_rate = cr_data
        if str(currency_name) == 'USD':  # currency product
            if c_name == 'MXN':  # currency purchase
                if c_rate_alter > 0:
                    """
                            this section not should execute because 
                            when the MXN not is captured the rate. 
                    """
                    new_last_cost = p_price_unit * c_rate_alter
                else:
                    new_last_cost = p_price_unit * c_rate
            else:
                """
                    this section is when product has same currency and purchase
                    """
                new_last_cost = p_price_unit

        else:  # MXN
            if c_name == "USD":
                if c_rate_alter > 0:
                    new_last_cost = p_price_unit / c_rate_alter
                else:
                    new_last_cost = p_price_unit / c_rate or 1
            else:
                new_last_cost = p_price_unit
        print("new last price" + str(p_name) + "--" + str(new_last_cost))
        self.last_cost = new_last_cost