# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved

from openerp.osv import osv
from openerp import fields, _, api, exceptions
from openerp.tools.misc import DEFAULT_SERVER_DATE_FORMAT


class MultiClaimPp(osv.osv_memory):
    _name = 'multi.claim.pp'

    supplier_id = fields.Many2one(comodel_name='res.partner', string=_("Supplier"), required=True)

    product_from_id = fields.Many2one(comodel_name='product.product', string=_("Product"), required=True)
    product_to_id = fields.Many2one(comodel_name='product.product', string=_("Product"))

    @api.multi
    def action_apply_filter(self):
        product_f1 = " default_code >= '{}' and  default_code <= '{}'".format(self.product_from_id.default_code,
                                                                  self.product_to_id.default_code or
                                                                  self.product_from_id.default_code)
        qry_product = 'select id from product_product where {}'.format(product_f1)
        self.env.cr.execute(qry_product)
        product_ids = str([c1[0] for c1 in self.env.cr.fetchall() or []])[1:-1]
        qry_pp = 'select product_id, currency_id, qty, reservation_qty from price_protection_product ' \
                 'where partner_id = {} and product_id in ({})'.format(self.supplier_id.id, product_ids)
        self.env.cr.execute(qry_pp)
        claim_lines = {}
        for product_id, currency_id, qty, rqty in self.env.cr.fetchall() or []:
            line = [0, False, {
                'product_id': product_id,
                'reservation_qty': rqty,
                'currency_id': currency_id,
                'traffic': 0,
                'new_cost': 0,
                'last_cost': 0,
                'available_qty': qty,
                'tax_id': False
            }]
            if str(currency_id) in claim_lines:
                claim_lines.get(str(currency_id)).append(line)
            else:
                claim_lines.update({str(currency_id): [line]})
        ppc_ids = []
        if len(claim_lines):
            _date = fields.date.today().strftime(DEFAULT_SERVER_DATE_FORMAT)
            for claim_line_id in claim_lines:
                ppp_ids = claim_lines.get(claim_line_id)
                claim = {
                    'supplier_folio': False,
                    'supplier_id': self.supplier_id.id,
                    'supplier_amount': False,
                    'message_follower_ids': False,
                    'currency_id': int(claim_line_id),
                    'state': 'draft',
                    'product_ids': ppp_ids,
                    'date': _date,
                    'message_ids': False
                }
                ppc_id = self.env['price.protection.claim'].create(claim)
                for l in ppc_id.product_ids:
                    l.with_context(supplier_id=self.supplier_id.id,
                                   currency_id=int(claim_line_id)).price_protection_onchange()
                    l.sum_qty_available_reservation_traffic()
                    #l.diff_cost_between_last_and_new()
                    #l.cal_amount_and_tax()
                ppc_id.cal_amount_and_tax_total()
                ppc_ids.append(ppc_id.id)
        else:
            raise exceptions.MissingError(_("The filter apply not mach any record."))

        return {
            'name': _("Price protection claim"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'price.protection.claim',
            'target': 'current',
            'domain': [('id', 'in', ppc_ids)],
        }
