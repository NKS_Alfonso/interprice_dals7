# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved

from openerp.osv import osv
from openerp import fields, _, api, exceptions
from openerp.tools.misc import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT , DATETIME_FORMATS_MAP
from .price_protection_claim import states_list
from olib.oCsv.oCsv import OCsv
from datetime import datetime

class PriceProtectionClaimReport(osv.TransientModel):
    _name = 'price.protection.claim.report'

    supplier_id = fields.Many2one(comodel_name='res.partner', string=_("Supplier"))

    state = fields.Selection(string=_("State"), default="draft", selection=states_list)

    datetime_from = fields.Datetime(string=_("Datetime from"))
    datetime_to = fields.Datetime(string=_("Datetime to"))

    @api.multi
    def export_csv(self):
        rl = self.action_apply_filter()
        headers = [[_("Folio"),
                    _("Supplier number"),
                    _("Supplier name"),
                    _("Date to move"),
                    _("Currency"),
                    _("Rate"),
                    _("Product manager"),
                    _("User who assigned"),
                    _("Supplier folio"),
                    _("Supplier amount"),
                    _("Supplier currency"),
                    _("Down Price"),
                    _("Status"),
                    _("User who cancelled"),
                    _("Date who cancelled"),
                    _("Comment"),
                    _("User who created"),
                    _("User who generated"),
                    _("Date who generated"),
                    _("User who applied"),
                    _("Date who applied")]]
        rows = headers
        for r in rl:
            self.env.cr.execute("select get_rate('purchase', 'USD', '{}') ".format(r.date))
            rate = self.env.cr.fetchone() or 0

            rows.append([r.folio,
                         r.supplier_id.supplier_number,
                         r.supplier_id.name,
                         self.fmt_datetime(r.date) if r.date else '',
                         r.currency_id.name,
                         rate[0] if type(rate) is tuple else 0,
                         r.supplier_id.user_id.name or '',
                         r.user_assigned.name if r.user_assigned else '',
                         r.supplier_folio or '',
                         r.supplier_amount or 1.0,
                         r.supplier_currency_id.name if r.supplier_currency_id else '',
                         self.fmt_datetime(r.down_price) if r.down_price else '',
                         r.state,
                         r.user_cancelled.name if r.user_cancelled else '',
                         self.fmt_datetime(r.datetime_cancelled) if r.datetime_cancelled else '',
                         r.comment or '',
                         r.user_created.name if r.user_created else '',
                         r.user_generated.name if r.user_generated else '',
                         self.fmt_datetime(r.datetime_generated) if r.datetime_generated else '',
                         r.user_applied.name if r.user_applied else '',
                         self.fmt_datetime(r.datetime_applied) if r.datetime_applied else '',
                         ])
        obj_osv = OCsv()
        _file = obj_osv.csv_base64(headers)
        download_id = self.pool['r.download'].create(cr=self.env.cr, uid=self.env.uid, vals={
            'file_name': 'ppc-{}.csv'.format(fields.datetime.today().strftime(DEFAULT_SERVER_DATE_FORMAT)),
            'type_file': 'csv',
            'file': _file,
        })
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % download_id,
            'target': 'self',
        }

    @api.multi
    def show_screen(self):
        rl = self.action_apply_filter()
        ids = [r.id for r in rl]
        return {
            'name': _("Price protection claim"),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'price.protection.claim',
            'target': 'current',
            'domain': [('id', 'in', ids)],
        }

    # @api.multi
    def action_apply_filter(self):
        f0 = []
        if self.datetime_from:
            f0.append(('date', '>=', self.datetime_from))
        if self.datetime_to:
            f0.append(('date', '<=', self.datetime_to))
        if self.supplier_id:
            f0.append(('supplier_id', '=', self.supplier_id.id))
        if self.state:
            f0.append(('state', '=', self.state))

        results_list = self.env['price.protection.claim'].search(f0)
        if len(results_list):
            pass
        else:
            raise exceptions.MissingError(_("Does not match any record, Please apply new filters."))
        return results_list

    def fmt_datetime(self, _datetime):
        dt = fields.Datetime.context_timestamp(self, fields.Datetime.from_string(_datetime))

        return dt.strftime('%m-%d-%Y '+DATETIME_FORMATS_MAP['%T'])
