# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved

{
    'name': "Price protection",

    'summary': """
        Process is used to claim price whit partner and change average price
        without update product stock ...
    """,

    'author': "Yonn Xyz",
    'website': "https://yonn.xyz/d",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Purchase',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'purchase', 'account', 'price_list'],

    # always loaded
    'data': [
        'data/groups.xml',
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/menus.xml',
        'views/price_protection_view.xml',
        'views/multi_claim_pp_wizard.xml',
        'views/price_protection_claim_report_wizard.xml',
        'wizard/button_cancel.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
      #  'demo.xml',
    ],
}
