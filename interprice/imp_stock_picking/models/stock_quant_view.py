# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, tools


class ViewStockQuant(models.Model):
    """View stock quant"""

    # odoo model properties
    _name = 'view.stock.quant'
    _table = 'view_stock_quant'
    _description = 'View stock quant'
    _auto = False  # Don't let odoo create table
    _order = 'id ASC'

    # initial methods

    # Selection list

    # View fields
    view_stock_move = fields.Many2one(
        comodel_name='view.stock.move',
        readonly=True,
        string=_('View Stock Move')
    )
    move_id = fields.Many2one(
        comodel_name='stock.move',
        readonly=True,
        string=_("Reserved for movement"),
    )
    product = fields.Many2one(
        comodel_name='product.product',
        readonly=True,
        string=_("Product"),
    )
    quantity = fields.Float(
        readonly=True,
        string=_("Quantity"),
    )
    location = fields.Many2one(
        comodel_name='stock.location',
        readonly=True,
        string=_("Location"),
    )
    lot_id = fields.Many2one(
        comodel_name='stock.production.lot',
        readonly=True,
        string=_("Lot"),
    )
    in_date = fields.Datetime(
        readonly=True,
        string=_("In Date"),
    )
    product_cost = fields.Float(
        readonly=True,
        string=_("Value of the inventory"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_view_stock_quant(cr):
        tools.sql.drop_view_if_exists(cr, 'view_stock_quant')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW view_stock_quant AS (
                SELECT
                    id,
                    view_stock_move,
                    move_id,
                    product,
                    quantity,
                    location,
                    lot_id,
                    in_date,
                    product_cost,
                    reservation_id
                FROM (
                    SELECT
                        SQMR.quant_id AS id,
                        SQMR.move_id AS view_stock_move,
                        SQMR.move_id AS move_id,
                        SQ.product_id AS product,
                        SQ.qty AS quantity,
                        SQ.location_id AS location,
                        SQ.lot_id AS lot_id,
                        SQ.in_date AS in_date,
                        SQ.cost AS product_cost,
                        SQ.reservation_id AS reservation_id
                    FROM
                        stock_quant AS SQ
                        INNER JOIN stock_quant_move_rel AS SQMR
                        ON SQMR.quant_id = SQ.id
                    ) AS VSQ
            );
            """
        )
