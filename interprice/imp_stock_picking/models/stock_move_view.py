# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, tools, fields, api, _
from .stock_quant_view import ViewStockQuant
from .stock_pack_operation_view import ViewStockPackOperation


class ViewStockMove(models.Model):
    """View stock move"""

    # odoo model properties
    _name = 'view.stock.move'
    _table = 'view_stock_move'
    _description = 'View stock move'
    _auto = False  # Don't let odoo create table
    _order = 'id ASC'

    # Selection list
    _selection_list = [
        ('draft', _('New')),
        ('cancel', _('Cancel')),
        ('waiting', _('Waiting for another movement')),
        ('confirmed', _('Waiting for availability')),
        ('assigned', _('Availability')),
        ('done', _('Done')),
    ]

    # Initial method
    @api.one
    def get_stock_quant_ids(self):
        values = []
        if self.id and self.state == 'done':
            self.env.cr.execute(
                """
                    Select id from view_stock_quant Where move_id = %s
                """, (self.id,)
            )
        if self.env.cr.rowcount:
            res = self.env.cr.fetchall()
            for quant_id in res:
                values.append(quant_id[0])
        self.stock_quant_ids = values

    @api.one
    def get_stock_quant_reserved_ids(self):
        values = []
        if self.id and self.state != 'done':
            self.env.cr.execute(
                """
                    Select id from view_stock_quant Where reservation_id = %s
                """, (self.id,)
            )
        if self.env.cr.rowcount:
            res = self.env.cr.fetchall()
            for quant_id in res:
                values.append(quant_id[0])
        self.stock_quant_reserved_ids = values

    # View fields
    product = fields.Many2one(
        comodel_name='product.product',
        readonly=True,
        string=_("Product"),
    )
    quantity = fields.Integer(
        readonly=True,
        string=_("Quantity"),
    )
    product_uom = fields.Many2one(
        comodel_name='product.uom',
        readonly=True,
        string=_("Unit of measurement"),
    )
    destination_location = fields.Many2one(
        comodel_name='stock.location',
        readonly=True,
        string=_("Destination location"),
    )
    origin_location = fields.Many2one(
        comodel_name='stock.location',
        readonly=True,
        string=_("Origin location"),
    )
    availability = fields.Text(
        readonly=True,
        string=_("Availability"),
    )
    state = fields.Selection(
        selection=_selection_list,
        readonly=True,
        string=_("State"),
    )
    picking_id = fields.Many2one(
        comodel_name='stock.picking',
        readonly=True,
        string=_("Delivery order"),
    )
    stock_quant_ids = fields.One2many(
        comodel_name='view.stock.quant',
        inverse_name='view_stock_move',
        compute=get_stock_quant_ids,
        string=_("Quants moved"),
        readonly=True,
    )
    stock_quant_reserved_ids = fields.One2many(
        comodel_name='view.stock.quant',
        inverse_name='view_stock_move',
        compute=get_stock_quant_reserved_ids,
        string=_("Quants moved"),
        readonly=True,
    )

    # Create PostgreSQL view
    def init(self, cr):
        # Drop view
        tools.sql.drop_view_if_exists(cr, 'view_stock_move')
        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW view_stock_move AS (
                SELECT
                    id,
                    product,
                    quantity,
                    product_uom,
                    destination_location,
                    origin_location,
                    availability,
                    state,
                    picking_id
                FROM (
                    SELECT
                        SM.id AS id,
                        PP.id AS product,
                        SM.product_uom_qty AS quantity,
                        PUOM.id AS product_uom,
                        SLD.id AS destination_location,
                        SLO.id AS origin_location,
                        '' AS availability,
                        SM.state AS state,
                        SP.id AS picking_id
                    FROM
                        stock_move AS SM
                        INNER JOIN stock_picking AS SP ON SP.id = SM.picking_id
                        INNER JOIN product_product AS PP ON SM.product_id = PP.id
                        INNER JOIN product_uom AS PUOM ON SM.product_uom = PUOM.id
                        INNER JOIN stock_location AS SLO ON SM.location_id = SLO.id
                        INNER JOIN stock_location AS SLD ON SM.location_dest_id = SLD.id
                    ) AS VSM
            );
        """
        )
        ViewStockQuant.create_view_stock_quant(cr)
        ViewStockPackOperation.create_view_stock_pack_operation(cr)
