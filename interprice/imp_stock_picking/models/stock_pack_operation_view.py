# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, tools


class ViewStockPackOperation(models.Model):
    """View stock quant"""

    # odoo model properties
    _name = 'view.stock.pack.operation'
    _table = 'view_stock_pack_operation'
    _description = 'View stock pack operation'
    _auto = False  # Don't let odoo create table
    _order = 'id ASC'

    # initial methods

    # Selection list

    # View fields
    product_id = fields.Many2one(
        comodel_name='product.product',
        readonly=True,
        string=_("Product"),
    )
    product_uom_id = fields.Many2one(
        comodel_name='product.uom',
        readonly=True,
        string=_("Unit of measurement"),
    )
    lot_id = fields.Many2one(
        comodel_name='stock.production.lot',
        readonly=True,
        string=_("Lot"),
    )
    location_id = fields.Many2one(
        comodel_name='stock.location',
        readonly=True,
        string=_("Origin location"),
    )
    location_dest_id = fields.Many2one(
        comodel_name='stock.location',
        readonly=True,
        string=_("Destination location"),
    )
    picking_id = fields.Many2one(
        comodel_name='stock.picking',
        readonly=True,
        string=_("Delivery order"),
    )
    product_qty = fields.Integer(
        readonly=True,
        string=_("Quantity"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_view_stock_pack_operation(cr):
        tools.sql.drop_view_if_exists(cr, 'view_stock_pack_operation')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW view_stock_pack_operation AS (
                SELECT
                    id,
                    product_id,
                    product_uom_id,
                    lot_id,
                    location_id,
                    location_dest_id,
                    picking_id,
                    product_qty
                FROM (
                    SELECT
                        SPO.id AS id,
                        SPO.product_id AS product_id,
                        SPO.product_uom_id AS product_uom_id,
                        SPO.lot_id AS lot_id,
                        SPO.location_id AS location_id,
                        SPO.location_dest_id AS location_dest_id,
                        SPO.picking_id AS picking_id,
                        SPO.product_qty AS product_qty
                    FROM
                        stock_pack_operation AS SPO
                    ) AS VSQ
            );
            """
        )
