# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': 'Improvement of performance in transfer',

    'summary': "Improvement of performance in transfer of warehouse.",
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    'category': 'Warehouse',
    'version': '1.0',

    'depends': [
        'stock'
    ],
    'data': [
        'views/stock_move_view.xml',
        'views/stock_quant_view.xml',
        'views/stock_pack_operation_view.xml',
        'views/stock_picking_view.xml',
    ],
    'installable': True,
}
