#   -*- coding: utf-8 -*-
#   Copyright (C) 2016 Grupo Vadeto Developers
#   Author(s): jose.bautista@gvadeto.com
from openerp import models, fields, api
import time
from openerp.osv import osv, fields as fieldsv7


class ResCurrencyRate(models.Model):
    _inherit = 'res.currency.rate'

    rate_sale = fields.Float('TC Venta', digits=(12, 6), help='The rate of the currency to the currency of rate 1')


class ResCurrency(osv.osv):
    _inherit = 'res.currency'

    def _current_rate_sale(self, cr, uid, ids,name, arg, context=None):
        return self._get_current_rate_sale(cr, uid, ids, context=context)

    def _current_rate_sale_silent(self, cr, uid, ids, name, arg,context=None):
        return self._get_current_rate_sale(cr, uid, ids, raise_on_no_rate=False, context=context)

    def _get_current_rate_sale(self, cr, uid, ids, raise_on_no_rate=True, context=None):

        if context is None:
            context = {}
        res = {}

        date = context.get('date') or time.strftime('%Y-%m-%d')
        for id in ids:
            cr.execute("""SELECT rate_sale FROM res_currency_rate
                       WHERE currency_id = %s
                       AND to_date(to_char(name,'YYYY/MM/DD'), 'YYYY/MM/DD') <= %s
                       ORDER BY name desc LIMIT 1""",
                       (id, date))
            if cr.rowcount:
                res[id] = cr.fetchone()[0]
            elif not raise_on_no_rate:
                res[id]= 0
            else:res[id]= 0.00
            # currency = self.browse(cr, uid, id, context=context)
            # raise osv.except_osv(_('Error!'),_("No currency rate associated for currency '%s' for the given period" % (currency.name)))
        return res
    _columns = {
        'rate_sale': fieldsv7.function(_current_rate_sale, string='Current Rate', digits=(12, 6),
                                      help='The rate of the currency to the currency of rate 1.'),
        'rate_sale_silent': fieldsv7.function(_current_rate_sale_silent, string='Current Rate', digits=(12, 6),
                                       help='The rate of the currency to the currency of rate 1 (0 if no rate defined).')
    }

    # def recompute_payment_rate(self, cr, uid, ids, vals, currency_id, date, ttype, journal_id, amount, context=None):
    #     super(ResCurrency, self).recompute_payment_rate()