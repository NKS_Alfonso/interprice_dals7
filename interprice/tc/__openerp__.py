#   -*- coding: utf-8 -*-
#   Copyright (C) 2016 Grupo Vadeto Developers
#   Author(s): jose.bautista@gvadeto.com

{
    'name': "Tipo de cambio de ventas",
    'description': """
        Se agrego una nueva columna de tipo de cambio de ventas,

    """,
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': "Accounting",
    'version': "1.1",
    'depends': [
        'base',
    ],
    'data': [
        'res_currency_view.xml',

    ],
    'demo': [

    ],
    'css': [

    ],
    'js': [

    ],
    'application': False,
    'installable': True,
}