# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': 'Credit Extension',
    'depends': ['base', 'sale', 'account'],
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Uncategorized',
    'version': '0.1',
    'data': [
        'wizard/sale_invoices_wizard_view.xml',
        'views/res_partner_view.xml',
        'wizard/credit_extension_wizard_view.xml',
        'wizard/credit_extension_report_wizard_view.xml',
        'reports/credit_extension_line_report.xml',
        'data/handle_install.xml',
    ],
    'application': True,
}
