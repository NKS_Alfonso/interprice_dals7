# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp.osv import osv, fields
from openerp.tools.translate import _


class SalesInvoicesWizard(osv.osv_memory):
    _name = 'sales.invoices.wizard'

    def _get_account_invoice(self, cr, uid, context=None):
        invoices = []
        for partner in self.pool.get('res.partner').browse(
                cr, uid, context['active_id'], context):
            account_invoice_list = self.pool.get('account.invoice').search(
                cr, uid, [
                    ('partner_id', '=', partner.id),
                    ('state', 'in', ('open', 'draft')),
                    ('type', '=', 'out_invoice')
                ], order='number asc')
            if account_invoice_list:
                for i in self.pool.get('account.invoice').browse(
                        cr, uid, account_invoice_list, context):
                    amount_total = 0.0
                    balance = 0.0
                    if i.state == 'open':
                        if i.currency_id.name == i.company_id.currency_id.name:
                            amount_total += i.amount_total
                            balance += i.residual
                        elif i.currency_id.name \
                                != i.company_id.currency_id.name:
                            amount_total += i.amount_total * i.rate
                            balance += i.residual * i.rate
                    elif i.state == 'draft':
                        if i.picking_dev_id:
                            if i.currency_id.name \
                                    == i.company_id.currency_id.name:
                                amount_total += i.amount_total
                                balance += i.amount_total
                            elif i.currency_id.name \
                                    != i.company_id.currency_id.name:
                                from_currency_id = i.currency_id.id
                                to_currency_id = i.company_id.currency_id.id
                                currency_obj = self.pool.get('res.currency')
                                amount_total += currency_obj.compute(
                                    cr, uid, from_currency_id, to_currency_id,
                                    i.amount_total,
                                    round=True,
                                    context=context)
                                balance += currency_obj.compute(
                                    cr,
                                    uid,
                                    from_currency_id,
                                    to_currency_id,
                                    i.amount_total,
                                    round=True,
                                    context=context
                                )
                    if amount_total or balance:
                        invoices.append({
                            'number': i.number if i.number else _('draft'),
                            'amount_total': amount_total,
                            'residual': balance
                        })
        return invoices

    def _get_sale_order(self, cr, uid, context=None):
        orders = []
        for partner in self.pool.get('res.partner').browse(
                cr, uid, context['active_id'], context):
            sale_orders = self.pool.get('sale.order').search(
                cr, uid, [
                    ('partner_id', 'child_of', [partner.id]),
                    ('state', 'not in', [
                        'draft', 'cancel', 'wait_risk', 'invoice_except'
                    ]
                     )
                ], context=context)
            if sale_orders:
                for o in self.pool.get('sale.order').browse(
                        cr, uid, sale_orders, context):
                    cr.execute("""
                            SELECT ai.id FROM account_invoice AS ai
                            INNER JOIN stock_picking AS sp ON ai.origin=sp.name
                            INNER JOIN sale_order AS so ON sp.origin=so.name
                            WHERE so.id=%s""", (o.id,))
                    if not cr.rowcount:
                        amount_total = 0.0
                        if o.currency.id == o.company_id.currency_id.id:
                            amount_total += o.amount_total
                        elif o.currency.id != o.company_id.currency_id.id:
                            from_currency_id = o.currency.id
                            to_currency_id = o.company_id.currency_id.id
                            currency_obj = self.pool.get('res.currency')
                            amount_total = currency_obj.compute(
                                cr, uid, from_currency_id, to_currency_id,
                                o.amount_total, round=True, context=context
                            )
                        orders.append({'name': o.name,
                                       'amount_total': amount_total})
        return orders

    # account_invoice = fields.One2many(
    #     'account.invoice',
    #     'partner_id',
    #     string='Invoices',
    #     readonly='True',
    #     default=_get_account_invoice,
    # ),
    # sale_order = fields.One2many(
    #     'sale.order',
    #     'partner_id',
    #     string='Sale Orders',
    #     readonly='True',
    #     default=_get_sale_order,
    # ),
    _columns = {
        'account_invoice': fields.one2many('account.invoice', 'partner_id', string='Invoices', readonly="True"),
        'sale_order': fields.one2many('sale.order', 'partner_id', string='Sale Orders', readonly="True"),
    }

    _defaults = {
        'account_invoice': _get_account_invoice,
        'sale_order': _get_sale_order,
    }
