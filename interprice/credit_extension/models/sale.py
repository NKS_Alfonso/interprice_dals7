# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, exceptions, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def create(self, values):
        record = super(SaleOrder, self).create(values)
        if record.partner_id.forma_pago.str_nombre == 'Firma Factura':
            credit = record.partner_id.total_available_credit
            amount_total = record.amount_total
            if record.currency.name == 'USD':
                amount_total = record.convert_usd_to_mxn()[0]
            if credit < amount_total:
                raise exceptions.Warning(
                    _('Available credit: %(balance).2f M.N. exceeded') % {
                        'balance': record.partner_id.total_available_credit
                    })

        return record

    @api.one
    def convert_usd_to_mxn(self):
        usd_currency = self.env['res.currency'].search([('name', '=', 'USD')])
        tc = usd_currency.rate_silent
        amount_untaxed = self.amount_untaxed
        taxes = self.amount_tax
        usd_price = 1 / tc
        amount_in_mxn = usd_price * (amount_untaxed + taxes)
        return amount_in_mxn

    @api.one
    def write(self, values):
        if 'state' in values:
            amount_total = self.amount_total
            if self.currency.name == 'USD':
                amount_total = self.convert_usd_to_mxn()[0]

            if values['state'] == 'progress':
                if self.partner_id.total_available_credit < amount_total:
                    raise exceptions.Warning(
                        _('Available credit: %(balance).2f M.N. exceeded') % {
                            'balance': self.partner_id.total_available_credit
                        })
                else:
                    extension = self.partner_id.balance
                    line_credit = self.partner_id.available_credit
                    debt = self.partner_id.total_debt_credit
                    if extension >= amount_total:
                        extension -= amount_total
                        self.partner_id.balance = extension
                    else:
                        amount_total -= extension
                        line_credit -= amount_total
                        self.partner_id.balance = 0
                        self.partner_id.available_credit = line_credit

                    self.partner_id.total_debt_credit = \
                        (debt + amount_total)

            if values['state'] == 'cancel' or values['state'] == 'done':
                available_credit = self.partner_id.available_credit
                debt = self.partner_id.total_debt_credit
                self.partner_id.total_debt_credit = \
                    (debt - amount_total)
                self.partner_id.available_credit = \
                    (available_credit + amount_total)

        return super(SaleOrder, self).write(values)
