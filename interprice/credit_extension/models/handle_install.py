from openerp import models, fields, api


class handle_install(models.Model):
    _name = "handle.install"

    name = fields.Char('Name')

    @api.model
    def create(self, vals):

        module = self.env['ir.module.module'].search([
            ('name', '=', 'nan_partner_risk')
        ])
        if module.state == 'installed':
            res_partner_ids = self.env['res.partner'].search([
                ('customer', '=', True)
            ])
            for partner in res_partner_ids:
                sql = "UPDATE res_partner SET \
                max_credit=%s, available_credit=%s, total_debt_credit=%s \
                WHERE id=%s"
                self.env.cr.execute(sql,
                                    (partner.company_credit_limit,
                                     partner.available_risk,
                                     partner.total_debt,
                                     partner.id))

        return super(handle_install, self).create(vals)
