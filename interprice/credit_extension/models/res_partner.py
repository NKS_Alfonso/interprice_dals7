# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models


class ResPartner(models.Model):
    _inherit = 'res.partner'
    credit_extension_lines = fields.One2many(
        'credit.extension.line',
        'partner_id'
    )
    balance = fields.Float(
        'Balance',
        default=0,
        digits=(10, 2),
        readonly=True,
    )
    max_credit = fields.Float(
        'Credit limit',
        default=0,
        digits=(10, 2),
    )
    total_debt_credit = fields.Float(
        'Total debt',
        default=0,
        digits=(10, 2),
        readonly=True,
    )
    available_credit = fields.Float(
        'Avalable Credit',
        default=0,
        digits=(10, 2),
        readonly=True,
    )
    total_available_credit = fields.Float(
        'Total available credit',
        compute='_compute_total_available_credit',
        readonly=True,
    )
    credit_use_percent = fields.Float(
        'Total use credit',
        compute='_compute_credit_use_percent',
        readonly=True
    )

    @api.one
    def write(self, values):
        if 'max_credit' in values:
            if values['max_credit'] <= 0:
                values['available_credit'] = \
                    values['max_credit'] - self.total_debt_credit
            else:
                diff = values['max_credit'] - self.max_credit
                values['available_credit'] = self.available_credit + diff

        return super(ResPartner, self).write(values)

    @api.one
    def _compute_credit_use_percent(self):
        val = 0
        if self.max_credit:
            val = 100.0 * self.total_debt_credit / self.max_credit
        else:
            val = 100
        self.credit_use_percent = val

    @api.one
    def _compute_total_available_credit(self):
        self.total_available_credit = (self.available_credit + self.balance)

    @api.multi
    def show_wizard_credit_extension_report(self):
        self.ensure_one()
        wizard_form = self.env.ref(
            'credit_extension.credit_extension_report_view',
            False
        )

        view_id = self.env['credit.extension.report.wizard']
        vals = {
            'partner_id': self.id,
        }
        new = view_id.create(vals)
        return {
            'name': 'Report Credit Extension',
            'type': 'ir.actions.act_window',
            'res_model': 'credit.extension.report.wizard',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    @api.multi
    def show_wizard_credit_extension(self):
        self.ensure_one()
        wizard_form = self.env.ref(
            'credit_extension.credit_extension_wizard_form', False)

        view_id = self.env['credit.extension.wizard']
        vals = {
            'partner_id': self.id,
            'amount': 0,
            'credit_limit': self.max_credit,
            'payment_method': self.forma_pago.str_nombre
        }
        new = view_id.create(vals)
        return {
            'name': 'Credit Extension',
            'type': 'ir.actions.act_window',
            'res_model': 'credit.extension.wizard',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }
