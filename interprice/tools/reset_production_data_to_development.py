# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import olib.PsqlContector as psqlc
import string
from random import choice


class ResetProductionDataToDevelopment:

    conn = None

    def __init__(self, host=None, database=None, user=None,
                 password=None, port=None, server=None):
        try:
            if not host:
                host = 'localhost'
            if not port:
                port = 5432
            self.conn = psqlc.PsqlConnector(
                database=database, user=user,
                password=password, host=host,
                port=port, server=server
            )
            if self.conn:
                length = 18
                values = string.lowercase + string.uppercase + string.digits
                # Parameters Pac
                cr = self.conn
                cr.execute("""
                SELECT *
                FROM params_pac;
                """)
                if self.conn.count() > 0:
                    query_params_pac = self.conn.many()
                    print '*****RESET PARAMETERS OF PAC*****'
                    for line in query_params_pac:
                        p = ""
                        pwd = p.join([choice(values) for i in range(length)])
                        id = line[0]
                        cr.execute("""
                                   UPDATE params_pac SET
                                   url_webservice = 'field reseted',
                                   namespace = 'field reseted',
                                   password = %s
                                   WHERE id = %s;
                                   """, (pwd, id)
                                   )
                        print ' ° Parameters reseted: ' + str(line[3])
                    print ''
                # Outgoing mail servers
                cr = self.conn
                cr.execute("""
                            SELECT *
                            FROM ir_mail_server;
                            """)
                if self.conn.count() > 0:
                    query_outgoing_mail = self.conn.many()
                    print '*****RESET PARAMETERS OF OUTGOING MAIL SERVERS*****'
                    for line in query_outgoing_mail:
                        p = ""
                        pwd = p.join([choice(values) for i in range(length)])
                        id = line[0]
                        cr.execute("""
                                   UPDATE ir_mail_server SET
                                   smtp_user = 'field reseted',
                                   smtp_pass = %s
                                   WHERE id = %s;
                                   """, (pwd, id)
                                   )
                        print ' ° Parameters reseted: ' + str(line[4])
                    print ''
                # Incoming mail servers
                cr = self.conn
                cr.execute("""
                            SELECT *
                            FROM fetchmail_server;
                            """)
                if self.conn.count() > 0:
                    query_incoming_mail = self.conn.many()
                    print '*****RESET PARAMETERS OF INCOMING MAIL SERVERS*****'
                    for line in query_incoming_mail:
                        p = ""
                        pwd = p.join([choice(values) for i in range(length)])
                        id = line[0]
                        cr.execute("""
                                   UPDATE fetchmail_server SET
                                   server = 'field reseted',
                                   password = %s
                                   WHERE id = %s;
                                   """, (pwd, id)
                                   )
                        print ' ° Parameters reseted: ' + str(line[18])
                    print ''
                # Users
                cr = self.conn
                cr.execute("""
                            SELECT *
                            FROM res_users;
                            """)
                if self.conn.count() > 0:
                    query_users = self.conn.many()
                    print '*****RESET PARAMETERS OF USERS*****'
                    for line in query_users:
                        id = line[0]
                        if line[2] == 'adm_gv_todo':
                            cr.execute("""
                                       UPDATE res_users SET
                                       login = 'admin',
                                       password = 'admin'
                                       WHERE id = %s;
                                       """, (id,)
                                       )
                            print ' ° Parameters reseted: ' + \
                                  str(line[2]) + \
                                  ' ==> new login="admin" / new password="admin"'
                        else:
                            cr.execute("""
                                       UPDATE res_users SET
                                       password = 'admin'
                                       WHERE id = %s;
                                       """, (id,)
                                       )

                            print ' ° Parameters reseted: ' + \
                                  str(line[2]) + ' ==> new password="admin"'
                    print ''
            else:
                raise Exception("Fail connection!")
        except Exception, e:
            print (e)
