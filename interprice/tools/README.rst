** TOOLS EXECUTION GUIDE **
===========================

Reset Production Data To Development
====================================

Tool to restart the PAC data, Outgoing mail servers, Incoming mail servers and Users.
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Steps to execute:
*****************

1. Open your terminal: Ctrl + Alt + t
2. user@lap:~$ cd interprice
3. user@lap:~/interprice$ python2.7
4. >> from interprice.tools.reset_production_data_to_development import ResetProductionDataToDevelopment as conn
5. >> conn(database='blackpcs_2017-12-14_17-20-26', user='odoo', password='password') if working in localhost.
6. >> conn(host='is not local host', database='blackpcs_2017-12-14_17-20-26', user='odoo', password='password', port='is not port 5432') if not working in localhost.

It will return the following:
*****************************

*****RESET PARAMETERS OF PAC*****

- Parameters reseted: PAC DIV - Firmar - Prod
- Parameters reseted: PAC DIV - Cancelar - Prod

*****RESET PARAMETERS OF OUTGOING MAIL SERVERS*****

- Parameters reseted: FacturaE

*****RESET PARAMETERS OF INCOMING MAIL SERVERS*****

- Parameters reseted: Oportunidad

*****RESET PARAMETERS OF USERS*****

- Login = admin / admin