# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author TO-DO Developers

{
    'name': 'Documentos y Movimientos Bancarios',
    'description': 
        """Modulo para registrar Movimientos Bancarios realizados, 
        ya sean del tipo de ingresos o egresos        
        """,
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'depends': ['base',
                'account',
                #'mail'
        ],
    'data': [
        'security/ir.model.access.csv',
        'views/bank_document_view.xml',
        'data/document_sequence.xml',
        'data/movimiento_bancario_ingreso_egreso_tasa_cambio_edit.xml'
    ]
}
