# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _, exceptions
from openerp.osv import fields as fields7
from datetime import datetime
import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import *
from openerp.exceptions import except_orm, Warning, RedirectWarning


class documentos_bancarios(models.Model):
    _inherit = 'mail.thread'
    _name = 'documentos.bancarios'
    _description = 'Documento bancario'
    _rec_name = 'documento'
    _defaults = {
        'tipo_movimiento': 'ingreso',
    }

    _sql_constraints = [
        ('documento_unique', 'UNIQUE(documento)',
         "El nombre del documento debe ser unico"),
    ]

    folio = fields.Char(string='')
    documento = fields.Char(String='Documento', required=True)
    # descripcion = fields.Text(String='Descripcion', required=True)
    tipo_cuenta = fields.Many2one(
                                comodel_name='account.account',
                                String='Cuenta',
                                required=True,
                                )
    tipo_movimiento = fields.Selection(
                                    selection=[
                                        ('ingreso', 'Ingreso'),
                                        ('egreso', 'Egreso')],
                                    string='Tipo de movimiento',
                                    required=True)
    impuesto = fields.Many2one('account.tax', 'Impuesto')

    secuencia = fields.Many2one(
                            comodel_name='ir.sequence',
                            string='Secuencia',
                            required=True
                            )

    @api.multi
    def write(self, vals):
        return super(documentos_bancarios, self).write(vals)


def obtener_periodo_actual(self):
    obtener_fecha = datetime.now().date()
    fecha_periodo = obtener_fecha.strftime("%m/%Y")
    return self.env['account.period'].search([('name', '=', fecha_periodo)])


def obtener_cuentas(self, vals):
    if self.tipo_Movimiento == 'ingreso':
        cuenta_debito = self.cuenta_bancaria.journal_id.\
                        default_debit_account_id  # se le quita
        cuenta_credito = self.doc_bancario.tipo_cuenta  # se le abona
    elif self.tipo_Movimiento == 'egreso':
        cuenta_debito = self.cuenta_bancaria.journal_id.\
            default_credit_account_id  # se abona
        cuenta_credito = self.doc_bancario.tipo_cuenta  # Se quita
    return cuenta_debito, cuenta_credito


def set_moveline_fields(self, vals, movement_created_id,
                        movement_created_name):
    '''
    Campos para crear un registro move.line
    self = objeto de la propia clase
    vals = valoress del metodo create()
    movement_created_id = Id de la Poliza creada
    line_type = debit, credit and tax
    '''
    cuenta_debito, cuenta_credito = obtener_cuentas(self, vals)

    # Linea del deber
    linea_debito_mxn = {
        'account_id': cuenta_debito.id,
        'journal_id': self.cuenta_bancaria.journal_id.id,
        'move_id': movement_created_id,
        'name': '/',
        'period_id': obtener_periodo_actual(self).id,
        'docto_referencia': self.folio_interno,
        }
    linea_debito_usd = linea_debito_mxn

    # linea del haber
    linea_credito_mxn = {
        'account_id': cuenta_credito.id,
        'journal_id': self.cuenta_bancaria.journal_id.id,
        'move_id': movement_created_id,
        'name': movement_created_name,
        'period_id': obtener_periodo_actual(self).id,
        'docto_referencia': self.folio_interno,
        }
    linea_credito_usd = linea_credito_mxn

    # Toma la cuenta del diario del impuesto cuando no tiene
    # la account_collected_voucher_id
    if not self.doc_bancario.impuesto.account_collected_voucher_id.id:
        cuenta_impuesto = self.doc_bancario.impuesto.account_collected_id.id
    else:
        cuenta_impuesto = self.doc_bancario.impuesto.\
            account_collected_voucher_id.id

    line_tax = {
        'account_id': cuenta_impuesto,
        'journal_id': self.cuenta_bancaria.journal_id.id,
        'move_id': movement_created_id,
        'name': self.doc_bancario.impuesto.name,
        'period_id': obtener_periodo_actual(self).id,
        'docto_referencia': self.folio_interno,
        }

# MOVIMIENTO DE TIPO INGRESO MXN
    if self.tipo_Movimiento == 'ingreso' and self.tipo_moneda.name == 'MXN':

        # VALIDAR: INGRESO TIPO MXN SIN IMPUESTO
        if not self.doc_bancario.impuesto.id and self.state != 'generado':
            linea_debito_mxn['debit'] = self.importe
            linea_credito_mxn['credit'] = self.importe

            return linea_debito_mxn, linea_credito_mxn

        # VALIDAR: INGRESO TIPO MXN CON IMPUESTO
        elif self.doc_bancario.impuesto.id and self.state != 'generado':
            # CREACION DE LINEAS SI EL IMPUESTO ES NEGATIVO
            if self.doc_bancario.impuesto.amount < 0:
                linea_debito_mxn['debit'] = float_round(
                    self.importe - float_round(
                        self.importe * self.doc_bancario.impuesto.amount,precision_digits=2),precision_digits=2)
                linea_debito_mxn['tax_amount'] = linea_debito_mxn['debit']

                linea_credito_mxn['credit'] = self.importe
                linea_credito_mxn['tax_amount'] = self.importe

                line_tax['credit'] = float_round(
                    self.importe * self.doc_bancario.impuesto.amount,precision_digits=2) * -1
                line_tax['tax_amount'] = line_tax['credit']
                line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id
            # CREACION DE LINEAS SI EL IMPUESTO ES 0
            else:
                linea_debito_mxn['debit'] = self.importe
                linea_credito_mxn['credit'] = float_round(
                    self.importe / (
                        1 + self.doc_bancario.impuesto.amount),precision_digits=2)
                linea_credito_mxn['tax_amount'] = linea_credito_mxn['credit']

                # LINEA PARA CUANDO EL IMPUESTO DE LA CUENTA ES 0
                if self.doc_bancario.impuesto.amount == 0:
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id
                # LINEAS SI EL IMPUESTO ES MAYOR DE 0
                else:
                    line_tax['credit'] = self.importe - linea_credito_mxn['credit']
                    line_tax['tax_amount'] = line_tax['credit']
                    line_tax['amount_base'] = linea_credito_mxn['credit']
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id

            return linea_debito_mxn, linea_credito_mxn, line_tax

        # CANCELAR: INGRESO TIPO MXN SIN IMPUESTO
        #~ elif not self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ linea_debito_mxn['credit'] = self.importe
            #~ linea_credito_mxn['debit'] = self.importe
#~
            #~ return linea_debito_mxn, linea_credito_mxn

        # CANCELAR: INGRESO TIPO MXN CON IMPUESTO
        #~ elif self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ # CREACION DE LINEAS CUANDO EL IMPUESTO ES NEGATIVO
            #~ if self.doc_bancario.impuesto.amount < 0:
                #~ linea_debito_mxn['credit'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
                #~ linea_debito_mxn['tax_amount'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
#~
                #~ linea_credito_mxn['debit'] = self.importe
                #~ linea_credito_mxn['tax_amount'] = self.importe
#~
                #~ line_tax['debit'] = self.importe * round(
                    #~ self.doc_bancario.impuesto.amount, 4) * -1
                #~ line_tax['tax_amount'] = self.importe * round(
                    #~ self.doc_bancario.impuesto.amount, 4) * -1
                #~ line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id
            #~ # CREACION DE LINEAS CUANDO EL IMPUESTO ES POSITIVO
            #~ else:
                #~ linea_debito_mxn['credit'] = self.importe
                #~ linea_credito_mxn['debit'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
#~
                #~ linea_credito_mxn['tax_amount'] = self.importe
#~
                #~ # LINEA PARA CUANDO EL IMPUESTO DE LA CUENTA ES 0
                #~ if self.doc_bancario.impuesto.amount == 0:
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
                #~ # LINEA PARA CUANDO EL IMPUESTO DE LA CUENTA ES MAYOR A 0
                #~ else:
                    #~ line_tax['debit'] = self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)
                    #~ line_tax['tax_amount'] = self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)
                    #~ line_tax['amount_base'] = self.importe
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
#~
            #~ return linea_debito_mxn, linea_credito_mxn, line_tax

# MOVIMIENTO DE TIPO EGRESO MXN
    elif self.tipo_Movimiento == 'egreso' and self.tipo_moneda.name == 'MXN':
        # VALIDAR: EGRESO TIPO MXN SIN IMPUESTO
        if not self.doc_bancario.impuesto.id and self.state != 'generado':
            linea_debito_mxn['credit'] = self.importe
            linea_credito_mxn['debit'] = self.importe
            return linea_debito_mxn, linea_credito_mxn

        # VALIDAR: EGRESO TIPO MXN CON IMPUESTO
        elif self.doc_bancario.impuesto.id and self.state != 'generado':
            # CREACION DE LINEAS CUANDO EL IMPUESTO ES NEGATIVO
            if self.doc_bancario.impuesto.amount < 0:
                linea_debito_mxn['credit'] = float_round(
                    self.importe - float_round(
                        self.importe * self.doc_bancario.impuesto.amount,precision_digits=2),precision_digits=2)
                linea_debito_mxn['tax_amount'] = linea_debito_mxn['credit']

                linea_credito_mxn['debit'] = self.importe
                linea_credito_mxn['tax_amount'] = self.importe

                line_tax['debit'] = float_round(
                    self.importe * self.doc_bancario.impuesto.amount,precision_digits=2) * -1
                line_tax['tax_amount'] = line_tax['debit']
                line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id

            else:
                linea_debito_mxn['credit'] = self.importe
                linea_credito_mxn['debit'] = float_round(
                    self.importe / (
                        1 + self.doc_bancario.impuesto.amount),precision_digits=2)
                linea_credito_mxn['tax_amount'] = linea_credito_mxn['debit']

                # LINEA PARA CUANDO EL IMPUESTO DE LA CUENTA ES 0
                if self.doc_bancario.impuesto.amount == 0:
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id
                else:
                    line_tax['debit'] = self.importe - linea_credito_mxn['debit']
                    line_tax['tax_amount'] = line_tax['debit']
                    line_tax['amount_base'] = linea_credito_mxn['debit']
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id

            return linea_debito_mxn, linea_credito_mxn, line_tax

        # CANCELAR: EGRESO TIPO MXN SIN IMPUESTO
        #~ elif not self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ linea_debito_mxn['debit'] = self.importe
            #~ linea_credito_mxn['credit'] = self.importe
            #~ return linea_debito_mxn, linea_credito_mxn

        # CANCELAR: EGRESO TIPO MXN CON IMPUESTO
        #~ elif self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ if self.doc_bancario.impuesto.amount < 0:
                #~ linea_credito_mxn['debit'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
                #~ linea_credito_mxn['tax_amount'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
#~
                #~ linea_debito_mxn['credit'] = self.importe
                #~ linea_debito_mxn['tax_amount'] = self.importe
#~
                #~ line_tax['credit'] = self.importe * round(
                    #~ self.doc_bancario.impuesto.amount, 4) * -1
                #~ line_tax['tax_amount'] = self.importe * round(
                    #~ self.doc_bancario.impuesto.amount, 4) * -1
                #~ #line_tax['amount_base'] = self.importe
                #~ line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id
#~
            #~ else:
                #~ linea_debito_mxn['debit'] = self.importe
                #~ linea_credito_mxn['credit'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
                #~ linea_credito_mxn['tax_amount'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
                #~
#~
                #~ # LINEA PARA CUANDO EL IMPUESTO DE LA CUENTA ES 0
                #~ if self.doc_bancario.impuesto.amount == 0:
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
                #~ else:
                    #~ line_tax['credit'] = self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)
                    #~ line_tax['tax_amount'] = self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)
                    #~ line_tax['amount_base'] = self.importe - (
                    #~ self.importe * round(self.doc_bancario.impuesto.amount, 4))
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
            #~ return linea_debito_mxn, linea_credito_mxn, line_tax

# MOVIMIENTO DE TIPO INGRESO USD
    elif self.tipo_Movimiento == 'ingreso' and self.tipo_moneda.name == 'USD':
        if not self.doc_bancario.impuesto.id and self.state != 'generado':
            # INGRESO TIPO USD SIN IMPUESTOS
            linea_credito_usd['credit'] = float_round(
                self.importe * float_round(
                    self.tipo_cambio,precision_digits=4), precision_digits=2)
            linea_credito_usd['amount_currency'] = self.importe * -1
            linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                currency2_id.id

            linea_debito_usd['debit'] = float_round(
                self.importe * float_round(
                self.tipo_cambio,precision_digits=4), precision_digits=2)
            linea_debito_usd['amount_currency'] = self.importe
            linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                currency2_id.id

            return linea_debito_usd, linea_credito_usd

        # VALIDAR: INGRESO TIPO USD CON IMPUESTO
        elif self.doc_bancario.impuesto.id and self.state != 'generado':
            # CREACION DE LINEAS SI EL IMPUESTO ES NEGATIVO
            if self.doc_bancario.impuesto.amount < 0:
                linea_credito_usd['credit'] = float_round(
                    self.importe * float_round(
                        self.tipo_cambio,precision_digits=4), precision_digits=2)
                linea_credito_usd['amount_currency'] = self.importe * -1
                linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id
                linea_credito_usd['tax_amount'] = linea_credito_usd['credit']

                linea_debito_usd['debit'] = float_round(
                    linea_credito_usd['credit'], precision_digits=2) - (
                        float_round(linea_credito_usd['credit'] *\
                            self.doc_bancario.impuesto.amount, precision_digits=2))
                linea_debito_usd['amount_currency'] = (
                    self.importe - float_round(
                        self.importe * self.doc_bancario.impuesto.amount,precision_digits=2))
                linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id

                line_tax['credit'] = (float_round(
                    linea_credito_usd['credit'] * \
                        self.doc_bancario.impuesto.amount, precision_digits=2)) * -1
                line_tax['amount_currency'] = float_round(
                    self.importe * self.doc_bancario.impuesto.amount, precision_digits=2)
                line_tax['currency_id'] = self.cuenta_bancaria.currency2_id.id
                line_tax['tax_amount'] = line_tax['credit']
                line_tax['amount_base'] = linea_credito_usd['credit']
                line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id

            # CREACION DE LINEAS SI EL IMPUESTO ES POSITIVO
            else:
                linea_debito_usd['debit'] = float_round(
                    self.importe * float_round(
                        self.tipo_cambio,precision_digits=4), precision_digits=2)
                linea_debito_usd['amount_currency'] = self.importe
                linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id

                linea_credito_usd['credit'] = float_round(
                    linea_debito_usd['debit'] / (
                        1 + self.doc_bancario.impuesto.amount), precision_digits=2)
                linea_credito_usd['amount_currency'] = float_round(
                    self.importe / (
                        1 + self.doc_bancario.impuesto.amount),precision_digits=2) * -1
                linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id
                linea_credito_usd['tax_amount'] = linea_credito_usd['credit']

                # LINEA PARA CUANDO EL IMPUESTO DE LA CUENTA ES 0
                if self.doc_bancario.impuesto.amount == 0:
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id
                    line_tax['currency_id'] = self.cuenta_bancaria.\
                        currency2_id.id
                    line_tax['debit'] = 0
                else:
                    line_tax['credit'] = float_round(
                        linea_debito_usd['debit'] -\
                            linea_credito_usd['credit'], precision_digits=2)
                    line_tax['amount_currency'] = float_round(
                        line_tax['credit'] / float_round(
                            self.tipo_cambio,precision_digits=4),precision_digits=2) * -1
                    line_tax['currency_id'] = self.cuenta_bancaria.\
                        currency2_id.id
                    line_tax['tax_amount'] = line_tax['credit']
                    line_tax['amount_base'] = linea_credito_usd['credit']
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id

            return linea_debito_usd, linea_credito_usd, line_tax

        #~ # CANCELAR: INGRESO TIPO USD SIN IMPUESTO
        #~ elif not self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ linea_credito_usd['debit'] = self.importe * self.tipo_cambio
            #~ linea_credito_usd['amount_currency'] = self.importe
            #~ linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                #~ currency2_id.id
            #~ linea_debito_usd['credit'] = round(self.importe * round(
                #~ self.tipo_cambio, 4), 2)
            #~ linea_debito_usd['amount_currency'] = self.importe * -1
            #~ linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                #~ currency2_id.id
#~
            #~ return linea_debito_usd, linea_credito_usd

        # CANCELAR: INGRESO TIPO USD CON IMPUESTO
        #~ elif self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ if self.doc_bancario.impuesto.amount < 0:
                #~ linea_debito_usd['credit'] = round((
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))), 2)
                #~ linea_debito_usd['amount_currency'] = (
                    #~ self.importe - (self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4))) * -1
                #~ linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
#~
                #~ linea_credito_usd['debit'] = (
                    #~ self.importe * self.tipo_cambio)
#~
                #~ linea_credito_usd['amount_currency'] = self.importe
                #~ linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
                #~ # linea_credito_usd['tax_amount'] = round((
                #~ #    self.importe * self.tipo_cambio) - (
                #~ #        self.tipo_cambio * (self.importe * round(
                #~ #            self.doc_bancario.impuesto.amount, 4))), 2)
#~
                #~ linea_credito_usd['tax_amount'] = (
                    #~ self.importe * self.tipo_cambio)
#~
                #~ line_tax['debit'] = self.tipo_cambio * (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)) * -1
                #~ line_tax['amount_currency'] = (
                    #~ self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)) * -1
                #~ line_tax['currency_id'] = self.cuenta_bancaria.currency2_id.id
                #~ line_tax['tax_amount'] = self.tipo_cambio * (
                    #~ self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4))
                #~ line_tax['amount_base'] = (
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)))
                #~ line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id
            #~ else:
                #~ linea_debito_usd['credit'] = (
                    #~ self.importe * self.tipo_cambio)
                #~ linea_debito_usd['amount_currency'] = self.importe * -1
                #~ linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
#~
                #~ linea_credito_usd['debit'] = round((
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))), 2)
                #~ linea_credito_usd['amount_currency'] = (
                    #~ self.importe - (self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)))
                #~ linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
                #~ linea_credito_usd['tax_amount'] = round((
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))), 2)
#~
                #~ if self.doc_bancario.impuesto.amount == 0:
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
                    #~ line_tax['currency_id'] = self.cuenta_bancaria.\
                        #~ currency2_id.id
                    #~ line_tax['debit'] = 0
                #~ else:
                    #~ line_tax['debit'] = self.tipo_cambio * (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))
                    #~ line_tax['amount_currency'] = (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))
                    #~ line_tax['currency_id'] = self.cuenta_bancaria.\
                        #~ currency2_id.id
                    #~ line_tax['tax_amount'] = self.tipo_cambio * (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))
                    #~ line_tax['amount_base'] = round((
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))), 2)
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
#~
            #~ return linea_debito_usd, linea_credito_usd, line_tax

# MOVIMIENTO DE TIPO EGRESO USD
    elif self.tipo_Movimiento == 'egreso' and self.tipo_moneda.name == 'USD':
        # VALIDAR: EGRESO TIPO USD SIN IMPUESTOS
        if not self.doc_bancario.impuesto.id and self.state != 'generado':
            linea_debito_usd['credit'] = float_round(
                self.importe * float_round(
                    self.tipo_cambio,precision_digits=4), precision_digits=2)
            linea_debito_usd['amount_currency'] = self.importe * -1
            linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                currency2_id.id

            linea_credito_usd['debit'] = float_round(
                self.importe * float_round(
                    self.tipo_cambio,precision_digits=4), precision_digits=2)
            #self.importe * self.tipo_cambio
            linea_credito_usd['amount_currency'] = self.importe
            linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                currency2_id.id

            return linea_debito_usd, linea_credito_mxn

        # VALIDAR: EGRESO TIPO USD CON IMPUESTO
        elif self.doc_bancario.impuesto.id and self.state != 'generado':
            if self.doc_bancario.impuesto.amount < 0:
                linea_credito_usd['debit'] = float_round(
                    self.importe * float_round(
                        self.tipo_cambio,precision_digits=4), precision_digits=2)
                linea_credito_usd['amount_currency'] = self.importe
                linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id
                linea_credito_usd['tax_amount'] = linea_credito_usd['debit']

                linea_debito_usd['credit'] = linea_credito_usd['debit'] -\
                    float_round(
                        linea_credito_usd['debit'] * self.doc_bancario.impuesto.amount,precision_digits=2)
                linea_debito_usd['amount_currency'] = float_round(
                    self.importe - float_round(
                        self.importe * self.doc_bancario.impuesto.amount, precision_digits=2),precision_digits=2) * -1
                linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id

                line_tax['debit'] = float_round(
                    linea_credito_usd['debit'] * self.doc_bancario.impuesto.amount,precision_digits=2) * -1
                line_tax['amount_currency'] = float_round(
                    self.importe * self.doc_bancario.impuesto.amount, precision_digits=2) * -1
                line_tax['currency_id'] = self.cuenta_bancaria.currency2_id.id
                line_tax['tax_amount'] = line_tax['debit'] #* -1
                line_tax['amount_base'] = linea_credito_usd['debit']
                line_tax['tax_id_secondary'] = self.doc_bancario.impuesto.id

            else:
                linea_debito_usd['credit'] = float_round(
                    self.importe * float_round(
                        self.tipo_cambio,precision_digits=4), precision_digits=2)
                linea_debito_usd['amount_currency'] = self.importe * -1
                linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id

                linea_credito_usd['debit'] = float_round(
                    linea_debito_usd['credit'] / (
                        1 + self.doc_bancario.impuesto.amount), precision_digits=2)
                linea_credito_usd['amount_currency'] = float_round(
                    self.importe / (
                        1 + self.doc_bancario.impuesto.amount),precision_digits=2)
                linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    currency2_id.id
                linea_credito_usd['tax_amount'] = linea_credito_usd['debit']

                if self.doc_bancario.impuesto.amount == 0:
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id
                    line_tax['currency_id'] = self.cuenta_bancaria.\
                        currency2_id.id
                else:
                    line_tax['debit'] = float_round(
                        linea_debito_usd['credit'] -\
                            linea_credito_usd['debit'], precision_digits=2)
                    line_tax['amount_currency'] = float_round(
                        line_tax['debit'] / float_round(
                            self.tipo_cambio,precision_digits=4),precision_digits=2)
                    line_tax['currency_id'] = self.cuenta_bancaria.\
                        currency2_id.id
                    line_tax['tax_amount'] = line_tax['debit']
                    line_tax['amount_base'] = linea_credito_usd['debit']
                    line_tax['tax_id_secondary'] = self.doc_bancario.\
                        impuesto.id

            return linea_debito_usd, linea_credito_mxn, line_tax

        # CANCELAR: EGRESO TIPO USD SIN IMPUESTOS
        #~ elif not self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ linea_debito_usd['credit'] = round(
                #~ self.importe * round(self.tipo_cambio, 4), 2)
            #~ linea_debito_usd['amount_currency'] = self.importe * -1
            #~ linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                #~ currency2_id.id
            #~ linea_credito_usd['debit'] = self.importe * self.tipo_cambio
            #~ linea_credito_usd['amount_currency'] = self.importe
            #~ linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                #~ currency2_id.id
#~
            #~ return linea_debito_usd, linea_credito_mxn

        # CANCELAR: EGRESO TIPO USD CON IMPUESTOS
        #~ elif self.doc_bancario.impuesto.id and self.state == 'generado':
            #~ if self.doc_bancario.impuesto.amount < 0:
                #~ linea_debito_usd['credit'] = (self.importe * self.tipo_cambio)
                #~ linea_debito_usd['amount_currency'] = self.importe * -1
                #~ linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
                #~ linea_debito_usd['tax_amount'] = (
                    #~ self.importe * self.tipo_cambio)
#~
                #~ linea_credito_usd['debit'] = (
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)))
                #~ linea_credito_usd['amount_currency'] = (
                    #~ self.importe - (self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)))
                #~ linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
                #~ # linea_credito_usd['tax_amount'] = (
                    #~ # self.importe * self.tipo_cambio) - (
                        #~ # self.tipo_cambio * (self.importe * round(
                            #~ # self.doc_bancario.impuesto.amount, 4)))
#~
                #~ line_tax['credit'] = self.tipo_cambio * (
                    #~ self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)) * -1
                #~ line_tax['amount_currency'] = (
                    #~ self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4)) #* -1
                #~ line_tax['currency_id'] = self.cuenta_bancaria.currency2_id.id
                #~ line_tax['tax_amount'] = self.tipo_cambio * (
                    #~ self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4))
                #~ line_tax['amount_base'] = (self.importe * self.tipo_cambio)
                #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                    #~ impuesto.id
#~
            #~ else:
                #~ linea_debito_usd['credit'] = (
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)))
                #~ linea_debito_usd['amount_currency'] = (
                    #~ self.importe - (self.importe * round(
                        #~ self.doc_bancario.impuesto.amount, 4))) * -1
                #~ linea_debito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
                #~ linea_debito_usd['tax_amount'] = (
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)))
                #~
                #~ linea_credito_usd['debit'] = (self.importe * self.tipo_cambio)
                #~ linea_credito_usd['amount_currency'] = self.importe
                #~ linea_credito_usd['currency_id'] = self.cuenta_bancaria.\
                    #~ currency2_id.id
                #~
                #~ if self.doc_bancario.impuesto.amount == 0:
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
                    #~ line_tax['currency_id'] = self.cuenta_bancaria.\
                        #~ currency2_id.id
                #~ else:
                    #~ line_tax['credit'] = self.tipo_cambio * (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))
                    #~ line_tax['amount_currency'] = (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)) * -1
                    #~ line_tax['currency_id'] = self.cuenta_bancaria.\
                        #~ currency2_id.id
                    #~ line_tax['tax_amount'] = self.tipo_cambio * (
                        #~ self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4))
                    #~ line_tax['amount_base'] = (
                    #~ self.importe * self.tipo_cambio) - (
                        #~ self.tipo_cambio * (self.importe * round(
                            #~ self.doc_bancario.impuesto.amount, 4)))
                    #~ line_tax['tax_id_secondary'] = self.doc_bancario.\
                        #~ impuesto.id
#~
            #~ return linea_debito_usd, linea_credito_mxn, line_tax


class ingresos_egresos(models.Model):
    _inherit = 'mail.thread'
    _name = 'ingresos.egresos'
    _description = 'Movimiento'
    _rec_name = 'folio_interno'

    _defaults = {
        'tipo_Movimiento': 'ingreso',
        'state': 'borrador',
        'fecha': fields7.date.context_today,
    }
    fecha = fields.Date(
        String='Fecha',
        required=True
    )
    folio_interno = fields.Char(string='')
    doc_bancario = fields.Many2one(
        comodel_name='documentos.bancarios',
        String='Documento bancario',
        required=True,
        domain=[('id', '=', 0)]
    )
    cuenta_bancaria = fields.Many2one(
        comodel_name='res.partner.bank',
        string='Cuenta bancaria',
        required=True
    )
    observacion = fields.Text('Observaciones')
    tipo_moneda = fields.Many2one('res.currency')
    tipo_moneda_related = fields.Char(related='tipo_moneda.name')
    tipo_cambio = fields.Float(
        String='Tasa de cambio',
        digits=(12, 4),
        Store=True
    )
    importe = fields.Float(
        String='Importe',
        digits=dp.get_precision('Account'),
        required=True
    )
    tipo_Movimiento = fields.Selection('_obtener_tipo_movimiento',
                                       string='Tipo de movimiento',
                                       required=True)
    state = fields.Selection([
        ('borrador', 'Borrador'),
        ('generado', 'Generado'),
        ('cancelado', 'Cancelado')],
        string='Status',
        required=True, readonly=True
    )

    impuesto = fields.Many2one(
        comodel_name='documentos.bancarios'
    )
    movement = fields.Many2one(
        string='Póliza',
        readonly=True,
        comodel_name='account.move',
        copy=False,
    )
    cancelado = fields.Many2one(
        string='Cancelado',
        required=False,
        readonly=True,
        comodel_name='account.move',
        copy=False,
    )
    fecha_validacion = fields.Date(
        default=fields.Date.today,
        help=('Fecha de validación cuando el documento afecta al banco'),
        required=True,
        string='Fecha afectación banco'
    )

    @api.one
    @api.constrains('importe')
    def _validar_importe_(self):
        """
        El importe debe ser flotante y mayor que cero.
        """
        if type(self.importe) != float:
            raise exceptions.ValidationError("El importe debe ser numérico")
        if self.importe <= 0.00:
            raise exceptions.ValidationError("El importe no debe ser cero")

    @api.one
    @api.constrains('tipo_cambio')
    def _validar_tipo_cambio_(self):
        """
        El tipo de cambio debe ser flotante y mayor que cero.
        """
        if type(self.tipo_cambio) != float:
            raise exceptions.ValidationError(
                                    "El tipo de cambio debe ser tipo numérico"
                                    )
        if self.tipo_cambio <= 0.00:
            raise exceptions.ValidationError(
                                    "El tipo de cambio debe ser mayor a cero"
                                    )

    @api.onchange('tipo_Movimiento')
    def onchange_documento_bancario(self):
        res = {}
        self.cuenta_bancaria = False
        self.doc_bancario = False
        self.importe = False
        if not self.cuenta_bancaria:
            domain = [('id', '=', 0)]
        else:
            domain = [
                    ("tipo_movimiento", "=", self.tipo_Movimiento),
                ]
        res['domain'] = {
            "doc_bancario": domain
        }
        return res

    @api.onchange('cuenta_bancaria')
    def onchange_cuenta_bancaria(self):
        self.doc_bancario = False
        if self.tipo_moneda:
            res = {}
            domain = []
            doc_banc = self.env['documentos.bancarios'].search(
                [
                    ('tipo_cuenta.currency_id', '=', False),
                    ("tipo_movimiento", "=", self.tipo_Movimiento)
                ]
            )
            doc_banc_ids = [doc.id for doc in doc_banc]
            if doc_banc_ids and self.cuenta_bancaria.company_id.\
                    currency_id == self.tipo_moneda:
                domain = [
                    '|',
                    ("tipo_cuenta.currency_id", "=", self.tipo_moneda.id),
                    ('id', 'in', doc_banc_ids),
                    ("tipo_movimiento", "=", self.tipo_Movimiento)
                ]
            elif self.tipo_moneda and self.tipo_Movimiento and self.cuenta_bancaria:
                domain = [
                    ("tipo_cuenta.currency_id", "=", self.tipo_moneda.id),
                    ("tipo_movimiento", "=", self.tipo_Movimiento)
                ]
            else:
                domain = [('id', '=', 0)]
            res['domain'] = {"doc_bancario": domain}
            return res

    def obtener_tipo_cambio(self, currency_id):
        valor_de_tasa = self.env['res.currency'].search(
            [('id', '=', currency_id)]).rate_silent
        if not valor_de_tasa:
            valor_de_tasa = 1
        return round(1/valor_de_tasa, 4)

    def obtener_tipo_moneda(self, objeto):
        moneda = objeto.company_id.currency_id
        if objeto.currency_id:
            moneda = objeto.currency_id
        return moneda

    @api.onchange('cuenta_bancaria')
    def obtener_tasa_cambio(self):
        self.tipo_moneda = self.obtener_tipo_moneda(self.cuenta_bancaria)
        self.tipo_cambio = self.obtener_tipo_cambio(self.tipo_moneda.id)

    @api.model
    def _obtener_tipo_movimiento(self):
        return [('ingreso', _('Ingreso')), ('egreso', _('Egreso'))]

    @api.one
    def boton_validar(self):
        if self.tipo_moneda != self.cuenta_bancaria.company_id.currency_id and \
            self.tipo_moneda != self.doc_bancario.tipo_cuenta.currency_id:
            raise exceptions.Warning(
                                    "La moneda del diario del documento bancario ha sido cambiada"
                                    )
        elif self.tipo_moneda == self.cuenta_bancaria.company_id.currency_id and \
            self.doc_bancario.tipo_cuenta.currency_id:
            raise exceptions.Warning(
                                    "La moneda del diario del documento bancario ha sido cambiada"
                                    )

        vals = dict(self._context)
        # vals['state'] = 'borrador'

        # self.folio_interno = self.env['ir.sequence'].next_by_code(
        #    'ingresos.egresos.folio_interno')
        self.folio_interno = self.env['ir.sequence'].search(
            [('id', '=', self.doc_bancario.secuencia.id)]).next_by_id(
                self.doc_bancario.secuencia.id)

        # vals['folio_interno'] = folio_interno
        # self.folio_interno = folio_interno
        movement = self.env['account.move']
        movement_line = self.env['account.move.line']

        movement_created = movement.create({
                    'create_uid': self.env.user.id,
                    'date': fields.datetime.now(),
                    'journal_id': self.cuenta_bancaria.journal_id.id,
                    'period_id': obtener_periodo_actual(self).id,
                    'ref': self.folio_interno,
                    'state': 'draft'
                    })

        #movement_created.post()
        vals['movement'] = movement_created.id
        impuesto_id = self.doc_bancario.impuesto.id

        credit_id = None
        debit_id = None
        tax_id = None
        if not impuesto_id:
            line_debit, line_credit = set_moveline_fields(
                self, vals, movement_created.id, movement_created.name)
            if self.tipo_moneda.name == 'USD' and self.tipo_Movimiento == 'ingreso':
                credit_id = movement_line.create(line_credit)
                debit_id = movement_line.create(line_debit)
            elif self.tipo_moneda.name == 'USD' and self.tipo_Movimiento == 'egreso':
                debit_id = movement_line.create(line_debit)
                credit_id = movement_line.create(line_credit)
            else:
                credit_id = movement_line.create(line_credit)
                debit_id = movement_line.create(line_debit)
        else:
            line_debit, line_credit, line_impuesto = set_moveline_fields(
                self, vals, movement_created.id, movement_created.name)
            if self.tipo_moneda.name == 'USD' and self.tipo_Movimiento == 'ingreso':
                credit_id = movement_line.create(line_credit)
                debit_id = movement_line.create(line_debit)
                tax_id = movement_line.create(line_impuesto)
            elif self.tipo_moneda.name == 'USD' and self.tipo_Movimiento == 'egreso':
                debit_id = movement_line.create(line_debit)
                credit_id = movement_line.create(line_credit)
                tax_id = movement_line.create(line_impuesto)
            else:
                debit_id = movement_line.create(line_debit)
                credit_id = movement_line.create(line_credit)
                tax_id = movement_line.create(line_impuesto)

        if movement_created.post():
            self.movement = movement_created
            self.state = 'generado'
            credit_id.write({'name':movement_created.name})
            post_vars = {
                'subject': "Informacion",
                'body': "El movimiento <b> %s </b> ha sido validado <br>" % self.\
                    folio_interno + "Se ha creado la poliza <b> %s </b>" % self.\
                    movement.name,
            }
            self.message_post(type="notification",
                            subtype="mt_comment", **post_vars)
        else:
            raise exceptions.ValidationError(
                                    "No se puede crear una poliza con movimientos descuadrados"
                                    )

        return movement_created

    @api.one
    def boton_cancelar(self):
        """
        Se crea una Poliza invirtiendo las cuentas de debito y credito.
        Requierimientos:
        Poliza creada -- Es necesario recibir todos los valores incluidos en
                        la poliza creada con anterioridad, para solo realizar
                        la inversio de los campos y crear una poliza de cancelacion
        """
        poliza_cancelada = self.movement.copy()
        poliza_cancelada.update({
                'create_uid': self.env.user.id,
                'date': fields.datetime.now(),
                'period_id': obtener_periodo_actual(self).id,
                'ref': self.folio_interno + str(' Mov. Cancelado'),
                                })

        for line in poliza_cancelada.line_id:
            actualizar = {}
            if line.debit:
                actualizar.update({'credit':line.debit, 'debit':0})
            else:
                actualizar.update({'credit':0, 'debit':line.credit})
            if line.currency_id and line.debit:
                actualizar.update({'amount_currency': line.amount_currency * -1})
            elif line.currency_id and line.credit:
                actualizar.update({'amount_currency': line.amount_currency * -1})
            line.write(actualizar)

        vals = dict(self._context)
        vals['state'] = 'borrador'
        vals['folio_interno'] = self.folio_interno
        self.cancelado = poliza_cancelada

        self.state = 'cancelado'
        self.update({'state': self.state, 'cancelado': poliza_cancelada.id, })

        post_vars = {
            'subject': "Informacion",
            'body': "El movimiento <b> %s </b> ha sido cancelado <br>" % self.\
                folio_interno + "Se ha creado la poliza <b> %s </b>" % self.\
                cancelado.name,
        }
        self.message_post(type="notification",
                          subtype="mt_comment", **post_vars)


    @api.model
    def create(self, vals):
        cuenta_bancaria_id = vals['cuenta_bancaria']
        moneda = self.obtener_tipo_moneda(self.cuenta_bancaria.browse(
            cuenta_bancaria_id))

        if 'tipo_moneda' not in vals:
            vals['tipo_moneda'] = moneda.id

        if 'tipo_cambio' not in vals:
            vals['tipo_cambio'] = self.obtener_tipo_cambio(moneda.id)

        vals['folio_interno'] = '/'
        vals['state'] = 'borrador'

        return super(ingresos_egresos, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'cuenta_bancaria' not in vals:
            cuenta_bancaria_id = self.cuenta_bancaria.id
            moneda = self.obtener_tipo_moneda(self.cuenta_bancaria.browse(
                cuenta_bancaria_id))
        else:
            cuenta_bancaria_id=vals['cuenta_bancaria']
            moneda = self.obtener_tipo_moneda(self.cuenta_bancaria.browse(
                cuenta_bancaria_id))

        if 'tipo_moneda' not in vals:
            vals['tipo_moneda'] = moneda.id

        if 'tipo_cambio' not in vals:
            if self.tipo_cambio > 1:
                vals['tipo_cambio'] = self.tipo_cambio
            else:
                vals['tipo_cambio'] = self.obtener_tipo_cambio(moneda.id)
        else:
            if vals['tipo_cambio'] <= 1:
                vals['tipo_cambio'] = self.tipo_cambio
            else:
                tipo_cambio = vals['tipo_cambio']

        return super(ingresos_egresos, self).write(vals)

    @api.multi
    def unlink(self):
        for movimiento in self:
            if movimiento.state in (u'cancelado',u'generado'):
                raise Warning(_('No puede eliminar un movimiento que no esté en estado de borrador.'))
            if movimiento.movement:
                raise Warning(_('No se puede borrar un movimiento después de haber sido validado y se ha creado una poliza'))
            if movimiento.cancelado:
                raise Warning(_('No se puede borrar un movimiento después de haber sido cancelado y se ha creado una poliza'))
        return super(ingresos_egresos, self).unlink()
