from openerp import models,fields,api

class stock_picking(models.Model):
    _inherit='stock.picking'

    pack_operation_ids=fields.One2many('stock.pack.operation', 'picking_id', string='Related Packing Operations',states={'cancel': [('readonly', True)]})
    amazon_instance_id = fields.Many2one("amazon.instance.ept","Instances")

    
    @api.model
    def _create_invoice_from_picking(self,picking, vals):
        if picking.sale_id:
            amazon_order = self.env['amazon.sale.order.ept'].search([('sale_order_id','=',picking.sale_id.id),('amazon_reference','!=',False)])
            amazon_order and vals.update({'amazon_instance_id':amazon_order.instance_id.id})
        return super(stock_picking,self)._create_invoice_from_picking(picking,vals)
    
    
    @api.one
    @api.depends("group_id")
    def get_amazon_orders(self):
        sale_obj=self.env['sale.order']
        amazon_sale_obj=self.env['amazon.sale.order.ept']
        for record in self:
            if record.picking_type_id and record.picking_type_id.code=='outgoing' and record.group_id:
                sale_order=sale_obj.search([('procurement_group_id', '=', record.group_id.id)])
                amazon_order=sale_order and amazon_sale_obj.search([('sale_order_id','=',sale_order.id),('amazon_reference','!=',False)])
                if amazon_order:
                    record.is_amazon_delivery_order=True
                else:
                    record.is_amazon_delivery_order=False

    is_amazon_delivery_order = fields.Boolean("Amazon Delivery Order",compute="get_amazon_orders",store=True)    
    updated_in_amazon=fields.Boolean("Updated In Amazon",default=False,copy=False)
    
    @api.multi
    def mark_sent_amazon(self):
        for picking in self:
            picking.write({'updated_in_amazon':False})
        return True
    @api.multi
    def mark_not_sent_amazon(self):
        for picking in self:
            picking.write({'updated_in_amazon':True})
        return True

    
