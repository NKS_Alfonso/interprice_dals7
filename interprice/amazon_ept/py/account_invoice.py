from openerp import models,api,fields
class account_invoice(models.Model):
    _inherit="account.invoice"
    
    amazon_instance_id = fields.Many2one("amazon.instance.ept","Instances")
    @api.model
    def send_amazon_invoice_via_email(self):
        instance_obj=self.env['amazon.instance.ept']
        seller_obj=self.env['amazon.seller.ept']
        invoice_obj=self.env['account.invoice']
        ctx = dict(self._context) or {}
        seller_id = ctx.get('seller_id',False)
        if seller_id:
            seller = seller_obj.search([('id','=',seller_id)])
            if not seller:
                return True
            email_template= self.env.ref('account.email_template_edi_invoice', False)
            instances = instance_obj.search([('seller_id','=',seller.id)])
            for instance in instances:
                invoices=invoice_obj.search([('amazon_instance_id','=',instance.id),('type','=','out_invoice'),('state','in',['open','paid']),('sent','=',False)])
                for x in xrange(0, len(invoices),10):           
                    invs = invoices[x:x +10]  
                    for invoice in invs:
                        email_template.send_mail(invoice.id)
                        invoice.write({'sent':True})                                    
                    self._cr.commit()
        return True
