from openerp import models,fields,api
from ..amazon_emipro_api.mws import Sellers
from datetime import datetime
class amazon_seller_ept(models.Model):
    _name = "amazon.seller.ept"
    
#     def _get_default_company(self):
#         company_id = self.env.user._get_company()
#         if not company_id:
#             raise Warning(_('There is no default company for the current user!'))
#         return company_id    

    def _get_default_company(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        return company_id
    
    name = fields.Char(size=120, string='Name', required=True)
    access_key = fields.Char("Access Key")
    secret_key = fields.Char("Secret Key")
    merchant_id = fields.Char("Merchant Id")
    company_id = fields.Many2one('res.company',string='Company', required=True)
    #company_id = fields.Many2one('res.company',string='Company', required=True,default=_get_default_company)    
    pro_advt_access_key = fields.Char("Access Key")
    pro_advt_scrt_access_key = fields.Char("Secret Access Key")
    pro_advt_associate_tag = fields.Char("Associate Tag")
    
    country_id = fields.Many2one('res.country',string = "Region", domain="[('amazon_marketplace_code','!=',False)]")
    warehouse_ids = fields.One2many('stock.warehouse','seller_id', string='Warehouses')
    marketplace_ids = fields.One2many('amazon.marketplace.ept', 'seller_id', string='Marketplaces')
    order_auto_import = fields.Boolean(string='Auto Order Import?')
    order_last_sync_on = fields.Datetime("Last Order Sync Time")
    stock_auto_export=fields.Boolean(string="Stock Auto Export?")
    settlement_report_auto_create = fields.Boolean("Auto Create Settlement Report ?",default=False)
    settlement_report_auto_process = fields.Boolean("Auto Process Settlement Report ?",default=False)
    auto_send_invoice=fields.Boolean("Auto Send Invoice Via Email ?",default=False)
    shipment_last_sync_on = fields.Datetime("Last Shipment Sync Time")
    order_auto_update = fields.Boolean("Auto Update Order Shipment ?",default=False)
    settlement_report_last_sync_on = fields.Datetime("Settlement Report Last Sync Time")
    transaction_line_ids = fields.One2many('amazon.transaction.line.ept','seller_id','Transactions')
    
    create_new_product = fields.Boolean('Allow to create new product if not found in odoo ?', default=False)
    
    _defaults = {
                 'company_id': _get_default_company,
                 }

    @api.model
    def auto_import_sale_order_ept(self):
        amazon_sale_order_obj = self.env['amazon.sale.order.ept']
        ctx = dict(self._context) or {}
        seller_id = ctx.get('seller_id',False)
        if seller_id:
            seller = self.search([('id','=',seller_id)])
            amazon_sale_order_obj.import_sales_order(seller)
            seller.write({'order_last_sync_on':datetime.now()})
        return True


    @api.model
    def auto_update_order_status_ept(self):
        amazon_sale_order_obj=self.env['amazon.sale.order.ept']
        ctx = dict(self._context) or {}
        seller_id = ctx.get('seller_id',False)
        if seller_id:
            seller = self.search([('id','=',seller_id)])
            amazon_sale_order_obj.update_order_status(seller)
            seller.write({'shipment_last_sync_on':datetime.now()})
        return True

    @api.model
    def auto_export_inventory_ept(self):
        amazon_product_obj = self.env['amazon.product.ept']
        ctx = dict(self._context) or {}
        seller_id = ctx.get('seller_id',False)
        if seller_id:
            seller = self.search([('id','=',seller_id)])
            if not seller:
                return True
            instances = self.env['amazon.instance.ept'].search([('seller_id','=',seller.id)])
            for instance in instances:
                amazon_product_obj.export_stock_levels(instance)
                instance.write({'inventory_last_sync_on':datetime.now()})
        return True
                
    @api.multi
    def load_marketplace(self):
        mws_obj = Sellers(access_key=str(self.access_key),secret_key=str(self.secret_key),account_id=str(self.merchant_id),region=self.country_id.amazon_marketplace_code or self.country_id.code)
        marketplace_obj = self.env['amazon.marketplace.ept']
        currency_obj = self.env['res.currency']
        lang_obj = self.env['res.lang']
        country_obj = self.env['res.country']
        
        list_of_wrapper=[]
        try:
            result = mws_obj.list_marketplace_participations()
            result.parsed.get('ListParticipations',{})
        except Exception,e:
            raise Warning('Given Credentials is incorrect, please provide correct Credentials.')
        
        list_of_wrapper.append(result)
        next_token=result.parsed.get('NextToken',{}).get('value')
        while next_token:
            try:
                result=mws_obj.list_marketplace_participations_by_next_token(next_token)
            except Exception,e:
                raise Warning(str(e))
            next_token=result.parsed.get('NextToken',{}).get('value')
            list_of_wrapper.append(result)                
        
        for wrapper_obj in list_of_wrapper:
            participations = wrapper_obj.parsed.get('ListParticipations',{}).get('Participation',[])
            if not isinstance(participations,list):
                participations=[participations]
                
            marketplaces = wrapper_obj.parsed.get('ListMarketplaces',{}).get('Marketplace',[])
            if not isinstance(marketplaces,list):
                marketplaces=[marketplaces]

            participations_dict = dict(map(lambda x:(x.get('MarketplaceId',{}).get('value',''), x.get('SellerId',{}).get('value',False)),participations))
            for marketplace in marketplaces:
                country_code = marketplace.get('DefaultCountryCode',{}).get('value')
                name = marketplace.get('Name',{}).get('value','')
                domain = marketplace.get('DomainName',{}).get('value','')
                land_code = marketplace.get('DefaultLanguageCode',{}).get('value','')
                currency_code = marketplace.get('DefaultCurrencyCode',{}).get('value','')
                marketplace_id = marketplace.get('MarketplaceId',{}).get('value','')
                currency_id = currency_obj.search([('name','=',currency_code)])
                lang_id = lang_obj.search([('code','=',land_code)])
                country_id = country_obj.search([('code','=',country_code)])
                vals = {
                        'seller_id':self.id,
                        'name' : name,
                        'market_place_id':marketplace_id,
                        'is_participated':participations_dict.get(marketplace_id,False),
                        'domain' : domain,
                        'currency_id' : currency_id and currency_id[0].id or False,
                        'lang_id' : lang_id and lang_id[0].id or False,
                        'country_id' : country_id and country_id[0].id or self.country_id and self.country_id.id or False
                        }
                marketplace_rec = marketplace_obj.search([('seller_id','=',self.id),('market_place_id','=',marketplace_id)])
                if marketplace_rec:
                    marketplace_rec.write(vals)
                else:
                    marketplace_obj.create(vals)
        return True
        
