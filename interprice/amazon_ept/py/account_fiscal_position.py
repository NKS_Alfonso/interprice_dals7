from openerp.osv import fields, osv

class account_fiscal_position(osv.osv):
    _inherit = 'account.fiscal.position'
    
    _columns = {
                'origin_country_ept':fields.many2one('res.country',string='Origin Country'),
                }
    
    
    def get_fiscal_position(self, cr, uid, company_id, partner_id, delivery_id=None, context=None):
        result = super(account_fiscal_position,self).get_fiscal_position(cr,uid,company_id,partner_id,delivery_id=delivery_id,context=context)
        context = context and context or {}
        origin_country_ept = context.get('origin_country_ept',False)
        if not origin_country_ept or (not delivery_id and not partner_id):
            return result
            
        # This can be easily overriden to apply more complex fiscal rules
        part_obj = self.pool['res.partner']
        partner = part_obj.browse(cr, uid, partner_id, context=context)
        
        # if no delivery use invocing
        if delivery_id:
            delivery = part_obj.browse(cr, uid, delivery_id, context=context)
        else:
            delivery = partner
        
        # partner manually set fiscal position always win
        if delivery.property_account_position or partner.property_account_position:
            return delivery.property_account_position.id or partner.property_account_position.id
            
        domain = [
            ('auto_apply', '=', True),'|',('origin_country_ept','=',False),('origin_country_ept','=',origin_country_ept),
            '|', ('vat_required', '=', False), ('vat_required', '=', partner.vat_subjected),
        ]
        
        fiscal_position_ids = self.search(cr, uid, domain + [('country_id', '=', delivery.country_id.id)], context=context, limit=1)
        if fiscal_position_ids:
            return fiscal_position_ids[0]

        fiscal_position_ids = self.search(cr, uid, domain + [('country_group_id.country_ids', '=', delivery.country_id.id)], context=context, limit=1)
        if fiscal_position_ids:
            return fiscal_position_ids[0]

        fiscal_position_ids = self.search(cr, uid, domain + [('country_id', '=', None), ('country_group_id', '=', None)], context=context, limit=1)
        if fiscal_position_ids:
            return fiscal_position_ids[0]
        return False
