# ./categ_xsd.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:e92452c8d3e28a9e27abfc9994d2007779e7f4c9
# Generated 2016-06-15 11:27:53.351628 by PyXB version 1.2.4 using Python 2.7.6.final.0
# Namespace AbsentNamespace0

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six

# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:3232c51a-32ec-11e6-a508-b0c09032edbe')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.4'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.CreateAbsentNamespace()
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: [anonymous]
class STD_ANON (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 26, 4)
    _Documentation = None
STD_ANON._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(10))
STD_ANON._InitializeFacetMap(STD_ANON._CF_maxLength)

# Atomic simple type: [anonymous]
class STD_ANON_ (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 50, 4)
    _Documentation = None
STD_ANON_._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_._InitializeFacetMap(STD_ANON_._CF_minLength,
   STD_ANON_._CF_maxLength)

# Atomic simple type: [anonymous]
class STD_ANON_2 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 98, 4)
    _Documentation = None
STD_ANON_2._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_2._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_2._InitializeFacetMap(STD_ANON_2._CF_minLength,
   STD_ANON_2._CF_maxLength)

# Atomic simple type: AddressLine
class AddressLine (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AddressLine')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 108, 1)
    _Documentation = None
AddressLine._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(60))
AddressLine._InitializeFacetMap(AddressLine._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'AddressLine', AddressLine)

# Atomic simple type: [anonymous]
class STD_ANON_3 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 121, 5)
    _Documentation = None
STD_ANON_3._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_3, enum_prefix=None)
STD_ANON_3.Voice = STD_ANON_3._CF_enumeration.addEnumeration(unicode_value='Voice', tag='Voice')
STD_ANON_3.Fax = STD_ANON_3._CF_enumeration.addEnumeration(unicode_value='Fax', tag='Fax')
STD_ANON_3._InitializeFacetMap(STD_ANON_3._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_4 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 129, 5)
    _Documentation = None
STD_ANON_4._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(30))
STD_ANON_4._InitializeFacetMap(STD_ANON_4._CF_maxLength)

# Atomic simple type: [anonymous]
class STD_ANON_5 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 149, 5)
    _Documentation = None
STD_ANON_5._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_5, enum_prefix=None)
STD_ANON_5.TextOnly = STD_ANON_5._CF_enumeration.addEnumeration(unicode_value='TextOnly', tag='TextOnly')
STD_ANON_5.HTML = STD_ANON_5._CF_enumeration.addEnumeration(unicode_value='HTML', tag='HTML')
STD_ANON_5._InitializeFacetMap(STD_ANON_5._CF_enumeration)

# Atomic simple type: EmailBase
class EmailBase (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EmailBase')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 159, 1)
    _Documentation = None
EmailBase._CF_pattern = pyxb.binding.facets.CF_pattern()
EmailBase._CF_pattern.addPattern(pattern='[^@]+@[^@\\.]+(\\.[^@\\.]+)+')
EmailBase._InitializeFacetMap(EmailBase._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'EmailBase', EmailBase)

# Atomic simple type: [anonymous]
class STD_ANON_6 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 175, 7)
    _Documentation = None
STD_ANON_6._InitializeFacetMap()

# Atomic simple type: [anonymous]
class STD_ANON_7 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 245, 7)
    _Documentation = None
STD_ANON_7._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_7, enum_prefix=None)
STD_ANON_7.Principal = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='Principal', tag='Principal')
STD_ANON_7.Shipping = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='Shipping', tag='Shipping')
STD_ANON_7.CODFee = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='CODFee', tag='CODFee')
STD_ANON_7.Tax = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='Tax', tag='Tax')
STD_ANON_7.ShippingTax = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='ShippingTax', tag='ShippingTax')
STD_ANON_7.RestockingFee = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='RestockingFee', tag='RestockingFee')
STD_ANON_7.RestockingFeeTax = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='RestockingFeeTax', tag='RestockingFeeTax')
STD_ANON_7.GiftWrap = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='GiftWrap', tag='GiftWrap')
STD_ANON_7.GiftWrapTax = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='GiftWrapTax', tag='GiftWrapTax')
STD_ANON_7.Surcharge = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='Surcharge', tag='Surcharge')
STD_ANON_7.ReturnShipping = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='ReturnShipping', tag='ReturnShipping')
STD_ANON_7.Goodwill = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='Goodwill', tag='Goodwill')
STD_ANON_7.ExportCharge = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='ExportCharge', tag='ExportCharge')
STD_ANON_7.COD = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='COD', tag='COD')
STD_ANON_7.CODTax = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='CODTax', tag='CODTax')
STD_ANON_7.Other = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='Other', tag='Other')
STD_ANON_7.FreeReplacementReturnShipping = STD_ANON_7._CF_enumeration.addEnumeration(unicode_value='FreeReplacementReturnShipping', tag='FreeReplacementReturnShipping')
STD_ANON_7._InitializeFacetMap(STD_ANON_7._CF_enumeration)

# Atomic simple type: BaseCurrencyCode
class BaseCurrencyCode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BaseCurrencyCode')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 310, 1)
    _Documentation = None
BaseCurrencyCode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BaseCurrencyCode, enum_prefix=None)
BaseCurrencyCode.USD = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='USD', tag='USD')
BaseCurrencyCode.GBP = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='GBP', tag='GBP')
BaseCurrencyCode.EUR = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='EUR', tag='EUR')
BaseCurrencyCode.JPY = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='JPY', tag='JPY')
BaseCurrencyCode.CAD = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CAD', tag='CAD')
BaseCurrencyCode.CNY = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CNY', tag='CNY')
BaseCurrencyCode.INR = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='INR', tag='INR')
BaseCurrencyCode.MXN = BaseCurrencyCode._CF_enumeration.addEnumeration(unicode_value='MXN', tag='MXN')
BaseCurrencyCode._InitializeFacetMap(BaseCurrencyCode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BaseCurrencyCode', BaseCurrencyCode)

# Atomic simple type: GlobalCurrencyCode
class GlobalCurrencyCode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'GlobalCurrencyCode')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 322, 1)
    _Documentation = None
GlobalCurrencyCode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=GlobalCurrencyCode, enum_prefix=None)
GlobalCurrencyCode.AED = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='AED', tag='AED')
GlobalCurrencyCode.ALL = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ALL', tag='ALL')
GlobalCurrencyCode.ARS = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ARS', tag='ARS')
GlobalCurrencyCode.ATS = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ATS', tag='ATS')
GlobalCurrencyCode.AUD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='AUD', tag='AUD')
GlobalCurrencyCode.BAM = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BAM', tag='BAM')
GlobalCurrencyCode.BEF = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BEF', tag='BEF')
GlobalCurrencyCode.BGN = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BGN', tag='BGN')
GlobalCurrencyCode.BHD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BHD', tag='BHD')
GlobalCurrencyCode.BOB = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BOB', tag='BOB')
GlobalCurrencyCode.BRL = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BRL', tag='BRL')
GlobalCurrencyCode.BYR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='BYR', tag='BYR')
GlobalCurrencyCode.CAD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CAD', tag='CAD')
GlobalCurrencyCode.CHF = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CHF', tag='CHF')
GlobalCurrencyCode.CLP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CLP', tag='CLP')
GlobalCurrencyCode.CNY = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CNY', tag='CNY')
GlobalCurrencyCode.COP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='COP', tag='COP')
GlobalCurrencyCode.CRC = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CRC', tag='CRC')
GlobalCurrencyCode.CSD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CSD', tag='CSD')
GlobalCurrencyCode.CZK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='CZK', tag='CZK')
GlobalCurrencyCode.DEM = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='DEM', tag='DEM')
GlobalCurrencyCode.DKK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='DKK', tag='DKK')
GlobalCurrencyCode.DOP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='DOP', tag='DOP')
GlobalCurrencyCode.DZD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='DZD', tag='DZD')
GlobalCurrencyCode.EEK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='EEK', tag='EEK')
GlobalCurrencyCode.EGP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='EGP', tag='EGP')
GlobalCurrencyCode.ESP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ESP', tag='ESP')
GlobalCurrencyCode.EUR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='EUR', tag='EUR')
GlobalCurrencyCode.FIM = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='FIM', tag='FIM')
GlobalCurrencyCode.FRF = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='FRF', tag='FRF')
GlobalCurrencyCode.GBP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='GBP', tag='GBP')
GlobalCurrencyCode.GRD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='GRD', tag='GRD')
GlobalCurrencyCode.GTQ = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='GTQ', tag='GTQ')
GlobalCurrencyCode.HKD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='HKD', tag='HKD')
GlobalCurrencyCode.HNL = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='HNL', tag='HNL')
GlobalCurrencyCode.HRK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='HRK', tag='HRK')
GlobalCurrencyCode.HUF = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='HUF', tag='HUF')
GlobalCurrencyCode.IDR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='IDR', tag='IDR')
GlobalCurrencyCode.ILS = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ILS', tag='ILS')
GlobalCurrencyCode.INR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='INR', tag='INR')
GlobalCurrencyCode.IQD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='IQD', tag='IQD')
GlobalCurrencyCode.ISK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ISK', tag='ISK')
GlobalCurrencyCode.ITL = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ITL', tag='ITL')
GlobalCurrencyCode.JOD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='JOD', tag='JOD')
GlobalCurrencyCode.JPY = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='JPY', tag='JPY')
GlobalCurrencyCode.KRW = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='KRW', tag='KRW')
GlobalCurrencyCode.KWD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='KWD', tag='KWD')
GlobalCurrencyCode.LBP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='LBP', tag='LBP')
GlobalCurrencyCode.LTL = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='LTL', tag='LTL')
GlobalCurrencyCode.LUF = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='LUF', tag='LUF')
GlobalCurrencyCode.LVL = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='LVL', tag='LVL')
GlobalCurrencyCode.LYD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='LYD', tag='LYD')
GlobalCurrencyCode.MAD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='MAD', tag='MAD')
GlobalCurrencyCode.MKD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='MKD', tag='MKD')
GlobalCurrencyCode.MXN = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='MXN', tag='MXN')
GlobalCurrencyCode.MYR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='MYR', tag='MYR')
GlobalCurrencyCode.NIO = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='NIO', tag='NIO')
GlobalCurrencyCode.NOK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='NOK', tag='NOK')
GlobalCurrencyCode.NZD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='NZD', tag='NZD')
GlobalCurrencyCode.OMR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='OMR', tag='OMR')
GlobalCurrencyCode.PAB = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='PAB', tag='PAB')
GlobalCurrencyCode.PEN = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='PEN', tag='PEN')
GlobalCurrencyCode.PHP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='PHP', tag='PHP')
GlobalCurrencyCode.PLN = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='PLN', tag='PLN')
GlobalCurrencyCode.PTE = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='PTE', tag='PTE')
GlobalCurrencyCode.PYG = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='PYG', tag='PYG')
GlobalCurrencyCode.QAR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='QAR', tag='QAR')
GlobalCurrencyCode.RON = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='RON', tag='RON')
GlobalCurrencyCode.RSD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='RSD', tag='RSD')
GlobalCurrencyCode.RUB = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='RUB', tag='RUB')
GlobalCurrencyCode.SAR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SAR', tag='SAR')
GlobalCurrencyCode.SDG = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SDG', tag='SDG')
GlobalCurrencyCode.SEK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SEK', tag='SEK')
GlobalCurrencyCode.SGD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SGD', tag='SGD')
GlobalCurrencyCode.SKK = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SKK', tag='SKK')
GlobalCurrencyCode.SVC = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SVC', tag='SVC')
GlobalCurrencyCode.SYP = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='SYP', tag='SYP')
GlobalCurrencyCode.THB = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='THB', tag='THB')
GlobalCurrencyCode.TND = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='TND', tag='TND')
GlobalCurrencyCode.TRY = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='TRY', tag='TRY')
GlobalCurrencyCode.TWD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='TWD', tag='TWD')
GlobalCurrencyCode.UAH = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='UAH', tag='UAH')
GlobalCurrencyCode.USD = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='USD', tag='USD')
GlobalCurrencyCode.UYU = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='UYU', tag='UYU')
GlobalCurrencyCode.VEF = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='VEF', tag='VEF')
GlobalCurrencyCode.VND = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='VND', tag='VND')
GlobalCurrencyCode.YER = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='YER', tag='YER')
GlobalCurrencyCode.ZAR = GlobalCurrencyCode._CF_enumeration.addEnumeration(unicode_value='ZAR', tag='ZAR')
GlobalCurrencyCode._InitializeFacetMap(GlobalCurrencyCode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'GlobalCurrencyCode', GlobalCurrencyCode)

# Atomic simple type: EnergyUnitOfMeasure
class EnergyUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnergyUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 414, 1)
    _Documentation = None
EnergyUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=EnergyUnitOfMeasure, enum_prefix=None)
EnergyUnitOfMeasure.BTU = EnergyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='BTU', tag='BTU')
EnergyUnitOfMeasure.watts = EnergyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='watts', tag='watts')
EnergyUnitOfMeasure.joules = EnergyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='joules', tag='joules')
EnergyUnitOfMeasure.kilojoules = EnergyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilojoules', tag='kilojoules')
EnergyUnitOfMeasure._InitializeFacetMap(EnergyUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'EnergyUnitOfMeasure', EnergyUnitOfMeasure)

# Atomic simple type: WaterResistantType
class WaterResistantType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WaterResistantType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 422, 1)
    _Documentation = None
WaterResistantType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=WaterResistantType, enum_prefix=None)
WaterResistantType.not_water_resistant = WaterResistantType._CF_enumeration.addEnumeration(unicode_value='not_water_resistant', tag='not_water_resistant')
WaterResistantType.water_resistant = WaterResistantType._CF_enumeration.addEnumeration(unicode_value='water_resistant', tag='water_resistant')
WaterResistantType.waterproof = WaterResistantType._CF_enumeration.addEnumeration(unicode_value='waterproof', tag='waterproof')
WaterResistantType._InitializeFacetMap(WaterResistantType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'WaterResistantType', WaterResistantType)

# Atomic simple type: AntennaTypeValues
class AntennaTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AntennaTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 429, 1)
    _Documentation = None
AntennaTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AntennaTypeValues, enum_prefix=None)
AntennaTypeValues.fixed = AntennaTypeValues._CF_enumeration.addEnumeration(unicode_value='fixed', tag='fixed')
AntennaTypeValues.internal = AntennaTypeValues._CF_enumeration.addEnumeration(unicode_value='internal', tag='internal')
AntennaTypeValues.retractable = AntennaTypeValues._CF_enumeration.addEnumeration(unicode_value='retractable', tag='retractable')
AntennaTypeValues._InitializeFacetMap(AntennaTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AntennaTypeValues', AntennaTypeValues)

# Atomic simple type: ModemTypeValues
class ModemTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ModemTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 436, 1)
    _Documentation = None
ModemTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ModemTypeValues, enum_prefix=None)
ModemTypeValues.analog_modem = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='analog_modem', tag='analog_modem')
ModemTypeValues.data_fax_voice = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='data_fax_voice', tag='data_fax_voice')
ModemTypeValues.isdn_modem = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='isdn_modem', tag='isdn_modem')
ModemTypeValues.cable = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='cable', tag='cable')
ModemTypeValues.data_modem = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='data_modem', tag='data_modem')
ModemTypeValues.network_modem = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='network_modem', tag='network_modem')
ModemTypeValues.cellular = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='cellular', tag='cellular')
ModemTypeValues.digital = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='digital', tag='digital')
ModemTypeValues.unknown_modem_type = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='unknown_modem_type', tag='unknown_modem_type')
ModemTypeValues.csu = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='csu', tag='csu')
ModemTypeValues.dsl = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='dsl', tag='dsl')
ModemTypeValues.voice = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='voice', tag='voice')
ModemTypeValues.data_fax = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='data_fax', tag='data_fax')
ModemTypeValues.dsu = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='dsu', tag='dsu')
ModemTypeValues.winmodem = ModemTypeValues._CF_enumeration.addEnumeration(unicode_value='winmodem', tag='winmodem')
ModemTypeValues._InitializeFacetMap(ModemTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ModemTypeValues', ModemTypeValues)

# Atomic simple type: DockingStationExternalInterfaceTypeValues
class DockingStationExternalInterfaceTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DockingStationExternalInterfaceTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 455, 1)
    _Documentation = None
DockingStationExternalInterfaceTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DockingStationExternalInterfaceTypeValues, enum_prefix=None)
DockingStationExternalInterfaceTypeValues.firewire_1600 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='firewire_1600', tag='firewire_1600')
DockingStationExternalInterfaceTypeValues.firewire_3200 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='firewire_3200', tag='firewire_3200')
DockingStationExternalInterfaceTypeValues.firewire_400 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='firewire_400', tag='firewire_400')
DockingStationExternalInterfaceTypeValues.firewire_800 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='firewire_800', tag='firewire_800')
DockingStationExternalInterfaceTypeValues.firewire_esata = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='firewire_esata', tag='firewire_esata')
DockingStationExternalInterfaceTypeValues.usb1_0 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb1.0', tag='usb1_0')
DockingStationExternalInterfaceTypeValues.usb1_1 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb1.1', tag='usb1_1')
DockingStationExternalInterfaceTypeValues.usb2_0 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb2.0', tag='usb2_0')
DockingStationExternalInterfaceTypeValues.usb3_0 = DockingStationExternalInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb3.0', tag='usb3_0')
DockingStationExternalInterfaceTypeValues._InitializeFacetMap(DockingStationExternalInterfaceTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DockingStationExternalInterfaceTypeValues', DockingStationExternalInterfaceTypeValues)

# Atomic simple type: RemovableMemoryValues
class RemovableMemoryValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemovableMemoryValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 468, 1)
    _Documentation = None
RemovableMemoryValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=RemovableMemoryValues, enum_prefix=None)
RemovableMemoryValues.n1_4_inch_audio = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='1_4_inch_audio', tag='n1_4_inch_audio')
RemovableMemoryValues.n2_5_mm_audio = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='2_5_mm_audio', tag='n2_5_mm_audio')
RemovableMemoryValues.n3_0_v_ttl = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='3.0_v_ttl', tag='n3_0_v_ttl')
RemovableMemoryValues.n3_5_floppy = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='3_5_floppy', tag='n3_5_floppy')
RemovableMemoryValues.n3_5_mm_audio = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='3_5_mm_audio', tag='n3_5_mm_audio')
RemovableMemoryValues.ata = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ata', tag='ata')
RemovableMemoryValues.ata_flash_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ata_flash_card', tag='ata_flash_card')
RemovableMemoryValues.audio_video_port = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='audio_video_port', tag='audio_video_port')
RemovableMemoryValues.bluetooth = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='bluetooth', tag='bluetooth')
RemovableMemoryValues.built_in_flash_memory = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='built_in_flash_memory', tag='built_in_flash_memory')
RemovableMemoryValues.cd_r = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='cd-r', tag='cd_r')
RemovableMemoryValues.cd_rw = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='cd_rw', tag='cd_rw')
RemovableMemoryValues.cdr_drive = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='cdr_drive', tag='cdr_drive')
RemovableMemoryValues.compact_disc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='compact_disc', tag='compact_disc')
RemovableMemoryValues.compact_flash_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='compact_flash_card', tag='compact_flash_card')
RemovableMemoryValues.compact_flash_type_i_or_ii = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='compact_flash_type_i_or_ii', tag='compact_flash_type_i_or_ii')
RemovableMemoryValues.compactflash_type_i = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='compactflash_type_i', tag='compactflash_type_i')
RemovableMemoryValues.compactflash_type_ii = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='compactflash_type_ii', tag='compactflash_type_ii')
RemovableMemoryValues.component_video = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='component_video', tag='component_video')
RemovableMemoryValues.composite_video = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='composite_video', tag='composite_video')
RemovableMemoryValues.d_sub = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='d_sub', tag='d_sub')
RemovableMemoryValues.dmi = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dmi', tag='dmi')
RemovableMemoryValues.dssi = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dssi', tag='dssi')
RemovableMemoryValues.dvd_r = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dvd_r', tag='dvd_r')
RemovableMemoryValues.dvd_rw = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dvd_rw', tag='dvd_rw')
RemovableMemoryValues.dvi_x_1 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dvi_x_1', tag='dvi_x_1')
RemovableMemoryValues.dvi_x_2 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dvi_x_2', tag='dvi_x_2')
RemovableMemoryValues.dvi_x_4 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='dvi_x_4', tag='dvi_x_4')
RemovableMemoryValues.eide = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='eide', tag='eide')
RemovableMemoryValues.eisa = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='eisa', tag='eisa')
RemovableMemoryValues.ethernet = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ethernet', tag='ethernet')
RemovableMemoryValues.express_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='express_card', tag='express_card')
RemovableMemoryValues.fibre_channel = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='fibre_channel', tag='fibre_channel')
RemovableMemoryValues.firewire_1600 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='firewire_1600', tag='firewire_1600')
RemovableMemoryValues.firewire_3200 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='firewire_3200', tag='firewire_3200')
RemovableMemoryValues.firewire_400 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='firewire_400', tag='firewire_400')
RemovableMemoryValues.firewire_800 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='firewire_800', tag='firewire_800')
RemovableMemoryValues.firewire_esata = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='firewire_esata', tag='firewire_esata')
RemovableMemoryValues.game_port = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='game_port', tag='game_port')
RemovableMemoryValues.gbic = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='gbic', tag='gbic')
RemovableMemoryValues.hdmi = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='hdmi', tag='hdmi')
RemovableMemoryValues.headphone = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='headphone', tag='headphone')
RemovableMemoryValues.hp_hsc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='hp_hsc', tag='hp_hsc')
RemovableMemoryValues.hp_pb = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='hp_pb', tag='hp_pb')
RemovableMemoryValues.hs_mmc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='hs_mmc', tag='hs_mmc')
RemovableMemoryValues.ibm_microdrive = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ibm_microdrive', tag='ibm_microdrive')
RemovableMemoryValues.ide = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ide', tag='ide')
RemovableMemoryValues.ieee_1284 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ieee_1284', tag='ieee_1284')
RemovableMemoryValues.ieee_1394 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ieee_1394', tag='ieee_1394')
RemovableMemoryValues.infrared = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='infrared', tag='infrared')
RemovableMemoryValues.internal_w_removable_media = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='internal_w_removable_media', tag='internal_w_removable_media')
RemovableMemoryValues.iomega_clik_disk = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='iomega_clik_disk', tag='iomega_clik_disk')
RemovableMemoryValues.isa = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='isa', tag='isa')
RemovableMemoryValues.isp = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='isp', tag='isp')
RemovableMemoryValues.lanc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='lanc', tag='lanc')
RemovableMemoryValues.mca = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mca', tag='mca')
RemovableMemoryValues.media_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='media_card', tag='media_card')
RemovableMemoryValues.memory_stick = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick', tag='memory_stick')
RemovableMemoryValues.memory_stick_duo = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_duo', tag='memory_stick_duo')
RemovableMemoryValues.memory_stick_micro = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_micro', tag='memory_stick_micro')
RemovableMemoryValues.memory_stick_pro = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_pro', tag='memory_stick_pro')
RemovableMemoryValues.memory_stick_pro_duo = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_pro_duo', tag='memory_stick_pro_duo')
RemovableMemoryValues.memory_stick_pro_hg_duo = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_pro_hg_duo', tag='memory_stick_pro_hg_duo')
RemovableMemoryValues.memory_stick_select = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_select', tag='memory_stick_select')
RemovableMemoryValues.memory_stick_xc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_xc', tag='memory_stick_xc')
RemovableMemoryValues.memory_stick_xc_hg_micro = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_xc_hg_micro', tag='memory_stick_xc_hg_micro')
RemovableMemoryValues.memory_stick_xc_micro = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_xc_micro', tag='memory_stick_xc_micro')
RemovableMemoryValues.micard = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='micard', tag='micard')
RemovableMemoryValues.micro_sdhc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='micro_sdhc', tag='micro_sdhc')
RemovableMemoryValues.micro_sdxc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='micro_sdxc', tag='micro_sdxc')
RemovableMemoryValues.microsd = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='microsd', tag='microsd')
RemovableMemoryValues.mini_dvd = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mini_dvd', tag='mini_dvd')
RemovableMemoryValues.mini_hdmi = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mini_hdmi', tag='mini_hdmi')
RemovableMemoryValues.mini_pci = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mini_pci', tag='mini_pci')
RemovableMemoryValues.mini_sdhc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mini_sdhc', tag='mini_sdhc')
RemovableMemoryValues.mini_sdxc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mini_sdxc', tag='mini_sdxc')
RemovableMemoryValues.minisd = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='minisd', tag='minisd')
RemovableMemoryValues.mmc_micro = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='mmc_micro', tag='mmc_micro')
RemovableMemoryValues.multimedia_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='multimedia_card', tag='multimedia_card')
RemovableMemoryValues.multimedia_card_mobile = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='multimedia_card_mobile', tag='multimedia_card_mobile')
RemovableMemoryValues.multimedia_card_plus = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='multimedia_card_plus', tag='multimedia_card_plus')
RemovableMemoryValues.multipronged_audio = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='multipronged_audio', tag='multipronged_audio')
RemovableMemoryValues.nubus = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='nubus', tag='nubus')
RemovableMemoryValues.parallel_interface = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='parallel_interface', tag='parallel_interface')
RemovableMemoryValues.pc_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pc_card', tag='pc_card')
RemovableMemoryValues.pci = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci', tag='pci')
RemovableMemoryValues.pci_64 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_64', tag='pci_64')
RemovableMemoryValues.pci_64_hot_plug = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_64_hot_plug', tag='pci_64_hot_plug')
RemovableMemoryValues.pci_64_hot_plug_33_mhz = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_64_hot_plug_33_mhz', tag='pci_64_hot_plug_33_mhz')
RemovableMemoryValues.pci_64_hot_plug_66_mhz = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_64_hot_plug_66_mhz', tag='pci_64_hot_plug_66_mhz')
RemovableMemoryValues.pci_express_x4 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x4', tag='pci_express_x4')
RemovableMemoryValues.pci_express_x8 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x8', tag='pci_express_x8')
RemovableMemoryValues.pci_hot_plug = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_hot_plug', tag='pci_hot_plug')
RemovableMemoryValues.pci_raid = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_raid', tag='pci_raid')
RemovableMemoryValues.pci_x = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x', tag='pci_x')
RemovableMemoryValues.pci_x_1 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_1', tag='pci_x_1')
RemovableMemoryValues.pci_x_100_mhz = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_100_mhz', tag='pci_x_100_mhz')
RemovableMemoryValues.pci_x_133_mhz = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_133_mhz', tag='pci_x_133_mhz')
RemovableMemoryValues.pci_x_16 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_16', tag='pci_x_16')
RemovableMemoryValues.pci_x_16_gb = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_16_gb', tag='pci_x_16_gb')
RemovableMemoryValues.pci_x_4 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_4', tag='pci_x_4')
RemovableMemoryValues.pci_x_66_mhz = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_66_mhz', tag='pci_x_66_mhz')
RemovableMemoryValues.pci_x_8 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_8', tag='pci_x_8')
RemovableMemoryValues.pci_x_hot_plug = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_hot_plug', tag='pci_x_hot_plug')
RemovableMemoryValues.pci_x_hot_plug_133_mhz = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pci_x_hot_plug_133_mhz', tag='pci_x_hot_plug_133_mhz')
RemovableMemoryValues.pcmcia = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pcmcia', tag='pcmcia')
RemovableMemoryValues.pcmcia_ii = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pcmcia_ii', tag='pcmcia_ii')
RemovableMemoryValues.pcmcia_iii = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pcmcia_iii', tag='pcmcia_iii')
RemovableMemoryValues.pictbridge = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='pictbridge', tag='pictbridge')
RemovableMemoryValues.ps2 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ps/2', tag='ps2')
RemovableMemoryValues.radio_frequency = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='radio_frequency', tag='radio_frequency')
RemovableMemoryValues.rs_mmc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='rs_mmc', tag='rs_mmc')
RemovableMemoryValues.s_video = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='s_video', tag='s_video')
RemovableMemoryValues.sas = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sas', tag='sas')
RemovableMemoryValues.sata_1_5_gb = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sata_1_5_gb', tag='sata_1_5_gb')
RemovableMemoryValues.sata_3_0_gb = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sata_3_0_gb', tag='sata_3_0_gb')
RemovableMemoryValues.sata_6_0_gb = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sata_6_0_gb', tag='sata_6_0_gb')
RemovableMemoryValues.sbus = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sbus', tag='sbus')
RemovableMemoryValues.scsi = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='scsi', tag='scsi')
RemovableMemoryValues.sdhc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sdhc', tag='sdhc')
RemovableMemoryValues.sdio = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sdio', tag='sdio')
RemovableMemoryValues.sdxc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sdxc', tag='sdxc')
RemovableMemoryValues.secure_digital = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='secure_digital', tag='secure_digital')
RemovableMemoryValues.secure_mmc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='secure_mmc', tag='secure_mmc')
RemovableMemoryValues.serial_interface = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='serial_interface', tag='serial_interface')
RemovableMemoryValues.sim_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='sim_card', tag='sim_card')
RemovableMemoryValues.smartmedia_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='smartmedia_card', tag='smartmedia_card')
RemovableMemoryValues.solid_state_drive = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='solid_state_drive', tag='solid_state_drive')
RemovableMemoryValues.spd = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='spd', tag='spd')
RemovableMemoryValues.springboard_module = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='springboard_module', tag='springboard_module')
RemovableMemoryValues.ssfdc = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='ssfdc', tag='ssfdc')
RemovableMemoryValues.superdisk = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='superdisk', tag='superdisk')
RemovableMemoryValues.transflash = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='transflash', tag='transflash')
RemovableMemoryValues.unknown = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
RemovableMemoryValues.usb = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='usb', tag='usb')
RemovableMemoryValues.usb1_0 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='usb1.0', tag='usb1_0')
RemovableMemoryValues.usb1_1 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='usb1.1', tag='usb1_1')
RemovableMemoryValues.usb3_0 = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='usb3.0', tag='usb3_0')
RemovableMemoryValues.usb_docking_station = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='usb_docking_station', tag='usb_docking_station')
RemovableMemoryValues.usb_streaming = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='usb_streaming', tag='usb_streaming')
RemovableMemoryValues.vga = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='vga', tag='vga')
RemovableMemoryValues.xd_picture_card = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card', tag='xd_picture_card')
RemovableMemoryValues.xd_picture_card_h = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card_h', tag='xd_picture_card_h')
RemovableMemoryValues.xd_picture_card_m = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card_m', tag='xd_picture_card_m')
RemovableMemoryValues.xd_picture_card_m_plus = RemovableMemoryValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card_m_plus', tag='xd_picture_card_m_plus')
RemovableMemoryValues._InitializeFacetMap(RemovableMemoryValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'RemovableMemoryValues', RemovableMemoryValues)

# Atomic simple type: GraphicsRAMTypeValues
class GraphicsRAMTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'GraphicsRAMTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 617, 1)
    _Documentation = None
GraphicsRAMTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=GraphicsRAMTypeValues, enum_prefix=None)
GraphicsRAMTypeValues.n72_pin_edo_simm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='72_pin_edo_simm', tag='n72_pin_edo_simm')
GraphicsRAMTypeValues.ddr2_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='ddr2_sdram', tag='ddr2_sdram')
GraphicsRAMTypeValues.ddr3_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='ddr3_sdram', tag='ddr3_sdram')
GraphicsRAMTypeValues.ddr4_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='ddr4_sdram', tag='ddr4_sdram')
GraphicsRAMTypeValues.ddr5_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='ddr5_sdram', tag='ddr5_sdram')
GraphicsRAMTypeValues.ddr_dram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='ddr_dram', tag='ddr_dram')
GraphicsRAMTypeValues.ddr_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='ddr_sdram', tag='ddr_sdram')
GraphicsRAMTypeValues.dimm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='dimm', tag='dimm')
GraphicsRAMTypeValues.dram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='dram', tag='dram')
GraphicsRAMTypeValues.edo_dram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='edo_dram', tag='edo_dram')
GraphicsRAMTypeValues.eeprom = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='eeprom', tag='eeprom')
GraphicsRAMTypeValues.eprom = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='eprom', tag='eprom')
GraphicsRAMTypeValues.fpm_dram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='fpm_dram', tag='fpm_dram')
GraphicsRAMTypeValues.fpm_ram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='fpm_ram', tag='fpm_ram')
GraphicsRAMTypeValues.gddr3 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='gddr3', tag='gddr3')
GraphicsRAMTypeValues.gddr4 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='gddr4', tag='gddr4')
GraphicsRAMTypeValues.gddr5 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='gddr5', tag='gddr5')
GraphicsRAMTypeValues.l2_cache = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='l2_cache', tag='l2_cache')
GraphicsRAMTypeValues.micro_dimm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='micro_dimm', tag='micro_dimm')
GraphicsRAMTypeValues.pc2_4200 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc2_4200', tag='pc2_4200')
GraphicsRAMTypeValues.pc2_4300 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc2_4300', tag='pc2_4300')
GraphicsRAMTypeValues.pc2_5300 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc2_5300', tag='pc2_5300')
GraphicsRAMTypeValues.pc2_5400 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc2_5400', tag='pc2_5400')
GraphicsRAMTypeValues.pc2_6000 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc2_6000', tag='pc2_6000')
GraphicsRAMTypeValues.pc_100_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_100_sdram', tag='pc_100_sdram')
GraphicsRAMTypeValues.pc_1066 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_1066', tag='pc_1066')
GraphicsRAMTypeValues.pc_133_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_133_sdram', tag='pc_133_sdram')
GraphicsRAMTypeValues.pc_1600 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_1600', tag='pc_1600')
GraphicsRAMTypeValues.pc_2100_ddr = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_2100_ddr', tag='pc_2100_ddr')
GraphicsRAMTypeValues.pc_2700_ddr = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_2700_ddr', tag='pc_2700_ddr')
GraphicsRAMTypeValues.pc_3000 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_3000', tag='pc_3000')
GraphicsRAMTypeValues.pc_3200_ddr = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_3200_ddr', tag='pc_3200_ddr')
GraphicsRAMTypeValues.pc_3500_ddr = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_3500_ddr', tag='pc_3500_ddr')
GraphicsRAMTypeValues.pc_3700 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_3700', tag='pc_3700')
GraphicsRAMTypeValues.pc_4000_ddr = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_4000_ddr', tag='pc_4000_ddr')
GraphicsRAMTypeValues.pc_4200 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_4200', tag='pc_4200')
GraphicsRAMTypeValues.pc_4300 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_4300', tag='pc_4300')
GraphicsRAMTypeValues.pc_4400 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_4400', tag='pc_4400')
GraphicsRAMTypeValues.pc_66_sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_66_sdram', tag='pc_66_sdram')
GraphicsRAMTypeValues.pc_800 = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_800', tag='pc_800')
GraphicsRAMTypeValues.rambus = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='rambus', tag='rambus')
GraphicsRAMTypeValues.rdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='rdram', tag='rdram')
GraphicsRAMTypeValues.rimm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='rimm', tag='rimm')
GraphicsRAMTypeValues.sdram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sdram', tag='sdram')
GraphicsRAMTypeValues.sgram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sgram', tag='sgram')
GraphicsRAMTypeValues.shared = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='shared', tag='shared')
GraphicsRAMTypeValues.simm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='simm', tag='simm')
GraphicsRAMTypeValues.sipp = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sipp', tag='sipp')
GraphicsRAMTypeValues.sldram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sldram', tag='sldram')
GraphicsRAMTypeValues.sodimm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sodimm', tag='sodimm')
GraphicsRAMTypeValues.sorimm = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sorimm', tag='sorimm')
GraphicsRAMTypeValues.sram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='sram', tag='sram')
GraphicsRAMTypeValues.unknown = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
GraphicsRAMTypeValues.vram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='vram', tag='vram')
GraphicsRAMTypeValues.wram = GraphicsRAMTypeValues._CF_enumeration.addEnumeration(unicode_value='wram', tag='wram')
GraphicsRAMTypeValues._InitializeFacetMap(GraphicsRAMTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'GraphicsRAMTypeValues', GraphicsRAMTypeValues)

# Atomic simple type: HardDriveInterfaceTypeValues
class HardDriveInterfaceTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HardDriveInterfaceTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 676, 1)
    _Documentation = None
HardDriveInterfaceTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=HardDriveInterfaceTypeValues, enum_prefix=None)
HardDriveInterfaceTypeValues.ata = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata', tag='ata')
HardDriveInterfaceTypeValues.ata100 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata100', tag='ata100')
HardDriveInterfaceTypeValues.ata133 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata133', tag='ata133')
HardDriveInterfaceTypeValues.ata_2 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata_2', tag='ata_2')
HardDriveInterfaceTypeValues.ata_3 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata_3', tag='ata_3')
HardDriveInterfaceTypeValues.ata_4 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata_4', tag='ata_4')
HardDriveInterfaceTypeValues.ata_5 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ata_5', tag='ata_5')
HardDriveInterfaceTypeValues.atapi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='atapi', tag='atapi')
HardDriveInterfaceTypeValues.dma = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='dma', tag='dma')
HardDriveInterfaceTypeValues.eide = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='eide', tag='eide')
HardDriveInterfaceTypeValues.eio = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='eio', tag='eio')
HardDriveInterfaceTypeValues.esata = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='esata', tag='esata')
HardDriveInterfaceTypeValues.esdi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='esdi', tag='esdi')
HardDriveInterfaceTypeValues.ethernet = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ethernet', tag='ethernet')
HardDriveInterfaceTypeValues.ethernet_100base_t = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ethernet_100base_t', tag='ethernet_100base_t')
HardDriveInterfaceTypeValues.ethernet_100base_tx = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ethernet_100base_tx', tag='ethernet_100base_tx')
HardDriveInterfaceTypeValues.ethernet_10_100_1000 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ethernet_10_100_1000', tag='ethernet_10_100_1000')
HardDriveInterfaceTypeValues.ethernet_10base_t = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ethernet_10base_t', tag='ethernet_10base_t')
HardDriveInterfaceTypeValues.fast_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fast_scsi', tag='fast_scsi')
HardDriveInterfaceTypeValues.fast_wide_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fast_wide_scsi', tag='fast_wide_scsi')
HardDriveInterfaceTypeValues.fata = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fata', tag='fata')
HardDriveInterfaceTypeValues.fc_al = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fc_al', tag='fc_al')
HardDriveInterfaceTypeValues.fc_al_2 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fc_al_2', tag='fc_al_2')
HardDriveInterfaceTypeValues.fdd = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fdd', tag='fdd')
HardDriveInterfaceTypeValues.fibre_channel = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='fibre_channel', tag='fibre_channel')
HardDriveInterfaceTypeValues.firewire = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='firewire', tag='firewire')
HardDriveInterfaceTypeValues.ide = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ide', tag='ide')
HardDriveInterfaceTypeValues.ieee_1284 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ieee_1284', tag='ieee_1284')
HardDriveInterfaceTypeValues.ieee_1394b = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ieee_1394b', tag='ieee_1394b')
HardDriveInterfaceTypeValues.iscsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='iscsi', tag='iscsi')
HardDriveInterfaceTypeValues.lvds = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='lvds', tag='lvds')
HardDriveInterfaceTypeValues.pc_card = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='pc_card', tag='pc_card')
HardDriveInterfaceTypeValues.pci_express_x16 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x16', tag='pci_express_x16')
HardDriveInterfaceTypeValues.pci_express_x4 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x4', tag='pci_express_x4')
HardDriveInterfaceTypeValues.pci_express_x8 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x8', tag='pci_express_x8')
HardDriveInterfaceTypeValues.raid = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='raid', tag='raid')
HardDriveInterfaceTypeValues.scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='scsi', tag='scsi')
HardDriveInterfaceTypeValues.serial_ata = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='serial_ata', tag='serial_ata')
HardDriveInterfaceTypeValues.serial_ata150 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='serial_ata150', tag='serial_ata150')
HardDriveInterfaceTypeValues.serial_ata300 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='serial_ata300', tag='serial_ata300')
HardDriveInterfaceTypeValues.serial_ata600 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='serial_ata600', tag='serial_ata600')
HardDriveInterfaceTypeValues.serial_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='serial_scsi', tag='serial_scsi')
HardDriveInterfaceTypeValues.solid_state = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='solid_state', tag='solid_state')
HardDriveInterfaceTypeValues.ssa = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ssa', tag='ssa')
HardDriveInterfaceTypeValues.st412 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='st412', tag='st412')
HardDriveInterfaceTypeValues.ultra2_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra2_scsi', tag='ultra2_scsi')
HardDriveInterfaceTypeValues.ultra2_wide_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra2_wide_scsi', tag='ultra2_wide_scsi')
HardDriveInterfaceTypeValues.ultra3_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra3_scsi', tag='ultra3_scsi')
HardDriveInterfaceTypeValues.ultra_160_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra_160_scsi', tag='ultra_160_scsi')
HardDriveInterfaceTypeValues.ultra_320_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra_320_scsi', tag='ultra_320_scsi')
HardDriveInterfaceTypeValues.ultra_ata = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra_ata', tag='ultra_ata')
HardDriveInterfaceTypeValues.ultra_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra_scsi', tag='ultra_scsi')
HardDriveInterfaceTypeValues.ultra_wide_scsi = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='ultra_wide_scsi', tag='ultra_wide_scsi')
HardDriveInterfaceTypeValues.unknown = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
HardDriveInterfaceTypeValues.usb = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb', tag='usb')
HardDriveInterfaceTypeValues.usb_1_1 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb_1.1', tag='usb_1_1')
HardDriveInterfaceTypeValues.usb_2_0 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb_2.0', tag='usb_2_0')
HardDriveInterfaceTypeValues.usb_2_0_3_0 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb_2.0_3.0', tag='usb_2_0_3_0')
HardDriveInterfaceTypeValues.usb_3_0 = HardDriveInterfaceTypeValues._CF_enumeration.addEnumeration(unicode_value='usb_3.0', tag='usb_3_0')
HardDriveInterfaceTypeValues._InitializeFacetMap(HardDriveInterfaceTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'HardDriveInterfaceTypeValues', HardDriveInterfaceTypeValues)

# Atomic simple type: SupportedImageTypeValues
class SupportedImageTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SupportedImageTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 739, 1)
    _Documentation = None
SupportedImageTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=SupportedImageTypeValues, enum_prefix=None)
SupportedImageTypeValues.n3f_tiff = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='3f_tiff', tag='n3f_tiff')
SupportedImageTypeValues.bmp = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='bmp', tag='bmp')
SupportedImageTypeValues.canon_raw = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='canon_raw', tag='canon_raw')
SupportedImageTypeValues.eps = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='eps', tag='eps')
SupportedImageTypeValues.exif = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='exif', tag='exif')
SupportedImageTypeValues.flashpix_fpx = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='flashpix_fpx', tag='flashpix_fpx')
SupportedImageTypeValues.gif = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='gif', tag='gif')
SupportedImageTypeValues.jpeg = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='jpeg', tag='jpeg')
SupportedImageTypeValues.pcx = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='pcx', tag='pcx')
SupportedImageTypeValues.pgpf = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='pgpf', tag='pgpf')
SupportedImageTypeValues.pict = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='pict', tag='pict')
SupportedImageTypeValues.png = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='png', tag='png')
SupportedImageTypeValues.psd = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='psd', tag='psd')
SupportedImageTypeValues.raw = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='raw', tag='raw')
SupportedImageTypeValues.tga = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='tga', tag='tga')
SupportedImageTypeValues.tiff = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='tiff', tag='tiff')
SupportedImageTypeValues.unknown = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
SupportedImageTypeValues.wif = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='wif', tag='wif')
SupportedImageTypeValues.wmf = SupportedImageTypeValues._CF_enumeration.addEnumeration(unicode_value='wmf', tag='wmf')
SupportedImageTypeValues._InitializeFacetMap(SupportedImageTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'SupportedImageTypeValues', SupportedImageTypeValues)

# Atomic simple type: HardwareInterfaceValues
class HardwareInterfaceValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HardwareInterfaceValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 762, 1)
    _Documentation = None
HardwareInterfaceValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=HardwareInterfaceValues, enum_prefix=None)
HardwareInterfaceValues.n1_4_inch_audio = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='1_4_inch_audio', tag='n1_4_inch_audio')
HardwareInterfaceValues.n2_5_mm_audio = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='2_5_mm_audio', tag='n2_5_mm_audio')
HardwareInterfaceValues.n3_0_v_ttl = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='3.0_v_ttl', tag='n3_0_v_ttl')
HardwareInterfaceValues.n3_5_floppy = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='3_5_floppy', tag='n3_5_floppy')
HardwareInterfaceValues.n3_5_mm_audio = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='3_5_mm_audio', tag='n3_5_mm_audio')
HardwareInterfaceValues.ata = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ata', tag='ata')
HardwareInterfaceValues.ata_flash_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ata_flash_card', tag='ata_flash_card')
HardwareInterfaceValues.audio_video_port = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='audio_video_port', tag='audio_video_port')
HardwareInterfaceValues.bluetooth = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='bluetooth', tag='bluetooth')
HardwareInterfaceValues.built_in_flash_memory = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='built_in_flash_memory', tag='built_in_flash_memory')
HardwareInterfaceValues.cd_r = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='cd-r', tag='cd_r')
HardwareInterfaceValues.cd_rw = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='cd_rw', tag='cd_rw')
HardwareInterfaceValues.cdr_drive = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='cdr_drive', tag='cdr_drive')
HardwareInterfaceValues.compact_disc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='compact_disc', tag='compact_disc')
HardwareInterfaceValues.compact_flash_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='compact_flash_card', tag='compact_flash_card')
HardwareInterfaceValues.compact_flash_type_i_or_ii = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='compact_flash_type_i_or_ii', tag='compact_flash_type_i_or_ii')
HardwareInterfaceValues.compactflash_type_i = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='compactflash_type_i', tag='compactflash_type_i')
HardwareInterfaceValues.compactflash_type_ii = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='compactflash_type_ii', tag='compactflash_type_ii')
HardwareInterfaceValues.component_video = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='component_video', tag='component_video')
HardwareInterfaceValues.composite_video = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='composite_video', tag='composite_video')
HardwareInterfaceValues.d_sub = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='d_sub', tag='d_sub')
HardwareInterfaceValues.dmi = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dmi', tag='dmi')
HardwareInterfaceValues.dssi = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dssi', tag='dssi')
HardwareInterfaceValues.dvd_r = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dvd_r', tag='dvd_r')
HardwareInterfaceValues.dvd_rw = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dvd_rw', tag='dvd_rw')
HardwareInterfaceValues.dvi_x_1 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dvi_x_1', tag='dvi_x_1')
HardwareInterfaceValues.dvi_x_2 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dvi_x_2', tag='dvi_x_2')
HardwareInterfaceValues.dvi_x_4 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='dvi_x_4', tag='dvi_x_4')
HardwareInterfaceValues.eide = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='eide', tag='eide')
HardwareInterfaceValues.eisa = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='eisa', tag='eisa')
HardwareInterfaceValues.ethernet = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ethernet', tag='ethernet')
HardwareInterfaceValues.express_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='express_card', tag='express_card')
HardwareInterfaceValues.fibre_channel = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='fibre_channel', tag='fibre_channel')
HardwareInterfaceValues.firewire_1600 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='firewire_1600', tag='firewire_1600')
HardwareInterfaceValues.firewire_3200 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='firewire_3200', tag='firewire_3200')
HardwareInterfaceValues.firewire_400 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='firewire_400', tag='firewire_400')
HardwareInterfaceValues.firewire_800 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='firewire_800', tag='firewire_800')
HardwareInterfaceValues.firewire_esata = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='firewire_esata', tag='firewire_esata')
HardwareInterfaceValues.game_port = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='game_port', tag='game_port')
HardwareInterfaceValues.gbic = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='gbic', tag='gbic')
HardwareInterfaceValues.hdmi = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='hdmi', tag='hdmi')
HardwareInterfaceValues.headphone = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='headphone', tag='headphone')
HardwareInterfaceValues.hp_hsc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='hp_hsc', tag='hp_hsc')
HardwareInterfaceValues.hp_pb = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='hp_pb', tag='hp_pb')
HardwareInterfaceValues.hs_mmc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='hs_mmc', tag='hs_mmc')
HardwareInterfaceValues.ibm_microdrive = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ibm_microdrive', tag='ibm_microdrive')
HardwareInterfaceValues.ide = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ide', tag='ide')
HardwareInterfaceValues.ieee_1284 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ieee_1284', tag='ieee_1284')
HardwareInterfaceValues.infrared = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='infrared', tag='infrared')
HardwareInterfaceValues.internal_w_removable_media = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='internal_w_removable_media', tag='internal_w_removable_media')
HardwareInterfaceValues.iomega_clik_disk = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='iomega_clik_disk', tag='iomega_clik_disk')
HardwareInterfaceValues.isa = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='isa', tag='isa')
HardwareInterfaceValues.isp = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='isp', tag='isp')
HardwareInterfaceValues.lanc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='lanc', tag='lanc')
HardwareInterfaceValues.mca = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mca', tag='mca')
HardwareInterfaceValues.media_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='media_card', tag='media_card')
HardwareInterfaceValues.memory_stick = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick', tag='memory_stick')
HardwareInterfaceValues.memory_stick_duo = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_duo', tag='memory_stick_duo')
HardwareInterfaceValues.memory_stick_micro = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_micro', tag='memory_stick_micro')
HardwareInterfaceValues.memory_stick_pro = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_pro', tag='memory_stick_pro')
HardwareInterfaceValues.memory_stick_pro_duo = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_pro_duo', tag='memory_stick_pro_duo')
HardwareInterfaceValues.memory_stick_pro_hg_duo = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_pro_hg_duo', tag='memory_stick_pro_hg_duo')
HardwareInterfaceValues.memory_stick_select = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_select', tag='memory_stick_select')
HardwareInterfaceValues.memory_stick_xc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_xc', tag='memory_stick_xc')
HardwareInterfaceValues.memory_stick_xc_hg_micro = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_xc_hg_micro', tag='memory_stick_xc_hg_micro')
HardwareInterfaceValues.memory_stick_xc_micro = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='memory_stick_xc_micro', tag='memory_stick_xc_micro')
HardwareInterfaceValues.micard = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='micard', tag='micard')
HardwareInterfaceValues.micro_sdhc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='micro_sdhc', tag='micro_sdhc')
HardwareInterfaceValues.micro_sdxc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='micro_sdxc', tag='micro_sdxc')
HardwareInterfaceValues.microsd = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='microsd', tag='microsd')
HardwareInterfaceValues.mini_dvd = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mini_dvd', tag='mini_dvd')
HardwareInterfaceValues.mini_hdmi = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mini_hdmi', tag='mini_hdmi')
HardwareInterfaceValues.mini_pci = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mini_pci', tag='mini_pci')
HardwareInterfaceValues.mini_sdhc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mini_sdhc', tag='mini_sdhc')
HardwareInterfaceValues.mini_sdxc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mini_sdxc', tag='mini_sdxc')
HardwareInterfaceValues.minisd = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='minisd', tag='minisd')
HardwareInterfaceValues.mmc_micro = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='mmc_micro', tag='mmc_micro')
HardwareInterfaceValues.multimedia_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='multimedia_card', tag='multimedia_card')
HardwareInterfaceValues.multimedia_card_mobile = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='multimedia_card_mobile', tag='multimedia_card_mobile')
HardwareInterfaceValues.multimedia_card_plus = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='multimedia_card_plus', tag='multimedia_card_plus')
HardwareInterfaceValues.multipronged_audio = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='multipronged_audio', tag='multipronged_audio')
HardwareInterfaceValues.nubus = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='nubus', tag='nubus')
HardwareInterfaceValues.parallel_interface = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='parallel_interface', tag='parallel_interface')
HardwareInterfaceValues.pc_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pc_card', tag='pc_card')
HardwareInterfaceValues.pci = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci', tag='pci')
HardwareInterfaceValues.pci_64 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_64', tag='pci_64')
HardwareInterfaceValues.pci_64_hot_plug = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_64_hot_plug', tag='pci_64_hot_plug')
HardwareInterfaceValues.pci_64_hot_plug_33_mhz = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_64_hot_plug_33_mhz', tag='pci_64_hot_plug_33_mhz')
HardwareInterfaceValues.pci_64_hot_plug_66_mhz = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_64_hot_plug_66_mhz', tag='pci_64_hot_plug_66_mhz')
HardwareInterfaceValues.pci_express_x4 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x4', tag='pci_express_x4')
HardwareInterfaceValues.pci_express_x8 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_express_x8', tag='pci_express_x8')
HardwareInterfaceValues.pci_hot_plug = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_hot_plug', tag='pci_hot_plug')
HardwareInterfaceValues.pci_raid = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_raid', tag='pci_raid')
HardwareInterfaceValues.pci_x = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x', tag='pci_x')
HardwareInterfaceValues.pci_x_1 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_1', tag='pci_x_1')
HardwareInterfaceValues.pci_x_100_mhz = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_100_mhz', tag='pci_x_100_mhz')
HardwareInterfaceValues.pci_x_16 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_16', tag='pci_x_16')
HardwareInterfaceValues.pci_x_16_gb = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_16_gb', tag='pci_x_16_gb')
HardwareInterfaceValues.pci_x_4 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_4', tag='pci_x_4')
HardwareInterfaceValues.pci_x_66_mhz = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_66_mhz', tag='pci_x_66_mhz')
HardwareInterfaceValues.pci_x_8 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_8', tag='pci_x_8')
HardwareInterfaceValues.pci_x_hot_plug = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_hot_plug', tag='pci_x_hot_plug')
HardwareInterfaceValues.pci_x_hot_plug_133_mhz = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_hot_plug_133_mhz', tag='pci_x_hot_plug_133_mhz')
HardwareInterfaceValues.pcmcia = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pcmcia', tag='pcmcia')
HardwareInterfaceValues.pcmcia_ii = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pcmcia_ii', tag='pcmcia_ii')
HardwareInterfaceValues.pcmcia_iii = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pcmcia_iii', tag='pcmcia_iii')
HardwareInterfaceValues.pictbridge = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pictbridge', tag='pictbridge')
HardwareInterfaceValues.ps2 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ps/2', tag='ps2')
HardwareInterfaceValues.radio_frequency = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='radio_frequency', tag='radio_frequency')
HardwareInterfaceValues.rs_mmc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='rs_mmc', tag='rs_mmc')
HardwareInterfaceValues.s_video = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='s_video', tag='s_video')
HardwareInterfaceValues.sas = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sas', tag='sas')
HardwareInterfaceValues.sata_1_5_gb = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sata_1_5_gb', tag='sata_1_5_gb')
HardwareInterfaceValues.sata_3_0_gb = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sata_3_0_gb', tag='sata_3_0_gb')
HardwareInterfaceValues.sata_6_0_gb = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sata_6_0_gb', tag='sata_6_0_gb')
HardwareInterfaceValues.sbus = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sbus', tag='sbus')
HardwareInterfaceValues.scsi = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='scsi', tag='scsi')
HardwareInterfaceValues.sdhc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sdhc', tag='sdhc')
HardwareInterfaceValues.sdio = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sdio', tag='sdio')
HardwareInterfaceValues.sdxc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sdxc', tag='sdxc')
HardwareInterfaceValues.secure_digital = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='secure_digital', tag='secure_digital')
HardwareInterfaceValues.secure_mmc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='secure_mmc', tag='secure_mmc')
HardwareInterfaceValues.serial_interface = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='serial_interface', tag='serial_interface')
HardwareInterfaceValues.sim_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='sim_card', tag='sim_card')
HardwareInterfaceValues.smartmedia_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='smartmedia_card', tag='smartmedia_card')
HardwareInterfaceValues.solid_state_drive = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='solid_state_drive', tag='solid_state_drive')
HardwareInterfaceValues.spd = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='spd', tag='spd')
HardwareInterfaceValues.springboard_module = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='springboard_module', tag='springboard_module')
HardwareInterfaceValues.ssfdc = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ssfdc', tag='ssfdc')
HardwareInterfaceValues.superdisk = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='superdisk', tag='superdisk')
HardwareInterfaceValues.transflash = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='transflash', tag='transflash')
HardwareInterfaceValues.unknown = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
HardwareInterfaceValues.usb = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb', tag='usb')
HardwareInterfaceValues.usb1_0 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb1.0', tag='usb1_0')
HardwareInterfaceValues.usb1_1 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb1.1', tag='usb1_1')
HardwareInterfaceValues.usb2_0 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb2.0', tag='usb2_0')
HardwareInterfaceValues.usb3_0 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb3.0', tag='usb3_0')
HardwareInterfaceValues.usb_docking_station = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb_docking_station', tag='usb_docking_station')
HardwareInterfaceValues.usb_streaming = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='usb_streaming', tag='usb_streaming')
HardwareInterfaceValues.vga = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='vga', tag='vga')
HardwareInterfaceValues.xd_picture_card = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card', tag='xd_picture_card')
HardwareInterfaceValues.xd_picture_card_h = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card_h', tag='xd_picture_card_h')
HardwareInterfaceValues.xd_picture_card_m = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card_m', tag='xd_picture_card_m')
HardwareInterfaceValues.xd_picture_card_m_plus = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='xd_picture_card_m_plus', tag='xd_picture_card_m_plus')
HardwareInterfaceValues.ieee_1394 = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='ieee_1394', tag='ieee_1394')
HardwareInterfaceValues.pci_x_133_mhz = HardwareInterfaceValues._CF_enumeration.addEnumeration(unicode_value='pci_x_133_mhz', tag='pci_x_133_mhz')
HardwareInterfaceValues._InitializeFacetMap(HardwareInterfaceValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'HardwareInterfaceValues', HardwareInterfaceValues)

# Atomic simple type: VideotapeRecordingSpeedType
class VideotapeRecordingSpeedType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VideotapeRecordingSpeedType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 912, 1)
    _Documentation = None
VideotapeRecordingSpeedType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VideotapeRecordingSpeedType, enum_prefix=None)
VideotapeRecordingSpeedType.unknown = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
VideotapeRecordingSpeedType.sp = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='sp', tag='sp')
VideotapeRecordingSpeedType.ep = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='ep', tag='ep')
VideotapeRecordingSpeedType.slp = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='slp', tag='slp')
VideotapeRecordingSpeedType.lp = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='lp', tag='lp')
VideotapeRecordingSpeedType.Unknown = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='Unknown', tag='Unknown')
VideotapeRecordingSpeedType.SP = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='SP', tag='SP')
VideotapeRecordingSpeedType.EP = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='EP', tag='EP')
VideotapeRecordingSpeedType.SLP = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='SLP', tag='SLP')
VideotapeRecordingSpeedType.LP = VideotapeRecordingSpeedType._CF_enumeration.addEnumeration(unicode_value='LP', tag='LP')
VideotapeRecordingSpeedType._InitializeFacetMap(VideotapeRecordingSpeedType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VideotapeRecordingSpeedType', VideotapeRecordingSpeedType)

# Atomic simple type: ThreeDTechnologyValues
class ThreeDTechnologyValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ThreeDTechnologyValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 926, 1)
    _Documentation = None
ThreeDTechnologyValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ThreeDTechnologyValues, enum_prefix=None)
ThreeDTechnologyValues.active = ThreeDTechnologyValues._CF_enumeration.addEnumeration(unicode_value='active', tag='active')
ThreeDTechnologyValues.anaglyphic = ThreeDTechnologyValues._CF_enumeration.addEnumeration(unicode_value='anaglyphic', tag='anaglyphic')
ThreeDTechnologyValues.auto_stereoscopic = ThreeDTechnologyValues._CF_enumeration.addEnumeration(unicode_value='auto_stereoscopic', tag='auto_stereoscopic')
ThreeDTechnologyValues.passive = ThreeDTechnologyValues._CF_enumeration.addEnumeration(unicode_value='passive', tag='passive')
ThreeDTechnologyValues._InitializeFacetMap(ThreeDTechnologyValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ThreeDTechnologyValues', ThreeDTechnologyValues)

# Atomic simple type: WirelessTypeValues
class WirelessTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WirelessTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 934, 1)
    _Documentation = None
WirelessTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=WirelessTypeValues, enum_prefix=None)
WirelessTypeValues.n2_4_ghz_radio_frequency = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='2.4_ghz_radio_frequency', tag='n2_4_ghz_radio_frequency')
WirelessTypeValues.n5_8_ghz_radio_frequency = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='5.8_ghz_radio_frequency', tag='n5_8_ghz_radio_frequency')
WirelessTypeValues.n802_11_A = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_A', tag='n802_11_A')
WirelessTypeValues.n802_11_AB = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_AB', tag='n802_11_AB')
WirelessTypeValues.n802_11_ABG = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_ABG', tag='n802_11_ABG')
WirelessTypeValues.n802_11_AG = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_AG', tag='n802_11_AG')
WirelessTypeValues.n802_11_B = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_B', tag='n802_11_B')
WirelessTypeValues.n802_11_BGN = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_BGN', tag='n802_11_BGN')
WirelessTypeValues.n802_11_G = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_G', tag='n802_11_G')
WirelessTypeValues.n802_11_G_108Mbps = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_G_108Mbps', tag='n802_11_G_108Mbps')
WirelessTypeValues.n802_11_N = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='802_11_N', tag='n802_11_N')
WirelessTypeValues.n900_mhz_radio_frequency = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='900_mhz_radio_frequency', tag='n900_mhz_radio_frequency')
WirelessTypeValues.bluetooth = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='bluetooth', tag='bluetooth')
WirelessTypeValues.dect = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='dect', tag='dect')
WirelessTypeValues.dect_6_0 = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='dect_6.0', tag='dect_6_0')
WirelessTypeValues.infrared = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='infrared', tag='infrared')
WirelessTypeValues.irda = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='irda', tag='irda')
WirelessTypeValues.radio_frequency = WirelessTypeValues._CF_enumeration.addEnumeration(unicode_value='radio_frequency', tag='radio_frequency')
WirelessTypeValues._InitializeFacetMap(WirelessTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'WirelessTypeValues', WirelessTypeValues)

# Atomic simple type: ProcessorTypeValues
class ProcessorTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ProcessorTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 956, 1)
    _Documentation = None
ProcessorTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ProcessorTypeValues, enum_prefix=None)
ProcessorTypeValues.n5x86 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='5x86', tag='n5x86')
ProcessorTypeValues.n68000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68000', tag='n68000')
ProcessorTypeValues.n68030 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68030', tag='n68030')
ProcessorTypeValues.n68040 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68040', tag='n68040')
ProcessorTypeValues.n68328 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68328', tag='n68328')
ProcessorTypeValues.n68882 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68882', tag='n68882')
ProcessorTypeValues.n68ez328 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68ez328', tag='n68ez328')
ProcessorTypeValues.n68lc040 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='68lc040', tag='n68lc040')
ProcessorTypeValues.n6x86 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='6x86', tag='n6x86')
ProcessorTypeValues.n6x86mx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='6x86mx', tag='n6x86mx')
ProcessorTypeValues.n8031 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='8031', tag='n8031')
ProcessorTypeValues.n8032 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='8032', tag='n8032')
ProcessorTypeValues.n80386 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80386', tag='n80386')
ProcessorTypeValues.n80486 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80486', tag='n80486')
ProcessorTypeValues.n80486dx2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80486dx2', tag='n80486dx2')
ProcessorTypeValues.n80486slc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80486slc', tag='n80486slc')
ProcessorTypeValues.n80486sx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80486sx', tag='n80486sx')
ProcessorTypeValues.n80c186 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80c186', tag='n80c186')
ProcessorTypeValues.n80c31 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80c31', tag='n80c31')
ProcessorTypeValues.n80c32 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80c32', tag='n80c32')
ProcessorTypeValues.n80c88 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='80c88', tag='n80c88')
ProcessorTypeValues.alpha_21064a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21064a', tag='alpha_21064a')
ProcessorTypeValues.alpha_21164 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21164', tag='alpha_21164')
ProcessorTypeValues.alpha_21164a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21164a', tag='alpha_21164a')
ProcessorTypeValues.alpha_21264 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21264', tag='alpha_21264')
ProcessorTypeValues.alpha_21264a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21264a', tag='alpha_21264a')
ProcessorTypeValues.alpha_21264b = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21264b', tag='alpha_21264b')
ProcessorTypeValues.alpha_21264c = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21264c', tag='alpha_21264c')
ProcessorTypeValues.alpha_21264d = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21264d', tag='alpha_21264d')
ProcessorTypeValues.alpha_21364 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_21364', tag='alpha_21364')
ProcessorTypeValues.alpha_ev7 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='alpha_ev7', tag='alpha_ev7')
ProcessorTypeValues.amd_a_series = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='amd_a_series', tag='amd_a_series')
ProcessorTypeValues.amd_c_series = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='amd_c_series', tag='amd_c_series')
ProcessorTypeValues.amd_e_series = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='amd_e_series', tag='amd_e_series')
ProcessorTypeValues.amd_v_series = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='amd_v_series', tag='amd_v_series')
ProcessorTypeValues.arm610 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='arm610', tag='arm610')
ProcessorTypeValues.arm710 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='arm710', tag='arm710')
ProcessorTypeValues.arm_7100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='arm_7100', tag='arm_7100')
ProcessorTypeValues.arm710a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='arm710a', tag='arm710a')
ProcessorTypeValues.arm710t = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='arm710t', tag='arm710t')
ProcessorTypeValues.arm7500fe = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='arm7500fe', tag='arm7500fe')
ProcessorTypeValues.athlon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon', tag='athlon')
ProcessorTypeValues.Athlon_2650e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_2650e', tag='Athlon_2650e')
ProcessorTypeValues.Athlon_2850e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_2850e', tag='Athlon_2850e')
ProcessorTypeValues.athlon_4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_4', tag='athlon_4')
ProcessorTypeValues.athlon_64 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_64', tag='athlon_64')
ProcessorTypeValues.Athlon_64_2650 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_2650', tag='Athlon_64_2650')
ProcessorTypeValues.Athlon_64_3500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_3500', tag='Athlon_64_3500')
ProcessorTypeValues.Athlon_64_4200_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_4200_plus', tag='Athlon_64_4200_plus')
ProcessorTypeValues.Athlon_64_4800_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_4800_plus', tag='Athlon_64_4800_plus')
ProcessorTypeValues.athlon_64_for_dtr = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_64_for_dtr', tag='athlon_64_for_dtr')
ProcessorTypeValues.athlon_64_fx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_64_fx', tag='athlon_64_fx')
ProcessorTypeValues.Athlon_64_Single_Core_TF_20 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_Single_Core_TF_20', tag='Athlon_64_Single_Core_TF_20')
ProcessorTypeValues.Athlon_64_TF_36 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_TF_36', tag='Athlon_64_TF_36')
ProcessorTypeValues.athlon_64_x2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_64_x2', tag='athlon_64_x2')
ProcessorTypeValues.athlon_64_X2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_64_X2', tag='athlon_64_X2')
ProcessorTypeValues.Athlon_64_X2_3600_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_3600_plus', tag='Athlon_64_X2_3600_plus')
ProcessorTypeValues.Athlon_64_X2_4000_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_4000_plus', tag='Athlon_64_X2_4000_plus')
ProcessorTypeValues.Athlon_64_X2_4200_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_4200_plus', tag='Athlon_64_X2_4200_plus')
ProcessorTypeValues.Athlon_64_X2_4450B = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_4450B', tag='Athlon_64_X2_4450B')
ProcessorTypeValues.Athlon_64_X2_4800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_4800', tag='Athlon_64_X2_4800')
ProcessorTypeValues.Athlon_64_X2_5000_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_5000_plus', tag='Athlon_64_X2_5000_plus')
ProcessorTypeValues.Athlon_64_X2_5050 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_5050', tag='Athlon_64_X2_5050')
ProcessorTypeValues.Athlon_64_X2_5200_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_5200_plus', tag='Athlon_64_X2_5200_plus')
ProcessorTypeValues.Athlon_64_X2_5400_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_5400_plus', tag='Athlon_64_X2_5400_plus')
ProcessorTypeValues.Athlon_64_X2_5600_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_5600_plus', tag='Athlon_64_X2_5600_plus')
ProcessorTypeValues.Athlon_64_X2_6000_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_6000_plus', tag='Athlon_64_X2_6000_plus')
ProcessorTypeValues.Athlon_64_X2_6400_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_6400_plus', tag='Athlon_64_X2_6400_plus')
ProcessorTypeValues.Athlon_64_X2_Dual_Core_3800_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_Dual_Core_3800_plus', tag='Athlon_64_X2_Dual_Core_3800_plus')
ProcessorTypeValues.Athlon_64_X2_Dual_Core_4400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_Dual_Core_4400', tag='Athlon_64_X2_Dual_Core_4400')
ProcessorTypeValues.Athlon_64_X2_Dual_Core_4450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_Dual_Core_4450', tag='Athlon_64_X2_Dual_Core_4450')
ProcessorTypeValues.Athlon_64_X2_Dual_Core_L310 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_Dual_Core_L310', tag='Athlon_64_X2_Dual_Core_L310')
ProcessorTypeValues.Athlon_64_X2_Dual_Core_TK_42 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_Dual_Core_TK_42', tag='Athlon_64_X2_Dual_Core_TK_42')
ProcessorTypeValues.Athlon_64_X2_QL_64_Dual_Core = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_64_X2_QL_64_Dual_Core', tag='Athlon_64_X2_QL_64_Dual_Core')
ProcessorTypeValues.Athlon_Dual_Core_QL62A = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_Dual_Core_QL62A', tag='Athlon_Dual_Core_QL62A')
ProcessorTypeValues.Athlon_II = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II', tag='Athlon_II')
ProcessorTypeValues.Athlon_II_170u = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_170u', tag='Athlon_II_170u')
ProcessorTypeValues.Athlon_II_Dual_Core_240e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_240e', tag='Athlon_II_Dual_Core_240e')
ProcessorTypeValues.Athlon_II_Dual_Core_245 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_245', tag='Athlon_II_Dual_Core_245')
ProcessorTypeValues.Athlon_II_Dual_Core_260 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_260', tag='Athlon_II_Dual_Core_260')
ProcessorTypeValues.Athlon_II_Dual_Core_260u = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_260u', tag='Athlon_II_Dual_Core_260u')
ProcessorTypeValues.Athlon_II_Dual_Core_M320 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_M320', tag='Athlon_II_Dual_Core_M320')
ProcessorTypeValues.Athlon_II_Dual_Core_P320 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_P320', tag='Athlon_II_Dual_Core_P320')
ProcessorTypeValues.Athlon_II_Dual_Core_P360 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Dual_Core_P360', tag='Athlon_II_Dual_Core_P360')
ProcessorTypeValues.Athlon_II_Neo_Single_Core_K125 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Neo_Single_Core_K125', tag='Athlon_II_Neo_Single_Core_K125')
ProcessorTypeValues.Athlon_II_Neo_X2_Dual_Core_K325 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Neo_X2_Dual_Core_K325', tag='Athlon_II_Neo_X2_Dual_Core_K325')
ProcessorTypeValues.Athlon_II_Neo_X2_Dual_Core_K625 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Neo_X2_Dual_Core_K625', tag='Athlon_II_Neo_X2_Dual_Core_K625')
ProcessorTypeValues.Athlon_II_Single_Core_160u = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_Single_Core_160u', tag='Athlon_II_Single_Core_160u')
ProcessorTypeValues.Athlon_II_X2_B24 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_B24', tag='Athlon_II_X2_B24')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_170u = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_170u', tag='Athlon_II_X2_Dual_Core_170u')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_215 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_215', tag='Athlon_II_X2_Dual_Core_215')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_235e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_235e', tag='Athlon_II_X2_Dual_Core_235e')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_240 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_240', tag='Athlon_II_X2_Dual_Core_240')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_240e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_240e', tag='Athlon_II_X2_Dual_Core_240e')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_245 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_245', tag='Athlon_II_X2_Dual_Core_245')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_250', tag='Athlon_II_X2_Dual_Core_250')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_250U = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_250U', tag='Athlon_II_X2_Dual_Core_250U')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_255 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_255', tag='Athlon_II_X2_Dual_Core_255')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_3250e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_3250e', tag='Athlon_II_X2_Dual_Core_3250e')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_M300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_M300', tag='Athlon_II_X2_Dual_Core_M300')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_M320 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_M320', tag='Athlon_II_X2_Dual_Core_M320')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_M340 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_M340', tag='Athlon_II_X2_Dual_Core_M340')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_M520 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_M520', tag='Athlon_II_X2_Dual_Core_M520')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_P320 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_P320', tag='Athlon_II_X2_Dual_Core_P320')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_P340 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_P340', tag='Athlon_II_X2_Dual_Core_P340')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_P360 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_P360', tag='Athlon_II_X2_Dual_Core_P360')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_QL_60 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_QL_60', tag='Athlon_II_X2_Dual_Core_QL_60')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_QL_62 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_QL_62', tag='Athlon_II_X2_Dual_Core_QL_62')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_TK_53 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_TK_53', tag='Athlon_II_X2_Dual_Core_TK_53')
ProcessorTypeValues.Athlon_II_X2_Dual_Core_TK_57 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X2_Dual_Core_TK_57', tag='Athlon_II_X2_Dual_Core_TK_57')
ProcessorTypeValues.athlon_ii_x3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_ii_x3', tag='athlon_ii_x3')
ProcessorTypeValues.Athlon_II_X3_Triple_Core_400E = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X3_Triple_Core_400E', tag='Athlon_II_X3_Triple_Core_400E')
ProcessorTypeValues.Athlon_II_X3_Triple_Core_425 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X3_Triple_Core_425', tag='Athlon_II_X3_Triple_Core_425')
ProcessorTypeValues.Athlon_II_X3_Triple_Core_435 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X3_Triple_Core_435', tag='Athlon_II_X3_Triple_Core_435')
ProcessorTypeValues.athlon_ii_x4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_ii_x4', tag='athlon_ii_x4')
ProcessorTypeValues.Athlon_II_X4_Dual_Core_240e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Dual_Core_240e', tag='Athlon_II_X4_Dual_Core_240e')
ProcessorTypeValues.Athlon_II_X4_Quad_Core = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core', tag='Athlon_II_X4_Quad_Core')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_600E = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_600E', tag='Athlon_II_X4_Quad_Core_600E')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_605e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_605e', tag='Athlon_II_X4_Quad_Core_605e')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_615e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_615e', tag='Athlon_II_X4_Quad_Core_615e')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_620 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_620', tag='Athlon_II_X4_Quad_Core_620')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_630 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_630', tag='Athlon_II_X4_Quad_Core_630')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_635 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_635', tag='Athlon_II_X4_Quad_Core_635')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_640 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_640', tag='Athlon_II_X4_Quad_Core_640')
ProcessorTypeValues.Athlon_II_X4_Quad_Core_645 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_II_X4_Quad_Core_645', tag='Athlon_II_X4_Quad_Core_645')
ProcessorTypeValues.Athlon_LE_1640 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_LE_1640', tag='Athlon_LE_1640')
ProcessorTypeValues.athlon_mp = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_mp', tag='athlon_mp')
ProcessorTypeValues.Athlon_Neo_Single_Core_MV_40 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_Neo_Single_Core_MV_40', tag='Athlon_Neo_Single_Core_MV_40')
ProcessorTypeValues.Athlon_Neo_X2_Dual_Core_L325 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_Neo_X2_Dual_Core_L325', tag='Athlon_Neo_X2_Dual_Core_L325')
ProcessorTypeValues.Athlon_Neo_X2_Dual_Core_L335 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_Neo_X2_Dual_Core_L335', tag='Athlon_Neo_X2_Dual_Core_L335')
ProcessorTypeValues.Athlon_X2_Dual_Core_4200_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_4200_plus', tag='Athlon_X2_Dual_Core_4200_plus')
ProcessorTypeValues.Athlon_X2_Dual_Core_5000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_5000', tag='Athlon_X2_Dual_Core_5000')
ProcessorTypeValues.Athlon_X2_Dual_Core_5000_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_5000_plus', tag='Athlon_X2_Dual_Core_5000_plus')
ProcessorTypeValues.Athlon_X2_Dual_Core_5400_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_5400_plus', tag='Athlon_X2_Dual_Core_5400_plus')
ProcessorTypeValues.Athlon_X2_Dual_Core_7550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_7550', tag='Athlon_X2_Dual_Core_7550')
ProcessorTypeValues.Athlon_X2_Dual_Core_7750 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_7750', tag='Athlon_X2_Dual_Core_7750')
ProcessorTypeValues.Athlon_X2_Dual_Core_QL_66 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Athlon_X2_Dual_Core_QL_66', tag='Athlon_X2_Dual_Core_QL_66')
ProcessorTypeValues.athlon_xp = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_xp', tag='athlon_xp')
ProcessorTypeValues.athlon_xp_m = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='athlon_xp_m', tag='athlon_xp_m')
ProcessorTypeValues.Atom_D410 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_D410', tag='Atom_D410')
ProcessorTypeValues.Atom_D425 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_D425', tag='Atom_D425')
ProcessorTypeValues.Atom_D510 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_D510', tag='Atom_D510')
ProcessorTypeValues.Atom_D525 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_D525', tag='Atom_D525')
ProcessorTypeValues.Atom_N230 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N230', tag='Atom_N230')
ProcessorTypeValues.Atom_N270 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N270', tag='Atom_N270')
ProcessorTypeValues.Atom_N280 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N280', tag='Atom_N280')
ProcessorTypeValues.Atom_N330 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N330', tag='Atom_N330')
ProcessorTypeValues.Atom_N470 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N470', tag='Atom_N470')
ProcessorTypeValues.Atom_N475 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N475', tag='Atom_N475')
ProcessorTypeValues.Atom_N550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_N550', tag='Atom_N550')
ProcessorTypeValues.Atom_Silverthorne = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_Silverthorne', tag='Atom_Silverthorne')
ProcessorTypeValues.Atom_Z330 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_Z330', tag='Atom_Z330')
ProcessorTypeValues.Atom_Z515 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Atom_Z515', tag='Atom_Z515')
ProcessorTypeValues.bulverde = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='bulverde', tag='bulverde')
ProcessorTypeValues.c167cr = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='c167cr', tag='c167cr')
ProcessorTypeValues.celeron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='celeron', tag='celeron')
ProcessorTypeValues.Celeron_450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_450', tag='Celeron_450')
ProcessorTypeValues.Celeron_585 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_585', tag='Celeron_585')
ProcessorTypeValues.Celeron_743 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_743', tag='Celeron_743')
ProcessorTypeValues.Celeron_900 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_900', tag='Celeron_900')
ProcessorTypeValues.Celeron_925 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_925', tag='Celeron_925')
ProcessorTypeValues.Celeron_D_Processor_360 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_D_Processor_360', tag='Celeron_D_Processor_360')
ProcessorTypeValues.Celeron_D_Processor_420 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_D_Processor_420', tag='Celeron_D_Processor_420')
ProcessorTypeValues.Celeron_D_Processor_440 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_D_Processor_440', tag='Celeron_D_Processor_440')
ProcessorTypeValues.Celeron_E1200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_E1200', tag='Celeron_E1200')
ProcessorTypeValues.Celeron_E1500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_E1500', tag='Celeron_E1500')
ProcessorTypeValues.Celeron_E3200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_E3200', tag='Celeron_E3200')
ProcessorTypeValues.Celeron_E3300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_E3300', tag='Celeron_E3300')
ProcessorTypeValues.Celeron_M_353 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_353', tag='Celeron_M_353')
ProcessorTypeValues.Celeron_M_440 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_440', tag='Celeron_M_440')
ProcessorTypeValues.Celeron_M_520 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_520', tag='Celeron_M_520')
ProcessorTypeValues.Celeron_M_530 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_530', tag='Celeron_M_530')
ProcessorTypeValues.Celeron_M_540 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_540', tag='Celeron_M_540')
ProcessorTypeValues.Celeron_M_550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_550', tag='Celeron_M_550')
ProcessorTypeValues.Celeron_M_560 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_560', tag='Celeron_M_560')
ProcessorTypeValues.Celeron_M_575 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_575', tag='Celeron_M_575')
ProcessorTypeValues.Celeron_M_585 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_585', tag='Celeron_M_585')
ProcessorTypeValues.Celeron_M_T1400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_M_T1400', tag='Celeron_M_T1400')
ProcessorTypeValues.Celeron_SU2300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_SU2300', tag='Celeron_SU2300')
ProcessorTypeValues.Celeron_T1500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_T1500', tag='Celeron_T1500')
ProcessorTypeValues.Celeron_T3000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_T3000', tag='Celeron_T3000')
ProcessorTypeValues.Celeron_T4500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Celeron_T4500', tag='Celeron_T4500')
ProcessorTypeValues.Core_2_Duo = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo', tag='Core_2_Duo')
ProcessorTypeValues.Core_2_Duo_E2200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E2200', tag='Core_2_Duo_E2200')
ProcessorTypeValues.Core_2_Duo_E4000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E4000', tag='Core_2_Duo_E4000')
ProcessorTypeValues.Core_2_Duo_E4300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E4300', tag='Core_2_Duo_E4300')
ProcessorTypeValues.Core_2_Duo_E4400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E4400', tag='Core_2_Duo_E4400')
ProcessorTypeValues.Core_2_Duo_E4500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E4500', tag='Core_2_Duo_E4500')
ProcessorTypeValues.Core_2_Duo_E4600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E4600', tag='Core_2_Duo_E4600')
ProcessorTypeValues.Core_2_Duo_E5500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E5500', tag='Core_2_Duo_E5500')
ProcessorTypeValues.Core_2_Duo_E6300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E6300', tag='Core_2_Duo_E6300')
ProcessorTypeValues.Core_2_Duo_E6400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E6400', tag='Core_2_Duo_E6400')
ProcessorTypeValues.Core_2_Duo_E6420 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E6420', tag='Core_2_Duo_E6420')
ProcessorTypeValues.Core_2_Duo_E6600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E6600', tag='Core_2_Duo_E6600')
ProcessorTypeValues.Core_2_Duo_E7200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E7200', tag='Core_2_Duo_E7200')
ProcessorTypeValues.Core_2_Duo_E7300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E7300', tag='Core_2_Duo_E7300')
ProcessorTypeValues.Core_2_Duo_E7400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E7400', tag='Core_2_Duo_E7400')
ProcessorTypeValues.Core_2_Duo_E7500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E7500', tag='Core_2_Duo_E7500')
ProcessorTypeValues.Core_2_Duo_E7600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E7600', tag='Core_2_Duo_E7600')
ProcessorTypeValues.Core_2_Duo_E8400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E8400', tag='Core_2_Duo_E8400')
ProcessorTypeValues.Core_2_Duo_E8500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_E8500', tag='Core_2_Duo_E8500')
ProcessorTypeValues.Core_2_Duo_L7500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_L7500', tag='Core_2_Duo_L7500')
ProcessorTypeValues.Core_2_Duo_P3750 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P3750', tag='Core_2_Duo_P3750')
ProcessorTypeValues.Core_2_Duo_P7350 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P7350', tag='Core_2_Duo_P7350')
ProcessorTypeValues.Core_2_Duo_P7370 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P7370', tag='Core_2_Duo_P7370')
ProcessorTypeValues.Core_2_Duo_P7450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P7450', tag='Core_2_Duo_P7450')
ProcessorTypeValues.Core_2_Duo_P7550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P7550', tag='Core_2_Duo_P7550')
ProcessorTypeValues.Core_2_Duo_P8400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P8400', tag='Core_2_Duo_P8400')
ProcessorTypeValues.Core_2_Duo_P8600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P8600', tag='Core_2_Duo_P8600')
ProcessorTypeValues.Core_2_Duo_P8700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P8700', tag='Core_2_Duo_P8700')
ProcessorTypeValues.Core_2_Duo_P8800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P8800', tag='Core_2_Duo_P8800')
ProcessorTypeValues.Core_2_Duo_P9500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P9500', tag='Core_2_Duo_P9500')
ProcessorTypeValues.Core_2_Duo_P9600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_P9600', tag='Core_2_Duo_P9600')
ProcessorTypeValues.Core_2_Duo_SL7100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SL7100', tag='Core_2_Duo_SL7100')
ProcessorTypeValues.Core_2_Duo_SL9300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SL9300', tag='Core_2_Duo_SL9300')
ProcessorTypeValues.Core_2_Duo_SL9400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SL9400', tag='Core_2_Duo_SL9400')
ProcessorTypeValues.Core_2_Duo_SL9600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SL9600', tag='Core_2_Duo_SL9600')
ProcessorTypeValues.Core_2_Duo_SP9400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SP9400', tag='Core_2_Duo_SP9400')
ProcessorTypeValues.Core_2_Duo_SU4100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SU4100', tag='Core_2_Duo_SU4100')
ProcessorTypeValues.Core_2_Duo_SU7300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SU7300', tag='Core_2_Duo_SU7300')
ProcessorTypeValues.Core_2_Duo_SU9300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SU9300', tag='Core_2_Duo_SU9300')
ProcessorTypeValues.Core_2_Duo_SU9400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SU9400', tag='Core_2_Duo_SU9400')
ProcessorTypeValues.Core_2_Duo_SU_9600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_SU_9600', tag='Core_2_Duo_SU_9600')
ProcessorTypeValues.Core_2_Duo_T2310 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T2310', tag='Core_2_Duo_T2310')
ProcessorTypeValues.Core_2_Duo_T2330 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T2330', tag='Core_2_Duo_T2330')
ProcessorTypeValues.Core_2_Duo_T2390 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T2390', tag='Core_2_Duo_T2390')
ProcessorTypeValues.Core_2_Duo_T2450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T2450', tag='Core_2_Duo_T2450')
ProcessorTypeValues.Core_2_Duo_T4200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T4200', tag='Core_2_Duo_T4200')
ProcessorTypeValues.Core_2_Duo_T5200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5200', tag='Core_2_Duo_T5200')
ProcessorTypeValues.Core_2_Duo_T5250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5250', tag='Core_2_Duo_T5250')
ProcessorTypeValues.Core_2_Duo_T5270 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5270', tag='Core_2_Duo_T5270')
ProcessorTypeValues.Core_2_Duo_T5300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5300', tag='Core_2_Duo_T5300')
ProcessorTypeValues.Core_2_Duo_T5450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5450', tag='Core_2_Duo_T5450')
ProcessorTypeValues.Core_2_Duo_T5470 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5470', tag='Core_2_Duo_T5470')
ProcessorTypeValues.Core_2_Duo_T5500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5500', tag='Core_2_Duo_T5500')
ProcessorTypeValues.Core_2_Duo_T5550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5550', tag='Core_2_Duo_T5550')
ProcessorTypeValues.Core_2_Duo_T5600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5600', tag='Core_2_Duo_T5600')
ProcessorTypeValues.Core_2_Duo_T5670 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5670', tag='Core_2_Duo_T5670')
ProcessorTypeValues.Core_2_Duo_T5750 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5750', tag='Core_2_Duo_T5750')
ProcessorTypeValues.Core_2_Duo_T5800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5800', tag='Core_2_Duo_T5800')
ProcessorTypeValues.Core_2_Duo_T5850 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5850', tag='Core_2_Duo_T5850')
ProcessorTypeValues.Core_2_Duo_T5870 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T5870', tag='Core_2_Duo_T5870')
ProcessorTypeValues.Core_2_Duo_T6400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T6400', tag='Core_2_Duo_T6400')
ProcessorTypeValues.Core_2_Duo__T6500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo__T6500', tag='Core_2_Duo__T6500')
ProcessorTypeValues.Core_2_Duo_T6570 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T6570', tag='Core_2_Duo_T6570')
ProcessorTypeValues.Core_2_Duo_T6600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T6600', tag='Core_2_Duo_T6600')
ProcessorTypeValues.Core_2_Duo_T6670 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T6670', tag='Core_2_Duo_T6670')
ProcessorTypeValues.Core_2_Duo_T7100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7100', tag='Core_2_Duo_T7100')
ProcessorTypeValues.Core_2_Duo_T7200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7200', tag='Core_2_Duo_T7200')
ProcessorTypeValues.Core_2_Duo_T7250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7250', tag='Core_2_Duo_T7250')
ProcessorTypeValues.Core_2_Duo_T7270 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7270', tag='Core_2_Duo_T7270')
ProcessorTypeValues.Core_2_Duo_T7300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7300', tag='Core_2_Duo_T7300')
ProcessorTypeValues.Core_2_Duo_T7350 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7350', tag='Core_2_Duo_T7350')
ProcessorTypeValues.Core_2_Duo_T7400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7400', tag='Core_2_Duo_T7400')
ProcessorTypeValues.Core_2_Duo_T7500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7500', tag='Core_2_Duo_T7500')
ProcessorTypeValues.Core_2_Duo_T7700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T7700', tag='Core_2_Duo_T7700')
ProcessorTypeValues.Core_2_Duo_T8100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T8100', tag='Core_2_Duo_T8100')
ProcessorTypeValues.Core_2_Duo_T8300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T8300', tag='Core_2_Duo_T8300')
ProcessorTypeValues.Core_2_Duo_T8400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T8400', tag='Core_2_Duo_T8400')
ProcessorTypeValues.Core_2_Duo_T8700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T8700', tag='Core_2_Duo_T8700')
ProcessorTypeValues.Core_2_Duo_T9300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T9300', tag='Core_2_Duo_T9300')
ProcessorTypeValues.Core_2_Duo_T9400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T9400', tag='Core_2_Duo_T9400')
ProcessorTypeValues.Core_2_Duo_T9500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T9500', tag='Core_2_Duo_T9500')
ProcessorTypeValues.Core_2_Duo_T9550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T9550', tag='Core_2_Duo_T9550')
ProcessorTypeValues.Core_2_Duo_T9600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T9600', tag='Core_2_Duo_T9600')
ProcessorTypeValues.Core_2_Duo_T9900 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_T9900', tag='Core_2_Duo_T9900')
ProcessorTypeValues.Core_2_Duo_U1400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_U1400', tag='Core_2_Duo_U1400')
ProcessorTypeValues.Core_2_Duo_U2200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_U2200', tag='Core_2_Duo_U2200')
ProcessorTypeValues.Core_2_Duo_U7500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_U7500', tag='Core_2_Duo_U7500')
ProcessorTypeValues.Core_2_Duo_U7700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Duo_U7700', tag='Core_2_Duo_U7700')
ProcessorTypeValues.Core_2_Quad_9600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_9600', tag='Core_2_Quad_9600')
ProcessorTypeValues.Core_2_Quad_Q6600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q6600', tag='Core_2_Quad_Q6600')
ProcessorTypeValues.Core_2_Quad_Q6700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q6700', tag='Core_2_Quad_Q6700')
ProcessorTypeValues.Core_2_Quad_Q8200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q8200', tag='Core_2_Quad_Q8200')
ProcessorTypeValues.Core_2_Quad_Q8200S = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q8200S', tag='Core_2_Quad_Q8200S')
ProcessorTypeValues.Core_2_Quad_Q8300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q8300', tag='Core_2_Quad_Q8300')
ProcessorTypeValues.Core_2_Quad_Q8400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q8400', tag='Core_2_Quad_Q8400')
ProcessorTypeValues.Core_2_Quad_Q8400S = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q8400S', tag='Core_2_Quad_Q8400S')
ProcessorTypeValues.Core_2_Quad_Q9000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9000', tag='Core_2_Quad_Q9000')
ProcessorTypeValues.Core_2_Quad_Q9300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9300', tag='Core_2_Quad_Q9300')
ProcessorTypeValues.Core_2_Quad_Q9400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9400', tag='Core_2_Quad_Q9400')
ProcessorTypeValues.Core_2_Quad_Q9400S = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9400S', tag='Core_2_Quad_Q9400S')
ProcessorTypeValues.Core_2_Quad_Q9450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9450', tag='Core_2_Quad_Q9450')
ProcessorTypeValues.Core_2_Quad_Q9500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9500', tag='Core_2_Quad_Q9500')
ProcessorTypeValues.Core_2_Quad_Q9550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Quad_Q9550', tag='Core_2_Quad_Q9550')
ProcessorTypeValues.Core_2_Solo_SU3500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_2_Solo_SU3500', tag='Core_2_Solo_SU3500')
ProcessorTypeValues.Core_Duo_1V_L2400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_Duo_1V_L2400', tag='Core_Duo_1V_L2400')
ProcessorTypeValues.Core_Duo_LV_L2400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_Duo_LV_L2400', tag='Core_Duo_LV_L2400')
ProcessorTypeValues.Core_Duo_T2250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_Duo_T2250', tag='Core_Duo_T2250')
ProcessorTypeValues.Core_Duo_T2400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_Duo_T2400', tag='Core_Duo_T2400')
ProcessorTypeValues.Core_Duo_U2400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_Duo_U2400', tag='Core_Duo_U2400')
ProcessorTypeValues.core_i3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='core_i3', tag='core_i3')
ProcessorTypeValues.Core_i3_2330M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_2330M', tag='Core_i3_2330M')
ProcessorTypeValues.Core_i3_330UM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_330UM', tag='Core_i3_330UM')
ProcessorTypeValues.Core_i3_350M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_350M', tag='Core_i3_350M')
ProcessorTypeValues.Core_i3_370M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_370M', tag='Core_i3_370M')
ProcessorTypeValues.Core_i3_380M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_380M', tag='Core_i3_380M')
ProcessorTypeValues.Core_i3_380UM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_380UM', tag='Core_i3_380UM')
ProcessorTypeValues.Core_i3_520M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_520M', tag='Core_i3_520M')
ProcessorTypeValues.Core_i3_530 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_530', tag='Core_i3_530')
ProcessorTypeValues.Core_i3_530M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_530M', tag='Core_i3_530M')
ProcessorTypeValues.Core_i3_540 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_540', tag='Core_i3_540')
ProcessorTypeValues.Core_i3_540M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_540M', tag='Core_i3_540M')
ProcessorTypeValues.Core_i3_550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i3_550', tag='Core_i3_550')
ProcessorTypeValues.core_i5 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='core_i5', tag='core_i5')
ProcessorTypeValues.Core_i5_2300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_2300', tag='Core_i5_2300')
ProcessorTypeValues.Core_i5_2520M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_2520M', tag='Core_i5_2520M')
ProcessorTypeValues.Core_i5_2540M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_2540M', tag='Core_i5_2540M')
ProcessorTypeValues.Core_i5_430M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_430M', tag='Core_i5_430M')
ProcessorTypeValues.Core_i5_430UM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_430UM', tag='Core_i5_430UM')
ProcessorTypeValues.Core_i5_450M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_450M', tag='Core_i5_450M')
ProcessorTypeValues.Core_i5_460M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_460M', tag='Core_i5_460M')
ProcessorTypeValues.Core_i5_470UM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_470UM', tag='Core_i5_470UM')
ProcessorTypeValues.Core_i5_480M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_480M', tag='Core_i5_480M')
ProcessorTypeValues.Core_i5_560M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_560M', tag='Core_i5_560M')
ProcessorTypeValues.Core_i5_650 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_650', tag='Core_i5_650')
ProcessorTypeValues.Core_i5_655K = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_655K', tag='Core_i5_655K')
ProcessorTypeValues.Core_i5_660 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_660', tag='Core_i5_660')
ProcessorTypeValues.Core_i5_750 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_750', tag='Core_i5_750')
ProcessorTypeValues.Core_i5_760 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5_760', tag='Core_i5_760')
ProcessorTypeValues.Core_i5__760 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i5__760', tag='Core_i5__760')
ProcessorTypeValues.core_i7 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='core_i7', tag='core_i7')
ProcessorTypeValues.Core_i7_2600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_2600', tag='Core_i7_2600')
ProcessorTypeValues.Core_i7_2620QM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_2620QM', tag='Core_i7_2620QM')
ProcessorTypeValues.Core_i7_2630QM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_2630QM', tag='Core_i7_2630QM')
ProcessorTypeValues.Core_i7_2720QM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_2720QM', tag='Core_i7_2720QM')
ProcessorTypeValues.Core_i7_2820QM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_2820QM', tag='Core_i7_2820QM')
ProcessorTypeValues.Core_i7_4800MQ = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_4800MQ', tag='Core_i7_4800MQ')
ProcessorTypeValues.Core_i7_620LM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_620LM', tag='Core_i7_620LM')
ProcessorTypeValues.Core_i7_620M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_620M', tag='Core_i7_620M')
ProcessorTypeValues.Core_i7_640LM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_640LM', tag='Core_i7_640LM')
ProcessorTypeValues.Core_i7_640M = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_640M', tag='Core_i7_640M')
ProcessorTypeValues.Core_i7_640UM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_640UM', tag='Core_i7_640UM')
ProcessorTypeValues.Core_i7_680UM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_680UM', tag='Core_i7_680UM')
ProcessorTypeValues.Core_i7_740QM = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_740QM', tag='Core_i7_740QM')
ProcessorTypeValues.Core_i7_860 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_860', tag='Core_i7_860')
ProcessorTypeValues.Core_i7_870 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_870', tag='Core_i7_870')
ProcessorTypeValues.Core_i7_875K = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_875K', tag='Core_i7_875K')
ProcessorTypeValues.Core_i7_920 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_920', tag='Core_i7_920')
ProcessorTypeValues.Core_i7_930 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_930', tag='Core_i7_930')
ProcessorTypeValues.Core_i7_940 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_940', tag='Core_i7_940')
ProcessorTypeValues.Core_i7_950 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_950', tag='Core_i7_950')
ProcessorTypeValues.Core_i7_960 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_960', tag='Core_i7_960')
ProcessorTypeValues.Core_i7_980 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_i7_980', tag='Core_i7_980')
ProcessorTypeValues.Core_Solo_U1500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Core_Solo_U1500', tag='Core_Solo_U1500')
ProcessorTypeValues.crusoe_5800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='crusoe_5800', tag='crusoe_5800')
ProcessorTypeValues.crusoe_tm3200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='crusoe_tm3200', tag='crusoe_tm3200')
ProcessorTypeValues.crusoe_tm5500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='crusoe_tm5500', tag='crusoe_tm5500')
ProcessorTypeValues.crusoe_tm5600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='crusoe_tm5600', tag='crusoe_tm5600')
ProcessorTypeValues.C_Series_C_50 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='C_Series_C_50', tag='C_Series_C_50')
ProcessorTypeValues.cyrix_iii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='cyrix_iii', tag='cyrix_iii')
ProcessorTypeValues.cyrix_mii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='cyrix_mii', tag='cyrix_mii')
ProcessorTypeValues.duron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='duron', tag='duron')
ProcessorTypeValues.eden = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='eden', tag='eden')
ProcessorTypeValues.eden_esp = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='eden_esp', tag='eden_esp')
ProcessorTypeValues.eden_esp_4000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='eden_esp_4000', tag='eden_esp_4000')
ProcessorTypeValues.eden_esp_5000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='eden_esp_5000', tag='eden_esp_5000')
ProcessorTypeValues.eden_esp_6000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='eden_esp_6000', tag='eden_esp_6000')
ProcessorTypeValues.eden_esp_7000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='eden_esp_7000', tag='eden_esp_7000')
ProcessorTypeValues.efficeon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='efficeon', tag='efficeon')
ProcessorTypeValues.efficeon_tm8000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='efficeon_tm8000', tag='efficeon_tm8000')
ProcessorTypeValues.efficeon_tm8600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='efficeon_tm8600', tag='efficeon_tm8600')
ProcessorTypeValues.efficion_8800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='efficion_8800', tag='efficion_8800')
ProcessorTypeValues.elansc300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='elansc300', tag='elansc300')
ProcessorTypeValues.elansc310 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='elansc310', tag='elansc310')
ProcessorTypeValues.elansc400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='elansc400', tag='elansc400')
ProcessorTypeValues.E_Series_Dual_Core_E_350 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='E_Series_Dual_Core_E_350', tag='E_Series_Dual_Core_E_350')
ProcessorTypeValues.E_Series_Processor_E_240 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='E_Series_Processor_E_240', tag='E_Series_Processor_E_240')
ProcessorTypeValues.extremecpu = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='extremecpu', tag='extremecpu')
ProcessorTypeValues.fx_series_eight_core_fx_8100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_eight_core_fx_8100', tag='fx_series_eight_core_fx_8100')
ProcessorTypeValues.fx_series_eight_core_fx_8120 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_eight_core_fx_8120', tag='fx_series_eight_core_fx_8120')
ProcessorTypeValues.fx_series_eight_core_fx_8150 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_eight_core_fx_8150', tag='fx_series_eight_core_fx_8150')
ProcessorTypeValues.fx_series_quad_core_fx_4100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_quad_core_fx_4100', tag='fx_series_quad_core_fx_4100')
ProcessorTypeValues.fx_series_quad_core_fx_4170 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_quad_core_fx_4170', tag='fx_series_quad_core_fx_4170')
ProcessorTypeValues.fx_series_quad_core_fx_b4150 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_quad_core_fx_b4150', tag='fx_series_quad_core_fx_b4150')
ProcessorTypeValues.fx_series_six_core_fx_6100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_six_core_fx_6100', tag='fx_series_six_core_fx_6100')
ProcessorTypeValues.fx_series_six_core_fx_6120 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='fx_series_six_core_fx_6120', tag='fx_series_six_core_fx_6120')
ProcessorTypeValues.g3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='g3', tag='g3')
ProcessorTypeValues.g4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='g4', tag='g4')
ProcessorTypeValues.g5 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='g5', tag='g5')
ProcessorTypeValues.geode_gx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='geode_gx', tag='geode_gx')
ProcessorTypeValues.Geode_GX = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Geode_GX', tag='Geode_GX')
ProcessorTypeValues.geode_gx1 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='geode_gx1', tag='geode_gx1')
ProcessorTypeValues.geode_gxlv = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='geode_gxlv', tag='geode_gxlv')
ProcessorTypeValues.geode_gxm = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='geode_gxm', tag='geode_gxm')
ProcessorTypeValues.geoden_x = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='geoden_x', tag='geoden_x')
ProcessorTypeValues.h8s = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='h8s', tag='h8s')
ProcessorTypeValues.handheld_engine_cxd2230ga = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='handheld_engine_cxd2230ga', tag='handheld_engine_cxd2230ga')
ProcessorTypeValues.handheld_engine_cxd2230ga_temp = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='handheld_engine_cxd2230ga_temp', tag='handheld_engine_cxd2230ga_temp')
ProcessorTypeValues.hitachi_sh3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='hitachi_sh3', tag='hitachi_sh3')
ProcessorTypeValues.hypersparc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='hypersparc', tag='hypersparc')
ProcessorTypeValues.intel_atom = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom', tag='intel_atom')
ProcessorTypeValues.intel_atom_230 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_230', tag='intel_atom_230')
ProcessorTypeValues.intel_atom_330 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_330', tag='intel_atom_330')
ProcessorTypeValues.intel_atom_n270 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_n270', tag='intel_atom_n270')
ProcessorTypeValues.intel_atom_n280 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_n280', tag='intel_atom_n280')
ProcessorTypeValues.intel_atom_n450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_n450', tag='intel_atom_n450')
ProcessorTypeValues.intel_atom_n455 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_n455', tag='intel_atom_n455')
ProcessorTypeValues.intel_atom_z520 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_z520', tag='intel_atom_z520')
ProcessorTypeValues.intel_atom_z530 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_atom_z530', tag='intel_atom_z530')
ProcessorTypeValues.intel_celeron_d = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_celeron_d', tag='intel_celeron_d')
ProcessorTypeValues.intel_centrino = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_centrino', tag='intel_centrino')
ProcessorTypeValues.intel_centrino_2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_centrino_2', tag='intel_centrino_2')
ProcessorTypeValues.intel_core_2_duo = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_2_duo', tag='intel_core_2_duo')
ProcessorTypeValues.intel_core_2_duo_mobile = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_2_duo_mobile', tag='intel_core_2_duo_mobile')
ProcessorTypeValues.intel_core_2_extreme = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_2_extreme', tag='intel_core_2_extreme')
ProcessorTypeValues.intel_core_2_quad = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_2_quad', tag='intel_core_2_quad')
ProcessorTypeValues.intel_core_2_solo = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_2_solo', tag='intel_core_2_solo')
ProcessorTypeValues.intel_core_duo = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_duo', tag='intel_core_duo')
ProcessorTypeValues.intel_core_solo = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_core_solo', tag='intel_core_solo')
ProcessorTypeValues.Intel_Mobile_CPU = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Intel_Mobile_CPU', tag='Intel_Mobile_CPU')
ProcessorTypeValues.intel_pentium_4_ht = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_pentium_4_ht', tag='intel_pentium_4_ht')
ProcessorTypeValues.intel_pentium_d = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_pentium_d', tag='intel_pentium_d')
ProcessorTypeValues.intel_strongarm = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_strongarm', tag='intel_strongarm')
ProcessorTypeValues.intel_xeon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_xeon', tag='intel_xeon')
ProcessorTypeValues.intel_xeon_mp = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_xeon_mp', tag='intel_xeon_mp')
ProcessorTypeValues.intel_xscale_pxa250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_xscale_pxa250', tag='intel_xscale_pxa250')
ProcessorTypeValues.intel_xscale_pxa255 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_xscale_pxa255', tag='intel_xscale_pxa255')
ProcessorTypeValues.intel_xscale_pxa263 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='intel_xscale_pxa263', tag='intel_xscale_pxa263')
ProcessorTypeValues.itanium = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='itanium', tag='itanium')
ProcessorTypeValues.itanium_2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='itanium_2', tag='itanium_2')
ProcessorTypeValues.k5 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k5', tag='k5')
ProcessorTypeValues.k6_2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k6_2', tag='k6_2')
ProcessorTypeValues.k6_2e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k6_2e', tag='k6_2e')
ProcessorTypeValues.k6_2_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k6_2_plus', tag='k6_2_plus')
ProcessorTypeValues.k6_3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k6_3', tag='k6_3')
ProcessorTypeValues.k6_iii_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k6_iii_plus', tag='k6_iii_plus')
ProcessorTypeValues.k6_mmx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='k6_mmx', tag='k6_mmx')
ProcessorTypeValues.mc68328 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mc68328', tag='mc68328')
ProcessorTypeValues.mc68ez328 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mc68ez328', tag='mc68ez328')
ProcessorTypeValues.mc68sz328 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mc68sz328', tag='mc68sz328')
ProcessorTypeValues.mc68vz328 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mc68vz328', tag='mc68vz328')
ProcessorTypeValues.mc88110 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mc88110', tag='mc88110')
ProcessorTypeValues.mediagx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mediagx', tag='mediagx')
ProcessorTypeValues.mediagxi = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mediagxi', tag='mediagxi')
ProcessorTypeValues.mediagxlv = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mediagxlv', tag='mediagxlv')
ProcessorTypeValues.mediagxm = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mediagxm', tag='mediagxm')
ProcessorTypeValues.microsparc_ii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='microsparc_ii', tag='microsparc_ii')
ProcessorTypeValues.microsparc_iiep = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='microsparc_iiep', tag='microsparc_iiep')
ProcessorTypeValues.mobile_athlon_4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_athlon_4', tag='mobile_athlon_4')
ProcessorTypeValues.mobile_athlon_xp_m = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_athlon_xp_m', tag='mobile_athlon_xp_m')
ProcessorTypeValues.mobile_athon_64 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_athon_64', tag='mobile_athon_64')
ProcessorTypeValues.mobile_celeron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_celeron', tag='mobile_celeron')
ProcessorTypeValues.mobile_duron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_duron', tag='mobile_duron')
ProcessorTypeValues.mobile_k6_2_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_k6_2_plus', tag='mobile_k6_2_plus')
ProcessorTypeValues.mobile_pentium_2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_pentium_2', tag='mobile_pentium_2')
ProcessorTypeValues.mobile_pentium_3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_pentium_3', tag='mobile_pentium_3')
ProcessorTypeValues.mobile_pentium_4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_pentium_4', tag='mobile_pentium_4')
ProcessorTypeValues.mobile_pentium_4_ht = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_pentium_4_ht', tag='mobile_pentium_4_ht')
ProcessorTypeValues.mobile_sempron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='mobile_sempron', tag='mobile_sempron')
ProcessorTypeValues.motorola_dragonball = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='motorola_dragonball', tag='motorola_dragonball')
ProcessorTypeValues.nec_mips = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='nec_mips', tag='nec_mips')
ProcessorTypeValues.none = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='none', tag='none')
ProcessorTypeValues.omap1710 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='omap1710', tag='omap1710')
ProcessorTypeValues.omap310 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='omap310', tag='omap310')
ProcessorTypeValues.omap311 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='omap311', tag='omap311')
ProcessorTypeValues.omap850 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='omap850', tag='omap850')
ProcessorTypeValues.opteron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='opteron', tag='opteron')
ProcessorTypeValues.pa_7100lc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_7100lc', tag='pa_7100lc')
ProcessorTypeValues.pa_7200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_7200', tag='pa_7200')
ProcessorTypeValues.pa_7300lc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_7300lc', tag='pa_7300lc')
ProcessorTypeValues.pa_8000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8000', tag='pa_8000')
ProcessorTypeValues.pa_8200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8200', tag='pa_8200')
ProcessorTypeValues.pa_8500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8500', tag='pa_8500')
ProcessorTypeValues.pa_8600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8600', tag='pa_8600')
ProcessorTypeValues.pa_8700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8700', tag='pa_8700')
ProcessorTypeValues.pa_8700_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8700_plus', tag='pa_8700_plus')
ProcessorTypeValues.pa_8800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8800', tag='pa_8800')
ProcessorTypeValues.pa_8900 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pa_8900', tag='pa_8900')
ProcessorTypeValues.pentium = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium', tag='pentium')
ProcessorTypeValues.pentium_2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_2', tag='pentium_2')
ProcessorTypeValues.pentium_3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_3', tag='pentium_3')
ProcessorTypeValues.pentium_3_xeon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_3_xeon', tag='pentium_3_xeon')
ProcessorTypeValues.pentium_4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_4', tag='pentium_4')
ProcessorTypeValues.pentium_4_extreme_edition = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_4_extreme_edition', tag='pentium_4_extreme_edition')
ProcessorTypeValues.Pentium_D_925 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_D_925', tag='Pentium_D_925')
ProcessorTypeValues.Pentium_D_T2060 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_D_T2060', tag='Pentium_D_T2060')
ProcessorTypeValues.pentium_dual_core = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_dual_core', tag='pentium_dual_core')
ProcessorTypeValues.Pentium_E2140 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E2140', tag='Pentium_E2140')
ProcessorTypeValues.Pentium_E2160 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E2160', tag='Pentium_E2160')
ProcessorTypeValues.Pentium_E2180 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E2180', tag='Pentium_E2180')
ProcessorTypeValues.Pentium_E2200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E2200', tag='Pentium_E2200')
ProcessorTypeValues.Pentium_E2220 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E2220', tag='Pentium_E2220')
ProcessorTypeValues.Pentium_E3200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E3200', tag='Pentium_E3200')
ProcessorTypeValues.Pentium_E4400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E4400', tag='Pentium_E4400')
ProcessorTypeValues.Pentium_E5200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5200', tag='Pentium_E5200')
ProcessorTypeValues.Pentium_E5300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5300', tag='Pentium_E5300')
ProcessorTypeValues.Pentium_E5301 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5301', tag='Pentium_E5301')
ProcessorTypeValues.Pentium_E5400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5400', tag='Pentium_E5400')
ProcessorTypeValues.Pentium_E5500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5500', tag='Pentium_E5500')
ProcessorTypeValues.Pentium_E5700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5700', tag='Pentium_E5700')
ProcessorTypeValues.Pentium_E5800 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E5800', tag='Pentium_E5800')
ProcessorTypeValues.Pentium_E6300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E6300', tag='Pentium_E6300')
ProcessorTypeValues.Pentium_E6600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E6600', tag='Pentium_E6600')
ProcessorTypeValues.Pentium_E6700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E6700', tag='Pentium_E6700')
ProcessorTypeValues.Pentium_E7200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E7200', tag='Pentium_E7200')
ProcessorTypeValues.Pentium_E7400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E7400', tag='Pentium_E7400')
ProcessorTypeValues.Pentium_E8400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_E8400', tag='Pentium_E8400')
ProcessorTypeValues.Pentium_G6950 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_G6950', tag='Pentium_G6950')
ProcessorTypeValues.pentium_iii_e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_iii_e', tag='pentium_iii_e')
ProcessorTypeValues.pentium_iii_s = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_iii_s', tag='pentium_iii_s')
ProcessorTypeValues.pentium_ii_xeon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_ii_xeon', tag='pentium_ii_xeon')
ProcessorTypeValues.pentium_m = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_m', tag='pentium_m')
ProcessorTypeValues.Pentium_M_738 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_M_738', tag='Pentium_M_738')
ProcessorTypeValues.Pentium_M_778 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_M_778', tag='Pentium_M_778')
ProcessorTypeValues.pentium_mmx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_mmx', tag='pentium_mmx')
ProcessorTypeValues.Pentium_P6000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_P6000', tag='Pentium_P6000')
ProcessorTypeValues.Pentium_P6100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_P6100', tag='Pentium_P6100')
ProcessorTypeValues.Pentium_P6200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_P6200', tag='Pentium_P6200')
ProcessorTypeValues.pentium_pro = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_pro', tag='pentium_pro')
ProcessorTypeValues.Pentium_SU2700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_SU2700', tag='Pentium_SU2700')
ProcessorTypeValues.Pentium_SU4100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_SU4100', tag='Pentium_SU4100')
ProcessorTypeValues.Pentium_T2080 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2080', tag='Pentium_T2080')
ProcessorTypeValues.Pentium_T2130 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2130', tag='Pentium_T2130')
ProcessorTypeValues.Pentium_T2310 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2310', tag='Pentium_T2310')
ProcessorTypeValues.Pentium_T2330 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2330', tag='Pentium_T2330')
ProcessorTypeValues.Pentium_T2350 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2350', tag='Pentium_T2350')
ProcessorTypeValues.Pentium_T2370 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2370', tag='Pentium_T2370')
ProcessorTypeValues.Pentium_T2390 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T2390', tag='Pentium_T2390')
ProcessorTypeValues.Pentium_T3200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T3200', tag='Pentium_T3200')
ProcessorTypeValues.Pentium_T3400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T3400', tag='Pentium_T3400')
ProcessorTypeValues.Pentium_T4200 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T4200', tag='Pentium_T4200')
ProcessorTypeValues.Pentium_T4300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T4300', tag='Pentium_T4300')
ProcessorTypeValues.Pentium_T4400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T4400', tag='Pentium_T4400')
ProcessorTypeValues.Pentium_T4500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T4500', tag='Pentium_T4500')
ProcessorTypeValues.Pentium_T6570 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_T6570', tag='Pentium_T6570')
ProcessorTypeValues.Pentium_U5400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_U5400', tag='Pentium_U5400')
ProcessorTypeValues.Pentium_U5600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Pentium_U5600', tag='Pentium_U5600')
ProcessorTypeValues.pentium_xeon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pentium_xeon', tag='pentium_xeon')
ProcessorTypeValues.phenom_dual_core = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_dual_core', tag='phenom_dual_core')
ProcessorTypeValues.phenom_ii_x2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_ii_x2', tag='phenom_ii_x2')
ProcessorTypeValues.Phenom_II_X2_Dual_Core_511 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X2_Dual_Core_511', tag='Phenom_II_X2_Dual_Core_511')
ProcessorTypeValues.Phenom_II_X2_Dual_Core_550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X2_Dual_Core_550', tag='Phenom_II_X2_Dual_Core_550')
ProcessorTypeValues.Phenom_II_X2_Dual_Core_N620 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X2_Dual_Core_N620', tag='Phenom_II_X2_Dual_Core_N620')
ProcessorTypeValues.Phenom_II_X2_Dual_Core_N640 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X2_Dual_Core_N640', tag='Phenom_II_X2_Dual_Core_N640')
ProcessorTypeValues.Phenom_II_X2_Dual_Core_N650 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X2_Dual_Core_N650', tag='Phenom_II_X2_Dual_Core_N650')
ProcessorTypeValues.Phenom_II_X2_Dual_Core_N660 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X2_Dual_Core_N660', tag='Phenom_II_X2_Dual_Core_N660')
ProcessorTypeValues.phenom_ii_x3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_ii_x3', tag='phenom_ii_x3')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_8400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_8400', tag='Phenom_II_X3_Triple_Core_8400')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_8450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_8450', tag='Phenom_II_X3_Triple_Core_8450')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_8550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_8550', tag='Phenom_II_X3_Triple_Core_8550')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_8650 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_8650', tag='Phenom_II_X3_Triple_Core_8650')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_B75 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_B75', tag='Phenom_II_X3_Triple_Core_B75')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_N830 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_N830', tag='Phenom_II_X3_Triple_Core_N830')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_N850 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_N850', tag='Phenom_II_X3_Triple_Core_N850')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_P820 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_P820', tag='Phenom_II_X3_Triple_Core_P820')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_P840 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_P840', tag='Phenom_II_X3_Triple_Core_P840')
ProcessorTypeValues.Phenom_II_X3_Triple_Core_P860 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X3_Triple_Core_P860', tag='Phenom_II_X3_Triple_Core_P860')
ProcessorTypeValues.phenom_ii_x4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_ii_x4', tag='phenom_ii_x4')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_810 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_810', tag='Phenom_II_X4_Quad_Core_810')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_820 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_820', tag='Phenom_II_X4_Quad_Core_820')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_830 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_830', tag='Phenom_II_X4_Quad_Core_830')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_840T = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_840T', tag='Phenom_II_X4_Quad_Core_840T')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_910 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_910', tag='Phenom_II_X4_Quad_Core_910')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9100E = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9100E', tag='Phenom_II_X4_Quad_Core_9100E')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9150 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9150', tag='Phenom_II_X4_Quad_Core_9150')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_920 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_920', tag='Phenom_II_X4_Quad_Core_920')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_925 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_925', tag='Phenom_II_X4_Quad_Core_925')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9350 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9350', tag='Phenom_II_X4_Quad_Core_9350')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_945 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_945', tag='Phenom_II_X4_Quad_Core_945')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9450 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9450', tag='Phenom_II_X4_Quad_Core_9450')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9500', tag='Phenom_II_X4_Quad_Core_9500')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_955 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_955', tag='Phenom_II_X4_Quad_Core_955')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9550', tag='Phenom_II_X4_Quad_Core_9550')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9600', tag='Phenom_II_X4_Quad_Core_9600')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_965 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_965', tag='Phenom_II_X4_Quad_Core_965')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9650 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9650', tag='Phenom_II_X4_Quad_Core_9650')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9750 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9750', tag='Phenom_II_X4_Quad_Core_9750')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9850 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9850', tag='Phenom_II_X4_Quad_Core_9850')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_9950 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_9950', tag='Phenom_II_X4_Quad_Core_9950')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_N820 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_N820', tag='Phenom_II_X4_Quad_Core_N820')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_N930 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_N930', tag='Phenom_II_X4_Quad_Core_N930')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_N950 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_N950', tag='Phenom_II_X4_Quad_Core_N950')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_P920 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_P920', tag='Phenom_II_X4_Quad_Core_P920')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_P940 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_P940', tag='Phenom_II_X4_Quad_Core_P940')
ProcessorTypeValues.Phenom_II_X4_Quad_Core_P960 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X4_Quad_Core_P960', tag='Phenom_II_X4_Quad_Core_P960')
ProcessorTypeValues.phenom_ii_x6 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_ii_x6', tag='phenom_ii_x6')
ProcessorTypeValues.Phenom_II_X6_Six_Core_1035T = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X6_Six_Core_1035T', tag='Phenom_II_X6_Six_Core_1035T')
ProcessorTypeValues.Phenom_II_X6_Six_Core_1045T = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X6_Six_Core_1045T', tag='Phenom_II_X6_Six_Core_1045T')
ProcessorTypeValues.Phenom_II_X6_Six_Core_1055T = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X6_Six_Core_1055T', tag='Phenom_II_X6_Six_Core_1055T')
ProcessorTypeValues.Phenom_II_X6_Six_Core_1090T = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Phenom_II_X6_Six_Core_1090T', tag='Phenom_II_X6_Six_Core_1090T')
ProcessorTypeValues.phenom_quad_core = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_quad_core', tag='phenom_quad_core')
ProcessorTypeValues.phenom_triple_core = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='phenom_triple_core', tag='phenom_triple_core')
ProcessorTypeValues.power3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='power3', tag='power3')
ProcessorTypeValues.power3_ii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='power3_ii', tag='power3_ii')
ProcessorTypeValues.power4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='power4', tag='power4')
ProcessorTypeValues.power4_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='power4_plus', tag='power4_plus')
ProcessorTypeValues.power5 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='power5', tag='power5')
ProcessorTypeValues.powerpc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc', tag='powerpc')
ProcessorTypeValues.powerpc_403 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_403', tag='powerpc_403')
ProcessorTypeValues.powerpc_403ga = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_403ga', tag='powerpc_403ga')
ProcessorTypeValues.powerpc_403_gcx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_403_gcx', tag='powerpc_403_gcx')
ProcessorTypeValues.powerpc_440gx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_440gx', tag='powerpc_440gx')
ProcessorTypeValues.powerpc_601 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_601', tag='powerpc_601')
ProcessorTypeValues.powerpc_603 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_603', tag='powerpc_603')
ProcessorTypeValues.powerpc_603e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_603e', tag='powerpc_603e')
ProcessorTypeValues.powerpc_603ev = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_603ev', tag='powerpc_603ev')
ProcessorTypeValues.powerpc_604 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_604', tag='powerpc_604')
ProcessorTypeValues.powerpc_604e = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_604e', tag='powerpc_604e')
ProcessorTypeValues.powerpc_740_g3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_740_g3', tag='powerpc_740_g3')
ProcessorTypeValues.powerpc_750cx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_750cx', tag='powerpc_750cx')
ProcessorTypeValues.powerpc_750_g3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_750_g3', tag='powerpc_750_g3')
ProcessorTypeValues.powerpc_970 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_970', tag='powerpc_970')
ProcessorTypeValues.powerpc_rs64 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_rs64', tag='powerpc_rs64')
ProcessorTypeValues.powerpc_rs64_ii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_rs64_ii', tag='powerpc_rs64_ii')
ProcessorTypeValues.powerpc_rs64_iii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_rs64_iii', tag='powerpc_rs64_iii')
ProcessorTypeValues.powerpc_rs64_iv = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='powerpc_rs64_iv', tag='powerpc_rs64_iv')
ProcessorTypeValues.pr31700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='pr31700', tag='pr31700')
ProcessorTypeValues.r10000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r10000', tag='r10000')
ProcessorTypeValues.r12000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r12000', tag='r12000')
ProcessorTypeValues.r12000a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r12000a', tag='r12000a')
ProcessorTypeValues.r14000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r14000', tag='r14000')
ProcessorTypeValues.r14000a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r14000a', tag='r14000a')
ProcessorTypeValues.r16000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r16000', tag='r16000')
ProcessorTypeValues.r16000a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r16000a', tag='r16000a')
ProcessorTypeValues.r16000b = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r16000b', tag='r16000b')
ProcessorTypeValues.r3900 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r3900', tag='r3900')
ProcessorTypeValues.r3910 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r3910', tag='r3910')
ProcessorTypeValues.r3912 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r3912', tag='r3912')
ProcessorTypeValues.r4000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r4000', tag='r4000')
ProcessorTypeValues.r4300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r4300', tag='r4300')
ProcessorTypeValues.r4310 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r4310', tag='r4310')
ProcessorTypeValues.r4640 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r4640', tag='r4640')
ProcessorTypeValues.r4700 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r4700', tag='r4700')
ProcessorTypeValues.r5000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r5000', tag='r5000')
ProcessorTypeValues.r5230 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='r5230', tag='r5230')
ProcessorTypeValues.rm5231 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='rm5231', tag='rm5231')
ProcessorTypeValues.s3c2410 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='s3c2410', tag='s3c2410')
ProcessorTypeValues.s3c2440 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='s3c2440', tag='s3c2440')
ProcessorTypeValues.s3c2442 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='s3c2442', tag='s3c2442')
ProcessorTypeValues.sa_110 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sa_110', tag='sa_110')
ProcessorTypeValues.sa_1100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sa_1100', tag='sa_1100')
ProcessorTypeValues.sa_1110 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sa_1110', tag='sa_1110')
ProcessorTypeValues.sempron = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sempron', tag='sempron')
ProcessorTypeValues.Sempron_140 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_140', tag='Sempron_140')
ProcessorTypeValues.Sempron_3500_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_3500_plus', tag='Sempron_3500_plus')
ProcessorTypeValues.Sempron_3600_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_3600_plus', tag='Sempron_3600_plus')
ProcessorTypeValues.Sempron_LE_1250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_LE_1250', tag='Sempron_LE_1250')
ProcessorTypeValues.Sempron_LE_1300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_LE_1300', tag='Sempron_LE_1300')
ProcessorTypeValues.Sempron_M100 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_M100', tag='Sempron_M100')
ProcessorTypeValues.Sempron_M120 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Sempron_M120', tag='Sempron_M120')
ProcessorTypeValues.sh_4 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sh_4', tag='sh_4')
ProcessorTypeValues.sh7709a = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sh7709a', tag='sh7709a')
ProcessorTypeValues.sis550 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sis550', tag='sis550')
ProcessorTypeValues.snapdragon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='snapdragon', tag='snapdragon')
ProcessorTypeValues.sparc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sparc', tag='sparc')
ProcessorTypeValues.sparc64v = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sparc64v', tag='sparc64v')
ProcessorTypeValues.sparc_ii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='sparc_ii', tag='sparc_ii')
ProcessorTypeValues.supersparc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='supersparc', tag='supersparc')
ProcessorTypeValues.supersparc_ii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='supersparc_ii', tag='supersparc_ii')
ProcessorTypeValues.tegra = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tegra', tag='tegra')
ProcessorTypeValues.tegra_2_0 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tegra_2_0', tag='tegra_2_0')
ProcessorTypeValues.tegra_250 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tegra_250', tag='tegra_250')
ProcessorTypeValues.tegra_3_0 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tegra_3_0', tag='tegra_3_0')
ProcessorTypeValues.texas_instruments_omap1510 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='texas_instruments_omap1510', tag='texas_instruments_omap1510')
ProcessorTypeValues.tm_44 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tm_44', tag='tm_44')
ProcessorTypeValues.tmpr3922au = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tmpr3922au', tag='tmpr3922au')
ProcessorTypeValues.turbosparc = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='turbosparc', tag='turbosparc')
ProcessorTypeValues.turion_64 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='turion_64', tag='turion_64')
ProcessorTypeValues.turion_64_x2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='turion_64_x2', tag='turion_64_x2')
ProcessorTypeValues.Turion_64_X2_Dual_Core_RM_70 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Dual_Core_RM_70', tag='Turion_64_X2_Dual_Core_RM_70')
ProcessorTypeValues.Turion_64_X2_Dual_Core_RM_72 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Dual_Core_RM_72', tag='Turion_64_X2_Dual_Core_RM_72')
ProcessorTypeValues.Turion_64_X2_Dual_Core_TL_52 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Dual_Core_TL_52', tag='Turion_64_X2_Dual_Core_TL_52')
ProcessorTypeValues.Turion_64_X2_Dual_Core_TL_56 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Dual_Core_TL_56', tag='Turion_64_X2_Dual_Core_TL_56')
ProcessorTypeValues.Turion_64_X2_Mobile = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Mobile', tag='Turion_64_X2_Mobile')
ProcessorTypeValues.Turion_64_X2_RM_74 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_RM_74', tag='Turion_64_X2_RM_74')
ProcessorTypeValues.Turion_64_X2_TK_53 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TK_53', tag='Turion_64_X2_TK_53')
ProcessorTypeValues.Turion_64_X2_TK_55 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TK_55', tag='Turion_64_X2_TK_55')
ProcessorTypeValues.Turion_64_X2_TK_57 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TK_57', tag='Turion_64_X2_TK_57')
ProcessorTypeValues.Turion_64_X2_TK_58 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TK_58', tag='Turion_64_X2_TK_58')
ProcessorTypeValues.Turion_64_X2_TL_50 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_50', tag='Turion_64_X2_TL_50')
ProcessorTypeValues.Turion_64_X2_TL_57 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_57', tag='Turion_64_X2_TL_57')
ProcessorTypeValues.Turion_64_X2_TL_58 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_58', tag='Turion_64_X2_TL_58')
ProcessorTypeValues.Turion_64_X2_TL_60 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_60', tag='Turion_64_X2_TL_60')
ProcessorTypeValues.Turion_64_X2_TL_62 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_62', tag='Turion_64_X2_TL_62')
ProcessorTypeValues.Turion_64_X2_TL_64_Gold = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_64_Gold', tag='Turion_64_X2_TL_64_Gold')
ProcessorTypeValues.Turion_64_X2_TL_66 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_TL_66', tag='Turion_64_X2_TL_66')
ProcessorTypeValues.Turion_64_X2_Ultra_ZM_82 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Ultra_ZM_82', tag='Turion_64_X2_Ultra_ZM_82')
ProcessorTypeValues.Turion_64_X2_Ultra_ZM_85 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Ultra_ZM_85', tag='Turion_64_X2_Ultra_ZM_85')
ProcessorTypeValues.Turion_64_X2_Ultra_ZM_87 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_Ultra_ZM_87', tag='Turion_64_X2_Ultra_ZM_87')
ProcessorTypeValues.Turion_64_X2_ZM_72 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_ZM_72', tag='Turion_64_X2_ZM_72')
ProcessorTypeValues.Turion_64_X2_ZM_74 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_ZM_74', tag='Turion_64_X2_ZM_74')
ProcessorTypeValues.Turion_64_X2_ZM_80 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_64_X2_ZM_80', tag='Turion_64_X2_ZM_80')
ProcessorTypeValues.Turion_II_Neo_X2_Dual_Core_K625 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_Neo_X2_Dual_Core_K625', tag='Turion_II_Neo_X2_Dual_Core_K625')
ProcessorTypeValues.Turion_II_Neo_X2_Dual_Core_L625 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_Neo_X2_Dual_Core_L625', tag='Turion_II_Neo_X2_Dual_Core_L625')
ProcessorTypeValues.Turion_II_Ultra_X2_Dual_Core_M600 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_Ultra_X2_Dual_Core_M600', tag='Turion_II_Ultra_X2_Dual_Core_M600')
ProcessorTypeValues.Turion_II_Ultra_X2_Dual_Core_M620 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_Ultra_X2_Dual_Core_M620', tag='Turion_II_Ultra_X2_Dual_Core_M620')
ProcessorTypeValues.Turion_II_X2_Dual_Core_M300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_X2_Dual_Core_M300', tag='Turion_II_X2_Dual_Core_M300')
ProcessorTypeValues.Turion_II_X2_Dual_Core_M500 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_X2_Dual_Core_M500', tag='Turion_II_X2_Dual_Core_M500')
ProcessorTypeValues.Turion_II_X2_Dual_Core_M520 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_X2_Dual_Core_M520', tag='Turion_II_X2_Dual_Core_M520')
ProcessorTypeValues.Turion_II_X2_Dual_Core_P520 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_X2_Dual_Core_P520', tag='Turion_II_X2_Dual_Core_P520')
ProcessorTypeValues.Turion_II_X2_Dual_Core_P540 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_X2_Dual_Core_P540', tag='Turion_II_X2_Dual_Core_P540')
ProcessorTypeValues.Turion_II_X2_Dual_Core_P560 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_II_X2_Dual_Core_P560', tag='Turion_II_X2_Dual_Core_P560')
ProcessorTypeValues.Turion_X2_Dual_Core_RM_75 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_X2_Dual_Core_RM_75', tag='Turion_X2_Dual_Core_RM_75')
ProcessorTypeValues.Turion_X2_Ultra_Dual_Core_ZM_85 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Turion_X2_Ultra_Dual_Core_ZM_85', tag='Turion_X2_Ultra_Dual_Core_ZM_85')
ProcessorTypeValues.tx3922 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='tx3922', tag='tx3922')
ProcessorTypeValues.ultrasparc_i = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_i', tag='ultrasparc_i')
ProcessorTypeValues.ultrasparc_ii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_ii', tag='ultrasparc_ii')
ProcessorTypeValues.ultrasparc_iie = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iie', tag='ultrasparc_iie')
ProcessorTypeValues.ultrasparc_iii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iii', tag='ultrasparc_iii')
ProcessorTypeValues.ultrasparc_iii_cu = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iii_cu', tag='ultrasparc_iii_cu')
ProcessorTypeValues.ultrasparc_iiii = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iiii', tag='ultrasparc_iiii')
ProcessorTypeValues.ultrasparc_iii_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iii_plus', tag='ultrasparc_iii_plus')
ProcessorTypeValues.ultrasparc_iis = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iis', tag='ultrasparc_iis')
ProcessorTypeValues.ultrasparc_iv = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iv', tag='ultrasparc_iv')
ProcessorTypeValues.ultrasparc_iv_plus = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_iv_plus', tag='ultrasparc_iv_plus')
ProcessorTypeValues.ultrasparc_s400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_s400', tag='ultrasparc_s400')
ProcessorTypeValues.ultrasparc_t1 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='ultrasparc_t1', tag='ultrasparc_t1')
ProcessorTypeValues.unknown = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
ProcessorTypeValues.v25 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='v25', tag='v25')
ProcessorTypeValues.v30 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='v30', tag='v30')
ProcessorTypeValues.v30mx = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='v30mx', tag='v30mx')
ProcessorTypeValues.via_cyrix_c3 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='via_cyrix_c3', tag='via_cyrix_c3')
ProcessorTypeValues.vr4111 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='vr4111', tag='vr4111')
ProcessorTypeValues.vr4121 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='vr4121', tag='vr4121')
ProcessorTypeValues.vr4122 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='vr4122', tag='vr4122')
ProcessorTypeValues.vr4131 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='vr4131', tag='vr4131')
ProcessorTypeValues.vr4181 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='vr4181', tag='vr4181')
ProcessorTypeValues.vr4300 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='vr4300', tag='vr4300')
ProcessorTypeValues.V_Series_Single_Core_V105 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='V_Series_Single_Core_V105', tag='V_Series_Single_Core_V105')
ProcessorTypeValues.V_Series_Single_Core_V120 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='V_Series_Single_Core_V120', tag='V_Series_Single_Core_V120')
ProcessorTypeValues.V_Series_Single_Core_V140 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='V_Series_Single_Core_V140', tag='V_Series_Single_Core_V140')
ProcessorTypeValues.winchip_2 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='winchip_2', tag='winchip_2')
ProcessorTypeValues.winchip_c6 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='winchip_c6', tag='winchip_c6')
ProcessorTypeValues.Xeon = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon', tag='Xeon')
ProcessorTypeValues.Xeon_3000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_3000', tag='Xeon_3000')
ProcessorTypeValues.Xeon_3530 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_3530', tag='Xeon_3530')
ProcessorTypeValues.Xeon_5000 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_5000', tag='Xeon_5000')
ProcessorTypeValues.Xeon_5400 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_5400', tag='Xeon_5400')
ProcessorTypeValues.Xeon_E5504 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_E5504', tag='Xeon_E5504')
ProcessorTypeValues.Xeon_E5506 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_E5506', tag='Xeon_E5506')
ProcessorTypeValues.Xeon_E5520 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_E5520', tag='Xeon_E5520')
ProcessorTypeValues.Xeon_E5530 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_E5530', tag='Xeon_E5530')
ProcessorTypeValues.Xeon_W3503 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_W3503', tag='Xeon_W3503')
ProcessorTypeValues.Xeon_W5580 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_W5580', tag='Xeon_W5580')
ProcessorTypeValues.Xeon_X5560 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='Xeon_X5560', tag='Xeon_X5560')
ProcessorTypeValues.xscale = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale', tag='xscale')
ProcessorTypeValues.xscale_pxa260 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale_pxa260', tag='xscale_pxa260')
ProcessorTypeValues.xscale_pxa261 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale_pxa261', tag='xscale_pxa261')
ProcessorTypeValues.xscale_pxa262 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale_pxa262', tag='xscale_pxa262')
ProcessorTypeValues.xscale_pxa270 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale_pxa270', tag='xscale_pxa270')
ProcessorTypeValues.xscale_pxa272 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale_pxa272', tag='xscale_pxa272')
ProcessorTypeValues.xscale_pxa901 = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='xscale_pxa901', tag='xscale_pxa901')
ProcessorTypeValues.emptyString = ProcessorTypeValues._CF_enumeration.addEnumeration(unicode_value='', tag='emptyString')
ProcessorTypeValues._InitializeFacetMap(ProcessorTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ProcessorTypeValues', ProcessorTypeValues)

# Atomic simple type: BaseCurrencyAmount
class BaseCurrencyAmount (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BaseCurrencyAmount')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1704, 1)
    _Documentation = None
BaseCurrencyAmount._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(20))
BaseCurrencyAmount._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
BaseCurrencyAmount._InitializeFacetMap(BaseCurrencyAmount._CF_totalDigits,
   BaseCurrencyAmount._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'BaseCurrencyAmount', BaseCurrencyAmount)

# Atomic simple type: BasePositiveCurrencyAmount
class BasePositiveCurrencyAmount (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BasePositiveCurrencyAmount')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1710, 1)
    _Documentation = None
BasePositiveCurrencyAmount._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=BasePositiveCurrencyAmount, value=pyxb.binding.datatypes.decimal('0.0'))
BasePositiveCurrencyAmount._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=BasePositiveCurrencyAmount, value=pyxb.binding.datatypes.decimal('99999999.99'))
BasePositiveCurrencyAmount._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(10))
BasePositiveCurrencyAmount._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
BasePositiveCurrencyAmount._InitializeFacetMap(BasePositiveCurrencyAmount._CF_minInclusive,
   BasePositiveCurrencyAmount._CF_maxInclusive,
   BasePositiveCurrencyAmount._CF_totalDigits,
   BasePositiveCurrencyAmount._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'BasePositiveCurrencyAmount', BasePositiveCurrencyAmount)

# Atomic simple type: BasePriceCurrencyAmount
class BasePriceCurrencyAmount (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BasePriceCurrencyAmount')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1718, 1)
    _Documentation = None
BasePriceCurrencyAmount._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(20))
BasePriceCurrencyAmount._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(4))
BasePriceCurrencyAmount._InitializeFacetMap(BasePriceCurrencyAmount._CF_totalDigits,
   BasePriceCurrencyAmount._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'BasePriceCurrencyAmount', BasePriceCurrencyAmount)

# Atomic simple type: StringBasePriceCurrencyAmount
class StringBasePriceCurrencyAmount (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StringBasePriceCurrencyAmount')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1725, 1)
    _Documentation = None
StringBasePriceCurrencyAmount._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(24))
StringBasePriceCurrencyAmount._InitializeFacetMap(StringBasePriceCurrencyAmount._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'StringBasePriceCurrencyAmount', StringBasePriceCurrencyAmount)

# Atomic simple type: [anonymous]
class STD_ANON_8 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1765, 2)
    _Documentation = None
STD_ANON_8._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_8, enum_prefix=None)
STD_ANON_8.Ship = STD_ANON_8._CF_enumeration.addEnumeration(unicode_value='Ship', tag='Ship')
STD_ANON_8.InStorePickup = STD_ANON_8._CF_enumeration.addEnumeration(unicode_value='InStorePickup', tag='InStorePickup')
STD_ANON_8.MerchantDelivery = STD_ANON_8._CF_enumeration.addEnumeration(unicode_value='MerchantDelivery', tag='MerchantDelivery')
STD_ANON_8._InitializeFacetMap(STD_ANON_8._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_9 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1781, 2)
    _Documentation = None
STD_ANON_9._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_9, enum_prefix=None)
STD_ANON_9.Standard = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='Standard', tag='Standard')
STD_ANON_9.Expedited = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='Expedited', tag='Expedited')
STD_ANON_9.Scheduled = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='Scheduled', tag='Scheduled')
STD_ANON_9.NextDay = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='NextDay', tag='NextDay')
STD_ANON_9.SecondDay = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='SecondDay', tag='SecondDay')
STD_ANON_9.Next = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='Next', tag='Next')
STD_ANON_9.Second = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='Second', tag='Second')
STD_ANON_9.Priority = STD_ANON_9._CF_enumeration.addEnumeration(unicode_value='Priority', tag='Priority')
STD_ANON_9._InitializeFacetMap(STD_ANON_9._CF_enumeration)

# Atomic simple type: FulfillReadiness
class FulfillReadiness (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FulfillReadiness')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1800, 1)
    _Documentation = None
FulfillReadiness._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=FulfillReadiness, enum_prefix=None)
FulfillReadiness.drop_ship_ready = FulfillReadiness._CF_enumeration.addEnumeration(unicode_value='drop_ship_ready', tag='drop_ship_ready')
FulfillReadiness.not_ready = FulfillReadiness._CF_enumeration.addEnumeration(unicode_value='not_ready', tag='not_ready')
FulfillReadiness.receive_ready = FulfillReadiness._CF_enumeration.addEnumeration(unicode_value='receive_ready', tag='receive_ready')
FulfillReadiness.exception_receive_ready = FulfillReadiness._CF_enumeration.addEnumeration(unicode_value='exception_receive_ready', tag='exception_receive_ready')
FulfillReadiness.po_ready = FulfillReadiness._CF_enumeration.addEnumeration(unicode_value='po_ready', tag='po_ready')
FulfillReadiness.unknown = FulfillReadiness._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
FulfillReadiness._InitializeFacetMap(FulfillReadiness._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'FulfillReadiness', FulfillReadiness)

# Atomic simple type: [anonymous]
class STD_ANON_10 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1818, 2)
    _Documentation = None
STD_ANON_10._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_10, enum_prefix=None)
STD_ANON_10.USPS = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='USPS', tag='USPS')
STD_ANON_10.UPS = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='UPS', tag='UPS')
STD_ANON_10.UPSMI = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='UPSMI', tag='UPSMI')
STD_ANON_10.FedEx = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='FedEx', tag='FedEx')
STD_ANON_10.DHL = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='DHL', tag='DHL')
STD_ANON_10.Fastway = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Fastway', tag='Fastway')
STD_ANON_10.GLS = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='GLS', tag='GLS')
STD_ANON_10.GO = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='GO!', tag='GO')
STD_ANON_10.Hermes_Logistik_Gruppe = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Hermes Logistik Gruppe', tag='Hermes_Logistik_Gruppe')
STD_ANON_10.Royal_Mail = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Royal Mail', tag='Royal_Mail')
STD_ANON_10.Parcelforce = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Parcelforce', tag='Parcelforce')
STD_ANON_10.City_Link = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='City Link', tag='City_Link')
STD_ANON_10.TNT = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='TNT', tag='TNT')
STD_ANON_10.Target = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Target', tag='Target')
STD_ANON_10.SagawaExpress = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='SagawaExpress', tag='SagawaExpress')
STD_ANON_10.NipponExpress = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='NipponExpress', tag='NipponExpress')
STD_ANON_10.YamatoTransport = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='YamatoTransport', tag='YamatoTransport')
STD_ANON_10.DHL_Global_Mail = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='DHL Global Mail', tag='DHL_Global_Mail')
STD_ANON_10.UPS_Mail_Innovations = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='UPS Mail Innovations', tag='UPS_Mail_Innovations')
STD_ANON_10.FedEx_SmartPost = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='FedEx SmartPost', tag='FedEx_SmartPost')
STD_ANON_10.OSM = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='OSM', tag='OSM')
STD_ANON_10.OnTrac = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='OnTrac', tag='OnTrac')
STD_ANON_10.Streamlite = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Streamlite', tag='Streamlite')
STD_ANON_10.Newgistics = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Newgistics', tag='Newgistics')
STD_ANON_10.Canada_Post = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Canada Post', tag='Canada_Post')
STD_ANON_10.Blue_Package = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Blue Package', tag='Blue_Package')
STD_ANON_10.Chronopost = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Chronopost', tag='Chronopost')
STD_ANON_10.Deutsche_Post = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Deutsche Post', tag='Deutsche_Post')
STD_ANON_10.DPD = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='DPD', tag='DPD')
STD_ANON_10.La_Poste = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='La Poste', tag='La_Poste')
STD_ANON_10.Parcelnet = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Parcelnet', tag='Parcelnet')
STD_ANON_10.Poste_Italiane = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Poste Italiane', tag='Poste_Italiane')
STD_ANON_10.SDA = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='SDA', tag='SDA')
STD_ANON_10.Smartmail = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Smartmail', tag='Smartmail')
STD_ANON_10.FEDEX_JP = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='FEDEX_JP', tag='FEDEX_JP')
STD_ANON_10.JP_EXPRESS = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='JP_EXPRESS', tag='JP_EXPRESS')
STD_ANON_10.NITTSU = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='NITTSU', tag='NITTSU')
STD_ANON_10.SAGAWA = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='SAGAWA', tag='SAGAWA')
STD_ANON_10.YAMATO = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='YAMATO', tag='YAMATO')
STD_ANON_10.BlueDart = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='BlueDart', tag='BlueDart')
STD_ANON_10.AFLFedex = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='AFL/Fedex', tag='AFLFedex')
STD_ANON_10.Aramex = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Aramex', tag='Aramex')
STD_ANON_10.India_Post = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='India Post', tag='India_Post')
STD_ANON_10.Professional = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Professional', tag='Professional')
STD_ANON_10.DTDC = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='DTDC', tag='DTDC')
STD_ANON_10.Overnite_Express = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Overnite Express', tag='Overnite_Express')
STD_ANON_10.First_Flight = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='First Flight', tag='First_Flight')
STD_ANON_10.Delhivery = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Delhivery', tag='Delhivery')
STD_ANON_10.Lasership = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Lasership', tag='Lasership')
STD_ANON_10.Yodel = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Yodel', tag='Yodel')
STD_ANON_10.Other = STD_ANON_10._CF_enumeration.addEnumeration(unicode_value='Other', tag='Other')
STD_ANON_10._InitializeFacetMap(STD_ANON_10._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_11 (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1882, 2)
    _Documentation = None
STD_ANON_11._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
STD_ANON_11._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(250))
STD_ANON_11._InitializeFacetMap(STD_ANON_11._CF_minLength,
   STD_ANON_11._CF_maxLength)

# Atomic simple type: IDNumber
class IDNumber (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IDNumber')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1904, 1)
    _Documentation = None
IDNumber._CF_pattern = pyxb.binding.facets.CF_pattern()
IDNumber._CF_pattern.addPattern(pattern='\\d{1,20}')
IDNumber._InitializeFacetMap(IDNumber._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'IDNumber', IDNumber)

# Atomic simple type: MessageIDNumber
class MessageIDNumber (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MessageIDNumber')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1909, 1)
    _Documentation = None
MessageIDNumber._CF_pattern = pyxb.binding.facets.CF_pattern()
MessageIDNumber._CF_pattern.addPattern(pattern='\\d{1,18}')
MessageIDNumber._InitializeFacetMap(MessageIDNumber._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'MessageIDNumber', MessageIDNumber)

# Atomic simple type: SevenString
class SevenString (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SevenString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1921, 1)
    _Documentation = None
SevenString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(7))
SevenString._InitializeFacetMap(SevenString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'SevenString', SevenString)

# Atomic simple type: TenString
class TenString (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TenString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1926, 1)
    _Documentation = None
TenString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(10))
TenString._InitializeFacetMap(TenString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TenString', TenString)

# Atomic simple type: TwentyString
class TwentyString (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TwentyString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1931, 1)
    _Documentation = None
TwentyString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(20))
TwentyString._InitializeFacetMap(TwentyString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TwentyString', TwentyString)

# Atomic simple type: ThirtyString
class ThirtyString (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ThirtyString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1936, 1)
    _Documentation = None
ThirtyString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(30))
ThirtyString._InitializeFacetMap(ThirtyString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'ThirtyString', ThirtyString)

# Atomic simple type: FortyString
class FortyString (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FortyString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1941, 1)
    _Documentation = None
FortyString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(40))
FortyString._InitializeFacetMap(FortyString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'FortyString', FortyString)

# Atomic simple type: FiveStringNotNull
class FiveStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FiveStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1946, 1)
    _Documentation = None
FiveStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
FiveStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(5))
FiveStringNotNull._InitializeFacetMap(FiveStringNotNull._CF_minLength,
   FiveStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'FiveStringNotNull', FiveStringNotNull)

# Atomic simple type: TenStringNotNull
class TenStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TenStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1952, 1)
    _Documentation = None
TenStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
TenStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(10))
TenStringNotNull._InitializeFacetMap(TenStringNotNull._CF_minLength,
   TenStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TenStringNotNull', TenStringNotNull)

# Atomic simple type: TwentyStringNotNull
class TwentyStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TwentyStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1958, 1)
    _Documentation = None
TwentyStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
TwentyStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(20))
TwentyStringNotNull._InitializeFacetMap(TwentyStringNotNull._CF_minLength,
   TwentyStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TwentyStringNotNull', TwentyStringNotNull)

# Atomic simple type: ThirtyStringNotNull
class ThirtyStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ThirtyStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1964, 1)
    _Documentation = None
ThirtyStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
ThirtyStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(30))
ThirtyStringNotNull._InitializeFacetMap(ThirtyStringNotNull._CF_minLength,
   ThirtyStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'ThirtyStringNotNull', ThirtyStringNotNull)

# Atomic simple type: FortyStringNotNull
class FortyStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FortyStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1970, 1)
    _Documentation = None
FortyStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
FortyStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(40))
FortyStringNotNull._InitializeFacetMap(FortyStringNotNull._CF_minLength,
   FortyStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'FortyStringNotNull', FortyStringNotNull)

# Atomic simple type: PARTSLINK
class PARTSLINK (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PARTSLINK')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1976, 1)
    _Documentation = None
PARTSLINK._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(9))
PARTSLINK._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(9))
PARTSLINK._CF_pattern = pyxb.binding.facets.CF_pattern()
PARTSLINK._CF_pattern.addPattern(pattern='[a-zA-Z]{2}\\d{7}')
PARTSLINK._InitializeFacetMap(PARTSLINK._CF_minLength,
   PARTSLINK._CF_maxLength,
   PARTSLINK._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'PARTSLINK', PARTSLINK)

# Atomic simple type: String
class String (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'String')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1983, 1)
    _Documentation = None
String._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(50))
String._InitializeFacetMap(String._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'String', String)

# Atomic simple type: StringNotNull
class StringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1988, 1)
    _Documentation = None
StringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
StringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(50))
StringNotNull._InitializeFacetMap(StringNotNull._CF_minLength,
   StringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'StringNotNull', StringNotNull)

# Atomic simple type: HundredString
class HundredString (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HundredString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1994, 1)
    _Documentation = None
HundredString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(100))
HundredString._InitializeFacetMap(HundredString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'HundredString', HundredString)

# Atomic simple type: HundredFiftyStringNotNull
class HundredFiftyStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HundredFiftyStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1999, 1)
    _Documentation = None
HundredFiftyStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
HundredFiftyStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(150))
HundredFiftyStringNotNull._InitializeFacetMap(HundredFiftyStringNotNull._CF_minLength,
   HundredFiftyStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'HundredFiftyStringNotNull', HundredFiftyStringNotNull)

# Atomic simple type: MediumString
class MediumString (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MediumString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2005, 1)
    _Documentation = None
MediumString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(200))
MediumString._InitializeFacetMap(MediumString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'MediumString', MediumString)

# Atomic simple type: MediumStringNotNull
class MediumStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MediumStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2010, 1)
    _Documentation = None
MediumStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
MediumStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(200))
MediumStringNotNull._InitializeFacetMap(MediumStringNotNull._CF_minLength,
   MediumStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'MediumStringNotNull', MediumStringNotNull)

# Atomic simple type: TwoFiftyStringNotNull
class TwoFiftyStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TwoFiftyStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2016, 1)
    _Documentation = None
TwoFiftyStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
TwoFiftyStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(250))
TwoFiftyStringNotNull._InitializeFacetMap(TwoFiftyStringNotNull._CF_minLength,
   TwoFiftyStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TwoFiftyStringNotNull', TwoFiftyStringNotNull)

# Atomic simple type: LongString
class LongString (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LongString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2022, 1)
    _Documentation = None
LongString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(500))
LongString._InitializeFacetMap(LongString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'LongString', LongString)

# Atomic simple type: LongStringNotNull
class LongStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LongStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2027, 1)
    _Documentation = None
LongStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
LongStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(500))
LongStringNotNull._InitializeFacetMap(LongStringNotNull._CF_minLength,
   LongStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'LongStringNotNull', LongStringNotNull)

# Atomic simple type: SuperLongString
class SuperLongString (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SuperLongString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2033, 1)
    _Documentation = None
SuperLongString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(1000))
SuperLongString._InitializeFacetMap(SuperLongString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'SuperLongString', SuperLongString)

# Atomic simple type: SuperLongStringNotNull
class SuperLongStringNotNull (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SuperLongStringNotNull')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2038, 1)
    _Documentation = None
SuperLongStringNotNull._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
SuperLongStringNotNull._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(1000))
SuperLongStringNotNull._InitializeFacetMap(SuperLongStringNotNull._CF_minLength,
   SuperLongStringNotNull._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'SuperLongStringNotNull', SuperLongStringNotNull)

# Atomic simple type: TwoThousandString
class TwoThousandString (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TwoThousandString')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2044, 1)
    _Documentation = None
TwoThousandString._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(2000))
TwoThousandString._InitializeFacetMap(TwoThousandString._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TwoThousandString', TwoThousandString)

# Atomic simple type: [anonymous]
class STD_ANON_12 (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2098, 2)
    _Documentation = None
STD_ANON_12._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(80))
STD_ANON_12._InitializeFacetMap(STD_ANON_12._CF_maxLength)

# Atomic simple type: [anonymous]
class STD_ANON_13 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2112, 2)
    _Documentation = None
STD_ANON_13._CF_pattern = pyxb.binding.facets.CF_pattern()
STD_ANON_13._CF_pattern.addPattern(pattern='\\w{3}-\\w{7}-\\w{7}')
STD_ANON_13._InitializeFacetMap(STD_ANON_13._CF_pattern)

# Atomic simple type: [anonymous]
class STD_ANON_14 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2126, 2)
    _Documentation = None
STD_ANON_14._CF_pattern = pyxb.binding.facets.CF_pattern()
STD_ANON_14._CF_pattern.addPattern(pattern='\\d{14}')
STD_ANON_14._InitializeFacetMap(STD_ANON_14._CF_pattern)

# Atomic simple type: [anonymous]
class STD_ANON_15 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2151, 5)
    _Documentation = None
STD_ANON_15._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_15, enum_prefix=None)
STD_ANON_15.ISBN = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='ISBN', tag='ISBN')
STD_ANON_15.UPC = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='UPC', tag='UPC')
STD_ANON_15.EAN = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='EAN', tag='EAN')
STD_ANON_15.ASIN = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='ASIN', tag='ASIN')
STD_ANON_15.GTIN = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='GTIN', tag='GTIN')
STD_ANON_15.GCID = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='GCID', tag='GCID')
STD_ANON_15.PZN = STD_ANON_15._CF_enumeration.addEnumeration(unicode_value='PZN', tag='PZN')
STD_ANON_15._InitializeFacetMap(STD_ANON_15._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_16 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2164, 5)
    _Documentation = None
STD_ANON_16._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(8))
STD_ANON_16._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(16))
STD_ANON_16._InitializeFacetMap(STD_ANON_16._CF_minLength,
   STD_ANON_16._CF_maxLength)

# Atomic simple type: [anonymous]
class STD_ANON_17 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2185, 5)
    _Documentation = None
STD_ANON_17._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_17, enum_prefix=None)
STD_ANON_17.UPC = STD_ANON_17._CF_enumeration.addEnumeration(unicode_value='UPC', tag='UPC')
STD_ANON_17.EAN = STD_ANON_17._CF_enumeration.addEnumeration(unicode_value='EAN', tag='EAN')
STD_ANON_17.GTIN = STD_ANON_17._CF_enumeration.addEnumeration(unicode_value='GTIN', tag='GTIN')
STD_ANON_17._InitializeFacetMap(STD_ANON_17._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_18 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2194, 5)
    _Documentation = None
STD_ANON_18._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(8))
STD_ANON_18._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(16))
STD_ANON_18._InitializeFacetMap(STD_ANON_18._CF_minLength,
   STD_ANON_18._CF_maxLength)

# Atomic simple type: PromotionApplicationType
class PromotionApplicationType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PromotionApplicationType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2220, 1)
    _Documentation = None
PromotionApplicationType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PromotionApplicationType, enum_prefix=None)
PromotionApplicationType.Principal = PromotionApplicationType._CF_enumeration.addEnumeration(unicode_value='Principal', tag='Principal')
PromotionApplicationType.Shipping = PromotionApplicationType._CF_enumeration.addEnumeration(unicode_value='Shipping', tag='Shipping')
PromotionApplicationType._InitializeFacetMap(PromotionApplicationType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PromotionApplicationType', PromotionApplicationType)

# Atomic simple type: [anonymous]
class STD_ANON_19 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2234, 2)
    _Documentation = None
STD_ANON_19._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(6))
STD_ANON_19._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(12))
STD_ANON_19._InitializeFacetMap(STD_ANON_19._CF_minLength,
   STD_ANON_19._CF_maxLength)

# Atomic simple type: SKUType
class SKUType (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SKUType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2270, 1)
    _Documentation = None
SKUType._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
SKUType._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(40))
SKUType._InitializeFacetMap(SKUType._CF_minLength,
   SKUType._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'SKUType', SKUType)

# Atomic simple type: [anonymous]
class STD_ANON_20 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2294, 2)
    _Documentation = None
STD_ANON_20._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_20, enum_prefix=None)
STD_ANON_20.New = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='New', tag='New')
STD_ANON_20.UsedLikeNew = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='UsedLikeNew', tag='UsedLikeNew')
STD_ANON_20.UsedVeryGood = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='UsedVeryGood', tag='UsedVeryGood')
STD_ANON_20.UsedGood = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='UsedGood', tag='UsedGood')
STD_ANON_20.UsedAcceptable = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='UsedAcceptable', tag='UsedAcceptable')
STD_ANON_20.CollectibleLikeNew = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='CollectibleLikeNew', tag='CollectibleLikeNew')
STD_ANON_20.CollectibleVeryGood = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='CollectibleVeryGood', tag='CollectibleVeryGood')
STD_ANON_20.CollectibleGood = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='CollectibleGood', tag='CollectibleGood')
STD_ANON_20.CollectibleAcceptable = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='CollectibleAcceptable', tag='CollectibleAcceptable')
STD_ANON_20.Refurbished = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='Refurbished', tag='Refurbished')
STD_ANON_20.Club = STD_ANON_20._CF_enumeration.addEnumeration(unicode_value='Club', tag='Club')
STD_ANON_20._InitializeFacetMap(STD_ANON_20._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_21 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2334, 5)
    _Documentation = None
STD_ANON_21._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_21, enum_prefix=None)
STD_ANON_21.windows = STD_ANON_21._CF_enumeration.addEnumeration(unicode_value='windows', tag='windows')
STD_ANON_21.mac = STD_ANON_21._CF_enumeration.addEnumeration(unicode_value='mac', tag='mac')
STD_ANON_21.linux = STD_ANON_21._CF_enumeration.addEnumeration(unicode_value='linux', tag='linux')
STD_ANON_21._InitializeFacetMap(STD_ANON_21._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_22 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2375, 2)
    _Documentation = None
STD_ANON_22._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_22, enum_prefix=None)
STD_ANON_22.beige = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='beige', tag='beige')
STD_ANON_22.black = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='black', tag='black')
STD_ANON_22.blue = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='blue', tag='blue')
STD_ANON_22.brass = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='brass', tag='brass')
STD_ANON_22.bronze = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='bronze', tag='bronze')
STD_ANON_22.brown = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='brown', tag='brown')
STD_ANON_22.burst = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='burst', tag='burst')
STD_ANON_22.chrome = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='chrome', tag='chrome')
STD_ANON_22.clear = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='clear', tag='clear')
STD_ANON_22.gold = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='gold', tag='gold')
STD_ANON_22.gray = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='gray', tag='gray')
STD_ANON_22.green = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='green', tag='green')
STD_ANON_22.metallic = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='metallic', tag='metallic')
STD_ANON_22.multi_colored = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='multi-colored', tag='multi_colored')
STD_ANON_22.natural = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='natural', tag='natural')
STD_ANON_22.off_white = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='off-white', tag='off_white')
STD_ANON_22.orange = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='orange', tag='orange')
STD_ANON_22.pink = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='pink', tag='pink')
STD_ANON_22.purple = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='purple', tag='purple')
STD_ANON_22.red = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='red', tag='red')
STD_ANON_22.silver = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='silver', tag='silver')
STD_ANON_22.white = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='white', tag='white')
STD_ANON_22.yellow = STD_ANON_22._CF_enumeration.addEnumeration(unicode_value='yellow', tag='yellow')
STD_ANON_22._InitializeFacetMap(STD_ANON_22._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_23 (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2411, 2)
    _Documentation = None
STD_ANON_23._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_23, value=pyxb.binding.datatypes.decimal('0.01'))
STD_ANON_23._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_23, value=pyxb.binding.datatypes.decimal('2500.0'))
STD_ANON_23._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(6))
STD_ANON_23._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_23._InitializeFacetMap(STD_ANON_23._CF_minInclusive,
   STD_ANON_23._CF_maxInclusive,
   STD_ANON_23._CF_totalDigits,
   STD_ANON_23._CF_fractionDigits)

# Atomic simple type: PositiveInteger
class PositiveInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PositiveInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2427, 1)
    _Documentation = None
PositiveInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(12))
PositiveInteger._InitializeFacetMap(PositiveInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'PositiveInteger', PositiveInteger)

# Atomic simple type: TwoDigitInteger
class TwoDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TwoDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2432, 1)
    _Documentation = None
TwoDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(2))
TwoDigitInteger._InitializeFacetMap(TwoDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'TwoDigitInteger', TwoDigitInteger)

# Atomic simple type: ThreeDigitInteger
class ThreeDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ThreeDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2437, 1)
    _Documentation = None
ThreeDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(3))
ThreeDigitInteger._InitializeFacetMap(ThreeDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'ThreeDigitInteger', ThreeDigitInteger)

# Atomic simple type: FourDigitMinimumTwoDigitInteger
class FourDigitMinimumTwoDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FourDigitMinimumTwoDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2442, 1)
    _Documentation = None
FourDigitMinimumTwoDigitInteger._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=FourDigitMinimumTwoDigitInteger, value=pyxb.binding.datatypes.positiveInteger(10))
FourDigitMinimumTwoDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(4))
FourDigitMinimumTwoDigitInteger._InitializeFacetMap(FourDigitMinimumTwoDigitInteger._CF_minInclusive,
   FourDigitMinimumTwoDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'FourDigitMinimumTwoDigitInteger', FourDigitMinimumTwoDigitInteger)

# Atomic simple type: FiveDigitInteger
class FiveDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FiveDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2448, 1)
    _Documentation = None
FiveDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(5))
FiveDigitInteger._InitializeFacetMap(FiveDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'FiveDigitInteger', FiveDigitInteger)

# Atomic simple type: SixDigitInteger
class SixDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SixDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2453, 1)
    _Documentation = None
SixDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(6))
SixDigitInteger._InitializeFacetMap(SixDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'SixDigitInteger', SixDigitInteger)

# Atomic simple type: SevenDigitInteger
class SevenDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SevenDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2458, 1)
    _Documentation = None
SevenDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(7))
SevenDigitInteger._InitializeFacetMap(SevenDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'SevenDigitInteger', SevenDigitInteger)

# Atomic simple type: TenDigitInteger
class TenDigitInteger (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TenDigitInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2463, 1)
    _Documentation = None
TenDigitInteger._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(10))
TenDigitInteger._InitializeFacetMap(TenDigitInteger._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'TenDigitInteger', TenDigitInteger)

# Atomic simple type: PercentageType
class PercentageType (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PercentageType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2475, 1)
    _Documentation = None
PercentageType._CF_minExclusive = pyxb.binding.facets.CF_minExclusive(value_datatype=pyxb.binding.datatypes.decimal, value=pyxb.binding.datatypes.anySimpleType('0'))
PercentageType._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=PercentageType, value=pyxb.binding.datatypes.decimal('100.0'))
PercentageType._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
PercentageType._InitializeFacetMap(PercentageType._CF_minExclusive,
   PercentageType._CF_maxInclusive,
   PercentageType._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'PercentageType', PercentageType)

# Atomic simple type: IntegerPercentageType
class IntegerPercentageType (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IntegerPercentageType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2482, 1)
    _Documentation = None
IntegerPercentageType._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=IntegerPercentageType, value=pyxb.binding.datatypes.positiveInteger(1))
IntegerPercentageType._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=IntegerPercentageType, value=pyxb.binding.datatypes.positiveInteger(100))
IntegerPercentageType._InitializeFacetMap(IntegerPercentageType._CF_minInclusive,
   IntegerPercentageType._CF_maxInclusive)
Namespace.addCategoryObject('typeBinding', 'IntegerPercentageType', IntegerPercentageType)

# Atomic simple type: Dimension
class Dimension (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Dimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2495, 1)
    _Documentation = None
Dimension._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(12))
Dimension._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
Dimension._InitializeFacetMap(Dimension._CF_totalDigits,
   Dimension._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'Dimension', Dimension)

# Atomic simple type: PositiveDimension
class PositiveDimension (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PositiveDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2501, 1)
    _Documentation = None
PositiveDimension._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=PositiveDimension, value=pyxb.binding.datatypes.decimal('0.0'))
PositiveDimension._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(12))
PositiveDimension._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
PositiveDimension._InitializeFacetMap(PositiveDimension._CF_minInclusive,
   PositiveDimension._CF_totalDigits,
   PositiveDimension._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'PositiveDimension', PositiveDimension)

# Atomic simple type: PositiveNonZeroDimension
class PositiveNonZeroDimension (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PositiveNonZeroDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2508, 1)
    _Documentation = None
PositiveNonZeroDimension._CF_minExclusive = pyxb.binding.facets.CF_minExclusive(value_datatype=pyxb.binding.datatypes.decimal, value=pyxb.binding.datatypes.anySimpleType('0.00'))
PositiveNonZeroDimension._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(12))
PositiveNonZeroDimension._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
PositiveNonZeroDimension._InitializeFacetMap(PositiveNonZeroDimension._CF_minExclusive,
   PositiveNonZeroDimension._CF_totalDigits,
   PositiveNonZeroDimension._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'PositiveNonZeroDimension', PositiveNonZeroDimension)

# Atomic simple type: FourDecimal
class FourDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FourDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2515, 1)
    _Documentation = None
FourDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(12))
FourDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(4))
FourDecimal._InitializeFacetMap(FourDecimal._CF_totalDigits,
   FourDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'FourDecimal', FourDecimal)

# Atomic simple type: FourPositiveNonZeroDecimal
class FourPositiveNonZeroDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FourPositiveNonZeroDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2521, 1)
    _Documentation = None
FourPositiveNonZeroDecimal._CF_minExclusive = pyxb.binding.facets.CF_minExclusive(value_datatype=pyxb.binding.datatypes.decimal, value=pyxb.binding.datatypes.anySimpleType('0.0000'))
FourPositiveNonZeroDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(12))
FourPositiveNonZeroDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(4))
FourPositiveNonZeroDecimal._InitializeFacetMap(FourPositiveNonZeroDecimal._CF_minExclusive,
   FourPositiveNonZeroDecimal._CF_totalDigits,
   FourPositiveNonZeroDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'FourPositiveNonZeroDecimal', FourPositiveNonZeroDecimal)

# Atomic simple type: SixDigitDecimalFractionOne
class SixDigitDecimalFractionOne (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SixDigitDecimalFractionOne')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2528, 1)
    _Documentation = None
SixDigitDecimalFractionOne._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(6))
SixDigitDecimalFractionOne._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(1))
SixDigitDecimalFractionOne._InitializeFacetMap(SixDigitDecimalFractionOne._CF_totalDigits,
   SixDigitDecimalFractionOne._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'SixDigitDecimalFractionOne', SixDigitDecimalFractionOne)

# Atomic simple type: TwoDigitDecimal
class TwoDigitDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TwoDigitDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2534, 1)
    _Documentation = None
TwoDigitDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(2))
TwoDigitDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(1))
TwoDigitDecimal._InitializeFacetMap(TwoDigitDecimal._CF_totalDigits,
   TwoDigitDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'TwoDigitDecimal', TwoDigitDecimal)

# Atomic simple type: ThreeDigitDecimal
class ThreeDigitDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ThreeDigitDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2540, 1)
    _Documentation = None
ThreeDigitDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(3))
ThreeDigitDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(1))
ThreeDigitDecimal._InitializeFacetMap(ThreeDigitDecimal._CF_totalDigits,
   ThreeDigitDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'ThreeDigitDecimal', ThreeDigitDecimal)

# Atomic simple type: FourDigitDecimal
class FourDigitDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FourDigitDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2546, 1)
    _Documentation = None
FourDigitDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(4))
FourDigitDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
FourDigitDecimal._InitializeFacetMap(FourDigitDecimal._CF_totalDigits,
   FourDigitDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'FourDigitDecimal', FourDigitDecimal)

# Atomic simple type: FiveDigitDecimal
class FiveDigitDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FiveDigitDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2552, 1)
    _Documentation = None
FiveDigitDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(5))
FiveDigitDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
FiveDigitDecimal._InitializeFacetMap(FiveDigitDecimal._CF_totalDigits,
   FiveDigitDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'FiveDigitDecimal', FiveDigitDecimal)

# Atomic simple type: SixDigitDecimal
class SixDigitDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SixDigitDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2558, 1)
    _Documentation = None
SixDigitDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(6))
SixDigitDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
SixDigitDecimal._InitializeFacetMap(SixDigitDecimal._CF_totalDigits,
   SixDigitDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'SixDigitDecimal', SixDigitDecimal)

# Atomic simple type: SevenDigitDecimal
class SevenDigitDecimal (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SevenDigitDecimal')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2564, 1)
    _Documentation = None
SevenDigitDecimal._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(7))
SevenDigitDecimal._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
SevenDigitDecimal._InitializeFacetMap(SevenDigitDecimal._CF_totalDigits,
   SevenDigitDecimal._CF_fractionDigits)
Namespace.addCategoryObject('typeBinding', 'SevenDigitDecimal', SevenDigitDecimal)

# Atomic simple type: FourDigitYear
class FourDigitYear (pyxb.binding.datatypes.positiveInteger):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FourDigitYear')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2577, 1)
    _Documentation = None
FourDigitYear._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(4))
FourDigitYear._InitializeFacetMap(FourDigitYear._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'FourDigitYear', FourDigitYear)

# Atomic simple type: FourDigitYearPlusNV
class FourDigitYearPlusNV (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FourDigitYearPlusNV')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2582, 1)
    _Documentation = None
FourDigitYearPlusNV._CF_pattern = pyxb.binding.facets.CF_pattern()
FourDigitYearPlusNV._CF_pattern.addPattern(pattern='[0-9]{4}|NV|nv')
FourDigitYearPlusNV._InitializeFacetMap(FourDigitYearPlusNV._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'FourDigitYearPlusNV', FourDigitYearPlusNV)

# Atomic simple type: [anonymous]
class STD_ANON_24 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2593, 2)
    _Documentation = None
STD_ANON_24._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_24, enum_prefix=None)
STD_ANON_24.in_store = STD_ANON_24._CF_enumeration.addEnumeration(unicode_value='in_store', tag='in_store')
STD_ANON_24.direct_ship = STD_ANON_24._CF_enumeration.addEnumeration(unicode_value='direct_ship', tag='direct_ship')
STD_ANON_24._InitializeFacetMap(STD_ANON_24._CF_enumeration)

# Atomic simple type: AreaUnitOfMeasure
class AreaUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AreaUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3063, 1)
    _Documentation = None
AreaUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AreaUnitOfMeasure, enum_prefix=None)
AreaUnitOfMeasure.square_in = AreaUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='square-in', tag='square_in')
AreaUnitOfMeasure.square_ft = AreaUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='square-ft', tag='square_ft')
AreaUnitOfMeasure.square_cm = AreaUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='square-cm', tag='square_cm')
AreaUnitOfMeasure.square_m = AreaUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='square-m', tag='square_m')
AreaUnitOfMeasure._InitializeFacetMap(AreaUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AreaUnitOfMeasure', AreaUnitOfMeasure)

# Atomic simple type: AirFlowDisplacementUnitOfMeasure
class AirFlowDisplacementUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AirFlowDisplacementUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3071, 1)
    _Documentation = None
AirFlowDisplacementUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AirFlowDisplacementUnitOfMeasure, enum_prefix=None)
AirFlowDisplacementUnitOfMeasure.cubic_feet_per_minute = AirFlowDisplacementUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_feet_per_minute', tag='cubic_feet_per_minute')
AirFlowDisplacementUnitOfMeasure.cubic_feet_per_hour = AirFlowDisplacementUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_feet_per_hour', tag='cubic_feet_per_hour')
AirFlowDisplacementUnitOfMeasure._InitializeFacetMap(AirFlowDisplacementUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AirFlowDisplacementUnitOfMeasure', AirFlowDisplacementUnitOfMeasure)

# Atomic simple type: BurnTimeUnitOfMeasure
class BurnTimeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BurnTimeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3077, 1)
    _Documentation = None
BurnTimeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BurnTimeUnitOfMeasure, enum_prefix=None)
BurnTimeUnitOfMeasure.minutes = BurnTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='minutes', tag='minutes')
BurnTimeUnitOfMeasure.hours = BurnTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='hours', tag='hours')
BurnTimeUnitOfMeasure.cycles = BurnTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cycles', tag='cycles')
BurnTimeUnitOfMeasure._InitializeFacetMap(BurnTimeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BurnTimeUnitOfMeasure', BurnTimeUnitOfMeasure)

# Atomic simple type: LengthUnitOfMeasure
class LengthUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LengthUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3084, 1)
    _Documentation = None
LengthUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LengthUnitOfMeasure, enum_prefix=None)
LengthUnitOfMeasure.MM = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MM', tag='MM')
LengthUnitOfMeasure.CM = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='CM', tag='CM')
LengthUnitOfMeasure.M = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='M', tag='M')
LengthUnitOfMeasure.IN = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='IN', tag='IN')
LengthUnitOfMeasure.FT = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='FT', tag='FT')
LengthUnitOfMeasure.inches = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='inches', tag='inches')
LengthUnitOfMeasure.feet = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='feet', tag='feet')
LengthUnitOfMeasure.meters = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='meters', tag='meters')
LengthUnitOfMeasure.decimeters = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='decimeters', tag='decimeters')
LengthUnitOfMeasure.centimeters = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='centimeters', tag='centimeters')
LengthUnitOfMeasure.millimeters = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='millimeters', tag='millimeters')
LengthUnitOfMeasure.micrometers = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='micrometers', tag='micrometers')
LengthUnitOfMeasure.nanometers = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanometers', tag='nanometers')
LengthUnitOfMeasure.picometers = LengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='picometers', tag='picometers')
LengthUnitOfMeasure._InitializeFacetMap(LengthUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LengthUnitOfMeasure', LengthUnitOfMeasure)

# Atomic simple type: LuminanceUnitOfMeasure
class LuminanceUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LuminanceUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3102, 1)
    _Documentation = None
LuminanceUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LuminanceUnitOfMeasure, enum_prefix=None)
LuminanceUnitOfMeasure.lumens = LuminanceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='lumens', tag='lumens')
LuminanceUnitOfMeasure._InitializeFacetMap(LuminanceUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LuminanceUnitOfMeasure', LuminanceUnitOfMeasure)

# Atomic simple type: LuminousIntensityUnitOfMeasure
class LuminousIntensityUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LuminousIntensityUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3107, 1)
    _Documentation = None
LuminousIntensityUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LuminousIntensityUnitOfMeasure, enum_prefix=None)
LuminousIntensityUnitOfMeasure.candela = LuminousIntensityUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='candela', tag='candela')
LuminousIntensityUnitOfMeasure._InitializeFacetMap(LuminousIntensityUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LuminousIntensityUnitOfMeasure', LuminousIntensityUnitOfMeasure)

# Atomic simple type: VolumeUnitOfMeasure
class VolumeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3112, 1)
    _Documentation = None
VolumeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VolumeUnitOfMeasure, enum_prefix=None)
VolumeUnitOfMeasure.cubic_cm = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic-cm', tag='cubic_cm')
VolumeUnitOfMeasure.cubic_ft = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic-ft', tag='cubic_ft')
VolumeUnitOfMeasure.cubic_in = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic-in', tag='cubic_in')
VolumeUnitOfMeasure.cubic_m = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic-m', tag='cubic_m')
VolumeUnitOfMeasure.cubic_yd = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cubic-yd', tag='cubic_yd')
VolumeUnitOfMeasure.cup = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='cup', tag='cup')
VolumeUnitOfMeasure.fluid_oz = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='fluid-oz', tag='fluid_oz')
VolumeUnitOfMeasure.gallon = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='gallon', tag='gallon')
VolumeUnitOfMeasure.liter = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='liter', tag='liter')
VolumeUnitOfMeasure.milliliter = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliliter', tag='milliliter')
VolumeUnitOfMeasure.ounce = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='ounce', tag='ounce')
VolumeUnitOfMeasure.pint = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='pint', tag='pint')
VolumeUnitOfMeasure.quart = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='quart', tag='quart')
VolumeUnitOfMeasure.liters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='liters', tag='liters')
VolumeUnitOfMeasure.deciliters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='deciliters', tag='deciliters')
VolumeUnitOfMeasure.centiliters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='centiliters', tag='centiliters')
VolumeUnitOfMeasure.milliliters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliliters', tag='milliliters')
VolumeUnitOfMeasure.microliters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microliters', tag='microliters')
VolumeUnitOfMeasure.nanoliters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanoliters', tag='nanoliters')
VolumeUnitOfMeasure.picoliters = VolumeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='picoliters', tag='picoliters')
VolumeUnitOfMeasure._InitializeFacetMap(VolumeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VolumeUnitOfMeasure', VolumeUnitOfMeasure)

# Atomic simple type: WeightUnitOfMeasure
class WeightUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WeightUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3136, 1)
    _Documentation = None
WeightUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=WeightUnitOfMeasure, enum_prefix=None)
WeightUnitOfMeasure.GR = WeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='GR', tag='GR')
WeightUnitOfMeasure.KG = WeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='KG', tag='KG')
WeightUnitOfMeasure.OZ = WeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='OZ', tag='OZ')
WeightUnitOfMeasure.LB = WeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='LB', tag='LB')
WeightUnitOfMeasure.MG = WeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MG', tag='MG')
WeightUnitOfMeasure._InitializeFacetMap(WeightUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'WeightUnitOfMeasure', WeightUnitOfMeasure)

# Atomic simple type: JewelryLengthUnitOfMeasure
class JewelryLengthUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'JewelryLengthUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3145, 1)
    _Documentation = None
JewelryLengthUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=JewelryLengthUnitOfMeasure, enum_prefix=None)
JewelryLengthUnitOfMeasure.MM = JewelryLengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MM', tag='MM')
JewelryLengthUnitOfMeasure.CM = JewelryLengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='CM', tag='CM')
JewelryLengthUnitOfMeasure.IN = JewelryLengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='IN', tag='IN')
JewelryLengthUnitOfMeasure._InitializeFacetMap(JewelryLengthUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'JewelryLengthUnitOfMeasure', JewelryLengthUnitOfMeasure)

# Atomic simple type: JewelryWeightUnitOfMeasure
class JewelryWeightUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'JewelryWeightUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3152, 1)
    _Documentation = None
JewelryWeightUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=JewelryWeightUnitOfMeasure, enum_prefix=None)
JewelryWeightUnitOfMeasure.GR = JewelryWeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='GR', tag='GR')
JewelryWeightUnitOfMeasure.KG = JewelryWeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='KG', tag='KG')
JewelryWeightUnitOfMeasure.OZ = JewelryWeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='OZ', tag='OZ')
JewelryWeightUnitOfMeasure.LB = JewelryWeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='LB', tag='LB')
JewelryWeightUnitOfMeasure.CARATS = JewelryWeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='CARATS', tag='CARATS')
JewelryWeightUnitOfMeasure.DWT = JewelryWeightUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='DWT', tag='DWT')
JewelryWeightUnitOfMeasure._InitializeFacetMap(JewelryWeightUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'JewelryWeightUnitOfMeasure', JewelryWeightUnitOfMeasure)

# Atomic simple type: DegreeUnitOfMeasure
class DegreeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DegreeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3162, 1)
    _Documentation = None
DegreeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DegreeUnitOfMeasure, enum_prefix=None)
DegreeUnitOfMeasure.degrees = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='degrees', tag='degrees')
DegreeUnitOfMeasure.microradian = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microradian', tag='microradian')
DegreeUnitOfMeasure.arc_minute = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='arc_minute', tag='arc_minute')
DegreeUnitOfMeasure.arc_sec = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='arc_sec', tag='arc_sec')
DegreeUnitOfMeasure.milliradian = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliradian', tag='milliradian')
DegreeUnitOfMeasure.radians = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='radians', tag='radians')
DegreeUnitOfMeasure.turns = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='turns', tag='turns')
DegreeUnitOfMeasure.revolutions = DegreeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='revolutions', tag='revolutions')
DegreeUnitOfMeasure._InitializeFacetMap(DegreeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DegreeUnitOfMeasure', DegreeUnitOfMeasure)

# Atomic simple type: MemorySizeUnitOfMeasure
class MemorySizeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MemorySizeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3174, 1)
    _Documentation = None
MemorySizeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=MemorySizeUnitOfMeasure, enum_prefix=None)
MemorySizeUnitOfMeasure.TB = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='TB', tag='TB')
MemorySizeUnitOfMeasure.GB = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='GB', tag='GB')
MemorySizeUnitOfMeasure.MB = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MB', tag='MB')
MemorySizeUnitOfMeasure.KB = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='KB', tag='KB')
MemorySizeUnitOfMeasure.KO = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='KO', tag='KO')
MemorySizeUnitOfMeasure.MO = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MO', tag='MO')
MemorySizeUnitOfMeasure.GO = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='GO', tag='GO')
MemorySizeUnitOfMeasure.TO = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='TO', tag='TO')
MemorySizeUnitOfMeasure.bytes = MemorySizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='bytes', tag='bytes')
MemorySizeUnitOfMeasure._InitializeFacetMap(MemorySizeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'MemorySizeUnitOfMeasure', MemorySizeUnitOfMeasure)

# Atomic simple type: FrequencyUnitOfMeasure
class FrequencyUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FrequencyUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3187, 1)
    _Documentation = None
FrequencyUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=FrequencyUnitOfMeasure, enum_prefix=None)
FrequencyUnitOfMeasure.Hz = FrequencyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='Hz', tag='Hz')
FrequencyUnitOfMeasure.KHz = FrequencyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='KHz', tag='KHz')
FrequencyUnitOfMeasure.MHz = FrequencyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MHz', tag='MHz')
FrequencyUnitOfMeasure.GHz = FrequencyUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='GHz', tag='GHz')
FrequencyUnitOfMeasure._InitializeFacetMap(FrequencyUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'FrequencyUnitOfMeasure', FrequencyUnitOfMeasure)

# Atomic simple type: AmperageUnitOfMeasure
class AmperageUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AmperageUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3195, 1)
    _Documentation = None
AmperageUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AmperageUnitOfMeasure, enum_prefix=None)
AmperageUnitOfMeasure.amps = AmperageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='amps', tag='amps')
AmperageUnitOfMeasure.kiloamps = AmperageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kiloamps', tag='kiloamps')
AmperageUnitOfMeasure.microamps = AmperageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microamps', tag='microamps')
AmperageUnitOfMeasure.milliamps = AmperageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliamps', tag='milliamps')
AmperageUnitOfMeasure.nanoamps = AmperageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanoamps', tag='nanoamps')
AmperageUnitOfMeasure.picoamps = AmperageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='picoamps', tag='picoamps')
AmperageUnitOfMeasure._InitializeFacetMap(AmperageUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AmperageUnitOfMeasure', AmperageUnitOfMeasure)

# Atomic simple type: TimeUnitOfMeasure
class TimeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TimeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3211, 1)
    _Documentation = None
TimeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TimeUnitOfMeasure, enum_prefix=None)
TimeUnitOfMeasure.sec = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='sec', tag='sec')
TimeUnitOfMeasure.min = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='min', tag='min')
TimeUnitOfMeasure.hr = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='hr', tag='hr')
TimeUnitOfMeasure.days = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='days', tag='days')
TimeUnitOfMeasure.hours = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='hours', tag='hours')
TimeUnitOfMeasure.minutes = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='minutes', tag='minutes')
TimeUnitOfMeasure.seconds = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='seconds', tag='seconds')
TimeUnitOfMeasure.milliseconds = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliseconds', tag='milliseconds')
TimeUnitOfMeasure.microseconds = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microseconds', tag='microseconds')
TimeUnitOfMeasure.nanoseconds = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanoseconds', tag='nanoseconds')
TimeUnitOfMeasure.picoseconds = TimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='picoseconds', tag='picoseconds')
TimeUnitOfMeasure._InitializeFacetMap(TimeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TimeUnitOfMeasure', TimeUnitOfMeasure)

# Atomic simple type: BatteryAverageLifeUnitOfMeasure
class BatteryAverageLifeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BatteryAverageLifeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3226, 1)
    _Documentation = None
BatteryAverageLifeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BatteryAverageLifeUnitOfMeasure, enum_prefix=None)
BatteryAverageLifeUnitOfMeasure.minutes = BatteryAverageLifeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='minutes', tag='minutes')
BatteryAverageLifeUnitOfMeasure.hours = BatteryAverageLifeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='hours', tag='hours')
BatteryAverageLifeUnitOfMeasure.days = BatteryAverageLifeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='days', tag='days')
BatteryAverageLifeUnitOfMeasure.weeks = BatteryAverageLifeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='weeks', tag='weeks')
BatteryAverageLifeUnitOfMeasure.months = BatteryAverageLifeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='months', tag='months')
BatteryAverageLifeUnitOfMeasure.years = BatteryAverageLifeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='years', tag='years')
BatteryAverageLifeUnitOfMeasure._InitializeFacetMap(BatteryAverageLifeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BatteryAverageLifeUnitOfMeasure', BatteryAverageLifeUnitOfMeasure)

# Atomic simple type: DataTransferUnitOfMeasure
class DataTransferUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataTransferUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3236, 1)
    _Documentation = None
DataTransferUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DataTransferUnitOfMeasure, enum_prefix=None)
DataTransferUnitOfMeasure.KHz = DataTransferUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='KHz', tag='KHz')
DataTransferUnitOfMeasure.MHz = DataTransferUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MHz', tag='MHz')
DataTransferUnitOfMeasure.GHz = DataTransferUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='GHz', tag='GHz')
DataTransferUnitOfMeasure.Mbps = DataTransferUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='Mbps', tag='Mbps')
DataTransferUnitOfMeasure.Gbps = DataTransferUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='Gbps', tag='Gbps')
DataTransferUnitOfMeasure._InitializeFacetMap(DataTransferUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DataTransferUnitOfMeasure', DataTransferUnitOfMeasure)

# Atomic simple type: ResistanceUnitOfMeasure
class ResistanceUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ResistanceUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3245, 1)
    _Documentation = None
ResistanceUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ResistanceUnitOfMeasure, enum_prefix=None)
ResistanceUnitOfMeasure.ohms = ResistanceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='ohms', tag='ohms')
ResistanceUnitOfMeasure._InitializeFacetMap(ResistanceUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ResistanceUnitOfMeasure', ResistanceUnitOfMeasure)

# Atomic simple type: DateUnitOfMeasure
class DateUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DateUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3250, 1)
    _Documentation = None
DateUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DateUnitOfMeasure, enum_prefix=None)
DateUnitOfMeasure.days = DateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='days', tag='days')
DateUnitOfMeasure.weeks = DateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='weeks', tag='weeks')
DateUnitOfMeasure.months = DateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='months', tag='months')
DateUnitOfMeasure.years = DateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='years', tag='years')
DateUnitOfMeasure._InitializeFacetMap(DateUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DateUnitOfMeasure', DateUnitOfMeasure)

# Atomic simple type: AssemblyTimeUnitOfMeasure
class AssemblyTimeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AssemblyTimeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3258, 1)
    _Documentation = None
AssemblyTimeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AssemblyTimeUnitOfMeasure, enum_prefix=None)
AssemblyTimeUnitOfMeasure.minutes = AssemblyTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='minutes', tag='minutes')
AssemblyTimeUnitOfMeasure.hours = AssemblyTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='hours', tag='hours')
AssemblyTimeUnitOfMeasure.days = AssemblyTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='days', tag='days')
AssemblyTimeUnitOfMeasure.weeks = AssemblyTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='weeks', tag='weeks')
AssemblyTimeUnitOfMeasure.months = AssemblyTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='months', tag='months')
AssemblyTimeUnitOfMeasure.years = AssemblyTimeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='years', tag='years')
AssemblyTimeUnitOfMeasure._InitializeFacetMap(AssemblyTimeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AssemblyTimeUnitOfMeasure', AssemblyTimeUnitOfMeasure)

# Atomic simple type: AgeRecommendedUnitOfMeasure
class AgeRecommendedUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AgeRecommendedUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3268, 1)
    _Documentation = None
AgeRecommendedUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AgeRecommendedUnitOfMeasure, enum_prefix=None)
AgeRecommendedUnitOfMeasure.months = AgeRecommendedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='months', tag='months')
AgeRecommendedUnitOfMeasure.years = AgeRecommendedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='years', tag='years')
AgeRecommendedUnitOfMeasure._InitializeFacetMap(AgeRecommendedUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AgeRecommendedUnitOfMeasure', AgeRecommendedUnitOfMeasure)

# Atomic simple type: BatteryPowerUnitOfMeasure
class BatteryPowerUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BatteryPowerUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3274, 1)
    _Documentation = None
BatteryPowerUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BatteryPowerUnitOfMeasure, enum_prefix=None)
BatteryPowerUnitOfMeasure.milliamp_hours = BatteryPowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliamp_hours', tag='milliamp_hours')
BatteryPowerUnitOfMeasure.amp_hours = BatteryPowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='amp_hours', tag='amp_hours')
BatteryPowerUnitOfMeasure.volt_amperes = BatteryPowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='volt_amperes', tag='volt_amperes')
BatteryPowerUnitOfMeasure.watt_hours = BatteryPowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='watt_hours', tag='watt_hours')
BatteryPowerUnitOfMeasure._InitializeFacetMap(BatteryPowerUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BatteryPowerUnitOfMeasure', BatteryPowerUnitOfMeasure)

# Atomic simple type: VoltageUnitOfMeasure
class VoltageUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VoltageUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3282, 1)
    _Documentation = None
VoltageUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VoltageUnitOfMeasure, enum_prefix=None)
VoltageUnitOfMeasure.volts = VoltageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='volts', tag='volts')
VoltageUnitOfMeasure.millivolts = VoltageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='millivolts', tag='millivolts')
VoltageUnitOfMeasure.microvolts = VoltageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microvolts', tag='microvolts')
VoltageUnitOfMeasure.nanovolts = VoltageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanovolts', tag='nanovolts')
VoltageUnitOfMeasure.kilovolts = VoltageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilovolts', tag='kilovolts')
VoltageUnitOfMeasure._InitializeFacetMap(VoltageUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VoltageUnitOfMeasure', VoltageUnitOfMeasure)

# Atomic simple type: WattageUnitOfMeasure
class WattageUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WattageUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3291, 1)
    _Documentation = None
WattageUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=WattageUnitOfMeasure, enum_prefix=None)
WattageUnitOfMeasure.watts = WattageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='watts', tag='watts')
WattageUnitOfMeasure.kilowatts = WattageUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilowatts', tag='kilowatts')
WattageUnitOfMeasure._InitializeFacetMap(WattageUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'WattageUnitOfMeasure', WattageUnitOfMeasure)

# Atomic simple type: MillimeterUnitOfMeasure
class MillimeterUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MillimeterUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3297, 1)
    _Documentation = None
MillimeterUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=MillimeterUnitOfMeasure, enum_prefix=None)
MillimeterUnitOfMeasure.millimeters = MillimeterUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='millimeters', tag='millimeters')
MillimeterUnitOfMeasure._InitializeFacetMap(MillimeterUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'MillimeterUnitOfMeasure', MillimeterUnitOfMeasure)

# Atomic simple type: TemperatureRatingUnitOfMeasure
class TemperatureRatingUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TemperatureRatingUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3302, 1)
    _Documentation = None
TemperatureRatingUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TemperatureRatingUnitOfMeasure, enum_prefix=None)
TemperatureRatingUnitOfMeasure.degrees_celsius = TemperatureRatingUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='degrees-celsius', tag='degrees_celsius')
TemperatureRatingUnitOfMeasure.degrees_fahrenheit = TemperatureRatingUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='degrees-fahrenheit', tag='degrees_fahrenheit')
TemperatureRatingUnitOfMeasure.kelvin = TemperatureRatingUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kelvin', tag='kelvin')
TemperatureRatingUnitOfMeasure._InitializeFacetMap(TemperatureRatingUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TemperatureRatingUnitOfMeasure', TemperatureRatingUnitOfMeasure)

# Atomic simple type: ClothingSizeUnitOfMeasure
class ClothingSizeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ClothingSizeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3309, 1)
    _Documentation = None
ClothingSizeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ClothingSizeUnitOfMeasure, enum_prefix=None)
ClothingSizeUnitOfMeasure.IN = ClothingSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='IN', tag='IN')
ClothingSizeUnitOfMeasure.CM = ClothingSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='CM', tag='CM')
ClothingSizeUnitOfMeasure._InitializeFacetMap(ClothingSizeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ClothingSizeUnitOfMeasure', ClothingSizeUnitOfMeasure)

# Atomic simple type: PowerUnitOfMeasure
class PowerUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PowerUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3315, 1)
    _Documentation = None
PowerUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PowerUnitOfMeasure, enum_prefix=None)
PowerUnitOfMeasure.watts = PowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='watts', tag='watts')
PowerUnitOfMeasure.kilowatts = PowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilowatts', tag='kilowatts')
PowerUnitOfMeasure.horsepower = PowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='horsepower', tag='horsepower')
PowerUnitOfMeasure.watts_per_sec = PowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='watts-per-sec', tag='watts_per_sec')
PowerUnitOfMeasure._InitializeFacetMap(PowerUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PowerUnitOfMeasure', PowerUnitOfMeasure)

# Atomic simple type: ResolutionUnitOfMeasure
class ResolutionUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ResolutionUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3323, 1)
    _Documentation = None
ResolutionUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ResolutionUnitOfMeasure, enum_prefix=None)
ResolutionUnitOfMeasure.megapixels = ResolutionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='megapixels', tag='megapixels')
ResolutionUnitOfMeasure.pixels = ResolutionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='pixels', tag='pixels')
ResolutionUnitOfMeasure.lines_per_inch = ResolutionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='lines_per_inch', tag='lines_per_inch')
ResolutionUnitOfMeasure._InitializeFacetMap(ResolutionUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ResolutionUnitOfMeasure', ResolutionUnitOfMeasure)

# Atomic simple type: ApertureUnitOfMeasure
class ApertureUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ApertureUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3330, 1)
    _Documentation = None
ApertureUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ApertureUnitOfMeasure, enum_prefix=None)
ApertureUnitOfMeasure.f = ApertureUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='f', tag='f')
ApertureUnitOfMeasure._InitializeFacetMap(ApertureUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ApertureUnitOfMeasure', ApertureUnitOfMeasure)

# Atomic simple type: ContinuousShootingUnitOfMeasure
class ContinuousShootingUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ContinuousShootingUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3335, 1)
    _Documentation = None
ContinuousShootingUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ContinuousShootingUnitOfMeasure, enum_prefix=None)
ContinuousShootingUnitOfMeasure.frames = ContinuousShootingUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='frames', tag='frames')
ContinuousShootingUnitOfMeasure._InitializeFacetMap(ContinuousShootingUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ContinuousShootingUnitOfMeasure', ContinuousShootingUnitOfMeasure)

# Atomic simple type: CurrentUnitOfMeasure
class CurrentUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CurrentUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3348, 1)
    _Documentation = None
CurrentUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=CurrentUnitOfMeasure, enum_prefix=None)
CurrentUnitOfMeasure.mA = CurrentUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='mA', tag='mA')
CurrentUnitOfMeasure.A = CurrentUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='A', tag='A')
CurrentUnitOfMeasure._InitializeFacetMap(CurrentUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'CurrentUnitOfMeasure', CurrentUnitOfMeasure)

# Atomic simple type: ForceUnitOfMeasure
class ForceUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ForceUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3354, 1)
    _Documentation = None
ForceUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ForceUnitOfMeasure, enum_prefix=None)
ForceUnitOfMeasure.newtons = ForceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='newtons', tag='newtons')
ForceUnitOfMeasure.Newton = ForceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='Newton', tag='Newton')
ForceUnitOfMeasure.pounds = ForceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='pounds', tag='pounds')
ForceUnitOfMeasure.kilograms = ForceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilograms', tag='kilograms')
ForceUnitOfMeasure.PSI = ForceUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='PSI', tag='PSI')
ForceUnitOfMeasure._InitializeFacetMap(ForceUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ForceUnitOfMeasure', ForceUnitOfMeasure)

# Atomic simple type: HardnessUnitOfMeasure
class HardnessUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HardnessUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3363, 1)
    _Documentation = None
HardnessUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=HardnessUnitOfMeasure, enum_prefix=None)
HardnessUnitOfMeasure.brinnell = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='brinnell', tag='brinnell')
HardnessUnitOfMeasure.vickers = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='vickers', tag='vickers')
HardnessUnitOfMeasure.rockwell_a = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='rockwell_a', tag='rockwell_a')
HardnessUnitOfMeasure.rockwell_b = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='rockwell_b', tag='rockwell_b')
HardnessUnitOfMeasure.rockwell_c = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='rockwell_c', tag='rockwell_c')
HardnessUnitOfMeasure.rockwell_d = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='rockwell_d', tag='rockwell_d')
HardnessUnitOfMeasure.shore_a = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_a', tag='shore_a')
HardnessUnitOfMeasure.shore_b = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_b', tag='shore_b')
HardnessUnitOfMeasure.shore_c = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_c', tag='shore_c')
HardnessUnitOfMeasure.shore_d = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_d', tag='shore_d')
HardnessUnitOfMeasure.shore_do = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_do', tag='shore_do')
HardnessUnitOfMeasure.shore_e = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_e', tag='shore_e')
HardnessUnitOfMeasure.shore_m = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_m', tag='shore_m')
HardnessUnitOfMeasure.shore_o = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_o', tag='shore_o')
HardnessUnitOfMeasure.shore_oo = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_oo', tag='shore_oo')
HardnessUnitOfMeasure.shore_ooo = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_ooo', tag='shore_ooo')
HardnessUnitOfMeasure.shore_ooo_s = HardnessUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='shore_ooo_s', tag='shore_ooo_s')
HardnessUnitOfMeasure._InitializeFacetMap(HardnessUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'HardnessUnitOfMeasure', HardnessUnitOfMeasure)

# Atomic simple type: NoiseLevelUnitOfMeasure
class NoiseLevelUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NoiseLevelUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3384, 1)
    _Documentation = None
NoiseLevelUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=NoiseLevelUnitOfMeasure, enum_prefix=None)
NoiseLevelUnitOfMeasure.dBA = NoiseLevelUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='dBA', tag='dBA')
NoiseLevelUnitOfMeasure.decibels = NoiseLevelUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='decibels', tag='decibels')
NoiseLevelUnitOfMeasure._InitializeFacetMap(NoiseLevelUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'NoiseLevelUnitOfMeasure', NoiseLevelUnitOfMeasure)

# Atomic simple type: SunProtectionUnitOfMeasure
class SunProtectionUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SunProtectionUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3390, 1)
    _Documentation = None
SunProtectionUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=SunProtectionUnitOfMeasure, enum_prefix=None)
SunProtectionUnitOfMeasure.sun_protection_factor = SunProtectionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='sun_protection_factor', tag='sun_protection_factor')
SunProtectionUnitOfMeasure._InitializeFacetMap(SunProtectionUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'SunProtectionUnitOfMeasure', SunProtectionUnitOfMeasure)

# Atomic simple type: TemperatureUnitOfMeasure
class TemperatureUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TemperatureUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3395, 1)
    _Documentation = None
TemperatureUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TemperatureUnitOfMeasure, enum_prefix=None)
TemperatureUnitOfMeasure.C = TemperatureUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='C', tag='C')
TemperatureUnitOfMeasure.F = TemperatureUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='F', tag='F')
TemperatureUnitOfMeasure._InitializeFacetMap(TemperatureUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TemperatureUnitOfMeasure', TemperatureUnitOfMeasure)

# Atomic simple type: VolumeRateUnitOfMeasure
class VolumeRateUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeRateUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3423, 1)
    _Documentation = None
VolumeRateUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VolumeRateUnitOfMeasure, enum_prefix=None)
VolumeRateUnitOfMeasure.milliliters_per_second = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliliters per second', tag='milliliters_per_second')
VolumeRateUnitOfMeasure.centiliters_per_second = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='centiliters per second', tag='centiliters_per_second')
VolumeRateUnitOfMeasure.liters_per_second = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='liters per second', tag='liters_per_second')
VolumeRateUnitOfMeasure.milliliters_per_minute = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='milliliters per minute', tag='milliliters_per_minute')
VolumeRateUnitOfMeasure.liters_per_minute = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='liters per minute', tag='liters_per_minute')
VolumeRateUnitOfMeasure.microliters_per_second = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microliters per second', tag='microliters_per_second')
VolumeRateUnitOfMeasure.nanoliters_per_second = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanoliters per second', tag='nanoliters_per_second')
VolumeRateUnitOfMeasure.picoliters_per_second = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='picoliters per second', tag='picoliters_per_second')
VolumeRateUnitOfMeasure.microliters_per_minute = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='microliters per minute', tag='microliters_per_minute')
VolumeRateUnitOfMeasure.nanoliters_per_minute = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='nanoliters per minute', tag='nanoliters_per_minute')
VolumeRateUnitOfMeasure.picoliters_per_minute = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='picoliters per minute', tag='picoliters_per_minute')
VolumeRateUnitOfMeasure.gallons_per_day = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='gallons_per_day', tag='gallons_per_day')
VolumeRateUnitOfMeasure.liters_per_day = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='liters_per_day', tag='liters_per_day')
VolumeRateUnitOfMeasure.liters = VolumeRateUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='liters', tag='liters')
VolumeRateUnitOfMeasure._InitializeFacetMap(VolumeRateUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VolumeRateUnitOfMeasure', VolumeRateUnitOfMeasure)

# Atomic simple type: [anonymous]
class STD_ANON_25 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3455, 4)
    _Documentation = None
STD_ANON_25._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(10))
STD_ANON_25._InitializeFacetMap(STD_ANON_25._CF_maxLength)

# Atomic simple type: LanguageStringType
class LanguageStringType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LanguageStringType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3491, 1)
    _Documentation = None
LanguageStringType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LanguageStringType, enum_prefix=None)
LanguageStringType.Abkhazian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Abkhazian', tag='Abkhazian')
LanguageStringType.Adygei = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Adygei', tag='Adygei')
LanguageStringType.Afar = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Afar', tag='Afar')
LanguageStringType.Afrikaans = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Afrikaans', tag='Afrikaans')
LanguageStringType.Albanian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Albanian', tag='Albanian')
LanguageStringType.Alsatian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Alsatian', tag='Alsatian')
LanguageStringType.Amharic = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Amharic', tag='Amharic')
LanguageStringType.Arabic = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Arabic', tag='Arabic')
LanguageStringType.Aramaic = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Aramaic', tag='Aramaic')
LanguageStringType.Armenian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Armenian', tag='Armenian')
LanguageStringType.Assamese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Assamese', tag='Assamese')
LanguageStringType.Aymara = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Aymara', tag='Aymara')
LanguageStringType.Azerbaijani = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Azerbaijani', tag='Azerbaijani')
LanguageStringType.Bambara = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bambara', tag='Bambara')
LanguageStringType.Bashkir = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bashkir', tag='Bashkir')
LanguageStringType.Basque = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Basque', tag='Basque')
LanguageStringType.Bengali = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bengali', tag='Bengali')
LanguageStringType.Berber = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Berber', tag='Berber')
LanguageStringType.Bhutani = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bhutani', tag='Bhutani')
LanguageStringType.Bihari = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bihari', tag='Bihari')
LanguageStringType.Bislama = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bislama', tag='Bislama')
LanguageStringType.Breton = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Breton', tag='Breton')
LanguageStringType.Bulgarian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Bulgarian', tag='Bulgarian')
LanguageStringType.Burmese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Burmese', tag='Burmese')
LanguageStringType.Buryat = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Buryat', tag='Buryat')
LanguageStringType.Byelorussian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Byelorussian', tag='Byelorussian')
LanguageStringType.CantoneseChinese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='CantoneseChinese', tag='CantoneseChinese')
LanguageStringType.Castillian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Castillian', tag='Castillian')
LanguageStringType.Catalan = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Catalan', tag='Catalan')
LanguageStringType.Cayuga = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Cayuga', tag='Cayuga')
LanguageStringType.Cheyenne = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Cheyenne', tag='Cheyenne')
LanguageStringType.Chinese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Chinese', tag='Chinese')
LanguageStringType.ClassicalNewari = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='ClassicalNewari', tag='ClassicalNewari')
LanguageStringType.Cornish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Cornish', tag='Cornish')
LanguageStringType.Corsican = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Corsican', tag='Corsican')
LanguageStringType.Creole = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Creole', tag='Creole')
LanguageStringType.CrimeanTatar = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='CrimeanTatar', tag='CrimeanTatar')
LanguageStringType.Croatian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Croatian', tag='Croatian')
LanguageStringType.Czech = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Czech', tag='Czech')
LanguageStringType.Danish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Danish', tag='Danish')
LanguageStringType.Dargwa = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Dargwa', tag='Dargwa')
LanguageStringType.Dutch = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Dutch', tag='Dutch')
LanguageStringType.English = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='English', tag='English')
LanguageStringType.Esperanto = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Esperanto', tag='Esperanto')
LanguageStringType.Estonian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Estonian', tag='Estonian')
LanguageStringType.Faroese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Faroese', tag='Faroese')
LanguageStringType.Farsi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Farsi', tag='Farsi')
LanguageStringType.Fiji = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Fiji', tag='Fiji')
LanguageStringType.Filipino = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Filipino', tag='Filipino')
LanguageStringType.Finnish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Finnish', tag='Finnish')
LanguageStringType.Flemish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Flemish', tag='Flemish')
LanguageStringType.French = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='French', tag='French')
LanguageStringType.FrenchCanadian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='FrenchCanadian', tag='FrenchCanadian')
LanguageStringType.Frisian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Frisian', tag='Frisian')
LanguageStringType.Galician = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Galician', tag='Galician')
LanguageStringType.Georgian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Georgian', tag='Georgian')
LanguageStringType.German = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='German', tag='German')
LanguageStringType.Gibberish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Gibberish', tag='Gibberish')
LanguageStringType.Greek = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Greek', tag='Greek')
LanguageStringType.Greenlandic = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Greenlandic', tag='Greenlandic')
LanguageStringType.Guarani = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Guarani', tag='Guarani')
LanguageStringType.Gujarati = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Gujarati', tag='Gujarati')
LanguageStringType.Gullah = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Gullah', tag='Gullah')
LanguageStringType.Hausa = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Hausa', tag='Hausa')
LanguageStringType.Hawaiian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Hawaiian', tag='Hawaiian')
LanguageStringType.Hebrew = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Hebrew', tag='Hebrew')
LanguageStringType.Hindi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Hindi', tag='Hindi')
LanguageStringType.Hmong = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Hmong', tag='Hmong')
LanguageStringType.Hungarian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Hungarian', tag='Hungarian')
LanguageStringType.Icelandic = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Icelandic', tag='Icelandic')
LanguageStringType.IndoEuropean = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='IndoEuropean', tag='IndoEuropean')
LanguageStringType.Indonesian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Indonesian', tag='Indonesian')
LanguageStringType.Ingush = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Ingush', tag='Ingush')
LanguageStringType.Interlingua = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Interlingua', tag='Interlingua')
LanguageStringType.Interlingue = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Interlingue', tag='Interlingue')
LanguageStringType.Inuktitun = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Inuktitun', tag='Inuktitun')
LanguageStringType.Inuktitut = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Inuktitut', tag='Inuktitut')
LanguageStringType.Inupiak = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Inupiak', tag='Inupiak')
LanguageStringType.Inupiaq = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Inupiaq', tag='Inupiaq')
LanguageStringType.Irish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Irish', tag='Irish')
LanguageStringType.Italian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Italian', tag='Italian')
LanguageStringType.Japanese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Japanese', tag='Japanese')
LanguageStringType.Javanese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Javanese', tag='Javanese')
LanguageStringType.Kalaallisut = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kalaallisut', tag='Kalaallisut')
LanguageStringType.Kalmyk = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kalmyk', tag='Kalmyk')
LanguageStringType.Kannada = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kannada', tag='Kannada')
LanguageStringType.KarachayBalkar = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='KarachayBalkar', tag='KarachayBalkar')
LanguageStringType.Kashmiri = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kashmiri', tag='Kashmiri')
LanguageStringType.Kashubian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kashubian', tag='Kashubian')
LanguageStringType.Kazakh = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kazakh', tag='Kazakh')
LanguageStringType.Khmer = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Khmer', tag='Khmer')
LanguageStringType.Kinyarwanda = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kinyarwanda', tag='Kinyarwanda')
LanguageStringType.Kirghiz = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kirghiz', tag='Kirghiz')
LanguageStringType.Kirundi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kirundi', tag='Kirundi')
LanguageStringType.Klingon = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Klingon', tag='Klingon')
LanguageStringType.Korean = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Korean', tag='Korean')
LanguageStringType.Kurdish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Kurdish', tag='Kurdish')
LanguageStringType.Ladino = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Ladino', tag='Ladino')
LanguageStringType.Lao = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Lao', tag='Lao')
LanguageStringType.Lapp = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Lapp', tag='Lapp')
LanguageStringType.Latin = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Latin', tag='Latin')
LanguageStringType.Latvian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Latvian', tag='Latvian')
LanguageStringType.Lingala = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Lingala', tag='Lingala')
LanguageStringType.Lithuanian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Lithuanian', tag='Lithuanian')
LanguageStringType.Lojban = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Lojban', tag='Lojban')
LanguageStringType.LowerSorbian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='LowerSorbian', tag='LowerSorbian')
LanguageStringType.Macedonian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Macedonian', tag='Macedonian')
LanguageStringType.Malagasy = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Malagasy', tag='Malagasy')
LanguageStringType.Malay = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Malay', tag='Malay')
LanguageStringType.Malayalam = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Malayalam', tag='Malayalam')
LanguageStringType.Maltese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Maltese', tag='Maltese')
LanguageStringType.MandarinChinese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='MandarinChinese', tag='MandarinChinese')
LanguageStringType.Maori = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Maori', tag='Maori')
LanguageStringType.Marathi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Marathi', tag='Marathi')
LanguageStringType.Mende = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Mende', tag='Mende')
LanguageStringType.MiddleEnglish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='MiddleEnglish', tag='MiddleEnglish')
LanguageStringType.Mirandese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Mirandese', tag='Mirandese')
LanguageStringType.Moksha = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Moksha', tag='Moksha')
LanguageStringType.Moldavian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Moldavian', tag='Moldavian')
LanguageStringType.Mongo = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Mongo', tag='Mongo')
LanguageStringType.Mongolian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Mongolian', tag='Mongolian')
LanguageStringType.Multilingual = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Multilingual', tag='Multilingual')
LanguageStringType.Nauru = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Nauru', tag='Nauru')
LanguageStringType.Navaho = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Navaho', tag='Navaho')
LanguageStringType.Nepali = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Nepali', tag='Nepali')
LanguageStringType.Nogai = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Nogai', tag='Nogai')
LanguageStringType.Norwegian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Norwegian', tag='Norwegian')
LanguageStringType.Occitan = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Occitan', tag='Occitan')
LanguageStringType.OldEnglish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='OldEnglish', tag='OldEnglish')
LanguageStringType.Oriya = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Oriya', tag='Oriya')
LanguageStringType.Oromo = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Oromo', tag='Oromo')
LanguageStringType.Pashto = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Pashto', tag='Pashto')
LanguageStringType.Persian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Persian', tag='Persian')
LanguageStringType.PigLatin = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='PigLatin', tag='PigLatin')
LanguageStringType.Polish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Polish', tag='Polish')
LanguageStringType.Portuguese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Portuguese', tag='Portuguese')
LanguageStringType.Punjabi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Punjabi', tag='Punjabi')
LanguageStringType.Quechua = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Quechua', tag='Quechua')
LanguageStringType.Romance = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Romance', tag='Romance')
LanguageStringType.Romanian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Romanian', tag='Romanian')
LanguageStringType.Romany = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Romany', tag='Romany')
LanguageStringType.Russian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Russian', tag='Russian')
LanguageStringType.Samaritan = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Samaritan', tag='Samaritan')
LanguageStringType.Samoan = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Samoan', tag='Samoan')
LanguageStringType.Sangho = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sangho', tag='Sangho')
LanguageStringType.Sanskrit = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sanskrit', tag='Sanskrit')
LanguageStringType.Serbian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Serbian', tag='Serbian')
LanguageStringType.Serbo_Croatian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Serbo-Croatian', tag='Serbo_Croatian')
LanguageStringType.Sesotho = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sesotho', tag='Sesotho')
LanguageStringType.Setswana = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Setswana', tag='Setswana')
LanguageStringType.Shona = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Shona', tag='Shona')
LanguageStringType.SichuanYi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='SichuanYi', tag='SichuanYi')
LanguageStringType.Sicilian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sicilian', tag='Sicilian')
LanguageStringType.SignLanguage = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='SignLanguage', tag='SignLanguage')
LanguageStringType.Sindhi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sindhi', tag='Sindhi')
LanguageStringType.Sinhalese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sinhalese', tag='Sinhalese')
LanguageStringType.Siswati = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Siswati', tag='Siswati')
LanguageStringType.Slavic = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Slavic', tag='Slavic')
LanguageStringType.Slovak = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Slovak', tag='Slovak')
LanguageStringType.Slovakian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Slovakian', tag='Slovakian')
LanguageStringType.Slovene = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Slovene', tag='Slovene')
LanguageStringType.Somali = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Somali', tag='Somali')
LanguageStringType.Spanish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Spanish', tag='Spanish')
LanguageStringType.Sumerian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sumerian', tag='Sumerian')
LanguageStringType.Sundanese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Sundanese', tag='Sundanese')
LanguageStringType.Swahili = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Swahili', tag='Swahili')
LanguageStringType.Swedish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Swedish', tag='Swedish')
LanguageStringType.SwissGerman = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='SwissGerman', tag='SwissGerman')
LanguageStringType.Syriac = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Syriac', tag='Syriac')
LanguageStringType.Tagalog = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tagalog', tag='Tagalog')
LanguageStringType.TaiwaneseChinese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='TaiwaneseChinese', tag='TaiwaneseChinese')
LanguageStringType.Tajik = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tajik', tag='Tajik')
LanguageStringType.Tamil = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tamil', tag='Tamil')
LanguageStringType.Tatar = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tatar', tag='Tatar')
LanguageStringType.Telugu = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Telugu', tag='Telugu')
LanguageStringType.Thai = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Thai', tag='Thai')
LanguageStringType.Tibetan = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tibetan', tag='Tibetan')
LanguageStringType.Tigrinya = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tigrinya', tag='Tigrinya')
LanguageStringType.Tonga = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tonga', tag='Tonga')
LanguageStringType.Tsonga = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Tsonga', tag='Tsonga')
LanguageStringType.Turkish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Turkish', tag='Turkish')
LanguageStringType.Turkmen = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Turkmen', tag='Turkmen')
LanguageStringType.Twi = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Twi', tag='Twi')
LanguageStringType.Udmurt = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Udmurt', tag='Udmurt')
LanguageStringType.Uighur = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Uighur', tag='Uighur')
LanguageStringType.Ukrainian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Ukrainian', tag='Ukrainian')
LanguageStringType.Ukranian = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Ukranian', tag='Ukranian')
LanguageStringType.Unknown = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Unknown', tag='Unknown')
LanguageStringType.Urdu = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Urdu', tag='Urdu')
LanguageStringType.Uzbek = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Uzbek', tag='Uzbek')
LanguageStringType.Vietnamese = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Vietnamese', tag='Vietnamese')
LanguageStringType.Volapuk = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Volapuk', tag='Volapuk')
LanguageStringType.Welsh = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Welsh', tag='Welsh')
LanguageStringType.Wolof = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Wolof', tag='Wolof')
LanguageStringType.Xhosa = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Xhosa', tag='Xhosa')
LanguageStringType.Yiddish = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Yiddish', tag='Yiddish')
LanguageStringType.Yoruba = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Yoruba', tag='Yoruba')
LanguageStringType.Zhuang = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Zhuang', tag='Zhuang')
LanguageStringType.Zulu = LanguageStringType._CF_enumeration.addEnumeration(unicode_value='Zulu', tag='Zulu')
LanguageStringType._InitializeFacetMap(LanguageStringType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LanguageStringType', LanguageStringType)

# Atomic simple type: LanguageSWVG
class LanguageSWVG (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LanguageSWVG')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3694, 1)
    _Documentation = None
LanguageSWVG._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LanguageSWVG, enum_prefix=None)
LanguageSWVG.adygei = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='adygei', tag='adygei')
LanguageSWVG.afrikaans = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='afrikaans', tag='afrikaans')
LanguageSWVG.albanian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='albanian', tag='albanian')
LanguageSWVG.alsatian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='alsatian', tag='alsatian')
LanguageSWVG.amharic = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='amharic', tag='amharic')
LanguageSWVG.arabic = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='arabic', tag='arabic')
LanguageSWVG.armenian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='armenian', tag='armenian')
LanguageSWVG.assamese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='assamese', tag='assamese')
LanguageSWVG.bambara = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='bambara', tag='bambara')
LanguageSWVG.basque = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='basque', tag='basque')
LanguageSWVG.bengali = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='bengali', tag='bengali')
LanguageSWVG.berber = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='berber', tag='berber')
LanguageSWVG.breton = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='breton', tag='breton')
LanguageSWVG.bulgarian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='bulgarian', tag='bulgarian')
LanguageSWVG.buryat = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='buryat', tag='buryat')
LanguageSWVG.cantonese_chinese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='cantonese_chinese', tag='cantonese_chinese')
LanguageSWVG.castillian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='castillian', tag='castillian')
LanguageSWVG.catalan = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='catalan', tag='catalan')
LanguageSWVG.cayuga = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='cayuga', tag='cayuga')
LanguageSWVG.cheyenne = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='cheyenne', tag='cheyenne')
LanguageSWVG.chinese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='chinese', tag='chinese')
LanguageSWVG.classical_newari = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='classical_newari', tag='classical_newari')
LanguageSWVG.cornish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='cornish', tag='cornish')
LanguageSWVG.corsican = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='corsican', tag='corsican')
LanguageSWVG.creole = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='creole', tag='creole')
LanguageSWVG.crimean_tatar = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='crimean_tatar', tag='crimean_tatar')
LanguageSWVG.croatian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='croatian', tag='croatian')
LanguageSWVG.czech = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='czech', tag='czech')
LanguageSWVG.danish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='danish', tag='danish')
LanguageSWVG.dargwa = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='dargwa', tag='dargwa')
LanguageSWVG.dutch = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='dutch', tag='dutch')
LanguageSWVG.english = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='english', tag='english')
LanguageSWVG.esperanto = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='esperanto', tag='esperanto')
LanguageSWVG.estonian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='estonian', tag='estonian')
LanguageSWVG.farsi = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='farsi', tag='farsi')
LanguageSWVG.filipino = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='filipino', tag='filipino')
LanguageSWVG.finnish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='finnish', tag='finnish')
LanguageSWVG.flemish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='flemish', tag='flemish')
LanguageSWVG.french = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='french', tag='french')
LanguageSWVG.french_canadian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='french_canadian', tag='french_canadian')
LanguageSWVG.georgian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='georgian', tag='georgian')
LanguageSWVG.german = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='german', tag='german')
LanguageSWVG.gibberish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='gibberish', tag='gibberish')
LanguageSWVG.greek = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='greek', tag='greek')
LanguageSWVG.gujarati = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='gujarati', tag='gujarati')
LanguageSWVG.gullah = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='gullah', tag='gullah')
LanguageSWVG.hausa = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='hausa', tag='hausa')
LanguageSWVG.hawaiian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='hawaiian', tag='hawaiian')
LanguageSWVG.hebrew = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='hebrew', tag='hebrew')
LanguageSWVG.hindi = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='hindi', tag='hindi')
LanguageSWVG.hmong = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='hmong', tag='hmong')
LanguageSWVG.hungarian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='hungarian', tag='hungarian')
LanguageSWVG.icelandic = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='icelandic', tag='icelandic')
LanguageSWVG.indo_european = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='indo_european', tag='indo_european')
LanguageSWVG.indonesian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='indonesian', tag='indonesian')
LanguageSWVG.ingush = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='ingush', tag='ingush')
LanguageSWVG.inuktitun = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='inuktitun', tag='inuktitun')
LanguageSWVG.inuktitut = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='inuktitut', tag='inuktitut')
LanguageSWVG.inupiaq = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='inupiaq', tag='inupiaq')
LanguageSWVG.irish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='irish', tag='irish')
LanguageSWVG.italian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='italian', tag='italian')
LanguageSWVG.japanese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='japanese', tag='japanese')
LanguageSWVG.kalaallisut = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='kalaallisut', tag='kalaallisut')
LanguageSWVG.kalmyk = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='kalmyk', tag='kalmyk')
LanguageSWVG.karachay_balkar = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='karachay_balkar', tag='karachay_balkar')
LanguageSWVG.kashubian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='kashubian', tag='kashubian')
LanguageSWVG.kazakh = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='kazakh', tag='kazakh')
LanguageSWVG.khmer = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='khmer', tag='khmer')
LanguageSWVG.klingon = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='klingon', tag='klingon')
LanguageSWVG.korean = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='korean', tag='korean')
LanguageSWVG.kurdish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='kurdish', tag='kurdish')
LanguageSWVG.ladino = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='ladino', tag='ladino')
LanguageSWVG.lao = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='lao', tag='lao')
LanguageSWVG.lapp = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='lapp', tag='lapp')
LanguageSWVG.latin = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='latin', tag='latin')
LanguageSWVG.lithuanian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='lithuanian', tag='lithuanian')
LanguageSWVG.lojban = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='lojban', tag='lojban')
LanguageSWVG.lower_sorbian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='lower_sorbian', tag='lower_sorbian')
LanguageSWVG.macedonian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='macedonian', tag='macedonian')
LanguageSWVG.malagasy = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='malagasy', tag='malagasy')
LanguageSWVG.malay = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='malay', tag='malay')
LanguageSWVG.malayalam = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='malayalam', tag='malayalam')
LanguageSWVG.maltese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='maltese', tag='maltese')
LanguageSWVG.mandarin_chinese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='mandarin_chinese', tag='mandarin_chinese')
LanguageSWVG.maori = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='maori', tag='maori')
LanguageSWVG.mende = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='mende', tag='mende')
LanguageSWVG.middle_english = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='middle_english', tag='middle_english')
LanguageSWVG.mirandese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='mirandese', tag='mirandese')
LanguageSWVG.moksha = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='moksha', tag='moksha')
LanguageSWVG.mongo = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='mongo', tag='mongo')
LanguageSWVG.mongolian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='mongolian', tag='mongolian')
LanguageSWVG.multilingual = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='multilingual', tag='multilingual')
LanguageSWVG.navaho = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='navaho', tag='navaho')
LanguageSWVG.nogai = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='nogai', tag='nogai')
LanguageSWVG.norwegian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='norwegian', tag='norwegian')
LanguageSWVG.old_english = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='old_english', tag='old_english')
LanguageSWVG.persian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='persian', tag='persian')
LanguageSWVG.pig_latin = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='pig_latin', tag='pig_latin')
LanguageSWVG.polish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='polish', tag='polish')
LanguageSWVG.portuguese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='portuguese', tag='portuguese')
LanguageSWVG.romance = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='romance', tag='romance')
LanguageSWVG.romanian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='romanian', tag='romanian')
LanguageSWVG.romany = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='romany', tag='romany')
LanguageSWVG.russian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='russian', tag='russian')
LanguageSWVG.samaritan = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='samaritan', tag='samaritan')
LanguageSWVG.sanskrit = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='sanskrit', tag='sanskrit')
LanguageSWVG.serbian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='serbian', tag='serbian')
LanguageSWVG.serbo_croatian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='serbo-croatian', tag='serbo_croatian')
LanguageSWVG.sichuan_yi = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='sichuan_yi', tag='sichuan_yi')
LanguageSWVG.sicilian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='sicilian', tag='sicilian')
LanguageSWVG.sign_language = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='sign_language', tag='sign_language')
LanguageSWVG.slavic = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='slavic', tag='slavic')
LanguageSWVG.slovak = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='slovak', tag='slovak')
LanguageSWVG.slovene = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='slovene', tag='slovene')
LanguageSWVG.somali = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='somali', tag='somali')
LanguageSWVG.spanish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='spanish', tag='spanish')
LanguageSWVG.sumerian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='sumerian', tag='sumerian')
LanguageSWVG.swahili = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='swahili', tag='swahili')
LanguageSWVG.swedish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='swedish', tag='swedish')
LanguageSWVG.swiss_german = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='swiss_german', tag='swiss_german')
LanguageSWVG.tagalog = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='tagalog', tag='tagalog')
LanguageSWVG.taiwanese_chinese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='taiwanese_chinese', tag='taiwanese_chinese')
LanguageSWVG.tamil = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='tamil', tag='tamil')
LanguageSWVG.thai = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='thai', tag='thai')
LanguageSWVG.tibetan = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='tibetan', tag='tibetan')
LanguageSWVG.turkish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='turkish', tag='turkish')
LanguageSWVG.udmurt = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='udmurt', tag='udmurt')
LanguageSWVG.ukrainian = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='ukrainian', tag='ukrainian')
LanguageSWVG.unknown = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
LanguageSWVG.urdu = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='urdu', tag='urdu')
LanguageSWVG.vietnamese = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='vietnamese', tag='vietnamese')
LanguageSWVG.welsh = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='welsh', tag='welsh')
LanguageSWVG.wolof = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='wolof', tag='wolof')
LanguageSWVG.xhosa = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='xhosa', tag='xhosa')
LanguageSWVG.yiddish = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='yiddish', tag='yiddish')
LanguageSWVG.zulu = LanguageSWVG._CF_enumeration.addEnumeration(unicode_value='zulu', tag='zulu')
LanguageSWVG._InitializeFacetMap(LanguageSWVG._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LanguageSWVG', LanguageSWVG)

# Atomic simple type: MusicFormatType
class MusicFormatType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MusicFormatType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3834, 1)
    _Documentation = None
MusicFormatType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=MusicFormatType, enum_prefix=None)
MusicFormatType.authorized_bootleg = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='authorized_bootleg', tag='authorized_bootleg')
MusicFormatType.bsides = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='bsides', tag='bsides')
MusicFormatType.best_of = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='best_of', tag='best_of')
MusicFormatType.box_set = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='box_set', tag='box_set')
MusicFormatType.original_recording = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='original_recording', tag='original_recording')
MusicFormatType.reissued = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='reissued', tag='reissued')
MusicFormatType.remastered = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='remastered', tag='remastered')
MusicFormatType.soundtrack = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='soundtrack', tag='soundtrack')
MusicFormatType.special_edition = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='special_edition', tag='special_edition')
MusicFormatType.special_limited_edition = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='special_limited_edition', tag='special_limited_edition')
MusicFormatType.cast_recording = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='cast_recording', tag='cast_recording')
MusicFormatType.compilation = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='compilation', tag='compilation')
MusicFormatType.deluxe_edition = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='deluxe_edition', tag='deluxe_edition')
MusicFormatType.digital_sound = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='digital_sound', tag='digital_sound')
MusicFormatType.double_lp = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='double_lp', tag='double_lp')
MusicFormatType.explicit_lyrics = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='explicit_lyrics', tag='explicit_lyrics')
MusicFormatType.hi_fidelity = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='hi-fidelity', tag='hi_fidelity')
MusicFormatType.import_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='import', tag='import_')
MusicFormatType.limited_collectors_edition = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='limited_collectors_edition', tag='limited_collectors_edition')
MusicFormatType.limited_edition = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='limited_edition', tag='limited_edition')
MusicFormatType.remixes = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='remixes', tag='remixes')
MusicFormatType.live = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='live', tag='live')
MusicFormatType.extra_tracks = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='extra_tracks', tag='extra_tracks')
MusicFormatType.cutout = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='cutout', tag='cutout')
MusicFormatType.cd_and_dvd = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='cd_and_dvd', tag='cd_and_dvd')
MusicFormatType.dual_disc = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='dual_disc', tag='dual_disc')
MusicFormatType.hybrid_sacd = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='hybrid_sacd', tag='hybrid_sacd')
MusicFormatType.cd_single = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='cd-single', tag='cd_single')
MusicFormatType.maxi_single = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='maxi_single', tag='maxi_single')
MusicFormatType.sacd = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='sacd', tag='sacd')
MusicFormatType.minidisc = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='minidisc', tag='minidisc')
MusicFormatType.uk_import = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='uk_import', tag='uk_import')
MusicFormatType.us_import = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='us_import', tag='us_import')
MusicFormatType.jp_import = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='jp_import', tag='jp_import')
MusicFormatType.enhanced = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='enhanced', tag='enhanced')
MusicFormatType.clean = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='clean', tag='clean')
MusicFormatType.copy_protected_cd = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='copy_protected_cd', tag='copy_protected_cd')
MusicFormatType.double_lp_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='double_lp', tag='double_lp_')
MusicFormatType.soundtrack_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='soundtrack', tag='soundtrack_')
MusicFormatType.cd_single_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='cd-single', tag='cd_single_')
MusicFormatType.remastered_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='remastered', tag='remastered_')
MusicFormatType.box_set_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='box_set', tag='box_set_')
MusicFormatType.double_cd = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='double_cd', tag='double_cd')
MusicFormatType.karaoke = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='karaoke', tag='karaoke')
MusicFormatType.limited_edition_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='limited_edition', tag='limited_edition_')
MusicFormatType.maxi_single_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='maxi_single', tag='maxi_single_')
MusicFormatType.mp3_audio = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='mp3_audio', tag='mp3_audio')
MusicFormatType.ringle = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='ringle', tag='ringle')
MusicFormatType.cd_and_dvd_ = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='cd_and_dvd', tag='cd_and_dvd_')
MusicFormatType.shm_cd = MusicFormatType._CF_enumeration.addEnumeration(unicode_value='shm_cd', tag='shm_cd')
MusicFormatType._InitializeFacetMap(MusicFormatType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'MusicFormatType', MusicFormatType)

# Atomic simple type: GiftCardsFormatType
class GiftCardsFormatType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'GiftCardsFormatType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3888, 1)
    _Documentation = None
GiftCardsFormatType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=GiftCardsFormatType, enum_prefix=None)
GiftCardsFormatType.email_gift_cards = GiftCardsFormatType._CF_enumeration.addEnumeration(unicode_value='email_gift_cards', tag='email_gift_cards')
GiftCardsFormatType.plastic_gift_cards = GiftCardsFormatType._CF_enumeration.addEnumeration(unicode_value='plastic_gift_cards', tag='plastic_gift_cards')
GiftCardsFormatType.print_at_home = GiftCardsFormatType._CF_enumeration.addEnumeration(unicode_value='print_at_home', tag='print_at_home')
GiftCardsFormatType.multi_pack = GiftCardsFormatType._CF_enumeration.addEnumeration(unicode_value='multi_pack', tag='multi_pack')
GiftCardsFormatType.facebook = GiftCardsFormatType._CF_enumeration.addEnumeration(unicode_value='facebook', tag='facebook')
GiftCardsFormatType.gift_box = GiftCardsFormatType._CF_enumeration.addEnumeration(unicode_value='gift_box', tag='gift_box')
GiftCardsFormatType._InitializeFacetMap(GiftCardsFormatType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'GiftCardsFormatType', GiftCardsFormatType)

# Atomic simple type: CountryOfOriginType
class CountryOfOriginType (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CountryOfOriginType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3898, 1)
    _Documentation = None
CountryOfOriginType._CF_pattern = pyxb.binding.facets.CF_pattern()
CountryOfOriginType._CF_pattern.addPattern(pattern='[a-zA-Z][a-zA-Z]|unknown')
CountryOfOriginType._InitializeFacetMap(CountryOfOriginType._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'CountryOfOriginType', CountryOfOriginType)

# Atomic simple type: AudioEncodingType
class AudioEncodingType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AudioEncodingType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3903, 1)
    _Documentation = None
AudioEncodingType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AudioEncodingType, enum_prefix=None)
AudioEncodingType.n5_1_disney_enhanced_home_theater_mix = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='5_1_disney_enhanced_home_theater_mix', tag='n5_1_disney_enhanced_home_theater_mix')
AudioEncodingType.n7_1_disney_enhanced_home_theater_mix = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='7_1_disney_enhanced_home_theater_mix', tag='n7_1_disney_enhanced_home_theater_mix')
AudioEncodingType.analog = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='analog', tag='analog')
AudioEncodingType.digital_atrac = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='digital_atrac', tag='digital_atrac')
AudioEncodingType.dolby_digital_1_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_1.0', tag='dolby_digital_1_0')
AudioEncodingType.dolby_digital_2_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_2.0', tag='dolby_digital_2_0')
AudioEncodingType.dolby_digital_2_0_mono = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_2.0_mono', tag='dolby_digital_2_0_mono')
AudioEncodingType.dolby_digital_2_0_stereo = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_2.0_stereo', tag='dolby_digital_2_0_stereo')
AudioEncodingType.dolby_digital_2_0_surround = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_2.0_surround', tag='dolby_digital_2_0_surround')
AudioEncodingType.dolby_digital_2_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_2.1', tag='dolby_digital_2_1')
AudioEncodingType.dolby_digital_3_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_3.0', tag='dolby_digital_3_0')
AudioEncodingType.dolby_digital_4_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_4.0', tag='dolby_digital_4_0')
AudioEncodingType.dolby_digital_4_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_4.1', tag='dolby_digital_4_1')
AudioEncodingType.dolby_digital_5_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_5.0', tag='dolby_digital_5_0')
AudioEncodingType.dolby_digital_5_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_5.1', tag='dolby_digital_5_1')
AudioEncodingType.dolby_digital_5_1_es = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_5.1_es', tag='dolby_digital_5_1_es')
AudioEncodingType.dolby_digital_5_1_ex = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_5.1_ex', tag='dolby_digital_5_1_ex')
AudioEncodingType.dolby_digital_6_1_es = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_6.1_es', tag='dolby_digital_6_1_es')
AudioEncodingType.dolby_digital_6_1_ex = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_6.1_ex', tag='dolby_digital_6_1_ex')
AudioEncodingType.dolby_digital_ex = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_ex', tag='dolby_digital_ex')
AudioEncodingType.dolby_digital_live = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_live', tag='dolby_digital_live')
AudioEncodingType.dolby_digital_plus = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_plus', tag='dolby_digital_plus')
AudioEncodingType.dolby_digital_plus_2_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_plus_2_0', tag='dolby_digital_plus_2_0')
AudioEncodingType.dolby_digital_plus_5_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_digital_plus_5_1', tag='dolby_digital_plus_5_1')
AudioEncodingType.dolby_stereo_analog = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_stereo_analog', tag='dolby_stereo_analog')
AudioEncodingType.dolby_surround = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_surround', tag='dolby_surround')
AudioEncodingType.dolby_truehd = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_truehd', tag='dolby_truehd')
AudioEncodingType.dolby_truehd_5_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dolby_truehd_5_1', tag='dolby_truehd_5_1')
AudioEncodingType.dts_5_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_5.0', tag='dts_5_0')
AudioEncodingType.dts_5_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_5.1', tag='dts_5_1')
AudioEncodingType.dts_6_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_6.1', tag='dts_6_1')
AudioEncodingType.dts_6_1_es = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_6_1_es', tag='dts_6_1_es')
AudioEncodingType.dts_6_1_es_ = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_6.1_es', tag='dts_6_1_es_')
AudioEncodingType.dts_es = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_es', tag='dts_es')
AudioEncodingType.dts_hd_high_res_audio = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_hd_high_res_audio', tag='dts_hd_high_res_audio')
AudioEncodingType.dts_interactive = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='dts_interactive', tag='dts_interactive')
AudioEncodingType.hi_res_96_24_digital_surround = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='hi_res_96_24_digital_surround', tag='hi_res_96_24_digital_surround')
AudioEncodingType.mlp_lossless = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='mlp_lossless', tag='mlp_lossless')
AudioEncodingType.mono = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='mono', tag='mono')
AudioEncodingType.mpeg_1_2_0 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='mpeg_1_2.0', tag='mpeg_1_2_0')
AudioEncodingType.mpeg_2_5_1 = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='mpeg_2_5.1', tag='mpeg_2_5_1')
AudioEncodingType.pcm = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='pcm', tag='pcm')
AudioEncodingType.pcm_24bit_96khz = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='pcm_24bit_96khz', tag='pcm_24bit_96khz')
AudioEncodingType.pcm_mono = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='pcm_mono', tag='pcm_mono')
AudioEncodingType.pcm_stereo = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='pcm_stereo', tag='pcm_stereo')
AudioEncodingType.pcm_surround = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='pcm_surround', tag='pcm_surround')
AudioEncodingType.quadraphonic = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='quadraphonic', tag='quadraphonic')
AudioEncodingType.stereo = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='stereo', tag='stereo')
AudioEncodingType.surround = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='surround', tag='surround')
AudioEncodingType.thx_surround_ex = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='thx_surround_ex', tag='thx_surround_ex')
AudioEncodingType.unknown_audio_encoding = AudioEncodingType._CF_enumeration.addEnumeration(unicode_value='unknown_audio_encoding', tag='unknown_audio_encoding')
AudioEncodingType._InitializeFacetMap(AudioEncodingType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AudioEncodingType', AudioEncodingType)

# Atomic simple type: ZoomUnitOfMeasure
class ZoomUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ZoomUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3963, 1)
    _Documentation = None
ZoomUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ZoomUnitOfMeasure, enum_prefix=None)
ZoomUnitOfMeasure.x = ZoomUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='x', tag='x')
ZoomUnitOfMeasure._InitializeFacetMap(ZoomUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ZoomUnitOfMeasure', ZoomUnitOfMeasure)

# Atomic simple type: PixelUnitOfMeasure
class PixelUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PixelUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3975, 1)
    _Documentation = None
PixelUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PixelUnitOfMeasure, enum_prefix=None)
PixelUnitOfMeasure.pixels = PixelUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='pixels', tag='pixels')
PixelUnitOfMeasure.MP = PixelUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MP', tag='MP')
PixelUnitOfMeasure._InitializeFacetMap(PixelUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PixelUnitOfMeasure', PixelUnitOfMeasure)

# Atomic simple type: PressureUnitOfMeasure
class PressureUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PressureUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3988, 1)
    _Documentation = None
PressureUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PressureUnitOfMeasure, enum_prefix=None)
PressureUnitOfMeasure.bars = PressureUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='bars', tag='bars')
PressureUnitOfMeasure.psi = PressureUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='psi', tag='psi')
PressureUnitOfMeasure.pascal = PressureUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='pascal', tag='pascal')
PressureUnitOfMeasure._InitializeFacetMap(PressureUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PressureUnitOfMeasure', PressureUnitOfMeasure)

# Atomic simple type: OpticalPowerUnitOfMeasure
class OpticalPowerUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OpticalPowerUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4002, 1)
    _Documentation = None
OpticalPowerUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=OpticalPowerUnitOfMeasure, enum_prefix=None)
OpticalPowerUnitOfMeasure.diopters = OpticalPowerUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='diopters', tag='diopters')
OpticalPowerUnitOfMeasure._InitializeFacetMap(OpticalPowerUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'OpticalPowerUnitOfMeasure', OpticalPowerUnitOfMeasure)

# Atomic simple type: StoneCreationMethod
class StoneCreationMethod (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StoneCreationMethod')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4056, 1)
    _Documentation = None
StoneCreationMethod._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=StoneCreationMethod, enum_prefix=None)
StoneCreationMethod.natural = StoneCreationMethod._CF_enumeration.addEnumeration(unicode_value='natural', tag='natural')
StoneCreationMethod.simulated = StoneCreationMethod._CF_enumeration.addEnumeration(unicode_value='simulated', tag='simulated')
StoneCreationMethod.synthetic = StoneCreationMethod._CF_enumeration.addEnumeration(unicode_value='synthetic', tag='synthetic')
StoneCreationMethod._InitializeFacetMap(StoneCreationMethod._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'StoneCreationMethod', StoneCreationMethod)

# Atomic simple type: AspectRatio
class AspectRatio (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AspectRatio')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4063, 1)
    _Documentation = None
AspectRatio._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AspectRatio, enum_prefix=None)
AspectRatio.n201 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2:01', tag='n201')
AspectRatio.n403 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='4:03', tag='n403')
AspectRatio.n1109 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='11:09', tag='n1109')
AspectRatio.n1409 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='14:09', tag='n1409')
AspectRatio.n1609 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='16:09', tag='n1609')
AspectRatio.n1_271 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.27:1', tag='n1_271')
AspectRatio.n1_291 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.29:1', tag='n1_291')
AspectRatio.n1_301 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.30:1', tag='n1_301')
AspectRatio.n1_331 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.33:1', tag='n1_331')
AspectRatio.n1_341 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.34:1', tag='n1_341')
AspectRatio.n1_351 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.35:1', tag='n1_351')
AspectRatio.n1_371 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.37:1', tag='n1_371')
AspectRatio.n1_381 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.38:1', tag='n1_381')
AspectRatio.n1_441 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.44:1', tag='n1_441')
AspectRatio.n1_451 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.45:1', tag='n1_451')
AspectRatio.n1_501 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.50:1', tag='n1_501')
AspectRatio.n1_551 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.55:1', tag='n1_551')
AspectRatio.n1_581 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.58:1', tag='n1_581')
AspectRatio.n1_591 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.59:1', tag='n1_591')
AspectRatio.n1_601 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.60:1', tag='n1_601')
AspectRatio.n1_631 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.63:1', tag='n1_631')
AspectRatio.n1_651 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.65:1', tag='n1_651')
AspectRatio.n1_661 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.66:1', tag='n1_661')
AspectRatio.n1_671 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.67:1', tag='n1_671')
AspectRatio.n1_701 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.70:1', tag='n1_701')
AspectRatio.n1_711 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.71:1', tag='n1_711')
AspectRatio.n1_741 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.74:1', tag='n1_741')
AspectRatio.n1_751 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.75:1', tag='n1_751')
AspectRatio.n1_761 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.76:1', tag='n1_761')
AspectRatio.n1_771 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.77:1', tag='n1_771')
AspectRatio.n1_781 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.78:1', tag='n1_781')
AspectRatio.n1_831 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.83:1', tag='n1_831')
AspectRatio.n1_851 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.85:1', tag='n1_851')
AspectRatio.n1_871 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.87:1', tag='n1_871')
AspectRatio.n1_881 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.88:1', tag='n1_881')
AspectRatio.n1_981 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='1.98:1', tag='n1_981')
AspectRatio.n2_101 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.10:1', tag='n2_101')
AspectRatio.n2_201 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.20:1', tag='n2_201')
AspectRatio.n2_211 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.21:1', tag='n2_211')
AspectRatio.n2_221 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.22:1', tag='n2_221')
AspectRatio.n2_301 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.30:1', tag='n2_301')
AspectRatio.n2_311 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.31:1', tag='n2_311')
AspectRatio.n2_331 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.33:1', tag='n2_331')
AspectRatio.n2_351 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.35:1', tag='n2_351')
AspectRatio.n2_391 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.39:1', tag='n2_391')
AspectRatio.n2_401 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.40:1', tag='n2_401')
AspectRatio.n2_551 = AspectRatio._CF_enumeration.addEnumeration(unicode_value='2.55:1', tag='n2_551')
AspectRatio.unknown_aspect_ratio = AspectRatio._CF_enumeration.addEnumeration(unicode_value='unknown_aspect_ratio', tag='unknown_aspect_ratio')
AspectRatio._InitializeFacetMap(AspectRatio._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AspectRatio', AspectRatio)

# Atomic simple type: BatteryCellTypeValues
class BatteryCellTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BatteryCellTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4115, 1)
    _Documentation = None
BatteryCellTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BatteryCellTypeValues, enum_prefix=None)
BatteryCellTypeValues.NiCAD = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='NiCAD', tag='NiCAD')
BatteryCellTypeValues.NiMh = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='NiMh', tag='NiMh')
BatteryCellTypeValues.alkaline = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='alkaline', tag='alkaline')
BatteryCellTypeValues.aluminum_oxygen = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='aluminum_oxygen', tag='aluminum_oxygen')
BatteryCellTypeValues.lead_acid = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lead_acid', tag='lead_acid')
BatteryCellTypeValues.lead_calcium = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lead_calcium', tag='lead_calcium')
BatteryCellTypeValues.lithium = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lithium', tag='lithium')
BatteryCellTypeValues.lithium_ion = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lithium_ion', tag='lithium_ion')
BatteryCellTypeValues.lithium_manganese_dioxide = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lithium_manganese_dioxide', tag='lithium_manganese_dioxide')
BatteryCellTypeValues.lithium_metal = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lithium_metal', tag='lithium_metal')
BatteryCellTypeValues.lithium_polymer = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='lithium_polymer', tag='lithium_polymer')
BatteryCellTypeValues.manganese = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='manganese', tag='manganese')
BatteryCellTypeValues.polymer = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='polymer', tag='polymer')
BatteryCellTypeValues.silver_oxide = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='silver_oxide', tag='silver_oxide')
BatteryCellTypeValues.zinc = BatteryCellTypeValues._CF_enumeration.addEnumeration(unicode_value='zinc', tag='zinc')
BatteryCellTypeValues._InitializeFacetMap(BatteryCellTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BatteryCellTypeValues', BatteryCellTypeValues)

# Atomic simple type: HazmatItemType
class HazmatItemType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HazmatItemType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4139, 1)
    _Documentation = None
HazmatItemType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=HazmatItemType, enum_prefix=None)
HazmatItemType.butane = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='butane', tag='butane')
HazmatItemType.fuel_cell = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='fuel_cell', tag='fuel_cell')
HazmatItemType.gasoline = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='gasoline', tag='gasoline')
HazmatItemType.orm_d_class_1 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_1', tag='orm_d_class_1')
HazmatItemType.orm_d_class_2 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_2', tag='orm_d_class_2')
HazmatItemType.orm_d_class_3 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_3', tag='orm_d_class_3')
HazmatItemType.orm_d_class_4 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_4', tag='orm_d_class_4')
HazmatItemType.orm_d_class_5 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_5', tag='orm_d_class_5')
HazmatItemType.orm_d_class_6 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_6', tag='orm_d_class_6')
HazmatItemType.orm_d_class_7 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_7', tag='orm_d_class_7')
HazmatItemType.orm_d_class_8 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_8', tag='orm_d_class_8')
HazmatItemType.orm_d_class_9 = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='orm_d_class_9', tag='orm_d_class_9')
HazmatItemType.sealed_lead_acid_battery = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='sealed_lead_acid_battery', tag='sealed_lead_acid_battery')
HazmatItemType.unknown = HazmatItemType._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
HazmatItemType._InitializeFacetMap(HazmatItemType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'HazmatItemType', HazmatItemType)

# Atomic simple type: AmazonMaturityRatingType
class AmazonMaturityRatingType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AmazonMaturityRatingType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4157, 1)
    _Documentation = None
AmazonMaturityRatingType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AmazonMaturityRatingType, enum_prefix=None)
AmazonMaturityRatingType.adult_content = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='adult_content', tag='adult_content')
AmazonMaturityRatingType.ages_13_and_older = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='ages_13_and_older', tag='ages_13_and_older')
AmazonMaturityRatingType.ages_17_and_older = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='ages_17_and_older', tag='ages_17_and_older')
AmazonMaturityRatingType.ages_9_and_older = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='ages_9_and_older', tag='ages_9_and_older')
AmazonMaturityRatingType.all_ages = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='all_ages', tag='all_ages')
AmazonMaturityRatingType.children = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='children', tag='children')
AmazonMaturityRatingType.rating_pending = AmazonMaturityRatingType._CF_enumeration.addEnumeration(unicode_value='rating_pending', tag='rating_pending')
AmazonMaturityRatingType._InitializeFacetMap(AmazonMaturityRatingType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AmazonMaturityRatingType', AmazonMaturityRatingType)

# Atomic simple type: IdentityPackageType
class IdentityPackageType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IdentityPackageType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4168, 1)
    _Documentation = None
IdentityPackageType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=IdentityPackageType, enum_prefix=None)
IdentityPackageType.bulk = IdentityPackageType._CF_enumeration.addEnumeration(unicode_value='bulk', tag='bulk')
IdentityPackageType.frustration_free = IdentityPackageType._CF_enumeration.addEnumeration(unicode_value='frustration_free', tag='frustration_free')
IdentityPackageType.traditional = IdentityPackageType._CF_enumeration.addEnumeration(unicode_value='traditional', tag='traditional')
IdentityPackageType._InitializeFacetMap(IdentityPackageType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'IdentityPackageType', IdentityPackageType)

# Atomic simple type: SerialNumberFormatType
class SerialNumberFormatType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SerialNumberFormatType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4175, 1)
    _Documentation = None
SerialNumberFormatType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=SerialNumberFormatType, enum_prefix=None)
SerialNumberFormatType.a_or_z_alphanumeric_13 = SerialNumberFormatType._CF_enumeration.addEnumeration(unicode_value='a_or_z_alphanumeric_13', tag='a_or_z_alphanumeric_13')
SerialNumberFormatType.alphanumeric_8 = SerialNumberFormatType._CF_enumeration.addEnumeration(unicode_value='alphanumeric_8', tag='alphanumeric_8')
SerialNumberFormatType.numeric_14 = SerialNumberFormatType._CF_enumeration.addEnumeration(unicode_value='numeric_14', tag='numeric_14')
SerialNumberFormatType.alphanumeric_14 = SerialNumberFormatType._CF_enumeration.addEnumeration(unicode_value='alphanumeric_14', tag='alphanumeric_14')
SerialNumberFormatType.numeric_12 = SerialNumberFormatType._CF_enumeration.addEnumeration(unicode_value='numeric_12', tag='numeric_12')
SerialNumberFormatType.w_alphanumeric_12 = SerialNumberFormatType._CF_enumeration.addEnumeration(unicode_value='w_alphanumeric_12', tag='w_alphanumeric_12')
SerialNumberFormatType._InitializeFacetMap(SerialNumberFormatType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'SerialNumberFormatType', SerialNumberFormatType)

# Atomic simple type: [anonymous]
class STD_ANON_26 (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4235, 5)
    _Documentation = None
STD_ANON_26._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(1500))
STD_ANON_26._InitializeFacetMap(STD_ANON_26._CF_maxLength)

# Atomic simple type: HumanInterfaceInputType
class HumanInterfaceInputType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HumanInterfaceInputType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4409, 1)
    _Documentation = None
HumanInterfaceInputType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=HumanInterfaceInputType, enum_prefix=None)
HumanInterfaceInputType.buttons = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='buttons', tag='buttons')
HumanInterfaceInputType.dial = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='dial', tag='dial')
HumanInterfaceInputType.handwriting_recognition = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='handwriting_recognition', tag='handwriting_recognition')
HumanInterfaceInputType.keyboard = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='keyboard', tag='keyboard')
HumanInterfaceInputType.keypad = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='keypad', tag='keypad')
HumanInterfaceInputType.keypad_stroke = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='keypad_stroke', tag='keypad_stroke')
HumanInterfaceInputType.keypad_stroke_ = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='keypad_stroke', tag='keypad_stroke_')
HumanInterfaceInputType.microphone = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='microphone', tag='microphone')
HumanInterfaceInputType.touch_screen = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='touch_screen', tag='touch_screen')
HumanInterfaceInputType.touch_screen_stylus_pen = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='touch_screen_stylus_pen', tag='touch_screen_stylus_pen')
HumanInterfaceInputType.trackpoint_pointing_device = HumanInterfaceInputType._CF_enumeration.addEnumeration(unicode_value='trackpoint_pointing_device', tag='trackpoint_pointing_device')
HumanInterfaceInputType._InitializeFacetMap(HumanInterfaceInputType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'HumanInterfaceInputType', HumanInterfaceInputType)

# Atomic simple type: HumanInterfaceOutputType
class HumanInterfaceOutputType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HumanInterfaceOutputType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4424, 1)
    _Documentation = None
HumanInterfaceOutputType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=HumanInterfaceOutputType, enum_prefix=None)
HumanInterfaceOutputType.screen = HumanInterfaceOutputType._CF_enumeration.addEnumeration(unicode_value='screen', tag='screen')
HumanInterfaceOutputType.speaker = HumanInterfaceOutputType._CF_enumeration.addEnumeration(unicode_value='speaker', tag='speaker')
HumanInterfaceOutputType._InitializeFacetMap(HumanInterfaceOutputType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'HumanInterfaceOutputType', HumanInterfaceOutputType)

# Atomic simple type: BluRayRegionType
class BluRayRegionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BluRayRegionType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4430, 1)
    _Documentation = None
BluRayRegionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BluRayRegionType, enum_prefix=None)
BluRayRegionType.region_a = BluRayRegionType._CF_enumeration.addEnumeration(unicode_value='region_a', tag='region_a')
BluRayRegionType.region_b = BluRayRegionType._CF_enumeration.addEnumeration(unicode_value='region_b', tag='region_b')
BluRayRegionType.region_c = BluRayRegionType._CF_enumeration.addEnumeration(unicode_value='region_c', tag='region_c')
BluRayRegionType.region_free = BluRayRegionType._CF_enumeration.addEnumeration(unicode_value='region_free', tag='region_free')
BluRayRegionType._InitializeFacetMap(BluRayRegionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BluRayRegionType', BluRayRegionType)

# Atomic simple type: VinylRecordDetailsType
class VinylRecordDetailsType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VinylRecordDetailsType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4438, 1)
    _Documentation = None
VinylRecordDetailsType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VinylRecordDetailsType, enum_prefix=None)
VinylRecordDetailsType.lp = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='lp', tag='lp')
VinylRecordDetailsType.n12_single = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='12_single', tag='n12_single')
VinylRecordDetailsType.n45 = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='45', tag='n45')
VinylRecordDetailsType.ep = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='ep', tag='ep')
VinylRecordDetailsType.n78 = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='78', tag='n78')
VinylRecordDetailsType.other = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='other', tag='other')
VinylRecordDetailsType.unknown = VinylRecordDetailsType._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
VinylRecordDetailsType._InitializeFacetMap(VinylRecordDetailsType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VinylRecordDetailsType', VinylRecordDetailsType)

# Atomic simple type: SweetnessAtHarvestUnitOfMeasure
class SweetnessAtHarvestUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SweetnessAtHarvestUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4456, 1)
    _Documentation = None
SweetnessAtHarvestUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=SweetnessAtHarvestUnitOfMeasure, enum_prefix=None)
SweetnessAtHarvestUnitOfMeasure.brix = SweetnessAtHarvestUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='brix', tag='brix')
SweetnessAtHarvestUnitOfMeasure._InitializeFacetMap(SweetnessAtHarvestUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'SweetnessAtHarvestUnitOfMeasure', SweetnessAtHarvestUnitOfMeasure)

# Atomic simple type: VineyardYieldUnitOfMeasure
class VineyardYieldUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VineyardYieldUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4461, 1)
    _Documentation = None
VineyardYieldUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VineyardYieldUnitOfMeasure, enum_prefix=None)
VineyardYieldUnitOfMeasure.tons = VineyardYieldUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='tons', tag='tons')
VineyardYieldUnitOfMeasure._InitializeFacetMap(VineyardYieldUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VineyardYieldUnitOfMeasure', VineyardYieldUnitOfMeasure)

# Atomic simple type: AllergenInformationType
class AllergenInformationType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AllergenInformationType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4466, 1)
    _Documentation = None
AllergenInformationType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AllergenInformationType, enum_prefix=None)
AllergenInformationType.abalone = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='abalone', tag='abalone')
AllergenInformationType.abalone_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='abalone_free', tag='abalone_free')
AllergenInformationType.amberjack = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='amberjack', tag='amberjack')
AllergenInformationType.amberjack_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='amberjack_free', tag='amberjack_free')
AllergenInformationType.apple = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='apple', tag='apple')
AllergenInformationType.apple_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='apple_free', tag='apple_free')
AllergenInformationType.banana = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='banana', tag='banana')
AllergenInformationType.banana_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='banana_free', tag='banana_free')
AllergenInformationType.barley = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='barley', tag='barley')
AllergenInformationType.barley_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='barley_free', tag='barley_free')
AllergenInformationType.beef = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='beef', tag='beef')
AllergenInformationType.beef_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='beef_free', tag='beef_free')
AllergenInformationType.buckwheat = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='buckwheat', tag='buckwheat')
AllergenInformationType.buckwheat_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='buckwheat_free', tag='buckwheat_free')
AllergenInformationType.celery = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='celery', tag='celery')
AllergenInformationType.celery_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='celery_free', tag='celery_free')
AllergenInformationType.chicken_meat = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='chicken_meat', tag='chicken_meat')
AllergenInformationType.chicken_meat_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='chicken_meat_free', tag='chicken_meat_free')
AllergenInformationType.codfish = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='codfish', tag='codfish')
AllergenInformationType.codfish_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='codfish_free', tag='codfish_free')
AllergenInformationType.crab = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='crab', tag='crab')
AllergenInformationType.crab_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='crab_free', tag='crab_free')
AllergenInformationType.dairy = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='dairy', tag='dairy')
AllergenInformationType.dairy_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='dairy_free', tag='dairy_free')
AllergenInformationType.eggs = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='eggs', tag='eggs')
AllergenInformationType.egg_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='egg_free', tag='egg_free')
AllergenInformationType.fish = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='fish', tag='fish')
AllergenInformationType.fish_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='fish_free', tag='fish_free')
AllergenInformationType.gelatin = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='gelatin', tag='gelatin')
AllergenInformationType.gelatin_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='gelatin_free', tag='gelatin_free')
AllergenInformationType.gluten = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='gluten', tag='gluten')
AllergenInformationType.gluten_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='gluten_free', tag='gluten_free')
AllergenInformationType.kiwi = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='kiwi', tag='kiwi')
AllergenInformationType.kiwi_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='kiwi_free', tag='kiwi_free')
AllergenInformationType.mackerel = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='mackerel', tag='mackerel')
AllergenInformationType.mackerel_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='mackerel_free', tag='mackerel_free')
AllergenInformationType.melon = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='melon', tag='melon')
AllergenInformationType.melon_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='melon_free', tag='melon_free')
AllergenInformationType.mushroom = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='mushroom', tag='mushroom')
AllergenInformationType.mushroom_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='mushroom_free', tag='mushroom_free')
AllergenInformationType.octopus = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='octopus', tag='octopus')
AllergenInformationType.octopus_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='octopus_free', tag='octopus_free')
AllergenInformationType.orange = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='orange', tag='orange')
AllergenInformationType.orange_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='orange_free', tag='orange_free')
AllergenInformationType.peach = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='peach', tag='peach')
AllergenInformationType.peach_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='peach_free', tag='peach_free')
AllergenInformationType.peanuts = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='peanuts', tag='peanuts')
AllergenInformationType.peanut_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='peanut_free', tag='peanut_free')
AllergenInformationType.pork = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='pork', tag='pork')
AllergenInformationType.pork_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='pork_free', tag='pork_free')
AllergenInformationType.salmon = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='salmon', tag='salmon')
AllergenInformationType.salmon_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='salmon_free', tag='salmon_free')
AllergenInformationType.salmon_roe = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='salmon_roe', tag='salmon_roe')
AllergenInformationType.salmon_roe_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='salmon_roe_free', tag='salmon_roe_free')
AllergenInformationType.scad = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='scad', tag='scad')
AllergenInformationType.scad_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='scad_free', tag='scad_free')
AllergenInformationType.scallop = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='scallop', tag='scallop')
AllergenInformationType.scallop_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='scallop_free', tag='scallop_free')
AllergenInformationType.sesame_seeds = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='sesame_seeds', tag='sesame_seeds')
AllergenInformationType.sesame_seeds_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='sesame_seeds_free', tag='sesame_seeds_free')
AllergenInformationType.shellfish = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='shellfish', tag='shellfish')
AllergenInformationType.shellfish_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='shellfish_free', tag='shellfish_free')
AllergenInformationType.shrimp = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='shrimp', tag='shrimp')
AllergenInformationType.shrimp_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='shrimp_free', tag='shrimp_free')
AllergenInformationType.soy = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='soy', tag='soy')
AllergenInformationType.soy_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='soy_free', tag='soy_free')
AllergenInformationType.squid = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='squid', tag='squid')
AllergenInformationType.squid_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='squid_free', tag='squid_free')
AllergenInformationType.tree_nuts = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='tree_nuts', tag='tree_nuts')
AllergenInformationType.tree_nut_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='tree_nut_free', tag='tree_nut_free')
AllergenInformationType.tuna = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='tuna', tag='tuna')
AllergenInformationType.tuna_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='tuna_free', tag='tuna_free')
AllergenInformationType.walnut = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='walnut', tag='walnut')
AllergenInformationType.walnut_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='walnut_free', tag='walnut_free')
AllergenInformationType.yam = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='yam', tag='yam')
AllergenInformationType.yam_free = AllergenInformationType._CF_enumeration.addEnumeration(unicode_value='yam_free', tag='yam_free')
AllergenInformationType._InitializeFacetMap(AllergenInformationType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AllergenInformationType', AllergenInformationType)

# Atomic simple type: CustomerReturnPolicyType
class CustomerReturnPolicyType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CustomerReturnPolicyType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4546, 1)
    _Documentation = None
CustomerReturnPolicyType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=CustomerReturnPolicyType, enum_prefix=None)
CustomerReturnPolicyType.collectible = CustomerReturnPolicyType._CF_enumeration.addEnumeration(unicode_value='collectible', tag='collectible')
CustomerReturnPolicyType.restocking_fee = CustomerReturnPolicyType._CF_enumeration.addEnumeration(unicode_value='restocking_fee', tag='restocking_fee')
CustomerReturnPolicyType.standard = CustomerReturnPolicyType._CF_enumeration.addEnumeration(unicode_value='standard', tag='standard')
CustomerReturnPolicyType.non_returnable = CustomerReturnPolicyType._CF_enumeration.addEnumeration(unicode_value='non_returnable', tag='non_returnable')
CustomerReturnPolicyType.seasonal = CustomerReturnPolicyType._CF_enumeration.addEnumeration(unicode_value='seasonal', tag='seasonal')
CustomerReturnPolicyType.unknown = CustomerReturnPolicyType._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
CustomerReturnPolicyType._InitializeFacetMap(CustomerReturnPolicyType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'CustomerReturnPolicyType', CustomerReturnPolicyType)

# Atomic simple type: AlcoholContentUnitOfMeasure
class AlcoholContentUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AlcoholContentUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4556, 1)
    _Documentation = None
AlcoholContentUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AlcoholContentUnitOfMeasure, enum_prefix=None)
AlcoholContentUnitOfMeasure.percent_by_volume = AlcoholContentUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='percent_by_volume', tag='percent_by_volume')
AlcoholContentUnitOfMeasure.percent_by_weight = AlcoholContentUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='percent_by_weight', tag='percent_by_weight')
AlcoholContentUnitOfMeasure.unit_of_alcohol = AlcoholContentUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='unit_of_alcohol', tag='unit_of_alcohol')
AlcoholContentUnitOfMeasure._InitializeFacetMap(AlcoholContentUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AlcoholContentUnitOfMeasure', AlcoholContentUnitOfMeasure)

# Atomic simple type: ComputerPlatformValues
class ComputerPlatformValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ComputerPlatformValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4563, 1)
    _Documentation = None
ComputerPlatformValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ComputerPlatformValues, enum_prefix=None)
ComputerPlatformValues.game_boy_advance = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='game_boy_advance', tag='game_boy_advance')
ComputerPlatformValues.gameboy = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='gameboy', tag='gameboy')
ComputerPlatformValues.gameboy_color = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='gameboy_color', tag='gameboy_color')
ComputerPlatformValues.gamecube = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='gamecube', tag='gamecube')
ComputerPlatformValues.gizmondo = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='gizmondo', tag='gizmondo')
ComputerPlatformValues.linux = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='linux', tag='linux')
ComputerPlatformValues.macintosh = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='macintosh', tag='macintosh')
ComputerPlatformValues.n_gage = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='n_gage', tag='n_gage')
ComputerPlatformValues.nintendo_ds = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='nintendo_ds', tag='nintendo_ds')
ComputerPlatformValues.nintendo_NES = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='nintendo_NES', tag='nintendo_NES')
ComputerPlatformValues.nintendo_super_NES = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='nintendo_super_NES', tag='nintendo_super_NES')
ComputerPlatformValues.nintendo_wii = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='nintendo_wii', tag='nintendo_wii')
ComputerPlatformValues.nintendo64 = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='nintendo64', tag='nintendo64')
ComputerPlatformValues.palm = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='palm', tag='palm')
ComputerPlatformValues.playstation = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='playstation', tag='playstation')
ComputerPlatformValues.playstation_2 = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='playstation_2', tag='playstation_2')
ComputerPlatformValues.playstation_vita = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='playstation_vita', tag='playstation_vita')
ComputerPlatformValues.pocket_pc = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='pocket_pc', tag='pocket_pc')
ComputerPlatformValues.powermac = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='powermac', tag='powermac')
ComputerPlatformValues.sega_saturn = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='sega_saturn', tag='sega_saturn')
ComputerPlatformValues.sony_psp = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='sony_psp', tag='sony_psp')
ComputerPlatformValues.super_nintendo = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='super_nintendo', tag='super_nintendo')
ComputerPlatformValues.unix = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='unix', tag='unix')
ComputerPlatformValues.windows = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='windows', tag='windows')
ComputerPlatformValues.xbox = ComputerPlatformValues._CF_enumeration.addEnumeration(unicode_value='xbox', tag='xbox')
ComputerPlatformValues._InitializeFacetMap(ComputerPlatformValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ComputerPlatformValues', ComputerPlatformValues)

# Atomic simple type: NeckSizeUnitOfMeasure
class NeckSizeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NeckSizeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4628, 1)
    _Documentation = None
NeckSizeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=NeckSizeUnitOfMeasure, enum_prefix=None)
NeckSizeUnitOfMeasure.CM = NeckSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='CM', tag='CM')
NeckSizeUnitOfMeasure.IN = NeckSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='IN', tag='IN')
NeckSizeUnitOfMeasure.MM = NeckSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='MM', tag='MM')
NeckSizeUnitOfMeasure.M = NeckSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='M', tag='M')
NeckSizeUnitOfMeasure.FT = NeckSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='FT', tag='FT')
NeckSizeUnitOfMeasure._InitializeFacetMap(NeckSizeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'NeckSizeUnitOfMeasure', NeckSizeUnitOfMeasure)

# Atomic simple type: CycleLengthUnitOfMeasure
class CycleLengthUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CycleLengthUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4644, 1)
    _Documentation = None
CycleLengthUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=CycleLengthUnitOfMeasure, enum_prefix=None)
CycleLengthUnitOfMeasure.CM = CycleLengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='CM', tag='CM')
CycleLengthUnitOfMeasure.IN = CycleLengthUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='IN', tag='IN')
CycleLengthUnitOfMeasure._InitializeFacetMap(CycleLengthUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'CycleLengthUnitOfMeasure', CycleLengthUnitOfMeasure)

# Atomic simple type: BootSizeUnitOfMeasure
class BootSizeUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BootSizeUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4657, 1)
    _Documentation = None
BootSizeUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=BootSizeUnitOfMeasure, enum_prefix=None)
BootSizeUnitOfMeasure.adult_us = BootSizeUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='adult_us', tag='adult_us')
BootSizeUnitOfMeasure._InitializeFacetMap(BootSizeUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'BootSizeUnitOfMeasure', BootSizeUnitOfMeasure)

# Atomic simple type: LithiumBatteryPackagingType
class LithiumBatteryPackagingType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LithiumBatteryPackagingType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4662, 1)
    _Documentation = None
LithiumBatteryPackagingType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LithiumBatteryPackagingType, enum_prefix=None)
LithiumBatteryPackagingType.batteries_contained_in_equipment = LithiumBatteryPackagingType._CF_enumeration.addEnumeration(unicode_value='batteries_contained_in_equipment', tag='batteries_contained_in_equipment')
LithiumBatteryPackagingType.batteries_only = LithiumBatteryPackagingType._CF_enumeration.addEnumeration(unicode_value='batteries_only', tag='batteries_only')
LithiumBatteryPackagingType.batteries_packed_with_equipment = LithiumBatteryPackagingType._CF_enumeration.addEnumeration(unicode_value='batteries_packed_with_equipment', tag='batteries_packed_with_equipment')
LithiumBatteryPackagingType._InitializeFacetMap(LithiumBatteryPackagingType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LithiumBatteryPackagingType', LithiumBatteryPackagingType)

# Atomic simple type: EnergyLabelEfficiencyClass
class EnergyLabelEfficiencyClass (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnergyLabelEfficiencyClass')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4669, 1)
    _Documentation = None
EnergyLabelEfficiencyClass._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=EnergyLabelEfficiencyClass, enum_prefix=None)
EnergyLabelEfficiencyClass.a = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='a', tag='a')
EnergyLabelEfficiencyClass.b = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='b', tag='b')
EnergyLabelEfficiencyClass.c = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='c', tag='c')
EnergyLabelEfficiencyClass.d = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='d', tag='d')
EnergyLabelEfficiencyClass.e = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='e', tag='e')
EnergyLabelEfficiencyClass.f = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='f', tag='f')
EnergyLabelEfficiencyClass.g = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='g', tag='g')
EnergyLabelEfficiencyClass.a_plus = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='a_plus', tag='a_plus')
EnergyLabelEfficiencyClass.a_plus_plus = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='a_plus_plus', tag='a_plus_plus')
EnergyLabelEfficiencyClass.a_plus_plus_plus = EnergyLabelEfficiencyClass._CF_enumeration.addEnumeration(unicode_value='a_plus_plus_plus', tag='a_plus_plus_plus')
EnergyLabelEfficiencyClass._InitializeFacetMap(EnergyLabelEfficiencyClass._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'EnergyLabelEfficiencyClass', EnergyLabelEfficiencyClass)

# Atomic simple type: DistributionDesignationValues
class DistributionDesignationValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DistributionDesignationValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4683, 1)
    _Documentation = None
DistributionDesignationValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DistributionDesignationValues, enum_prefix=None)
DistributionDesignationValues.jp_parallel_import = DistributionDesignationValues._CF_enumeration.addEnumeration(unicode_value='jp_parallel_import', tag='jp_parallel_import')
DistributionDesignationValues._InitializeFacetMap(DistributionDesignationValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DistributionDesignationValues', DistributionDesignationValues)

# Atomic simple type: DensityUnitOfMeasure
class DensityUnitOfMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DensityUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4695, 1)
    _Documentation = None
DensityUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DensityUnitOfMeasure, enum_prefix=None)
DensityUnitOfMeasure.grams_per_square_meter = DensityUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='grams_per_square_meter', tag='grams_per_square_meter')
DensityUnitOfMeasure._InitializeFacetMap(DensityUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DensityUnitOfMeasure', DensityUnitOfMeasure)

# Atomic simple type: CapacityUnitMeasure
class CapacityUnitMeasure (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CapacityUnitMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4707, 1)
    _Documentation = None
CapacityUnitMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=CapacityUnitMeasure, enum_prefix=None)
CapacityUnitMeasure.cubic_centimeters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_centimeters', tag='cubic_centimeters')
CapacityUnitMeasure.cubic_feet = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_feet', tag='cubic_feet')
CapacityUnitMeasure.cubic_inches = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_inches', tag='cubic_inches')
CapacityUnitMeasure.cubic_meters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_meters', tag='cubic_meters')
CapacityUnitMeasure.cubic_yards = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='cubic_yards', tag='cubic_yards')
CapacityUnitMeasure.cups = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='cups', tag='cups')
CapacityUnitMeasure.fluid_ounces = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='fluid_ounces', tag='fluid_ounces')
CapacityUnitMeasure.gallons = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='gallons', tag='gallons')
CapacityUnitMeasure.imperial_gallons = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='imperial_gallons', tag='imperial_gallons')
CapacityUnitMeasure.liters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='liters', tag='liters')
CapacityUnitMeasure.milliliters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='milliliters', tag='milliliters')
CapacityUnitMeasure.ounces = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='ounces', tag='ounces')
CapacityUnitMeasure.pints = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='pints', tag='pints')
CapacityUnitMeasure.quarts = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='quarts', tag='quarts')
CapacityUnitMeasure.deciliters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='deciliters', tag='deciliters')
CapacityUnitMeasure.centiliters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='centiliters', tag='centiliters')
CapacityUnitMeasure.microliters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='microliters', tag='microliters')
CapacityUnitMeasure.nanoliters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='nanoliters', tag='nanoliters')
CapacityUnitMeasure.picoliters = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='picoliters', tag='picoliters')
CapacityUnitMeasure.grams = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='grams', tag='grams')
CapacityUnitMeasure.kilograms = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='kilograms', tag='kilograms')
CapacityUnitMeasure.ounces_ = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='ounces', tag='ounces_')
CapacityUnitMeasure.pounds = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='pounds', tag='pounds')
CapacityUnitMeasure.milligrams = CapacityUnitMeasure._CF_enumeration.addEnumeration(unicode_value='milligrams', tag='milligrams')
CapacityUnitMeasure._InitializeFacetMap(CapacityUnitMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'CapacityUnitMeasure', CapacityUnitMeasure)

# Atomic simple type: Originality
class Originality (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Originality')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4735, 1)
    _Documentation = None
Originality._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=Originality, enum_prefix=None)
Originality.Original = Originality._CF_enumeration.addEnumeration(unicode_value='Original', tag='Original')
Originality.Original_Limited_Edition = Originality._CF_enumeration.addEnumeration(unicode_value='Original Limited Edition', tag='Original_Limited_Edition')
Originality.Reproduced = Originality._CF_enumeration.addEnumeration(unicode_value='Reproduced', tag='Reproduced')
Originality.Reproduced_Limited_Edition = Originality._CF_enumeration.addEnumeration(unicode_value='Reproduced Limited Edition', tag='Reproduced_Limited_Edition')
Originality.Replica = Originality._CF_enumeration.addEnumeration(unicode_value='Replica', tag='Replica')
Originality.Replica_Limited_Edition = Originality._CF_enumeration.addEnumeration(unicode_value='Replica Limited Edition', tag='Replica_Limited_Edition')
Originality.Limited_Edition = Originality._CF_enumeration.addEnumeration(unicode_value='Limited Edition', tag='Limited_Edition')
Originality.Manufactured = Originality._CF_enumeration.addEnumeration(unicode_value='Manufactured', tag='Manufactured')
Originality.Licensed = Originality._CF_enumeration.addEnumeration(unicode_value='Licensed', tag='Licensed')
Originality.Vintage = Originality._CF_enumeration.addEnumeration(unicode_value='Vintage', tag='Vintage')
Originality._InitializeFacetMap(Originality._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'Originality', Originality)

# Atomic simple type: ServingUnit
class ServingUnit (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ServingUnit')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4756, 8)
    _Documentation = None
ServingUnit._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ServingUnit, enum_prefix=None)
ServingUnit.percent_fda = ServingUnit._CF_enumeration.addEnumeration(unicode_value='percent-fda', tag='percent_fda')
ServingUnit.mg = ServingUnit._CF_enumeration.addEnumeration(unicode_value='mg', tag='mg')
ServingUnit.gr = ServingUnit._CF_enumeration.addEnumeration(unicode_value='gr', tag='gr')
ServingUnit.ml = ServingUnit._CF_enumeration.addEnumeration(unicode_value='ml', tag='ml')
ServingUnit.grams = ServingUnit._CF_enumeration.addEnumeration(unicode_value='grams', tag='grams')
ServingUnit.milligrams = ServingUnit._CF_enumeration.addEnumeration(unicode_value='milligrams', tag='milligrams')
ServingUnit.milliliters = ServingUnit._CF_enumeration.addEnumeration(unicode_value='milliliters', tag='milliliters')
ServingUnit._InitializeFacetMap(ServingUnit._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ServingUnit', ServingUnit)

# Atomic simple type: BinaryInteger
class BinaryInteger (pyxb.binding.datatypes.integer):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BinaryInteger')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4767, 2)
    _Documentation = None
BinaryInteger._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=BinaryInteger, value=pyxb.binding.datatypes.integer(0))
BinaryInteger._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=BinaryInteger, value=pyxb.binding.datatypes.integer(1))
BinaryInteger._InitializeFacetMap(BinaryInteger._CF_minInclusive,
   BinaryInteger._CF_maxInclusive)
Namespace.addCategoryObject('typeBinding', 'BinaryInteger', BinaryInteger)

# Atomic simple type: OrganizationTaxRoles
class OrganizationTaxRoles (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OrganizationTaxRoles')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4773, 2)
    _Documentation = None
OrganizationTaxRoles._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=OrganizationTaxRoles, enum_prefix=None)
OrganizationTaxRoles.doctor = OrganizationTaxRoles._CF_enumeration.addEnumeration(unicode_value='doctor', tag='doctor')
OrganizationTaxRoles.dentist = OrganizationTaxRoles._CF_enumeration.addEnumeration(unicode_value='dentist', tag='dentist')
OrganizationTaxRoles.hospital = OrganizationTaxRoles._CF_enumeration.addEnumeration(unicode_value='hospital', tag='hospital')
OrganizationTaxRoles.clinic = OrganizationTaxRoles._CF_enumeration.addEnumeration(unicode_value='clinic', tag='clinic')
OrganizationTaxRoles._InitializeFacetMap(OrganizationTaxRoles._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'OrganizationTaxRoles', OrganizationTaxRoles)

# Atomic simple type: B2bQuantityPriceTypeValues
class B2bQuantityPriceTypeValues (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'B2bQuantityPriceTypeValues')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4781, 2)
    _Documentation = None
B2bQuantityPriceTypeValues._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=B2bQuantityPriceTypeValues, enum_prefix=None)
B2bQuantityPriceTypeValues.fixed = B2bQuantityPriceTypeValues._CF_enumeration.addEnumeration(unicode_value='fixed', tag='fixed')
B2bQuantityPriceTypeValues.percent = B2bQuantityPriceTypeValues._CF_enumeration.addEnumeration(unicode_value='percent', tag='percent')
B2bQuantityPriceTypeValues._InitializeFacetMap(B2bQuantityPriceTypeValues._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'B2bQuantityPriceTypeValues', B2bQuantityPriceTypeValues)

# Atomic simple type: [anonymous]
class STD_ANON_27 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 27, 5)
    _Documentation = None
STD_ANON_27._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_27, enum_prefix=None)
STD_ANON_27.batteries_contained_in_equipment = STD_ANON_27._CF_enumeration.addEnumeration(unicode_value='batteries_contained_in_equipment', tag='batteries_contained_in_equipment')
STD_ANON_27.batteries_only = STD_ANON_27._CF_enumeration.addEnumeration(unicode_value='batteries_only', tag='batteries_only')
STD_ANON_27.batteries_packed_with_equipment = STD_ANON_27._CF_enumeration.addEnumeration(unicode_value='batteries_packed_with_equipment', tag='batteries_packed_with_equipment')
STD_ANON_27._InitializeFacetMap(STD_ANON_27._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_28 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 57, 8)
    _Documentation = None
STD_ANON_28._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_28, enum_prefix=None)
STD_ANON_28.parent = STD_ANON_28._CF_enumeration.addEnumeration(unicode_value='parent', tag='parent')
STD_ANON_28.child = STD_ANON_28._CF_enumeration.addEnumeration(unicode_value='child', tag='child')
STD_ANON_28._InitializeFacetMap(STD_ANON_28._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_29 (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 65, 8)
    _Documentation = None
STD_ANON_29._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_29, enum_prefix=None)
STD_ANON_29.Size = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Size', tag='Size')
STD_ANON_29.Capacity = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Capacity', tag='Capacity')
STD_ANON_29.Color = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Color', tag='Color')
STD_ANON_29.Size_Color = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Size-Color', tag='Size_Color')
STD_ANON_29.Scent = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Scent', tag='Scent')
STD_ANON_29.Size_Scent = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Size-Scent', tag='Size_Scent')
STD_ANON_29.PatternName = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='PatternName', tag='PatternName')
STD_ANON_29.Size_UnitCount = STD_ANON_29._CF_enumeration.addEnumeration(unicode_value='Size-UnitCount', tag='Size_UnitCount')
STD_ANON_29._InitializeFacetMap(STD_ANON_29._CF_enumeration)

# Atomic simple type: [anonymous]
class STD_ANON_30 (pyxb.binding.datatypes.normalizedString):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 124, 5)
    _Documentation = None
STD_ANON_30._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(1500))
STD_ANON_30._InitializeFacetMap(STD_ANON_30._CF_maxLength)

# Atomic simple type: [anonymous]
class STD_ANON_31 (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 160, 5)
    _Documentation = None
STD_ANON_31._CF_length = pyxb.binding.facets.CF_length(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_31._InitializeFacetMap(STD_ANON_31._CF_length)

# Atomic simple type: [anonymous]
class STD_ANON_32 (StringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 199, 8)
    _Documentation = None
STD_ANON_32._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_32, enum_prefix=None)
STD_ANON_32.battery_type_23A = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_2/3A', tag='battery_type_23A')
STD_ANON_32.battery_type_43A = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_4/3A', tag='battery_type_43A')
STD_ANON_32.battery_type_45A = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_4/5A', tag='battery_type_45A')
STD_ANON_32.battery_type_9v = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_9v', tag='battery_type_9v')
STD_ANON_32.battery_type_12v = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_12v', tag='battery_type_12v')
STD_ANON_32.battery_type_a = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_a', tag='battery_type_a')
STD_ANON_32.battery_type_a76 = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_a76', tag='battery_type_a76')
STD_ANON_32.battery_type_aa = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_aa', tag='battery_type_aa')
STD_ANON_32.battery_type_aaa = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_aaa', tag='battery_type_aaa')
STD_ANON_32.battery_type_aaaa = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_aaaa', tag='battery_type_aaaa')
STD_ANON_32.battery_type_c = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_c', tag='battery_type_c')
STD_ANON_32.battery_type_cr123a = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_cr123a', tag='battery_type_cr123a')
STD_ANON_32.battery_type_cr2 = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_cr2', tag='battery_type_cr2')
STD_ANON_32.battery_type_cr5 = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_cr5', tag='battery_type_cr5')
STD_ANON_32.battery_type_d = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_d', tag='battery_type_d')
STD_ANON_32.battery_type_lithium_ion = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_lithium_ion', tag='battery_type_lithium_ion')
STD_ANON_32.battery_type_lithium_metal = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_lithium_metal', tag='battery_type_lithium_metal')
STD_ANON_32.battery_type_L_SC = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_L-SC', tag='battery_type_L_SC')
STD_ANON_32.battery_type_p76 = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_p76', tag='battery_type_p76')
STD_ANON_32.battery_type_product_specific = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_product_specific', tag='battery_type_product_specific')
STD_ANON_32.battery_type_SC = STD_ANON_32._CF_enumeration.addEnumeration(unicode_value='battery_type_SC', tag='battery_type_SC')
STD_ANON_32._InitializeFacetMap(STD_ANON_32._CF_enumeration)

# Union simple type: GraduationIntervalUnitOfMeasure
# superclasses pyxb.binding.datatypes.anySimpleType
class GraduationIntervalUnitOfMeasure (pyxb.binding.basis.STD_union):

    """Simple type that is a union of LengthUnitOfMeasure, WeightUnitOfMeasure, VolumeUnitOfMeasure."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'GraduationIntervalUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3205, 1)
    _Documentation = None

    _MemberTypes = ( LengthUnitOfMeasure, WeightUnitOfMeasure, VolumeUnitOfMeasure, )
GraduationIntervalUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=GraduationIntervalUnitOfMeasure)
GraduationIntervalUnitOfMeasure._CF_pattern = pyxb.binding.facets.CF_pattern()
GraduationIntervalUnitOfMeasure.MM = 'MM'         # originally LengthUnitOfMeasure.MM
GraduationIntervalUnitOfMeasure.CM = 'CM'         # originally LengthUnitOfMeasure.CM
GraduationIntervalUnitOfMeasure.M = 'M'           # originally LengthUnitOfMeasure.M
GraduationIntervalUnitOfMeasure.IN = 'IN'         # originally LengthUnitOfMeasure.IN
GraduationIntervalUnitOfMeasure.FT = 'FT'         # originally LengthUnitOfMeasure.FT
GraduationIntervalUnitOfMeasure.inches = 'inches' # originally LengthUnitOfMeasure.inches
GraduationIntervalUnitOfMeasure.feet = 'feet'     # originally LengthUnitOfMeasure.feet
GraduationIntervalUnitOfMeasure.meters = 'meters' # originally LengthUnitOfMeasure.meters
GraduationIntervalUnitOfMeasure.decimeters = 'decimeters'# originally LengthUnitOfMeasure.decimeters
GraduationIntervalUnitOfMeasure.centimeters = 'centimeters'# originally LengthUnitOfMeasure.centimeters
GraduationIntervalUnitOfMeasure.millimeters = 'millimeters'# originally LengthUnitOfMeasure.millimeters
GraduationIntervalUnitOfMeasure.micrometers = 'micrometers'# originally LengthUnitOfMeasure.micrometers
GraduationIntervalUnitOfMeasure.nanometers = 'nanometers'# originally LengthUnitOfMeasure.nanometers
GraduationIntervalUnitOfMeasure.picometers = 'picometers'# originally LengthUnitOfMeasure.picometers
GraduationIntervalUnitOfMeasure.GR = 'GR'         # originally WeightUnitOfMeasure.GR
GraduationIntervalUnitOfMeasure.KG = 'KG'         # originally WeightUnitOfMeasure.KG
GraduationIntervalUnitOfMeasure.OZ = 'OZ'         # originally WeightUnitOfMeasure.OZ
GraduationIntervalUnitOfMeasure.LB = 'LB'         # originally WeightUnitOfMeasure.LB
GraduationIntervalUnitOfMeasure.MG = 'MG'         # originally WeightUnitOfMeasure.MG
GraduationIntervalUnitOfMeasure.cubic_cm = 'cubic-cm'# originally VolumeUnitOfMeasure.cubic_cm
GraduationIntervalUnitOfMeasure.cubic_ft = 'cubic-ft'# originally VolumeUnitOfMeasure.cubic_ft
GraduationIntervalUnitOfMeasure.cubic_in = 'cubic-in'# originally VolumeUnitOfMeasure.cubic_in
GraduationIntervalUnitOfMeasure.cubic_m = 'cubic-m'# originally VolumeUnitOfMeasure.cubic_m
GraduationIntervalUnitOfMeasure.cubic_yd = 'cubic-yd'# originally VolumeUnitOfMeasure.cubic_yd
GraduationIntervalUnitOfMeasure.cup = 'cup'       # originally VolumeUnitOfMeasure.cup
GraduationIntervalUnitOfMeasure.fluid_oz = 'fluid-oz'# originally VolumeUnitOfMeasure.fluid_oz
GraduationIntervalUnitOfMeasure.gallon = 'gallon' # originally VolumeUnitOfMeasure.gallon
GraduationIntervalUnitOfMeasure.liter = 'liter'   # originally VolumeUnitOfMeasure.liter
GraduationIntervalUnitOfMeasure.milliliter = 'milliliter'# originally VolumeUnitOfMeasure.milliliter
GraduationIntervalUnitOfMeasure.ounce = 'ounce'   # originally VolumeUnitOfMeasure.ounce
GraduationIntervalUnitOfMeasure.pint = 'pint'     # originally VolumeUnitOfMeasure.pint
GraduationIntervalUnitOfMeasure.quart = 'quart'   # originally VolumeUnitOfMeasure.quart
GraduationIntervalUnitOfMeasure.liters = 'liters' # originally VolumeUnitOfMeasure.liters
GraduationIntervalUnitOfMeasure.deciliters = 'deciliters'# originally VolumeUnitOfMeasure.deciliters
GraduationIntervalUnitOfMeasure.centiliters = 'centiliters'# originally VolumeUnitOfMeasure.centiliters
GraduationIntervalUnitOfMeasure.milliliters = 'milliliters'# originally VolumeUnitOfMeasure.milliliters
GraduationIntervalUnitOfMeasure.microliters = 'microliters'# originally VolumeUnitOfMeasure.microliters
GraduationIntervalUnitOfMeasure.nanoliters = 'nanoliters'# originally VolumeUnitOfMeasure.nanoliters
GraduationIntervalUnitOfMeasure.picoliters = 'picoliters'# originally VolumeUnitOfMeasure.picoliters
GraduationIntervalUnitOfMeasure._InitializeFacetMap(GraduationIntervalUnitOfMeasure._CF_enumeration,
   GraduationIntervalUnitOfMeasure._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'GraduationIntervalUnitOfMeasure', GraduationIntervalUnitOfMeasure)

# Union simple type: VolumeAndVolumeRateUnitOfMeasure
# superclasses pyxb.binding.datatypes.anySimpleType
class VolumeAndVolumeRateUnitOfMeasure (pyxb.binding.basis.STD_union):

    """Simple type that is a union of VolumeUnitOfMeasure, VolumeRateUnitOfMeasure."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeAndVolumeRateUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3208, 1)
    _Documentation = None

    _MemberTypes = ( VolumeUnitOfMeasure, VolumeRateUnitOfMeasure, )
VolumeAndVolumeRateUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VolumeAndVolumeRateUnitOfMeasure)
VolumeAndVolumeRateUnitOfMeasure._CF_pattern = pyxb.binding.facets.CF_pattern()
VolumeAndVolumeRateUnitOfMeasure.cubic_cm = 'cubic-cm'# originally VolumeUnitOfMeasure.cubic_cm
VolumeAndVolumeRateUnitOfMeasure.cubic_ft = 'cubic-ft'# originally VolumeUnitOfMeasure.cubic_ft
VolumeAndVolumeRateUnitOfMeasure.cubic_in = 'cubic-in'# originally VolumeUnitOfMeasure.cubic_in
VolumeAndVolumeRateUnitOfMeasure.cubic_m = 'cubic-m'# originally VolumeUnitOfMeasure.cubic_m
VolumeAndVolumeRateUnitOfMeasure.cubic_yd = 'cubic-yd'# originally VolumeUnitOfMeasure.cubic_yd
VolumeAndVolumeRateUnitOfMeasure.cup = 'cup'      # originally VolumeUnitOfMeasure.cup
VolumeAndVolumeRateUnitOfMeasure.fluid_oz = 'fluid-oz'# originally VolumeUnitOfMeasure.fluid_oz
VolumeAndVolumeRateUnitOfMeasure.gallon = 'gallon'# originally VolumeUnitOfMeasure.gallon
VolumeAndVolumeRateUnitOfMeasure.liter = 'liter'  # originally VolumeUnitOfMeasure.liter
VolumeAndVolumeRateUnitOfMeasure.milliliter = 'milliliter'# originally VolumeUnitOfMeasure.milliliter
VolumeAndVolumeRateUnitOfMeasure.ounce = 'ounce'  # originally VolumeUnitOfMeasure.ounce
VolumeAndVolumeRateUnitOfMeasure.pint = 'pint'    # originally VolumeUnitOfMeasure.pint
VolumeAndVolumeRateUnitOfMeasure.quart = 'quart'  # originally VolumeUnitOfMeasure.quart
VolumeAndVolumeRateUnitOfMeasure.liters = 'liters'# originally VolumeUnitOfMeasure.liters
VolumeAndVolumeRateUnitOfMeasure.deciliters = 'deciliters'# originally VolumeUnitOfMeasure.deciliters
VolumeAndVolumeRateUnitOfMeasure.centiliters = 'centiliters'# originally VolumeUnitOfMeasure.centiliters
VolumeAndVolumeRateUnitOfMeasure.milliliters = 'milliliters'# originally VolumeUnitOfMeasure.milliliters
VolumeAndVolumeRateUnitOfMeasure.microliters = 'microliters'# originally VolumeUnitOfMeasure.microliters
VolumeAndVolumeRateUnitOfMeasure.nanoliters = 'nanoliters'# originally VolumeUnitOfMeasure.nanoliters
VolumeAndVolumeRateUnitOfMeasure.picoliters = 'picoliters'# originally VolumeUnitOfMeasure.picoliters
VolumeAndVolumeRateUnitOfMeasure.milliliters_per_second = 'milliliters per second'# originally VolumeRateUnitOfMeasure.milliliters_per_second
VolumeAndVolumeRateUnitOfMeasure.centiliters_per_second = 'centiliters per second'# originally VolumeRateUnitOfMeasure.centiliters_per_second
VolumeAndVolumeRateUnitOfMeasure.liters_per_second = 'liters per second'# originally VolumeRateUnitOfMeasure.liters_per_second
VolumeAndVolumeRateUnitOfMeasure.milliliters_per_minute = 'milliliters per minute'# originally VolumeRateUnitOfMeasure.milliliters_per_minute
VolumeAndVolumeRateUnitOfMeasure.liters_per_minute = 'liters per minute'# originally VolumeRateUnitOfMeasure.liters_per_minute
VolumeAndVolumeRateUnitOfMeasure.microliters_per_second = 'microliters per second'# originally VolumeRateUnitOfMeasure.microliters_per_second
VolumeAndVolumeRateUnitOfMeasure.nanoliters_per_second = 'nanoliters per second'# originally VolumeRateUnitOfMeasure.nanoliters_per_second
VolumeAndVolumeRateUnitOfMeasure.picoliters_per_second = 'picoliters per second'# originally VolumeRateUnitOfMeasure.picoliters_per_second
VolumeAndVolumeRateUnitOfMeasure.microliters_per_minute = 'microliters per minute'# originally VolumeRateUnitOfMeasure.microliters_per_minute
VolumeAndVolumeRateUnitOfMeasure.nanoliters_per_minute = 'nanoliters per minute'# originally VolumeRateUnitOfMeasure.nanoliters_per_minute
VolumeAndVolumeRateUnitOfMeasure.picoliters_per_minute = 'picoliters per minute'# originally VolumeRateUnitOfMeasure.picoliters_per_minute
VolumeAndVolumeRateUnitOfMeasure.gallons_per_day = 'gallons_per_day'# originally VolumeRateUnitOfMeasure.gallons_per_day
VolumeAndVolumeRateUnitOfMeasure.liters_per_day = 'liters_per_day'# originally VolumeRateUnitOfMeasure.liters_per_day
VolumeAndVolumeRateUnitOfMeasure.liters = 'liters'# originally VolumeRateUnitOfMeasure.liters
VolumeAndVolumeRateUnitOfMeasure._InitializeFacetMap(VolumeAndVolumeRateUnitOfMeasure._CF_enumeration,
   VolumeAndVolumeRateUnitOfMeasure._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'VolumeAndVolumeRateUnitOfMeasure', VolumeAndVolumeRateUnitOfMeasure)

# Atomic simple type: EnergyConsumptionUnitOfMeasure
class EnergyConsumptionUnitOfMeasure (String, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnergyConsumptionUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3340, 1)
    _Documentation = None
EnergyConsumptionUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=EnergyConsumptionUnitOfMeasure, enum_prefix=None)
EnergyConsumptionUnitOfMeasure.kilowatt_hours = EnergyConsumptionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilowatt_hours', tag='kilowatt_hours')
EnergyConsumptionUnitOfMeasure.btu = EnergyConsumptionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='btu', tag='btu')
EnergyConsumptionUnitOfMeasure.kilowatts = EnergyConsumptionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilowatts', tag='kilowatts')
EnergyConsumptionUnitOfMeasure.watt_hours = EnergyConsumptionUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='watt_hours', tag='watt_hours')
EnergyConsumptionUnitOfMeasure._InitializeFacetMap(EnergyConsumptionUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'EnergyConsumptionUnitOfMeasure', EnergyConsumptionUnitOfMeasure)

# Atomic simple type: TorqueUnitOfMeasure
class TorqueUnitOfMeasure (StringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TorqueUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3408, 1)
    _Documentation = None
TorqueUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TorqueUnitOfMeasure, enum_prefix=None)
TorqueUnitOfMeasure.foot_lbs = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='foot-lbs', tag='foot_lbs')
TorqueUnitOfMeasure.inch_lbs = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='inch-lbs', tag='inch_lbs')
TorqueUnitOfMeasure.centimeter_kilograms = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='centimeter_kilograms', tag='centimeter_kilograms')
TorqueUnitOfMeasure.foot_pounds = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='foot_pounds', tag='foot_pounds')
TorqueUnitOfMeasure.inch_ounces = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='inch_ounces', tag='inch_ounces')
TorqueUnitOfMeasure.inch_pounds = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='inch_pounds', tag='inch_pounds')
TorqueUnitOfMeasure.kilonewtons = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilonewtons', tag='kilonewtons')
TorqueUnitOfMeasure.kilograms_per_millimeter = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilograms_per_millimeter', tag='kilograms_per_millimeter')
TorqueUnitOfMeasure.newton_meters = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='newton_meters', tag='newton_meters')
TorqueUnitOfMeasure.newton_millimeters = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='newton_millimeters', tag='newton_millimeters')
TorqueUnitOfMeasure.newtons = TorqueUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='newtons', tag='newtons')
TorqueUnitOfMeasure._InitializeFacetMap(TorqueUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TorqueUnitOfMeasure', TorqueUnitOfMeasure)

# Atomic simple type: SpeedUnitOfMeasure
class SpeedUnitOfMeasure (String, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SpeedUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4336, 1)
    _Documentation = None
SpeedUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=SpeedUnitOfMeasure, enum_prefix=None)
SpeedUnitOfMeasure.feet_per_minute = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='feet_per_minute', tag='feet_per_minute')
SpeedUnitOfMeasure.miles_per_hour = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='miles_per_hour', tag='miles_per_hour')
SpeedUnitOfMeasure.kilometers_per_hour = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='kilometers_per_hour', tag='kilometers_per_hour')
SpeedUnitOfMeasure.RPM = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='RPM', tag='RPM')
SpeedUnitOfMeasure.RPS = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='RPS', tag='RPS')
SpeedUnitOfMeasure.meters_per_second = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='meters per second', tag='meters_per_second')
SpeedUnitOfMeasure.centimeters_per_second = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='centimeters per second', tag='centimeters_per_second')
SpeedUnitOfMeasure.millimeters_per_second = SpeedUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='millimeters per second', tag='millimeters_per_second')
SpeedUnitOfMeasure._InitializeFacetMap(SpeedUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'SpeedUnitOfMeasure', SpeedUnitOfMeasure)

# Atomic simple type: ToyAwardType
class ToyAwardType (MediumStringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ToyAwardType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4353, 1)
    _Documentation = None
ToyAwardType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ToyAwardType, enum_prefix=None)
ToyAwardType.australia_toy_fair_boys_toy_of_the_year = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='australia_toy_fair_boys_toy_of_the_year', tag='australia_toy_fair_boys_toy_of_the_year')
ToyAwardType.australia_toy_fair_toy_of_the_year = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='australia_toy_fair_toy_of_the_year', tag='australia_toy_fair_toy_of_the_year')
ToyAwardType.baby_and_you = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='baby_and_you', tag='baby_and_you')
ToyAwardType.babyworld = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='babyworld', tag='babyworld')
ToyAwardType.child_magazine = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='child_magazine', tag='child_magazine')
ToyAwardType.creative_child_magazine = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='creative_child_magazine', tag='creative_child_magazine')
ToyAwardType.dr_toys_100_best_child_products = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='dr_toys_100_best_child_products', tag='dr_toys_100_best_child_products')
ToyAwardType.energizer_battery_operated_toy_of_the_yr = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='energizer_battery_operated_toy_of_the_yr', tag='energizer_battery_operated_toy_of_the_yr')
ToyAwardType.family_fun_toy_of_the_year_seal = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='family_fun_toy_of_the_year_seal', tag='family_fun_toy_of_the_year_seal')
ToyAwardType.games_magazine = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='games_magazine', tag='games_magazine')
ToyAwardType.gomama_today = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='gomama_today', tag='gomama_today')
ToyAwardType.german_toy_association_toy_of_the_year = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='german_toy_association_toy_of_the_year', tag='german_toy_association_toy_of_the_year')
ToyAwardType.hamleys_toy_of_the_year = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='hamleys_toy_of_the_year', tag='hamleys_toy_of_the_year')
ToyAwardType.junior = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='junior', tag='junior')
ToyAwardType.lion_mark = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='lion_mark', tag='lion_mark')
ToyAwardType.mother_and_baby = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='mother_and_baby', tag='mother_and_baby')
ToyAwardType.mum_knows_best = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='mum_knows_best', tag='mum_knows_best')
ToyAwardType.national_parenting_approval_award = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='national_parenting_approval_award', tag='national_parenting_approval_award')
ToyAwardType.norwegian_toy_association_toy_of_the_yr = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='norwegian_toy_association_toy_of_the_yr', tag='norwegian_toy_association_toy_of_the_yr')
ToyAwardType.oppenheim_toys = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='oppenheim_toys', tag='oppenheim_toys')
ToyAwardType.parents_choice_portfolio = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='parents_choice_portfolio', tag='parents_choice_portfolio')
ToyAwardType.parents_magazine = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='parents_magazine', tag='parents_magazine')
ToyAwardType.practical_parenting = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='practical_parenting', tag='practical_parenting')
ToyAwardType.prima_baby = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='prima_baby', tag='prima_baby')
ToyAwardType.reddot = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='reddot', tag='reddot')
ToyAwardType.rdj_france_best_electronic_toy_of_the_yr = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='rdj_france_best_electronic_toy_of_the_yr', tag='rdj_france_best_electronic_toy_of_the_yr')
ToyAwardType.rdj_france_best_toy_of_the_year = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='rdj_france_best_toy_of_the_year', tag='rdj_france_best_toy_of_the_year')
ToyAwardType.the_times = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='the_times', tag='the_times')
ToyAwardType.toy_wishes = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='toy_wishes', tag='toy_wishes')
ToyAwardType.uk_npd_report_number_one_selling_toy = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='uk_npd_report_number_one_selling_toy', tag='uk_npd_report_number_one_selling_toy')
ToyAwardType.unknown = ToyAwardType._CF_enumeration.addEnumeration(unicode_value='unknown', tag='unknown')
ToyAwardType._InitializeFacetMap(ToyAwardType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ToyAwardType', ToyAwardType)

# Atomic simple type: PowerPlugType
class PowerPlugType (MediumStringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PowerPlugType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4388, 1)
    _Documentation = None
PowerPlugType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PowerPlugType, enum_prefix=None)
PowerPlugType.type_a_2pin_jp = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_a_2pin_jp', tag='type_a_2pin_jp')
PowerPlugType.type_e_2pin_fr = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_e_2pin_fr', tag='type_e_2pin_fr')
PowerPlugType.type_j_3pin_ch = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_j_3pin_ch', tag='type_j_3pin_ch')
PowerPlugType.type_a_2pin_na = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_a_2pin_na', tag='type_a_2pin_na')
PowerPlugType.type_ef_2pin_eu = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_ef_2pin_eu', tag='type_ef_2pin_eu')
PowerPlugType.type_k_3pin_dk = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_k_3pin_dk', tag='type_k_3pin_dk')
PowerPlugType.type_b_3pin_jp = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_b_3pin_jp', tag='type_b_3pin_jp')
PowerPlugType.type_f_2pin_de = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_f_2pin_de', tag='type_f_2pin_de')
PowerPlugType.type_l_3pin_it = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_l_3pin_it', tag='type_l_3pin_it')
PowerPlugType.type_b_3pin_na = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_b_3pin_na', tag='type_b_3pin_na')
PowerPlugType.type_g_3pin_uk = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_g_3pin_uk', tag='type_g_3pin_uk')
PowerPlugType.type_m_3pin_za = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_m_3pin_za', tag='type_m_3pin_za')
PowerPlugType.type_c_2pin_eu = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_c_2pin_eu', tag='type_c_2pin_eu')
PowerPlugType.type_h_3pin_il = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_h_3pin_il', tag='type_h_3pin_il')
PowerPlugType.type_n_3pin_br = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_n_3pin_br', tag='type_n_3pin_br')
PowerPlugType.type_d_3pin_in = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_d_3pin_in', tag='type_d_3pin_in')
PowerPlugType.type_i_3pin_au = PowerPlugType._CF_enumeration.addEnumeration(unicode_value='type_i_3pin_au', tag='type_i_3pin_au')
PowerPlugType._InitializeFacetMap(PowerPlugType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PowerPlugType', PowerPlugType)

# Atomic simple type: TargetGenderType
class TargetGenderType (StringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TargetGenderType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4449, 1)
    _Documentation = None
TargetGenderType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TargetGenderType, enum_prefix=None)
TargetGenderType.male = TargetGenderType._CF_enumeration.addEnumeration(unicode_value='male', tag='male')
TargetGenderType.female = TargetGenderType._CF_enumeration.addEnumeration(unicode_value='female', tag='female')
TargetGenderType.unisex = TargetGenderType._CF_enumeration.addEnumeration(unicode_value='unisex', tag='unisex')
TargetGenderType._InitializeFacetMap(TargetGenderType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TargetGenderType', TargetGenderType)

# Atomic simple type: MagnificationUnitOfMeasure
class MagnificationUnitOfMeasure (String, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MagnificationUnitOfMeasure')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4599, 1)
    _Documentation = None
MagnificationUnitOfMeasure._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=MagnificationUnitOfMeasure, enum_prefix=None)
MagnificationUnitOfMeasure.multiplier_x = MagnificationUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='multiplier_x', tag='multiplier_x')
MagnificationUnitOfMeasure.diopters = MagnificationUnitOfMeasure._CF_enumeration.addEnumeration(unicode_value='diopters', tag='diopters')
MagnificationUnitOfMeasure._InitializeFacetMap(MagnificationUnitOfMeasure._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'MagnificationUnitOfMeasure', MagnificationUnitOfMeasure)

# Atomic simple type: CarSeatWeightGroupEUType
class CarSeatWeightGroupEUType (MediumStringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CarSeatWeightGroupEUType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4612, 2)
    _Documentation = None
CarSeatWeightGroupEUType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=CarSeatWeightGroupEUType, enum_prefix=None)
CarSeatWeightGroupEUType.group_zero = CarSeatWeightGroupEUType._CF_enumeration.addEnumeration(unicode_value='group_zero', tag='group_zero')
CarSeatWeightGroupEUType.group_zero_plus = CarSeatWeightGroupEUType._CF_enumeration.addEnumeration(unicode_value='group_zero_plus', tag='group_zero_plus')
CarSeatWeightGroupEUType.group_one = CarSeatWeightGroupEUType._CF_enumeration.addEnumeration(unicode_value='group_one', tag='group_one')
CarSeatWeightGroupEUType.group_two = CarSeatWeightGroupEUType._CF_enumeration.addEnumeration(unicode_value='group_two', tag='group_two')
CarSeatWeightGroupEUType.group_three = CarSeatWeightGroupEUType._CF_enumeration.addEnumeration(unicode_value='group_three', tag='group_three')
CarSeatWeightGroupEUType._InitializeFacetMap(CarSeatWeightGroupEUType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'CarSeatWeightGroupEUType', CarSeatWeightGroupEUType)

# Atomic simple type: [anonymous]
class STD_ANON_33 (StringNotNull, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 151, 5)
    _Documentation = None
STD_ANON_33._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_33, enum_prefix=None)
STD_ANON_33.male = STD_ANON_33._CF_enumeration.addEnumeration(unicode_value='male', tag='male')
STD_ANON_33.female = STD_ANON_33._CF_enumeration.addEnumeration(unicode_value='female', tag='female')
STD_ANON_33.unisex = STD_ANON_33._CF_enumeration.addEnumeration(unicode_value='unisex', tag='unisex')
STD_ANON_33._InitializeFacetMap(STD_ANON_33._CF_enumeration)

# Complex type AddressType with content type ELEMENT_ONLY
class AddressType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AddressType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AddressType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 19, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Name'), 'Name', '__AbsentNamespace0_AddressType_Name', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 21, 3), )

    
    Name = property(__Name.value, __Name.set, None, None)

    
    # Element FormalTitle uses Python identifier FormalTitle
    __FormalTitle = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'FormalTitle'), 'FormalTitle', '__AbsentNamespace0_AddressType_FormalTitle', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 22, 3), )

    
    FormalTitle = property(__FormalTitle.value, __FormalTitle.set, None, 'e.g.  Mr., Ms., etc.')

    
    # Element GivenName uses Python identifier GivenName
    __GivenName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'GivenName'), 'GivenName', '__AbsentNamespace0_AddressType_GivenName', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 32, 3), )

    
    GivenName = property(__GivenName.value, __GivenName.set, None, "Usually the customer's first name.")

    
    # Element FamilyName uses Python identifier FamilyName
    __FamilyName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'FamilyName'), 'FamilyName', '__AbsentNamespace0_AddressType_FamilyName', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 37, 3), )

    
    FamilyName = property(__FamilyName.value, __FamilyName.set, None, "Usually the customer's last name.")

    
    # Element AddressFieldOne uses Python identifier AddressFieldOne
    __AddressFieldOne = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AddressFieldOne'), 'AddressFieldOne', '__AbsentNamespace0_AddressType_AddressFieldOne', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 42, 3), )

    
    AddressFieldOne = property(__AddressFieldOne.value, __AddressFieldOne.set, None, None)

    
    # Element AddressFieldTwo uses Python identifier AddressFieldTwo
    __AddressFieldTwo = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AddressFieldTwo'), 'AddressFieldTwo', '__AbsentNamespace0_AddressType_AddressFieldTwo', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 43, 3), )

    
    AddressFieldTwo = property(__AddressFieldTwo.value, __AddressFieldTwo.set, None, None)

    
    # Element AddressFieldThree uses Python identifier AddressFieldThree
    __AddressFieldThree = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AddressFieldThree'), 'AddressFieldThree', '__AbsentNamespace0_AddressType_AddressFieldThree', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 44, 3), )

    
    AddressFieldThree = property(__AddressFieldThree.value, __AddressFieldThree.set, None, None)

    
    # Element City uses Python identifier City
    __City = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'City'), 'City', '__AbsentNamespace0_AddressType_City', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 45, 3), )

    
    City = property(__City.value, __City.set, None, None)

    
    # Element County uses Python identifier County
    __County = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'County'), 'County', '__AbsentNamespace0_AddressType_County', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 46, 3), )

    
    County = property(__County.value, __County.set, None, None)

    
    # Element StateOrRegion uses Python identifier StateOrRegion
    __StateOrRegion = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'StateOrRegion'), 'StateOrRegion', '__AbsentNamespace0_AddressType_StateOrRegion', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 47, 3), )

    
    StateOrRegion = property(__StateOrRegion.value, __StateOrRegion.set, None, None)

    
    # Element PostalCode uses Python identifier PostalCode
    __PostalCode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PostalCode'), 'PostalCode', '__AbsentNamespace0_AddressType_PostalCode', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 48, 3), )

    
    PostalCode = property(__PostalCode.value, __PostalCode.set, None, None)

    
    # Element CountryCode uses Python identifier CountryCode
    __CountryCode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'CountryCode'), 'CountryCode', '__AbsentNamespace0_AddressType_CountryCode', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 49, 3), )

    
    CountryCode = property(__CountryCode.value, __CountryCode.set, None, None)

    
    # Element PhoneNumber uses Python identifier PhoneNumber
    __PhoneNumber = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PhoneNumber'), 'PhoneNumber', '__AbsentNamespace0_AddressType_PhoneNumber', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 57, 3), )

    
    PhoneNumber = property(__PhoneNumber.value, __PhoneNumber.set, None, None)

    
    # Element isDefaultShipping uses Python identifier isDefaultShipping
    __isDefaultShipping = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'isDefaultShipping'), 'isDefaultShipping', '__AbsentNamespace0_AddressType_isDefaultShipping', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 58, 3), )

    
    isDefaultShipping = property(__isDefaultShipping.value, __isDefaultShipping.set, None, 'Only one default shipping address can exist at any given\n\t\t\t\t\t\ttime. If more than one address has this set to "true," then the last one\n\t\t\t\t\t\twill become the default.')

    
    # Element isDefaultBilling uses Python identifier isDefaultBilling
    __isDefaultBilling = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'isDefaultBilling'), 'isDefaultBilling', '__AbsentNamespace0_AddressType_isDefaultBilling', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 65, 3), )

    
    isDefaultBilling = property(__isDefaultBilling.value, __isDefaultBilling.set, None, 'Only one default billing address can exist at any given time.\n\t\t\t\t\t\tIf more than one address has this set to "true," then the last one will\n\t\t\t\t\t\tbecome the default.')

    
    # Element isDefaultOneClick uses Python identifier isDefaultOneClick
    __isDefaultOneClick = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'isDefaultOneClick'), 'isDefaultOneClick', '__AbsentNamespace0_AddressType_isDefaultOneClick', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 72, 3), )

    
    isDefaultOneClick = property(__isDefaultOneClick.value, __isDefaultOneClick.set, None, 'Only one default OneClick address can exist at any given\n\t\t\t\t\t\ttime. If more than one address has this set to "true," then the last one\n\t\t\t\t\t\twill become the default.')

    _ElementMap.update({
        __Name.name() : __Name,
        __FormalTitle.name() : __FormalTitle,
        __GivenName.name() : __GivenName,
        __FamilyName.name() : __FamilyName,
        __AddressFieldOne.name() : __AddressFieldOne,
        __AddressFieldTwo.name() : __AddressFieldTwo,
        __AddressFieldThree.name() : __AddressFieldThree,
        __City.name() : __City,
        __County.name() : __County,
        __StateOrRegion.name() : __StateOrRegion,
        __PostalCode.name() : __PostalCode,
        __CountryCode.name() : __CountryCode,
        __PhoneNumber.name() : __PhoneNumber,
        __isDefaultShipping.name() : __isDefaultShipping,
        __isDefaultBilling.name() : __isDefaultBilling,
        __isDefaultOneClick.name() : __isDefaultOneClick
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'AddressType', AddressType)


# Complex type AddressTypeSupportNonCity with content type ELEMENT_ONLY
class AddressTypeSupportNonCity (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AddressTypeSupportNonCity with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AddressTypeSupportNonCity')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 86, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Name'), 'Name', '__AbsentNamespace0_AddressTypeSupportNonCity_Name', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 88, 3), )

    
    Name = property(__Name.value, __Name.set, None, None)

    
    # Element AddressFieldOne uses Python identifier AddressFieldOne
    __AddressFieldOne = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AddressFieldOne'), 'AddressFieldOne', '__AbsentNamespace0_AddressTypeSupportNonCity_AddressFieldOne', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 89, 3), )

    
    AddressFieldOne = property(__AddressFieldOne.value, __AddressFieldOne.set, None, None)

    
    # Element AddressFieldTwo uses Python identifier AddressFieldTwo
    __AddressFieldTwo = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AddressFieldTwo'), 'AddressFieldTwo', '__AbsentNamespace0_AddressTypeSupportNonCity_AddressFieldTwo', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 90, 3), )

    
    AddressFieldTwo = property(__AddressFieldTwo.value, __AddressFieldTwo.set, None, None)

    
    # Element AddressFieldThree uses Python identifier AddressFieldThree
    __AddressFieldThree = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AddressFieldThree'), 'AddressFieldThree', '__AbsentNamespace0_AddressTypeSupportNonCity_AddressFieldThree', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 91, 3), )

    
    AddressFieldThree = property(__AddressFieldThree.value, __AddressFieldThree.set, None, None)

    
    # Element City uses Python identifier City
    __City = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'City'), 'City', '__AbsentNamespace0_AddressTypeSupportNonCity_City', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 92, 3), )

    
    City = property(__City.value, __City.set, None, None)

    
    # Element DistrictOrCounty uses Python identifier DistrictOrCounty
    __DistrictOrCounty = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'DistrictOrCounty'), 'DistrictOrCounty', '__AbsentNamespace0_AddressTypeSupportNonCity_DistrictOrCounty', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 93, 3), )

    
    DistrictOrCounty = property(__DistrictOrCounty.value, __DistrictOrCounty.set, None, None)

    
    # Element County uses Python identifier County
    __County = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'County'), 'County', '__AbsentNamespace0_AddressTypeSupportNonCity_County', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 94, 3), )

    
    County = property(__County.value, __County.set, None, None)

    
    # Element StateOrRegion uses Python identifier StateOrRegion
    __StateOrRegion = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'StateOrRegion'), 'StateOrRegion', '__AbsentNamespace0_AddressTypeSupportNonCity_StateOrRegion', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 95, 3), )

    
    StateOrRegion = property(__StateOrRegion.value, __StateOrRegion.set, None, None)

    
    # Element PostalCode uses Python identifier PostalCode
    __PostalCode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PostalCode'), 'PostalCode', '__AbsentNamespace0_AddressTypeSupportNonCity_PostalCode', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 96, 3), )

    
    PostalCode = property(__PostalCode.value, __PostalCode.set, None, None)

    
    # Element CountryCode uses Python identifier CountryCode
    __CountryCode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'CountryCode'), 'CountryCode', '__AbsentNamespace0_AddressTypeSupportNonCity_CountryCode', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 97, 3), )

    
    CountryCode = property(__CountryCode.value, __CountryCode.set, None, None)

    
    # Element PhoneNumber uses Python identifier PhoneNumber
    __PhoneNumber = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PhoneNumber'), 'PhoneNumber', '__AbsentNamespace0_AddressTypeSupportNonCity_PhoneNumber', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 105, 3), )

    
    PhoneNumber = property(__PhoneNumber.value, __PhoneNumber.set, None, None)

    _ElementMap.update({
        __Name.name() : __Name,
        __AddressFieldOne.name() : __AddressFieldOne,
        __AddressFieldTwo.name() : __AddressFieldTwo,
        __AddressFieldThree.name() : __AddressFieldThree,
        __City.name() : __City,
        __DistrictOrCounty.name() : __DistrictOrCounty,
        __County.name() : __County,
        __StateOrRegion.name() : __StateOrRegion,
        __PostalCode.name() : __PostalCode,
        __CountryCode.name() : __CountryCode,
        __PhoneNumber.name() : __PhoneNumber
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'AddressTypeSupportNonCity', AddressTypeSupportNonCity)


# Complex type AmazonFees with content type ELEMENT_ONLY
class AmazonFees (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AmazonFees with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AmazonFees')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 169, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Fee uses Python identifier Fee
    __Fee = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Fee'), 'Fee', '__AbsentNamespace0_AmazonFees_Fee', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 171, 3), )

    
    Fee = property(__Fee.value, __Fee.set, None, None)

    _ElementMap.update({
        __Fee.name() : __Fee
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'AmazonFees', AmazonFees)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 172, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 174, 6), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Amount uses Python identifier Amount
    __Amount = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Amount'), 'Amount', '__AbsentNamespace0_CTD_ANON_Amount', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 179, 6), )

    
    Amount = property(__Amount.value, __Amount.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Amount.name() : __Amount
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_ (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 191, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element AreBatteriesIncluded uses Python identifier AreBatteriesIncluded
    __AreBatteriesIncluded = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AreBatteriesIncluded'), 'AreBatteriesIncluded', '__AbsentNamespace0_CTD_ANON__AreBatteriesIncluded', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 193, 4), )

    
    AreBatteriesIncluded = property(__AreBatteriesIncluded.value, __AreBatteriesIncluded.set, None, None)

    
    # Element AreBatteriesRequired uses Python identifier AreBatteriesRequired
    __AreBatteriesRequired = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AreBatteriesRequired'), 'AreBatteriesRequired', '__AbsentNamespace0_CTD_ANON__AreBatteriesRequired', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 194, 4), )

    
    AreBatteriesRequired = property(__AreBatteriesRequired.value, __AreBatteriesRequired.set, None, None)

    
    # Element BatterySubgroup uses Python identifier BatterySubgroup
    __BatterySubgroup = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatterySubgroup'), 'BatterySubgroup', '__AbsentNamespace0_CTD_ANON__BatterySubgroup', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 195, 4), )

    
    BatterySubgroup = property(__BatterySubgroup.value, __BatterySubgroup.set, None, None)

    _ElementMap.update({
        __AreBatteriesIncluded.name() : __AreBatteriesIncluded,
        __AreBatteriesRequired.name() : __AreBatteriesRequired,
        __BatterySubgroup.name() : __BatterySubgroup
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_2 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 196, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element BatteryType uses Python identifier BatteryType
    __BatteryType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryType'), 'BatteryType', '__AbsentNamespace0_CTD_ANON_2_BatteryType', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 198, 7), )

    
    BatteryType = property(__BatteryType.value, __BatteryType.set, None, None)

    
    # Element NumberOfBatteries uses Python identifier NumberOfBatteries
    __NumberOfBatteries = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'NumberOfBatteries'), 'NumberOfBatteries', '__AbsentNamespace0_CTD_ANON_2_NumberOfBatteries', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 225, 7), )

    
    NumberOfBatteries = property(__NumberOfBatteries.value, __NumberOfBatteries.set, None, None)

    _ElementMap.update({
        __BatteryType.name() : __BatteryType,
        __NumberOfBatteries.name() : __NumberOfBatteries
    })
    _AttributeMap.update({
        
    })



# Complex type BuyerPrice with content type ELEMENT_ONLY
class BuyerPrice (pyxb.binding.basis.complexTypeDefinition):
    """Complex type BuyerPrice with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BuyerPrice')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 239, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Component uses Python identifier Component
    __Component = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Component'), 'Component', '__AbsentNamespace0_BuyerPrice_Component', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 241, 3), )

    
    Component = property(__Component.value, __Component.set, None, None)

    _ElementMap.update({
        __Component.name() : __Component
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'BuyerPrice', BuyerPrice)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_3 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 242, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_3_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 244, 6), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Amount uses Python identifier Amount
    __Amount = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Amount'), 'Amount', '__AbsentNamespace0_CTD_ANON_3_Amount', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 267, 6), )

    
    Amount = property(__Amount.value, __Amount.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Amount.name() : __Amount
    })
    _AttributeMap.update({
        
    })



# Complex type DirectPaymentType with content type ELEMENT_ONLY
class DirectPaymentType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type DirectPaymentType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DirectPaymentType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 278, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Component uses Python identifier Component
    __Component = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Component'), 'Component', '__AbsentNamespace0_DirectPaymentType_Component', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 280, 3), )

    
    Component = property(__Component.value, __Component.set, None, None)

    _ElementMap.update({
        __Component.name() : __Component
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'DirectPaymentType', DirectPaymentType)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_4 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 281, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_4_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 283, 6), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Amount uses Python identifier Amount
    __Amount = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Amount'), 'Amount', '__AbsentNamespace0_CTD_ANON_4_Amount', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 284, 6), )

    
    Amount = property(__Amount.value, __Amount.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Amount.name() : __Amount
    })
    _AttributeMap.update({
        
    })



# Complex type DatedPrice with content type ELEMENT_ONLY
class DatedPrice (pyxb.binding.basis.complexTypeDefinition):
    """Complex type DatedPrice with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DatedPrice')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1730, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element StartDate uses Python identifier StartDate
    __StartDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'StartDate'), 'StartDate', '__AbsentNamespace0_DatedPrice_StartDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1732, 3), )

    
    StartDate = property(__StartDate.value, __StartDate.set, None, None)

    
    # Element EndDate uses Python identifier EndDate
    __EndDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'EndDate'), 'EndDate', '__AbsentNamespace0_DatedPrice_EndDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1733, 3), )

    
    EndDate = property(__EndDate.value, __EndDate.set, None, None)

    
    # Element Price uses Python identifier Price
    __Price = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Price'), 'Price', '__AbsentNamespace0_DatedPrice_Price', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1735, 4), )

    
    Price = property(__Price.value, __Price.set, None, None)

    
    # Element PreviousPrice uses Python identifier PreviousPrice
    __PreviousPrice = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PreviousPrice'), 'PreviousPrice', '__AbsentNamespace0_DatedPrice_PreviousPrice', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1736, 4), )

    
    PreviousPrice = property(__PreviousPrice.value, __PreviousPrice.set, None, None)

    
    # Attribute delete uses Python identifier delete
    __delete = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'delete'), 'delete', '__AbsentNamespace0_DatedPrice_delete', pyxb.binding.datatypes.boolean)
    __delete._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1739, 2)
    __delete._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1739, 2)
    
    delete = property(__delete.value, __delete.set, None, None)

    _ElementMap.update({
        __StartDate.name() : __StartDate,
        __EndDate.name() : __EndDate,
        __Price.name() : __Price,
        __PreviousPrice.name() : __PreviousPrice
    })
    _AttributeMap.update({
        __delete.name() : __delete
    })
Namespace.addCategoryObject('typeBinding', 'DatedPrice', DatedPrice)


# Complex type DatedCompareAtPrice with content type ELEMENT_ONLY
class DatedCompareAtPrice (pyxb.binding.basis.complexTypeDefinition):
    """Complex type DatedCompareAtPrice with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DatedCompareAtPrice')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1741, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element StartDate uses Python identifier StartDate
    __StartDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'StartDate'), 'StartDate', '__AbsentNamespace0_DatedCompareAtPrice_StartDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1743, 3), )

    
    StartDate = property(__StartDate.value, __StartDate.set, None, None)

    
    # Element EndDate uses Python identifier EndDate
    __EndDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'EndDate'), 'EndDate', '__AbsentNamespace0_DatedCompareAtPrice_EndDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1744, 3), )

    
    EndDate = property(__EndDate.value, __EndDate.set, None, None)

    
    # Element CompareAtPrice uses Python identifier CompareAtPrice
    __CompareAtPrice = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'CompareAtPrice'), 'CompareAtPrice', '__AbsentNamespace0_DatedCompareAtPrice_CompareAtPrice', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1745, 3), )

    
    CompareAtPrice = property(__CompareAtPrice.value, __CompareAtPrice.set, None, None)

    
    # Attribute delete uses Python identifier delete
    __delete = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'delete'), 'delete', '__AbsentNamespace0_DatedCompareAtPrice_delete', pyxb.binding.datatypes.boolean)
    __delete._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1747, 2)
    __delete._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1747, 2)
    
    delete = property(__delete.value, __delete.set, None, None)

    _ElementMap.update({
        __StartDate.name() : __StartDate,
        __EndDate.name() : __EndDate,
        __CompareAtPrice.name() : __CompareAtPrice
    })
    _AttributeMap.update({
        __delete.name() : __delete
    })
Namespace.addCategoryObject('typeBinding', 'DatedCompareAtPrice', DatedCompareAtPrice)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_5 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2148, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_5_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2150, 4), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Value uses Python identifier Value
    __Value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Value'), 'Value', '__AbsentNamespace0_CTD_ANON_5_Value', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2163, 4), )

    
    Value = property(__Value.value, __Value.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Value.name() : __Value
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_6 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2182, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_6_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2184, 4), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Value uses Python identifier Value
    __Value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Value'), 'Value', '__AbsentNamespace0_CTD_ANON_6_Value', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2193, 4), )

    
    Value = property(__Value.value, __Value.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Value.name() : __Value
    })
    _AttributeMap.update({
        
    })



# Complex type PromotionDataType with content type ELEMENT_ONLY
class PromotionDataType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PromotionDataType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PromotionDataType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2248, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element MerchantPromotionID uses Python identifier MerchantPromotionID
    __MerchantPromotionID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'MerchantPromotionID'), 'MerchantPromotionID', '__AbsentNamespace0_PromotionDataType_MerchantPromotionID', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2097, 1), )

    
    MerchantPromotionID = property(__MerchantPromotionID.value, __MerchantPromotionID.set, None, None)

    
    # Element PromotionClaimCode uses Python identifier PromotionClaimCode
    __PromotionClaimCode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PromotionClaimCode'), 'PromotionClaimCode', '__AbsentNamespace0_PromotionDataType_PromotionClaimCode', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2233, 1), )

    
    PromotionClaimCode = property(__PromotionClaimCode.value, __PromotionClaimCode.set, None, None)

    
    # Element Component uses Python identifier Component
    __Component = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Component'), 'Component', '__AbsentNamespace0_PromotionDataType_Component', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2252, 3), )

    
    Component = property(__Component.value, __Component.set, None, None)

    _ElementMap.update({
        __MerchantPromotionID.name() : __MerchantPromotionID,
        __PromotionClaimCode.name() : __PromotionClaimCode,
        __Component.name() : __Component
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'PromotionDataType', PromotionDataType)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_7 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2253, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_7_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2255, 6), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Amount uses Python identifier Amount
    __Amount = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Amount'), 'Amount', '__AbsentNamespace0_CTD_ANON_7_Amount', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2256, 6), )

    
    Amount = property(__Amount.value, __Amount.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Amount.name() : __Amount
    })
    _AttributeMap.update({
        
    })



# Complex type ConditionInfo with content type ELEMENT_ONLY
class ConditionInfo (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ConditionInfo with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ConditionInfo')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2287, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element ConditionNote uses Python identifier ConditionNote
    __ConditionNote = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ConditionNote'), 'ConditionNote', '__AbsentNamespace0_ConditionInfo_ConditionNote', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2290, 3), )

    
    ConditionNote = property(__ConditionNote.value, __ConditionNote.set, None, None)

    
    # Element ConditionType uses Python identifier ConditionType
    __ConditionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ConditionType'), 'ConditionType', '__AbsentNamespace0_ConditionInfo_ConditionType', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2293, 1), )

    
    ConditionType = property(__ConditionType.value, __ConditionType.set, None, None)

    _ElementMap.update({
        __ConditionNote.name() : __ConditionNote,
        __ConditionType.name() : __ConditionType
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'ConditionInfo', ConditionInfo)


# Complex type CustomizationInfoType with content type ELEMENT_ONLY
class CustomizationInfoType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CustomizationInfoType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CustomizationInfoType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2317, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CustomizationInfoType_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2319, 3), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element Data uses Python identifier Data
    __Data = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Data'), 'Data', '__AbsentNamespace0_CustomizationInfoType_Data', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2320, 3), )

    
    Data = property(__Data.value, __Data.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __Data.name() : __Data
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'CustomizationInfoType', CustomizationInfoType)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_8 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2331, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Type uses Python identifier Type
    __Type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_CTD_ANON_8_Type', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2333, 4), )

    
    Type = property(__Type.value, __Type.set, None, None)

    
    # Element SystemRequirements uses Python identifier SystemRequirements
    __SystemRequirements = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SystemRequirements'), 'SystemRequirements', '__AbsentNamespace0_CTD_ANON_8_SystemRequirements', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2342, 4), )

    
    SystemRequirements = property(__SystemRequirements.value, __SystemRequirements.set, None, None)

    _ElementMap.update({
        __Type.name() : __Type,
        __SystemRequirements.name() : __SystemRequirements
    })
    _AttributeMap.update({
        
    })



# Complex type RebateType with content type ELEMENT_ONLY
class RebateType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type RebateType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RebateType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2351, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element RebateStartDate uses Python identifier RebateStartDate
    __RebateStartDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RebateStartDate'), 'RebateStartDate', '__AbsentNamespace0_RebateType_RebateStartDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2353, 3), )

    
    RebateStartDate = property(__RebateStartDate.value, __RebateStartDate.set, None, None)

    
    # Element RebateEndDate uses Python identifier RebateEndDate
    __RebateEndDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RebateEndDate'), 'RebateEndDate', '__AbsentNamespace0_RebateType_RebateEndDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2354, 3), )

    
    RebateEndDate = property(__RebateEndDate.value, __RebateEndDate.set, None, None)

    
    # Element RebateMessage uses Python identifier RebateMessage
    __RebateMessage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RebateMessage'), 'RebateMessage', '__AbsentNamespace0_RebateType_RebateMessage', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2355, 3), )

    
    RebateMessage = property(__RebateMessage.value, __RebateMessage.set, None, None)

    
    # Element RebateName uses Python identifier RebateName
    __RebateName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RebateName'), 'RebateName', '__AbsentNamespace0_RebateType_RebateName', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2356, 3), )

    
    RebateName = property(__RebateName.value, __RebateName.set, None, None)

    _ElementMap.update({
        __RebateStartDate.name() : __RebateStartDate,
        __RebateEndDate.name() : __RebateEndDate,
        __RebateMessage.name() : __RebateMessage,
        __RebateName.name() : __RebateName
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'RebateType', RebateType)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_9 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2367, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Color uses Python identifier Color
    __Color = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Color'), 'Color', '__AbsentNamespace0_CTD_ANON_9_Color', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2369, 4), )

    
    Color = property(__Color.value, __Color.set, None, None)

    
    # Element ColorMap uses Python identifier ColorMap
    __ColorMap = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ColorMap'), 'ColorMap', '__AbsentNamespace0_CTD_ANON_9_ColorMap', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2374, 1), )

    
    ColorMap = property(__ColorMap.value, __ColorMap.set, None, None)

    _ElementMap.update({
        __Color.name() : __Color,
        __ColorMap.name() : __ColorMap
    })
    _AttributeMap.update({
        
    })



# Complex type Customer with content type ELEMENT_ONLY
class Customer (pyxb.binding.basis.complexTypeDefinition):
    """Complex type Customer with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Customer')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3448, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Name'), 'Name', '__AbsentNamespace0_Customer_Name', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3450, 3), )

    
    Name = property(__Name.value, __Name.set, None, None)

    
    # Element FormalTitle uses Python identifier FormalTitle
    __FormalTitle = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'FormalTitle'), 'FormalTitle', '__AbsentNamespace0_Customer_FormalTitle', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3451, 3), )

    
    FormalTitle = property(__FormalTitle.value, __FormalTitle.set, None, 'e.g. Mr., Ms., etc.')

    
    # Element GivenName uses Python identifier GivenName
    __GivenName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'GivenName'), 'GivenName', '__AbsentNamespace0_Customer_GivenName', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3461, 3), )

    
    GivenName = property(__GivenName.value, __GivenName.set, None, "Usually the customer's first name.")

    
    # Element FamilyName uses Python identifier FamilyName
    __FamilyName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'FamilyName'), 'FamilyName', '__AbsentNamespace0_Customer_FamilyName', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3466, 3), )

    
    FamilyName = property(__FamilyName.value, __FamilyName.set, None, "Usually the customer's last name.")

    
    # Element Email uses Python identifier Email
    __Email = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Email'), 'Email', '__AbsentNamespace0_Customer_Email', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3471, 3), )

    
    Email = property(__Email.value, __Email.set, None, None)

    
    # Element BirthDate uses Python identifier BirthDate
    __BirthDate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BirthDate'), 'BirthDate', '__AbsentNamespace0_Customer_BirthDate', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3472, 3), )

    
    BirthDate = property(__BirthDate.value, __BirthDate.set, None, "The customer's birth date")

    
    # Element CustomerAddress uses Python identifier CustomerAddress
    __CustomerAddress = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'CustomerAddress'), 'CustomerAddress', '__AbsentNamespace0_Customer_CustomerAddress', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3477, 3), )

    
    CustomerAddress = property(__CustomerAddress.value, __CustomerAddress.set, None, None)

    _ElementMap.update({
        __Name.name() : __Name,
        __FormalTitle.name() : __FormalTitle,
        __GivenName.name() : __GivenName,
        __FamilyName.name() : __FamilyName,
        __Email.name() : __Email,
        __BirthDate.name() : __BirthDate,
        __CustomerAddress.name() : __CustomerAddress
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'Customer', Customer)


# Complex type NameValuePair with content type ELEMENT_ONLY
class NameValuePair (pyxb.binding.basis.complexTypeDefinition):
    """Complex type NameValuePair with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NameValuePair')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3485, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Name'), 'Name', '__AbsentNamespace0_NameValuePair_Name', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3487, 3), )

    
    Name = property(__Name.value, __Name.set, None, None)

    
    # Element Value uses Python identifier Value
    __Value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Value'), 'Value', '__AbsentNamespace0_NameValuePair_Value', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3488, 3), )

    
    Value = property(__Value.value, __Value.set, None, None)

    _ElementMap.update({
        __Name.name() : __Name,
        __Value.name() : __Value
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'NameValuePair', NameValuePair)


# Complex type WeightRecommendationType with content type ELEMENT_ONLY
class WeightRecommendationType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type WeightRecommendationType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WeightRecommendationType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4197, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element MaximumWeightRecommendation uses Python identifier MaximumWeightRecommendation
    __MaximumWeightRecommendation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MaximumWeightRecommendation'), 'MaximumWeightRecommendation', '__AbsentNamespace0_WeightRecommendationType_MaximumWeightRecommendation', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4199, 3), )

    
    MaximumWeightRecommendation = property(__MaximumWeightRecommendation.value, __MaximumWeightRecommendation.set, None, None)

    
    # Element MinimumWeightRecommendation uses Python identifier MinimumWeightRecommendation
    __MinimumWeightRecommendation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MinimumWeightRecommendation'), 'MinimumWeightRecommendation', '__AbsentNamespace0_WeightRecommendationType_MinimumWeightRecommendation', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4200, 3), )

    
    MinimumWeightRecommendation = property(__MinimumWeightRecommendation.value, __MinimumWeightRecommendation.set, None, None)

    _ElementMap.update({
        __MaximumWeightRecommendation.name() : __MaximumWeightRecommendation,
        __MinimumWeightRecommendation.name() : __MinimumWeightRecommendation
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'WeightRecommendationType', WeightRecommendationType)


# Complex type CharacterDataType with content type ELEMENT_ONLY
class CharacterDataType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CharacterDataType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CharacterDataType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4209, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element SKU uses Python identifier SKU
    __SKU = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SKU'), 'SKU', '__AbsentNamespace0_CharacterDataType_SKU', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2269, 1), )

    
    SKU = property(__SKU.value, __SKU.set, None, None)

    
    # Element EffectiveTimestamp uses Python identifier EffectiveTimestamp
    __EffectiveTimestamp = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'EffectiveTimestamp'), 'EffectiveTimestamp', '__AbsentNamespace0_CharacterDataType_EffectiveTimestamp', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4212, 3), )

    
    EffectiveTimestamp = property(__EffectiveTimestamp.value, __EffectiveTimestamp.set, None, None)

    
    # Element Plugin uses Python identifier Plugin
    __Plugin = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Plugin'), 'Plugin', '__AbsentNamespace0_CharacterDataType_Plugin', True, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4213, 3), )

    
    Plugin = property(__Plugin.value, __Plugin.set, None, None)

    
    # Element AdditionalMessageDiscriminator uses Python identifier AdditionalMessageDiscriminator
    __AdditionalMessageDiscriminator = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'AdditionalMessageDiscriminator'), 'AdditionalMessageDiscriminator', '__AbsentNamespace0_CharacterDataType_AdditionalMessageDiscriminator', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4214, 3), )

    
    AdditionalMessageDiscriminator = property(__AdditionalMessageDiscriminator.value, __AdditionalMessageDiscriminator.set, None, None)

    
    # Element Payload uses Python identifier Payload
    __Payload = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Payload'), 'Payload', '__AbsentNamespace0_CharacterDataType_Payload', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4215, 3), )

    
    Payload = property(__Payload.value, __Payload.set, None, None)

    
    # Attribute schemaVersion uses Python identifier schemaVersion
    __schemaVersion = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'schemaVersion'), 'schemaVersion', '__AbsentNamespace0_CharacterDataType_schemaVersion', pyxb.binding.datatypes.string)
    __schemaVersion._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4217, 2)
    __schemaVersion._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4217, 2)
    
    schemaVersion = property(__schemaVersion.value, __schemaVersion.set, None, None)

    
    # Attribute isOfferOnlyUpdate uses Python identifier isOfferOnlyUpdate
    __isOfferOnlyUpdate = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'isOfferOnlyUpdate'), 'isOfferOnlyUpdate', '__AbsentNamespace0_CharacterDataType_isOfferOnlyUpdate', pyxb.binding.datatypes.boolean)
    __isOfferOnlyUpdate._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4218, 2)
    __isOfferOnlyUpdate._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4218, 2)
    
    isOfferOnlyUpdate = property(__isOfferOnlyUpdate.value, __isOfferOnlyUpdate.set, None, None)

    _ElementMap.update({
        __SKU.name() : __SKU,
        __EffectiveTimestamp.name() : __EffectiveTimestamp,
        __Plugin.name() : __Plugin,
        __AdditionalMessageDiscriminator.name() : __AdditionalMessageDiscriminator,
        __Payload.name() : __Payload
    })
    _AttributeMap.update({
        __schemaVersion.name() : __schemaVersion,
        __isOfferOnlyUpdate.name() : __isOfferOnlyUpdate
    })
Namespace.addCategoryObject('typeBinding', 'CharacterDataType', CharacterDataType)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_10 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4231, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element IsRecalled uses Python identifier IsRecalled
    __IsRecalled = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'IsRecalled'), 'IsRecalled', '__AbsentNamespace0_CTD_ANON_10_IsRecalled', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4233, 4), )

    
    IsRecalled = property(__IsRecalled.value, __IsRecalled.set, None, None)

    
    # Element RecallDescription uses Python identifier RecallDescription
    __RecallDescription = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RecallDescription'), 'RecallDescription', '__AbsentNamespace0_CTD_ANON_10_RecallDescription', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4234, 4), )

    
    RecallDescription = property(__RecallDescription.value, __RecallDescription.set, None, None)

    _ElementMap.update({
        __IsRecalled.name() : __IsRecalled,
        __RecallDescription.name() : __RecallDescription
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_11 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4250, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element MinimumManufacturerAgeRecommended uses Python identifier MinimumManufacturerAgeRecommended
    __MinimumManufacturerAgeRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MinimumManufacturerAgeRecommended'), 'MinimumManufacturerAgeRecommended', '__AbsentNamespace0_CTD_ANON_11_MinimumManufacturerAgeRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4252, 4), )

    
    MinimumManufacturerAgeRecommended = property(__MinimumManufacturerAgeRecommended.value, __MinimumManufacturerAgeRecommended.set, None, None)

    
    # Element MaximumManufacturerAgeRecommended uses Python identifier MaximumManufacturerAgeRecommended
    __MaximumManufacturerAgeRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MaximumManufacturerAgeRecommended'), 'MaximumManufacturerAgeRecommended', '__AbsentNamespace0_CTD_ANON_11_MaximumManufacturerAgeRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4253, 4), )

    
    MaximumManufacturerAgeRecommended = property(__MaximumManufacturerAgeRecommended.value, __MaximumManufacturerAgeRecommended.set, None, None)

    
    # Element MinimumMerchantAgeRecommended uses Python identifier MinimumMerchantAgeRecommended
    __MinimumMerchantAgeRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MinimumMerchantAgeRecommended'), 'MinimumMerchantAgeRecommended', '__AbsentNamespace0_CTD_ANON_11_MinimumMerchantAgeRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4254, 4), )

    
    MinimumMerchantAgeRecommended = property(__MinimumMerchantAgeRecommended.value, __MinimumMerchantAgeRecommended.set, None, None)

    
    # Element MaximumMerchantAgeRecommended uses Python identifier MaximumMerchantAgeRecommended
    __MaximumMerchantAgeRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MaximumMerchantAgeRecommended'), 'MaximumMerchantAgeRecommended', '__AbsentNamespace0_CTD_ANON_11_MaximumMerchantAgeRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4255, 4), )

    
    MaximumMerchantAgeRecommended = property(__MaximumMerchantAgeRecommended.value, __MaximumMerchantAgeRecommended.set, None, None)

    _ElementMap.update({
        __MinimumManufacturerAgeRecommended.name() : __MinimumManufacturerAgeRecommended,
        __MaximumManufacturerAgeRecommended.name() : __MaximumManufacturerAgeRecommended,
        __MinimumMerchantAgeRecommended.name() : __MinimumMerchantAgeRecommended,
        __MaximumMerchantAgeRecommended.name() : __MaximumMerchantAgeRecommended
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_12 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4265, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element MinimumManufacturerWeightRecommended uses Python identifier MinimumManufacturerWeightRecommended
    __MinimumManufacturerWeightRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MinimumManufacturerWeightRecommended'), 'MinimumManufacturerWeightRecommended', '__AbsentNamespace0_CTD_ANON_12_MinimumManufacturerWeightRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4267, 4), )

    
    MinimumManufacturerWeightRecommended = property(__MinimumManufacturerWeightRecommended.value, __MinimumManufacturerWeightRecommended.set, None, None)

    
    # Element MaximumManufacturerWeightRecommended uses Python identifier MaximumManufacturerWeightRecommended
    __MaximumManufacturerWeightRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MaximumManufacturerWeightRecommended'), 'MaximumManufacturerWeightRecommended', '__AbsentNamespace0_CTD_ANON_12_MaximumManufacturerWeightRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4268, 4), )

    
    MaximumManufacturerWeightRecommended = property(__MaximumManufacturerWeightRecommended.value, __MaximumManufacturerWeightRecommended.set, None, None)

    _ElementMap.update({
        __MinimumManufacturerWeightRecommended.name() : __MinimumManufacturerWeightRecommended,
        __MaximumManufacturerWeightRecommended.name() : __MaximumManufacturerWeightRecommended
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_13 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4278, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element MinimumHeightRecommended uses Python identifier MinimumHeightRecommended
    __MinimumHeightRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MinimumHeightRecommended'), 'MinimumHeightRecommended', '__AbsentNamespace0_CTD_ANON_13_MinimumHeightRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4280, 4), )

    
    MinimumHeightRecommended = property(__MinimumHeightRecommended.value, __MinimumHeightRecommended.set, None, None)

    
    # Element MaximumHeightRecommended uses Python identifier MaximumHeightRecommended
    __MaximumHeightRecommended = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MaximumHeightRecommended'), 'MaximumHeightRecommended', '__AbsentNamespace0_CTD_ANON_13_MaximumHeightRecommended', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4281, 4), )

    
    MaximumHeightRecommended = property(__MaximumHeightRecommended.value, __MaximumHeightRecommended.set, None, None)

    _ElementMap.update({
        __MinimumHeightRecommended.name() : __MinimumHeightRecommended,
        __MaximumHeightRecommended.name() : __MaximumHeightRecommended
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_14 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4291, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element ForwardFacingMaximumWeight uses Python identifier ForwardFacingMaximumWeight
    __ForwardFacingMaximumWeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ForwardFacingMaximumWeight'), 'ForwardFacingMaximumWeight', '__AbsentNamespace0_CTD_ANON_14_ForwardFacingMaximumWeight', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4293, 4), )

    
    ForwardFacingMaximumWeight = property(__ForwardFacingMaximumWeight.value, __ForwardFacingMaximumWeight.set, None, None)

    
    # Element ForwardFacingMinimumWeight uses Python identifier ForwardFacingMinimumWeight
    __ForwardFacingMinimumWeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ForwardFacingMinimumWeight'), 'ForwardFacingMinimumWeight', '__AbsentNamespace0_CTD_ANON_14_ForwardFacingMinimumWeight', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4294, 4), )

    
    ForwardFacingMinimumWeight = property(__ForwardFacingMinimumWeight.value, __ForwardFacingMinimumWeight.set, None, None)

    _ElementMap.update({
        __ForwardFacingMaximumWeight.name() : __ForwardFacingMaximumWeight,
        __ForwardFacingMinimumWeight.name() : __ForwardFacingMinimumWeight
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_15 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4304, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element RearFacingMaximumWeight uses Python identifier RearFacingMaximumWeight
    __RearFacingMaximumWeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RearFacingMaximumWeight'), 'RearFacingMaximumWeight', '__AbsentNamespace0_CTD_ANON_15_RearFacingMaximumWeight', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4306, 4), )

    
    RearFacingMaximumWeight = property(__RearFacingMaximumWeight.value, __RearFacingMaximumWeight.set, None, None)

    
    # Element RearFacingMinimumWeight uses Python identifier RearFacingMinimumWeight
    __RearFacingMinimumWeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'RearFacingMinimumWeight'), 'RearFacingMinimumWeight', '__AbsentNamespace0_CTD_ANON_15_RearFacingMinimumWeight', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4307, 4), )

    
    RearFacingMinimumWeight = property(__RearFacingMinimumWeight.value, __RearFacingMinimumWeight.set, None, None)

    _ElementMap.update({
        __RearFacingMaximumWeight.name() : __RearFacingMaximumWeight,
        __RearFacingMinimumWeight.name() : __RearFacingMinimumWeight
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_16 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4317, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element ShoulderHarnessMaximumHeight uses Python identifier ShoulderHarnessMaximumHeight
    __ShoulderHarnessMaximumHeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ShoulderHarnessMaximumHeight'), 'ShoulderHarnessMaximumHeight', '__AbsentNamespace0_CTD_ANON_16_ShoulderHarnessMaximumHeight', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4319, 4), )

    
    ShoulderHarnessMaximumHeight = property(__ShoulderHarnessMaximumHeight.value, __ShoulderHarnessMaximumHeight.set, None, None)

    
    # Element ShoulderHarnessMinimumHeight uses Python identifier ShoulderHarnessMinimumHeight
    __ShoulderHarnessMinimumHeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ShoulderHarnessMinimumHeight'), 'ShoulderHarnessMinimumHeight', '__AbsentNamespace0_CTD_ANON_16_ShoulderHarnessMinimumHeight', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4320, 4), )

    
    ShoulderHarnessMinimumHeight = property(__ShoulderHarnessMinimumHeight.value, __ShoulderHarnessMinimumHeight.set, None, None)

    _ElementMap.update({
        __ShoulderHarnessMaximumHeight.name() : __ShoulderHarnessMaximumHeight,
        __ShoulderHarnessMinimumHeight.name() : __ShoulderHarnessMinimumHeight
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_17 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 14, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element ProductType uses Python identifier ProductType
    __ProductType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ProductType'), 'ProductType', '__AbsentNamespace0_CTD_ANON_17_ProductType', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 16, 4), )

    
    ProductType = property(__ProductType.value, __ProductType.set, None, None)

    
    # Element BatteryTypeLithiumIon uses Python identifier BatteryTypeLithiumIon
    __BatteryTypeLithiumIon = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryTypeLithiumIon'), 'BatteryTypeLithiumIon', '__AbsentNamespace0_CTD_ANON_17_BatteryTypeLithiumIon', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 23, 4), )

    
    BatteryTypeLithiumIon = property(__BatteryTypeLithiumIon.value, __BatteryTypeLithiumIon.set, None, None)

    
    # Element BatteryTypeLithiumMetal uses Python identifier BatteryTypeLithiumMetal
    __BatteryTypeLithiumMetal = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryTypeLithiumMetal'), 'BatteryTypeLithiumMetal', '__AbsentNamespace0_CTD_ANON_17_BatteryTypeLithiumMetal', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 24, 4), )

    
    BatteryTypeLithiumMetal = property(__BatteryTypeLithiumMetal.value, __BatteryTypeLithiumMetal.set, None, None)

    
    # Element LithiumBatteryEnergyContent uses Python identifier LithiumBatteryEnergyContent
    __LithiumBatteryEnergyContent = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'LithiumBatteryEnergyContent'), 'LithiumBatteryEnergyContent', '__AbsentNamespace0_CTD_ANON_17_LithiumBatteryEnergyContent', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 25, 4), )

    
    LithiumBatteryEnergyContent = property(__LithiumBatteryEnergyContent.value, __LithiumBatteryEnergyContent.set, None, None)

    
    # Element LithiumBatteryPackaging uses Python identifier LithiumBatteryPackaging
    __LithiumBatteryPackaging = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'LithiumBatteryPackaging'), 'LithiumBatteryPackaging', '__AbsentNamespace0_CTD_ANON_17_LithiumBatteryPackaging', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 26, 4), )

    
    LithiumBatteryPackaging = property(__LithiumBatteryPackaging.value, __LithiumBatteryPackaging.set, None, None)

    
    # Element LithiumBatteryVoltage uses Python identifier LithiumBatteryVoltage
    __LithiumBatteryVoltage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'LithiumBatteryVoltage'), 'LithiumBatteryVoltage', '__AbsentNamespace0_CTD_ANON_17_LithiumBatteryVoltage', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 35, 4), )

    
    LithiumBatteryVoltage = property(__LithiumBatteryVoltage.value, __LithiumBatteryVoltage.set, None, None)

    
    # Element LithiumBatteryWeight uses Python identifier LithiumBatteryWeight
    __LithiumBatteryWeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'LithiumBatteryWeight'), 'LithiumBatteryWeight', '__AbsentNamespace0_CTD_ANON_17_LithiumBatteryWeight', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 36, 4), )

    
    LithiumBatteryWeight = property(__LithiumBatteryWeight.value, __LithiumBatteryWeight.set, None, None)

    
    # Element MedicineClassification uses Python identifier MedicineClassification
    __MedicineClassification = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MedicineClassification'), 'MedicineClassification', '__AbsentNamespace0_CTD_ANON_17_MedicineClassification', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 37, 4), )

    
    MedicineClassification = property(__MedicineClassification.value, __MedicineClassification.set, None, None)

    
    # Element NumberOfLithiumIonCells uses Python identifier NumberOfLithiumIonCells
    __NumberOfLithiumIonCells = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'NumberOfLithiumIonCells'), 'NumberOfLithiumIonCells', '__AbsentNamespace0_CTD_ANON_17_NumberOfLithiumIonCells', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 38, 4), )

    
    NumberOfLithiumIonCells = property(__NumberOfLithiumIonCells.value, __NumberOfLithiumIonCells.set, None, None)

    
    # Element NumberOfLithiumMetalCells uses Python identifier NumberOfLithiumMetalCells
    __NumberOfLithiumMetalCells = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'NumberOfLithiumMetalCells'), 'NumberOfLithiumMetalCells', '__AbsentNamespace0_CTD_ANON_17_NumberOfLithiumMetalCells', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 39, 4), )

    
    NumberOfLithiumMetalCells = property(__NumberOfLithiumMetalCells.value, __NumberOfLithiumMetalCells.set, None, None)

    
    # Element PlugType uses Python identifier PlugType
    __PlugType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PlugType'), 'PlugType', '__AbsentNamespace0_CTD_ANON_17_PlugType', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 41, 4), )

    
    PlugType = property(__PlugType.value, __PlugType.set, None, None)

    
    # Element SpecificUsesForProduct uses Python identifier SpecificUsesForProduct
    __SpecificUsesForProduct = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SpecificUsesForProduct'), 'SpecificUsesForProduct', '__AbsentNamespace0_CTD_ANON_17_SpecificUsesForProduct', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 42, 4), )

    
    SpecificUsesForProduct = property(__SpecificUsesForProduct.value, __SpecificUsesForProduct.set, None, None)

    
    # Element Certification uses Python identifier Certification
    __Certification = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Certification'), 'Certification', '__AbsentNamespace0_CTD_ANON_17_Certification', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 43, 4), )

    
    Certification = property(__Certification.value, __Certification.set, None, None)

    
    # Element SunProtection uses Python identifier SunProtection
    __SunProtection = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SunProtection'), 'SunProtection', '__AbsentNamespace0_CTD_ANON_17_SunProtection', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 44, 4), )

    
    SunProtection = property(__SunProtection.value, __SunProtection.set, None, None)

    
    # Element Voltage uses Python identifier Voltage
    __Voltage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Voltage'), 'Voltage', '__AbsentNamespace0_CTD_ANON_17_Voltage', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 45, 4), )

    
    Voltage = property(__Voltage.value, __Voltage.set, None, None)

    
    # Element Wattage uses Python identifier Wattage
    __Wattage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Wattage'), 'Wattage', '__AbsentNamespace0_CTD_ANON_17_Wattage', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 46, 4), )

    
    Wattage = property(__Wattage.value, __Wattage.set, None, None)

    _ElementMap.update({
        __ProductType.name() : __ProductType,
        __BatteryTypeLithiumIon.name() : __BatteryTypeLithiumIon,
        __BatteryTypeLithiumMetal.name() : __BatteryTypeLithiumMetal,
        __LithiumBatteryEnergyContent.name() : __LithiumBatteryEnergyContent,
        __LithiumBatteryPackaging.name() : __LithiumBatteryPackaging,
        __LithiumBatteryVoltage.name() : __LithiumBatteryVoltage,
        __LithiumBatteryWeight.name() : __LithiumBatteryWeight,
        __MedicineClassification.name() : __MedicineClassification,
        __NumberOfLithiumIonCells.name() : __NumberOfLithiumIonCells,
        __NumberOfLithiumMetalCells.name() : __NumberOfLithiumMetalCells,
        __PlugType.name() : __PlugType,
        __SpecificUsesForProduct.name() : __SpecificUsesForProduct,
        __Certification.name() : __Certification,
        __SunProtection.name() : __SunProtection,
        __Voltage.name() : __Voltage,
        __Wattage.name() : __Wattage
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_18 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 17, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element BeautyMisc uses Python identifier BeautyMisc
    __BeautyMisc = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'BeautyMisc'), 'BeautyMisc', '__AbsentNamespace0_CTD_ANON_18_BeautyMisc', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 50, 1), )

    
    BeautyMisc = property(__BeautyMisc.value, __BeautyMisc.set, None, None)

    _ElementMap.update({
        __BeautyMisc.name() : __BeautyMisc
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_19 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 51, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Battery uses Python identifier Battery
    __Battery = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Battery'), 'Battery', '__AbsentNamespace0_CTD_ANON_19_Battery', False, pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 190, 1), )

    
    Battery = property(__Battery.value, __Battery.set, None, None)

    
    # Element VariationData uses Python identifier VariationData
    __VariationData = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'VariationData'), 'VariationData', '__AbsentNamespace0_CTD_ANON_19_VariationData', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 53, 4), )

    
    VariationData = property(__VariationData.value, __VariationData.set, None, None)

    
    # Element UnitCount uses Python identifier UnitCount
    __UnitCount = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'UnitCount'), 'UnitCount', '__AbsentNamespace0_CTD_ANON_19_UnitCount', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 86, 4), )

    
    UnitCount = property(__UnitCount.value, __UnitCount.set, None, None)

    
    # Element Count uses Python identifier Count
    __Count = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Count'), 'Count', '__AbsentNamespace0_CTD_ANON_19_Count', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 101, 4), )

    
    Count = property(__Count.value, __Count.set, None, None)

    
    # Element NumberOfItems uses Python identifier NumberOfItems
    __NumberOfItems = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'NumberOfItems'), 'NumberOfItems', '__AbsentNamespace0_CTD_ANON_19_NumberOfItems', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 107, 4), )

    
    NumberOfItems = property(__NumberOfItems.value, __NumberOfItems.set, None, None)

    
    # Element BatteryAverageLife uses Python identifier BatteryAverageLife
    __BatteryAverageLife = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryAverageLife'), 'BatteryAverageLife', '__AbsentNamespace0_CTD_ANON_19_BatteryAverageLife', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 108, 4), )

    
    BatteryAverageLife = property(__BatteryAverageLife.value, __BatteryAverageLife.set, None, None)

    
    # Element BatteryCellComposition uses Python identifier BatteryCellComposition
    __BatteryCellComposition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryCellComposition'), 'BatteryCellComposition', '__AbsentNamespace0_CTD_ANON_19_BatteryCellComposition', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 109, 4), )

    
    BatteryCellComposition = property(__BatteryCellComposition.value, __BatteryCellComposition.set, None, None)

    
    # Element BatteryAverageLifeStandby uses Python identifier BatteryAverageLifeStandby
    __BatteryAverageLifeStandby = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryAverageLifeStandby'), 'BatteryAverageLifeStandby', '__AbsentNamespace0_CTD_ANON_19_BatteryAverageLifeStandby', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 110, 4), )

    
    BatteryAverageLifeStandby = property(__BatteryAverageLifeStandby.value, __BatteryAverageLifeStandby.set, None, None)

    
    # Element BatteryChargeTime uses Python identifier BatteryChargeTime
    __BatteryChargeTime = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryChargeTime'), 'BatteryChargeTime', '__AbsentNamespace0_CTD_ANON_19_BatteryChargeTime', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 112, 4), )

    
    BatteryChargeTime = property(__BatteryChargeTime.value, __BatteryChargeTime.set, None, None)

    
    # Element BatteryDescription uses Python identifier BatteryDescription
    __BatteryDescription = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryDescription'), 'BatteryDescription', '__AbsentNamespace0_CTD_ANON_19_BatteryDescription', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 113, 4), )

    
    BatteryDescription = property(__BatteryDescription.value, __BatteryDescription.set, None, None)

    
    # Element BatteryFormFactor uses Python identifier BatteryFormFactor
    __BatteryFormFactor = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryFormFactor'), 'BatteryFormFactor', '__AbsentNamespace0_CTD_ANON_19_BatteryFormFactor', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 114, 4), )

    
    BatteryFormFactor = property(__BatteryFormFactor.value, __BatteryFormFactor.set, None, None)

    
    # Element BatteryPower uses Python identifier BatteryPower
    __BatteryPower = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BatteryPower'), 'BatteryPower', '__AbsentNamespace0_CTD_ANON_19_BatteryPower', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 115, 4), )

    
    BatteryPower = property(__BatteryPower.value, __BatteryPower.set, None, None)

    
    # Element DisplayLength uses Python identifier DisplayLength
    __DisplayLength = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'DisplayLength'), 'DisplayLength', '__AbsentNamespace0_CTD_ANON_19_DisplayLength', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 116, 4), )

    
    DisplayLength = property(__DisplayLength.value, __DisplayLength.set, None, None)

    
    # Element DisplayWeight uses Python identifier DisplayWeight
    __DisplayWeight = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'DisplayWeight'), 'DisplayWeight', '__AbsentNamespace0_CTD_ANON_19_DisplayWeight', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 117, 4), )

    
    DisplayWeight = property(__DisplayWeight.value, __DisplayWeight.set, None, None)

    
    # Element DisplayVolume uses Python identifier DisplayVolume
    __DisplayVolume = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'DisplayVolume'), 'DisplayVolume', '__AbsentNamespace0_CTD_ANON_19_DisplayVolume', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 118, 4), )

    
    DisplayVolume = property(__DisplayVolume.value, __DisplayVolume.set, None, None)

    
    # Element SkinType uses Python identifier SkinType
    __SkinType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SkinType'), 'SkinType', '__AbsentNamespace0_CTD_ANON_19_SkinType', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 119, 4), )

    
    SkinType = property(__SkinType.value, __SkinType.set, None, None)

    
    # Element SkinTone uses Python identifier SkinTone
    __SkinTone = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SkinTone'), 'SkinTone', '__AbsentNamespace0_CTD_ANON_19_SkinTone', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 120, 4), )

    
    SkinTone = property(__SkinTone.value, __SkinTone.set, None, None)

    
    # Element HairType uses Python identifier HairType
    __HairType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'HairType'), 'HairType', '__AbsentNamespace0_CTD_ANON_19_HairType', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 121, 4), )

    
    HairType = property(__HairType.value, __HairType.set, None, None)

    
    # Element IncludedComponents uses Python identifier IncludedComponents
    __IncludedComponents = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'IncludedComponents'), 'IncludedComponents', '__AbsentNamespace0_CTD_ANON_19_IncludedComponents', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 122, 4), )

    
    IncludedComponents = property(__IncludedComponents.value, __IncludedComponents.set, None, None)

    
    # Element Ingredients uses Python identifier Ingredients
    __Ingredients = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Ingredients'), 'Ingredients', '__AbsentNamespace0_CTD_ANON_19_Ingredients', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 123, 4), )

    
    Ingredients = property(__Ingredients.value, __Ingredients.set, None, None)

    
    # Element ManufacturerWarrantyType uses Python identifier ManufacturerWarrantyType
    __ManufacturerWarrantyType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ManufacturerWarrantyType'), 'ManufacturerWarrantyType', '__AbsentNamespace0_CTD_ANON_19_ManufacturerWarrantyType', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 130, 4), )

    
    ManufacturerWarrantyType = property(__ManufacturerWarrantyType.value, __ManufacturerWarrantyType.set, None, None)

    
    # Element MaterialType uses Python identifier MaterialType
    __MaterialType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MaterialType'), 'MaterialType', '__AbsentNamespace0_CTD_ANON_19_MaterialType', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 131, 4), )

    
    MaterialType = property(__MaterialType.value, __MaterialType.set, None, None)

    
    # Element MfrWarrantyDescriptionLabor uses Python identifier MfrWarrantyDescriptionLabor
    __MfrWarrantyDescriptionLabor = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MfrWarrantyDescriptionLabor'), 'MfrWarrantyDescriptionLabor', '__AbsentNamespace0_CTD_ANON_19_MfrWarrantyDescriptionLabor', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 133, 4), )

    
    MfrWarrantyDescriptionLabor = property(__MfrWarrantyDescriptionLabor.value, __MfrWarrantyDescriptionLabor.set, None, None)

    
    # Element MfrWarrantyDescriptionParts uses Python identifier MfrWarrantyDescriptionParts
    __MfrWarrantyDescriptionParts = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'MfrWarrantyDescriptionParts'), 'MfrWarrantyDescriptionParts', '__AbsentNamespace0_CTD_ANON_19_MfrWarrantyDescriptionParts', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 135, 4), )

    
    MfrWarrantyDescriptionParts = property(__MfrWarrantyDescriptionParts.value, __MfrWarrantyDescriptionParts.set, None, None)

    
    # Element ModelName uses Python identifier ModelName
    __ModelName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ModelName'), 'ModelName', '__AbsentNamespace0_CTD_ANON_19_ModelName', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 137, 4), )

    
    ModelName = property(__ModelName.value, __ModelName.set, None, None)

    
    # Element Indications uses Python identifier Indications
    __Indications = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Indications'), 'Indications', '__AbsentNamespace0_CTD_ANON_19_Indications', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 138, 4), )

    
    Indications = property(__Indications.value, __Indications.set, None, None)

    
    # Element Directions uses Python identifier Directions
    __Directions = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Directions'), 'Directions', '__AbsentNamespace0_CTD_ANON_19_Directions', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 139, 4), )

    
    Directions = property(__Directions.value, __Directions.set, None, None)

    
    # Element Warnings uses Python identifier Warnings
    __Warnings = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Warnings'), 'Warnings', '__AbsentNamespace0_CTD_ANON_19_Warnings', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 140, 4), )

    
    Warnings = property(__Warnings.value, __Warnings.set, None, None)

    
    # Element ItemForm uses Python identifier ItemForm
    __ItemForm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ItemForm'), 'ItemForm', '__AbsentNamespace0_CTD_ANON_19_ItemForm', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 141, 4), )

    
    ItemForm = property(__ItemForm.value, __ItemForm.set, None, None)

    
    # Element ItemPackageQuantity uses Python identifier ItemPackageQuantity
    __ItemPackageQuantity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ItemPackageQuantity'), 'ItemPackageQuantity', '__AbsentNamespace0_CTD_ANON_19_ItemPackageQuantity', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 142, 4), )

    
    ItemPackageQuantity = property(__ItemPackageQuantity.value, __ItemPackageQuantity.set, None, None)

    
    # Element Flavor uses Python identifier Flavor
    __Flavor = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Flavor'), 'Flavor', '__AbsentNamespace0_CTD_ANON_19_Flavor', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 143, 4), )

    
    Flavor = property(__Flavor.value, __Flavor.set, None, None)

    
    # Element Coverage uses Python identifier Coverage
    __Coverage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Coverage'), 'Coverage', '__AbsentNamespace0_CTD_ANON_19_Coverage', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 144, 4), )

    
    Coverage = property(__Coverage.value, __Coverage.set, None, None)

    
    # Element FinishType uses Python identifier FinishType
    __FinishType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'FinishType'), 'FinishType', '__AbsentNamespace0_CTD_ANON_19_FinishType', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 145, 4), )

    
    FinishType = property(__FinishType.value, __FinishType.set, None, None)

    
    # Element ItemSpecialty uses Python identifier ItemSpecialty
    __ItemSpecialty = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ItemSpecialty'), 'ItemSpecialty', '__AbsentNamespace0_CTD_ANON_19_ItemSpecialty', True, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 146, 4), )

    
    ItemSpecialty = property(__ItemSpecialty.value, __ItemSpecialty.set, None, None)

    
    # Element PatternName uses Python identifier PatternName
    __PatternName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PatternName'), 'PatternName', '__AbsentNamespace0_CTD_ANON_19_PatternName', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 147, 4), )

    
    PatternName = property(__PatternName.value, __PatternName.set, None, None)

    
    # Element PowerSource uses Python identifier PowerSource
    __PowerSource = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'PowerSource'), 'PowerSource', '__AbsentNamespace0_CTD_ANON_19_PowerSource', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 148, 4), )

    
    PowerSource = property(__PowerSource.value, __PowerSource.set, None, None)

    
    # Element IsAdultProduct uses Python identifier IsAdultProduct
    __IsAdultProduct = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'IsAdultProduct'), 'IsAdultProduct', '__AbsentNamespace0_CTD_ANON_19_IsAdultProduct', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 149, 4), )

    
    IsAdultProduct = property(__IsAdultProduct.value, __IsAdultProduct.set, None, None)

    
    # Element TargetGender uses Python identifier TargetGender
    __TargetGender = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'TargetGender'), 'TargetGender', '__AbsentNamespace0_CTD_ANON_19_TargetGender', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 150, 4), )

    
    TargetGender = property(__TargetGender.value, __TargetGender.set, None, None)

    
    # Element CountryOfOrigin uses Python identifier CountryOfOrigin
    __CountryOfOrigin = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'CountryOfOrigin'), 'CountryOfOrigin', '__AbsentNamespace0_CTD_ANON_19_CountryOfOrigin', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 159, 4), )

    
    CountryOfOrigin = property(__CountryOfOrigin.value, __CountryOfOrigin.set, None, None)

    
    # Element SellerWarrantyDescription uses Python identifier SellerWarrantyDescription
    __SellerWarrantyDescription = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SellerWarrantyDescription'), 'SellerWarrantyDescription', '__AbsentNamespace0_CTD_ANON_19_SellerWarrantyDescription', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 166, 4), )

    
    SellerWarrantyDescription = property(__SellerWarrantyDescription.value, __SellerWarrantyDescription.set, None, None)

    
    # Element SizeMap uses Python identifier SizeMap
    __SizeMap = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SizeMap'), 'SizeMap', '__AbsentNamespace0_CTD_ANON_19_SizeMap', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 167, 4), )

    
    SizeMap = property(__SizeMap.value, __SizeMap.set, None, None)

    
    # Element BrandRegionOfOrigin uses Python identifier BrandRegionOfOrigin
    __BrandRegionOfOrigin = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'BrandRegionOfOrigin'), 'BrandRegionOfOrigin', '__AbsentNamespace0_CTD_ANON_19_BrandRegionOfOrigin', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 169, 4), )

    
    BrandRegionOfOrigin = property(__BrandRegionOfOrigin.value, __BrandRegionOfOrigin.set, None, None)

    
    # Element SpecialFeatures uses Python identifier SpecialFeatures
    __SpecialFeatures = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'SpecialFeatures'), 'SpecialFeatures', '__AbsentNamespace0_CTD_ANON_19_SpecialFeatures', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 170, 4), )

    
    SpecialFeatures = property(__SpecialFeatures.value, __SpecialFeatures.set, None, None)

    
    # Element TargetAudienceBase uses Python identifier TargetAudienceBase
    __TargetAudienceBase = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'TargetAudienceBase'), 'TargetAudienceBase', '__AbsentNamespace0_CTD_ANON_19_TargetAudienceBase', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 171, 4), )

    
    TargetAudienceBase = property(__TargetAudienceBase.value, __TargetAudienceBase.set, None, None)

    _ElementMap.update({
        __Battery.name() : __Battery,
        __VariationData.name() : __VariationData,
        __UnitCount.name() : __UnitCount,
        __Count.name() : __Count,
        __NumberOfItems.name() : __NumberOfItems,
        __BatteryAverageLife.name() : __BatteryAverageLife,
        __BatteryCellComposition.name() : __BatteryCellComposition,
        __BatteryAverageLifeStandby.name() : __BatteryAverageLifeStandby,
        __BatteryChargeTime.name() : __BatteryChargeTime,
        __BatteryDescription.name() : __BatteryDescription,
        __BatteryFormFactor.name() : __BatteryFormFactor,
        __BatteryPower.name() : __BatteryPower,
        __DisplayLength.name() : __DisplayLength,
        __DisplayWeight.name() : __DisplayWeight,
        __DisplayVolume.name() : __DisplayVolume,
        __SkinType.name() : __SkinType,
        __SkinTone.name() : __SkinTone,
        __HairType.name() : __HairType,
        __IncludedComponents.name() : __IncludedComponents,
        __Ingredients.name() : __Ingredients,
        __ManufacturerWarrantyType.name() : __ManufacturerWarrantyType,
        __MaterialType.name() : __MaterialType,
        __MfrWarrantyDescriptionLabor.name() : __MfrWarrantyDescriptionLabor,
        __MfrWarrantyDescriptionParts.name() : __MfrWarrantyDescriptionParts,
        __ModelName.name() : __ModelName,
        __Indications.name() : __Indications,
        __Directions.name() : __Directions,
        __Warnings.name() : __Warnings,
        __ItemForm.name() : __ItemForm,
        __ItemPackageQuantity.name() : __ItemPackageQuantity,
        __Flavor.name() : __Flavor,
        __Coverage.name() : __Coverage,
        __FinishType.name() : __FinishType,
        __ItemSpecialty.name() : __ItemSpecialty,
        __PatternName.name() : __PatternName,
        __PowerSource.name() : __PowerSource,
        __IsAdultProduct.name() : __IsAdultProduct,
        __TargetGender.name() : __TargetGender,
        __CountryOfOrigin.name() : __CountryOfOrigin,
        __SellerWarrantyDescription.name() : __SellerWarrantyDescription,
        __SizeMap.name() : __SizeMap,
        __BrandRegionOfOrigin.name() : __BrandRegionOfOrigin,
        __SpecialFeatures.name() : __SpecialFeatures,
        __TargetAudienceBase.name() : __TargetAudienceBase
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_20 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 54, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element Parentage uses Python identifier Parentage
    __Parentage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Parentage'), 'Parentage', '__AbsentNamespace0_CTD_ANON_20_Parentage', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 56, 7), )

    
    Parentage = property(__Parentage.value, __Parentage.set, None, None)

    
    # Element VariationTheme uses Python identifier VariationTheme
    __VariationTheme = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'VariationTheme'), 'VariationTheme', '__AbsentNamespace0_CTD_ANON_20_VariationTheme', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 64, 7), )

    
    VariationTheme = property(__VariationTheme.value, __VariationTheme.set, None, None)

    
    # Element Size uses Python identifier Size
    __Size = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Size'), 'Size', '__AbsentNamespace0_CTD_ANON_20_Size', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 78, 7), )

    
    Size = property(__Size.value, __Size.set, None, None)

    
    # Element Color uses Python identifier Color
    __Color = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Color'), 'Color', '__AbsentNamespace0_CTD_ANON_20_Color', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 79, 7), )

    
    Color = property(__Color.value, __Color.set, None, None)

    
    # Element ColorMap uses Python identifier ColorMap
    __ColorMap = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'ColorMap'), 'ColorMap', '__AbsentNamespace0_CTD_ANON_20_ColorMap', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 80, 7), )

    
    ColorMap = property(__ColorMap.value, __ColorMap.set, None, None)

    
    # Element Scent uses Python identifier Scent
    __Scent = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Scent'), 'Scent', '__AbsentNamespace0_CTD_ANON_20_Scent', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 81, 7), )

    
    Scent = property(__Scent.value, __Scent.set, None, None)

    
    # Element Capacity uses Python identifier Capacity
    __Capacity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'Capacity'), 'Capacity', '__AbsentNamespace0_CTD_ANON_20_Capacity', False, pyxb.utils.utility.Location('/tmp/myoutput.xsd', 82, 7), )

    
    Capacity = property(__Capacity.value, __Capacity.set, None, None)

    _ElementMap.update({
        __Parentage.name() : __Parentage,
        __VariationTheme.name() : __VariationTheme,
        __Size.name() : __Size,
        __Color.name() : __Color,
        __ColorMap.name() : __ColorMap,
        __Scent.name() : __Scent,
        __Capacity.name() : __Capacity
    })
    _AttributeMap.update({
        
    })



# Complex type PhoneNumberType with content type SIMPLE
class PhoneNumberType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PhoneNumberType with content type SIMPLE"""
    _TypeDefinition = String
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PhoneNumberType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 113, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is String
    
    # Attribute Type uses Python identifier Type
    __Type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Type'), 'Type', '__AbsentNamespace0_PhoneNumberType_Type', STD_ANON_3)
    __Type._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 116, 4)
    __Type._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 116, 4)
    
    Type = property(__Type.value, __Type.set, None, 'Defaults to "Voice." Currently, only two voice numbers\n\t\t\t\t\t\t\tand one fax number are stored.')

    
    # Attribute Description uses Python identifier Description
    __Description = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Description'), 'Description', '__AbsentNamespace0_PhoneNumberType_Description', STD_ANON_4)
    __Description._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 128, 4)
    __Description._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 128, 4)
    
    Description = property(__Description.value, __Description.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __Type.name() : __Type,
        __Description.name() : __Description
    })
Namespace.addCategoryObject('typeBinding', 'PhoneNumberType', PhoneNumberType)


# Complex type EmailAddressType with content type SIMPLE
class EmailAddressType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type EmailAddressType with content type SIMPLE"""
    _TypeDefinition = EmailBase
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EmailAddressType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 145, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is EmailBase
    
    # Attribute PreferredFormat uses Python identifier PreferredFormat
    __PreferredFormat = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'PreferredFormat'), 'PreferredFormat', '__AbsentNamespace0_EmailAddressType_PreferredFormat', STD_ANON_5)
    __PreferredFormat._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 148, 4)
    __PreferredFormat._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 148, 4)
    
    PreferredFormat = property(__PreferredFormat.value, __PreferredFormat.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __PreferredFormat.name() : __PreferredFormat
    })
Namespace.addCategoryObject('typeBinding', 'EmailAddressType', EmailAddressType)


# Complex type CurrencyAmount with content type SIMPLE
class CurrencyAmount (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CurrencyAmount with content type SIMPLE"""
    _TypeDefinition = BaseCurrencyAmount
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CurrencyAmount')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 296, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is BaseCurrencyAmount
    
    # Attribute currency uses Python identifier currency
    __currency = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'currency'), 'currency', '__AbsentNamespace0_CurrencyAmount_currency', BaseCurrencyCode, required=True)
    __currency._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 299, 4)
    __currency._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 299, 4)
    
    currency = property(__currency.value, __currency.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __currency.name() : __currency
    })
Namespace.addCategoryObject('typeBinding', 'CurrencyAmount', CurrencyAmount)


# Complex type PositiveCurrencyAmount with content type SIMPLE
class PositiveCurrencyAmount (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PositiveCurrencyAmount with content type SIMPLE"""
    _TypeDefinition = BasePositiveCurrencyAmount
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PositiveCurrencyAmount')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 303, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is BasePositiveCurrencyAmount
    
    # Attribute currency uses Python identifier currency
    __currency = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'currency'), 'currency', '__AbsentNamespace0_PositiveCurrencyAmount_currency', BaseCurrencyCode, required=True)
    __currency._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 306, 4)
    __currency._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 306, 4)
    
    currency = property(__currency.value, __currency.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __currency.name() : __currency
    })
Namespace.addCategoryObject('typeBinding', 'PositiveCurrencyAmount', PositiveCurrencyAmount)


# Complex type EnergyRatingType with content type SIMPLE
class EnergyRatingType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type EnergyRatingType with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnergyRatingType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2600, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_EnergyRatingType_unitOfMeasure', EnergyUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2603, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2603, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'EnergyRatingType', EnergyRatingType)


# Complex type AreaDimension with content type SIMPLE
class AreaDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AreaDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AreaDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2614, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AreaDimension_unitOfMeasure', AreaUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2617, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2617, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AreaDimension', AreaDimension)


# Complex type AreaDimensionOptionalUnit with content type SIMPLE
class AreaDimensionOptionalUnit (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AreaDimensionOptionalUnit with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AreaDimensionOptionalUnit')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2621, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AreaDimensionOptionalUnit_unitOfMeasure', AreaUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2624, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2624, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AreaDimensionOptionalUnit', AreaDimensionOptionalUnit)


# Complex type AirFlowDisplacementDimension with content type SIMPLE
class AirFlowDisplacementDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AirFlowDisplacementDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AirFlowDisplacementDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2628, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AirFlowDisplacementDimension_unitOfMeasure', AirFlowDisplacementUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2631, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2631, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AirFlowDisplacementDimension', AirFlowDisplacementDimension)


# Complex type BurnTimeDimension with content type SIMPLE
class BurnTimeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type BurnTimeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BurnTimeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2635, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_BurnTimeDimension_unitOfMeasure', BurnTimeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2638, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2638, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'BurnTimeDimension', BurnTimeDimension)


# Complex type CurencyDimension with content type SIMPLE
class CurencyDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CurencyDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CurencyDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2642, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_CurencyDimension_unitOfMeasure', GlobalCurrencyCode, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2645, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2645, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'CurencyDimension', CurencyDimension)


# Complex type LengthDimension with content type SIMPLE
class LengthDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LengthDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LengthDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2649, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LengthDimension_unitOfMeasure', LengthUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2652, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2652, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LengthDimension', LengthDimension)


# Complex type LengthDimensionOptionalUnit with content type SIMPLE
class LengthDimensionOptionalUnit (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LengthDimensionOptionalUnit with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LengthDimensionOptionalUnit')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2656, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LengthDimensionOptionalUnit_unitOfMeasure', LengthUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2659, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2659, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LengthDimensionOptionalUnit', LengthDimensionOptionalUnit)


# Complex type LengthIntegerDimension with content type SIMPLE
class LengthIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LengthIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LengthIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2663, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LengthIntegerDimension_unitOfMeasure', LengthUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2666, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2666, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LengthIntegerDimension', LengthIntegerDimension)


# Complex type OptionalLengthIntegerDimension with content type SIMPLE
class OptionalLengthIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type OptionalLengthIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OptionalLengthIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2670, 7)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_OptionalLengthIntegerDimension_unitOfMeasure', LengthUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2673, 32)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2673, 32)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'OptionalLengthIntegerDimension', OptionalLengthIntegerDimension)


# Complex type LuminancePositiveIntegerDimension with content type SIMPLE
class LuminancePositiveIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LuminancePositiveIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LuminancePositiveIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2677, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LuminancePositiveIntegerDimension_unitOfMeasure', LuminanceUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2680, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2680, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LuminancePositiveIntegerDimension', LuminancePositiveIntegerDimension)


# Complex type LuminanceIntegerDimension with content type SIMPLE
class LuminanceIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LuminanceIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.nonNegativeInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LuminanceIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2684, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.nonNegativeInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LuminanceIntegerDimension_unitOfMeasure', LuminanceUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2687, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2687, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LuminanceIntegerDimension', LuminanceIntegerDimension)


# Complex type LuminanceDimension with content type SIMPLE
class LuminanceDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LuminanceDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LuminanceDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2691, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LuminanceDimension_unitOfMeasure', LuminanceUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2694, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2694, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LuminanceDimension', LuminanceDimension)


# Complex type VolumeDimension with content type SIMPLE
class VolumeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VolumeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2698, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VolumeDimension_unitOfMeasure', VolumeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2701, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2701, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VolumeDimension', VolumeDimension)


# Complex type VolumeIntegerDimension with content type SIMPLE
class VolumeIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VolumeIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2705, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VolumeIntegerDimension_unitOfMeasure', VolumeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2708, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2708, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VolumeIntegerDimension', VolumeIntegerDimension)


# Complex type VolumeRateDimension with content type SIMPLE
class VolumeRateDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VolumeRateDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeRateDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2712, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VolumeRateDimension_unitOfMeasure', VolumeRateUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2715, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2715, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VolumeRateDimension', VolumeRateDimension)


# Complex type WeightDimension with content type SIMPLE
class WeightDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type WeightDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WeightDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2719, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_WeightDimension_unitOfMeasure', WeightUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2722, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2722, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'WeightDimension', WeightDimension)


# Complex type WeightIntegerDimension with content type SIMPLE
class WeightIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type WeightIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WeightIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2726, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_WeightIntegerDimension_unitOfMeasure', WeightUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2729, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2729, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'WeightIntegerDimension', WeightIntegerDimension)


# Complex type JewelryLengthDimension with content type SIMPLE
class JewelryLengthDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type JewelryLengthDimension with content type SIMPLE"""
    _TypeDefinition = FourDecimal
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'JewelryLengthDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2733, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is FourDecimal
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_JewelryLengthDimension_unitOfMeasure', JewelryLengthUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2736, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2736, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'JewelryLengthDimension', JewelryLengthDimension)


# Complex type JewelryWeightDimension with content type SIMPLE
class JewelryWeightDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type JewelryWeightDimension with content type SIMPLE"""
    _TypeDefinition = FourDecimal
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'JewelryWeightDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2740, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is FourDecimal
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_JewelryWeightDimension_unitOfMeasure', JewelryWeightUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2743, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2743, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'JewelryWeightDimension', JewelryWeightDimension)


# Complex type PositiveWeightDimension with content type SIMPLE
class PositiveWeightDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PositiveWeightDimension with content type SIMPLE"""
    _TypeDefinition = PositiveDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PositiveWeightDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2747, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_PositiveWeightDimension_unitOfMeasure', WeightUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2750, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2750, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'PositiveWeightDimension', PositiveWeightDimension)


# Complex type PositiveNonZeroWeightDimension with content type SIMPLE
class PositiveNonZeroWeightDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PositiveNonZeroWeightDimension with content type SIMPLE"""
    _TypeDefinition = PositiveNonZeroDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PositiveNonZeroWeightDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2754, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveNonZeroDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_PositiveNonZeroWeightDimension_unitOfMeasure', WeightUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2757, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2757, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'PositiveNonZeroWeightDimension', PositiveNonZeroWeightDimension)


# Complex type DegreeDimension with content type SIMPLE
class DegreeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type DegreeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DegreeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2761, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_DegreeDimension_unitOfMeasure', DegreeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2764, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2764, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'DegreeDimension', DegreeDimension)


# Complex type MemorySizeDimension with content type SIMPLE
class MemorySizeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type MemorySizeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MemorySizeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2768, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_MemorySizeDimension_unitOfMeasure', MemorySizeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2771, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2771, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'MemorySizeDimension', MemorySizeDimension)


# Complex type MemorySizeIntegerDimension with content type SIMPLE
class MemorySizeIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type MemorySizeIntegerDimension with content type SIMPLE"""
    _TypeDefinition = PositiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MemorySizeIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2775, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_MemorySizeIntegerDimension_unitOfMeasure', MemorySizeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2778, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2778, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'MemorySizeIntegerDimension', MemorySizeIntegerDimension)


# Complex type FrequencyDimension with content type SIMPLE
class FrequencyDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type FrequencyDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FrequencyDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2782, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_FrequencyDimension_unitOfMeasure', FrequencyUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2785, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2785, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'FrequencyDimension', FrequencyDimension)


# Complex type FrequencyIntegerDimension with content type SIMPLE
class FrequencyIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type FrequencyIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FrequencyIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2789, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_FrequencyIntegerDimension_unitOfMeasure', FrequencyUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2792, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2792, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'FrequencyIntegerDimension', FrequencyIntegerDimension)


# Complex type AmperageDimension with content type SIMPLE
class AmperageDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AmperageDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AmperageDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2796, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AmperageDimension_unitOfMeasure', AmperageUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2799, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2799, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AmperageDimension', AmperageDimension)


# Complex type ResistanceDimension with content type SIMPLE
class ResistanceDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ResistanceDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ResistanceDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2803, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ResistanceDimension_unitOfMeasure', ResistanceUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2806, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2806, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ResistanceDimension', ResistanceDimension)


# Complex type TimeDimension with content type SIMPLE
class TimeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type TimeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TimeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2810, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_TimeDimension_unitOfMeasure', TimeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2813, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2813, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'TimeDimension', TimeDimension)


# Complex type StringTimeDimension with content type SIMPLE
class StringTimeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type StringTimeDimension with content type SIMPLE"""
    _TypeDefinition = StringNotNull
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StringTimeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2817, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is StringNotNull
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_StringTimeDimension_unitOfMeasure', TimeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2820, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2820, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'StringTimeDimension', StringTimeDimension)


# Complex type BatteryLifeDimension with content type SIMPLE
class BatteryLifeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type BatteryLifeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BatteryLifeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2824, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_BatteryLifeDimension_unitOfMeasure', BatteryAverageLifeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2827, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2827, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'BatteryLifeDimension', BatteryLifeDimension)


# Complex type TimeIntegerDimension with content type SIMPLE
class TimeIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type TimeIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TimeIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2831, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_TimeIntegerDimension_unitOfMeasure', TimeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2834, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2834, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'TimeIntegerDimension', TimeIntegerDimension)


# Complex type DateIntegerDimension with content type SIMPLE
class DateIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type DateIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DateIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2838, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_DateIntegerDimension_unitOfMeasure', DateUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2841, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2841, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'DateIntegerDimension', DateIntegerDimension)


# Complex type SubscriptionTermDimension with content type SIMPLE
class SubscriptionTermDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type SubscriptionTermDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SubscriptionTermDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2845, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_SubscriptionTermDimension_unitOfMeasure', DateUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2848, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2848, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'SubscriptionTermDimension', SubscriptionTermDimension)


# Complex type SunProtectionDimension with content type SIMPLE
class SunProtectionDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type SunProtectionDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SunProtectionDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2852, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_SunProtectionDimension_unitOfMeasure', SunProtectionUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2855, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2855, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'SunProtectionDimension', SunProtectionDimension)


# Complex type AssemblyTimeDimension with content type SIMPLE
class AssemblyTimeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AssemblyTimeDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AssemblyTimeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2859, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AssemblyTimeDimension_unitOfMeasure', AssemblyTimeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2862, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2862, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AssemblyTimeDimension', AssemblyTimeDimension)


# Complex type AgeRecommendedDimension with content type SIMPLE
class AgeRecommendedDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AgeRecommendedDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AgeRecommendedDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2866, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AgeRecommendedDimension_unitOfMeasure', AgeRecommendedUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2869, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2869, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AgeRecommendedDimension', AgeRecommendedDimension)


# Complex type MinimumAgeRecommendedDimension with content type SIMPLE
class MinimumAgeRecommendedDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type MinimumAgeRecommendedDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.nonNegativeInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MinimumAgeRecommendedDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2873, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.nonNegativeInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_MinimumAgeRecommendedDimension_unitOfMeasure', AgeRecommendedUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2876, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2876, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'MinimumAgeRecommendedDimension', MinimumAgeRecommendedDimension)


# Complex type BatteryPowerIntegerDimension with content type SIMPLE
class BatteryPowerIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type BatteryPowerIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BatteryPowerIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2880, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_BatteryPowerIntegerDimension_unitOfMeasure', BatteryPowerUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2883, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2883, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'BatteryPowerIntegerDimension', BatteryPowerIntegerDimension)


# Complex type BatteryPowerDimension with content type SIMPLE
class BatteryPowerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type BatteryPowerDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BatteryPowerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2887, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_BatteryPowerDimension_unitOfMeasure', BatteryPowerUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2890, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2890, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'BatteryPowerDimension', BatteryPowerDimension)


# Complex type LuminiousIntensityDimension with content type SIMPLE
class LuminiousIntensityDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LuminiousIntensityDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LuminiousIntensityDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2894, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_LuminiousIntensityDimension_unitOfMeasure', LuminousIntensityUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2897, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2897, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'LuminiousIntensityDimension', LuminiousIntensityDimension)


# Complex type VoltageDecimalDimension with content type SIMPLE
class VoltageDecimalDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VoltageDecimalDimension with content type SIMPLE"""
    _TypeDefinition = PositiveDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VoltageDecimalDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2901, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VoltageDecimalDimension_unitOfMeasure', VoltageUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2904, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2904, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VoltageDecimalDimension', VoltageDecimalDimension)


# Complex type VoltageIntegerDimension with content type SIMPLE
class VoltageIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VoltageIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VoltageIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2908, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VoltageIntegerDimension_unitOfMeasure', VoltageUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2911, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2911, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VoltageIntegerDimension', VoltageIntegerDimension)


# Complex type VoltageIntegerDimensionOptionalUnit with content type SIMPLE
class VoltageIntegerDimensionOptionalUnit (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VoltageIntegerDimensionOptionalUnit with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VoltageIntegerDimensionOptionalUnit')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2915, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VoltageIntegerDimensionOptionalUnit_unitOfMeasure', VoltageUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2918, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2918, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VoltageIntegerDimensionOptionalUnit', VoltageIntegerDimensionOptionalUnit)


# Complex type WattageIntegerDimension with content type SIMPLE
class WattageIntegerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type WattageIntegerDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WattageIntegerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2922, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_WattageIntegerDimension_unitOfMeasure', WattageUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2925, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2925, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'WattageIntegerDimension', WattageIntegerDimension)


# Complex type WattageDimension with content type SIMPLE
class WattageDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type WattageDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WattageDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2929, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_WattageDimension_unitOfMeasure', WattageUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2932, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2932, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'WattageDimension', WattageDimension)


# Complex type WattageDimensionOptionalUnit with content type SIMPLE
class WattageDimensionOptionalUnit (pyxb.binding.basis.complexTypeDefinition):
    """Complex type WattageDimensionOptionalUnit with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WattageDimensionOptionalUnit')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2936, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_WattageDimensionOptionalUnit_unitOfMeasure', WattageUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2939, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2939, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'WattageDimensionOptionalUnit', WattageDimensionOptionalUnit)


# Complex type MillimeterDecimalDimension with content type SIMPLE
class MillimeterDecimalDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type MillimeterDecimalDimension with content type SIMPLE"""
    _TypeDefinition = PositiveDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MillimeterDecimalDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2943, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_MillimeterDecimalDimension_unitOfMeasure', MillimeterUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2946, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2946, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'MillimeterDecimalDimension', MillimeterDecimalDimension)


# Complex type NoiseLevelDimension with content type SIMPLE
class NoiseLevelDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type NoiseLevelDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NoiseLevelDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2950, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_NoiseLevelDimension_unitOfMeasure', NoiseLevelUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2953, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2953, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'NoiseLevelDimension', NoiseLevelDimension)


# Complex type TemperatureDimension with content type SIMPLE
class TemperatureDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type TemperatureDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TemperatureDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2957, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_TemperatureDimension_unitOfMeasure', TemperatureUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2960, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2960, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'TemperatureDimension', TemperatureDimension)


# Complex type StringTemperatureDimension with content type SIMPLE
class StringTemperatureDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type StringTemperatureDimension with content type SIMPLE"""
    _TypeDefinition = StringNotNull
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StringTemperatureDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2964, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is StringNotNull
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_StringTemperatureDimension_unitOfMeasure', TemperatureUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2967, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2967, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'StringTemperatureDimension', StringTemperatureDimension)


# Complex type TemperatureRatingDimension with content type SIMPLE
class TemperatureRatingDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type TemperatureRatingDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TemperatureRatingDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2971, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_TemperatureRatingDimension_unitOfMeasure', TemperatureRatingUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2974, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2974, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'TemperatureRatingDimension', TemperatureRatingDimension)


# Complex type ClothingSizeDimension with content type SIMPLE
class ClothingSizeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ClothingSizeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ClothingSizeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2978, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ClothingSizeDimension_unitOfMeasure', ClothingSizeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2981, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2981, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ClothingSizeDimension', ClothingSizeDimension)


# Complex type StringLengthOptionalDimension with content type SIMPLE
class StringLengthOptionalDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type StringLengthOptionalDimension with content type SIMPLE"""
    _TypeDefinition = StringNotNull
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StringLengthOptionalDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2985, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is StringNotNull
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_StringLengthOptionalDimension_unitOfMeasure', LengthUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2988, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2988, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'StringLengthOptionalDimension', StringLengthOptionalDimension)


# Complex type StringLengthDimension with content type SIMPLE
class StringLengthDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type StringLengthDimension with content type SIMPLE"""
    _TypeDefinition = StringNotNull
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StringLengthDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2993, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is StringNotNull
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_StringLengthDimension_unitOfMeasure', LengthUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2996, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2996, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'StringLengthDimension', StringLengthDimension)


# Complex type CurrentDimension with content type SIMPLE
class CurrentDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CurrentDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CurrentDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3000, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_CurrentDimension_unitOfMeasure', CurrentUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3003, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3003, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'CurrentDimension', CurrentDimension)


# Complex type ForceDimension with content type SIMPLE
class ForceDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ForceDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ForceDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3021, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ForceDimension_unitOfMeasure', ForceUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3024, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3024, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ForceDimension', ForceDimension)


# Complex type HardnessDimension with content type SIMPLE
class HardnessDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type HardnessDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HardnessDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3028, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_HardnessDimension_unitOfMeasure', HardnessUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3031, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3031, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'HardnessDimension', HardnessDimension)


# Complex type SweetnessAtHarvestDimension with content type SIMPLE
class SweetnessAtHarvestDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type SweetnessAtHarvestDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SweetnessAtHarvestDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3035, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_SweetnessAtHarvestDimension_unitOfMeasure', SweetnessAtHarvestUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3038, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3038, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'SweetnessAtHarvestDimension', SweetnessAtHarvestDimension)


# Complex type VineyardYieldDimension with content type SIMPLE
class VineyardYieldDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VineyardYieldDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VineyardYieldDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3042, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VineyardYieldDimension_unitOfMeasure', VineyardYieldUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3045, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3045, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VineyardYieldDimension', VineyardYieldDimension)


# Complex type AlcoholContentDimension with content type SIMPLE
class AlcoholContentDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type AlcoholContentDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AlcoholContentDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3049, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_AlcoholContentDimension_unitOfMeasure', AlcoholContentUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3052, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3052, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'AlcoholContentDimension', AlcoholContentDimension)


# Complex type ZoomDimension with content type SIMPLE
class ZoomDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ZoomDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ZoomDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3968, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ZoomDimension_unitOfMeasure', ZoomUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3971, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3971, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ZoomDimension', ZoomDimension)


# Complex type PixelDimension with content type SIMPLE
class PixelDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PixelDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PixelDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3981, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_PixelDimension_unitOfMeasure', PixelUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3984, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3984, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'PixelDimension', PixelDimension)


# Complex type PressureDimension with content type SIMPLE
class PressureDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PressureDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PressureDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3995, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_PressureDimension_unitOfMeasure', PressureUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3998, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3998, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'PressureDimension', PressureDimension)


# Complex type OpticalPowerDimension with content type SIMPLE
class OpticalPowerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type OpticalPowerDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OpticalPowerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4007, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_OpticalPowerDimension_unitOfMeasure', OpticalPowerUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4010, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4010, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'OpticalPowerDimension', OpticalPowerDimension)


# Complex type PowerDimension with content type SIMPLE
class PowerDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type PowerDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PowerDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4014, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_PowerDimension_unitOfMeasure', PowerUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4017, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4017, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'PowerDimension', PowerDimension)


# Complex type ResolutionDimension with content type SIMPLE
class ResolutionDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ResolutionDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ResolutionDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4021, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ResolutionDimension_unitOfMeasure', ResolutionUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4024, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4024, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ResolutionDimension', ResolutionDimension)


# Complex type OptionalResolutionDimension with content type SIMPLE
class OptionalResolutionDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type OptionalResolutionDimension with content type SIMPLE"""
    _TypeDefinition = StringNotNull
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OptionalResolutionDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4028, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is StringNotNull
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_OptionalResolutionDimension_unitOfMeasure', ResolutionUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4031, 32)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4031, 32)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'OptionalResolutionDimension', OptionalResolutionDimension)


# Complex type ApertureDimension with content type SIMPLE
class ApertureDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ApertureDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ApertureDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4035, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ApertureDimension_unitOfMeasure', ApertureUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4038, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4038, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ApertureDimension', ApertureDimension)


# Complex type ContinuousShootingDimension with content type SIMPLE
class ContinuousShootingDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ContinuousShootingDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ContinuousShootingDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4042, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ContinuousShootingDimension_unitOfMeasure', ContinuousShootingUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4045, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4045, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ContinuousShootingDimension', ContinuousShootingDimension)


# Complex type LoyaltyCustomAttribute with content type SIMPLE
class LoyaltyCustomAttribute (pyxb.binding.basis.complexTypeDefinition):
    """Complex type LoyaltyCustomAttribute with content type SIMPLE"""
    _TypeDefinition = String
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LoyaltyCustomAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4190, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is String
    
    # Attribute attributeName uses Python identifier attributeName
    __attributeName = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attributeName'), 'attributeName', '__AbsentNamespace0_LoyaltyCustomAttribute_attributeName', String)
    __attributeName._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4193, 4)
    __attributeName._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4193, 4)
    
    attributeName = property(__attributeName.value, __attributeName.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __attributeName.name() : __attributeName
    })
Namespace.addCategoryObject('typeBinding', 'LoyaltyCustomAttribute', LoyaltyCustomAttribute)


# Complex type NeckSizeDimension with content type SIMPLE
class NeckSizeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type NeckSizeDimension with content type SIMPLE"""
    _TypeDefinition = PositiveDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NeckSizeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4621, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_NeckSizeDimension_unitOfMeasure', NeckSizeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4624, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4624, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'NeckSizeDimension', NeckSizeDimension)


# Complex type CycleLengthDimension with content type SIMPLE
class CycleLengthDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CycleLengthDimension with content type SIMPLE"""
    _TypeDefinition = PositiveDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CycleLengthDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4637, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_CycleLengthDimension_unitOfMeasure', CycleLengthUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4640, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4640, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'CycleLengthDimension', CycleLengthDimension)


# Complex type BootSizeDimension with content type SIMPLE
class BootSizeDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type BootSizeDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BootSizeDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4650, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_BootSizeDimension_unitOfMeasure', BootSizeUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4653, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4653, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'BootSizeDimension', BootSizeDimension)


# Complex type DensityDimension with content type SIMPLE
class DensityDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type DensityDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DensityDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4688, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_DensityDimension_unitOfMeasure', DensityUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4691, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4691, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'DensityDimension', DensityDimension)


# Complex type CapacityUnit with content type SIMPLE
class CapacityUnit (pyxb.binding.basis.complexTypeDefinition):
    """Complex type CapacityUnit with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CapacityUnit')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4700, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_CapacityUnit_unitOfMeasure', CapacityUnitMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4703, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4703, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'CapacityUnit', CapacityUnit)


# Complex type ServingDimension with content type SIMPLE
class ServingDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type ServingDimension with content type SIMPLE"""
    _TypeDefinition = PositiveDimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ServingDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4749, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveDimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_ServingDimension_unitOfMeasure', ServingUnit, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4752, 31)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4752, 31)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'ServingDimension', ServingDimension)


# Complex type [anonymous] with content type SIMPLE
class CTD_ANON_21 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 87, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_CTD_ANON_21_unitOfMeasure', StringNotNull, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 90, 8)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/myoutput.xsd', 90, 8)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })



# Complex type GraduationInterval with content type SIMPLE
class GraduationInterval (pyxb.binding.basis.complexTypeDefinition):
    """Complex type GraduationInterval with content type SIMPLE"""
    _TypeDefinition = PositiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'GraduationInterval')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3007, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is PositiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_GraduationInterval_unitOfMeasure', GraduationIntervalUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3010, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3010, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'GraduationInterval', GraduationInterval)


# Complex type VolumeAndVolumeRateDimension with content type SIMPLE
class VolumeAndVolumeRateDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type VolumeAndVolumeRateDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VolumeAndVolumeRateDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3014, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_VolumeAndVolumeRateDimension_unitOfMeasure', VolumeAndVolumeRateUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3017, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3017, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'VolumeAndVolumeRateDimension', VolumeAndVolumeRateDimension)


# Complex type TorqueType with content type SIMPLE
class TorqueType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type TorqueType with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TorqueType')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3401, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_TorqueType_unitOfMeasure', TorqueUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3404, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3404, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'TorqueType', TorqueType)


# Complex type EnergyConsumptionDimension with content type SIMPLE
class EnergyConsumptionDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type EnergyConsumptionDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnergyConsumptionDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4049, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_EnergyConsumptionDimension_unitOfMeasure', EnergyConsumptionUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4052, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4052, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'EnergyConsumptionDimension', EnergyConsumptionDimension)


# Complex type SpeedDimension with content type SIMPLE
class SpeedDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type SpeedDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SpeedDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4329, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_SpeedDimension_unitOfMeasure', SpeedUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4332, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4332, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'SpeedDimension', SpeedDimension)


# Complex type MagnificationDimension with content type SIMPLE
class MagnificationDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type MagnificationDimension with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.positiveInteger
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MagnificationDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4592, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.positiveInteger
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_MagnificationDimension_unitOfMeasure', MagnificationUnitOfMeasure, required=True)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4595, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4595, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'MagnificationDimension', MagnificationDimension)


# Complex type OptionalMagnificationDimension with content type SIMPLE
class OptionalMagnificationDimension (pyxb.binding.basis.complexTypeDefinition):
    """Complex type OptionalMagnificationDimension with content type SIMPLE"""
    _TypeDefinition = Dimension
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OptionalMagnificationDimension')
    _XSDLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4605, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is Dimension
    
    # Attribute unitOfMeasure uses Python identifier unitOfMeasure
    __unitOfMeasure = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'unitOfMeasure'), 'unitOfMeasure', '__AbsentNamespace0_OptionalMagnificationDimension_unitOfMeasure', MagnificationUnitOfMeasure)
    __unitOfMeasure._DeclarationLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4608, 4)
    __unitOfMeasure._UseLocation = pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4608, 4)
    
    unitOfMeasure = property(__unitOfMeasure.value, __unitOfMeasure.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __unitOfMeasure.name() : __unitOfMeasure
    })
Namespace.addCategoryObject('typeBinding', 'OptionalMagnificationDimension', OptionalMagnificationDimension)


Address = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Address'), AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 18, 1))
Namespace.addCategoryObject('elementBinding', Address.name().localName(), Address)

Battery = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Battery'), CTD_ANON_, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 190, 1))
Namespace.addCategoryObject('elementBinding', Battery.name().localName(), Battery)

FulfillmentCenterID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'FulfillmentCenterID'), String, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1756, 1))
Namespace.addCategoryObject('elementBinding', FulfillmentCenterID.name().localName(), FulfillmentCenterID)

FulfillmentMethod = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'FulfillmentMethod'), STD_ANON_8, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1764, 1))
Namespace.addCategoryObject('elementBinding', FulfillmentMethod.name().localName(), FulfillmentMethod)

FulfillmentServiceLevel = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'FulfillmentServiceLevel'), STD_ANON_9, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1780, 1))
Namespace.addCategoryObject('elementBinding', FulfillmentServiceLevel.name().localName(), FulfillmentServiceLevel)

CarrierCode = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'CarrierCode'), STD_ANON_10, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1817, 1))
Namespace.addCategoryObject('elementBinding', CarrierCode.name().localName(), CarrierCode)

ShipOption = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ShipOption'), STD_ANON_11, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1881, 1))
Namespace.addCategoryObject('elementBinding', ShipOption.name().localName(), ShipOption)

MarketplaceName = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MarketplaceName'), StringNotNull, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1896, 1))
Namespace.addCategoryObject('elementBinding', MarketplaceName.name().localName(), MarketplaceName)

ExternalCustomerID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ExternalCustomerID'), HundredString, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2056, 1))
Namespace.addCategoryObject('elementBinding', ExternalCustomerID.name().localName(), ExternalCustomerID)

MerchantOrderID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MerchantOrderID'), String, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2064, 1))
Namespace.addCategoryObject('elementBinding', MerchantOrderID.name().localName(), MerchantOrderID)

MerchantOrderItemID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MerchantOrderItemID'), String, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2072, 1))
Namespace.addCategoryObject('elementBinding', MerchantOrderItemID.name().localName(), MerchantOrderItemID)

MerchantFulfillmentID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MerchantFulfillmentID'), TwentyStringNotNull, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2080, 1))
Namespace.addCategoryObject('elementBinding', MerchantFulfillmentID.name().localName(), MerchantFulfillmentID)

ShipmentID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ShipmentID'), String, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2089, 1))
Namespace.addCategoryObject('elementBinding', ShipmentID.name().localName(), ShipmentID)

MerchantPromotionID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MerchantPromotionID'), STD_ANON_12, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2097, 1))
Namespace.addCategoryObject('elementBinding', MerchantPromotionID.name().localName(), MerchantPromotionID)

AmazonOrderID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AmazonOrderID'), STD_ANON_13, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2111, 1))
Namespace.addCategoryObject('elementBinding', AmazonOrderID.name().localName(), AmazonOrderID)

AmazonOrderItemCode = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AmazonOrderItemCode'), STD_ANON_14, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2125, 1))
Namespace.addCategoryObject('elementBinding', AmazonOrderItemCode.name().localName(), AmazonOrderItemCode)

AmazonCustomerID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AmazonCustomerID'), HundredString, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2139, 1))
Namespace.addCategoryObject('elementBinding', AmazonCustomerID.name().localName(), AmazonCustomerID)

StandardProductID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'StandardProductID'), CTD_ANON_5, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2147, 1))
Namespace.addCategoryObject('elementBinding', StandardProductID.name().localName(), StandardProductID)

RelatedProductID = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RelatedProductID'), CTD_ANON_6, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2181, 1))
Namespace.addCategoryObject('elementBinding', RelatedProductID.name().localName(), RelatedProductID)

ProductTaxCode = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ProductTaxCode'), StringNotNull, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2212, 1))
Namespace.addCategoryObject('elementBinding', ProductTaxCode.name().localName(), ProductTaxCode)

PromotionClaimCode = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PromotionClaimCode'), STD_ANON_19, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2233, 1))
Namespace.addCategoryObject('elementBinding', PromotionClaimCode.name().localName(), PromotionClaimCode)

SKU = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SKU'), SKUType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2269, 1))
Namespace.addCategoryObject('elementBinding', SKU.name().localName(), SKU)

ConditionType = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ConditionType'), STD_ANON_20, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2293, 1))
Namespace.addCategoryObject('elementBinding', ConditionType.name().localName(), ConditionType)

ComputerPlatform = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ComputerPlatform'), CTD_ANON_8, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2330, 1))
Namespace.addCategoryObject('elementBinding', ComputerPlatform.name().localName(), ComputerPlatform)

ColorSpecification = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ColorSpecification'), CTD_ANON_9, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2366, 1))
Namespace.addCategoryObject('elementBinding', ColorSpecification.name().localName(), ColorSpecification)

ColorMap = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ColorMap'), STD_ANON_22, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2374, 1))
Namespace.addCategoryObject('elementBinding', ColorMap.name().localName(), ColorMap)

Denomination = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Denomination'), STD_ANON_23, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2410, 1))
Namespace.addCategoryObject('elementBinding', Denomination.name().localName(), Denomination)

DeliveryChannel = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DeliveryChannel'), STD_ANON_24, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2592, 1))
Namespace.addCategoryObject('elementBinding', DeliveryChannel.name().localName(), DeliveryChannel)

CharacterData = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'CharacterData'), CharacterDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4208, 1))
Namespace.addCategoryObject('elementBinding', CharacterData.name().localName(), CharacterData)

Recall = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Recall'), CTD_ANON_10, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4230, 1))
Namespace.addCategoryObject('elementBinding', Recall.name().localName(), Recall)

AgeRecommendation = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AgeRecommendation'), CTD_ANON_11, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4249, 1))
Namespace.addCategoryObject('elementBinding', AgeRecommendation.name().localName(), AgeRecommendation)

WeightRecommendation = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WeightRecommendation'), CTD_ANON_12, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4264, 1))
Namespace.addCategoryObject('elementBinding', WeightRecommendation.name().localName(), WeightRecommendation)

HeightRecommendation = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'HeightRecommendation'), CTD_ANON_13, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4277, 1))
Namespace.addCategoryObject('elementBinding', HeightRecommendation.name().localName(), HeightRecommendation)

ForwardFacingWeight = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ForwardFacingWeight'), CTD_ANON_14, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4290, 1))
Namespace.addCategoryObject('elementBinding', ForwardFacingWeight.name().localName(), ForwardFacingWeight)

RearFacingWeight = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RearFacingWeight'), CTD_ANON_15, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4303, 1))
Namespace.addCategoryObject('elementBinding', RearFacingWeight.name().localName(), RearFacingWeight)

ShoulderHarnessHeight = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ShoulderHarnessHeight'), CTD_ANON_16, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4316, 1))
Namespace.addCategoryObject('elementBinding', ShoulderHarnessHeight.name().localName(), ShoulderHarnessHeight)

Beauty = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Beauty'), CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 13, 1))
Namespace.addCategoryObject('elementBinding', Beauty.name().localName(), Beauty)

BeautyMisc = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'BeautyMisc'), CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 50, 1))
Namespace.addCategoryObject('elementBinding', BeautyMisc.name().localName(), BeautyMisc)



AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Name'), String, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 21, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'FormalTitle'), STD_ANON, scope=AddressType, documentation='e.g.  Mr., Ms., etc.', location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 22, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'GivenName'), String, scope=AddressType, documentation="Usually the customer's first name.", location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 32, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'FamilyName'), String, scope=AddressType, documentation="Usually the customer's last name.", location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 37, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AddressFieldOne'), AddressLine, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 42, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AddressFieldTwo'), AddressLine, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 43, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AddressFieldThree'), AddressLine, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 44, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'City'), String, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 45, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'County'), String, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 46, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'StateOrRegion'), String, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 47, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PostalCode'), String, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 48, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'CountryCode'), STD_ANON_, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 49, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PhoneNumber'), PhoneNumberType, scope=AddressType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 57, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'isDefaultShipping'), pyxb.binding.datatypes.boolean, scope=AddressType, documentation='Only one default shipping address can exist at any given\n\t\t\t\t\t\ttime. If more than one address has this set to "true," then the last one\n\t\t\t\t\t\twill become the default.', location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 58, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'isDefaultBilling'), pyxb.binding.datatypes.boolean, scope=AddressType, documentation='Only one default billing address can exist at any given time.\n\t\t\t\t\t\tIf more than one address has this set to "true," then the last one will\n\t\t\t\t\t\tbecome the default.', location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 65, 3)))

AddressType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'isDefaultOneClick'), pyxb.binding.datatypes.boolean, scope=AddressType, documentation='Only one default OneClick address can exist at any given\n\t\t\t\t\t\ttime. If more than one address has this set to "true," then the last one\n\t\t\t\t\t\twill become the default.', location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 72, 3)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 22, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 32, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 37, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 43, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 44, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 45, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 46, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 47, 3))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 48, 3))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=3, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 57, 3))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 58, 3))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 65, 3))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 72, 3))
    counters.add(cc_12)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'Name')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 21, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'FormalTitle')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 22, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'GivenName')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 32, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'FamilyName')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 37, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'AddressFieldOne')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 42, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'AddressFieldTwo')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 43, 3))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'AddressFieldThree')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 44, 3))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'City')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 45, 3))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'County')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 46, 3))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'StateOrRegion')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 47, 3))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'PostalCode')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 48, 3))
    st_10 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'CountryCode')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 49, 3))
    st_11 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'PhoneNumber')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 57, 3))
    st_12 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'isDefaultShipping')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 58, 3))
    st_13 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'isDefaultBilling')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 65, 3))
    st_14 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(AddressType._UseForTag(pyxb.namespace.ExpandedName(None, 'isDefaultOneClick')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 72, 3))
    st_15 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    transitions.append(fac.Transition(st_9, [
         ]))
    transitions.append(fac.Transition(st_10, [
         ]))
    transitions.append(fac.Transition(st_11, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
         ]))
    transitions.append(fac.Transition(st_13, [
         ]))
    transitions.append(fac.Transition(st_14, [
         ]))
    transitions.append(fac.Transition(st_15, [
         ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, True) ]))
    st_15._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AddressType._Automaton = _BuildAutomaton()




AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Name'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 88, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AddressFieldOne'), AddressLine, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 89, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AddressFieldTwo'), AddressLine, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 90, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AddressFieldThree'), AddressLine, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 91, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'City'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 92, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'DistrictOrCounty'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 93, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'County'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 94, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'StateOrRegion'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 95, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PostalCode'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 96, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'CountryCode'), STD_ANON_2, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 97, 3)))

AddressTypeSupportNonCity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PhoneNumber'), String, scope=AddressTypeSupportNonCity, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 105, 3)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 90, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 91, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 92, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 93, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 94, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 95, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 96, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 105, 3))
    counters.add(cc_7)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'Name')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 88, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'AddressFieldOne')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 89, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'AddressFieldTwo')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 90, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'AddressFieldThree')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 91, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'City')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 92, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'DistrictOrCounty')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 93, 3))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'County')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 94, 3))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'StateOrRegion')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 95, 3))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'PostalCode')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 96, 3))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'CountryCode')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 97, 3))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(AddressTypeSupportNonCity._UseForTag(pyxb.namespace.ExpandedName(None, 'PhoneNumber')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 105, 3))
    st_10 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    transitions.append(fac.Transition(st_9, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
         ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, True) ]))
    st_10._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AddressTypeSupportNonCity._Automaton = _BuildAutomaton_()




AmazonFees._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Fee'), CTD_ANON, scope=AmazonFees, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 171, 3)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AmazonFees._UseForTag(pyxb.namespace.ExpandedName(None, 'Fee')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 171, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AmazonFees._Automaton = _BuildAutomaton_2()




CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), STD_ANON_6, scope=CTD_ANON, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 174, 6)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Amount'), CurrencyAmount, scope=CTD_ANON, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 179, 6)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 174, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, 'Amount')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 179, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton_3()




CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AreBatteriesIncluded'), pyxb.binding.datatypes.boolean, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 193, 4)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AreBatteriesRequired'), pyxb.binding.datatypes.boolean, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 194, 4)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatterySubgroup'), CTD_ANON_2, scope=CTD_ANON_, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 195, 4)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 193, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 194, 4))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=3, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 195, 4))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, 'AreBatteriesIncluded')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 193, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, 'AreBatteriesRequired')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 194, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(None, 'BatterySubgroup')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 195, 4))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_._Automaton = _BuildAutomaton_4()




CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryType'), STD_ANON_32, scope=CTD_ANON_2, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 198, 7)))

CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'NumberOfBatteries'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_2, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 225, 7)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryType')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 198, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(None, 'NumberOfBatteries')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 225, 7))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_2._Automaton = _BuildAutomaton_5()




BuyerPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Component'), CTD_ANON_3, scope=BuyerPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 241, 3)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(BuyerPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'Component')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 241, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
BuyerPrice._Automaton = _BuildAutomaton_6()




CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), STD_ANON_7, scope=CTD_ANON_3, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 244, 6)))

CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Amount'), CurrencyAmount, scope=CTD_ANON_3, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 267, 6)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 244, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(None, 'Amount')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 267, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_3._Automaton = _BuildAutomaton_7()




DirectPaymentType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Component'), CTD_ANON_4, scope=DirectPaymentType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 280, 3)))

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DirectPaymentType._UseForTag(pyxb.namespace.ExpandedName(None, 'Component')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 280, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DirectPaymentType._Automaton = _BuildAutomaton_8()




CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), pyxb.binding.datatypes.string, scope=CTD_ANON_4, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 283, 6)))

CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Amount'), CurrencyAmount, scope=CTD_ANON_4, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 284, 6)))

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 283, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(None, 'Amount')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 284, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_4._Automaton = _BuildAutomaton_9()




DatedPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'StartDate'), pyxb.binding.datatypes.dateTime, scope=DatedPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1732, 3)))

DatedPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'EndDate'), pyxb.binding.datatypes.dateTime, scope=DatedPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1733, 3)))

DatedPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Price'), CurrencyAmount, scope=DatedPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1735, 4)))

DatedPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PreviousPrice'), CurrencyAmount, scope=DatedPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1736, 4)))

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1732, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1733, 3))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DatedPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'StartDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1732, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DatedPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'EndDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1733, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DatedPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'Price')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1735, 4))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DatedPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'PreviousPrice')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1736, 4))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DatedPrice._Automaton = _BuildAutomaton_10()




DatedCompareAtPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'StartDate'), pyxb.binding.datatypes.dateTime, scope=DatedCompareAtPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1743, 3)))

DatedCompareAtPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'EndDate'), pyxb.binding.datatypes.dateTime, scope=DatedCompareAtPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1744, 3)))

DatedCompareAtPrice._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'CompareAtPrice'), CurrencyAmount, scope=DatedCompareAtPrice, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1745, 3)))

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1743, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1744, 3))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DatedCompareAtPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'StartDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1743, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DatedCompareAtPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'EndDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1744, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DatedCompareAtPrice._UseForTag(pyxb.namespace.ExpandedName(None, 'CompareAtPrice')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 1745, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DatedCompareAtPrice._Automaton = _BuildAutomaton_11()




CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), STD_ANON_15, scope=CTD_ANON_5, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2150, 4)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Value'), STD_ANON_16, scope=CTD_ANON_5, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2163, 4)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2150, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(None, 'Value')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2163, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_5._Automaton = _BuildAutomaton_12()




CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), STD_ANON_17, scope=CTD_ANON_6, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2184, 4)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Value'), STD_ANON_18, scope=CTD_ANON_6, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2193, 4)))

def _BuildAutomaton_13 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_13
    del _BuildAutomaton_13
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2184, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(None, 'Value')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2193, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_6._Automaton = _BuildAutomaton_13()




PromotionDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MerchantPromotionID'), STD_ANON_12, scope=PromotionDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2097, 1)))

PromotionDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PromotionClaimCode'), STD_ANON_19, scope=PromotionDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2233, 1)))

PromotionDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Component'), CTD_ANON_7, scope=PromotionDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2252, 3)))

def _BuildAutomaton_14 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_14
    del _BuildAutomaton_14
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PromotionDataType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PromotionClaimCode')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2250, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PromotionDataType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'MerchantPromotionID')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2251, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PromotionDataType._UseForTag(pyxb.namespace.ExpandedName(None, 'Component')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2252, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PromotionDataType._Automaton = _BuildAutomaton_14()




CTD_ANON_7._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), PromotionApplicationType, scope=CTD_ANON_7, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2255, 6)))

CTD_ANON_7._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Amount'), CurrencyAmount, scope=CTD_ANON_7, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2256, 6)))

def _BuildAutomaton_15 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_15
    del _BuildAutomaton_15
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_7._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2255, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_7._UseForTag(pyxb.namespace.ExpandedName(None, 'Amount')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2256, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_7._Automaton = _BuildAutomaton_15()




ConditionInfo._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ConditionNote'), TwoThousandString, scope=ConditionInfo, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2290, 3)))

ConditionInfo._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ConditionType'), STD_ANON_20, scope=ConditionInfo, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2293, 1)))

def _BuildAutomaton_16 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_16
    del _BuildAutomaton_16
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2290, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ConditionInfo._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ConditionType')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2289, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ConditionInfo._UseForTag(pyxb.namespace.ExpandedName(None, 'ConditionNote')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2290, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ConditionInfo._Automaton = _BuildAutomaton_16()




CustomizationInfoType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), StringNotNull, scope=CustomizationInfoType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2319, 3)))

CustomizationInfoType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Data'), LongString, scope=CustomizationInfoType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2320, 3)))

def _BuildAutomaton_17 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_17
    del _BuildAutomaton_17
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CustomizationInfoType._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2319, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CustomizationInfoType._UseForTag(pyxb.namespace.ExpandedName(None, 'Data')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2320, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CustomizationInfoType._Automaton = _BuildAutomaton_17()




CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Type'), STD_ANON_21, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2333, 4)))

CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SystemRequirements'), LongStringNotNull, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2342, 4)))

def _BuildAutomaton_18 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_18
    del _BuildAutomaton_18
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2342, 4))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(None, 'Type')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2333, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(None, 'SystemRequirements')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2342, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_8._Automaton = _BuildAutomaton_18()




RebateType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RebateStartDate'), pyxb.binding.datatypes.dateTime, scope=RebateType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2353, 3)))

RebateType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RebateEndDate'), pyxb.binding.datatypes.dateTime, scope=RebateType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2354, 3)))

RebateType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RebateMessage'), TwoFiftyStringNotNull, scope=RebateType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2355, 3)))

RebateType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RebateName'), FortyStringNotNull, scope=RebateType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2356, 3)))

def _BuildAutomaton_19 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_19
    del _BuildAutomaton_19
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RebateType._UseForTag(pyxb.namespace.ExpandedName(None, 'RebateStartDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2353, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RebateType._UseForTag(pyxb.namespace.ExpandedName(None, 'RebateEndDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2354, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RebateType._UseForTag(pyxb.namespace.ExpandedName(None, 'RebateMessage')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2355, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(RebateType._UseForTag(pyxb.namespace.ExpandedName(None, 'RebateName')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2356, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RebateType._Automaton = _BuildAutomaton_19()




CTD_ANON_9._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Color'), StringNotNull, scope=CTD_ANON_9, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2369, 4)))

CTD_ANON_9._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ColorMap'), STD_ANON_22, scope=CTD_ANON_9, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2374, 1)))

def _BuildAutomaton_20 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_20
    del _BuildAutomaton_20
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_9._UseForTag(pyxb.namespace.ExpandedName(None, 'Color')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2369, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_9._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ColorMap')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2370, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_9._Automaton = _BuildAutomaton_20()




Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Name'), String, scope=Customer, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3450, 3)))

Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'FormalTitle'), STD_ANON_25, scope=Customer, documentation='e.g. Mr., Ms., etc.', location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3451, 3)))

Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'GivenName'), String, scope=Customer, documentation="Usually the customer's first name.", location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3461, 3)))

Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'FamilyName'), String, scope=Customer, documentation="Usually the customer's last name.", location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3466, 3)))

Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Email'), EmailAddressType, scope=Customer, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3471, 3)))

Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BirthDate'), pyxb.binding.datatypes.date, scope=Customer, documentation="The customer's birth date", location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3472, 3)))

Customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'CustomerAddress'), AddressType, scope=Customer, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3477, 3)))

def _BuildAutomaton_21 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_21
    del _BuildAutomaton_21
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3450, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3451, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3461, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3466, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3471, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3472, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3477, 3))
    counters.add(cc_6)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'Name')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3450, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'FormalTitle')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3451, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'GivenName')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3461, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'FamilyName')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3466, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'Email')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3471, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'BirthDate')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3472, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(Customer._UseForTag(pyxb.namespace.ExpandedName(None, 'CustomerAddress')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3477, 3))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Customer._Automaton = _BuildAutomaton_21()




NameValuePair._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Name'), StringNotNull, scope=NameValuePair, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3487, 3)))

NameValuePair._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Value'), LongString, scope=NameValuePair, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3488, 3)))

def _BuildAutomaton_22 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_22
    del _BuildAutomaton_22
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(NameValuePair._UseForTag(pyxb.namespace.ExpandedName(None, 'Name')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3487, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(NameValuePair._UseForTag(pyxb.namespace.ExpandedName(None, 'Value')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 3488, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
NameValuePair._Automaton = _BuildAutomaton_22()




WeightRecommendationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MaximumWeightRecommendation'), PositiveWeightDimension, scope=WeightRecommendationType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4199, 3)))

WeightRecommendationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MinimumWeightRecommendation'), PositiveWeightDimension, scope=WeightRecommendationType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4200, 3)))

def _BuildAutomaton_23 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_23
    del _BuildAutomaton_23
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4199, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4200, 3))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(WeightRecommendationType._UseForTag(pyxb.namespace.ExpandedName(None, 'MaximumWeightRecommendation')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4199, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(WeightRecommendationType._UseForTag(pyxb.namespace.ExpandedName(None, 'MinimumWeightRecommendation')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4200, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
WeightRecommendationType._Automaton = _BuildAutomaton_23()




CharacterDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SKU'), SKUType, scope=CharacterDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 2269, 1)))

CharacterDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'EffectiveTimestamp'), pyxb.binding.datatypes.dateTime, scope=CharacterDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4212, 3)))

CharacterDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Plugin'), pyxb.binding.datatypes.string, scope=CharacterDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4213, 3)))

CharacterDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'AdditionalMessageDiscriminator'), pyxb.binding.datatypes.string, scope=CharacterDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4214, 3)))

CharacterDataType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Payload'), pyxb.binding.datatypes.string, scope=CharacterDataType, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4215, 3)))

def _BuildAutomaton_24 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_24
    del _BuildAutomaton_24
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4212, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4213, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4214, 3))
    counters.add(cc_2)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CharacterDataType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SKU')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4211, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CharacterDataType._UseForTag(pyxb.namespace.ExpandedName(None, 'EffectiveTimestamp')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4212, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CharacterDataType._UseForTag(pyxb.namespace.ExpandedName(None, 'Plugin')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4213, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CharacterDataType._UseForTag(pyxb.namespace.ExpandedName(None, 'AdditionalMessageDiscriminator')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4214, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CharacterDataType._UseForTag(pyxb.namespace.ExpandedName(None, 'Payload')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4215, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CharacterDataType._Automaton = _BuildAutomaton_24()




CTD_ANON_10._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'IsRecalled'), pyxb.binding.datatypes.boolean, scope=CTD_ANON_10, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4233, 4)))

CTD_ANON_10._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RecallDescription'), STD_ANON_26, scope=CTD_ANON_10, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4234, 4)))

def _BuildAutomaton_25 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_25
    del _BuildAutomaton_25
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_10._UseForTag(pyxb.namespace.ExpandedName(None, 'IsRecalled')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4233, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_10._UseForTag(pyxb.namespace.ExpandedName(None, 'RecallDescription')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4234, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_10._Automaton = _BuildAutomaton_25()




CTD_ANON_11._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MinimumManufacturerAgeRecommended'), MinimumAgeRecommendedDimension, scope=CTD_ANON_11, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4252, 4)))

CTD_ANON_11._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MaximumManufacturerAgeRecommended'), AgeRecommendedDimension, scope=CTD_ANON_11, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4253, 4)))

CTD_ANON_11._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MinimumMerchantAgeRecommended'), MinimumAgeRecommendedDimension, scope=CTD_ANON_11, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4254, 4)))

CTD_ANON_11._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MaximumMerchantAgeRecommended'), AgeRecommendedDimension, scope=CTD_ANON_11, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4255, 4)))

def _BuildAutomaton_26 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_26
    del _BuildAutomaton_26
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4252, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4253, 4))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4254, 4))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4255, 4))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_11._UseForTag(pyxb.namespace.ExpandedName(None, 'MinimumManufacturerAgeRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4252, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_11._UseForTag(pyxb.namespace.ExpandedName(None, 'MaximumManufacturerAgeRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4253, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_11._UseForTag(pyxb.namespace.ExpandedName(None, 'MinimumMerchantAgeRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4254, 4))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_11._UseForTag(pyxb.namespace.ExpandedName(None, 'MaximumMerchantAgeRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4255, 4))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_11._Automaton = _BuildAutomaton_26()




CTD_ANON_12._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MinimumManufacturerWeightRecommended'), WeightIntegerDimension, scope=CTD_ANON_12, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4267, 4)))

CTD_ANON_12._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MaximumManufacturerWeightRecommended'), WeightIntegerDimension, scope=CTD_ANON_12, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4268, 4)))

def _BuildAutomaton_27 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_27
    del _BuildAutomaton_27
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4267, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4268, 4))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_12._UseForTag(pyxb.namespace.ExpandedName(None, 'MinimumManufacturerWeightRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4267, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_12._UseForTag(pyxb.namespace.ExpandedName(None, 'MaximumManufacturerWeightRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4268, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_12._Automaton = _BuildAutomaton_27()




CTD_ANON_13._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MinimumHeightRecommended'), LengthDimension, scope=CTD_ANON_13, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4280, 4)))

CTD_ANON_13._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MaximumHeightRecommended'), LengthDimension, scope=CTD_ANON_13, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4281, 4)))

def _BuildAutomaton_28 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_28
    del _BuildAutomaton_28
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4280, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4281, 4))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_13._UseForTag(pyxb.namespace.ExpandedName(None, 'MinimumHeightRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4280, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_13._UseForTag(pyxb.namespace.ExpandedName(None, 'MaximumHeightRecommended')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4281, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_13._Automaton = _BuildAutomaton_28()




CTD_ANON_14._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ForwardFacingMaximumWeight'), WeightDimension, scope=CTD_ANON_14, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4293, 4)))

CTD_ANON_14._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ForwardFacingMinimumWeight'), WeightDimension, scope=CTD_ANON_14, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4294, 4)))

def _BuildAutomaton_29 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_29
    del _BuildAutomaton_29
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4293, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4294, 4))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_14._UseForTag(pyxb.namespace.ExpandedName(None, 'ForwardFacingMaximumWeight')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4293, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_14._UseForTag(pyxb.namespace.ExpandedName(None, 'ForwardFacingMinimumWeight')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4294, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_14._Automaton = _BuildAutomaton_29()




CTD_ANON_15._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RearFacingMaximumWeight'), WeightDimension, scope=CTD_ANON_15, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4306, 4)))

CTD_ANON_15._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'RearFacingMinimumWeight'), WeightDimension, scope=CTD_ANON_15, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4307, 4)))

def _BuildAutomaton_30 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_30
    del _BuildAutomaton_30
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4306, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4307, 4))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_15._UseForTag(pyxb.namespace.ExpandedName(None, 'RearFacingMaximumWeight')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4306, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_15._UseForTag(pyxb.namespace.ExpandedName(None, 'RearFacingMinimumWeight')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4307, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_15._Automaton = _BuildAutomaton_30()




CTD_ANON_16._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ShoulderHarnessMaximumHeight'), LengthDimension, scope=CTD_ANON_16, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4319, 4)))

CTD_ANON_16._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ShoulderHarnessMinimumHeight'), LengthDimension, scope=CTD_ANON_16, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4320, 4)))

def _BuildAutomaton_31 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_31
    del _BuildAutomaton_31
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4319, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4320, 4))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_16._UseForTag(pyxb.namespace.ExpandedName(None, 'ShoulderHarnessMaximumHeight')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4319, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_16._UseForTag(pyxb.namespace.ExpandedName(None, 'ShoulderHarnessMinimumHeight')), pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 4320, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_16._Automaton = _BuildAutomaton_31()




CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ProductType'), CTD_ANON_18, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 16, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryTypeLithiumIon'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 23, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryTypeLithiumMetal'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 24, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'LithiumBatteryEnergyContent'), pyxb.binding.datatypes.decimal, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 25, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'LithiumBatteryPackaging'), STD_ANON_27, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 26, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'LithiumBatteryVoltage'), pyxb.binding.datatypes.decimal, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 35, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'LithiumBatteryWeight'), pyxb.binding.datatypes.decimal, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 36, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MedicineClassification'), StringNotNull, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 37, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'NumberOfLithiumIonCells'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 38, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'NumberOfLithiumMetalCells'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 39, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PlugType'), HundredString, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 41, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SpecificUsesForProduct'), StringNotNull, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 42, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Certification'), StringNotNull, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 43, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SunProtection'), SunProtectionDimension, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 44, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Voltage'), VoltageDecimalDimension, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 45, 4)))

CTD_ANON_17._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Wattage'), WattageDimension, scope=CTD_ANON_17, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 46, 4)))

def _BuildAutomaton_32 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_32
    del _BuildAutomaton_32
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 23, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 24, 4))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 25, 4))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 26, 4))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 35, 4))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 36, 4))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 37, 4))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 38, 4))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 39, 4))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 41, 4))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=2, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 42, 4))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=2, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 43, 4))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 44, 4))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 45, 4))
    counters.add(cc_13)
    cc_14 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 46, 4))
    counters.add(cc_14)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'ProductType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 16, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryTypeLithiumIon')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 23, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryTypeLithiumMetal')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 24, 4))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'LithiumBatteryEnergyContent')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 25, 4))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'LithiumBatteryPackaging')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 26, 4))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'LithiumBatteryVoltage')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 35, 4))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'LithiumBatteryWeight')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 36, 4))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'MedicineClassification')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 37, 4))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'NumberOfLithiumIonCells')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 38, 4))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'NumberOfLithiumMetalCells')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 39, 4))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'PlugType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 41, 4))
    st_10 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'SpecificUsesForProduct')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 42, 4))
    st_11 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'Certification')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 43, 4))
    st_12 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'SunProtection')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 44, 4))
    st_13 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'Voltage')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 45, 4))
    st_14 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_14, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_17._UseForTag(pyxb.namespace.ExpandedName(None, 'Wattage')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 46, 4))
    st_15 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    transitions.append(fac.Transition(st_9, [
         ]))
    transitions.append(fac.Transition(st_10, [
         ]))
    transitions.append(fac.Transition(st_11, [
         ]))
    transitions.append(fac.Transition(st_12, [
         ]))
    transitions.append(fac.Transition(st_13, [
         ]))
    transitions.append(fac.Transition(st_14, [
         ]))
    transitions.append(fac.Transition(st_15, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_13, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_13, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_14, True) ]))
    st_15._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_17._Automaton = _BuildAutomaton_32()




CTD_ANON_18._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'BeautyMisc'), CTD_ANON_19, scope=CTD_ANON_18, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 50, 1)))

def _BuildAutomaton_33 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_33
    del _BuildAutomaton_33
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_18._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'BeautyMisc')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 19, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_18._Automaton = _BuildAutomaton_33()




CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Battery'), CTD_ANON_, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/amzn-base.xsd', 190, 1)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'VariationData'), CTD_ANON_20, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 53, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'UnitCount'), CTD_ANON_21, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 86, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Count'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 101, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'NumberOfItems'), pyxb.binding.datatypes.positiveInteger, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 107, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryAverageLife'), PositiveNonZeroDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 108, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryCellComposition'), BatteryCellTypeValues, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 109, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryAverageLifeStandby'), PositiveNonZeroDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 110, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryChargeTime'), PositiveNonZeroDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 112, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryDescription'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 113, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryFormFactor'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 114, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BatteryPower'), BatteryPowerIntegerDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 115, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'DisplayLength'), LengthDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 116, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'DisplayWeight'), WeightDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 117, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'DisplayVolume'), VolumeDimension, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 118, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SkinType'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 119, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SkinTone'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 120, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'HairType'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 121, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'IncludedComponents'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 122, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Ingredients'), STD_ANON_30, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 123, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ManufacturerWarrantyType'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 130, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MaterialType'), LongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 131, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MfrWarrantyDescriptionLabor'), SuperLongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 133, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'MfrWarrantyDescriptionParts'), SuperLongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 135, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ModelName'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 137, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Indications'), LongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 138, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Directions'), LongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 139, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Warnings'), LongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 140, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ItemForm'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 141, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ItemPackageQuantity'), PositiveInteger, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 142, 4), unicode_default='1'))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Flavor'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 143, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Coverage'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 144, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'FinishType'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 145, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ItemSpecialty'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 146, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PatternName'), String, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 147, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'PowerSource'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 148, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'IsAdultProduct'), pyxb.binding.datatypes.boolean, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 149, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'TargetGender'), STD_ANON_33, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 150, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'CountryOfOrigin'), STD_ANON_31, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 159, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SellerWarrantyDescription'), SuperLongStringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 166, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SizeMap'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 167, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'BrandRegionOfOrigin'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 169, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'SpecialFeatures'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 170, 4)))

CTD_ANON_19._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'TargetAudienceBase'), StringNotNull, scope=CTD_ANON_19, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 171, 4)))

def _BuildAutomaton_34 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_34
    del _BuildAutomaton_34
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 53, 4))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 86, 4))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 101, 4))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 107, 4))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 108, 4))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 109, 4))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 110, 4))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 112, 4))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 113, 4))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 114, 4))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 115, 4))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 116, 4))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 117, 4))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 118, 4))
    counters.add(cc_13)
    cc_14 = fac.CounterCondition(min=0, max=5, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 119, 4))
    counters.add(cc_14)
    cc_15 = fac.CounterCondition(min=0, max=5, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 120, 4))
    counters.add(cc_15)
    cc_16 = fac.CounterCondition(min=0, max=5, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 121, 4))
    counters.add(cc_16)
    cc_17 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 122, 4))
    counters.add(cc_17)
    cc_18 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 123, 4))
    counters.add(cc_18)
    cc_19 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 130, 4))
    counters.add(cc_19)
    cc_20 = fac.CounterCondition(min=0, max=3, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 131, 4))
    counters.add(cc_20)
    cc_21 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 133, 4))
    counters.add(cc_21)
    cc_22 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 135, 4))
    counters.add(cc_22)
    cc_23 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 137, 4))
    counters.add(cc_23)
    cc_24 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 138, 4))
    counters.add(cc_24)
    cc_25 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 139, 4))
    counters.add(cc_25)
    cc_26 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 140, 4))
    counters.add(cc_26)
    cc_27 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 141, 4))
    counters.add(cc_27)
    cc_28 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 142, 4))
    counters.add(cc_28)
    cc_29 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 143, 4))
    counters.add(cc_29)
    cc_30 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 144, 4))
    counters.add(cc_30)
    cc_31 = fac.CounterCondition(min=0, max=5, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 145, 4))
    counters.add(cc_31)
    cc_32 = fac.CounterCondition(min=0, max=5, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 146, 4))
    counters.add(cc_32)
    cc_33 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 147, 4))
    counters.add(cc_33)
    cc_34 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 148, 4))
    counters.add(cc_34)
    cc_35 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 149, 4))
    counters.add(cc_35)
    cc_36 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 150, 4))
    counters.add(cc_36)
    cc_37 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 159, 4))
    counters.add(cc_37)
    cc_38 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 166, 4))
    counters.add(cc_38)
    cc_39 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 167, 4))
    counters.add(cc_39)
    cc_40 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 168, 4))
    counters.add(cc_40)
    cc_41 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 169, 4))
    counters.add(cc_41)
    cc_42 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 170, 4))
    counters.add(cc_42)
    cc_43 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 171, 4))
    counters.add(cc_43)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'VariationData')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 53, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'UnitCount')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 86, 4))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Count')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 101, 4))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'NumberOfItems')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 107, 4))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryAverageLife')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 108, 4))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryCellComposition')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 109, 4))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryAverageLifeStandby')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 110, 4))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryChargeTime')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 112, 4))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryDescription')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 113, 4))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryFormFactor')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 114, 4))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BatteryPower')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 115, 4))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'DisplayLength')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 116, 4))
    st_11 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'DisplayWeight')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 117, 4))
    st_12 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'DisplayVolume')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 118, 4))
    st_13 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_14, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'SkinType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 119, 4))
    st_14 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_15, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'SkinTone')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 120, 4))
    st_15 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_16, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'HairType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 121, 4))
    st_16 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_16)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_17, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'IncludedComponents')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 122, 4))
    st_17 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_17)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_18, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Ingredients')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 123, 4))
    st_18 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_18)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_19, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'ManufacturerWarrantyType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 130, 4))
    st_19 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_19)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_20, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'MaterialType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 131, 4))
    st_20 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_20)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_21, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'MfrWarrantyDescriptionLabor')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 133, 4))
    st_21 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_21)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_22, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'MfrWarrantyDescriptionParts')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 135, 4))
    st_22 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_22)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_23, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'ModelName')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 137, 4))
    st_23 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_23)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_24, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Indications')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 138, 4))
    st_24 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_24)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_25, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Directions')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 139, 4))
    st_25 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_25)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_26, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Warnings')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 140, 4))
    st_26 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_26)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_27, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'ItemForm')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 141, 4))
    st_27 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_27)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_28, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'ItemPackageQuantity')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 142, 4))
    st_28 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_28)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_29, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Flavor')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 143, 4))
    st_29 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_29)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_30, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'Coverage')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 144, 4))
    st_30 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_30)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_31, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'FinishType')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 145, 4))
    st_31 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_31)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_32, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'ItemSpecialty')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 146, 4))
    st_32 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_32)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_33, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'PatternName')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 147, 4))
    st_33 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_33)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_34, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'PowerSource')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 148, 4))
    st_34 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_34)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_35, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'IsAdultProduct')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 149, 4))
    st_35 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_35)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_36, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'TargetGender')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 150, 4))
    st_36 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_36)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_37, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'CountryOfOrigin')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 159, 4))
    st_37 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_37)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_38, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'SellerWarrantyDescription')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 166, 4))
    st_38 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_38)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_39, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'SizeMap')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 167, 4))
    st_39 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_39)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_40, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Battery')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 168, 4))
    st_40 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_40)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_41, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'BrandRegionOfOrigin')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 169, 4))
    st_41 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_41)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_42, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'SpecialFeatures')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 170, 4))
    st_42 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_42)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_43, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_19._UseForTag(pyxb.namespace.ExpandedName(None, 'TargetAudienceBase')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 171, 4))
    st_43 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_43)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_13, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_13, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_14, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_14, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_15, True) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_15, False) ]))
    st_15._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_16, True) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_16, False) ]))
    st_16._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_17, True) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_17, False) ]))
    st_17._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_18, True) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_18, False) ]))
    st_18._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_19, True) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_19, False) ]))
    st_19._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_20, True) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_20, False) ]))
    st_20._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_21, True) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_21, False) ]))
    st_21._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_22, True) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_22, False) ]))
    st_22._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_23, True) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_23, False) ]))
    st_23._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_24, True) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_24, False) ]))
    st_24._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_25, True) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_25, False) ]))
    st_25._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_26, True) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_26, False) ]))
    st_26._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_27, True) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_27, False) ]))
    st_27._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_28, True) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_28, False) ]))
    st_28._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_29, True) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_29, False) ]))
    st_29._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_30, True) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_30, False) ]))
    st_30._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_31, True) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_31, False) ]))
    st_31._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_32, True) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_32, False) ]))
    st_32._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_33, True) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_33, False) ]))
    st_33._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_34, True) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_34, False) ]))
    st_34._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_35, True) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_35, False) ]))
    st_35._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_36, True) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_36, False) ]))
    st_36._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_37, True) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_37, False) ]))
    st_37._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_38, True) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_38, False) ]))
    st_38._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_39, True) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_39, False) ]))
    st_39._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_40, True) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_40, False) ]))
    st_40._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_41, True) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_41, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_41, False) ]))
    st_41._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_42, True) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_42, False) ]))
    st_42._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_43, True) ]))
    st_43._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_19._Automaton = _BuildAutomaton_34()




CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Parentage'), STD_ANON_28, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 56, 7)))

CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'VariationTheme'), STD_ANON_29, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 64, 7)))

CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Size'), StringNotNull, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 78, 7)))

CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Color'), StringNotNull, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 79, 7)))

CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'ColorMap'), StringNotNull, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 80, 7)))

CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Scent'), StringNotNull, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 81, 7)))

CTD_ANON_20._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'Capacity'), VolumeDimension, scope=CTD_ANON_20, location=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 82, 7)))

def _BuildAutomaton_35 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_35
    del _BuildAutomaton_35
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 64, 7))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 78, 7))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 79, 7))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 80, 7))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 81, 7))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/tmp/myoutput.xsd', 82, 7))
    counters.add(cc_5)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'Parentage')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 56, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'VariationTheme')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 64, 7))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'Size')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 78, 7))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'Color')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 79, 7))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'ColorMap')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 80, 7))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'Scent')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 81, 7))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_20._UseForTag(pyxb.namespace.ExpandedName(None, 'Capacity')), pyxb.utils.utility.Location('/tmp/myoutput.xsd', 82, 7))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_20._Automaton = _BuildAutomaton_35()

