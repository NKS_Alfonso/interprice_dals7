from openerp import models, fields, api

class account_bank_statement(models.Model):
    _inherit = 'account.bank.statement'
    settlement_ref = fields.Char(size=350, string='Amazon Settlement Ref',index=True)

