# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, exceptions, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'


    @api.model
    def create(self, vals):
        if 'pricelist_id' in vals:
            pricelist_obj = self.env['product.pricelist']
            pricelist_id = vals.get('pricelist_id')
            currency_id = pricelist_obj.browse(pricelist_id).currency_id
            vals['currency_id'] = currency_id.id
        res = super(PurchaseOrder, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if 'pricelist_id' in vals:
            pricelist_obj = self.env['product.pricelist']
            pricelist_id = vals.get('pricelist_id')
            currency_id = pricelist_obj.browse(pricelist_id).currency_id
            vals['currency_id'] = currency_id.id
        res = super(PurchaseOrder, self).write(vals)
        return res
