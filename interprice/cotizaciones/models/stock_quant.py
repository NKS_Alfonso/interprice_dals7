# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, api, _
from openerp.osv import osv


class StockPicking(models.Model):
    """Stock Picking new transfer to data base"""

    # odoo model properties
    _inherit = 'stock.quant'

    @api.multi
    def reserve_quant_to_move(self, move):
        ''' Reserve Stock Quants '''
        reserved_availability = int()
        move_id, qty, picking_id, move_state, product_id, location_id = move
        self.unreserve_quant_to_move(move_id, picking_id)
        self.env.cr.execute(
            """
            SELECT id, qty FROM stock_quant WHERE reservation_id IS NULL
            AND product_id = %s AND location_id = %s ORDER BY id asc""",
            (product_id, location_id)
        )
        if self.env.cr.rowcount:
            for (quant_id, product_qty) in self.env.cr.fetchall():
                if qty > reserved_availability:
                    self.env.cr.execute(""" UPDATE stock_quant SET reservation_id = %s WHERE id = %s""",
                                        (move_id, quant_id)
                                        )
                    reserved_availability += product_qty
                    if qty == reserved_availability:
                        break
                    elif qty < reserved_availability:
                        new_qty = abs(reserved_availability - qty)
                        self.env.cr.execute("""SELECT move_id FROM stock_quant_move_rel WHERE quant_id = %s""", (quant_id,))
                        res = self.env.cr.fetchall()
                        self.sudo().browse(quant_id).copy(default={'reservation_id': False, 'qty': new_qty, 'history_ids': [(4, x[0]) for x in res]})
                        self.sudo().browse(quant_id).write({'qty': abs(new_qty - product_qty)})
                        reserved_availability=qty
                        break
        if picking_id:
            self.env['stock.picking'].browse(picking_id).write({'recompute_pack_op': True})
        if move_state in ('confirmed', 'waiting') and qty == reserved_availability:
            self.env['stock.move'].browse(move_id).write({'state': 'assigned'})
        elif qty > reserved_availability:
            if reserved_availability > 0:
                self.env['stock.move'].browse(move_id).write({'partially_available': True})
            else:
                raise osv.except_osv(_('Operation Forbidden!'), _('Out of Stock'))

    @api.multi
    def unreserve_quant_to_move(self, move_id, picking_id):
        ''' Unreserve Stock Quants '''
        if move_id:
            self.env.cr.execute("""
            UPDATE stock_quant SET reservation_id = NULL WHERE reservation_id = %s
            """, (move_id,))
            if picking_id:
                self.env['stock.picking'].browse(picking_id).write({'recompute_pack_op': True})
            self.env['stock.move'].browse(move_id).write({'partially_available': False})
