# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, api, _
from openerp.osv import osv


class StockMove(models.Model):
    """Stock Picking new transfer to data base"""

    # odoo model properties
    _inherit = 'stock.move'

    @api.one
    def find_move_ancestors_quotations(self, move=None):
        """Find the first level ancestors of given move"""
        if not move:
            move = self
        ancestors = []
        move2 = move
        while move2:
            ancestors += [x.id for x in move2.move_orig_ids]
            move2 = not move2.move_orig_ids and move2.split_from or False
        return ancestors

    @api.multi
    def assign_unreservation(self):
        move_ids = [move.id for move in self]
        if move_ids:
            stock_move_ids = tuple(move_ids)
            self.env.cr.execute(
                """
                   SELECT
                       SM.id AS move_id,
                       SM.state AS moves_state,
                       SM.product_qty AS product_qty,
                       SM.origin_returned_move_id AS origin_returned_move_id,
                       SP.id AS picking_id,
                       SL.usage AS location_usage,
                       SL.id AS location_id,
                       PT.type AS product_type,
                       PP.id AS product_id
                   FROM stock_move As SM
                       INNER JOIN stock_picking AS SP ON SP.id = SM.picking_id
                       INNER JOIN stock_location AS SL ON SL.id = SM.location_id
                       INNER JOIN product_product AS PP ON PP.id = SM.product_id
                       INNER JOIN product_template AS PT ON PT.id = PP.product_tmpl_id
                   WHERE SM.id in %s
                """, (stock_move_ids,)
            )
            if self.env.cr.rowcount:
                for (move_id, move_state, product_qty, origin_returned_move_id, picking_id,
                     location_usage, location_id, product_type, product_id) in self.env.cr.fetchall():
                    if move_state in ('done', 'cancel'):
                        raise osv.except_osv(_('Operation Forbidden!'), _('Cannot unreserve a done move'))
                    self.env['stock.quant'].unreserve_quant_to_move(move_id, picking_id)
                    if self.browse(move_id).find_move_ancestors_quotations():
                        self.browse(move_id).write({'state': 'waiting'})
                    else:
                        self.browse(move_id).write({'state': 'confirmed'})

    @api.multi
    def assign_reservation(self):
        """ Checks the product type and accordingly writes the state.
        """
        # to_assign_moves = []
        # inf_moves = []
        move_ids = [move.id for move in self]
        if move_ids:
            stock_move_ids = tuple(move_ids)
            self.env.cr.execute(
                """
                   SELECT
                       SM.id AS move_id,
                       SM.state AS moves_state,
                       SM.product_qty AS product_qty,
                       SM.origin_returned_move_id AS origin_returned_move_id,
                       SP.id AS picking_id,
                       SL.usage AS location_usage,
                       SL.id AS location_id,
                       PT.type AS product_type,
                       PP.id AS product_id
                   FROM stock_move As SM
                       INNER JOIN stock_picking AS SP ON SP.id = SM.picking_id
                       INNER JOIN stock_location AS SL ON SL.id = SM.location_id
                       INNER JOIN product_product AS PP ON PP.id = SM.product_id
                       INNER JOIN product_template AS PT ON PT.id = PP.product_tmpl_id
                   WHERE SM.id in %s
                """, (stock_move_ids,)
            )
            if self.env.cr.rowcount:
                for (move_id, move_state, product_qty, origin_returned_move_id, picking_id,
                     location_usage, location_id, product_type, product_id) in self.env.cr.fetchall():
                    if product_type in ('consu', 'product') and move_state in ('confirmed', 'waiting'):
                        inf_move = [move_id, product_qty, picking_id, move_state, product_id, location_id]
                        self.env['stock.quant'].reserve_quant_to_move(inf_move)
