# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# __copyright__ = 'GVADETO'
# __author__ = 'Enrique Avendaño Trujillo'
# __email__ = 'enrique.avendano@gvadeto.com'
##############################################################################

from openerp import models, api
from openerp.osv import fields, osv
from openerp.tools.translate import _
import re
from openerp import netsvc
import logging
_logger = logging.getLogger(__name__)


class sale_order(osv.osv):
    _inherit = "sale.order"

    def action_button_confirm(self):
        vals = None
        confirm = True
        check_line_qty = self.check_line_qty(vals, confirm)
        if check_line_qty:
            res = super(sale_order, self).action_button_confirm()
            if res:
                name = self.name
                self.env.cr.execute("""SELECT sp.id
                                FROM stock_picking AS sp
                                WHERE origin='{}'""".format(name))
                picking_id = self.env.cr.fetchone()[0]
                if picking_id:
                    pick_id = self.env['stock.picking'].browse(picking_id)
                    if pick_id.state == 'waiting':
                        pick_id.rereserve_pick()
                    if pick_id.state == 'confirmed':
                        pick_id.picking_reservation()
        return True

    def action_wait(self, cr, uid, ids, context=None):
        check_stock = self.check_stock(cr, uid, ids, context=None)
        if check_stock:
            res = super(sale_order, self).action_wait(cr, uid, ids, context)
        return res

    def create(self, cr, uid, vals, context=None):
        ids = None
        confirm = False
        check_line_qty = self.check_line_qty(cr, uid, ids, vals, confirm, context=None)
        if check_line_qty:
            new_id = super(sale_order, self).create(cr, uid, vals, context)
        return new_id

    def check_empty_lines(self, cr, uid, vals, context=None):
        res = True
        warning_msgs = ''
        order_line = vals['order_line']
        for line in order_line:
            dict = line[2]
            if dict == False:
                pass
            else:
                if 'product_uom_qty' in dict:
                    product_qty = dict['product_uom_qty']
                    if product_qty <= 0:
                        res = False
                        warning_msgs = ('¡La cantidad de producto debe ser mayor a (0.00)!\n')
                        error = ('¡Error! Cantidad(%.2f) Incorrecta.\n') % \
                                (max(0, product_qty))
        if warning_msgs:
            raise osv.except_osv(error, warning_msgs)
        return res

    def write(self, cr, uid, ids, vals, context=None):
        confirm = False
        check_line_qty = self.check_line_qty(cr, uid, ids, vals, confirm, context=None)
        if check_line_qty:
            res = super(sale_order, self).write(cr, uid, ids, vals, context)
        return res

    def check_line_qty(self, cr, uid, ids, vals, confirm, context=None):
        res = True
        warning_msgs = ''
        if confirm == True:
            cr.execute("""SELECT sol.product_uom_qty
                            FROM sale_order_line AS sol
                            WHERE order_id=%s""", (ids))
            lines = cr.fetchall()
            if lines:
                for line in lines:
                    qty = line[0]
                    if qty <= 0:
                        res = False
                        warning_msgs = ('¡La cantidad de producto debe ser mayor a (0.00)!\n')
                        error = ('¡Error! Cantidad(%.2f) Incorrecta.\n') % \
                                (max(0, qty))
        else:
            if 'order_line' in vals:
                order_line = vals['order_line']
                if order_line == []:
                    warning_msgs = ('¡No puedes guardar una cotización sin líneas de pedido!\n')
                    error = ('¡Error! (0) Líneas de pedido.\n')
                else:
                    write = False
                    for lines in order_line:
                        index = lines[0]
                        if index == 2:
                            pass
                        else:
                            write = True
                    if write:
                        for line in order_line:

                            dict = line[2]
                            if dict == False:
                                pass
                            else:
                                if 'product_uom_qty' in dict:
                                    product_qty = dict['product_uom_qty']
                                    if product_qty <= 0:
                                        res = False
                                        warning_msgs = ('¡La cantidad de producto debe ser mayor a (0.00)!\n')
                                        error = ('¡Error! Cantidad(%.2f) Incorrecta.\n') % \
                                                (max(0, product_qty))
                    else:
                        warning_msgs = ('¡No puedes guardar una cotización sin líneas de pedido!\n')
                        error = ('¡Error! (0) Líneas de pedido.\n')

        if warning_msgs:
            raise osv.except_osv(error, warning_msgs)
        return res

    def check_stock(self, cr, uid, ids, context=None):
        order_id = ids
        cr.execute("""SELECT sol.product_id, sol.name, sol.product_uom_qty,
                        so.warehouse_id FROM sale_order AS so
                        INNER JOIN sale_order_line AS sol ON sol.order_id=so.id
                        WHERE sol.order_id=%s order by sol.id""", (order_id))
        lines = cr.fetchall()
        if lines != []:
            res = True
            warning_msgs = ''
            for line in lines:
                product_tmpl_id = [line[0]]
                cr.execute("""SELECT pt.type
                                FROM product_template AS pt
                                INNER JOIN product_product AS pp ON pp.product_tmpl_id = pt.id
                                WHERE pp.id=%s""", (product_tmpl_id))
                type = cr.fetchone()[0]
                if type != 'service':
                    product_id = line[0]
                    name = line[1]
                    qty = line[2]
                    warehouse = line[3]
                    cr.execute("""SELECT SUM(sq.qty)
                                    FROM stock_warehouse AS sw INNER JOIN
                                    stock_location AS sl ON sw.lot_stock_id = sl.id
                                    INNER JOIN stock_quant sq ON sl.id=sq.location_id
                                    AND reservation_id is null
                                    WHERE sl.usage='internal' and sw.id=%s
                                    AND product_id=%s""", (warehouse, product_id))
                    if cr.rowcount:
                        quants = cr.fetchone()[0]
                        if quants is None or quants is False:
                            quants = 0.0
                        if qty > quants:
                            res = False
                            faltante = qty - quants
                            warn_msg = ('%s - Disponible(s) %.2f - Faltante(s) %.2f.\n') % \
                                       (name, max(0, quants), max(0, faltante))
                            warning_msgs += warn_msg
                            error = ('¡Producto(s) con existencia(s) insuficiente(s)!')
            if warning_msgs:
                raise osv.except_osv(error, warning_msgs)
        else:
            raise osv.except_osv(_('Error!'), _('You cannot confirm a sales order which has no line.'))
        return res

    def instance_workflow_draf(self, cr, uid, sale_id, context=None):
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_create(uid, 'sale.order', sale_id, cr)
        return True

class sale_order_line(osv.osv):
    _inherit = "sale.order.line"

    def product_id_change_with_wh(self, cr, uid, ids, pricelist, product, qty=0,
                                  uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                                  lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False,
                                  flag=False, warehouse_id=False, context=None):
        context = context or {}
        product_uom_obj = self.pool.get('product.uom')
        product_obj = self.pool.get('product.product')
        warning = {}
        # UoM False due to hack which makes sure uom changes price, ... in product_id_change
        res = self.product_id_change(cr, uid, ids, pricelist, product, qty=qty,
                                     uom=False, qty_uos=qty_uos, uos=uos, name=name, partner_id=partner_id,
                                     lang=lang, update_tax=update_tax, date_order=date_order, packaging=packaging,
                                     fiscal_position=fiscal_position, flag=flag, context=context)
        if not product:
            res['value'].update({'product_packaging': False})
            return res
        # set product uom in context to get virtual stock in current uom
        if 'product_uom' in res.get('value', {}):
            # use the uom changed by super call
            context = dict(context, uom=res['value']['product_uom'])
        elif uom:
            # fallback on selected
            context = dict(context, uom=uom)

        # update of result obtained in super function
        product_obj = product_obj.browse(cr, uid, product, context=context)
        res['value'].update(
            {'product_tmpl_id': product_obj.product_tmpl_id.id, 'delay': (product_obj.sale_delay or 0.0)})

        # Calling product_packaging_change function after updating UoM
        res_packing = self.product_packaging_change(cr, uid, ids, pricelist, product, qty, uom, partner_id, packaging,
                                                    context=context)
        res['value'].update(res_packing.get('value', {}))
        warning_msgs = res_packing.get('warning') and res_packing['warning']['message'] or ''
        error = ('¡Error de configuración!')

        if product_obj.type == 'product':
            # determine if the product needs further check for stock availibility
            is_available = self._check_routing(cr, uid, ids, product_obj, warehouse_id, context=context)
            uom_record = False
            if uom:
                uom_record = product_uom_obj.browse(cr, uid, uom, context=context)
                if product_obj.uom_id.category_id.id != uom_record.category_id.id:
                    uom_record = False
            if not uom_record:
                uom_record = product_obj.uom_id
            # <Update TODO: Product fix>
            if not warehouse_id:
                error = ('No ha seleccionado ningún almacén')
                warning_msgs += ('Seleccione un almacén')
            if warehouse_id and product:
                cr.execute("""SELECT SUM(sq.qty)
                            FROM stock_warehouse AS sw INNER JOIN
                            stock_location AS sl ON sw.lot_stock_id = sl.id
                            INNER JOIN stock_quant sq ON sl.id=sq.location_id
                            AND reservation_id is null
                            WHERE sl.usage='internal' and sw.id=%s
                            and product_id=%s""", (warehouse_id, product))
                if cr.rowcount:
                    quants = cr.fetchone()[0]
                    if quants is None or quants is False:
                        quants = 0.0
                    if quants < qty:
                        disponible = quants
                        if not is_available:
                            # res['value'].update({'product_uom_qty': quants, })
                            faltante = qty - disponible
                            warn_msg = (
                                           '¡Prevé vender %.2f %s pero sólo %.2f %s están disponibles!\nLa(s) %.2f %s faltante(s) únicamente podrá venderla(s) cuando se encuntre(n) en Stock.') % \
                                       (qty, uom_record.name,
                                        max(0, disponible), uom_record.name,
                                        max(0, faltante), uom_record.name)
                            warning_msgs += ('El stock real es de: %.2f %s.\n\n') % \
                                            (max(0, product_obj.qty_available), uom_record.name) + warn_msg + "\n\n"
                            error = ('¡Producto sobre existencias de almacén!')
                        else:
                            res['value'].update({'product_uom_qty': qty, })
                            faltante = qty - disponible
                            warn_msg = (
                                           '¡Prevé vender %.2f %s pero sólo %.2f %s están disponibles!\nLa(s) %.2f %s faltante(s) únicamente podrá venderla(s) bajo pedido.') % \
                                       (qty, uom_record.name,
                                        max(0, disponible), uom_record.name,
                                        max(0, faltante), uom_record.name)
                            warning_msgs += ('El stock real es de: %.2f %s.\n\n') % \
                                            (max(0, product_obj.qty_available), uom_record.name) + warn_msg + "\n\n"
                            error = ('¡Producto sobre pedido!')
                    else:
                        if quants > 0 and qty < quants:
                            pass
                        elif (quants > quants):
                            res['value'].update({'product_uom_qty': quants, })
                        elif (qty == 0.0 or qty == False or qty == None):
                            res['value'].update({'product_uom_qty': quants, })
            # <EndUpdate TODO>

        if product:
            x = self.calc_price_unit(
                cr, uid, ids, client_id=partner_id,
                product_id=product, currency_id=context['currency_id'],
                context=context
            )
            if 'price_unit' in x['value']:
                res['value']['price_unit'] = x['value']['price_unit']

        if warning_msgs:
            warning = {
                'title': error,  # ('¡Error de configuración!'),
                'message': warning_msgs
            }
        res.update({'warning': warning})
        return res
