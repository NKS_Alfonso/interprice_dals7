# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models


class stock_transfer_details(models.Model):
    _inherit = 'stock.transfer_details'

    def do_detailed_transfer(self,cr,uid,ids,context=None):
        super(stock_transfer_details,self).do_detailed_transfer(cr,uid,ids,context=context)
        try:
            cost_center_origen_id = 0
            cost_center_dest_id = 0
            account_valuation_inventory_id = 0
            total_qty = 0
            total_price = 0
            total = 0
            account_move_id = 0
            picking_name = ""

            stock_picking_id = context.get('active_id', False)
            stock_picking_obj = self.pool.get(context.get('active_model')).read(cr,uid,stock_picking_id)
            picking_name = stock_picking_obj['name']
            print stock_picking_obj
            #pdb.set_trace()
            if stock_picking_obj['picking_type_code'] == 'internal':
                stock_move_ids = stock_picking_obj['move_lines']
                period_ids = self.pool.get('account.period').search(
                                cr,
                                uid,
                                [('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
                journals_id = self.pool.get('account.journal').search(
                                cr,
                                uid,
                                [('code','=','STJ'),('type','=','general')])
                account_move_id = self.pool.get('account.move').create(
                                cr,
                                uid,
                                {
                                    'name':'/',
                                    'ref':stock_picking_obj['name'],
                                    'journal_id':journals_id[0],
                                    'period_id':period_ids[0],
                                    'date':datetime.datetime.now()})
                categ_id = 0
                qty = 0

                for id_mov in stock_move_ids:
                    stock_move_obj = self.pool.get('stock.move').read(cr,uid,id_mov)
                    warehouse_ids = self.pool.get('stock.warehouse').search(
                                        cr,
                                        uid,
                                        [('lot_stock_id','=',stock_move_obj['location_id'][0])])
                    warehouse_dest_ids = self.pool.get('stock.warehouse').search(
                                        cr,
                                        uid,
                                        [('lot_stock_id','=',stock_move_obj['location_dest_id'][0])])
                    warehouse_obj = self.pool.get('stock.warehouse').read(cr,uid,warehouse_ids)
                    warehouse_dest_obj = self.pool.get('stock.warehouse').read(cr,uid,warehouse_dest_ids)
                    #pdb.set_trace()
                    #print stock_move_obj
                    cost_center_origen_id = warehouse_obj[0]['centro_costo_id'][0]
                    cost_center_dest_id = warehouse_dest_obj[0]['centro_costo_id'][0]
                    #product_obj_id = self.pool.get('product.product').search(cr, uid,[('id','=',stock_move_obj['product_id'][0])])[0]
                    #product_obj = self.pool.get('product.product').read(cr,uid,stock_move_obj['product_id'][0])
                    product_tmp_obj = self.pool.get('product.template').read(cr, uid, stock_move_obj['product_tmpl_id'][0])
                    product_categ_obj = self.pool.get('product.category').read(cr, uid, product_tmp_obj['categ_id'][0])

                    #pdb.set_trace()
                    if categ_id == 0:
                        categ_id = product_categ_obj['id']
                    elif categ_id != product_categ_obj['id']:
                        categ_id = product_categ_obj['id']
                        total = total_price
                        total_qty = 0
                        total_price = 0

                        account_move_line_id = self.pool.get('account.move.line').create(
                                                cr,
                                                uid,
                                                {
                                                    'move_id': account_move_id,
                                                    'journal_id':journals_id[0],
                                                    'period_id':period_ids[0],
                                                    'date':datetime.datetime.now(),
                                                    'name': stock_picking_obj['name'],
                                                    'account_id':account_valuation_inventory_id,
                                                    'debit':0,
                                                    'credit':abs(total),
                                                    'cost_center_id': cost_center_origen_id
                                                }
                                            )
                        account_move_line_id = self.pool.get('account.move.line').create(
                                                cr,
                                                uid,
                                                {
                                                    'move_id': account_move_id,
                                                    'journal_id':journals_id[0],
                                                    'period_id':period_ids[0],
                                                    'date':datetime.datetime.now(),
                                                    'name': stock_picking_obj['name'],
                                                    'account_id':account_valuation_inventory_id,
                                                    'debit':abs(total),
                                                    'credit':0,
                                                    'cost_center_id': cost_center_dest_id
                                                }
                                            )
                        total = 0

                    account_valuation_inventory_id = product_categ_obj['property_stock_valuation_account_id'][0]
                    qty = stock_move_obj['product_qty']
                    total_price += (product_tmp_obj['lst_price'] * qty)

                total = total_price
                total_qty = 0
                total_price = 0

                account_move_line_id = self.pool.get('account.move.line').create(
                                cr,
                                uid,
                                {
                                    'move_id': account_move_id,
                                    'journal_id':journals_id[0],
                                    'period_id':period_ids[0],
                                    'date':datetime.datetime.now(),
                                    'name': stock_picking_obj['name'],
                                    'account_id':account_valuation_inventory_id,
                                    'debit':0,
                                    'credit':abs(total),
                                    'cost_center_id': cost_center_origen_id
                                }
                            )
                account_move_line_id = self.pool.get('account.move.line').create(
                                cr,
                                uid,
                                {
                                    'move_id': account_move_id,
                                    'journal_id':journals_id[0],
                                    'period_id':period_ids[0],
                                    'date':datetime.datetime.now(),
                                    'name': stock_picking_obj['name'],
                                    'account_id':account_valuation_inventory_id,
                                    'debit':abs(total),
                                    'credit':0,
                                    'cost_center_id': cost_center_dest_id
                                }
                            )
                total = 0
            elif stock_picking_obj['picking_type_code'] == 'incoming':
                cost_center_id = 0
                po_ids = self.pool.get('purchase.order').search(cr,uid,[('name','=',stock_picking_obj['origin'])])
                po = self.pool.get('purchase.order').browse(cr,uid,po_ids)
                for p in po:
                    cost_center_id = p.centro_costo_id.id

                account_mov_line_ids = self.pool.get('account.move.line').search(cr,uid,[('ref','=',picking_name)])
                account_mov_lin = self.pool.get('account.move.line')

                for id_mov in account_mov_line_ids:
                    account_mov_lin.write(cr,uid,id_mov,{'cost_center_id': cost_center_id})
        except Exception, e:
            print "ERROR en stock_transfer_details " + str(e)
        finally:
            return True
