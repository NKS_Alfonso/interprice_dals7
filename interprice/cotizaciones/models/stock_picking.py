# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, api, fields, _
from openerp.osv import osv
from datetime import datetime as dt
from dateutil import tz


class StockPicking(models.Model):
    """Stock Picking new transfer to data base"""

    # odoo model properties
    _inherit = 'stock.picking'


    state = fields.Selection(track_visibility='onchange')
    date = fields.Datetime(track_visibility=False)
    min_date = fields.Datetime(track_visibility=False)

    @api.one
    def write(self, values):
        res = super(StockPicking, self).write(values)
        fields = self.fields_get()
        followers = self._get_followers(None, None, context={})

        mail_message = self.env['mail.thread']
        value = ''
        for key, val in values.items():
            if fields[key]['type'] == 'datetime' or fields[key]['type'] == 'date':
                value = self.convert_date_timezone(val) + ',' + '<b>Timezone: </b>' + self.env.user.partner_id.tz
            elif 'recompute_pack_op' == key:
                continue
            elif 'relation' in fields[key]:
                value = self.env[fields[key]['relation']].browse(int(val)).name
            elif 'selection' in fields[key]:
                for tuple_values in fields[key]['selection']:
                    if tuple_values[0] == val:
                        value = tuple_values[1]
                        break
            else:
                value = val

            if value:
                self.message_post(
                    body=_(str(value)),
                    partner_ids=[],
                    subject=_(fields[key]['string'].capitalize()),
                    type='notification',
                    # content_subtype='plaintext',
                    author_id=self.env.user.partner_id.id
                )
        return res

    @api.model
    def create(self, vals):
        res = super(StockPicking, self).create(vals)
        if res.date:
            msg = _("""<b>Creation Date:</b> {}, <b>Timezone: {}</b>""".format(self.convert_date_timezone(res.date),self.env.user.partner_id.tz))
            res.message_post(
                body=msg,
                partner_ids=[],
                type='notification',
                author_id=self.env.user.partner_id.id
            )
        if res.min_date:
            msg = _("""<b>Planned Date:</b> {}, <b>Timezone: {}</b>""".format(self.convert_date_timezone(res.min_date),self.env.user.partner_id.tz))
            res.message_post(
                body=msg,
                partner_ids=[],
                type='notification',
                author_id=self.env.user.partner_id.id
            )
        return res

    def convert_date_timezone(self, date):
        if date:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz(self.env.user.partner_id.tz)
            utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S')
            utc = utc.replace(tzinfo=from_zone)
            central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
            return central
        else:
            return ''

    @api.one
    def picking_unreservation(self):
        """
          Will remove all quants for picking in picking_ids
        """
        moves_to_unreserve = []
        pack_line_to_unreserve = []
        self.env.cr.execute(
            """
            SELECT id, state FROM stock_move WHERE picking_id = %s
            """, (self.id,)
        )
        if self.env.cr.rowcount:
            for move_id, state in self.env.cr.fetchall():
                if state not in ('cancel', 'done'):
                    moves_to_unreserve.append(move_id)
        self.env.cr.execute(
            """
            SELECT id FROM stock_pack_operation WHERE picking_id = %s
            """, (self.id,)
        )
        if self.env.cr.rowcount:
            for po_id in self.env.cr.fetchall():
                pack_line_to_unreserve.append(po_id[0])
        if moves_to_unreserve:
            if pack_line_to_unreserve:
                self.env['stock.pack.operation'].browse(pack_line_to_unreserve).unlink()
            self.env['stock.move'].browse(moves_to_unreserve).assign_unreservation()

    @api.one
    def picking_reservation(self):
        """ Check availability of picking moves.
        This has the effect of changing the state and reserve quants on available moves, and may
        also impact the state of the picking as it is computed based on move's states.
        @return: True
        """
        move_ids = []
        self.env.cr.execute(
            """
            SELECT id, state FROM stock_move WHERE picking_id = %s
            """, (self.id,)
        )
        if self.env.cr.rowcount:
            for move_id, state in self.env.cr.fetchall():
                if state not in ('draft', 'cancel', 'done'):
                    move_ids.append(move_id)
        if not move_ids:
            raise osv.except_osv(_('Warning!'), _('Nothing to check the availability for.'))
        if self.state == 'draft':
            self.action_confirm()
        self.env['stock.move'].browse(move_ids).assign_reservation()
        return True

    @api.multi
    def force_assign(self):
        res = super(StockPicking, self).force_assign()
        self.message_post(body=_('<b>Forced Availability</b>'))
        return res

    @api.multi
    def action_assign(self):
        if self._context.get('button_action_assign', False):
            return self.picking_reservation()
        return super(StockPicking, self).action_assign()

    @api.multi
    def rereserve_pick(self):
        if self._context.get('rereserve_pick', False):
            return self.picking_reservation()
        return super(StockPicking, self).rereserve_pick()

    @api.multi
    def do_unreserve(self):
        if self._context.get('do_unreserve', False):
            self.message_post(body=_('<b>Unreserved</b>'))
            return self.picking_unreservation()
        return super(StockPicking, self).do_unreserve()

    @api.multi
    def action_cancel(self):
        res = super(StockPicking, self).action_cancel()
        if self.state == 'cancel':
            fields = self.fields_get()
            for key, val in fields['state']['selection']:
                if key == 'cancel':
                    state_description = str(val)
            str_state = fields['state']['string']
            post_msg = _('{}').format(state_description)
            self.message_post(
                body=post_msg,
                partner_ids=[],
                subject=str_state,
                type='notification',
                author_id=self.env.user.partner_id.id
            )
        return res

class StockReturnPickingInherit(models.Model):
    _inherit = 'stock.return.picking'


    @api.multi
    def create_returns(self):
        res = super(StockReturnPickingInherit, self).create_returns()
        stock_picking_obj = self.env['stock.picking']
        active_id = self._context.get('active_id')
        if active_id:
            picking_id = stock_picking_obj.browse(active_id)
            if picking_id:
                picking_id.message_post(body=_('<b>Returned transfer</b>'))
        return res
