# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# __copyright__ = 'GVADETO'
# __author__ = 'Enrique Avendaño Trujillo'
# __email__ = 'enrique.avendano@gvadeto.com'
##############################################################################

{
    'name': 'Cotizaciones',
    'version': '1.0',
    'author': 'Enrique Avendaño Trujillo(GVadeto)',
    'category': 'Sale',
    'description': """
        Es un módulo que hereda al modelo sale.order los requerimientos y necesidades de interprice.
        Grupos que se usan denegar acceso.
        * Ventas/Cotizaciones/Pedidos/Lectura
        * Ventas/Cotizaciones/Pedidos/Invisible
         """,
    'website': 'https://',
    'license': 'AGPL-3',
    'depends': ['sale', 'sale_stock', 'delivery', 'price_list'],
    'init_xml': [],
    'demo_xml': [],
    'update_xml': [
                'views/cotizaciones_view.xml',
                'views/stock_pinking_view.xml',
                'views/sale_order_line_view.xml',
                'views/purchase_order_view.xml'

                 ],
    'installable': True,
    'active': False,
}
