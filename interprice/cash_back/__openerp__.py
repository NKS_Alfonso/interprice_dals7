# -*- coding: utf-8 -*-
{
    'name': 'Cash Back',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Accounting',
    'description': 'Manage cash back transactions',
    'depends': ['base', 'account', 'account_check'],
    'application': True,
    'data': [
        'views/cash_back_view.xml',
        'views/cash_back_sequence.xml',
        'views/account_check_view.xml',
        'reports/views/cash_back_by_date_bank_view.xml',
        'security/ir.model.access.csv',
    ]
}
