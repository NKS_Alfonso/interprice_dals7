��    p      �  �         p	     q	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     6
  /   ?
     o
     v
  	   
  	   �
     �
     �
     �
     �
     �
     �
          ,     I     Y     m     �     �     �  #   �     �     �  
   �     �       	        $  
   +  
   6     A     M     b     s     |  .   �  0   �  2   �               -     >     N     f     u     �     �     �     �     �  	   �  	   �     �     �  	   �  �   �     �  /   �  )   �     �     �     	          ,     <     L     e     ~  "   �     �     �      �     �     �        *     	   2     <     Y     `     q     }     �     �     �     �     �     �     �     �     �     �     �     �  
   �  	   �     �        E  	     O     V     ]     n     w     �     �     �     �     �     �     �     �       F   (     o  	   x     �     �     �  3   �  !      $   "  $   G     l  .   �  3   �  #   �       +     &   D  $   k     �  (   �     �     �     �     �                 
   !  
   ,     7     H  !   f     �     �  .   �      �     �  	             %     9     O     j     ~     �     �     �     �     �  	   �     �     �            �        �  /   �  ,   �                .     @     R     b  .   r  <   �     �  "   �     
     )  "   7     Z     h     q  C   y  
   �  (   �     �     �               !     )     7     C     L     \     d     l  	   u          �     �     �     �     �     �     B       @          %   ;   !   T   \       8   1   n   ,             >   #   +      e   b                d   V             i   C                     `   :   =   G          f   W      2       S          k   N   L   (       	   h       D   P   p   )       H         Z      _   *   F   m   a   
   E   I   j           ]   3   g      9   -   l   5   /          0   ^              4       ?       O   Y           U   $   K         J   R                        X   <          Q       c               o   6   &          A      7           "   .   [   M           '        Account Account Check Account Entry Advance payment Advance payment document Advance payment ref. Amount Balance Bank Bank account name Bank account number Bank information Bank journal checkbooks CSV File Can't cancel a cash back with an active check.
 Cancel Canceled Cash Back Cash back Cash back amount Cash back by date/bank report Cash back canceled  Cash back check Cash back client name Cash back currency Cash back date on validation Cash back document reference Cash back folio Cash back from date Cash back observations Cash back policies Cash back status Cash back to date Cash back transactions by date/bank Cash back type Check Check num. Check reference Check: %s 
 Checkbook Client Created by Created on Credit note Credit note document Credit note ref. Currency Date Date of the last message posted on the record. Do you want to cancel the cash back transaction? Do you want to validate the cash back transaction? Document Document amount Document balance Document client Document currency banks Document folio Document information Document type Draft Electronic transfer Email Thread Folio Followers From date General General Wizard Generated Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. Import must be equal or less than balance Import must be greater than 0 Is a Follower Journal currency Last Message Date Last Updated by Last Updated on Last move cash back date Last move cash back user Messages Messages and communication history No records found! Observations Only if this check is post dated Payment Date Policies Policy Records must be on draft status for delete Reference Same document currency banks Screen Search cash back State: %s 
 Status Summary Suspense account To date Tran_ref Unread Messages User Validate adv canceled check cn draft electronic generated to tran_ref Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-02-07 20:55+0000
PO-Revision-Date: 2017-02-07 14:58-0600
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
 Cuenta Cheque Entrada contable Anticipo Documento de anticipo Referencia del anticipo Importe Saldo Banco Nombre del banco Número de cuenta del banco Información del banco Chequeras del diario del banco Archivo CSV No se puede cancelar una devolución de efectivo con un cheque activo
 Cancelar Cancelado Devolución de efectivo Devolución de efectivo Importe de la devolución Reporte de devoluciones de efectivo por fecha/banco Devolución de efectivo cancelada Cheque de la devolución de efectivo Nombre del cliente de la devolución Moneda de la devolución Fecha de la devolución de efectivo al validar Referencia del documento de devolución de efectivo Folio de la devolución de efectivo Fecha desde Observaciones de la devolución de efectivo Pólizas de la devolución de efectivo Estado de la devolución de efectivo Fecha hasta Devoluciones de efectivo por fecha/banco Tipo de devolución Cheque Núm. Cheque Referencia del cheque Cheque: %s 
 Chequera Cliente Created by Created on Nota de crédito Documento de nota de crédito Referencia de la nota de crédito Moneda Fecha Date of the last message posted on the record. ¿Desea cancelar la devolución? ¿Desea validar la devolución? Documento Importe del documento Saldo del documento Cliente del documento Bancos con la misma moneda Folio del documento Información del documento Tipo de documento Borrador Transferencia electrónica Hilo de mensajes Folio Followers Fecha desde General General Wizard Generado Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. El importe debe ser menor o igual al balance El importe debe ser mayor a 0 Is a Follower Moneda del diario Last Message Date Last Updated by Last Updated on Fecha del último movimiento de la devolución Usuario que realizó el último movimiento de la devolución Messages Messages and communication history ¡No se encontraron registros! Observaciones Sólo si este cheque es posfechado Fecha de pago Pólizas Póliza Los registros deben estar en estado de borrador para poder eliminar Referencia Bancos con la misma moneda del documento Pantalla Buscar devolución Estado: %s 
 Estado Summary Cuenta puente Fecha hasta Tran_ref Unread Messages Usuario Validar Anticipo Cancelado Cheque Nota de crédito Borrador Transferencia electrónica Generado a tran_ref 