# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import models, fields, api, exceptions, tools, _


class CashBackByDateBank(models.TransientModel):
    """Cash back report by date/bank Wizard"""

    # odoo model properties
    _name = 'r.cash_back.by.date_bank'

    # Report filter fields
    date_from = fields.Date(
        help="Cash back from date",
        string="From date",
    )
    date_to = fields.Date(
        help="Cash back to date",
        string="To date",
    )
    bank = fields.Many2one(
        comodel_name='res.partner.bank',
        help="Document currency banks",
        string="Bank",
    )

    # Methods
    def _get_ids(self):
        # Filters
        self.ensure_one()
        date_from = self.date_from
        date_to = self.date_to
        bank_id = self.bank.id

        # Query
        sql = "SELECT id FROM r_cash_back_by_date_bank_view"

        if date_from or date_to or bank_id:
            sql += ' WHERE'

        # from, to and bank
        if date_from and date_to and bank_id:
            sql += " date >= '%s' AND date <= '%s' AND bank_id = %i" % (
                date_from, date_to, bank_id)
        # from and to
        elif date_from and date_to:
            sql += " date >= '%s' AND date <= '%s'" % (date_from, date_to)
        # from and bank
        elif date_from and bank_id:
            sql += " date >= '%s' AND bank_id = %i" % (date_from, bank_id)
        # from
        elif date_from:
            sql += " date >= '%s'" % (date_from)
        # to and bank
        elif date_to and bank_id:
            sql += " date <= '%s' AND bank_id = %i" % (date_to, bank_id)
        # to
        elif date_to:
            sql += " date <= '%s'" % (date_to)
        # bank
        elif bank_id:
            sql += " bank_id = %i" % (bank_id)

        sql += " ORDER BY date DESC"
        sql += ";"  # Close query

        self.env.cr.execute(sql)

        if self.env.cr.rowcount:
            view_ids = [int(i[0]) for i in self.env.cr.fetchall()]
        else:
            self.unlink()  # Delete temp data
            raise exceptions.Warning(_("No records found!"))

        return view_ids

    @api.multi
    def button_screen(self):
        ids = self._get_ids()
        return {
            'name': _('Cash back transactions by date/bank'),
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': 'r.cash_back.by.date_bank.view',
            'target': 'current',
            'context': {},
            'domain': [('id', 'in', ids)]
        }

    @api.multi
    def button_csv(self):
        ids = self._get_ids()
        csv_id = self.env['r.cash_back.by.date_bank.view'].csv_by_ids(_ids=ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    @api.onchange('date_from', 'date_to', 'bank')
    def validate_filters(self):
        df = self.date_from
        dt = self.date_to

        if df and dt and df > dt:
            self.date_from = False
            self.date_to = False
            raise exceptions.Warning(
                "'Date to' must be higher than 'Date from'.")


class CashBackByDateBankView(models.Model):
    """Cash back report by date/bank Wizard"""

    # odoo model properties
    _name = 'r.cash_back.by.date_bank.view'
    _table = 'r_cash_back_by_date_bank_view'
    _description = 'Cash back transactions by date/bank'
    _auto = False  # Don't let odoo create table
    # _order = 'id DESC'

    # View fieds
    folio = fields.Char(
        help="Cash back folio",
        readonly=True,
        string="Folio",
    )
    ref = fields.Char(
        readonly=True,
        string="Reference",
    )
    document_type = fields.Char(
        readonly=True,
        string="Document type",
    )
    document_folio = fields.Char(
        help="Advance payment document",
        readonly=True,
        string="Document folio",
    )
    date = fields.Date(
        help="Last move cash back date",
        readonly=True,
        string="Date",
    )
    client = fields.Char(
        help="Cash back client name",
        readonly=True,
        string="Client"
    )
    currency = fields.Char(
        help="Cash back currency",
        readonly=True,
        string="Currency"
    )
    amount = fields.Float(
        digits=(12, 2),
        help="Cash back amount",
        readonly=True,
        string="Amount",
    )
    user = fields.Char(
        help="Last move cash back user",
        readonly=True,
        string="User"
    )
    tran_ref = fields.Char(
        help="Check reference",
        readonly=True,
        string="Tran_ref"
    )
    bank = fields.Char(
        help="Bank account name",
        readonly=True,
        string="Bank"
    )
    account = fields.Char(
        help="Bank account number",
        readonly=True,
        string="Account"
    )
    status = fields.Char(
        help="Cash back status",
        readonly=True,
        string="Status"
    )
    observations = fields.Text(
        help="Cash back observations",
        readonly=True,
        string="Observations"
    )

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'r_cash_back_by_date_bank_view')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_cash_back_by_date_bank_view
            AS (
                SELECT
                    ROW_NUMBER() OVER (ORDER BY cb.id) AS id,
                    cb.folio AS folio,
                    CASE WHEN cb.cb_type='check'
                            THEN 'Cheque'
                        ELSE 'Transferencia electrónica'
                    END AS ref,
                    CASE WHEN cb.document_type='adv'
                            THEN 'Anticipo'
                        ELSE 'Nota de crédito'
                    END AS document_type,
                    CASE WHEN document_type='adv'
                            THEN av.number
                        ELSE ai.number
                    END AS document_folio,
                    cb.date AS date,
                    rp.name AS client,
                    rc.name AS currency,
                    cb.amount AS amount,
                    rpu.name AS user,
                    ac.number AS tran_ref,
                    rpb.bank_name AS bank,
                    rpb.acc_number AS account,
                    rpb.id AS bank_id,
                    CASE WHEN cb.status='generated'
                            THEN 'Generado'
                        WHEN cb.status='canceled'
                            THEN 'Cancelado'
                        ELSE 'Borrador'
                    END AS status,
                    cb.observations AS observations
                FROM cash_back cb
                LEFT JOIN account_voucher av ON av.id =
                    CASE WHEN cb.document_type='adv'
                        THEN cb.document_adv
                END
                LEFT JOIN account_invoice ai ON ai.id =
                    CASE WHEN cb.document_type='cn'
                        THEN cb.document_cn
                END
                LEFT JOIN account_check ac ON ac.id =
                    CASE WHEN cb.cb_type='check'
                        THEN cb.check
                    ELSE NULL
                END
                INNER JOIN res_partner rp ON rp.id =
                    CASE WHEN cb.document_type='adv'
                        THEN av.partner_id
                    ELSE ai.partner_id
                END
                INNER JOIN res_currency rc ON rc.id = cb.currency
                INNER JOIN res_partner_bank rpb ON rpb.id = cb.bank
                INNER JOIN res_users ru ON ru.id = cb.write_uid
                INNER JOIN res_partner rpu ON rpu.id = ru.partner_id
            )
            """
        )

    def csv_by_ids(self, _ids):
        data = self.env['r.cash_back.by.date_bank.view'].browse(_ids)

        data_csv = [
            [
                _('Folio'),
                _('Reference'),
                _('Document'),
                _('Folio'),
                _('Date'),
                _('Client'),
                _('Currency'),
                _('Amount'),
                _('User'),
                _('tran_ref'),
                _('Bank'),
                _('Account'),
                _('Status'),
                _('Observations')
            ]
        ]

        for row in data:
            data_csv.append([
                unicode(self._set_default(row.folio)).encode('utf8'),
                unicode(self._set_default(row.ref)).encode('utf8'),
                unicode(self._set_default(row.document_type)).encode('utf8'),
                unicode(self._set_default(row.document_folio)).encode('utf8'),
                unicode(self._set_default(row.date)).encode('utf8'),
                unicode(self._set_default(row.client)).encode('utf8'),
                unicode(self._set_default(row.currency)).encode('utf8'),
                unicode(self._set_default(row.amount, 0)).encode('utf8'),
                unicode(self._set_default(row.user)).encode('utf8'),
                unicode(self._set_default(row.tran_ref)).encode('utf8'),
                unicode(self._set_default(row.bank)).encode('utf8'),
                unicode(self._set_default(row.account)).encode('utf8'),
                unicode(self._set_default(row.status)).encode('utf8'),
                unicode(self._set_default(row.observations)).encode('utf8')
            ])
        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'CashBack_by_date_bank.csv',
                'type_file': 'csv',
                'file': file,
            })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
