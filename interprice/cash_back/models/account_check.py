# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import (models, fields)


class AccountCheck(models.Model):
    """Extended class for overriding methods and fields for checks"""

    # odoo model properties
    _inherit = 'account.check'

    # adds cash back reference
    cash_back = fields.Many2one(
        comodel_name='cash.back',
        help="Cash back document reference",
        ondelete='cascade',
        string="Cash back",
        readonly=True,
    )
