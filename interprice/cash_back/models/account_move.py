# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields


class AccountMove(models.Model):
    """Extended class for new relation between policies and documents"""

    # odoo model properties
    _inherit = 'account.move'

    # add cash back reference
    cash_back = fields.Many2one(
        comodel_name='cash.back',
        help="Cash back",
        string="Cash back",
    )
