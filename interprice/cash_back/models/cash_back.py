# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
from datetime import datetime
import math
# related third party imports
# local application/library specific imports
from openerp import models, fields, api, exceptions, _


class CashBack(models.Model):
    """Model for cash back transactions."""

    # odoo model properties
    _name = 'cash.back'
    _inherit = ['mail.thread']
    _rec_name = 'folio'

    # field selection options
    cash_back_type = [
        ('check', _('Check')),
        ('electronic', _('Electronic transfer'))
    ]
    cash_back_status = [
        ('draft', _('Draft')),
        ('generated', _('Generated')),
        ('canceled', _('Canceled')),
    ]
    cash_back_document_type = [
        ('adv', _('Advance payment')),
        ('cn', _('Credit note'))
    ]

    # custom fields
    cb_type = fields.Selection(
        default='electronic',
        required=True,
        selection=cash_back_type,
        string="Cash back type",
    )
    folio = fields.Char(
        copy=False,
        default='_',
        help="Cash back folio",
        readonly=True,
        store=True,
        string="Folio",
    )
    date = fields.Date(
        copy=False,
        help="Cash back date on validation",
        readonly=True,
        string="Date",
    )
    status = fields.Selection(
        copy=False,
        default='draft',
        help="Cash back status",
        selection=cash_back_status,
        string="Status",
    )
    document_type = fields.Selection(
        default='adv',
        selection=cash_back_document_type,
        string="Document type",
    )
    balance = fields.Float(
        digits=(12, 2),
        help="Document balance",
        readonly=True,
        store=True,
        string="Balance",
    )
    amount = fields.Float(
        digits=(12, 2),
        help="Document amount",
        required=True,
        string="Amount",
    )
    observations = fields.Text(
        help="Cash back observations",
        string="Observations",
    )

    # relation fields
    client = fields.Many2one(
        comodel_name='res.partner',
        help="Document client",
        readonly=True,
        store=True,
        string="Client",
    )
    document_adv = fields.Many2one(  # All payments with no documents attached
        comodel_name='account.voucher',
        help="Advance payment document",
        string="Document",
    )
    document_cn = fields.Many2one(  # All credit notes with residual
        comodel_name='account.invoice',
        domain=['&', ('type', '=', 'out_refund'), ('residual', '>', 0)],
        help="Credit note document",
        string="Document",
    )
    currency = fields.Many2one(
        comodel_name='res.currency',
        help="Journal currency",
        readonly=True,
        store=True,
        string="Currency",
    )
    bank = fields.Many2one(
        comodel_name='res.partner.bank',
        help="Same document currency banks",
        required=True,
        string="Bank",
    )
    account = fields.Many2one(
        comodel_name='account.account',
        domain=['&', ('type', '=', 'other'), ('active', '=', True)],
        required=True,
        string="Suspense account",
    )
    policy_ids = fields.One2many(
        comodel_name='account.move',
        help="Cash back policies",
        inverse_name='cash_back',
        readonly=True,
        string="Policies",
    )
    checkbook = fields.Many2one(
        comodel_name='account.checkbook',
        help="Bank journal checkbooks",
        string="Checkbook",
    )
    check = fields.Many2one(
        comodel_name='account.check',
        help="Cash back check",
        readonly=True,
        string="Check num.",
    )
    check_payment_date = fields.Date(
        help="Only if this check is post dated",
        string='Payment Date',
    )

    # General (Public) methods
    def get_company(self):
        company = self.env['res.company']._company_default_get('cash.back')
        return self.env['res.company'].browse(company)

    def get_current_period(self):
        """Returns fiscal period of date with only
        month and year.
        For cash back: date = validation date."""
        month_year = datetime.now().strftime("%m/%Y")
        return self.env['account.period'].search([('name', '=', month_year)])

    def get_rate_amount(self, _amount, _currency):
        """Returns the amount divided by the current currency
        rate and gets rounded to 2 dec."""
        # rate = _currency.rate_silent
        rate = _currency.rate_sale_silent
        amount = _amount / rate
        return math.ceil(amount * 100) / 100  # Round 2 dec

    def get_reference_line(self, _document):
        """This method retrieves the reference line of an account.voucher.
        Analyse all related lines and retrieves the line with residual (ref).
        This is used when creating new account.move (policies)."""
        ref_line = False
        move_lines = self.env['account.move.line'].search([
            ('move_id', '=', _document.move_id.id),
            ('state', '=', 'valid'),
            ('account_id.type', '=', 'receivable'),
            ('reconcile_id', '=', False),
            # ('reconcile_ref', '=', False),
            ('partner_id', '=', _document.partner_id.id),
            ('is_tax_voucher', '=', False)
        ])
        move_lines = [
            ml for ml in move_lines
            if ml.amount_residual or ml.amount_residual_currency]
        for line in move_lines:
            ref_line = line
        return ref_line

    def get_policy_lines(self, _policy, _type=''):
        """Returns the _policy lines depending on type.
        If no type is received, this method will retrieve all _policy lines
        Types are debit & credit.
        All lines may include reconciled lines and taxed lines."""
        if _type == 'debit':
            return self.env['account.move.line'].search(
                [('move_id', '=', _policy.id),
                 ('credit', '=', '0'),
                 ('debit', '!=', '0'),
                 ('is_tax_voucher', '=', False)])
        elif _type == 'credit':
            return self.env['account.move.line'].search(
                [('move_id', '=', _policy.id),
                 ('debit', '=', '0'),
                 ('credit', '!=', '0'),
                 ('is_tax_voucher', '=', False)])
        else:
            return _policy.line_id  # Returns all move lines

    def _clean_fields(self):
        """This method is called on document_type onchange to clear
        fields to prevent trash data on submit."""
        self.account = False
        self.amount = 0
        self.balance = 0
        self.bank = False
        self.checkbook = False
        self.client = False
        self.currency = False
        self.document_adv = False
        self.document_cn = False

    def get_advance_currency(self, _document):
        """Returns account.voucher currency if set
        or current company currency if not set.
        This method can be called from outside """
        if _document.journal_id.currency:
            return _document.journal_id.currency
        else:
            return self.get_company().currency_id

    def get_advance_balance(self, _document, _currency):
        """This method retrieves the current balance of account.voucher,
        Analyse all related documents and calculates it's total
        residual balance."""
        amount_residual = 0.00
        move_line = self.env['account.move.line'].search([
            ('move_id', '=', _document.move_id.id),
            ('state', '=', 'valid'),
            ('account_id.type', '=', 'receivable'),
            ('reconcile_id', '=', False),
            # ('reconcile_ref', '=', False),
            ('partner_id', '=', _document.partner_id.id),
            ('is_tax_voucher', '=', False)
        ])
        if _currency == self.get_company().currency_id:
            amount_residual = abs(move_line.amount_residual)
        else:
            amount_residual = abs(move_line.amount_residual_currency)

        return amount_residual

    def _create_cash_back_policy(self, _document, _reverse=False):
        """Creates a cash back policy and returns the policy object
        generated.
        - Validates if it's a policy or a reverse movement to determine
        debit/credit amounts."""
        today_str = fields.Date.context_today(self)  # Validation date
        period = self.get_current_period()
        amount = self.get_rate_amount(
            _amount=self.amount, _currency=self.currency)
        amount_currency_credit = 0
        amount_currency_debit = 0

        if _reverse:
            amount_currency_credit = self.amount if self.currency != self.get_company().currency_id else 0
            amount_currency_debit = -1 * \
                abs(self.amount) if self.currency != self.get_company(
                ).currency_id else 0
        else:
            amount_currency_credit = -1 * \
                abs(self.amount) if self.currency != self.get_company(
                ).currency_id else 0
            amount_currency_debit = self.amount if self.currency != self.get_company().currency_id else 0

        ref = _("Cash back canceled " + self.folio) if _reverse else self.folio
        # Create policy header
        header = {
            'state': 'draft',
            'journal_id': self.bank.journal_id.id,
            'period_id': period.id,
            'ref': ref,
            'date': today_str,
            'partner_id': self.client.id
        }
        policy = self.env['account.move'].create(header)
        # Create policy credit line
        line_credit = {
            'name': self.folio,
            'partner_id': self.client.id,
            'account_id': self.bank.journal_id.default_credit_account_id.id,
            'debit': amount if _reverse else 0,
            'credit': 0 if _reverse else amount,
            'currency_id': False if self.currency == self.get_company().currency_id else self.currency.id,
            'amount_currency': amount_currency_credit,
            'period_id': period.id,
            'journal_id': self.bank.journal_id.id,
            'move_id': policy.id,
            'date': self.date,
        }
        line_credit = self.env['account.move.line'].create(line_credit)
        # Create policy debit line
        line_debit = {
            'name': '/',
            'partner_id': self.client.id,
            'account_id': self.account.id,
            'debit': 0 if _reverse else amount,
            'credit': amount if _reverse else 0,
            'currency_id': False if self.currency == self.get_company().currency_id else self.currency.id,
            'amount_currency': amount_currency_debit,
            'period_id': period.id,
            'journal_id': self.bank.journal_id.id,
            'move_id': policy.id,
            'date': self.date,
        }
        line_debit = self.env['account.move.line'].create(line_debit)
        # Reconcile credit line

        # No reconcile debit line instead sets original reconcile
        """if self.document_type == 'adv':
            # reconcile_line = self.get_policy_lines(
            #    _policy=_document.move_id, _type='debit')
            reconcile_line = self.get_reference_line(_document=_document)
        elif self.document_type == 'cn':
            reconcile_line = self.get_policy_lines(
                _policy=_document.move_id, _type='credit')

        if reconcile_line:
            if not reconcile_line.reconcile_ref:
                reconcile_line.reconcile_partial(
                    writeoff_period_id=period.id,
                    writeoff_journal_id=_document.journal_id.id)
            line_debit.write({
                'reconcile_partial_id': reconcile_line.reconcile_partial_id.id,
                'reconcile_ref': reconcile_line.reconcile_ref, })
            line_credit.reconcile_partial(writeoff_period_id=period.id,
                                          writeoff_journal_id=self.bank.journal_id.id)"""
        reconcile_line = self.get_reference_line(_document=_document)
        line_debit.write({
            'reconcile_partial_id': reconcile_line.reconcile_partial_id.id,
            'reconcile_ref': reconcile_line.reconcile_ref, })
        return policy

    def _create_cash_back_check(self, _policy):
        """ This method creates a cash back check
        and returns the check object generated."""
        payment_date = self.check_payment_date if self.check_payment_date else fields.Date.context_today(
            self)

        check = {
            'number': self.checkbook.next_check_number,
            'checkbook_id': self.checkbook.id,
            'amount': self.amount,
            'company_currency_amount': self.get_rate_amount(_amount=self.amount, _currency=self.currency) if self.currency != self.get_company().currency_id else None,
            # 'voucher_id': 1,  # Not needed
            'other_partner_id': self.client.id,  # Needed if not voucher
            'type': 'issue_check',
            'issue_date': fields.Date.context_today(self),
            'issue_check_subtype': self.checkbook.issue_check_subtype,
            'journal_id': self.bank.journal_id.id,
            'payment_date': payment_date,
            'cash_back': self.id,
            'company_id': self.get_company().id,
        }
        self.check_payment_date = payment_date
        return self.env['account.check'].create(check)

    @api.one
    def button_validate(self):
        """This is the action called on view button, this method validates
        all the information in the cash back transaction and creates a policy
        and a check if needed for the current record."""

        self.folio = self.env['ir.sequence'].get('cash.back')

        # Add new payment (CASH BACK) to the original document
        if self.document_type == 'adv':
            self.document_adv.ensure_one()
            policy = self._create_cash_back_policy(  # Get policy
                _document=self.document_adv, _reverse=False)
        elif self.document_type == 'cn':
            self.document_cn.ensure_one()
            policy = self._create_cash_back_policy(  # Get policy
                _document=self.document_cn, _reverse=False)

        policy.ensure_one()
        policy.post()

        if self.cb_type == 'check':
            # Get cash back check
            check = self._create_cash_back_check(_policy=policy)
            check.ensure_one()
            check.signal_workflow('draft_router')
            self.check = check.id

        self.status = 'generated'  # Update cash back status
        self.date = fields.Date.context_today(self)
        self.policy_ids += policy  # add new policy to cash back transaction

        post_vars = {
            'subject': "Information",
            'body': "Cash back validated"
        }
        self.message_post(type="notification",
                          subtype="mt_comment", **post_vars)
        return {
            'type': 'ir.actions.client',
            'tag': 'action_info',
            'name': 'button_validate',
            'params': {
                'title': 'Correct validation',
                'text': 'Verify the generated documents',
                'sticky': False
            }
        }

    @api.one
    def button_cancel(self):
        """This is the action called on view button, this method cancels
        all the information in the cash back transaction and creates a policy
        and a check if needed for the current record."""
        if ((self.check and self.cb_type == 'check') and
                (self.check.state != 'draft' and self.check.state != 'handed')):
            raise exceptions.Warning(
                _("Can't cancel a cash back with an active check.\n") +
                _("Check: %s \n") % (self.check.name) +
                _("State: %s \n") % (self.check.state))
        else:
            self.check.action_cancel()
        # Get reversed policy
        if self.document_type == 'adv':
            policy = self._create_cash_back_policy(
                _document=self.document_adv, _reverse=True)
        if self.document_type == 'cn':
            policy = self._create_cash_back_policy(
                _document=self.document_cn, _reverse=True)

        policy.ensure_one()
        policy.button_cancel()
        policy.button_validate()

        self.policy_ids += policy  # add new policy to cash back transaction

        self.status = 'canceled'  # Update cash back status
        self.date = fields.Date.context_today(self)

        post_vars = {
            'subject': "Information",
            'body': "Cash back canceled"
        }
        self.message_post(type="notification",
                          subtype="mt_comment", **post_vars)
        return {
            'type': 'ir.actions.client',
            'tag': 'action_info',
            'name': 'button_cancel',
            'params': {
                'title': 'Canceled',
                'text': 'Verify the generated documents',
                'sticky': False
            }
        }

    @api.onchange('document_type')
    def cash_back_change(self):
        self._clean_fields()

    @api.onchange('document_cn')
    def get_credit_note_info(self):
        """This values are only for interface purpose,
        the saving functionality is defined on create/write
        overrided methods bellow."""
        self.client = self.document_cn.partner_id
        self.currency = self.document_cn.currency_id
        self.balance = self.document_cn.residual
        self.bank = False
        self.account = False
        self.checkbook = False

    @api.onchange('currency', 'cb_type')
    def get_banks_on_currency(self):
        """Filters all banks configured with the same currency
        as the current cash back document.
        If the cash back transaction includes a check
        then it will retrieve all banks with checkbooks."""
        res = {}
        account_domain = []
        bank_domain = []
        if self.currency:
            # Filter accounts
            if self.currency == self.get_company().currency_id:
                account_domain = [
                    ('currency_id', '=', False),
                    ('type', '=', 'other'),
                    ('active', '=', True)
                ]
            else:
                account_domain = [
                    ('currency_id', '=', self.currency.id),
                    ('type', '=', 'other'),
                    ('active', '=', True)
                ]
            if self.cb_type == 'check':
                account_checkbook = [ach.journal_id.id for ach in self.env[
                    'account.checkbook'].search([('id', '!=', 0)])]

                account_journal = self.env['account.journal'].search(
                    ['&', ('payment_subtype', '=', 'issue_check'),
                     ('id', 'in', account_checkbook)])
                account_journal = [aj.id for aj in account_journal]

                bank_domain = ['&', ('currency2_id', '=', self.currency.id),
                               ('journal_id', 'in', account_journal)]
            elif self.cb_type == 'electronic':
                bank_domain = ['&', ('currency2_id', '=', self.currency.id),
                               ('journal_id', '!=', False)]

        res['domain'] = {
            'account': account_domain,
            'bank': bank_domain,
        }

        self.bank = False
        self.account = False
        self.checkbook = False

        return res

    @api.onchange('bank')
    def get_checkbook_on_bank(self):
        """Returns all checkbooks configured for the current bank journal
        ONLY if cash back type is check"""
        res = {}
        if self.bank and self.cb_type == 'check':
            res['domain'] = {
                'checkbook': [('journal_id', '=', self.bank.journal_id.id)]
            }
        self.checkbook = False
        return res

    @api.onchange('balance')
    def set_amount_on_balance(self):
        self.amount = self.balance

    @api.onchange('document_adv')
    def _get_advance_docs(self):
        res = {}
        move_lines = self.env['account.move.line'].search([
            ('state', '=', 'valid'),
            ('account_id.type', '=', 'receivable'),
            ('reconcile_id', '=', False),
            # ('reconcile_ref', '=', False),
            ('is_tax_voucher', '=', False),
        ])
        move_line_ids = [ml.id for ml in move_lines
                         if ml.amount_residual or ml.amount_residual_currency]
        account_voucher_lines = self.env['account.voucher.line'].search(
            [('move_line_id', 'in', move_line_ids)]
        )
        account_vouchers = [avl.voucher_id.id for avl in account_voucher_lines
                            if avl.amount > avl.amount_original]
        account_vouchers = set(account_vouchers)
        account_vouchers = list(account_vouchers)
        res['domain'] = {
            'document_adv': [
                '&', ('state', '=', 'posted'),
                ('id', 'in', account_vouchers)
            ]
        }
        """This values are only for interface purpose,
        the saving functionality is defined on create/write
        overrided methods bellow."""
        if self.document_adv:
            self.client = self.document_adv.partner_id
            self.currency = self.get_advance_currency(
                _document=self.document_adv).id
            self.balance = self.get_advance_balance(
                _document=self.document_adv, _currency=self.currency)
        self.bank = False
        self.account = False
        self.checkbook = False
        return res

    @api.onchange('amount')
    def validate_amount(self):
        if self.balance and self.amount:
            if self.amount > self.balance:
                self.amount = self.balance
                raise exceptions.Warning(
                    _('Import must be equal or less than balance'))
            elif self.amount <= 0:
                self.amount = self.balance
                raise exceptions.Warning(_('Import must be greater than 0'))

    @api.model
    def create(self, vals):
        """Override original create method"""
        document_type = vals['document_type']
        if document_type == 'adv':
            docu_adv = self.document_adv.browse(vals['document_adv'])
            currency = self.get_advance_currency(_document=docu_adv)
            vals['client'] = docu_adv.partner_id.id
            vals['currency'] = currency.id
            vals['balance'] = self.get_advance_balance(
                _document=docu_adv, _currency=currency)
        elif document_type == 'cn':
            docu_cn = self.document_cn.browse(vals['document_cn'])
            vals['client'] = docu_cn.partner_id.id
            vals['balance'] = docu_cn.residual
            vals['currency'] = docu_cn.currency_id.id

        vals['folio'] = '/'
        return super(CashBack, self).create(vals)

    @api.multi
    def write(self, vals):
        """Override original write method"""
        try:
            document_type = vals['document_type']
            if document_type == 'adv':
                docu_adv = self.document_adv.browse(vals['document_adv'])
                currency = self.get_advance_currency(_document=docu_adv)
                vals['client'] = docu_adv.partner_id.id
                vals['currency'] = currency.id
                vals['balance'] = self.get_advance_balance(
                    _document=docu_adv, _currency=currency)
            elif document_type == 'cn':
                docu_cn = self.document_cn.browse(vals['document_cn'])
                vals['client'] = docu_cn.partner_id.id
                vals['balance'] = docu_cn.residual
                vals['currency'] = docu_cn.currency_id.id
        except:
            pass
        return super(CashBack, self).write(vals)

    @api.multi
    def unlink(self):
        """Override original unlink (delete) method"""
        for cash_back in self:
            if cash_back.status != 'draft':
                raise exceptions.Warning(
                    _("Records must be on draft status for delete"))
        return super(CashBack, self).unlink()
