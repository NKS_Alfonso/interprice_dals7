# -*- coding: utf-8 -*-
{
    'name': "Cargos a tarjetas",

    'summary': """
        Nuevo Modulo para el cargo a tarjetas""",

    'description': 'Nuevo modulo para dar de alta los pagos con tarjetas de credito y debito a mensualiades',

    'author': 'Copyright © 2018 Charger to cards - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale'],

    # always loaded
    'data': [
        'views/add_bank_cards.xml',
        'views/sale_order_view.xml',
         ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'test':[
    ],
    #'installable': True,
    #'auto_install': False,
    'application': True,
}
