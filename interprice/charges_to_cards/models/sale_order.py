# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
import time
from openerp.osv import osv
from openerp import models, fields, api, exceptions, _

class sale_order_inherit_add_cards(models.Model):
    _inherit = 'sale.order'

    name_bank = fields.Many2one('add.bank.cards',string="Bank terminals", required=False)

    def get_delivery_line(self):
        return self.env['sale.order.line'].search(
            [
                '&',
                ('order_id', '=', self.id),
                ('is_delivery', '=', True)
            ]
        )

    @api.onchange('name_bank')
    def on_change_bank(self):
        product_exists = False
        bank_product_exists = False
        line =[]
        if self.state == 'draft':
            if self.name_bank:
                product_bank = self.name_bank.name_bank.id
                suma = 0.0

                for line in self.order_line:
                    if line.product_id.id == self.name_bank.type_of_service.id:
                        line.unlink()
                        continue
                    suma += line.price_subtotal
                tar_price = suma*(self.name_bank.percentage_charge/100)

                values_add_card = {
                    'order_id': self.id,
                    'state': 'draft',
                    'name': self.name_bank.name_bank.name,
                    'product_uom_qty': 1,
                    'product_uom': self.name_bank.type_of_service.uom_id.id,
                    'product_id': self.name_bank.type_of_service.id,
                    'price_unit':tar_price,
                    'tax_id':[(6, 0, [tax.id for tax in self.name_bank.type_of_service.taxes_id])],
                    'purchase_price':5.00,
                    'is_delivery': False,
                }

                if product_exists:
                    self.write({
                        'order_line': [[1, toupdate, values_add_card]]
                        })
                else:
                    self.update({
                        'order_line': [[0, 0, values_add_card]]
                        })
