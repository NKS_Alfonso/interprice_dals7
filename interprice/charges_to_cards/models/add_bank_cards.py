# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import time
import datetime
import locale
import pytz
from datetime import datetime
from openerp.osv import osv
from openerp import models, fields, api, exceptions, _

class add_bank_cards(models.Model):
    _name = "add.bank.cards"

    _rec_name = 'name_bank'
    # _inherit = 'sale.order'

    name_bank = fields.Many2one('res.bank',string="Bank terminals",required=True)
    months_to_pay = fields.Char(string="Monthly payments", required=True)
    percentage_charge = fields.Float(string="Percentage charge to card", required=True)
    service_key = fields.Char(string="Service key", required=True)
    active = fields.Boolean(string="Active", default=True )
    user_name = fields.Many2one('res.users',default=lambda self:self.env.user and self.env.user.id or False ,string="Name Users", readonly=True)
    discharge_date = fields.Datetime(string='Discharge Date', required=True, readonly=True)
    type_of_service = fields.Many2one('product.product', string="Type of service",required=True)
    user_deactivated = fields.Many2one('res.users', default=lambda self:self.env.user and self.env.user.id or False, string="Low user", readonly=True)
    update_date = fields.Datetime(string="Update Date", required=True,)

    _defaults = {
        'discharge_date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        'update_date': lambda *b: time.strftime('%Y-%m-%d %H:%M:%S'),
        }



    def _currency(self, cursor, user, ids, name, args, context=None):
        res = {}
        res_currency_obj = self.pool.get('res.currency')
        res_users_obj = self.pool.get('res.users')
        default_currency = res_users_obj.browse(cursor, user,
                user, context=context).company_id.currency_idn
        for statement in self.browse(cursor, user, ids, context=context):
            currency = statement.journal_id.currency
            if not currency:
                currency = default_currency
            res[statement.id] = currency.id
        currency_names = {}
        for currency_id, currency_name in res_currency_obj.name_get(cursor,
                user, [x for x in res.values()], context=context):
            currency_names[currency_id] = currency_name
        for statement_id in res.keys():
            currency_id = res[statement_id]
            res[statement_id] = (currency_id, currency_names[currency_id])
        return res


    @api.multi
    def onchange_trade_date_transfert(self, discharge_date):
        if not discharge_date:
            discharge_date = fields.Date.context_today(self)
        datetime_today = datetime.datetime.strptime(discharge_date, tools.DEFAULT_SERVER_DATE_FORMAT)
        value_date = str((datetime_today + relativedelta(days=+2)).strftime(tools.DEFAULT_SERVER_DATE_FORMAT))
        return {'value': {'value_date': value_date}}

# onchage y actualizacion de la fecha y hora del campo update_date
    @api.onchange('active')
    def on_change_active(self):
        my_date = str(datetime.strftime(datetime.now(pytz.utc),"%Y-%m-%d %H:%M:%S%t"))


        if self.active == False:
            self.update_date = my_date

        else:
            self.update_date = my_date
