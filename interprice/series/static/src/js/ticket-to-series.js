(function() {
    var paths = window.location.pathname.split("/");
    var stock_picking_id = paths[paths.length-1];
    var userNameOpenerTable = window.opener.document.getElementById("user-id-" + stock_picking_id).innerHTML;
    var userNameVal;

    if (userNameOpenerTable == "No surtido") {
        userNameVal = window.opener.document.getElementById("user-name").innerHTML;
    } else {
        userNameVal = userNameOpenerTable;
    }
    if (userNameVal){
        document.getElementById("user-name").innerHTML = userNameVal;
    }
})();
