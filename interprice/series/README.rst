Ticket orden de venta
------------------

Ubicación del menu para imprimir el ticket

.. figure:: ../series/static/img/ticket_menu_location.png
    :alt: Menu impresión de ticket
    :width: 100%


Impresión de ticket de orden de venta

.. figure:: ../series/static/img/ticket.png
    :alt: Ticket a imprimir
    :width: 100%