# -*- coding: utf-8 -*-
{
    'name': "Series",

    'summary': """
        """,

    'author': "Copyright © 2016 TO-DO - All Rights Reserved",
    'website': "http://www.grupovadeto.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Purchase / Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'purchase',
        'sale',
        'account',
        'account_cost_center',
        'stock',
        'sale_stock'
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'templates.xml',
        'purchase_order_national_international/res_partner_view.xml',
        'purchase_order_national_international/purchase_order_view.xml',
        'stock/stock_picking_view.xml',
        'stock/stock_transfer_details_view.xml',
        #'account_invoice/account_invoice_view.xml',
        'stock/report_picking.xml',
        'stock/stock_picking_to_invoice.xml'
    ],
    'js': [
        'static/src/js/ticket-to-series.js',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo.xml',
    ],

}
