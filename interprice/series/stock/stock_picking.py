# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import _
from openerp.tools.config import config
from olib.PsqlContector import PsqlConnector
import uuid
import datetime
import cStringIO
import base64
import sys
from openerp.exceptions import except_orm
from types import ListType, DictType
from openerp.http import request

reload(sys)
sys.setdefaultencoding("utf-8")


class StockPicking(osv.osv):

    def get_picking_type(self, cr, uid, ids, context=None):
        """Funcion que nos dice que tipo de albaran es, entrada(compras), salida(ventas), transferencias internas(transferencia)"""
        picking_type = ""
        data = self.browse(cr, uid, ids)
        if (data.picking_type_id.code=='incoming'):
            picking_type = 'entrada'
        elif(data.picking_type_id.code=='outgoing'):
            picking_type = 'salida'
        elif(data.picking_type_id.code=='internal'):
            picking_type = 'trasnsferencia'

        return picking_type

    def _get_origen(self, cr, uid, ids, name, arg, context = {} ):
        try:
            result = {}
            mostrar = False
            estado_actual = self.pool.get('stock.picking').browse( cr, uid, ids[0] ).state

            if( estado_actual == 'borrador'):
              mostrar = False
            elif(estado_actual != 'borrador'):
                for stock in self.browse(cr, uid, ids, context= context) :
                    origen= stock.origin
                    if not origen:
                       mostrar = False
                       raise osv.except_osv( ('Alerta!'), ('El albarán no tiene un origen')+str(origen))

                    else:
                      for id in ids:
                        cr.execute("""select substr(origin,1,2 ) from stock_picking where id=%s""",(id,))
                        compra_venta = cr.fetchone()[0]
                        if( compra_venta == 'PO'):
                           cr.execute("""select nacional_internacional from purchase_order where name=%s""",(origen,))
                           nacional_internacional = cr.fetchone()[0]
                           if (nacional_internacional == 'I'):
                               mostrar = True
                           # raise osv.except_osv( ('Alerta!'), ('!=Xpik')+str(nacional_internacional))
                        else:
                            mostrar = False

            for record in self.browse( cr, uid, ids, context = context):
                result[record.id] = mostrar
            return	result

        except :
            return result

    _inherit = 'stock.picking'

    def crear_poliza_regreso(self, cr, uid, ids, context=None):
      """Crea una poliza con el total del albaran cuando se regresa de prestamos a facturacion"""
      move_line_id = 0
      period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
      for this in self.browse(cr, uid, ids):
       folio_consigna = this.origin
       ref = str(folio_consigna)+"("+(this.name)+")"
       total_picking = this.total_amount
       id_venta = self.pool.get('gv.venta').search(cr, uid,[('nombre', '=', folio_consigna)])[0]

      journal_id= self.pool.get('account.journal').search(cr, uid, [('code','=',"DRC")])[0]
      for obj in self.browse(cr,uid,ids):
          account_move_id = 0
          # res_currency_id = obj.account_currency_id.id
          for u in self.pool.get('res.users').browse(cr,uid,uid):
               company_id = u.company_id.id
      poliza_id = self.pool.get('account.move').create(cr, uid, {'journal_id':journal_id, 'ref':ref,'date':datetime.datetime.now(),'company_id':company_id}, context=context)

      obj_poliza = self.pool.get('account.move').browse( cr, uid, poliza_id, context=context)
      obj_journal = self.pool.get('account.journal').browse( cr, uid, journal_id, context=context)
      # raise osv.except_osv( ('Alerta!'), ('Datos')+str(obj_poliza.period_id.id)+" : "+str(journal_id)+" : "+str(poliza_id))
      move_line_id=self.pool.get('account.move.line').create(cr, uid, {
                                         'partner_id':company_id,
                                         'move_id':poliza_id,
                                         'period_id':period_ids[0],
                                         'date':datetime.datetime.now(),
                                         'name': folio_consigna,
                                         'account_id':obj_journal.default_debit_account_id.id,
                                         'debit':total_picking,
                                         'credit':0,
                                         })
      move_line_id=self.pool.get('account.move.line').create(cr, uid, {
                                         'move_id':poliza_id,
                                         'period_id':period_ids[0],
                                         'date':datetime.datetime.now(),
                                         'name': folio_consigna,
                                         'account_id':obj_journal.default_credit_account_id.id,
                                         'debit':0,
                                         'credit':total_picking,
                                      })
      self.pool.get('account.move').button_validate(cr,uid, [poliza_id], context)
      # for id in ids:
      #   cr.execute("UPDATE _gv_venta_consignacion SET account_move_id=%s WHERE id=%s",(poliza_id,id_venta,))

    def crear_poliza(self, cr, uid, ids, context=None):
      """Crea una poliza con los datos de la venta consignacion"""
      move_line_id = 0
      period_ids = self.pool.get('account.period').search(cr,uid,[('date_start','=',datetime.date.today().strftime('%Y-%m-01')),('special','=',False)])
      for this in self.browse(cr, uid, ids):
       folio_consigna = this.origin
       id_venta = self.pool.get('gv.venta').search(cr, uid,[('nombre', '=', folio_consigna)])[0]

      total_venta = self.pool.get('gv.venta').browse( cr, uid, id_venta ).total_venta
      journal_id= self.pool.get('account.journal').search(cr, uid, [('code','=',"VC")])[0]
      for obj in self.browse(cr,uid,ids):
          account_move_id = 0
          # res_currency_id = obj.account_currency_id.id
          for u in self.pool.get('res.users').browse(cr,uid,uid):
               company_id = u.company_id.id
      poliza_id = self.pool.get('account.move').create(cr, uid, {'journal_id':journal_id, 'ref':folio_consigna,'date':datetime.datetime.now(),'company_id':company_id}, context=context)

      obj_poliza = self.pool.get('account.move').browse( cr, uid, poliza_id, context=context)
      obj_journal = self.pool.get('account.journal').browse( cr, uid, journal_id, context=context)
      # raise osv.except_osv( ('Alerta!'), ('Datos')+str(obj_poliza.period_id.id)+" : "+str(journal_id)+" : "+str(poliza_id))
      move_line_id=self.pool.get('account.move.line').create(cr, uid, {
                                         'partner_id':company_id,
                                         'move_id':poliza_id,
                                         'period_id':period_ids[0],
                                         'date':datetime.datetime.now(),
                                         'name': folio_consigna,
                                         'account_id':obj_journal.default_debit_account_id.id,
                                         'debit':total_venta,
                                         'credit':0,
                                         })
      move_line_id=self.pool.get('account.move.line').create(cr, uid, {
                                         'move_id':poliza_id,
                                         'period_id':period_ids[0],
                                         'date':datetime.datetime.now(),
                                         'name': folio_consigna,
                                         'account_id':obj_journal.default_credit_account_id.id,
                                         'debit':0,
                                         'credit':total_venta,
                                      })
      self.pool.get('account.move').button_validate(cr,uid, [poliza_id], context)
      for id in ids:
        cr.execute("UPDATE _gv_venta_consignacion SET account_move_id=%s WHERE id=%s",(poliza_id,id_venta,))

    def _lote_automatico(self, cr, uid, ids, name, args, context):
        """Genera el lote en automatico"""
        if not ids : return {}
        result = {}
        lote = ""
        try:
            for this in self.browse(cr,uid,ids):
              tipe = self.get_picking_type(cr, uid, ids, context=context)
              if tipe == 'entrada':
                if this.origin and this.state=='assigned':
                    if this.origen_nacional_internacional == True:
                        lote = "I"+str(this.origin[2:])
                        if not this.lot:
                            self.write(cr, uid, ids, {'lot':lote}, context=context)
                    else:
                        lote = "N"+str(this.origin[2:])
                        if not this.lot:
                           self.write(cr, uid, ids, {'lot':lote}, context=context)
            result[this.id] = lote
            return result
        except:
              return {}

    def _rfc_proveedor(self, cr, uid, ids, name, args, context):
      """Verifica si el proveedor tiene un rfc o no"""
      try:
        if not ids : return {}
        result = {}
        rfc = False
        for this in self.browse(cr,uid,ids):
            #Para compras nacioales verifica si el partner tiene rfc
            if (this.origen_nacional_internacional == False):
                  if (this.partner_id.vat == '' or this.partner_id.vat == False):
                      rfc = True
                  else:
                      rfc = False

        result[this.id] = rfc
        return result
      except:return {}

    def _lote_duplicado(self, cr, uid, ids, name, args, context):
        """Verifica si el proveedor tiene un rfc o no"""
        if not ids : return {}
        result = {}
        lote= False
        try:
            for this in self.browse(cr,uid,ids):
                lot = this.lote
                #Para compras nacioales verifica si el folio se duplica o no
                cr.execute("SELECT count(*) FROM stock_production_lot WHERE name=%s",(lot,))
                count = cr.fetchone()[0]
                l = this.lot
                if count > 0 and (l == '' or l == False):
                    lote = True
            result[this.id] = lote
            return result
        except:
            return {}

    def _compra_o_venta(self, cr, uid, ids, name, args, context):
        """Verifica si el albaran proviene de una venta o de una compra"""
        if not ids : return {}
        result = {}
        compra_venta= False
        try:
            for this in self.browse(cr,uid,ids):
                if (this.state !='draft') :
                    if not this.origin:
                        compra_venta= False
                    else:
                        if(this.origin[:2] == 'SO' or this.origin[:2] == 'VC'):
                            compra_venta = True
                result[this.id] = compra_venta
                return result
        except:
             return {}

    def onchange_lote(self, cr, uid, ids, lot, context=None):
        """Comprueba si el lote existe en la base o no"""
        if not lot:
            return {'value': {}}
        else:
            cr.execute("SELECT count(*) FROM stock_production_lot WHERE name=%s",(lot,))
            count = cr.fetchone()[0]
            if count > 0 :
                return {'value': {'lot': 0}, 'warning': {'title': 'Alerta', 'message':'EL LOTE YA EXISTE  FAVOR DE CAMBIARLO MANUALMENTE'}}

    def do_enter_transfer_details(self, cr, uid, picking, context=None):
        '''
        This method is overwrite
        :param cr: Object connection psql
        :param uid: Uid user logged
        :param picking: List of ids
        :param context: Dict, of the context current
        :return:
        '''
        origin = file = csv = None

        stock_picking_objects = self.pool['stock.picking'].browse(cr, uid, picking, context=None)
        if stock_picking_objects and len(stock_picking_objects) > 0:
            for stock_picking_object in stock_picking_objects:
                origin = stock_picking_object.origin
                file = 'code,name,qty,serie\n'
                for move_line in stock_picking_object.move_lines:
                    product_qty = move_line.product_qty
                    for product in move_line.product_id:
                        product_name = product.name
                        product_code = product.code
                        product_track = product.track_all
                        if product_track:
                            # for index in range(0, int(product_qty)):
                            line_product = product_code + ',' + unicode(product_name).encode('utf8') + ',' + str(
                                1) + '\n'
                            file = file + (line_product * int(product_qty))
                        else:
                            file = file + product_code + ',' + unicode(product_name).encode('utf8') + ',' + str(
                                int(product_qty)) + ',S/N,\n'
                    buf = cStringIO.StringIO()
                    buf.write(file)
                    csv = base64.encodestring(buf.getvalue())
                    buf.close()


        if csv and origin:
            if not context:
                context = {}

            context.update({
                'active_model': self._name,
                'active_ids': picking,
                'active_id': len(picking) and picking[0] or False
            })

            created_id = self.pool['stock.transfer_details'].create(cr, uid,
                                                                    {
                                                                        'picking_id': len(picking) and picking[0] or False,
                                                                        'data': csv,
                                                                        'name': origin + '.csv'
                                                                    }, context)
            return self.pool['stock.transfer_details'].wizard_view(cr, uid, created_id, context)
        else:
            return super(StockPicking, self).do_enter_transfer_details(cr, uid, picking, context)

    def action_confirm(self, cr, uid, ids, context=None):
        '''
        This method is overwrite
        :param cr:
        :param uid:
        :param ids:
        :param context:
        :return:
        '''
        for picking in self.browse(cr, uid, ids, context=context):
            type_var_origin = type(picking.origin)
            if type_var_origin.__name__ == 'bool' and picking.origin == False:
                origin = 'TRA/'+picking.name
                picking.write({'origin': origin})
        return super(StockPicking, self).action_confirm(cr, uid, ids, context)

    _columns = {
                'origen_nacional_internacional' : fields.function(_get_origen,'Nacional/Internacional', type="boolean", store=True),
                'compra_o_venta': fields.function(_compra_o_venta, type="boolean"),
                #'lote' : fields.function(_lote_automatico,type="char",),
                #'rfc_proveedor' : fields.function(_rfc_proveedor,type="boolean"),
                #'lote_duplicado' : fields.function(_lote_duplicado,type="boolean"),
                'lot' : fields.char('.', size=254),
                #Este campo sirve para saber si la poliza que se creara sera por el total de la venta o por el total del albaran
                #puede tener dos valores fp(firtspicking) primer albaran que se genera cuando la venta se confirma
                #rg (albaran de regreso) albaran o albaranes que se generen con cada regreso de un producto
                #cada albaran de una venta consignacion tendra uno de estos dos valores.
                'picking_consigna': fields.char('Albaran-consignacion', size=254),
                'pedimento': fields.char(string="Pedimento"),
                'fecha_pedimento': fields.date(string="Fecha Pedimento"),
                'aduana': fields.char(string="Aduana")
              }

    def _get_url_back(self, cr, uid, ids):
        _url_base = self.pool['ir.config_parameter'].get_param(cr, uid, 'web.base.url')
        id = ids[0] if len(ids) > 0 else 0
        _url = _url_base + "/web#id=%s&view_type=form&model=stock.picking" % (id, )
        return _url, _url_base

    def do_load_series(self, cr, uid, ids, context=None):
        _url , _url_base = self._get_url_back(cr, uid, ids)
        _conn = PsqlConnector(
            host=config.get("todo_db_host"),
            database=config.get("todo_db_name"),
            user=config.get("todo_db_user"),
            password=config.get("todo_db_password")
        )
        _id = str(ids)[1:-1]
        _json_str = '{ "db" : {"name": "%s","host": "%s"},' \
                    ' "stock.picking": {"id":"%s"}, ' \
                    '"user": {"id": %s} , "web":{"url_back": "%s", "url_base":"%s"}' \
                    '}' \
                    % (cr.dbname, config.get("db_host"), _id, uid, _url, _url_base)
        _token = str(uuid.uuid4())
        _qry = "INSERT INTO todo_oauth2 (id, create_date, token, params) VALUES (DEFAULT ,now() , '%s', '%s');" % (_token, _json_str)
        if _conn.execute(_qry):
            return {
                'type': 'ir.actions.act_url',
                'url': "%sAplicaciones/ReporteSeries/index.php?token=%s" %(config.get("todo_web"), _token),
                'target': 'self',
                'nodestroy': True,
            }
        else:
            raise osv.except_osv('Carga masiva de series', _("Error al generar el token."))

    def sale_order_user(self):
        self.env.cr.execute(""" SELECT rp.name FROM sale_order so
                                JOIN res_users ru ON so.user_id=ru.id
                                JOIN res_partner rp ON ru.partner_id=rp.id
                                WHERE so.name=%s """,
                            (self.origin,))

        if self.env.cr.rowcount:
            user_name = self.env.cr.fetchall()[0][0]
            return user_name

    def sale_order_line_product(self):
        order_obj = self.pool['sale.order']
        order_id = order_obj.search(self._cr, self._uid, [('name', '=', self.origin)])
        sale_order_id = order_obj.browse(self._cr, self._uid, order_id)
        for prod in self.move_lines:
            order_line = self.env['sale.order.line'].search([('order_id', '=', sale_order_id.id),
                                                             ('product_id', '!=', prod.product_id.id)])
        return order_line

    def get_warehouse_product(self):
        order_obj = self.pool['sale.order']
        order_id = order_obj.search(self._cr, self._uid, [('name', '=', self.origin)])
        if order_id:
            sale_order_id = order_obj.browse(self._cr, self._uid, order_id)
            sale_order_id = sale_order_id.id
        else:
            sale_order_id = None
        self.env.cr.execute(""" SELECT pt.id FROM stock_move sm
                                JOIN product_product pp ON sm.product_id=pp.id
                                JOIN product_template pt ON pp.product_tmpl_id=pt.id
                                WHERE sm.picking_id=%s
                                UNION ALL
                                SELECT pt.id FROM sale_order_line sol
                                JOIN product_product pp on sol.product_id=pp.id
                                JOIN product_template pt on pp.product_tmpl_id=pt.id
                                WHERE sol.order_id=%s and pt.type != 'product' """,
                            (self.id, sale_order_id,))
        if self.env.cr.rowcount:
            prod_tmp = self.env.cr.fetchall()

        warehouse_obj = self.pool['location.warehouse']
        warehouse_id = warehouse_obj.search(self._cr, self._uid,
                                            [('product_template_id', 'in', prod_tmp),
                                             ('warehouse_id', '=', self.picking_type_id.warehouse_id.id)])

        record_obj = self.pool['location.record']
        record_id = record_obj.search(self._cr, self._uid, [('location_warehouse_id', 'in', warehouse_id),
                                                            ('type', '=', 'main')])

        location_record_id = record_obj.browse(self._cr, self._uid, record_id)

        return location_record_id

