# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    # domain method
    def get_stock_ids_to_invoice(self, cr, uid, ids, context=None):
        domain = []
        cr.execute(""" SELECT sp.id
                        FROM stock_picking sp
                        JOIN sale_order so
                        ON so.name=sp.origin
                        JOIN stock_picking_type spt
                        ON spt.id=sp.picking_type_id
                        WHERE sp.state='done'
                        AND sp.invoice_state='2binvoiced'
                        AND spt.code='outgoing' """
                   )
        if cr.rowcount:
            stock_ids = cr.fetchall()

            if stock_ids:
                domain = [('id', 'in', [stock_id[0] for stock_id in stock_ids])]
        else:
            domain = [('id', '=', False)]

        return {
            'type': 'ir.actions.act_window',
            'name': _('Deliveries to invoice'),
            'view_type': 'form',
            'view_mode': 'tree,form,calendar',
            'res_model': 'stock.picking',
            'target': 'current',
            'context': context,
            'domain': domain,
        }

    def get_stock_ids_to_ncc(self, cr, uid, ids, context=None):
        domain = []
        cr.execute(""" SELECT sp.id
                        FROM _gv_devolucion gvd
                        JOIN stock_picking sp ON sp.id=gvd.picking_id
                        JOIN stock_picking_type spt ON spt.id=sp.picking_type_id
                        WHERE spt.code='incoming'
                        AND sp.state='done'
                        AND sp.invoice_state='2binvoiced' """
                   )
        if cr.rowcount:
            stock_ids = cr.fetchall()

            if stock_ids:
                domain = [('id', 'in', [stock_id[0] for stock_id in stock_ids])]
        else:
            domain = [('id', '=', False)]

        return {
            'type': 'ir.actions.act_window',
            'name': _('Deliveries to NCC'),
            'view_type': 'form',
            'view_mode': 'tree,form,calendar',
            'res_model': 'stock.picking',
            'target': 'current',
            'context': context,
            'domain': domain,
        }
