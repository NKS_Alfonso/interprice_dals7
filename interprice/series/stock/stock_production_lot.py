from openerp import models, fields, api, _


class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'
    available = fields.Boolean(_('Available'), default=True)


class StockTransferDetailsItems(models.TransientModel):
    _inherit = 'stock.transfer_details_items'
    lot_id = fields.Many2one('stock.production.lot', 'Lot/Serial Number', domain="[('available', '=', True)]")