# -*- coding: utf-8 -*-

from openerp import models, api, _
from openerp.osv import fields, osv
import cStringIO
import openerp.addons.decimal_precision as dp
import base64
import csv
from olib.oLog import oLog


class StockWarehouse(osv.osv):

    _inherit = 'stock.warehouse'

    def name_get(self, cr, uid, ids, context=None):
        """
        Retorna un arreglo con el texto representativo del registro hacia los objetos de relación many2one
        * Argumentos
        @return bool
        """
        res = []
        for registro in self.read(cr, uid, ids, ['name', 'id']):
            res.append(
                (registro['id'],
                 unicode(registro['name']) +
                 ' (' + unicode(registro['id']) + ') '
                 )
            )
        return res

    _columns = {
        'gvconsigacion': fields.boolean("Activo")
    }


class AccountMove(osv.osv):
    _inherit = "account.move"

    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Factura')
    }


class StockTransferDetails(osv.osv_memory):

    _inherit = 'stock.transfer_details'

    @api.multi
    def group_history_stock(self, trans_type):
        picking_type = self.picking_id.get_picking_type()
        document_origen = self.picking_id.origin
        picking_origin = self.picking_id.name
        picking_id = self.picking_id.id
        dict_write_vals = {}
        for this in self.item_ids:
            if trans_type == 'salida':
                stock_location = this.sourceloc_id.id
                warehouse_id = self.env['stock.warehouse'].search([
                    ('lot_stock_id', '=', this.sourceloc_id.id)]).id
                location_id = this.sourceloc_id.id
            elif trans_type == 'entrada':
                stock_location = this.destinationloc_id.id
                warehouse_id = self.env['stock.warehouse'].search([
                    ('lot_stock_id', '=', this.destinationloc_id.id)]).id
                location_id = this.destinationloc_id.id
            elif not trans_type:
                if picking_type == 'entrada':
                    stock_location = location_id = self.picking_id.picking_type_id.default_location_dest_id.id
                    warehouse_id = self.picking_id.picking_type_id.warehouse_id.id
                elif picking_type == 'salida':
                    stock_location = location_id = self.picking_id.picking_type_id.default_location_src_id.id
                    warehouse_id = self.picking_id.picking_type_id.warehouse_id.id
            self._cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
                          INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
                          WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",
                             (this.product_id.id, stock_location,))
            if self._cr.rowcount:
                for (qty, sm) in self._cr.fetchall():
                    if qty == 0:
                        product_qty = 0
                    elif (qty > 0):
                        product_qty = sm
                    if this.product_id.id in dict_write_vals:
                        dict_write_vals[this.product_id.id].update({
                            'product_id': this.product_id.id,
                            'warehouse_id': warehouse_id,
                            'location_id': location_id,
                            'product_qty_move': ((this.quantity + abs(
                                dict_write_vals[this.product_id.id].get('product_qty_move', 0))) if
                                                 picking_type == 'entrada' or
                                                 trans_type == 'entrada'
                                                 else -(this.quantity + abs(
                                                    dict_write_vals[this.product_id.id].get('product_qty_move', 0)
                                                                            )
                                                        )
                                                 ),
                            'existencia_anterior': product_qty,
                            'existencia_posterior': (
                                (dict_write_vals[this.product_id.id].get('existencia_posterior', 0) + this.quantity) if
                                picking_type == 'entrada' or
                                trans_type == 'entrada'
                                else (dict_write_vals[this.product_id.id].get('existencia_posterior', 0) - this.quantity)
                            ),
                            'document_origin': document_origen,
                            'picking_origin': picking_origin,
                            'picking_id': picking_id,
                        })
                    else:
                        dict_write_vals.update({this.product_id.id: {
                            'product_id': this.product_id.id,
                            'warehouse_id': warehouse_id,
                            'location_id': location_id,
                            'product_qty_move': (this.quantity if
                                                 picking_type == 'entrada' or
                                                 trans_type == 'entrada'
                                                 else -(this.quantity)),
                            'existencia_anterior': product_qty,
                            'existencia_posterior': (product_qty + this.quantity if
                                                     picking_type == 'entrada' or
                                                     trans_type == 'entrada'
                                                     else product_qty - this.quantity),
                            'document_origin': document_origen,
                            'picking_origin': picking_origin,
                            'picking_id': picking_id,
                        }})
        return dict_write_vals

    def history_purchase(self, cr, uid, ids, context=None):
        """ Funcion que recorre la lista de productos de albaranes de entrada(compras) """
        # product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
        obj_history_stock = self.pool.get('history.stock')
        dict_write_vals = self.group_history_stock(cr, uid, ids, trans_type=False, context=context)
        for vals in dict_write_vals:
            obj_history_stock.create(cr, uid, dict_write_vals[vals], context=context)
        #obj_history_stock = self.pool.get('history.stock')
        #data = self.browse(cr, uid, ids)
        #document_origen = data.picking_id.origin
        #warehouse_id = data.picking_id.picking_type_id.warehouse_id.id
        #location_id = data.picking_id.picking_type_id.default_location_dest_id.id
        #picking_origin = data.picking_id.name
        #picking_id = data.picking_id.id
        #for this in data.item_ids:
        #    cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
        #                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
        #                  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",
        #               (this.product_id.id, location_id,))
        #    for (qty, sm) in cr.fetchall():
        #        if qty == 0:
        #            product_qty = 0
        #        elif (qty > 0):
        #            product_qty = sm
        #        write_vals = {
        #            'product_id': this.product_id.id,
        #            'warehouse_id': warehouse_id,
        #            'location_id': location_id,
        #            'product_qty_move': this.quantity,
        #            'existencia_anterior': product_qty,
        #            'existencia_posterior': this.quantity + product_qty,
        #            'document_origin': document_origen,
        #            'picking_origin': picking_origin,
        #            'picking_id': picking_id,
        #        }
        #        obj_history_stock.create(cr, uid, write_vals, context=context)

    def history_sale(self, cr, uid, ids, context=None):
        """Funcion que recorre la lista de productos de albaranes de salida(ventas)"""
        # product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
        obj_history_stock = self.pool.get('history.stock')
        dict_write_vals = self.group_history_stock(cr, uid, ids, trans_type=False, context=context)
        for vals in dict_write_vals:
            obj_history_stock.create(cr, uid, dict_write_vals[vals], context=context)
        #obj_history_stock = self.pool.get('history.stock')
        #data = self.browse(cr, uid, ids)
        #document_origen = data.picking_id.origin
        #warehouse_id = data.picking_id.picking_type_id.warehouse_id.id
        #location_id = data.picking_id.picking_type_id.default_location_src_id.id
        #picking_origin = data.picking_id.name
        #picking_id = data.picking_id.id
        #for this in data.item_ids:
        #    cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
        #                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
        #                  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",
        #               (this.product_id.id, location_id,))
        #    if cr.rowcount:
        #        for (qty, sm) in cr.fetchall():
        #            if qty == 0:
        #                product_qty = 0
        #            elif (qty > 0):
        #                product_qty = sm
        #            write_vals = {
        #                'product_id': this.product_id.id,
        #                'warehouse_id': warehouse_id,
        #                'location_id': location_id,
        #                'product_qty_move': -(this.quantity),
        #                'existencia_anterior': product_qty,
        #                'existencia_posterior': product_qty - this.quantity,
        #                'document_origin': document_origen,
        #                'picking_origin': picking_origin,
        #                'picking_id': picking_id,
        #            }
        #            obj_history_stock.create(cr, uid, write_vals, context=context)

    def history_transfer(self, cr, uid, ids, context=None):
        """Funcion que recorre la lista de productos de albaranes de transferencias(internas)"""
        # product_qty_move = Cantidad del producto que entra o sale (ventas o compras etc.)
        obj_history_stock = self.pool.get('history.stock')
        dict_write_vals = self.group_history_stock(cr, uid, ids, trans_type='salida', context=context)
        for vals in dict_write_vals:
            obj_history_stock.create(cr, uid, dict_write_vals[vals], context=context)

        dict_write_vals = self.group_history_stock(cr, uid, ids, trans_type='entrada', context=context)
        for vals in dict_write_vals:
            obj_history_stock.create(cr, uid, dict_write_vals[vals], context=context)
        #obj_history_stock = self.pool.get('history.stock')
        #data = self.browse(cr, uid, ids)
        #document_origen = data.picking_id.origin
        #picking_origin = data.picking_id.name
        #picking_id = data.picking_id.id
        #for this in data.item_ids:
        #    # historial origen (salida)
        #    cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
        #                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
        #                  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",
        #               (this.product_id.id, this.sourceloc_id.id,))
        #    if cr.rowcount:
        #        for (qty, sm) in cr.fetchall():
        #            if qty == 0:
        #                product_qty = 0
        #            elif (qty > 0):
        #                product_qty = sm
        #            write_vals = {
        #                'product_id': this.product_id.id,
        #                'warehouse_id': self.pool.get('stock.warehouse').search(cr, uid, [
        #                    ('lot_stock_id', '=', this.sourceloc_id.id)])[0],
        #                'location_id': this.sourceloc_id.id,
        #                'product_qty_move': -(this.quantity),
        #                'existencia_anterior': product_qty,
        #                'existencia_posterior': product_qty - this.quantity,
        #                'document_origin': document_origen,
        #                'picking_origin': picking_origin,
        #                'picking_id': picking_id,
        #            }
        #            obj_history_stock.create(cr, uid, write_vals, context=context)
        #    # historial destino (entrada)
        #    cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
        #                  INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
        #                  WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",
        #               (this.product_id.id, this.destinationloc_id.id,))
        #    if cr.rowcount:
        #        for (qty, sm) in cr.fetchall():
        #            if qty == 0:
        #                product_qty = 0
        #            elif (qty > 0):
        #                product_qty = sm
        #            write_vals = {
        #                'product_id': this.product_id.id,
        #                'warehouse_id': self.pool.get('stock.warehouse').search(cr, uid, [
        #                    ('lot_stock_id', '=', this.destinationloc_id.id)])[0],
        #                'location_id': this.destinationloc_id.id,
        #                'product_qty_move': this.quantity,
        #                'existencia_anterior': product_qty,
        #                'existencia_posterior': product_qty + this.quantity,
        #                'document_origin': document_origen,
        #                'picking_origin': picking_origin,
        #                'picking_id': picking_id,
        #            }
        #            obj_history_stock.create(cr, uid, write_vals, context=context)

    def _decode_csv(self, binary):
        msg_error_header = _('Error file CSV!')
        content_file = base64.b64decode(binary)
        lines = content_file.split('\n')
        if len(lines) <= 1:
            raise osv.except_osv(msg_error_header, _(
                "This file not is possible to process the content!\n only has the headers."))
        reader = csv.DictReader(lines)
        if 'code' not in reader.fieldnames \
                and 'name' not in reader.fieldnames \
                and 'qty' not in reader.fieldnames \
                and 'serie' not in reader.fieldnames:
            raise osv.except_osv(msg_error_header, _("The file header is incorrect !\nPlease add at the file the next headers (code, name, qty, serie)."))
        return reader

    # Begin series
    _no_duplicate = {}

    # changa name _validate_products_series  to_group_products
    def to_group_products(self, data_csv):
        msg_error_header = 'Validación de series!'
        self._no_duplicate = {}
        for product in data_csv:
            if type(product['serie']).__name__ == 'NoneType' or len(product['serie']) == 0:
                raise osv.except_osv(msg_error_header, _(
                    "Es necesario capturar la serie del producto (%s)" % product['code']))
            code = product['code'].strip()
            if product['serie'].strip() != 'S/N':
                serie = product['serie'].strip()
                self._validate_no_duplicate_csv(code, serie)
            else:
                self._no_duplicate[code] = {'series': None}
        return self._no_duplicate

    def _validate_no_duplicate_csv(self, code, serie):
        if code not in self._no_duplicate:
            self._no_duplicate[code] = {'series': [serie, ]}
        else:
            for if_duplicate in self._no_duplicate[code]['series']:
                if if_duplicate == serie:
                    self._no_duplicate = {}
                    raise osv.except_osv('Validación de series!', _(
                        "Duplicado de series del mismo producto (%s, %s). " % (code, serie) ))
            self._no_duplicate[code]['series'].append(serie)


    # Obsolete
    def _validate_series_po(self):
        msg_error_header = 'Validation series (DB)!'
        product_code = self._no_duplicate.keys()
        product_ids = self.pool['product.product'].search(self._cr, self._uid, [('default_code', 'in', product_code)], context=None)
        if product_ids and len(product_ids) > 0:
            products = self.pool['product.product'].browse(self._cr, self._uid, product_ids, context=None)
            for product in products:
                default_code = product.code.strip()
                track_all = product.product_tmpl_id.track_all
                product_series = self._no_duplicate[default_code]['series']
                if track_all:
                    if len(product_series) > 0:
                        series_not_availables_ids = self.pool['stock.production.lot'].search(self._cr, self._uid,[
                            ('product_id', '=', product.id),
                            ('name', 'in', product_series)
                        ], context=None)
                        series_not_availables = self.pool['stock.production.lot'].browse(self._cr, self._uid, series_not_availables_ids, context=None)
                        isExists = False
                        msg_series_exists = product.code + ': ' + product.name
                        for serie_not_available in series_not_availables:
                            isExists = True
                            msg_series_exists = msg_series_exists + ' (' + serie_not_available.name + '),'
                            # if serie_not_available.available:
                            #     pass
                            # else:
                            #     pass
                        if isExists:
                            raise osv.except_osv(msg_error_header, _(
                                "This serie already exists, please remove or change! (%s)" % msg_series_exists))
                    else: #
                        raise osv.except_osv(msg_error_header, _(
                            "Is necessary the series of catch! (%s: %s)" % product.code, product.name))
                else:
                    pass
        return True

    def _validate_series_so(self, type_doc):
        '''
        Verify if exists in the database the series of products when it is SO(sales order), or
        that no exists in the database when it is PO(Purchase order)
        :param type_doc:
        :return:
        '''
        msg_error_header = 'Validation series (DB)!'
        product_code = self._no_duplicate.keys()
        product_ids = self.pool['product.product'].search(self._cr, self._uid,
                                                          [('default_code', 'in', product_code)],
                                                          context=None)
        if product_ids and len(product_ids) > 0:
            products = self.pool['product.product'].browse(self._cr, self._uid, product_ids, context=None)
            for product in products:
                default_code = product.code.strip()
                track_all = product.product_tmpl_id.track_all
                product_series = self._no_duplicate[default_code]['series']
                if track_all:
                    if len(product_series) > 0:
                        series_not_availables_ids = self.pool['stock.production.lot'].search(self._cr, self._uid, [
                            ('product_id', '=', product.id),
                            ('name', 'in', product_series)
                        ], context=None)
                        series_not_availables = self.pool['stock.production.lot'].browse(self._cr, self._uid,
                                                                                         series_not_availables_ids,
                                    context=None)
                        product_series_db = [serie_not_available.name for serie_not_available in series_not_availables]
                        series_exists = []
                        if type_doc == 'PO':
                            series_exists = [x for x in product_series if x in product_series_db]
                            msg_series = 'Las series ya existen, eliminar o cambiar.'
                            series_exists_str = str(series_exists)
                        elif type_doc == 'SO':
                            if len(product_series) != len(product_series_db):
                                series_exists = [x for x in product_series if x not in product_series_db]
                                msg_series = 'Las series no existen en la base de datos.'
                                series_exists_str = str(series_exists)
                        if len(series_exists) > 0:
                            product_str = product.code + ': ' + product.name
                            raise osv.except_osv(msg_error_header, _( "(%s) : %s %s" % (product_str, msg_series, series_exists_str )))
                    else:
                        raise osv.except_osv(msg_error_header, _(
                            "Is necessary the series of catch! (%s: %s)" % product.code, product.name))
                else:
                    pass
        return True

    def _validate_series_exists_origin(self, type_doc):
        '''
        Validation of series, if that product exists in the origin movement.
        :param type_doc: string,
        :return: None | Exceptions
        '''
        msg_error_header = "Validacion de series en la base de datos, (%s)!" % type_doc
        products_codes = self._no_duplicate.keys()

        products_ids = self.pool['product.product'].search(
            self._cr,
            self._uid,
            [('default_code', 'in', products_codes)],
            context=None)

        if products_ids and len(products_ids) > 0:
            products = self.pool['product.product'].browse(self._cr, self._uid, products_ids, context=None)
            for product in products:
                default_code = product.code.strip()
                track_all = product.product_tmpl_id.track_all
                product_series = self._no_duplicate[default_code]['series']
                if track_all:
                    # By stock picking
                    source_location = self.picking_source_location_id.id
                    # By product
                    # source_location = [x.sourceloc_id.id for x in self.item_ids if x.sourceloc_id.id and x.product_id.id == product.id]

                    # For throw exception
                    products_series_str = str(product_series)[1:-1]
                    product_str = product.code + ": " + product.name
                    msg_series = "Estas series no existen en %s, \n o no existen en la base de datos." % self.picking_source_location_id.display_name

                    with_out_picking_current = ''
                    if self.picking_id:
                        with_out_picking_current = " and sp.id != %s" % ( self.picking_id.id)
                    query_series_product_exists = """SELECT
                    des_id, origin, name, product_id, product_qty
                      FROM (
                             SELECT
                               spo.location_dest_id des_id,
                               sp.origin,
                               spl.name,
                               spl.product_id,
                               spo.product_qty
                             FROM
                               stock_pack_operation spo
                               INNER JOIN stock_picking sp ON spo.picking_id = sp.id AND sp.state = 'done' """\
                              + with_out_picking_current + """
                               INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id
                             GROUP BY spo.location_dest_id, sp.origin, spl.name, spl.product_id, spo.create_date, spo.product_qty
                             ORDER BY spo.create_date DESC
                           ) T1 WHERE T1.name in (%s) and product_id = %s
                      GROUP BY name, product_id,des_id,origin,product_qty;""" % (products_series_str, product.id)

                    query_series_product_exists = """
                    SELECT
                          T1.*
                        FROM (
                               SELECT
                                 spo.location_dest_id des_id,
                                 sp.origin,
                                 spl.name,
                                 spl.product_id,
                                 spo.product_qty, spo.create_date
                               FROM
                                 stock_pack_operation spo
                                 INNER JOIN stock_picking sp ON spo.picking_id = sp.id AND sp.state = 'done'  """\
                              + with_out_picking_current + """
                                 INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id
                               GROUP BY spo.location_dest_id, sp.origin, spl.name, spl.product_id, spo.create_date, spo.product_qty
                               ORDER BY spo.create_date DESC
                             ) T1 INNER JOIN
                             (
                        SELECT
                          max(spo.create_date) create_date,
                          spl.name,
                          spl.product_id
                        FROM
                          stock_pack_operation spo
                          INNER JOIN stock_picking sp ON spo.picking_id = sp.id AND sp.state = 'done'  and sp.id != 53
                          INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id
                        WHERE spl.name in (%s)  and spl.product_id = %s
                        GROUP BY spl.name, spl.product_id )T2
                        ON T1.create_date = T2.create_date AND T1.name = T2.name AND T1.product_id = T2.product_id
                        ;
                    """  % (products_series_str, product.id)
                    oLog(__name__).info(query_series_product_exists)
                    self._cr.execute(query_series_product_exists)
                    if self._cr.rowcount:
                        data_series_products_exists = self._cr.fetchall()
                        products_series_db_not_exists_source = [x[2] for x in data_series_products_exists if x[0] != source_location]
                        if len(products_series_db_not_exists_source) == 0:
                            if self._cr.rowcount != len(product_series):
                                products_series_db_exists = [x[2] for x in data_series_products_exists]
                                products_series_db_not_exists = [x for x in product_series if x not in products_series_db_exists]
                                if len(products_series_db_not_exists) > 0:
                                    products_series_str = str(products_series_db_not_exists)[1:-1]
                                    raise osv.except_osv(msg_error_header, _("(%s) : %s %s" % (product_str, msg_series, products_series_str)))
                        else:
                            product_series_str = str(products_series_db_not_exists_source)[1:-1]
                            raise osv.except_osv(msg_error_header,_("(%s) : %s %s" % (product_str, msg_series, product_series_str)))
                    else:
                        raise osv.except_osv(msg_error_header, _("(%s) : %s %s" % (product_str, msg_series, products_series_str )))

    def _create_serie(self, serie, product_id):
        vals = {
            'name': serie,
            'product_id': product_id,
            'available': True,
            'ref': serie
        }
        new_serie_id = self.pool['stock.production.lot'].create(self._cr, self._uid, vals, context=self._context)
        # _query = "INSERT INTO stock_production_lot VALUES \
        #           (nextval('stock_production_lot_id_seq'), (now() at time zone 'UTC'), '%s', %s, NULL ,%s, (now() at time zone 'UTC'),NULL , %s, TRUE ) RETURNING id;" % \
        #          (str(serie), self._uid, self._uid, product_id,)
        #
        # if self._psql_connet.execute(_query):
        #     new_serie_id = self._psql_connet.one()
        #     if len(new_serie_id) > 0 and new_serie_id[0]:
        #         return new_serie_id[0]
        return new_serie_id

    @api.multi
    def auto_series(self, type_file):
        # try:
        # oLog(__name__).info('out lotes')
        # _type_file =self.name[:2]# so, po, tr
        for product in self.item_ids:
            if product.product_id.track_all:
                if product.product_id.code in self._no_duplicate:
                    product_series = self._no_duplicate[product.product_id.code]['series']
                    if type_file == 'PO':
                        isFirst = True
                        for product_serie in product_series:
                            _serie = product_serie
                            # _serie = uuid.uuid4()
                            lot_id = self._create_serie(_serie, product.product_id.id)
                            if isFirst:
                                product.lot_id = lot_id
                                isFirst = False
                                continue
                            if product.quantity > 1:
                                product.quantity = (product.quantity - 1)
                                new_id = product.copy(context=self.env.context)
                                new_id.quantity = 1
                                new_id.packop_id = False
                                new_id.lot_id = lot_id
                    elif type_file == 'SO' or type_file == 'TR':
                        if product.quantity > 1:
                            pass
                        else:
                            if len(product_series) > 0:
                                _serie_str = self._no_duplicate[product.product_id.code]['series'].pop(0)
                                _query_serie_str = """SELECT spl.id,spl.name, pp.default_code
                                FROM stock_production_lot spl INNER JOIN product_product pp
                                ON spl.product_id = pp.id
                                WHERE pp.id=%s AND spl.name='%s';
                                """ % (product.product_id.id, _serie_str)
                                self._cr.execute(_query_serie_str)
                                if self._cr.rowcount > 0:
                                    _serie_id = 0
                                    for _cr_fetchone in self._cr.fetchall():
                                        _serie_id = _cr_fetchone[0]
                                    product.lot_id = _serie_id
                                else:
                                    pass

        # except Exception, ex:
        #     raise osv.except_osv('Exception undefined', _( ("%s" % ex ) ))
        # return self[0].wizard_view()

    def _get_series_used_in_picking(self, picking_id=None, SOorPO=None):
        _series_dic = []
        where = ''
        if picking_id:
            where = ' where picking_id = %s' % (picking_id, )
        if SOorPO:
            where = " where origin = '%s' " % (SOorPO, )
        _qry_series_used = """
            SELECT * FROM (
              SELECT
                sp.id picking_id,
                sp.origin,
                spo.location_dest_id,
                sl.complete_name wh_name,
                spl.product_id,
                spl.id serie_id,
                spl.name serie_name
              FROM _gv_devolucion gd
                INNER JOIN account_invoice ai ON gd.invoice_id = ai.id
                INNER JOIN stock_picking sp ON sp.name = ai.origin
                INNER JOIN stock_pack_operation spo ON sp.id = spo.picking_id
                INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id
                INNER JOIN stock_location sl ON spo.location_dest_id = sl.id
              WHERE substring(gd.order_id FROM 1 FOR 2) = 'SO'

              UNION ALL

              SELECT
                sp.id,
                sp.origin,
                spo.location_dest_id,
                sl.complete_name wh_name,
                spl.product_id,
                spl.id,
                spl.name
              FROM _gv_devolucion gd
                INNER JOIN account_invoice ai ON gd.invoice_id = ai.id
                INNER JOIN stock_picking sp ON sp.origin = gd.order_id
                INNER JOIN stock_pack_operation spo ON sp.id = spo.picking_id
                INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id
                INNER JOIN stock_location sl ON spo.location_dest_id = sl.id
              WHERE substring(gd.order_id FROM 1 FOR 2) = 'PO'
            )T1 %s;
        """ % ( where)
        _qry_series_used = """
        SELECT *
            FROM (
                SELECT sp.id picking_id,so.name origin, spo.location_dest_id, sl.complete_name wh_name,
                 spo.product_id, pp.default_code product_code,spl.id serie_id, spl.name serie_name FROM
                 sale_order so INNER JOIN stock_picking sp ON so.name =  sp.origin
                 INNER JOIN stock_pack_operation spo ON sp.id = spo.picking_id
                 INNER JOIN stock_location sl ON spo.location_dest_id = sl.id
                 INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id AND spo.product_id =spl.product_id
                 INNER JOIN product_product pp ON pp.id =spo.product_id
               UNION ALL
               SELECT sp.id picking_id,po.name origin, spo.location_dest_id, sl.complete_name wh_name,
                 spo.product_id, pp.default_code product_code, spl.id serie_id, spl.name serie_name FROM
                 purchase_order po INNER JOIN stock_picking sp ON po.name =  sp.origin
                 INNER JOIN stock_pack_operation spo ON sp.id = spo.picking_id
                 INNER JOIN stock_location sl ON spo.location_dest_id = sl.id
                 INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id AND spo.product_id =spl.product_id
                   INNER JOIN product_product pp ON pp.id =spo.product_id
             ) T1 %s;
        """  % ( where)
        oLog(__name__).info(_qry_series_used)
        self._cr.execute(_qry_series_used)
        if self._cr.rowcount > 0:
            for (picking_id, origin, location_dest_id, wh_name, product_id, product_code, serie_id, serie_name) in self._cr.fetchall():
                # if not self.isLayout:
                _series_dic.append({
                        'product_id': product_id, 'product_code': product_code,
                        'serie_id': serie_id, 'serie_name': serie_name,
                        'wh_des': location_dest_id,'wh_name': wh_name})
            #     else:
            #         pass
            # _series_dic = {str(product_id) if not self.isLayout else product_code: }
        return _series_dic

    def _validation_out_refund(self, picking_id=None):
        _qry_refund = """
            SELECT order_id FROM _gv_devolucion WHERE picking_id=%s;
        """ % (picking_id, )
        # print _qry_refund
        self._cr.execute(_qry_refund)
        if self._cr.rowcount > 0:
            order_id = self._cr.fetchone()[0]
            invoice_series = self._get_series_used_in_picking(SOorPO=order_id)
            if len(invoice_series) == 0:
                return 0
            # print invoice_series
            if not self.isLayout:
                only_series = [( str(pi['product_code'])+'-'+str(pi['serie_id'])+'-'+str(pi['wh_des'])) for pi in invoice_series]
                # print only_series
                for item_id in self.item_ids:
                    serie_id = str(item_id.lot_id.id)
                    warehouse_source_id = str(item_id.sourceloc_id.id)
                    product_code = str(item_id.product_id.code)
                    # print warehouse_source_id
                    if item_id.product_id.track_all:
                        _product = product_code + '-' + serie_id +'-'+warehouse_source_id
                        # print _product
                        if _product not in only_series:
                #             # print 'Success'
                #             invoice_data = invoice_series[str(product_id)]
                #             if invoice_data['serie_id'] != serie_id.id:
                            raise osv.except_osv('Series factura', 'Verifique que series existan en el documento origen,  '
                                                                   'y el almacen de origen seleccionado.')
                #             if int(invoice_data['wh_des']) != warehouse_source_id:
                #                 raise osv.except_osv('Series factura', 'La serie no existe en la ubicación seleccionada.')
            else:
                for product in self._no_duplicate:
                    serie_csv = self._no_duplicate[product]
                    if 'series' in serie_csv and len(serie_csv['series']) > 0:
                        only_series = [(pi['serie_name']) for pi in invoice_series if str(pi['product_code']) == product]
                        for serie in serie_csv['series']:
                            if serie not in only_series:
                                raise osv.except_osv('Series factura',
                                                     'La serie seleccionada no existe en la factura origen.')

    def _no_duplicate_series_wzd(self):
        product_with_series = {}
        for item_id in self.item_ids:
            product_id = item_id.product_id.id
            if item_id.product_id.track_all and not item_id.lot_id:
                raise osv.except_osv('Aviso!', 'Debe asignar un número de serie para el producto %s' % (item_id.product_id.name, ))

            if item_id.product_id.track_all and item_id.quantity > 1:
                raise osv.except_osv('Duplicado de series (WZD)!', _(
                    "Los %s productos (%s: %s) no pueden tener la misma serie." %
                    (str(item_id.quantity), item_id.product_id.code, item_id.product_id.name, )))

            if item_id.product_id.track_all:
                lot_id = item_id.lot_id
                if product_id not in product_with_series:
                    product_with_series[product_id] = {'series_ids': [str(lot_id.id), ]}
                else:
                    product_with_series_tmp = product_with_series[product_id]['series_ids']
                    # for serie_id in product_with_series_tmp:
                    if str(lot_id.id) in product_with_series_tmp:
                        raise osv.except_osv('Duplicado de series (WZD)!', _(
                            "El producto (%s: %s) tiene series duplicadas (%s)!." %
                            (item_id.product_id.code, item_id.product_id.name, lot_id.name)))
                    else:
                        product_with_series[product_id]['series_ids'].append(str(lot_id.id))
            else:
                item_id.lot_id = False

    def _is_not_use_series(self):
        series_id_list = []
        for product in self.item_ids:
            if product.product_id.track_all:
                series_id_list.append(product.lot_id.id)
                
        if len(series_id_list) > 0:
            where_in = str(series_id_list)[1:-1]
            qry_is_not_used_series = """
                SELECT spl.id, spl.name, spo.location_dest_id wh_des_id
                  FROM stock_production_lot spl
                  LEFT JOIN stock_pack_operation spo ON spl.id = spo.lot_id
                WHERE
                  spl.id in (%s)
                ;
             """ % (where_in)
            self._cr.execute(qry_is_not_used_series)
            if self._cr.rowcount >0:
                for (id, name, wh_des_id) in self._cr.fetchall():
                    if wh_des_id != None:
                         raise osv.except_osv('Serire', 'Ya esta en uso.')

    @api.one
    def do_detailed_transfer(self):

        for product in self.item_ids:
            if not product.product_id.track_all:
                product.lot_id = False
        # Who am I?
        _picking_id = self.picking_id.id
        if _picking_id:
            self._cr.execute(
            """
                SELECT
                  substring(sp.origin FROM 1 FOR 2) as type_doc
                FROM stock_picking sp
                WHERE sp.id = %s
            """ % (_picking_id, )
            )
            _type_doc = ''
            if self._cr.rowcount > 0:
                _type_doc = self._cr.fetchone()[0]

            if not self.isLayout:
                self._no_duplicate_series_wzd()
                self._no_duplicate = {}
                for product in self.item_ids:
                    _product = product.product_id
                    if _product.track_all:
                        self._validate_no_duplicate_csv(_product.default_code, str(product.lot_id.name))
                    else:
                        self._no_duplicate[_product.default_code] = {'series': None}
            else:
                _file_csv = self._decode_csv(self.data)
                self.to_group_products(_file_csv)
                _products_qty_group = {}
                for product in self.item_ids:
                    if product.product_id.track_all:
                        _product_code = product.product_id.code
                        if _product_code in _products_qty_group:
                            _product_qty = _products_qty_group[_product_code]['qty']
                            _products_qty_group[_product_code]['qty'] = _product_qty + int(product.quantity)
                        else:
                            _products_qty_group[_product_code] = {'qty': int(product.quantity)}
                # print _products_qty_group
                for product in self.item_ids:
                    _product_code = product.product_id.code
                    if product.product_id.track_all and _product_code in self._no_duplicate and _product_code in _products_qty_group:
                        qty_cvs = len(self._no_duplicate[_product_code]['series'])
                        qty_wiz = _products_qty_group[_product_code]['qty']
                        # print (qty_cvs, qty_wiz)
                        if qty_wiz != qty_cvs:
                            raise osv.except_osv('(Error) Cantidad de productos.',
                                                 'El producto (%s) %s, tiene cantidades diferentes entre el archivo'
                                                 ' CSV(%s) con el Asistente(%s).' %
                                                 (product.product_id.code,
                                                  product.product_id.name,
                                                  qty_cvs,
                                                  qty_wiz,
                                                  )
                                                 )

            if _type_doc == 'DE':
                if not self.isLayout:
                    self._validation_out_refund(_picking_id)
                else:
                    self._validation_out_refund(_picking_id)
                    self._validate_series_so('SO')
                    self._validate_series_exists_origin('Notas de credito!')
            if _type_doc == 'TR':
                # if not self.isLayout:
                #     self._validate_series_so('SO')
                #     self._validate_series_exists_origin('Transfer!')
                # else:
                self._validate_series_so('SO')
                self._validate_series_exists_origin('Transferencias!')
            if _type_doc == 'SO':
                # if not self.isLayout:
                #     self._validate_series_so('SO')
                #     self._validate_series_exists_origin('Sales orders!')
                # else:
                self._validate_series_so('SO')
                self._validate_series_exists_origin('Ordenes de venta!')
            if _type_doc == 'PO':
                if not self.isLayout:
                    self._is_not_use_series()
                else:
                    self._validate_series_so('PO')
            if self.isLayout:
                self.auto_series(_type_doc)
        # raise osv.except_osv('Not', 'Not!!')
        return super(StockTransferDetails, self).do_detailed_transfer()

    # End series

    def _get_unit_cost(self, cr, uid, ids, name, args, context):
        stock_transfer_details = self.pool.get('stock.transfer_details_items')
        if not ids: return {}
        result = {}
        cost_unit = 0.0
        for transfer in self.browse(cr, uid, ids):
            # if transfer.picking_id.computation_cost == 'per_unit':
            for moves in transfer.picking_id.move_lines:
                for items in transfer.item_ids:
                    stock_transfer_details.write(cr, uid, items, {'unit_cost': moves.price_init})
            cost_unit = 0.01
            result[line.id] = cost_unit
        return result

    def _get_quantity_total(self, cr, uid, ids, name, args, context):
        if not ids: return {}
        result = {}
        quantity_total = 0.0
        for line in self.browse(cr, uid, ids):
            for i in line.item_ids:
                quantity_total += i.quantity
            result[line.id] = quantity_total
        return result

    def _amount_total(self, cr, uid, ids, name, args, context):
        if not ids: return {}
        result = {}
        amount_total = 0.0
        for line in self.browse(cr, uid, ids):
            if line.item_ids:
                for i in line.item_ids:
                    amount_total += i.sub_total
            result[line.id] = amount_total
        return result

    def _landed_cost(self, cr, uid, ids, name, args, context):
        if not ids: return {}
        result = {}
        landed_cost = 0.0
        for line in self.browse(cr, uid, ids):
            if line.picking_id:
                landed_cost = line.picking_id.cost_amount
            result[line.id] = landed_cost
        return result

    def _lote_automatico(self, cr, uid, ids, name, args, context):
        """Genera el lote en automatico con los datos del pedimento"""
        if not ids: return {}
        result = {}
        lote = ""
        for this in self.browse(cr, uid, ids):
            lote = this.picking_id.lot
        result[this.id] = lote
        return result

    def _rfc_proveedor(self, cr, uid, ids, name, args, context):
        """Verifica si el proveedor tiene un rfc o no"""
        if not ids: return {}
        result = {}
        rfc = False
        for this in self.browse(cr, uid, ids):
            # Para compras nacioales verifica si el partner tiene rfc
            if (this.picking_id.origen_nacional_internacional == False):
                if (this.picking_id.partner_id.vat == '' or this.picking_id.partner_id.vat == False):
                    rfc = True
                else:
                    rfc = False
        result[this.id] = rfc
        return result

    def _compra_o_venta(self, cr, uid, ids, name, args, context):
        """Verifica si el albaran proviene de una venta o de una compra"""
        if not ids: return {}
        result = {}
        compra_venta = False
        try:
            for this in self.browse(cr, uid, ids):
                if this.picking_id.compra_o_venta == True:
                    compra_venta = True

            result[this.id] = compra_venta
            return result
        except:
            return {}

    _columns = {
        'quantity_total': fields.function(_get_quantity_total, digits_compute=dp.get_precision('Account'),
                                          string='Quantity Total'),
        'total_amount': fields.function(_amount_total, digits_compute=dp.get_precision('Account'),
                                        string='Total Amount'),
        'landed_cost': fields.function(_landed_cost, digits_compute=dp.get_precision('Account'),
                                       string='Landed Cost Product'),
        'data': fields.binary('File', filters='*.csv'),
        'name': fields.char('File Name', readonly=True, store=False),
        'isLayout': fields.boolean(string='Generar layout'),
    }

    def update(self, cr, uid, ids, context=None):
        return self.wizard_view(cr, uid, ids, context=context)

    @api.onchange('isLayout')
    def on_change_is_layout(self):
        if self.isLayout:
            file = 'code,name,qty,serie\n'
            for item_id in self.item_ids:
                product_qty = item_id.quantity
                for product in item_id.product_id:
                    product_name = product.name
                    product_code = product.code
                    product_track = product.track_all
                    if product_track:
                        line_product = product_code + ',' + unicode(product_name).encode('utf8') + ',' + str(
                            1) + '\n'
                        file = file + (line_product * int(product_qty))
                    else:
                        file = file + product_code + ',' + unicode(product_name).encode('utf8') + ',' + str(
                            int(product_qty)) + ',S/N,\n'
            buf = cStringIO.StringIO()
            buf.write(file)
            csv = base64.encodestring(buf.getvalue())
            buf.close()
            self.data = csv
            return {
                'type': 'ir.actions.client',
                'tag': 'reload',
            }


class StockTransferDetailsItems(osv.osv):
    _inherit = 'stock.transfer_details_items'

    @api.multi
    def split_quantities(self):
        for det in self:
            for i in range(1, int(det.quantity)):
                det.quantity = (det.quantity - 1)
                new_id = det.copy(context=self.env.context)
                new_id.quantity = 1
                new_id.packop_id = False

        if self and self[0]:
            return self[0].transfer_id.wizard_view()

        YOpr35t0Y0