# -*- encoding: utf-8 -*-
from openerp import models, fields, api

class ResPartner(models.Model):
  _inherit = 'res.partner'

  internacional = fields.Boolean(string='International')