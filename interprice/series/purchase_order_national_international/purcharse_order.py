# -*- coding: utf-8 -*-
from openerp.osv import fields, osv


class PurchaseOrder(osv.osv):
    _inherit = 'purchase.order'

    _columns = {
        'nacional_internacional' : fields.selection([
                                                    ('N', 'Nacional'),
                                                    ('I', 'Internacional'),
                                                    ],
            'Compra',
            required=True,
            help='Tipo de compra'
        ),
        ############## CAMPOS many2one ######################
        'nacional_partner_id' : fields.many2one('res.partner', 'Proveedor',
                                                domain=[('internacional','=',False),('supplier','=',True)], required=False),
        'internacional_partner_id' : fields.many2one('res.partner', 'Proveedor',
                                                      domain=[('internacional','=',True),('supplier','=',True)], required=False),
        'costs_or_purchase':  fields.boolean('Costo Adicional?',help="""Marcando la casilla significa que la orden de compras sera usada como costo adicional en el modulo de costos adicionales"""),
        'partner_id_referencia' : fields.many2one('res.partner','Referencia(Segundo Provedor)')
      }

    def onchage_nacional_internacional(self, cr, uid, ids, nacional_internacional,order_line, context=None) :
        """
        Onchange que se ejecuta en cuando el campo nacional_internacional es seleccionado,
        Limpia el campo partner_id
        """
        try:
            if not (nacional_internacional) :
              return {'value' : {'partner_id': 0,
                                 'nacional_partner_id': 0,
                                 'internacional_partner_id':0,
                                 'journal_id':0},
                }
            if (nacional_internacional == 'N' and (not order_line or order_line != [(6, 0, [])])):
              code = self.pool.get('account.journal').search(cr,uid, [('code', '=', 'COMNA' )], context=context)[0]
              return {'value' : {'partner_id': 0,
                                 'nacional_partner_id': 0,
                                 'internacional_partner_id':0,
                                 'journal_id':code},
                'warning': {'title': '¡Alerta!', 'message': ''''Si cambia el tipo de compra eventualmente ,
                                                                los cambios existentes de IVA en las líneas del pedido no se actualizarán. '''}}
            elif (nacional_internacional == 'I' and (not order_line or order_line != [(6, 0, [])])):
              code = self.pool.get('account.journal').search(cr,uid, [('code', '=', 'COMIN' )], context=context)[0]
              return {'value' : {'partner_id': 0,
                                 'nacional_partner_id': 0,
                                 'internacional_partner_id':0,
                                 'currency_id' : 3,
                                 'journal_id':code},
                 'warning': {'title': '¡Alerta!', 'message': '''Si cambia el tipo de compra eventualmente ,
                                                                los cambios existentes de IVA en las líneas del pedido no se actualizarán. '''}}
            elif(nacional_internacional == 'N'):
              code = self.pool.get('account.journal').search(cr,uid, [('code', '=', 'COMNA' )], context=context)[0]
              return {'value' : {'partner_id': 0,
                                 'nacional_partner_id': 0,
                                 'internacional_partner_id':0,
                                 'journal_id':code}}
         
            elif (nacional_internacional == 'I'):
              code = self.pool.get('account.journal').search(cr,uid, [('code', '=', 'COMIN' )], context=context)[0]
              return {'value' : {'partner_id': 0,
                                 'nacional_partner_id': 0,
                                 'internacional_partner_id':0,
                                 'currency_id' : 3,
                                 'journal_id':code}}
        except:
           return {'value' : {}}
  
    def onchage_nacional_partner(self, cr, uid, ids, nacional_partner_id) :
        """
        """
        # try:
        if not (nacional_partner_id) :
          return {'value' : {},}
        else:
            return {'value' : {'partner_id': int(nacional_partner_id)}}
            # except:
            #    return {'value' : {}}
            
    def onchage_internacional_partner(self, cr, uid, ids, internacional_partner_id) :
        """
        """
        # try:
        if not (internacional_partner_id) :
          return {'value' : {},}
        else:
            return {'value' : {'partner_id': int(internacional_partner_id)}}
        # except:
        #    return {'value' : {}}   
    
    def onchange_currency_id(self, cr, uid, ids, currency_id,order_line, context=None) :
        # try:
        if not (currency_id) :
          return {'value' : {},}
        else:
             if not order_line or order_line != [(6, 0, [])]: 
                return {'value':{},
                        'warning': {'title': '¡Alerta!', 'message': 'Si cambia la la moneda eventualmente , los cambios existentes de IVA en las líneas del pedido no se actualizarán. '},}
               
        # except:
        #    return {'value' : {}}   



class PurchaseOrderLine(osv.osv):
    _inherit = 'purchase.order.line'

    def _readonly_iva(self, cr, uid, ids, name, args, context):
      """Bloquear el campo de iva cuando la moneda no sea nacional"""
      if not ids : return {}
      result = {}
      currency_id = False
      for this in self.browse(cr,uid,ids):
          if (this.order_id.currency_id):
                if (this.order_id.currency_id.name == "USD"):
                    currency_id = True
                else:
                    currency_id = False
            
      result[this.id] = currency_id
      return result
    _columns = {
                 'nacional_internacional' : fields.selection([
                                                            ('N', 'Nacional'),
                                                            ('I', 'Internacional'),
                                                            ], 'Nacional/Internacional', required=True),
                     
               }
   
    _defaults= {
        'nacional_internacional' : lambda self, cr, uid, context : ( context['nacional_internacional'] ) if ( context and ( 'nacional_internacional' in context ) ) else ( None ),
        }

