# -*- coding: utf-8 -*-
__author__ = 'Jose J. H. Bautista'
__email__ = 'jose.bautista@gmail.com'
__copyright__ = 'Copyright 2016, GVadeto'
__license__ = 'GV'
__version__ = '8.0.2'
import account_invoice