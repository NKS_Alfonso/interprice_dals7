# -*- config: utf-8 -*-
from openerp import _, api, exceptions, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    # pay_method_ids = fields.Many2many('pay.method', string='Metodos de pago')

    def ObtenerPeriodo(self):
        period_id = self.env['account.voucher']._get_period()
        if not period_id:
            raise Warning(
                _("The period not exists. Configuration / Periods ")
            )
        return period_id

    def DatosLinasFactura(self):
        lines_vals = {}

        for line in self.invoice_line:
            if line.product_id.type == 'product':
                if not line.product_id.categ_id.property_stock_journal:
                    raise exceptions.Warning(
                        _("The internal category the product '%s' is not defined 'Stock Journal'." %
                          (line.product_id.name, ))
                    )
                journal_id = line.product_id.categ_id.property_stock_journal.id
                journal_entry_posted = line.product_id.categ_id. \
                                           property_stock_journal.entry_posted
                product_id = line.product_id.id
                if not line.product_id.categ_id.property_stock_valuation_account_id:
                    raise exceptions.Warning(
                        _("The internal category the product '%s' is not defined 'Account Payment'." %
                          (line.product_id.name, ))
                    )
                account_credit = line.product_id.categ_id. \
                                     property_stock_valuation_account_id.id
                if not line.product_id.categ_id.property_stock_account_output_categ:
                    raise exceptions.Warning(
                        _("The internal category the product '%s' is not defined 'Account receive'." %
                          (line.product_id.name,))
                    )
                account_debit = line.product_id.categ_id. \
                                    property_stock_account_output_categ.id
                if journal_id in lines_vals.keys():
                    if product_id in lines_vals[journal_id]:
                        cantidad = lines_vals[journal_id][product_id]['cantidad']
                        precioCoste = lines_vals[journal_id][product_id]['precioCoste']
                        lines_vals[journal_id][product_id]['cantidad'] = cantidad + line.quantity
                        lines_vals[journal_id][product_id]['precioCoste'] = precioCoste + line.product_id.standard_price
                    else:
                        lines_vals[journal_id].update({product_id: {
                            'product': line.name,
                            'account_credit': account_credit,
                            'account_debit': account_debit,
                            'journal_entry_posted': journal_entry_posted,
                            'cantidad': line.quantity,
                            'precioCoste': line.product_id.standard_price}})
                else:
                    lines_vals.update({journal_id: {product_id: {
                        'product': line.name,
                        'account_credit': account_credit,
                        'account_debit': account_debit,
                        'journal_entry_posted': journal_entry_posted,
                        'cantidad': line.quantity,
                        'precioCoste': line.product_id.standard_price}}})
        return lines_vals

    def LineasPoliza(self, journal):
        lines_vals = []
        list_journal = self.DatosLinasFactura()
        for product in list_journal[journal]:
            name = list_journal[journal][product]['product']
            credit = list_journal[journal][product]['precioCoste'] * \
                     list_journal[journal][product]['cantidad']
            debit = list_journal[journal][product]['precioCoste'] * \
                    list_journal[journal][product]['cantidad']
            account_credit = list_journal[journal][product]['account_credit']
            account_debit = list_journal[journal][product]['account_debit']
            cantidad_prod = list_journal[journal][product]['cantidad']

            if self.type == 'out_invoice':
                lines_vals.append({
                    'name': name + ' ' + str(cantidad_prod),
                    'partner_id': self.partner_id.id,
                    'cost_center_id': self.cost_center_id.id,
                    'docto_referencia': self.number,
                    'account_id': account_debit,
                    'debit': debit,
                    'credit': 00,
                    'product_id': product})
                lines_vals.append({
                    'name': name + ' ' + str(cantidad_prod),
                    'partner_id': self.partner_id.id,
                    'cost_center_id': self.cost_center_id.id,
                    'docto_referencia': self.number,
                    'account_id': account_credit,
                    'debit': 00,
                    'credit': credit,
                    'product_id': product})
            if self.type == 'out_refund':
                lines_vals.append({
                    'name': name + ' ' + str(cantidad_prod),
                    'partner_id': self.partner_id.id,
                    'cost_center_id': self.cost_center_id.id,
                    'docto_referencia': self.number,
                    'account_id': account_credit,
                    'debit': debit,
                    'credit': 00,
                    'product_id': product})
                lines_vals.append({
                    'name': name + ' ' + str(cantidad_prod),
                    'partner_id': self.partner_id.id,
                    'cost_center_id': self.cost_center_id.id,
                    'docto_referencia': self.number,
                    'account_id': account_debit,
                    'debit': 00,
                    'credit': credit,
                    'product_id': product})

        return lines_vals

    @api.one
    def invoice_validate(self):
        res = super(AccountInvoice, self).invoice_validate()
        period_id = self.ObtenerPeriodo()

        if self.type in ('out_invoice', 'out_refund'):
            if self.picking_dev_id:
                account_move_obj = self.env['account.move']
                new_ids = []
                list_journal = self.DatosLinasFactura()
                for journal in list_journal:
                    line = self.LineasPoliza(journal)
                    move_vals = {
                        'journal_id': journal,
                        'period_id': period_id,
                        'ref': self.picking_dev_id.name,
                        'date': fields.Datetime.now(),
                        'invoice_id': self.id,
                        'line_id': [(0, 0, x) for x in line],
                    }
                    for product in list_journal[journal]:
                        posted = list_journal[journal][product]['journal_entry_posted']
                    if posted:
                        new_id = account_move_obj.create(move_vals)
                        new_id.post()
                        new_ids.append(new_id.id)
                    else:
                        new_id = account_move_obj.create(move_vals)
                        new_ids.append(new_id.id)

        #add_costcenter_line function add cost center id in the each account.move.lines equals cost center of account.invoice
        move_id = self.move_id
        cost_center_id = self.cost_center_id.id
        result = self.env['account.move.line'].add_costcenter_line(move_ids=move_id, cost_center_id=cost_center_id)

        return res

    @api.one
    def action_cancel(self):
        period_id = self.ObtenerPeriodo()
        date = fields.Date.context_today(self) or fields.Date.today()

        if self.type in ('out_invoice'):
            if self.picking_dev_id:
                self.env.cr.execute(""" SELECT am.id, am.name
                                                from account_invoice ai
                                                inner join stock_picking sp
                                                on ai.picking_dev_id=sp.id
                                                inner join account_move am
                                                on sp.name=am.ref
                                                where ai.id = %s """, (self.id,))
                id_move = self.env.cr.fetchall()
                for id, name in id_move:
                    object_move = self.env['account.move'].browse(id)
                    new_poliza = object_move.copy()
                    move_id = int(new_poliza)

                    for data in object_move.line_id:
                        product = data.product_id
                        for line in product:
                            account_credit = data.product_id.categ_id. \
                                                 property_stock_valuation_account_id.id,
                            account_debit = data.product_id.categ_id. \
                                                property_stock_account_output_categ.id,

                            self._cr.execute(""" UPDATE account_move_line SET
                                                 debit=%s, credit=%s
                                                 WHERE move_id=%s
                                                 AND account_id=%s
                                                 AND product_id=%s """,
                                             (data.credit, data.debit, move_id, account_debit, line.id,))
                            self._cr.execute(""" UPDATE account_move_line SET
                                                 debit=%s,credit=%s
                                                 WHERE move_id=%s
                                                 AND account_id=%s
                                                 AND product_id=%s """,
                                             (data.debit, data.credit, move_id, account_credit, line.id,))
                            cadena = name + str(" Cancelada")
                            self._cr.execute(""" UPDATE account_move SET
                                                 ref=%s, date=%s, period_id=%s
                                                 WHERE id=%s """,
                                             (cadena, date, period_id, move_id,))

        elif self.type in ('out_refund'):
            if self.picking_dev_id:
                self.env.cr.execute(""" SELECT am.id, am.name
                                                from account_invoice ai
                                                inner join stock_picking sp
                                                on ai.picking_dev_id=sp.id
                                                inner join account_move am
                                                on sp.name=am.ref
                                                where ai.id = %s """, (self.id,))
                id_move = self.env.cr.fetchall()
                for id, name in id_move:
                    object_move = self.env['account.move'].browse(id)
                    new_poliza = object_move.copy()
                    move_id = int(new_poliza)

                    for data in object_move.line_id:
                        product = data.product_id
                        for line in product:
                            account_credit = data.product_id.categ_id. \
                                                 property_stock_valuation_account_id.id,
                            account_debit = data.product_id.categ_id. \
                                                property_stock_account_output_categ.id,
                            self._cr.execute(""" UPDATE account_move_line SET
                                                 debit=%s, credit=%s
                                                 WHERE move_id=%s
                                                 AND account_id=%s
                                                 AND product_id=%s """,
                                             (data.credit, data.debit, move_id, account_credit, line.id,))
                            self._cr.execute(""" UPDATE account_move_line SET
                                                 debit=%s, credit=%s
                                                 WHERE move_id=%s
                                                 AND account_id=%s
                                                 AND product_id=%s """,
                                             (data.debit, data.credit, move_id, account_debit, line.id,))
                            cadena = name + str(" Cancelada")
                            self._cr.execute(""" UPDATE account_move SET
                                                 ref=%s, date=%s, period_id=%s
                                                 WHERE id=%s """,
                                             (cadena, date, period_id, move_id,))

        return super(AccountInvoice, self).action_cancel()


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    @api.multi
    def _compute_serie_obj(self):
        pass

    @api.multi
    def _compute_serie(self):
        for row in self:
            product_id = row.product_id.id
            quantity = row.quantity
            origin = row.origin
            if product_id and origin:
                qry = """
                    SELECT sp.id, sp.name, spo.product_id,spl.name, T1.aduana, T1.fecha_pedimento, T1.pedimento
                      FROM stock_picking sp INNER JOIN stock_pack_operation spo ON sp.id = spo.picking_id
                      INNER JOIN stock_production_lot spl ON spo.lot_id = spl.id
                      LEFT JOIN (
                        SELECT spl.id, sp.fecha_pedimento , sp.aduana, sp.pedimento, spo.product_id ,spl.name
                        from stock_picking sp
                          INNER JOIN stock_pack_operation spo ON sp.id = spo.picking_id
                          INNER JOIN stock_production_lot  spl ON spo.lot_id = spl.id
                        WHERE sp.origin LIKE 'PO%%'
                      )T1 ON spl.id = T1.id AND T1.product_id = %s
                    WHERE sp.name=%s
                """  # % (product_id, origin)
                self._cr.execute(qry, [product_id, origin])
                if self._cr.rowcount:
                    series = self._cr.fetchall()
                    series_dic = {}

                    for serie in series:
                        _serie = str(serie[3])
                        _customs = str(serie[4]) if serie[4] else 'S/N'
                        _pedimento_date = str(serie[5]) if serie[5] else ''
                        _pedimento = str(serie[6]) if serie[6] else ''
                        if _customs in series_dic:
                            series_dic[_customs]['series'].append(_serie)
                        else:
                            if _customs == 'S/N':
                                series_dic[_customs] = {'series': [_serie,]}
                            else:
                                series_dic[_customs] = {'aduana': _customs, 'fecha': _pedimento_date,
                                                        'numero': _pedimento, 'series': [_serie, ]}
                    series_str_not = ''
                    series_str_ok = ''

                    for serie in series_dic:
                        if serie == 'S/N':
                            data = series_dic[serie]
                            series_str_not = str(data)[1:-1] + '; '
                        else:
                            data = series_dic[serie]
                            series_str_ok = series_str_ok + str(data)[1:-1] + '; '
                    series_str = series_str_not + series_str_ok
                    row.series = series_str.replace("'", '')
                    row.series_obj = str(series_dic)
                else:
                    row.series = ''

    series = fields.Char(string='Series', store=False, compute='_compute_serie')
    series_obj = fields.Char(string='Series Objeto', store=False, compute='_compute_serie_obj')
