��    P      �  k         �  $   �     �     �     	            	   ,     6     ?     E     U     ^     e     v     �     �     �     �     �  
   �  1   �     0  
   8  6   C  3   z     �     �  4   �     	     	     &	  	   +	     5	     D	     V	     d	     l	  *   y	     �	  9   �	  
   �	     �	  x   
     {
     �
     �
  	   �
     �
     �
     �
  	   �
     �
     �
          "  `   ?  H   �  H   �  F   2  /   y  H   �  8   �     +     2     A     N  
   n     y     �  
   �     �  	   �  #   �     �  B   �  3        O  1   b  :   �  E  �  $        :     G     X     _     f  	   {     �     �     �     �     �     �     �     �     �     �          '  
   @  1   K     }  
   �  6   �  3   �     �       4   &     [     c     s  	   x     �     �     �     �     �  *   �     �  9   
     D     V  x   [     �     �     �  	             !  #   7  	   [     e     u     �     �  `   �  H     H   c  F   �  /   �  H   #  8   l     �     �     �     �  
   �     �       
   
            #   %     I  B   Q  3   �     �  1   �  :        L   $       H   ?       C   "   '   D      (       @          ;   +   5          7       M   O   )          .             3                /           6      :      <       =           E              
             I      B   2          N                    0   ,   !       G         1   &   F       *      -   J   P   4   8          A      	           >   #   K              9                %     Albarán / Transferir (Odoo nativo) (%s) : %s %s Account Entry Activo Aduana Albaran-consignacion Available Cantidad Clave Clave proveedor Cliente: Compra Costo Adicional? Datos de la aduana Deliveries to NCC Deliveries to invoice Dirección almacén: Dirección cliente: Dirección de proveedor: Documento: Duplicado de series del mismo producto (%s, %s).  ENTREGA Ejecutivo: El lote generado ya existe favor de editar manualmente El producto (%s: %s) tiene series duplicadas (%s)!. Error al generar el token. Error file CSV! Es necesario capturar la serie del producto (%s)code Factura Fecha Pedimento File File Name Generar layout Generar plantilla Internacional Invoice Invoice Line Is necessary the series of catch! (%s: %s) Landed Cost Product Los %s productos (%s: %s) no pueden tener la misma serie. Lot/Serial Lote Marcando la casilla significa que la orden de compras sera usada como costo adicional en el modulo de costos adicionales Metodos de pago Nacional Nacional/Internacional Pedimento Picking List Picking wizard Picking wizard items Proveedor Purchase Order Purchase Order Line Quantity Total Referencia(Segundo Provedor) The file header is incorrect !
Please add at the file the next headers (code, name, qty, serie). The internal category the product '%s' is not defined 'Account Payment'. The internal category the product '%s' is not defined 'Account receive'. The internal category the product '%s' is not defined 'Stock Journal'. The period not exists. Configuration / Periods  This file not is possible to process the content!
 only has the headers. This serie already exists, please remove or change! (%s) Ticket Tipo de compra Total Amount Total de artículos a entregar: Transferir Transferir (Odoo) True Ubicación Upload Warehouse series.rg_todo_odoo_transfer_native unknown {'invisible':['|',('partner_id','=',''),('partner_id','=',False)]} {'nacional_internacional' : nacional_internacional} {'no_create':True} {'readonly':[('nacional_internacional','=','I')]} {'readonly':[('state','in',('cancel','done','assigned'))]} Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-29 16:34+0000
PO-Revision-Date: 2017-09-29 11:39-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
  Albarán / Transferir (Odoo nativo) (%s) : %s %s Entrada contable Activo Aduana Albaran-consignacion Available Cantidad Clave Clave proveedor Cliente: Compra Costo Adicional? Datos de la aduana Albaranes para NCC Albaranes para FAC Dirección almacén: Dirección cliente: Dirección de proveedor: Documento: Duplicado de series del mismo producto (%s, %s).  ENTREGA Ejecutivo: El lote generado ya existe favor de editar manualmente El producto (%s: %s) tiene series duplicadas (%s)!. Error al generar el token. Error file CSV! Es necesario capturar la serie del producto (%s)code Factura Fecha Pedimento File File Name Generar layout Generar plantilla Internacional Factura Línea de factura Is necessary the series of catch! (%s: %s) Landed Cost Product Los %s productos (%s: %s) no pueden tener la misma serie. Lote/Nº de serie Lote Marcando la casilla significa que la orden de compras sera usada como costo adicional en el modulo de costos adicionales Metodos de pago Nacional Nacional/Internacional Pedimento Lista de albaranes Asistente de albarán Elementos del asistente de albarán Proveedor Orden de Compra Línea pedido de compra Quantity Total Referencia(Segundo Provedor) The file header is incorrect !
Please add at the file the next headers (code, name, qty, serie). The internal category the product '%s' is not defined 'Account Payment'. The internal category the product '%s' is not defined 'Account receive'. The internal category the product '%s' is not defined 'Stock Journal'. The period not exists. Configuration / Periods  This file not is possible to process the content!
 only has the headers. This serie already exists, please remove or change! (%s) Ticket Tipo de compra Total Amount Total de artículos a entregar: Transferir Transferir (Odoo) True Ubicación Upload Almacén series.rg_todo_odoo_transfer_native unknown {'invisible':['|',('partner_id','=',''),('partner_id','=',False)]} {'nacional_internacional' : nacional_internacional} {'no_create':True} {'readonly':[('nacional_internacional','=','I')]} {'readonly':[('state','in',('cancel','done','assigned'))]} 