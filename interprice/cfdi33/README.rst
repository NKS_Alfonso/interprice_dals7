=======================================================
Facturación electrónica version 3.3
=======================================================

******************************
 Otros
******************************
    * Relacion categoria Odoo - Sat
    .. figure:: ../cfdi33/static/img/rel-cat-sat.png
        :width: 100%

******************************
 Productos
******************************
    * Anticipo del bien o servicio, es necesario para timbrar anticipos
    .. figure:: ../cfdi33/static/img/anticipo-bien-servicio.png
        :width: 100%

    * Aplicacion de anticipo, es necesario para crear  Notas de anticipos
    .. figure:: ../cfdi33/static/img/aplicacion-anticipo.png
        :width: 100%

******************************
 PAC
******************************
    * Firma
    .. figure:: ../cfdi33/static/img/pac-sign.png
        :width: 100%

    * Cancelar
    .. figure:: ../cfdi33/static/img/pac-cancel.png
        :width: 100%

******************************
 Configuración de catálogos
******************************

    * Paises
    .. figure:: ../cfdi33/static/img/country.png
        :width: 100%

    * Monedas
    .. figure:: ../cfdi33/static/img/currency.png
        :width: 100%

    * Tipo de factor y tipo de impuesto, tasa o cuota
    .. figure:: ../cfdi33/static/img/factor_type-and-type_tax.png
        :width: 100%
    .. figure:: ../cfdi33/static/img/rate-and_fee.png
        :width: 100%


    * Método de pago(factura & nota de crédito)
    .. figure:: ../cfdi33/static/img/pay-method.png
        :width: 100%

    .. figure:: ../cfdi33/static/img/pay-method-invoice.png
        :width: 100%


    *  Tipo de pago (clientes, facturas & notas de crédito, ordenes de venta)
    .. figure:: ../cfdi33/static/img/payment_type.png
        :width: 100%
    .. figure:: ../cfdi33/static/img/payment_type-customer.png
        :width: 100%
    .. figure:: ../cfdi33/static/img/payment_type-invoice.png
        :width: 100%
    .. figure:: ../cfdi33/static/img/payment_type-sale_order.png
        :width: 100%

    * Régimen fiscal
    .. figure:: ../cfdi33/static/img/tax_regime.png
        :width: 100%

    * Unidad de medida
    .. figure:: ../cfdi33/static/img/unit_of_measure.png
        :width: 100%

    * Forma de pago (clientes, facturas & notas de crédito)
    .. figure:: ../cfdi33/static/img/way_to_pay.png
        :width: 100%
    .. figure:: ../cfdi33/static/img/way_to_pay-customer.png
        :width: 100%
    .. figure:: ../cfdi33/static/img/way_to_pay-invoice.png
        :width: 100%

    * Código postal
    .. figure:: ../cfdi33/static/img/zip_code.png
        :width: 100%