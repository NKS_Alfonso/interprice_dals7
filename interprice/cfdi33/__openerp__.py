# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation
{
    'name': "CFDI 3.3",

    'summary': """
        Base module for electronic invoicing version 3.3
    """,
    'author': 'Copyright © 2017 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'accounting',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'mail',
        'account',
        'account_voucher',
        'stock_account',
        'configuracion',
        'l10n_mx_regimen_fiscal',
        'l10n_mx_account_tax_category',
        'l10n_mx_account_invoice_tax',
        'l10n_mx_payment_method',
        'l10n_mx_params_pac',
        'l10n_mx_facturae_lib',
        'l10n_mx_facturae_cer',
        'l10n_mx_params_pac',
        'postal_code'
    ],

    # always loaded
    'data': [
        'security/security.xml',
        'views/catalogs/pay_method_view.xml',
        'views/catalogs/relationship_type_view.xml',
        'views/catalogs/sat_product_view.xml',
        'views/catalogs/unit_of_measure_view.xml',
        'views/catalogs/use_cfdi_view.xml',
        'views/res_company_view.xml',
        'views/account_invoice_view.xml',
        'views/product_category_view.xml',
        'views/res_partner_view.xml',
        'views/account_voucher.xml',
        'views/res_bank_inherit_view.xml',
        'views/account_journal.xml',
        'views/related_document_cfdi_view.xml',
        'views/account_journal_view.xml',
        'demos/cfdi_version_data.xml',
        'demos/cfdi_sign_email_template_data.xml',
        'views/cfdi_signed_report_pdf.xml',
        'views/product_template_view.xml',
        'data/terms_conditions_data.xml',
        'data/ir_cron.xml'
    ],

    # only loaded in demonstration mode
    'demo': [
        'demos/pay_method_data.xml',
        'demos/relationship_type_data.xml',
        'demos/country_data.xml',
        'demos/sat_product_data.xml',
        'demos/tax_regime_data.xml',
        'demos/unit_of_measure_data.xml',
        'demos/use_cfdi_data.xml',
        'demos/cfdi_version_data.xml',
    ],
}
