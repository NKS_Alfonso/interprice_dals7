# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class CfdiVersion(models.Model):
    """ Model created for manage versions CFDI """

    # odoo model properties
    _name = 'cfdi.version'
    _description = _('CFDI Version')
    _rec_name = 'version'
    _order = 'id desc'

    # SQL constraints
    _sql_constraints = [
        ('version_unique', 'unique(version)', _("The version must be unique!")),]

    # custom fields
    version = fields.Char(
        required=True,
        string=_("Version"),
        help=_("Number or version of invoicing CFDI."),
        copy=False
    )
    description = fields.Text(
        required=False,
        string=_("Description"),
        help=_("Description of version of invoicing CFDI."),
        copy=False
    )

    # method of properties _rec_name
    @api.multi
    def name_get(self):
        res = []
        for version in self:
            res.append((version.id, 'Ver. ' + version.version))
        return res
