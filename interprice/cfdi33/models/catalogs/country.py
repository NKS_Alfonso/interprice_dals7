# -*- coding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author      To-Do Developers

from openerp import models, fields, _


class Country(models.Model):
    _inherit = 'res.country'

    code = fields.Char(_('Country Code'), size=3, help=_(
        'The ISO 3166-1 country code in two chars or three chars. You can use this field for quick search.'))
