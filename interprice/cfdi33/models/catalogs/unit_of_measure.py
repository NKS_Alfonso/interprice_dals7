# -*- coding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author      To-Do Developers

from openerp import models, fields, _


class UnitOfMeasure(models.Model):
    _inherit = 'product.uom'
    code = fields.Char(string=_('Code'))
    symbol = fields.Char(string=_('Symbol'))

    _sql_constraints = [
        ('uniq_code', 'unique(code)', _("A product unit of measure already exists with this code ")),
    ]