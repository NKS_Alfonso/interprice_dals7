# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class RelationType(models.Model):
    """ Model created for create the catalog relationship type of CFDI"""

    # odoo model properties
    _name = 'relation.type'
    _inherit = ['mail.thread']
    _description = _('Relationship type')
    _rec_name = 'code'
    _order = 'id desc'

    # SQL constraints
    _sql_constraints = [
        ('code_unique', 'unique(code)', _("The code must be unique!")),]

    # custom fields
    code = fields.Char(
        required=True,
        string=_("Key"),
        help=_("key of relation."),
        copy=False
    )
    description = fields.Char(
        required=True,
        string=_("Description"),
        help=_("Description of Relationship"),
        copy=False
    )
    active = fields.Boolean(
        string=_("active"),
        help=_("True, Active the relation"),
        default=True
    )

    # method of properties _rec_name
    @api.multi
    def name_get(self):
        res = []
        for code in self:
            res.append((code.id, code.code + ' - ' + code.description))
        return res
