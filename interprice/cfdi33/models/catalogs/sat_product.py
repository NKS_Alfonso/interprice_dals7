# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class CatalogProductSat(models.Model):
    """ Model created for create the catalog of Uses of CFDI"""

    # odoo model properties
    _name = 'sat.product'
    _inherit = ['mail.thread']
    _description = _('Product SAT')
    _rec_name = 'code'
    _order = 'id desc'

    # SQL constraints
    _sql_constraints = [
        ('code_unique', 'unique(code)', _("The code must be unique!")),]

    # custom fields
    code = fields.Char(
        required=True,
        string=_("Key"),
        help=_("key of Product SAT"),
        copy=False
    )
    description = fields.Char(
        required=True,
        string=_("Description"),
        help=_("Description of Product SAT"),
        copy=False
    )
    active = fields.Boolean(
        string=_("active"),
        help=_("True, Active the Use"),
        default=True
    )

    # method of properties _rec_name
    @api.multi
    def name_get(self):
        res = []
        for code in self:
            res.append((code.id, code.code + ' - ' + code.description))
        return res
