# -*- coding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author      To-Do Developers

from openerp import models, fields, _


class PayMethodSat(models.Model):
    _name = 'pay.method.sat'
    _inherit = ['mail.thread']
    _order = 'id desc'
    _rec_name = 'description'

    code = fields.Char(string=_('Code') , required=True)
    description = fields.Text(string=_('Description'), required=True)

    _sql_constraints = [
        ('uniq_code', 'unique(code)', _("A pay method sat already exists with this code ")),
    ]
