# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#

from openerp import models, fields, api, exceptions, _
import pdb


class RefundAdvancePayment(models.Model):
    _name = 'related.document.cfdi'
    _inherit = 'mail.thread'

    _rec_name = 'number'
    _description = _('Related document cfdi')

    number = fields.Char(_('Name'), readonly=True)
    voucher_id = fields.Many2one(comodel_name='account.voucher', string=_('Voucher'), readonly=True)
    type = fields.Selection(selection=[('customer', _('Customer')), ('supplier', _('Supplier'))],
                            string=_('Type document'), readonly=True)
    document = fields.Selection(selection=[
        ('advance_payment', _('Advance payment')),
        ('refund', _('Refund'))
    ], readonly=True)
    related_document_cfdi_line_ids = fields.One2many(comodel_name='related.document.cfdi.line',
                                                      inverse_name='related_document_cfdi_id')

    advance_payment_id = fields.Many2one(comodel_name='advance.payment', readonly=True)
    account_invoice_id = fields.Many2one(comodel_name='account.invoice', readonly=True)

    _defaults = {
        'type': 'customer',
        'document': 'advance_payment'
    }

    @api.cr_uid_context
    def fields_view_get(
            self, cr, uid, view_id=None, view_type='form',
            context=None, toolbar=False, submenu=False):
        result = super(RefundAdvancePayment, self).fields_view_get(
            cr, uid, view_id, view_type, context, toolbar, submenu
        )
        env = api.Environment(cr, uid, [])
        self.env = env

        account_invoice_obj = self.env['account.invoice']
        state_document = account_invoice_obj.search(
            [('id', '=', context.get('active_id'))]
        ).state_document

        if view_type == 'form' and context.get('type') == 'out_refund' and state_document == 'not_signed':
            from lxml import etree
            root = etree.fromstring(result['fields']['related_document_cfdi_line_ids']['views']['tree']['arch'])
            tree_line_ids_xml = root.xpath('//tree')[0]

            if context.get('active_model') == 'account.invoice':
                # make one2many delete and create attrs true
                tree_line_ids_xml.set('delete', 'true')
                tree_line_ids_xml.set('create', 'true')
                result['fields']['related_document_cfdi_line_ids']['views']['tree']['arch'] = etree.tostring(root)

                # make account_invoice_id readonly = False
                root = etree.fromstring(result['fields']['related_document_cfdi_line_ids']['views']['tree']['arch'])
                field_xml = root.xpath('//field[@name="account_invoice_id"]')[0]
                field_xml.set(
                    'modifiers',
                    '{"readonly": false}'
                )
                field_xml.set(
                    'options',
                    '{"no_create": true}'
                )
                partner_id = account_invoice_obj.search([
                    ('id','=',context.get('active_id'))
                ]).partner_id.id
                currency_id = account_invoice_obj.search([
                    ('id','=',context.get('active_id'))
                ]).journal_id.currency.id
                if currency_id is False:
                    currency_id = self.env.user.company_id.currency_id.id
                field_xml.set(
                    'domain',
                    """[
                        ["partner_id","=",%s],
                        ["currency_id","=",%s],
                        ['type','=','out_invoice'],
                        ['state_document','=','signed'],
                        ['state','=','paid']
                    ]"""%(partner_id, currency_id)
                )
                result['fields']['related_document_cfdi_line_ids']['views']['tree']['arch'] = etree.tostring(root)

                # insert 2 buttons to save and/or discard changes
                root = etree.fromstring(result['arch'])
                form_xml = root.xpath('//form[@string="Reembolso del anticipo del cliente"]')[0]
                save_button = etree.Element(
                    "button",
                    {
                        "name": "save_wizard",
                        "string": "Save",
                        "type": "object",
                        "class": "oe_highlight"
                    }
                )
                discard_button = etree.Element(
                    "button",
                    {
                        "special": "cancel",
                        "string": "Discard",
                        "class": "oe_highlight"
                    }
                )
                form_xml.append(save_button)
                form_xml.append(discard_button)

            result['arch'] = etree.tostring(root)

        return result

    @api.multi
    def save_wizard(self):
        pass

    @api.one
    def write(self, vals):
        if self.env.context.get('type') == 'out_refund':
            if 'related_document_cfdi_line_ids' in vals:
                related_document_cfdi_line_ids = vals.get('related_document_cfdi_line_ids')
                account_invoice_obj = self.env['account.invoice']
                invoices_ids = []
                for line in related_document_cfdi_line_ids:
                    if isinstance(line[2], dict):
                        if line[2].get('account_invoice_id') is False:
                            raise exceptions.Warning(_("A record is empty, please either delete it or select an invoice"))
                        else:
                            invoices_ids.append(line[2].get('account_invoice_id'))
                    else:
                        pass
                duplicates = set([x for x in invoices_ids if invoices_ids.count(x) > 1])
                duplicated_invoices = []
                # invoices_currencies = []
                if duplicates:
                    for id in duplicates:
                        invoice = account_invoice_obj.search([('id','=',id)])
                        duplicated_invoices.append(invoice.number.encode("utf-8"))
                if duplicated_invoices:
                    raise exceptions.Warning(_("The invoice(s) %s are duplicated"%duplicated_invoices))
        return super(RefundAdvancePayment, self).write(vals)

    @api.model
    def create(self,vals):
        if self.env.context.get('type') == 'out_refund':
            if 'related_document_cfdi_line_ids' in vals:
                related_document_cfdi_line_ids = vals.get('related_document_cfdi_line_ids')
                account_invoice_obj = self.env['account.invoice']
                invoices_ids = []
                for line in related_document_cfdi_line_ids:
                    if 'account_invoice_id' not in line[2]:
                        raise exceptions.Warning(_("A record is empty, please either delete it or select an invoice"))
                    invoices_ids.append(line[2].get('account_invoice_id'))
                duplicates = set([x for x in invoices_ids if invoices_ids.count(x) > 1])
                duplicated_invoices = []
                # invoices_currencies = []
                if duplicates:
                    for id in duplicates:
                        invoice = account_invoice_obj.search([('id','=',id)])
                        duplicated_invoices.append(invoice.number.encode("utf-8"))
                if duplicated_invoices:
                    raise exceptions.Warning(_("The invoice(s) %s are duplicated"%duplicated_invoices))
            vals.update({
                'account_invoice_id': self.env.context.get('active_id'),
                'document': 'refund',
                'type': 'customer'
            })
        return super(RefundAdvancePayment, self).create(vals)

class RefundAdvancePaymentLine(models.Model):
    _name = 'related.document.cfdi.line'

    _description = _('Related document cfdi line')

    related_document_cfdi_id = fields.Many2one(comodel_name='related.document.cfdi')

    #number = fields.Char(string=_('Number'), related='account_invoice_id.number')
    invoice_datetime = fields.Datetime(string=_('Date'), related='account_invoice_id.invoice_datetime')
    account_invoice_id = fields.Many2one('account.invoice', string='Account invoice')
    state_list = [
        ('not_signed', _('Not signed')),
        ('signed', _('Signed')),
        ('cancelled', _('Canceled')),
    ]
    state_document = fields.Selection(selection=state_list, string=_('State document'), related='account_invoice_id.state_document')
    # sign = fields.Boolean(_('Sign'))

    @api.multi
    def sign_document(self):
        for record in self:
            uuids = []
            rdc = record.related_document_cfdi_id
            related_type = False
            if rdc.document == 'advance_payment' and rdc.type == 'customer':
                related_type = '07'
                for doc_signed in rdc.advance_payment_id.cfdi_sign_ids:
                    uuids.append({
                        'document_id': rdc.advance_payment_id.id,
                        'document_obj': rdc.advance_payment_id._model,
                        'partner_folio': rdc.advance_payment_id.number,
                        'move': 'relacionado',
                        'uuid': doc_signed.uuid
                    })
            elif rdc.document == 'refund' and rdc.type == 'customer':
                related_type = '02'
                for doc_signed in rdc.account_invoice_id.cfdi_sign_ids:
                    uuids.append({
                        'document_id': rdc.account_invoice_id.id,
                        'document_obj': rdc.account_invoice_id._model,
                        'partner_folio': rdc.account_invoice_id.number,
                        'move': 'relacionado',
                        'uuid': doc_signed.uuid
                    })
            if len(uuids) and related_type:
                if record.state_document not in ['signed', 'cancelled']:
                    record.account_invoice_id.get_firm_document(uuids=uuids, tipo_relacion=related_type)

    @api.onchange('account_invoice_id')
    def onchange_account_invoice_id(self):
        related_document_cfdi_id = self.env.context.get('related_document_cfdi_id')
        related_document_cfdi_obj = self.env['related.document.cfdi'].search(
            [('id', '=', related_document_cfdi_id)]
        )
        account_invoice_obj = self.env['account.invoice']
        journal_currency_id = account_invoice_obj.search([
            ('id', '=', self.env.context.get('active_id'))
        ]).journal_id.currency

        if self.account_invoice_id:
            for line in related_document_cfdi_obj.related_document_cfdi_line_ids:
                if self.account_invoice_id.id == line.account_invoice_id.id:
                    warning = {
                                'title': _("Warning!!!"),
                                'message': _("The invoice (%s) already exists on this record"%self.account_invoice_id.number)
                                }
                    self.account_invoice_id = False
                    return {'warning': warning}

    @api.model
    def create(self, vals):
        res = super(RefundAdvancePaymentLine, self).create(vals)
        mail_message = self.env['mail.thread']
        res.related_document_cfdi_id.message_post(
            body=_('Added invoice %s' % res.account_invoice_id.number),
            subject=_('Added'),
            type='notification',
            content_subtype='plaintext'
        )
        return res

    @api.one
    def unlink(self):
        mail_message = self.env['mail.thread']
        self.related_document_cfdi_id.message_post(
            body=_('Deleted invoice %s' % self.account_invoice_id.number),
            subject=_('Deleted'),
            type='notification',
            content_subtype='plaintext'
        )
        return super(RefundAdvancePaymentLine, self).unlink()
