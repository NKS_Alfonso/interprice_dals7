# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api
from openerp.addons.base_vat import base_vat
from openerp import _
from openerp.exceptions import Warning
import re


class ResBank(models.Model):
    _inherit = 'res.bank'

    rfc = fields.Char(
        string='RFC', help="Add in this field the RFC of the bank, registered before the SAT.")

    @api.multi
    def validate_vat_mx(self):
        res_id = self.env['res.users'].sudo().browse(self._uid).partner_id
        if self.rfc and res_id:
            # validation, RFC calling function
            status_vat = res_id.sudo().check_vat_mx(self.rfc)
            if status_vat:
                raise Warning(_('RFC correct'))
            else:
                raise Warning(_('RFC not valid for SAT'))

class ResPartnerBank(models.Model):
    _inherit = 'res.partner.bank'

    @api.onchange('acc_number')
    def validate_account_number_bank(self):
        if self.acc_number:
            acc_number_without_whitespace = re.sub(r"\s+", "", self.acc_number).encode('utf-8')
            pattern = re.compile(r"^[A-Z0-9_]{10,50}$", re.S)
            if not pattern.match(acc_number_without_whitespace):
                raise Warning(_("The account number does not complete the format required by the SAT"))
            self.acc_number = acc_number_without_whitespace

    @api.model
    def create(self, values):
        if values and values.get('acc_number'):
            acc_number = values.get('acc_number').encode('utf-8')
            pattern = re.compile(r"^[A-Z0-9_]{10,50}$", re.S)
            if not pattern.match(acc_number):
                raise Warning(_("The account number does not complete the format required by the SAT"))
        res_id = super(ResPartnerBank, self).create(values)
        return res_id

    @api.multi
    def write(self, values):
        if values and values.get('acc_number'):
            acc_number = values.get('acc_number').encode('utf-8')
            pattern = re.compile(r"^[A-Z0-9_]{10,50}$", re.S)
            if not pattern.match(acc_number):
                raise Warning(_("The account number does not complete the format required by the SAT"))
        res_id = super(ResPartnerBank, self).write(values)
        return res_id
