# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class AccountJournal(models.Model):
    """ Model extend for add use cfdi field"""

    # odoo properties
    _inherit = 'account.journal'
    # custom fields
    cfdi_version = fields.Many2one(
        comodel_name="cfdi.version",
        required=False,
        string=_("CFDI Version"),
        help=_("Assign to CFDI version."),
        copy=False,
    )
    payment_method = fields.Many2one(
        comodel_name="pay.method",
        required=False,
        string=_("Payment method"),
        help=_("Select payment method"))
