# -*- coding: utf-8 -*-
# Copyright � 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, api, fields, _


class ProductTemplate(models.Model):
    _inherit = 'product.template'


    amazonAsin = fields.Char(
        string=_('ASIN'),
        size=100
    )
