# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Parte(TSat):

    Cantidad = None
    ClaveProdServ = None
    Descripcion = None
    NoIdentificacion = None

    def __init__(self):
        super(Parte, self).__init__()
        self.Cantidad = TSat()
        self.ClaveProdServ = TSat()
        self.Descripcion = TSat()
        self.NoIdentificacion = TSat()
