# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from tsat import TSat


class Addenda(TSat):

    Folios = None
    DatosAdicionales = None
    DomicilioEmisor = None
    DomicilioReceptor = None

    def __init__(self):
        super(Addenda, self).__init__()
        self.Folios = TSat()
        self.DatosAdicionales = TSat()
        self.DomicilioEmisor = TSat()
        self.DomicilioReceptor = TSat()
