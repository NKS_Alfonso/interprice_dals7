# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from comprobante import Comprobante
from emisor import Emisor
from receptor import Receptor
from conceptos import Conceptos
from concepto import Concepto
from pagos import Pagos
from pago import Pago
from complemento import Complemento
from impuestos import Impuestos
from traslado import Traslado
from traslados import Traslados
from addenda import Addenda
from folios import Folios
from datosadicionales import DatosAdicionales
from domicilioemisor import DomicilioEmisor
from domicilioreceptor import DomicilioReceptor
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
import lxml.etree as Letree
from tsat import TSat
import re
import os
import uuid
from subprocess import PIPE, Popen
from openerp.exceptions import MissingError
import base64
from xml.dom import minidom


class XmlCfdi:

    def __init__(self):
        pass

    def node2xml(self, base, xml=None, parent=None, prefix=None):
        if self.isTsat(base):
            prefix_node = ""
            if parent is None:
                if base.prefix:
                    prefix_node = base.prefix
            else:
                if base.prefix:
                    prefix_node = base.prefix
                elif parent.prefix:
                    prefix_node = parent.prefix
                else:
                    prefix_node = prefix

            node = "{}{}".format(prefix_node, base.__class__.__name__)

            if xml is None:
                xml = Element(node)
            else:
                xml = etree.SubElement(xml, node)
            for attribute in base.__class__.__dict__.keys():
                if hasattr(base, attribute):
                    data = getattr(base, attribute, None)
                    if type(data) is TSat:
                        if data.value is not None:
                            prefix_attribute = ""
                            if data.prefix:
                                prefix_attribute = data.prefix
                            attr = "{}{}".format(prefix_attribute, attribute)
                            val = None
                            if type(data.value) is list:
                                if type(self.element1(data.value)) is str:
                                    val = "".join(data.value)
                                elif self.isTsat(self.element1(data.value)):
                                    for element in data.value:
                                        self.node2xml(element, xml=xml, parent=base, prefix=prefix_node)
                            elif type(data.value) in [int, long, float, unicode]:
                                val = str(data.value)
                            elif type(data.value) is str:
                                val = data.value
                            if val:
                                xml.set(attr, val)
                    else:
                        if self.isTsat(data):
                            self.node2xml(data, xml=xml, parent=base, prefix=prefix_node)
            return xml

    def object2xml(self, object_base):
        dom = self.node2xml(object_base, xml=None, parent=None)
        return self.xml_pretty(dom)

    def xml2file(self, xml, filename="xml.xml"):
        tree = etree.ElementTree(xml)
        tree.write(filename, xml_declaration=True, encoding='UTF-8')

    def str2domXml(self, path):
        return etree.parse(path)

    def domXml2str(self, dom):
        return etree.tostring(dom, encoding='UTF-8', method='xml')

    def element1(self, elements):
        for element in elements:
            return element

    def isTsat(self, instance):
        _is_tsat = False
        if re.search(r"<class", str(type(instance))):
            for inherit in instance.__class__.__bases__:
                if inherit is TSat:
                    _is_tsat = True
        return _is_tsat

    def path_tools(self):
        return "{}/../../tools/".format(os.path.dirname(__file__))

    def xml_transform_xslt(self, version, path_xml, _type=None, to_return='content'):
        try:
            path_tools = self.path_tools()
            path_saxon = "{}SaxonEE9-8-0-5J/saxon9ee.jar".format(path_tools)
            version = re.sub(r'\W+', '', version)
            _type_file = 'cadenaoriginal.xslt'
            if _type == 'tfd':
                _type_file = 'cadenaoriginal_tfd.xslt'
            output_txt = "/tmp/{}.txt".format(str(uuid.uuid4()))
            path_xslt = "{}{}/{}".format(path_tools, version, _type_file)
            command = "java -XX:MaxHeapSize=512m -XX:InitialHeapSize=512m -XX:CompressedClassSpaceSize=64m -XX:MaxMetaspaceSize=128m -XX:+UseConcMarkSweepGC -jar {} -xsl:{} -s:{} -o:{}".format(path_saxon, path_xslt, path_xml, output_txt)
            java = Popen(command, stderr=PIPE, stdout=PIPE, shell=True)
            output, err = java.communicate()
            if err:
                print command, output, err
                raise Exception(err)
            if to_return == 'content':
                with open(output_txt, 'r')as txt:
                    cad = txt.read()
                    txt.close()
            else:
                cad = output_txt
        except Exception as e:
            raise MissingError(e.message)
        return cad

    def xml_sign(self, path_original_string, path_pem_key, to_return='content'):
        try:
            output_txt = "/tmp/{}.txt".format(str(uuid.uuid4()))
            command = "openssl dgst -sha256 -sign {} {} | openssl enc -base64 -A -out {}".format(path_pem_key,
                                                                                             path_original_string,
                                                                                             output_txt)

            openssl = Popen(command, stderr=PIPE, stdout=PIPE, shell=True)
            output, err = openssl.communicate()
            if err:
                raise Exception(err)
            if to_return == 'content':
                with open(output_txt, 'r')as txt:
                    cad = txt.read()
                    txt.close()
            else:
                cad = output_txt
        except Exception as e:
            raise MissingError(e.message)
        return cad

    def xml_validate_xsd(self, version, dom_or_xml):
        version = re.sub(r'\W+', '', version)
        ok = False
        try:
            path_tools = self.path_tools()
            path_xsd = "{}{}/cfd.xsd".format(path_tools, version)
            xsd_parse = Letree.parse(path_xsd)
            xsd = Letree.XMLSchema(xsd_parse)
            dom = dom_or_xml
            if not isinstance(dom, Element):
                dom = Letree.parse(dom)
            xsd.assertValid(dom)
            ok = xsd.validate(dom)
        except Letree.DocumentInvalid, e:
            msgs = ''
            for msg in e.error_log.filter_from_errors():
                msgs += str(msg) + '\n'
            raise MissingError(msgs)
        except Exception, e:
            raise MissingError(e.message)
        return ok

    def xml_pretty(self, dom):
        pretty = [
            'cfdi:CfdiRelacionados',
            'cfdi:Emisor',
            'cfdi:Receptor',
            'cfdi:Conceptos',
            'cfdi:Impuestos',
            'cfdi:Complemento',
            'cfdi:Addenda']
        concept_pretty = [
            'cfdi:Impuestos',
            'cfdi:Parte'
        ]
        nodes = {}
        for node in dom:
            if node.tag == 'cfdi:Conceptos':
                sub_nodes = {}
                for sub_node in node:
                    if sub_node.tag == 'cfdi:Concepto':
                        pretty0 = []
                        for node0 in sub_node:
                            pretty0.append(node0)

                        for index0 in pretty0:
                            sub_node.remove(index0)

                        for old_pretty in concept_pretty:
                            for new_node in pretty0:
                                if new_node.tag == old_pretty:
                                    sub_node.append(new_node)
                    sub_nodes.update({sub_node.tag: sub_node})

                for index1 in sub_nodes.keys():
                    node.remove(sub_nodes.get(index1))

                for tag1 in ['cfdi:Concepto']:
                    to_add1 = sub_nodes.get(tag1, False)
                    if to_add1 is not False:
                        node.append(to_add1)
                        del sub_nodes[tag1]
                for other1 in sub_nodes.keys():
                    node.append(sub_nodes.get(other1))
            nodes.update({node.tag: node})

        for index in nodes.keys():
            dom.remove(nodes.get(index))

        for tag in pretty:
            to_add = nodes.get(tag, False)
            if to_add is not False:
                dom.append(to_add)
                del nodes[tag]
        for other in nodes.keys():
            dom.append(nodes.get(other))
        return dom

    def rm_node_xml(self, dom, tag):
        for node in dom:
            if node.tag == tag:
                dom.remove(node)
                return dom, node
        return None, None

    def encode64(self, string):
        return base64.b64encode(string)

    def decode64(self, string):
        return base64.b64decode(string)

    def read_file(self, path):
        cad = None
        with open(path, 'r') as f:
            cad = f.read()
            f.close()
        return cad

    def str2dom(self, _xml):
        return minidom.parseString(_xml)

    def write_file(self, data, path):
        with open(path, 'w+') as f:
            f.write(data)
            f.close()

    """
        Examples
    """

    def example_dom_xml(self):
        """

        :return:
        """

        comprobante = Comprobante()

        comprobante.Certificado.value = 'MIIGDDCCA/SgAwIBAgIUMDAwMDEwMDAwMDA0MDQxMTMyMzIwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTYxMTAxMjMwMDEwWhcNMjAxMTAxMjMwMDEwWjCBrDEaMBgGA1UEAxMRQkxBQ0tQQ1MgU0EgREUgQ1YxGjAYBgNVBCkTEUJMQUNLUENTIFNBIERFIENWMRowGAYDVQQKExFCTEFDS1BDUyBTQSBERSBDVjElMCMGA1UELRMcUU1FMTUwMzIzNFUzIC8gU0FDQTgxMDYyNTVRMjEeMBwGA1UEBRMVIC8gU0FDQTgxMDYyNU1KQ05TTjA5MQ8wDQYDVQQLEwZNYXRyaXowggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCkmbCxqFj41pe7W1N/zs+Bwa7vLHyF/YxN3Ci2xNga0b8OePJ5sOsIVIkvh9+h5/FlrRWAPQm6zv/4cbNP++MZuTtRxBkZNlz3Tc9LuCYQpoQ6eY4Kgi17QdsEdW+yJpyw0MopxNyy5BgkDqCK34D8zTLZjCYLxlz0DvI3k6pj/FByFfWppceoq+5yIO6BPc23DrCmeluUNzntk/i3RwqrBmKqgmR3KypUwQ1UtCcB3UTpBlRMbHqIooVdxWqZOXHjPxhlCG9MNkvZ8S3TB+woXvYPh4eLdK2cSlrSLjq03xlOrJLBPswd71PCdw02YlDsAEKA7yQNdJej99YQnf+5AgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQAKMQQ1iW6RSPdAUtcV5Dc7x+GIFOHwsw8sJkUaIughJXD0Y+KdQYfrXaiTgbVOsbD7yKghAtr9Xb3iLPs9xpYx9c9ykI3ZPOE6UQrE3d65f8HAx6/H5RnMaF/cA5EUhN8u1jTPBRU3DpO357LrDu1oUqMrocAzt2FZcmVXPLzooLCCp4JV2ErDUSs+iBpy1FT9BbXWdzth4w5Ct9KJGBAA3QJOaWovPDBRN5R25silpzlyrwZZz/Z/tHd6xLD+qp/MtzHhLXzZ5lxrnKnv2A9b8v1GU4kpglrwUmzNHK38O1gJJjYBj5PPIp1bfGUjYCNhYjwJdViyc52ljl2nuldbynMYXWP+LcSmKjwEyJdPAt08Vn+saZFzAxXPhBi9vGWVJlMf7WvrHvTfm9+uUh8s8lFw0dU5x0juz0FxnFcPmhsam2ATZpcyWQdPAcp4ysX7nGDTca87+oQwd+AGbW0qYlwTeUYTbWmaC3sQsL5aJpxR/yEZuY2CHPGpC/oT7CtfqyGLqSBcKJSCgZ1izSr6sgvziuEcGEETv4RBM5XrvG4rs+J+DTsVmXxovIeWv+lh8hr/ee7HISII+e3jB4c3By2Fog0QWrG4e8cbFLABMXK80xVf68gO9SnvzPkTrzQeIxnXmtSRQlMHMc5hYo58OdllP+hZlE5YrGGdddDMMw=='
        comprobante.Serie.value = 'A'
        comprobante.Folio.value = 'VP0000018'
        comprobante.Total.value = 10

        emisor = Emisor()
        emisor.Rfc.value = 'SDFSDF VFSSC'
        emisor.Nombre.value = 'Company'
        emisor.RegimenFiscal.value = '034'

        comprobante.Emisor = emisor

        receptor = Receptor()
        receptor.Rfc.value = 'RECRAFC'
        receptor.Nombre.value = 'Customer'
        receptor.ResidenciaFiscal.value = 'DSSD'
        receptor.NumRegIdTrib.value = 'SDFSD'
        receptor.UsoCFDI.value = '04'

        comprobante.Receptor = receptor

        concepto1 = Concepto()
        concepto1.Descripcion.value = 'c3'
        concepto1.Cantidad.value = 23

        concepto2 = Concepto()
        concepto2.Importe.value = 32323
        concepto2.Cantidad.value = 21
        concepto2.Impuestos = Impuestos()

        traslado1 = Traslado()
        traslado1.Base.value = 23423
        traslado1.TasaOCuota.value = "Tasa"

        concepto2.Impuestos.Traslados = Traslados()
        concepto2.Impuestos.Traslados.Traslado.value = [traslado1, traslado1]

        conceptos = Conceptos()
        conceptos.Concepto.value = [concepto1, concepto2]

        comprobante.Conceptos = conceptos

        comprobante.Impuestos = Impuestos()
        comprobante.Impuestos.Traslados = Traslados()
        # comprobante.Impuestos.Traslados.Traslado.value = [traslado1, traslado1, traslado1]

        addenda = Addenda()

        folios = Folios()
        folios.folioERP.value = "Frgd"
        folios.codigoCliente.value = "2323"

        addenda.Folios = folios

        datos_adicionales = DatosAdicionales()
        datos_adicionales.ordenCompra.value = "PO00031"
        addenda.DatosAdicionales = datos_adicionales

        domicilio_emisor = DomicilioEmisor()
        domicilio_emisor.codigoPostal.value = "34067"
        addenda.DomicilioEmisor = domicilio_emisor

        domicilio_receptor = DomicilioReceptor()
        domicilio_receptor.estado.value = "Jalisco"
        addenda.DomicilioReceptor = domicilio_receptor

        pago = Pago()
        pago.FechaPago.value = "2017-07-09T12:00:00"
        pago.FormaDePagoP.value = "03"
        pago.MonedaP.value = "USD"
        pago.Monto.value = "1891.80"
        pago.TipoCambioP.value = "18.03630"
        pago.RfcEmisorCtaOrd = "XEXX010101000"

        pagos = Pagos()
        pagos.Version.value = "1.0"
        pagos.Pago.value = [pago]
        complemento = Complemento()
        complemento.Pagos.value = [pagos]
        comprobante.Complemento = complemento
        comprobante.Addenda = addenda
        return self.object2xml(comprobante)

    def example_dom_xml_to_file(self):
        xml_obj = self.example_dom_xml()

    def example_dom_xml_transform_xslt(self):
        pass





