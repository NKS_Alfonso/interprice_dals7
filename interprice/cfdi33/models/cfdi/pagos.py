# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Pagos(TSat):
    Pago = None
    Version = None
    pago10 = None
    xmlns = None
    schemaLocation = None

    def __init__(self):
        super(Pagos, self).__init__(prefix='pago10:')
        self.Version = TSat()
        self.Pago = TSat()
        self.pago10 = TSat(value="http://www.sat.gob.mx/Pagos", prefix='xmlns:')
        self.schemaLocation = TSat(
            value=[
            	"http://www.sat.gob.mx/Pagos",
            	" http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd"
            ],
            prefix='xsi:')