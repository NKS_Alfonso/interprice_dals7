# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from tsat import TSat


class DomicilioReceptor(TSat):

    pais = None
    noInterior = None
    noExterior = None
    municipio = None
    localidad = None
    estado = None
    colonia = None
    codigoPostal = None
    calle = None

    def __init__(self):
        super(DomicilioReceptor, self).__init__()
        self.pais = TSat()
        self.noInterior = TSat()
        self.noExterior = TSat()
        self.municipio = TSat()
        self.localidad = TSat()
        self.estado = TSat()
        self.colonia = TSat()
        self.codigoPostal = TSat()
        self.calle = TSat()
