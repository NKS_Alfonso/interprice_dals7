# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Traslado(TSat):

    Base = None
    Importe = None
    Impuesto = None
    TasaOCuota = None
    TipoFactor = None

    def __init__(self):
        super(Traslado, self).__init__()
        self.Base = TSat()
        self.Importe = TSat()
        self.Impuesto = TSat()
        self.TasaOCuota = TSat()
        self.TipoFactor = TSat()
