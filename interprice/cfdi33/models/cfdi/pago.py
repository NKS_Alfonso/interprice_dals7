# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Pago(TSat):

    FechaPago = None
    FormaDePagoP = None
    MonedaP = None
    TipoCambioP = None
    Monto = None
    NumOperacion = None
    RfcEmisorCtaOrd = None
    NomBancoOrdExt = None
    CtaOrdenante = None
    RfcEmisorCtaBen = None
    CtaBeneficiario = None
    TipoCadPago = None
    CertPago = None
    CadPago = None
    SelloPago = None
    DoctoRelacionado = None

    def __init__(self):
        super(Pago, self).__init__()
        self.FechaPago = TSat()
        self.FormaDePagoP = TSat()
        self.MonedaP = TSat()
        self.TipoCambioP = TSat()
        self.Monto = TSat()
        self.NumOperacion = TSat()
        self.RfcEmisorCtaOrd = TSat()
        self.NomBancoOrdExt = TSat()
        self.CtaOrdenante = TSat()
        self.RfcEmisorCtaBen = TSat()
        self.CtaBeneficiario = TSat()
        self.TipoCadPago = TSat()
        self.CertPago = TSat()
        self.CadPago = TSat()
        self.SelloPago = TSat()
        self.DoctoRelacionado = TSat()
