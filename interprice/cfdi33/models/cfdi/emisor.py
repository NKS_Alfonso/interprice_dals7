# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Emisor(TSat):

    Rfc = None
    Nombre = None
    RegimenFiscal = None

    def __init__(self):
        super(Emisor, self).__init__()
        self.Rfc = TSat()
        self.Nombre = TSat(
            regex='[^|]{1,254}'
        )
        self.RegimenFiscal = TSat()
