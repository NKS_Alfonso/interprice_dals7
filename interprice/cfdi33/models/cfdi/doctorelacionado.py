# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class DoctoRelacionado(TSat):
    IdDocumento = None
    Serie = None
    Folio = None
    MonedaDR = None
    TipoCambioDR = None
    MetodoDePagoDR = None
    NumParcialidad = None
    ImpSaldoAnt = None
    ImpPagado = None
    ImpSaldoInsoluto = None

    def __init__(self):
        super(DoctoRelacionado, self).__init__()
        self.IdDocumento = TSat()
        self.Serie = TSat()
        self.Folio = TSat()
        self.MonedaDR = TSat()
        self.TipoCambioDR = TSat()
        self.MetodoDePagoDR = TSat()
        self.NumParcialidad = TSat()
        self.ImpSaldoAnt = TSat()
        self.ImpPagado = TSat()
        self.ImpSaldoInsoluto = TSat()
