# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Receptor(TSat):

    Rfc = None
    Nombre = None
    ResidenciaFiscal = None
    NumRegIdTrib = None
    UsoCFDI = None

    def __init__(self):
        super(Receptor, self).__init__()
        self.Rfc = TSat()
        self.Nombre = TSat(
            regex='[^|]{1,254}'
        )
        self.ResidenciaFiscal = TSat()
        self.NumRegIdTrib = TSat()
        self.UsoCFDI = TSat()
