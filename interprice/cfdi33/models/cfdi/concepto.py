# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class Concepto(TSat):

    Cantidad = None
    ClaveProdServ = None
    ClaveUnidad = None
    NoIdentificacion = None
    Unidad = None
    ValorUnitario = None
    Descripcion = None
    Importe = None

    Impuestos = None
    Parte = None

    def __init__(self):
        super(Concepto, self).__init__()
        self.Descripcion = TSat()
        self.Importe = TSat()
        self.Cantidad = TSat()
        self.ClaveProdServ = TSat()
        self.ClaveUnidad = TSat()
        self.NoIdentificacion = TSat()
        self.Unidad = TSat()
        self.ValorUnitario = TSat()
        self.Impuestos = TSat()
        self.Parte = TSat()
