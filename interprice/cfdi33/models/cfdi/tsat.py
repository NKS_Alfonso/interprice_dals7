# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
import re


class TSat (object):
    _node = None
    _regex = None
    _value = None
    _prefix = None

    def __init__(self, value=None, regex=None, node=None, doc=None, prefix=None):
        self._node = node
        self._regex = regex
        self._value = None
        self._prefix = prefix
        self.__doc__ = doc
        if value is not None:
            self.value = value

    @property
    def value(self):
        return self._value

    @value.getter
    def value(self):
        return self._value

    @value.setter
    def value(self, val):
        if val is not None and type(val) is str:
            if self._regex is not None:
                if re.search(r"{}".format(self._regex), val):
                    self._value = val
                else:
                    self._value = None
                    msg = "The regEx ( {} )->( {} ) pattern does not match :( ".format(self._regex, val)
                    raise msg
            else:
                self._value = val
        else:
            self._value = val

    @property
    def regex(self):
        return self._regex

    @property
    def node(self):
        return self._node

    @property
    def prefix(self):
        return self._prefix

    def printf(self):
        for attr in self.__class__.__dict__.keys():
            if hasattr(self, attr):
                obj = getattr(self, attr, None)
                if type(obj) is TSat:
                    print attr, ':', obj.value