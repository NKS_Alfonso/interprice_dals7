# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from tsat import TSat


class Comprobante(TSat):

    Certificado = None
    CondicionesDePago = None
    Fecha = None
    Folio = None
    FormaPago = None
    LugarExpedicion = None
    MetodoPago = None
    Moneda = None
    NoCertificado = None
    Sello = None
    Serie = None
    SubTotal = None
    Descuento = None
    TipoCambio = None
    TipoDeComprobante = None
    Total = None
    Version = None
    xmlns = None
    cfdi = None
    pago10 = None
    tfd = None
    xsi = None
    schemaLocation = None
    Emisor = None
    Receptor = None
    Conceptos = None
    Impuestos = None
    CfdiRelacionados = None
    Addenda = None
    Pagos = None
    Complemento = None
    def __init__(self):
        super(Comprobante, self).__init__(prefix='cfdi:')
        self.Certificado = TSat(
            regex='^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$',
            doc='RegEx from the RFC 4568'
        )
        self.CondicionesDePago = TSat()
        self.Fecha = TSat(doc="Format AAAA-MM-DDThh:mm:ss")
        self.Folio = TSat(regex='[^|]{1,40}')
        self.FormaPago = TSat()
        self.LugarExpedicion = TSat()
        self.MetodoPago = TSat()
        self.Moneda = TSat(doc="Format ISO 4217")
        self.NoCertificado = TSat(regex='[0-9]{20}')
        self.Sello = TSat()
        self.Serie = TSat(regex='[^|]{1,25}')
        self.SubTotal = TSat()
        self.Descuento = TSat()
        self.TipoCambio = TSat()
        self.TipoDeComprobante = TSat()
        self.Total = TSat()
        self.Version = TSat()
        #self.xmlns = TSat(value="http://www.sat.gob.mx/cfd/3")
        self.cfdi = TSat(value="http://www.sat.gob.mx/cfd/3", prefix='xmlns:')
        # self.pago10 = TSat(value="http://www.sat.gob.mx/Pagos", prefix=self.cfdi.prefix)
        # self.tfd = TSat(value="http://www.sat.gob.mx/TimbreFiscalDigital", prefix=self.cfdi.prefix)
        self.xsi = TSat(value="http://www.w3.org/2001/XMLSchema-instance", prefix=self.cfdi.prefix)
        
        self.schemaLocation = TSat(
            value=[
                "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd",
                # " http://www.sat.gob.mx/TimbreFiscalDigital",
                # " http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd",
                # "http://www.sat.gob.mx/TimbreFiscalDigital",
                # "http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd",
                " http://www.sat.gob.mx/Pagos",
                " http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd",
            ],
            prefix='xsi:'
        )
        self.Impuestos = TSat()
        self.CfdiRelacionados = TSat()
        self.Emisor = TSat()
        self.Receptor = TSat()
        self.Conceptos = TSat()
        self.Addenda = TSat()
        self.Pagos = TSat()
        self.Complemento = TSat()
