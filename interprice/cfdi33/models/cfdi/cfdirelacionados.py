# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from tsat import TSat


class CfdiRelacionados(TSat):

    TipoRelacion = None
    CfdiRelacionado = None

    def __init__(self):
        super(CfdiRelacionados, self).__init__()
        self.TipoRelacion = TSat()
        self.CfdiRelacionado = TSat()
