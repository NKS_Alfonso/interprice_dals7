# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, _


class ResPartner(models.Model):
    """ Model extend for add use cfdi field """

    # odoo model properties
    _inherit = 'res.partner'

    # custom fields
    use_cfdi = fields.Many2one(
        comodel_name="use.cfdi",
        required=True,
        string=_("Use CFDI"),
        help=_("Use that the customer will give to their CFDI invoices."),
        copy=False,
    )
    amazon_customer = fields.Boolean(
        string=_('Amazon customer')
    )
