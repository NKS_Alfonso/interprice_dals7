# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _
from openerp.exceptions import MissingError, Warning


class AccountInvoiceLine(models.Model):
    """ Model extend for add use cfdi field"""

    # Odoo properties
    _inherit = 'account.invoice.line'

    tax_amount = fields.Float(
        string="Tax amount",
        compute="compute_tax_amount"
    )

    @api.one
    @api.depends('price_subtotal')
    def compute_tax_amount(self):
        if self.product_id:
            if self.invoice_line_tax_id:
                iva = self.invoice_line_tax_id[0].amount
                tax_amount = round(self.price_subtotal * iva, 2)
                self.update({'tax_amount': tax_amount})

    @api.model
    def create(self, values):
        if not values.get("account_id"):
            raise Warning(
                _('The countable account not is Found'),
                _('The countable account is missing in a product and categories')
            )
        return super(AccountInvoiceLine, self).create(values)
