# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import models, fields


class ParamsPac33(models.Model):
    _name = 'params.pac33'

    name = fields.Char(
        'Name', size=128, required=True, help='Name for this param')
    url_webservice_sign = fields.Char(
        'URL WebService sign', size=256,
        help='URL of WebService used for send to sign the XML to PAC')
    namespace = fields.Char(
        'NameSpace', size=256,
        help='NameSpace of XML of the page of WebService of the PAC')
    user = fields.Char('User', size=128, help='Name user for login to PAC')
    password = fields.Char(
        'Password', size=128,
        help='Password user for login to PAC')
    enviroment = fields.Selection(
        selection=[('test', 'Test'), ('prod', 'Production')],
        string="Type of environment",
        required=True)
    company_id = fields.Many2one(
        'res.company', 'Company', required=True,
        default=lambda self: self.env['res.company']._company_default_get(
            'param.pac33'),
        help='Company where will configurate this param')
    active = fields.Boolean(
        'Active', default=1, help='Indicate if this param is active')
    url_webservice_cancel = fields.Char(
        'URL WebService cancel',
        help='Endpoint cancel in test or production')
    sequence = fields.Integer(
        'Sequence', default=10,
        help='If have more of a param, take the param with less sequence')
    certificate_link = fields.Char(
        'Certificate link', size=256,
        help='PAC have a public certificate that is necessary by customers to\
        check the validity of the XML and PDF')
