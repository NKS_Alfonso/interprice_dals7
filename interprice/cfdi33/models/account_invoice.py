# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _
from openerp.tools import float_round

from cfdi.comprobante import Comprobante
from cfdi.emisor import Emisor
from cfdi.receptor import Receptor

from cfdi.addenda import Addenda
from cfdi.folios import Folios
from cfdi.datosadicionales import DatosAdicionales
from cfdi.domicilioemisor import DomicilioEmisor
from cfdi.domicilioreceptor import DomicilioReceptor

from cfdi.impuestos import Impuestos
from cfdi.traslado import Traslado
from cfdi.traslados import Traslados
from cfdi.conceptos import Conceptos
from cfdi.concepto import Concepto
from cfdi.parte import Parte
from cfdi.cfdirelacionados import CfdiRelacionados
from cfdi.cfdirelacionado import CfdiRelacionado

from cfdi.xml_cfdi import XmlCfdi

from openerp.tools.misc import DATETIME_FORMATS_MAP
from datetime import datetime
import json
import httplib
import ssl
import lxml.etree as lxml
from openerp.exceptions import MissingError, Warning
import logging
_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    """ Model extend for add use cfdi field"""

    # Odoo properties
    _inherit = 'account.invoice'

    # Initial method
    def _compute_hide(self):
        if self.cfdi_sign_ids:
            self.hide_button_firm = True

    # list selection
    state_list = [
        ('not_signed', _('Not signed')),
        ('signed', _('Signed')),
        ('pending', _('Pending')),
        ('cancelled', _('Canceled')),
    ]

    # custom fields
    use_cfdi = fields.Many2one(
        comodel_name="use.cfdi",
        required=False,
        # readonly=True,
        string=_("Use CFDI"),
        help=_("Use that the customer will give to their CFDI invoices."),
        states={'draft': [('readonly', False)]},
        copy=False
    )
    pay_method_sat_id = fields.Many2one(
        'pay.method.sat',
        string=_('Pay method'),
        # readonly=True,
        states={'draft': [('readonly', False)]}
    )
    cfdi_version = fields.Many2one(
        comodel_name="cfdi.version",
        required=False,
        string=_("CFDI Version"),
        help=_("Assign to CFDI version."),
        copy=False,
    )
    cfdi_sign_ids = fields.One2many(
        comodel_name='cfdi.sign',
        inverse_name='account_invoice_id'
    )
    hide_button_firm = fields.Boolean(
        string="Hide Button Firm",
        compute=_compute_hide
    )
    state_document = fields.Selection(
        selection=state_list, default='not_signed', nocopy=True)
    relation_type = fields.Many2one(
        comodel_name='relation.type',
        string=_("Relation Type")
    )
    cancel_wcfdi = fields.Boolean(
        string="Not Use CFDI", help="Set True to don't use CFDI sign"
    )
    state = fields.Selection([
            ('draft','Draft'),
            ('proforma','Pro-forma'),
            ('proforma2','Pro-forma'),
            ('open','Open'),
            ('paid','Paid'),
            ('pending', _('Pending')),
            ('cancel','Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Pro-forma' when invoice is in Pro-forma status,invoice does not have an invoice number.\n"
             " * The 'Open' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice.\n"
             " * The 'Pending' status is used when an invoice is waiting for an answer to be cancelled.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice.")

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice,
                            payment_term, partner_bank_id, company_id):
        result = super(AccountInvoice, self).onchange_partner_id(
            type, partner_id, date_invoice, payment_term,
            partner_bank_id, company_id)
        msg1 = _("CFDI use is not configurated in your company settings")
        msg2 = _("Payment method is not configurated in your company settings")

        if type in ['out_invoice','in_refund']:
            if partner_id:
                if not self.use_cfdi:
                    result['value'].update({'use_cfdi': self.env[
                    'res.partner'].browse(partner_id).use_cfdi.id
                                        })

        if type in ['out_refund','in_invoice']:
            company_obj = self.env['res.company'].browse(company_id)

            if company_obj.use_cfdi.id and company_obj.pay_method_sat_id.id:
                result['value'].update(
                    {
                        'use_cfdi': company_obj.use_cfdi.id,
                        'pay_method_sat_id': company_obj.pay_method_sat_id.id
                    }
                )

            elif (not company_obj.use_cfdi.id) and company_obj.pay_method_sat_id.id:
                raise exceptions.Warning(msg1)

            elif company_obj.use_cfdi.id and (not company_obj.pay_method_sat_id.id):
                raise exceptions.Warning(msg2)

            else: raise exceptions.Warning(msg1 + '\n' + msg2)

        return result

    @api.model
    def create(self,vals):
        if self.env.context.get('type') == 'out_invoice':
            if not vals.get('use_cfdi'):
                if not self.use_cfdi:
                    vals['use_cfdi']=self.env.user.company_id.use_cfdi.id
        if self.env.context.get('type') == 'out_refund':
            if not vals.get('use_cfdi'):
                if not self.use_cfdi:
                    vals['use_cfdi']=self.env.user.company_id.use_cfdi.id
            if not vals.get('pay_method_sat_id'):
                vals['pay_method_sat_id']=self.env.user.company_id.pay_method_sat_id.id
        return super(AccountInvoice, self).create(vals)

    @api.multi
    def write(self,vals):
        if self.env.context.get('type') == 'out_invoice':
            if not vals.get('use_cfdi'):
                if not self.use_cfdi:
                    vals['use_cfdi']=self.env.user.company_id.use_cfdi.id
        if self.env.context.get('type') == 'out_refund':
            if not vals.get('use_cfdi'):
                if not self.use_cfdi:
                    vals['use_cfdi']=self.env.user.company_id.use_cfdi.id
            if not vals.get('pay_method_sat_id'):
                vals['pay_method_sat_id']=self.env.user.company_id.pay_method_sat_id.id
        return super(AccountInvoice, self).write(vals)

    @api.multi
    def action_cancel_draft(self):
        res = super(AccountInvoice, self).action_cancel_draft()
        self.cancel_wcfdi = False
        return res

    @api.multi
    def cancel_check_status(self):
        if self.state in ['draft','proforma2','open'] and self.state_document not in ['signed']:
            self.state = 'cancel'
            self.cancel_wcfdi = True
            self.action_cancel()
        if self.cfdi_sign_ids and self.state == 'open':
            for record in self.cfdi_sign_ids:
                uuid = record.uuid
                pac_id = record.pac_id
                select_pac = pac_id.select_pac
                if select_pac == 'diverza' or select_pac == False:
                    connection = httplib.HTTPSConnection(pac_id.url_webservice)
                    if connection:
                        method = 'PUT'
                        headers = {"Content-Type": "application/json"}
                        end_point = "/api/v1/documents/{uuid}/sat_cfdi_enquiry"
                        end_point = str(end_point).replace('{uuid}', uuid)
                        body =  {
                                    "credentials":
                                        {
                                            "id": pac_id.user,
                                            "token": pac_id.password
                                        }
                                }
                        connection.request(
                            method,
                            end_point,
                            json.dumps(body, ensure_ascii=False),
                            headers
                        )
                        response = connection.getresponse()
                        data_read = json.loads(response.read())
                        cancelation_status = data_read.get('estatus_cancelacion')
                        status = data_read.get('estado')
                        is_cancellable = data_read.get('es_cancelable')
                        _logger.info("Status: %s"%status)
                        _logger.info("Cancellation status: %s"%cancelation_status)
                        _logger.info("Is cancellable?: %s"%is_cancellable)
                        if status == 'Vigente':
                            if is_cancellable == 'Cancelable sin aceptación':
                                signature_cancellation_date = fields.Datetime.now()
                                self.state_document = 'cancelled'
                                self.state = 'cancel'
                                record.write({
                                    'signature_cancellation_date': signature_cancellation_date,
                                    'state_document': 'cancelled'
                                })
                                self.action_cancel()
                            elif is_cancellable == 'Cancelable con aceptación':
                                self.state_document = 'pending'
                                self.state = 'pending'
                                self.cancel_wcfdi = True
                                record.write({
                                    'state_document': 'pending'
                                })
                                self.cancel_cfdi()
                        if status == 'Cancelado':
                            self.state = 'cancel'
                            self.state_document = 'cancelled'
                            self.cancel_wcfdi = True
                            record.write({
                                'signature_cancellation_date': signature_cancellation_date,
                                'state_document': 'cancelled'
                            })
                            self.action_cancel()
                if select_pac == 'smarter_web':
                    cfdi33 = self.env['cfdi33']
                    o = cfdi33.XmlCfdi()
                    xml_signed_b64 = record.xml_signed
                    xml_signed = o.decode64(xml_signed_b64)
                    signed_xml_root = lxml.fromstring(xml_signed)
                    comprobante_attribs = signed_xml_root.attrib
                    amount_total = comprobante_attribs['Total']
                    for element in signed_xml_root.iter():
                        if element.tag == '{http://www.sat.gob.mx/cfd/3}Emisor':
                            re = element.attrib['Rfc']
                        if element.tag == '{http://www.sat.gob.mx/cfd/3}Receptor':
                            rr = element.attrib['Rfc']
                    if pac_id.url_webservice == 'services.test.sw.com.mx':
                        url = 'pruebacfdiconsultaqr.cloudapp.net'
                    elif pac_id.url_webservice == 'services.sw.com.mx':
                        url = 'consultaqr.facturaelectronica.sat.gob.mx'
                    connection = httplib.HTTPSConnection(url, context=ssl._create_unverified_context())
                    if connection:
                        method = 'POST'
                        end_point = "/ConsultaCFDIService.svc"
                        headers = {
                            "Content-type": "text/xml",
                            "Accept": "text/xml",
                            'SOAPAction': 'http://tempuri.org/IConsultaCFDIService/Consulta'
                        }
                        url_schemas = "http://schemas.xmlsoap.org/soap/envelope/"
                        url_tempuri = "http://tempuri.org/"
                        root = lxml.Element(lxml.QName(url_schemas, 'Envelope'), nsmap={'soapenv':url_schemas, 'tem':url_tempuri})
                        header = lxml.SubElement(root, lxml.QName(url_schemas,'Header'))
                        body = lxml.SubElement(root, lxml.QName(url_schemas,'Body'))
                        query = lxml.SubElement(body, lxml.QName(url_tempuri,'Consulta'))
                        printedExpression = lxml.SubElement(query, lxml.QName(url_tempuri,'expresionImpresa'))
                        printedExpression.text = lxml.CDATA("?re=%s&rr=%s&tt=%s&id=%s"%(re,rr,amount_total,uuid))
                        _logger.info(lxml.tostring(root, pretty_print=True, method='xml'))
                        connection.request(
                            method,
                            end_point,
                            lxml.tostring(root, pretty_print=True, method='xml'),
                            headers
                        )
                        response = connection.getresponse()
                        response_answer = response.read()
                        webservice_answer = lxml.fromstring(response_answer)
                        _logger.info(lxml.tostring(webservice_answer, pretty_print=True, method='xml'))
                        for element in webservice_answer.iter():
                            if element.tag == '{http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio}EsCancelable':
                                is_cancellable = ''.join(element.itertext())
                            if element.tag == '{http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio}Estado':
                                status = ''.join(element.itertext())
                            if element.tag == '{http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio}EstatusCancelacion':
                                cancelation_status = ''.join(element.itertext())
                        _logger.info("Status: %s"%status)
                        _logger.info("Cancellation status: %s"%cancelation_status)
                        _logger.info("Is cancellable?: %s"%is_cancellable)
                        if status == 'Vigente':
                            if is_cancellable == 'Cancelable sin aceptación':
                                self.action_cancel()
                                signature_cancellation_date = fields.Datetime.now()
                                self.state_document = 'cancelled'
                                self.state = 'cancel'
                                record.write({
                                    'signature_cancellation_date': signature_cancellation_date,
                                    'state_document': 'cancelled'
                                    })
                            elif is_cancellable == 'Cancelable con aceptación':
                                self.cancel_cfdi()
                                self.state_document = 'pending'
                                self.state = 'pending'
                                self.cancel_wcfdi = True
                                record.write({
                                    'state_document': 'pending'
                                })
                        if status == 'Cancelado':
                            self.state = 'cancel'
                            self.state_document = 'cancelled'
                            self.cancel_wcfdi = True
                            record.write({
                                'signature_cancellation_date': signature_cancellation_date,
                                'state_document': 'cancelled'
                            })
                            self.action_cancel()
        return True

    # NOTE: Function is wrotten in old api because ir_cron only seems to accept old api functions
    def cancel_invoices_cron(self, cr, uid, context=None):
        _logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@ CANCELLING UUIDS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        pending_invoices = self.search(cr, uid, [('state_document','=','pending')], None)
        invoice_ids = self.browse(cr, uid, pending_invoices, None)
        for invoice in invoice_ids:
            if invoice.cfdi_sign_ids:
                for record in invoice.cfdi_sign_ids:
                    uuids = []
                    uuid = record.uuid
                    pac_id = record.pac_id
                    certificate_number = record.certificate_number
                    select_pac = pac_id.select_pac
                    if select_pac == 'diverza' or select_pac == False:
                        connection = httplib.HTTPSConnection(pac_id.url_webservice)
                        if connection:
                            method = 'PUT'
                            headers = {"Content-Type": "application/json"}
                            end_point = "/api/v1/documents/{uuid}/sat_cfdi_enquiry"
                            end_point = str(end_point).replace('{uuid}', uuid)
                            body =  {
                                        "credentials":
                                            {
                                                "id": pac_id.user,
                                                "token": pac_id.password
                                            }
                                    }
                            connection.request(
                                method,
                                end_point,
                                json.dumps(body, ensure_ascii=False),
                                headers
                            )
                            response = connection.getresponse()
                            data_read = json.loads(response.read())
                            cancelation_status = data_read.get('estatus_cancelacion')
                            status = data_read.get('estado')
                            is_cancellable = data_read.get('es_cancelable')
                            _logger.info("Status: %s"%status)
                            _logger.info("Cancellation status: %s"%cancelation_status)
                            _logger.info("Is cancellable?: %s"%is_cancellable)
                            if status == 'Cancelado':
                                signature_cancellation_date = fields.Datetime.now()
                                record.write({
                                    'state_document': 'cancelled',
                                    'signature_cancellation_date': signature_cancellation_date,
                                })
                                invoice.write({
                                    'state_document': 'cancelled',
                                    'state': 'cancel',
                                    'cancel_wcfdi': True
                                })
                                invoice.action_cancel()
                                uuids.append(uuid)
                            elif status == 'Vigente':
                                if not cancelation_status == 'En proceso':
                                    invoice.state = 'open'
                                    invoice.state_document = 'signed'
                                    record.state_document = 'signed'
                    if select_pac == 'smarter_web':
                        cfdi33_obj = self.pool.get('cfdi33')
                        cfdi33 = cfdi33_obj.browse(cr, uid, [], context)
                        o = cfdi33.XmlCfdi()
                        xml_signed_b64 = record.xml_signed
                        xml_signed = o.decode64(xml_signed_b64)
                        signed_xml_root = lxml.fromstring(xml_signed)
                        comprobante_attribs = signed_xml_root.attrib
                        amount_total = comprobante_attribs['Total']
                        for element in signed_xml_root.iter():
                            if element.tag == '{http://www.sat.gob.mx/cfd/3}Emisor':
                                re = element.attrib['Rfc']
                            if element.tag == '{http://www.sat.gob.mx/cfd/3}Receptor':
                                rr = element.attrib['Rfc']
                        if pac_id.url_webservice == 'services.test.sw.com.mx':
                            url = 'pruebacfdiconsultaqr.cloudapp.net'
                        elif pac_id.url_webservice == 'services.sw.com.mx':
                            url = 'consultaqr.facturaelectronica.sat.gob.mx'
                        connection = httplib.HTTPSConnection(url, context=ssl._create_unverified_context())
                        if connection:
                            method = 'POST'
                            end_point = "/ConsultaCFDIService.svc"
                            headers = {
                                "Content-type": "text/xml",
                                "Accept": "text/xml",
                                'SOAPAction': 'http://tempuri.org/IConsultaCFDIService/Consulta'
                            }
                            url_schemas = "http://schemas.xmlsoap.org/soap/envelope/"
                            url_tempuri = "http://tempuri.org/"
                            root = lxml.Element(lxml.QName(url_schemas, 'Envelope'), nsmap={'soapenv':url_schemas, 'tem':url_tempuri})
                            header = lxml.SubElement(root, lxml.QName(url_schemas,'Header'))
                            body = lxml.SubElement(root, lxml.QName(url_schemas,'Body'))
                            query = lxml.SubElement(body, lxml.QName(url_tempuri,'Consulta'))
                            printedExpression = lxml.SubElement(query, lxml.QName(url_tempuri,'expresionImpresa'))
                            printedExpression.text = lxml.CDATA("?re=%s&rr=%s&tt=%s&id=%s"%(re,rr,amount_total,uuid))
                            _logger.info(lxml.tostring(root, pretty_print=True, method='xml'))
                            connection.request(
                                method,
                                end_point,
                                lxml.tostring(root, pretty_print=True, method='xml'),
                                headers
                            )
                            response = connection.getresponse()
                            response_answer = response.read()
                            webservice_answer = lxml.fromstring(response_answer)
                            _logger.info(lxml.tostring(webservice_answer, pretty_print=True, method='xml'))
                            for element in webservice_answer.iter():
                                if element.tag == '{http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio}EsCancelable':
                                    is_cancellable = ''.join(element.itertext())
                                if element.tag == '{http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio}Estado':
                                    status = ''.join(element.itertext())
                                if element.tag == '{http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio}EstatusCancelacion':
                                    cancelation_status = ''.join(element.itertext())
                            _logger.info("Status: %s"%status)
                            _logger.info("Cancellation status: %s"%cancelation_status)
                            _logger.info("Is cancellable?: %s"%is_cancellable)
                            if status == 'Cancelado':
                                signature_cancellation_date = fields.Datetime.now()
                                record.write({
                                    'state_document': 'cancelled',
                                    'signature_cancellation_date': signature_cancellation_date,
                                })
                                invoice.write({
                                    'state_document': 'cancelled',
                                    'state': 'cancel',
                                    'cancel_wcfdi': True
                                })
                                invoice.action_cancel()
                                uuids.append(uuid)
                            elif status == 'Vigente':
                                if not cancelation_status == 'En proceso':
                                    invoice.state = 'open'
                                    invoice.state_document = 'signed'
                                    record.state_document = 'signed'
            if uuids:
                _logger.info("@@@@@@@ CANCELLED THE FOLLOWING UUIDS @@@@@@@\n %s"%uuids)
        return True

    @api.multi
    def action_cancel(self):
        """ Super of method cancel invoice """
        if not self.cfdi_folio_fiscal and self.cfdi_version and\
                self.cfdi_version.version == '3.3':
            if not self.cancel_wcfdi:
                self.cancel_cfdi()
        return super(AccountInvoice, self).action_cancel()

    @api.model
    def cancel_cfdi(self):
        if len(self.cfdi_sign_ids) > 0:
            cfdi = self.env['cfdi33']
            xml_cfdi = cfdi.XmlCfdi()
            for cfdi_sign_id in self.cfdi_sign_ids:
                cancel_path = cfdi.tmp_file()
                cfdi_cancel = xml_cfdi.decode64(cfdi_sign_id.xml_signed)
                xml_cfdi.write_file(cfdi_cancel, cancel_path)
                # signature_cancellation_date = fields.Datetime.now()
                data = xml_cfdi.str2domXml(cancel_path)
                data = data.getroot()
                cfdi.sign_cfdi_xml(self.cfdi_version, 'cancel', data, self.type)
                # cfdi_sign_id.update({'signature_cancellation_date': signature_cancellation_date,
                #                      'state_document': 'cancelled'})
                # self.state_document = 'cancelled'
        else:
            pass

    @api.multi
    def get_firm_document(self, uuids=None, tipo_relacion=None):
        """ This is method create the digital sing  """
        if self.journal_id.cfdi_version and self.journal_id.cfdi_version.version == '3.3':
            self.cfdi_version = self.journal_id.cfdi_version.id
            return self.make_xml_invoice(uuids=uuids, tipo_relacion=tipo_relacion)

    # create xml
    _importe = 0.0
    _impuesto = ''
    _tasaocuota = ''
    _tipofactor = ''
    _subtotal = 0.0
    _totalimpuestostrasladados = 0.0

    @api.model
    def get_cfdi_rels(self):
        cfdi_docs_rel_ids =  self.env['related.document.cfdi.line'].search(
            [('account_invoice_id', '=', self.id)])
        uuid_list = []
        advance = 0
        refund = 0
        code = None
        if len(cfdi_docs_rel_ids) > 0:
            for doc_rel in cfdi_docs_rel_ids:
                dic_uuids = {}
                # anticipo
                if doc_rel.related_document_cfdi_id.advance_payment_id:
                    if doc_rel.related_document_cfdi_id.voucher_id.state != 'cancel' and not doc_rel.related_document_cfdi_id.voucher_id.cfdi_sign_ids:
                        advance_sign_ids = [sign_id.uuid.encode('utf-8') for sign_id in doc_rel.related_document_cfdi_id.advance_payment_id.cfdi_sign_ids]
                        dic_uuids['document_id'] = doc_rel.related_document_cfdi_id.advance_payment_id.id
                        dic_uuids['document_obj'] = doc_rel.related_document_cfdi_id.advance_payment_id._model
                        dic_uuids['partner_folio'] = doc_rel.related_document_cfdi_id.advance_payment_id.number
                        dic_uuids['move'] = 'relacionado'
                        dic_uuids['uuid'] = advance_sign_ids[0]
                        advance = advance +1
                        uuid_list.append(dic_uuids)
                # nota de credito
                else:
                    if doc_rel.related_document_cfdi_id.voucher_id.state != 'cancel':
                        refund_sign_ids = [sign_id.uuid.encode('utf-8') for sign_id in doc_rel.related_document_cfdi_id.account_invoice_id.cfdi_sign_ids]
                        dic_uuids['document_id'] = doc_rel.related_document_cfdi_id.account_invoice_id.id
                        dic_uuids['document_obj'] = doc_rel.related_document_cfdi_id.account_invoice_id._model
                        dic_uuids['partner_folio'] = doc_rel.related_document_cfdi_id.account_invoice_id.number
                        dic_uuids['move'] = 'relacionado'
                        dic_uuids['uuid'] = refund_sign_ids[0]
                        refund = refund + 1
                        uuid_list.append(dic_uuids)
                        #refund_sign_ids.extend([sign_id.uuid.encode('utf-8') for sign_id in doc_rel.related_document_cfdi_id])
            if advance > 0 and refund == 0:
                code = '07'
            elif advance == 0 and refund > 0:
                code = '02'
            elif refund > 0 and advance > 0:
                code = '07'
        return {'dic_uuids': uuid_list, 'code': code}
    @api.model
    def make_xml_invoice(self, uuids=None, tipo_relacion=None):
        """ Create XML Structure """
        if self.type == 'out_refund':
            related_documents = self.env['related.document.cfdi'].search([('account_invoice_id', '=', self.id)])
            if related_documents:
                uuids = []
                for sign in related_documents.related_document_cfdi_line_ids:
                    uuids.append({
                        'document_id': sign.account_invoice_id.id,
                        'document_obj': sign.account_invoice_id._model,
                        'partner_folio': sign.account_invoice_id.number,
                        'move': 'relacionado',
                        'uuid': sign.account_invoice_id.cfdi_sign_ids.uuid
                    })
                tipo_relacion = self.relation_type.code or '03'
            if self.picking_dev_id:
                dev = self.env['gv.devolucion'].search([('picking_id', '=', self.picking_dev_id.id)])
                if dev and dev.invoice_id and dev.invoice_id.cfdi_sign_ids:
                    uuids = []
                    for sign in dev.invoice_id.cfdi_sign_ids:
                        uuids.append({
                            'document_id': dev.invoice_id.id,
                            'document_obj': dev.invoice_id._model,
                            'partner_folio': dev.invoice_id.number,
                            'move': 'relacionado',
                            'uuid': sign.uuid
                        })
                    tipo_relacion = '03'
        elif self.type == 'out_invoice':
            uuids_dic = self.get_cfdi_rels()
            uuids_list = uuids_dic.get('dic_uuids')
            codetype = uuids_dic.get('code')
            if len(uuids_list) > 0:
                tipo_relacion = codetype
                uuids = uuids_list
        cfdi = self.env['cfdi33']
        certificate = cfdi.certificate()

        comprobante = Comprobante()

        comprobante.Version.value = '3.3'
        comprobante.Serie.value = self.get_serie()
        comprobante.Folio.value = self.number
        comprobante.Fecha.value = self.get_issue_datetime()
        comprobante.FormaPago.value = self.get_way_to_pay()
        comprobante.NoCertificado.value = certificate.get('serial_number')
        comprobante.Certificado.value = certificate.get('cer_base64')
        comprobante.CondicionesDePago.value = self.get_payment_term()
        # comprobante.SubTotal.value = self.amount_untaxed
        comprobante.schemaLocation.value = [
            "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd"
        ]
        comprobante.Descuento.value = None
        comprobante.Moneda.value = self.get_currency()
        comprobante.TipoCambio.value = self.get_rate()
        # comprobante.Total.value = self.amount_total
        comprobante.TipoDeComprobante.value = self.get_doc_type()
        comprobante.LugarExpedicion.value = self.get_expedition_place()
        comprobante.MetodoPago.value = self.get_pay_method()

        emisor = Emisor()
        emisor.Rfc.value = self.get_emisor_data('Rfc')
        emisor.Nombre.value = self.get_emisor_data('Nombre')
        emisor.RegimenFiscal.value = self.get_emisor_data('RegimenFiscal')

        comprobante.Emisor = emisor

        receptor = Receptor()
        receptor.Rfc.value = self.get_receptor_data('Rfc')
        receptor.Nombre.value = self.get_receptor_data('Nombre')
        receptor.ResidenciaFiscal.value = self.get_residential_registry().get('ResidenciaFiscal')
        receptor.NumRegIdTrib.value = self.get_residential_registry().get('NumRegIdTrib')
        receptor.UsoCFDI.value = self.use_cfdi.code

        comprobante.Receptor = receptor

        tr, uuid = self.get_tipo_relacion(uuids=uuids, tipo_relacion=tipo_relacion)
        if tr and uuid:
            comprobante.CfdiRelacionados = CfdiRelacionados()
            comprobante.CfdiRelacionados.TipoRelacion.value = tr
            comprobante.CfdiRelacionados.CfdiRelacionado.value = uuid

        # Addenda
        addenda = Addenda()
        folios = Folios()
        folios.folioERP.value = self.number
        folios.codigoCliente.value = self.partner_id.client_number

        datosadicionales = DatosAdicionales()
        datosadicionales.ordenCompra.value = self.get_orden_compra()

        addenda.Folios = folios
        addenda.DatosAdicionales = datosadicionales

        # Domicilio Fiscal
        domicilioemisor = DomicilioEmisor()
        company_address = self.journal_id.address_invoice_company_id
        domicilioemisor.pais.value = self.get_partner_home(company_address, 'Pais')
        domicilioemisor.noInterior.value = self.get_partner_home(company_address, 'noInterior')
        domicilioemisor.noExterior.value = self.get_partner_home(company_address, 'noExterior')
        domicilioemisor.municipio.value = self.get_partner_home(company_address, 'municipio')
        domicilioemisor.localidad.value = self.get_partner_home(company_address, 'localidad')
        domicilioemisor.estado.value = self.get_partner_home(company_address, 'estado')
        domicilioemisor.colonia.value = self.get_partner_home(company_address, 'colonia')
        domicilioemisor.codigoPostal.value = self.get_partner_home(company_address, 'codigoPostal')
        domicilioemisor.calle.value = self.get_partner_home(company_address, 'calle')

        addenda.DomicilioEmisor = domicilioemisor

        # Domicilio Receptor
        domicilioreceptor = DomicilioReceptor()
        partner_address = self.partner_id
        domicilioreceptor.pais.value = self.get_partner_home(partner_address, 'Pais')
        domicilioreceptor.noInterior.value = self.get_partner_home(partner_address, 'noInterior')
        domicilioreceptor.noExterior.value = self.get_partner_home(partner_address, 'noExterior')
        domicilioreceptor.municipio.value = self.get_partner_home(partner_address, 'municipio')
        domicilioreceptor.localidad.value = self.get_partner_home(partner_address, 'localidad')
        domicilioreceptor.estado.value = self.get_partner_home(partner_address, 'estado')
        domicilioreceptor.colonia.value = self.get_partner_home(partner_address, 'colonia')
        domicilioreceptor.codigoPostal.value = self.get_partner_home(partner_address, 'codigoPostal')
        domicilioreceptor.calle.value = self.get_partner_home(partner_address, 'calle')

        addenda.DomicilioReceptor = domicilioreceptor

        if self.partner_id.amazon_customer is False:
            comprobante.Addenda = addenda

        # Conceptos
        conceptos = Conceptos()
        conceptos.Concepto.value = self.get_concepto()

        comprobante.Conceptos = conceptos

        # Impuestos
        impuestos = Impuestos()
        impuestos.Traslados = Traslados()
        impuestos.Traslados.Traslado = Traslado()
        impuestos.Traslados.Traslado.Importe.value = float_round(self._importe, 2) if self._importe > 0.0 else None
        impuestos.Traslados.Traslado.Impuesto.value = self._impuesto
        impuestos.Traslados.Traslado.TasaOCuota.value = self._tasaocuota
        impuestos.Traslados.Traslado.TipoFactor.value = self._tipofactor
        impuestos.TotalImpuestosTrasladados.value = float_round(self._totalimpuestostrasladados, 2)

        comprobante.Impuestos = impuestos

        comprobante.SubTotal.value = self._subtotal
        comprobante.Total.value = float_round(self._totalimpuestostrasladados + self._subtotal, 2)

        comprobante.printf()

        # Version
        version = self.journal_id.cfdi_version

        # Generate dom Xml
        xmlcfdi = XmlCfdi()
        dom_ap = xmlcfdi.object2xml(comprobante)

        path_xml_ap = "{}{}.xml".format(cfdi.path_tmp(), cfdi.uuid())
        xmlcfdi.xml2file(dom_ap, filename=path_xml_ap)

        original_string = xmlcfdi.xml_transform_xslt(version.version, path_xml_ap, to_return='path')
        comprobante.Sello.value = xmlcfdi.xml_sign(original_string, certificate.get('pem_key'))

        dom_ap = xmlcfdi.object2xml(comprobante)

        xml_string_without_sign = xmlcfdi.read_file(path_xml_ap)

        # dom_ap = xmlcfdi.xml_pretty(dom_ap)
        node = None
        node_addenda = None
        if self.partner_id.amazon_customer is False:
            dom_ap, node = xmlcfdi.rm_node_xml(dom_ap, 'cfdi:Addenda')
        # node = xmlcfdi.object2xml(addenda)
        if dom_ap:
            xmlcfdi.xml2file(dom_ap, filename=path_xml_ap)
        ok = xmlcfdi.xml_validate_xsd(version.version, path_xml_ap)

        if ok:
            signature_date = fields.Datetime.now()
            result_sign = cfdi.sign_cfdi_xml(version, 'sign', dom_ap, self.type)
            sos_path = cfdi.tmp_file()
            xmlcfdi.write_file(result_sign.get('contentXml'), sos_path)
            signed_original_string = xmlcfdi.xml_transform_xslt(
                version.version, sos_path, _type='tfd', to_return='content')
            if node:
                dom_ap.append(node)
                node_addenda = xmlcfdi.str2dom(xmlcfdi.domXml2str(dom_ap))
                node_addenda = node_addenda.firstChild.getElementsByTagName('cfdi:Addenda')
            xml_signed = result_sign.get('contentDom')
            if node_addenda != None:
                if len(node_addenda) > 0:
                    xml_signed.firstChild.appendChild(node_addenda[0])
            # payment_term = self.get_payment_term()
            # if payment_term:
            #     xml_signed.firstChild.setAttribute('CondicionesDePago', payment_term)
            # xml_signed.getroot().append(node)

            cfdi_sign_params = {
                'original_string': xmlcfdi.read_file(original_string),
                'certificate_number': comprobante.NoCertificado.value,
                'certificate': comprobante.Certificado.value,
                'stamp': comprobante.Sello.value,
                'signature_date': signature_date,
                'move_id': self.move_id.id,
                'signed_original_string': signed_original_string,
                'uuid': result_sign.get('uuid'),
                'pac_id': result_sign.get('pac_id'),
                'xml': xmlcfdi.encode64(xml_string_without_sign),
                'xml_signed': xmlcfdi.encode64(xml_signed.toxml(encoding="utf-8")),
                # 'pdf_signed': '',
                'account_invoice_id': self.id,
                'state_document': 'signed',
            }

            self.cfdi_version = version.id
            cfdi_sign_id = self.env['cfdi.sign'].create(cfdi_sign_params)
            cfdi_sign_id.refresh_pdf()
            if type(uuids) is dict:
                uuids = []

            uuids.append({
                'document_id': self.id,
                'document_obj': self._model,
                'partner_folio': self.number,
                'move': 'origen',
                'uuid': cfdi_sign_params.get('uuid')
            })
            self.move_id.write({'cfdi_table_ids': [(0, 0, x) for x in uuids]})
            self.state_document = 'signed'
            return True
        else:
            return False

    @api.model
    def get_emisor_data(self, data):
        result = None
        message = ''
        if data == 'Rfc':
            result = self.get_rfc(self.company_id.partner_id)
            message = _('The sender does not have a registered RFC') if not result else ""
        elif data == 'Nombre':
            result = self.company_id.partner_id.name
            message = _('The sender does not have a registered Name') if not result else ""
        elif data == 'RegimenFiscal':
            result = self.company_id.partner_id.regimen_fiscal_id.code
            message = _('The sender does not have a registered tax regime code') if not result else ""
        if message:
            self.get_raise_error(message)
        return result

    @api.model
    def get_receptor_data(self, data):
        result = None
        message = ''
        if data == 'Rfc':
            result = self.get_rfc(self.partner_id)
            message = _('The receptor does not have a registered RFC') if not result else ""
        elif data == 'Nombre':
            result = self.partner_id.name
            message = _('The receptor does not have a registered Name') if not result else ""
        if message:
            self.get_raise_error(message)
        return result

    @api.model
    def get_concepto(self):
        result = []
        for line in self.invoice_line:
            concepto = Concepto()
            concepto.Cantidad.value = line.quantity if line.quantity else self.get_raise_error(_('No quantity concepto'))
            concepto.ClaveProdServ.value = self.get_data_concept(line.product_id, 'ClaveProdServ')
            concepto.ClaveUnidad.value = self.get_data_concept(line.product_id, 'ClaveUnidad')
            concepto.NoIdentificacion.value = self.get_data_concept(line.product_id, 'NoIdentificacion')
            concepto.Unidad.value = self.get_data_concept(line.product_id, 'Unidad')
            concepto.ValorUnitario.value = line.price_unit if line.price_unit else self.get_raise_error(_('No concept price unit'))
            concepto.Descripcion.value = self.get_data_concept(line.product_id, 'Descripcion')
            concepto.Importe.value = line.price_subtotal if line.price_subtotal else self.get_raise_error(_('No cost in concept'))

            traslado = Traslado()
            trasl = []
            for t in self.get_traslado(traslado, line):
                if t.Importe.value > 0.0:
                    trasl.append(t)
            if trasl:
                concepto.Impuestos = Impuestos()
                concepto.Impuestos.Traslados = Traslados()
                concepto.Impuestos.Traslados.Traslado.value = trasl
            if self.picking_dev_id:
                if self.picking_dev_id.pack_operation_ids and self.partner_id.amazon_customer is False:
                    picking = self.picking_dev_id
                    concepto.Parte.value = self.get_parte(picking, line)
                else:
                    concepto.Parte.value = None
            result.append(concepto)
            self._subtotal += concepto.Importe.value
        return result

    @api.model
    def get_parte(self, picking, line):
        result = []
        for pack_line in picking.pack_operation_ids:
            if pack_line.lot_id and pack_line.lot_id.name:
                if pack_line.product_id == line.product_id:
                    parte = Parte()
                    parte.Cantidad.value = pack_line.product_qty
                    parte.ClaveProdServ.value = self.get_data_concept(pack_line.product_id, 'ClaveProdServ')
                    parte.Descripcion.value = self.get_data_concept(pack_line.product_id, 'Descripcion')
                    parte.NoIdentificacion.value = pack_line.lot_id.name
                    result.append(parte)
        return result

    @api.model
    def get_traslado(self, obj, line):
        result = []
        if len(line.invoice_line_tax_id) > 1:
            raise MissingError(_('More the One Tax'))
        importe = 0.0
        impuesto = ''
        tasaocuota = ''
        tipofactor = ''
        for tax in line.invoice_line_tax_id:
            traslado = obj
            traslado.Base.value = line.price_subtotal if \
                line.price_subtotal else \
                self.get_raise_error(_('No base for generate tax'))

            tax_list = tax.compute_all(line.price_unit, line.quantity,
                                       product=line.product_id.id,
                                       partner=self.partner_id.id)
            taxes = tax_list.get('taxes', False)

            traslado.Importe.value = float_round(taxes[0].get('amount'), 2) if \
                taxes[0].get('amount') else None

            traslado.Impuesto.value = tax.tax_category_id.code if \
                tax.tax_category_id.code else \
                self.get_raise_error(_('No tax code'))

            traslado.TasaOCuota.value = str(tax.amount) + '0000' if \
                tax.amount else None

            traslado.TipoFactor.value = tax.tax_category_id.factor_type if \
                tax.tax_category_id.factor_type else \
                self.get_raise_error(_('No factor type for tax'))
            result.append(traslado)
            importe += traslado.Importe.value if traslado.Importe.value else 0.0
            impuesto = traslado.Impuesto.value
            tasaocuota = traslado.TasaOCuota.value
            tipofactor = traslado.TipoFactor.value
            self._totalimpuestostrasladados += traslado.Importe.value if traslado.Importe.value else 0.0
        self._importe += importe
        self._impuesto = impuesto
        self._tasaocuota = tasaocuota
        self._tipofactor = tipofactor
        return result

    @api.model
    def get_data_concept(self, product, data):
        result = None
        message = ''
        product_tmpl_id = product.product_tmpl_id
        if data == 'ClaveProdServ':
            result = product_tmpl_id.line.sat_product.code if \
                product_tmpl_id.line.sat_product.code else None
            if self.get_doc_type() == 'E':
                result = '84111506'
            message += _("No ClaveProdServ in product.\n") if not result else ''
        elif data == 'ClaveUnidad':
            result = product_tmpl_id.uom_id.code if \
                product_tmpl_id.uom_id.code else None
            if self.get_doc_type() == 'E':
                result = 'ACT'
            message += _("No ClaveUnidad in product.\n") if not result else ''
        elif data == 'NoIdentificacion':
            if self.partner_id.amazon_customer:
                result = product.amazonAsin if \
                    product.amazonAsin else None
            else:
                result = product.default_code if \
                    product.default_code else None
        elif data == 'Unidad':
            result = product_tmpl_id.uom_id.name if \
                product_tmpl_id.uom_id.name else None
        elif data == 'Descripcion':
            result = product.name_template if \
                product.name_template else None
            message += _("No product description.\n") if not result else ''
        if message:
            raise MissingError(message)
        return result

    @api.model
    def get_raise_error(self, message):
        if message:
            raise MissingError(message)

    @api.model
    def get_orden_compra(self):
        if self.origin:
            stock_picking = self.env['stock.picking'].search([('name', '=', self.origin)])
            sale_order = self.env['sale.order'].search([('name', '=', stock_picking.origin)])
            return sale_order.client_order_ref
        return None

    @api.model
    def get_partner_home(self, object, data):
        result = None
        if data == 'Pais':
            result = object.country_id.code
        elif data == 'noInterior':
            result = object.l10n_mx_street4
        elif data == 'noExterior':
            result = object.l10n_mx_street3
        elif data == 'municipio':
            result = object.l10n_mx_city2
        elif data == 'localidad':
            result = None
        elif data == 'estado':
            result = object.state_id.name
        elif data == 'colonia':
            result = object.street2
        elif data == 'codigoPostal':
            result = object.zip
        elif data == 'calle':
            result = object.street
        return result or None

    @api.model
    def get_residential_registry(self):
        if not self.partner_id.country_id.code:
            raise MissingError(_('The client not have country code assigned'))
        values = {}
        if self.partner_id.country_id.code == 'MEX':
            values.update({'ResidenciaFiscal': None,
                           'NumRegIdTrib': None})
        else:
            values.update({'ResidenciaFiscal': self.partner_id.country_id.code,
                           'NumRegIdTrib': self.get_receptor_data('Rfc')})
        return values

    @api.model
    def get_rfc(self, partner_id):
        if partner_id.vat:
            rfc = partner_id.vat
            rfc = rfc[2:]
            return rfc

    @api.model
    def get_expedition_place(self):
        _expedition_place = False
        if self.journal_id.address_invoice_company_id:
            _expedition_place = str(self.journal_id.address_invoice_company_id.zip.name)
        if not _expedition_place:
            raise MissingError(_('The zip code is required'))
        return str(_expedition_place)

    @api.model
    def get_rate(self):
        _rate = None
        if self.get_currency() not in ['MXN', 'XXX']:
            if self.currency_rate_alter > 0:
                _rate = self.currency_rate_alter
            else:
                if self.move_id:
                    for move_line in self.move_id.line_id:
                        if move_line.currency_id and move_line.amount_currency > 0:
                            _rate = (move_line.credit or move_line.debit) / move_line.amount_currency
                    if _rate is None or _rate == 0:
                        _("The field rate is required when is different NXN or XXX.\ Current value ({}) ".format(_rate))
            _rate = "{0:6f}".format(round(_rate, 6))
        return _rate

    @api.model
    def get_pay_method(self):
        return self.pay_method_sat_id.code or None

    @api.model
    def get_currency(self):
        _currency = None
        if len(self.journal_id.currency) > 0:
            _currency = self.journal_id.currency.name
        else:
            _currency = self.journal_id.company_id.currency_id.name
        return _currency

    @api.model
    def get_serie(self):
        _serie = None
        if self.journal_id.invoice_sequence_id:
            series = self.journal_id.invoice_sequence_id.approval_ids
        elif self.journal_id.sequence_id:
            series = self.journal_id.sequence_id.approval_ids
        if len(series):
            for serie in series:
                _serie = serie.serie
        return _serie

    @api.model
    def get_issue_datetime(self):
        _issue_dt = ''
        if self.invoice_datetime:
            central_dt = self.get_utc_city(self.invoice_datetime)
            _invoice_datetime = datetime.strptime(central_dt, "{} {}".format(
                DATETIME_FORMATS_MAP.get('%F'), DATETIME_FORMATS_MAP.get('%T')
            ))
            _issue_dt = _invoice_datetime.strftime(
                (DATETIME_FORMATS_MAP.get('%F') + 'T' + DATETIME_FORMATS_MAP.get('%T')))
            # _issue_dt = datetime.strptime(self.invoice_datetime, _issue_dt_format)

        else:
            raise MissingError(_('This field {} is required'.format(self.invoice_datetime.__name__)))
        return _issue_dt

    @api.model
    def get_utc_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        return dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')

    @api.model
    def get_way_to_pay(self):
        _way_to_pay = ''
        if self.pay_method_ids:
            for pay_method in self.pay_method_ids:
                _way_to_pay = pay_method.code
            #if self.get_doc_type() == 'I':  # invoice
            #    pass
            #elif self.get_doc_type() == 'E':
            #    if self.picking_dev_id:
            #        _origin = self.picking_dev_id.origin
            #        document_origin = self.env['account.invoice'].search([('picking_dev_id', '=', _origin)])
            #        if document_origin:
            #            if _way_to_pay != document_origin.get_way_to_pay():
            #                raise MissingError(
            #                    _('For this document is necessary has the same way to pay.\n Document ref: {} -> {}'.
            #                      format(self.number, document_origin.number))
            #                )
            #    else:
            #        pass  # Refound without reference
        else:
            raise Warning(_('No method of payment'))

        return _way_to_pay

    @api.model
    def get_doc_type(self):
        _type = None
        if self.type == 'out_invoice':
            _type = 'I'
        elif self.type == 'out_refund':
            _type = 'E'
        return _type

    @api.model
    def get_payment_term(self):
        _payment_term = None
        if self.payment_term:
            _payment_term = self.payment_term.name
        return _payment_term

    @api.model
    def get_tipo_relacion(self, uuids=None, tipo_relacion=None):
        code = None
        result = []
        # if self.type == 'out_invoice':
        if uuids and tipo_relacion:
            for uuid in uuids:
                cfdirelacionado = CfdiRelacionado()
                cfdirelacionado.UUID.value = uuid.get('uuid')
                result.append(cfdirelacionado)
            return tipo_relacion, result
        else:
            return code, result
        # elif self.type == 'out_refund':
        #     if self.picking_dev_id:
        #         dev = self.env['gv.devolucion'].search([('picking_id', '=', self.picking_dev_id.id)])
        #         print dev.name
        #         if dev and dev.invoice_id and dev.invoice_id.cfdi_sign_ids:
        #             print dev.invoice_id
        #             for sign in dev.invoice_id.cfdi_sign_ids:
        #                 cfdirelacionado = CfdiRelacionado()
        #                 cfdirelacionado.UUID.value = sign.uuid
        #                 result.append(cfdirelacionado)
        #             code = '03'
        #             if result:
        #                 return code, result
        #     else:
        #         return code, None

    @api.multi
    def action_view_related_documents(self):
        # if self.state == 'posted':
        type_sign = self.journal_id.account_account_apply_id.code

        if type_sign in ['refund']:
            rdc = self.env['related.document.cfdi'].search([('account_invoice_id', '=', self.id)])
            ok = len(rdc)
            # if type_sign == 'advance' and not ok:
            #     rdc = self.advance_or_refund(type_sign='advance_related')
            # elif type_sign == 'refund' and not ok:
            #     rdc = self.advance_or_refund(type_sign='refund')
            if len(rdc):
                _return_values = {
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'related.document.cfdi',
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'context': {'active_id': self.id},
                    'target': 'new',
                    'res_id': rdc.id,
                }
                return _return_values
            else:
                wizard_form = self.env.ref(
                    'cfdi33.iuv_todo_customer_related_document_cfdi_form',
                    False
                )

                _return_values = {
                    'name': 'Link Documents',
                    'type': 'ir.actions.act_window',
                    'res_model': 'related.document.cfdi',
                    'view_id': wizard_form.id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'context': {'active_id': self.id},
                    'target': 'new'
                }
                return _return_values
                # raise Warning(_("This credit note not has related documents"))
        else:
            raise Warning(_("This credit note not has related documents"))
        # else:
        #     raise Warning(_("Is necessary posted this credit note for view related documents."))
