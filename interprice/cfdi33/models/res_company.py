# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, _


class ResCompany(models.Model):
    _inherit = 'res.company'


    use_cfdi = fields.Many2one(
        comodel_name="use.cfdi",
        required=True,
        string=_("Use CFDI for Credit Notes"),
        help=_("Set a default CFDI use for Credit Notes"),
        copy=False
    )

    pay_method_sat_id = fields.Many2one(
        'pay.method.sat',
        string=_('Pay method for Credit Notes'),
        required=True
    )

    html_terms = fields.Html(
        string=_('Invoice Terms and Conditions'),
        help=_("HTML Format field for terms and conditions"),
    )
