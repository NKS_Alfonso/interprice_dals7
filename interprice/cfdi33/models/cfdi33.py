# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from urllib2 import Request
from openerp import models, fields, api, exceptions, _
import re
import uuid
import os
import time
import ast
from openerp.exceptions import Warning
from openerp.exceptions import MissingError
from openerp.tools.misc import DATETIME_FORMATS_MAP
from openerp.tools.misc import server_to_local_timestamp
from cfdi.impuestos import Impuestos
from cfdi.emisor import Emisor
from cfdi.receptor import Receptor
from cfdi.conceptos import Conceptos
from cfdi.concepto import Concepto
from cfdi.pago import Pago
from cfdi.pagos import Pagos
from cfdi.doctorelacionado import DoctoRelacionado
from cfdi.comprobante import Comprobante
from cfdi.domicilioreceptor import DomicilioReceptor
from cfdi.domicilioemisor import DomicilioEmisor
from cfdi.xml_cfdi import XmlCfdi
from cfdi.traslado import Traslado
from cfdi.traslados import Traslados
from cfdi.folios import Folios
from cfdi.addenda import Addenda
from cfdi.complemento import Complemento
from xml.etree import ElementTree
from xml.dom import minidom
import tempfile
import base64
import httplib
import json
from cfdi.cfdirelacionados import CfdiRelacionados
from cfdi.cfdirelacionado import CfdiRelacionado
import logging
_logger = logging.getLogger(__name__)


class CFDI33(models.Model):
    """ Base module for electronic invoicing version 3.3

    Fields:
      name (Char): Human readable name which will identify each record.

    Catalogos necesarios:
        Catálogo de productos / servicios.
            Agregar en el modulode Categorias
        Catálogo de uso de comprobantes.
            Agregar en el catalogo del cliente
            Agregar en el catalogo de facturas/notas
        Catálogo de Métodos de Pago
            Cambio de etiquetas en clientes / facturas / cotizacion
        Forma de pago
            Agregar campos de los codigo
        Catálogo de unidades de medida para los conceptos en el CFDI.
            agregar simbolo / codigo
        Impuestos
            agregar codigo
        Regimen
            Agregar campo codigo
        Catalogo de tipo relacion.
            nuevo catalogo

    Documenta en "Fields" los campos o catalogos agregados,
    quitar de "Catalogos necesarios" lo agregado.
    """

    _name = 'cfdi33'
    _description = u'Electronic invoicing version 3.3'

    def xml_creation(*args, **kwargs):
        """
        This function XML conectivity with PAC the Web Service.

        **Keyword args:
            headers:    Dictionary with API-client headers.
            xml:        String in XML format.

        Return:
            xml:        Dictionary with XML DOM object
        """
        pass

    def pac_ws(*args, **kwargs):
        """
        This function handles conectivity with PAC the Web Service.

        **Keyword args:
            headers:    Dictionary with API-client headers.
            xml:        XML DOM object.

        Returns:
            erros:      Dictionary with error if any.
            success:    Dictionary with PAC success response.

        """

        def _get_pacs_conf(self):
            """
            This private function should support get all PACs configuration
            e.g. URL, Token and certs.
            Also should be able to create headers for the api call request.

            Return:
                pacs:   list of dictionary with PAC information URL, Token,
                        headers, etc..
            """
            pacs = False
            return pacs

        def _api_call(self, pac, headers):
            """
            This private function should be able to make api request a PAC WS.
            """
            values = pac['values']
            URL = pac['URL']
            cert = pac['cert']
            request = Request(URL, data=values, headers=headers)

        pacs = _get_pacs_conf()
        resp = False

        for pac in pacs:
            resp = _api_call(pac)
            if resp == 'success':
                break
        if not resp or resp == 'error':
            exceptions.ValidationError("PAC couldn't be contacted")
        pass

    name = fields.Char(
        string='Name',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        size=50,
        translate=True
    )

    folio = fields.Char(
        string='Folio',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        size=50,
        translate=True
    )

    # SW DEVELOPERS INTEGRATION
    @api.model
    def smarter_web_33(self, **kwargs):
        pac = kwargs.get('pac')
        action = kwargs.get('action')
        data = kwargs.get('data')
        company = self.company().partner_id
        rfc = self.partner(company, 'rfc')
        email = self.partner(company, 'email')
        url_webservice = pac.url_webservice
        body = {}

        method = 'POST'

        # Get token
        auth = {
            "header": {
                "Content-Type": "application/json",
                "user": pac.user,
                "password": pac.password,
            }
        }
        connection = httplib.HTTPSConnection(url_webservice)
        if connection:
            connection.request(
                method,
                "/security/authenticate",
                {},
                auth.get('header')
            )
        response = connection.getresponse()
        if response.status in [200,201]:
            response_data = response.read()
            response_dict = ast.literal_eval(response_data)
            token = response_dict.get('data').get('token')
        else:
            raise exceptions.Warning(_("Failed to authenticate to Smarter Web service"))
        if token:
            headers = {
                "Authorization": "bearer %s" %token,
                "Content-Type": "application/json",
                "Cache-Control": "no-cache"
            }
            end_point = pac.namespace
            if action == 'sign':
                o = self.XmlCfdi()
                content = ElementTree.tostring(data, encoding='UTF-8', method='xml')
                if not email:
                    raise MissingError(_('The company email is required. Please config.'))
                body = {
                        "data": content,
                }
            else:  # cancel
                o = self.XmlCfdi()
                complement_obj = data.findall('{http://www.sat.gob.mx/cfd/3}Complemento')[0]
                if complement_obj is not None:
                    timbre = complement_obj.findall('{http://www.sat.gob.mx/TimbreFiscalDigital}TimbreFiscalDigital')[0]
                if timbre is not None:
                    uuid_sign = timbre.attrib.get('UUID')
                certificate_obj = self.env['res.company.facturae.certificate']
                comp_certificate = certificate_obj.search([
                    ('company_id','=',self.env.user.company_id.id)
                ])
                body = {
                    'uuid': uuid_sign,
                    'password': comp_certificate.certificate_password,
                    'rfc': rfc,
                    'b64Cer': o.encode64(comp_certificate.certificate_file),
                    'b64Key': o.encode64(comp_certificate.certificate_key_file)
                }
            return {
                'url': url_webservice,
                'end_point': end_point,
                'headers': headers,
                'body': json.dumps(body, ensure_ascii=False) if body else {},
                'method': method
            }

    @api.model
    def smarter_web_33_fail(self, **kwargs):
        response = kwargs.get('response')
        msg = "{}:".format(response.status)
        body = response.read()
        _logger.error(str(response.status))
        _logger.error(str(response.getheaders()))
        _logger.error(str(body))
        if re.search(r"application/json", response.getheader('content-type', default='')):
            data = json.loads(body)
            stage = data.get('stage', 'To-Do')
            message = data.get('message', 'To-Do')
            level = data.get('level', 0)
            code = data.get('code', 0)
            error_details = self.data2str(data.get('error_details', []))
            comp_error_details = self.data2str(data.get('comp_error_details', []))
            msg = "Code:{}: Level:{} Stage:{} \n {} \n {} \n {}".format(
                code,
                level,
                stage,
                message,
                error_details,
                comp_error_details
            )
        else:
            msg += body
        return msg

    @api.model
    def smarter_web_errors(self):
        pass

    @api.model
    def smarter_web_33_success(self, **kwargs):
        response = kwargs.get('response')
        action = kwargs.get('action')

        to_return = {
            'status_code': response.status,
        }
        body = response.read()
        _logger.info(str(response.status))
        _logger.info(str(response.getheaders()))
        _logger.info(str(body))
        if re.search(r"application/json", response.getheader('content-type', default='')):

            o = self.XmlCfdi()
            data = json.loads(body)
            if action == 'sign':
                cfdiXml = data.get('data').get('cfdi')
                uuid = data.get('data').get('uuid')
                cfdiDom = minidom.parseString(cfdiXml)
                cfdib64 = o.encode64(cfdiXml)
                to_return.update(
                    {
                        'contentb64': cfdib64,
                        'contentDom': cfdiDom,
                        'contentXml': cfdiXml,
                        'uuid': uuid,
                    })
            elif action == 'cancel':
                pass
        else:
            to_return.update({'body': body})
        return to_return

    @api.model
    def diverza_33(self, **kwargs):
        pac = kwargs.get('pac')
        action = kwargs.get('action')
        data = kwargs.get('data')
        doc = kwargs.get('doc')
        company = self.company().partner_id
        rfc = self.partner(company, 'rfc')
        email = self.partner(company, 'email')

        method = 'POST'

        headers = {
            "Content-Type": "application/json"
        }
        certificate_number = "{}".format(data.get('NoCertificado'))

        body = {
            "credentials": {
                "id": pac.user,
                "token": pac.password
            },
            "issuer": {
                "rfc": rfc
            },
            "document": {
                "certificate-number": certificate_number,
            }
        }

        if action == 'sign':
            fecha1 = data.get('Fecha').replace(":", "")
            fecha2 = fecha1.replace("-","")
            ref_id = "{}{}{}".format(data.get('TipoDeComprobante'), re.sub('\W+', '', data.get('Folio')),fecha2)
            o = self.XmlCfdi()
            content = ElementTree.tostring(data, encoding='UTF-8', method='xml')
            content = o.encode64(content)

            if re.search(r"complement", doc):
                _type = 'application/vnd.diverza.cfdi_3.3_complemento+xml'
            elif re.search(r"nomina", doc):
                _type = 'application/vnd.diverza.cfdi_3.3_complemento_nomina+xml'
            else:
                _type = "application/vnd.diverza.cfdi_3.3+xml"
            if not email:
                raise MissingError(_('The company email is required. Please config.'))
            body.update({
                "document": {
                    "ref-id": ref_id,
                    "certificate-number": certificate_number,
                    "section": "all",
                    "format": "xml",
                    "template": "letter",
                    "type": _type,
                    "content": content,
                },
                "receiver": {
                    "emails": [
                        # {
                        #     "email": email,
                        #     "format": "xml+pdf",
                        #     "template": "letter"
                        # }
                    ]
                }})
            end_point = pac.namespace
        else:  # cancel
            complement_obj = data.findall('{http://www.sat.gob.mx/cfd/3}Complemento')[0]
            if complement_obj is not None:
                timbre = complement_obj.findall('{http://www.sat.gob.mx/TimbreFiscalDigital}TimbreFiscalDigital')[0]
            if timbre is not None:
                uuid_sign = timbre.attrib.get('UUID')
            method = 'PUT'
            end_point = str(pac.namespace).replace('{uuid}', uuid_sign)
        return {
            'url': pac.url_webservice,
            'end_point': end_point,
            'headers': headers,
            'body': json.dumps(body, ensure_ascii=False),
            'method': method
        }

    @api.model
    def diverza_errors(self):
        pass

    @api.model
    def diverza_33_success(self, **kwargs):
        response = kwargs.get('response')
        action = kwargs.get('action')

        to_return = {
            'status_code': response.status,
        }
        body = response.read()
        _logger.info(str(response.status))
        _logger.info(str(response.getheaders()))
        _logger.info(str(body))
        if re.search(r"application/json", response.getheader('content-type', default='')):

            o = self.XmlCfdi()
            data = json.loads(body)
            if action == 'sign':
                content = data.get('content')
                uuid = data.get('uuid')
                ref_id = data.get('ref_id')
                contentXml = o.decode64(content)
                #path_xml = self.tmp_file()
                #o.write_file(contentXml, path_xml)
                #contentDom = o.str2domXml(path=path_xml)
                contentDom = minidom.parseString(contentXml)
                to_return.update(
                    {
                        'contentb64': content,
                        'contentDom': contentDom,
                        'contentXml': contentXml,
                        'uuid': uuid,
                        'ref_id': ref_id
                    })
            elif action == 'cancel':
                pass
        else:
            to_return.update({'body': body})
        return to_return

    @api.model
    def data2str(self, data):
        field_errors = ""
        if type(data) is list:
            for error in data:
                field_error = ""
                if type(error) is dict:
                    for field in error.keys():
                        val = error[field]
                        field_error += "{}: {}".format(field, val)
                elif type(error) is list:
                    for field in error:
                        field_error += "{}".format(field)
                else:
                    field_error = str(error)
                field_errors += field_error + "\n"
        elif type(data) is str:
            field_errors = data
        else:
            field_errors = str(data)
        return field_errors

    @api.model
    def diverza_33_fail(self, **kwargs):
        response = kwargs.get('response')
        msg = "{}:".format(response.status)
        body = response.read()
        _logger.error(str(response.status))
        _logger.error(str(response.getheaders()))
        _logger.error(str(body))
        if re.search(r"application/json", response.getheader('content-type', default='')):
            data = json.loads(body)
            stage = data.get('stage', 'To-Do')
            message = data.get('message', 'To-Do')
            level = data.get('level', 0)
            code = data.get('code', 0)
            error_details = self.data2str(data.get('error_details', []))
            comp_error_details = self.data2str(data.get('comp_error_details', []))
            msg = "Code:{}: Level:{} Stage:{} \n {} \n {} \n {}".format(
                code,
                level,
                stage,
                message,
                error_details,
                comp_error_details
            )
        else:
            msg += body
        return msg

    @api.model
    def sign_cfdi_xml(self, version, action, data, doc):
        select_pac = ''
        allowed_actions = ['sign', 'cancel']
        if action not in allowed_actions:
            raise MissingError(_('In the sign cfdi only it"s allowed the actions ({})'.format(str(allowed_actions))))
        allowed_docs = [
            'out_refund',
            'out_refund+complement',

            'out_invoice',
            'out_invoice+complement',

            'customer_advance_payment',
            'customer_advance_payment+complement',

            'customer_payment',
            'customer_payment+complement',
        ]
        if doc not in allowed_docs:
            raise MissingError(_('Only it"s allowed the next documents ({})'.format(str(allowed_docs))))
        if action is 'cancel':
            folio = data.attrib.get('Folio')
            tipo_comprobante = data.attrib.get('TipoDeComprobante')
            _type = False
            if tipo_comprobante == 'I':
                _type = 'out_invoice'
            elif tipo_comprobante == 'E':
                _type = 'out_refund'
            if tipo_comprobante == 'P':
                _type = 'payment'
            _logger.info('TIPO COMPROBANTE -- FOLIO')
            _logger.info(str(tipo_comprobante))
            _logger.info(str(folio))
            cfdi_sign_obj = self.env['cfdi.sign']
            cfdi_sign_id = cfdi_sign_obj.search([('account_invoice_id.number', '=', folio),('account_invoice_id.type', '=', _type)])
            pac_id = cfdi_sign_id.pac_id
            select_pac = pac_id.select_pac
            if _type=='payment':
                cfdi_sign_id = cfdi_sign_obj.search([('voucher_id.number', '=', folio)])
                pac_id = cfdi_sign_id.pac_id
                select_pac = pac_id.select_pac
            _logger.info(str(cfdi_sign_id))
            _logger.info(str(pac_id))
            _logger.info(str(select_pac))
        if version:
            if select_pac:
                pac_ids = self.env['params.pac'].search([
                    ('select_pac', '=', select_pac),
                    ('cfdi_version', '=', version.id),
                    ('method_type', '=', action),
                    ('active', '=', True)], order=' sequence asc')
            else:
                pac_ids = self.env['params.pac'].search([
                    ('cfdi_version', '=', version.id),
                    ('method_type', '=', action),
                    ('active', '=', True)], order=' sequence asc')
            print pac_ids
            if len(pac_ids):
                list_errors = []
                for pac_id in pac_ids:
                    try:
                        function_name = "{}_{}".format(pac_id.select_pac,
                                                       re.sub('\W+', '', pac_id.cfdi_version.version))
                        result = getattr(self, function_name)(pac=pac_id, action=action, data=data, doc=doc)
                        _logger.info(str(result))
                        connection = httplib.HTTPSConnection(result.get('url'))
                        connection.request(
                            result.get('method'),
                            result.get('end_point'),
                            result.get('body'),
                            result.get('headers')
                        )
                        response = connection.getresponse()
                        if response.status not in [200, 201]:
                            msg = "{} \n {}".format(pac_id.name,
                                                    getattr(self, "{}_fail".format(function_name))(response=response))
                            list_errors.append(msg)
                        else:
                            result = getattr(self, "{}_success".format(function_name))(response=response, action=action)
                            result.update({'pac_id': pac_id.id})
                            return result
                    except Exception as e:
                        list_errors.append(e.message)
                missing_error = ""
                for error in list_errors:
                    missing_error += error + '\n'
                    raise MissingError(missing_error)
            else:
                raise MissingError(_('Configure your PACs'))
        else:
            raise MissingError(_('Configure your cfdi version in journal!'))

    @api.model
    def tmp_file(self, extension='xml'):
        return self.path_tmp() + self.uuid() + '.' + extension

    def generate_cfdi_sello(self, cr=False, uid=False, ids=False, context=None):
        # TODO: Put encrypt date dynamic
        if context is None:
            context = {}
        fecha = context['fecha']
        year = float(time.strftime('%Y', time.strptime(
            fecha, '%Y-%m-%dT%H:%M:%S')))
        if year >= 2011:
            encrypt = "sha1"
        if year <= 2010:
            encrypt = "md5"
        certificate_lib = self.pool.get('facturae.certificate.library')
        fname_sign = certificate_lib.b64str_to_tempfile(cr, uid, ids, base64.encodestring(
            ''), file_suffix='.txt', file_prefix='openerp__' + (False or '') + \
                                                 '__sign__')
        result = certificate_lib._sign(cr, uid, ids, fname=context['fname_xml'],
                                       fname_xslt=context['fname_xslt'], fname_key=context['fname_key'],
                                       fname_out=fname_sign, encrypt=encrypt, type_key='PEM')
        return result

    def get_sello_to_cfdi(self, cr, uid, ids, xml_obj, context=None):
        # parsing to object xml to string
        doc_xml_tree = ElementTree.tostring(xml_obj, 'utf-8')
        # parse to minidom object
        doc_xml_dom = minidom.parseString(doc_xml_tree)
        # get date to comprobante
        date = doc_xml_dom.getElementsByTagName('cfdi:Comprobante')[0]
        # set date to context
        date_value = date.getAttributeNode('Fecha').value.encode('utf-8')
        context.update({'fecha': '2017-10-24T21:00:11'})
        invoice_number = "sn"
        (fileno_xml, fname_xml) = tempfile.mkstemp(
            '.xml', 'openerp_' + (invoice_number or '') + '__facturae__')
        fname_txt = fname_xml + '.txt'
        f = open(fname_xml, 'w')
        doc_xml_dom.writexml(
            f, indent='    ', addindent='    ', newl='\r\n', encoding='UTF-8')
        f.close()
        os.close(fileno_xml)
        (fileno_sign, fname_sign) = tempfile.mkstemp('.txt', 'openerp_' + (
            invoice_number or '') + '__facturae_txt_md5__')
        os.close(fileno_sign)
        context.update({
            'fname_xml': fname_xml,
            'fname_txt': fname_txt,
            'fname_sign': fname_sign,
        })
        # call method to generate stamp
        sello_str = self.generate_cfdi_sello(
            cr=False, uid=False, ids=False, context=context)
        if not sello_str:
            raise Warning(_('Error in Stamp !'), _(
                "Can't generate the stamp of the voucher"))
        return sello_str

    @api.model
    def certificate(self):
        file_globals = {}
        certificate_ids = self.env['res.company.facturae.certificate'].search([('active', '=', True)])
        xml_cfdi = self.XmlCfdi()
        for certificate_id in certificate_ids:
            try:
                data_pem = xml_cfdi.decode64(certificate_id.certificate_file_pem)
                path_pem = self.path_tmp() + self.uuid() + '.pem'
                xml_cfdi.write_file(data_pem, path_pem)
            except:
                raise MissingError(_('Not captured a CERTIFICATE file in format PEM, in the company!'))
            file_globals['pem'] = path_pem
            try:
                data_key = xml_cfdi.decode64(certificate_id.certificate_key_file_pem)
                path_key = self.path_tmp() + self.uuid() + '.pem.key'
                xml_cfdi.write_file(data_key, path_key)
            except:
                raise MissingError(_('Not captured a KEY file in format PEM, in the company!'))
            file_globals['pem_key'] = path_key
            path_cer = False
            try:
                data_cer = xml_cfdi.decode64(certificate_id.certificate_file)
                file_globals['cer_base64'] = certificate_id.certificate_file
                path_cer = self.path_tmp() + self.uuid() + '.cer'
                xml_cfdi.write_file(data_cer, path_cer)
            except:
                pass
            file_globals['cer'] = path_cer
            path_cer_key = False
            try:
                data_cer_key = xml_cfdi.decode64(certificate_id.certificate_key_file)
                path_cer_key = self.path_tmp() + self.uuid() + '.cer.key'
                xml_cfdi.write_file(data_cer_key, path_cer_key)
            except Exception, e:
                pass
            file_globals['cer_key'] = path_cer_key

            file_globals['password'] = certificate_id.certificate_password

            """if certificate_id.fname_xslt:
                if (certificate_id.fname_xslt[
                        0] == os.sep or certificate_id.fname_xslt[
                        1] == ':'):
                    file_globals['fname_xslt'] = certificate_id.fname_xslt
                else:
                    file_globals['fname_xslt'] = os.path.join(
                        tools.config["root_path"],
                        certificate_id.fname_xslt)
            else:
                # Search char "," for addons_path, now is multi-path
                all_paths = tools.config["addons_path"].split(",")
                for my_path in all_paths:
                    if os.path.isdir(os.path.join(
                            my_path, 'l10n_mx_facturae', 'SAT')):
                        # If dir is in path, save it on real_path
                        file_globals[
                            'fname_xslt'] = my_path and os.path.join(
                            my_path, 'l10n_mx_facturae', 'SAT',
                            'cadenaoriginal_2_0_l.xslt') or ''
                        break
            if not file_globals.get('fname_xslt', False):

                raise Warning(_('Warning !'), _(
                    'Not defined fname_xslt. !'))

            if not os.path.isfile(file_globals.get('fname_xslt', ' ')):
                raise Warning(_('Warning !'), _(
                    'No exist file [%s]. !') % (file_globals.get(
                        'fname_xslt', ' ')))
            """

            file_globals['serial_number'] = certificate_id.serial_number
            return file_globals
        raise MissingError(_('Can\'t get the Certificate. Ckeck your configuration.'))

    # call this function to get certificates
    def get_certifacates_company(self, cr, uid, ids, context=None):
        """
        @param fname_cer : Path more name of file created whit information
                    of certificate with suffix .pem
        @param pem : Boolean that indicate if file is .pem
        """
        context = dict(context)
        certificates = {}
        invoice_obj = self.pool.get('account.invoice')
        context.update(self.certificate(cr, uid))
        # get certicate om str
        cert_str = context.get('cer_base64')
        if not cert_str:
            raise Warning(
                _('Error in Certificate!'),
                _("Can't generate the Certificate of the voucher.\n"
                  "Ckeck your configuration."))
        cert_str = cert_str.replace(' ', '').replace('\n', '')
        certificates['certificate_str'] = cert_str or False
        # get num certificate
        noCertificado = context.get('serial_number')
        certificates['nocertificate'] = noCertificado or False
        if not noCertificado:
            raise Warning(
                _('Error in No. Certificate !'),
                _("Can't get the Certificate Number of the voucher.\n"
                  "Ckeck your configuration.")
            )
        return certificates

    @api.model
    def company(self):
        company = self.env['res.company']._company_default_get('cfdi')
        return self.env['res.company'].browse(company)

    @api.model
    def expedition_place(self, journal):
        partner = journal.address_invoice_company_id
        if partner:
            val = self.partner(partner, 'zip')
        else:
            raise MissingError(_('The journal {} is necessary config address invoice company'.format(journal.name)))
        return val.name or None

    @api.model
    def partner(self, partner, attr):

        def is_foreign():
            _isfor = False
            if partner.country_id.code != 'MEX':
                _isfor = True
            return _isfor

        val = None
        if partner:
            if attr == 'city':
                if partner.city_id:
                    val = partner.city_id.name
            elif attr == 'state':
                if partner.state_id:
                    val = partner.state_id.name
            elif attr == 'country':
                if partner.country_id:
                    val = partner.country_id.name
            elif attr == 'country_code':
                if partner.country_id:
                    val = partner.country_id.code
            elif attr == 'regime':
                if partner.regimen_fiscal_id:
                    val = partner.regimen_fiscal_id.code
            elif attr == 'rfc':
                val = self.rfc(partner.vat)
            elif attr == 'residential':
                if is_foreign():
                    if partner.country_id:
                        val = self.partner(partner, 'country_code')
                    else:
                        raise MissingError(_('The partner is foreign, is required set country'))
            elif attr == 'foreign_identity':
                if is_foreign():
                    val = self.partner(partner, 'rfc')
            else:
                val = getattr(partner, attr, None)
        return val

    @api.model
    def rfc(self, rfc):
        if rfc:
            rfc = rfc[2:]
        else:
            raise MissingError(_('Not have RFC partner assigned'))
        return rfc or None

    """
        Node xml commun for Cfdi 3.3
    """

    @api.model
    def issue_datetime(self, src_dt, dst_tz=None):
        if dst_tz is None:
            dst_tz = self.env.context.get('tz')
        if src_dt:
            _fmt_iso = DATETIME_FORMATS_MAP.get('%F') + 'T' + DATETIME_FORMATS_MAP.get('%T')
            _issue_datetime = server_to_local_timestamp(src_dt, DATETIME_FORMATS_MAP['%+'], _fmt_iso, dst_tz)
        else:
            raise MissingError(_('This field {} is required'.format(src_dt.__name__)))
        return _issue_datetime or None

    @api.model
    def receiver(self, partner):
        receptor = Receptor()
        if partner:
            receptor.Rfc.value = self.partner(partner, 'rfc')
            receptor.Nombre.value = self.partner(partner, 'name')
            receptor.ResidenciaFiscal.value = self.partner(partner, 'residential')
            receptor.NumRegIdTrib.value = self.partner(partner, 'foreign_identity')
            # receptor.UsoCFDI.value =
        return receptor

    @api.model
    def issuing(self, partner=None):
        emisor = Emisor()
        if partner is None:
            company = self.company()
            if company:
                partner = company.partner_id
        if partner:
            emisor.Rfc.value = self.partner(partner, 'rfc')
            emisor.Nombre.value = self.partner(partner, 'name')
            emisor.RegimenFiscal.value = self.partner(partner, 'regime')
        return emisor

    @api.model
    def issuing_address(self, issuing):
        _issuing = DomicilioEmisor()
        if issuing:
            _issuing.calle.value = self.partner(issuing, 'street')
            _issuing.codigoPostal.value = self.partner(issuing, 'zip')
            _issuing.colonia.value = self.partner(issuing, 'street2')
            _issuing.estado.value = self.partner(issuing, 'state')
            _issuing.municipio.value = self.partner(issuing, 'l10n_mx_city2')
            _issuing.pais.value = self.partner(issuing, 'country_code')
        return _issuing

    @api.model
    def receiver_address(self, receiver):
        _receiver = DomicilioReceptor()
        if receiver:
            _receiver.calle.value = self.partner(receiver, 'street')
            _receiver.codigoPostal.value = self.partner(receiver, 'zip')
            _receiver.colonia.value = self.partner(receiver, 'street2')
            _receiver.estado.value = self.partner(receiver, 'state')
            _receiver.municipio.value = self.partner(receiver, 'l10n_mx_city2')
            _receiver.pais.value = self.partner(receiver, 'country_code')
            _receiver.noExterior.value = self.partner(receiver, 'l10n_mx_street3')
            _receiver.noInterior.value = self.partner(receiver, 'l10n_mx_street4')
        return _receiver

    @api.model
    def sign(self, version, path_xml, path_pem):
        x = self.XmlCfdi()
        original_string = x.xml_transform_xslt(version, path_xml, to_return='path')
        return x.xml_sign(original_string, path_pem)

    @api.model
    def path_tmp(self):
        return '/tmp/'

    @api.model
    def uuid(self):
        return str(uuid.uuid4())

    @api.model
    def XmlCfdi(self):
        return XmlCfdi()

    @api.model
    def Comprobante(self):
        return Comprobante()

    @api.model
    def Concepto(self):
        return Concepto()

    @api.model
    def Impuestos(self):
        return Impuestos()

    @api.model
    def Traslado(self):
        return Traslado()

    @api.model
    def Traslados(self):
        return Traslados()

    @api.model
    def Conceptos(self):
        return Conceptos()

    @api.model
    def Addenda(self):
        return Addenda()

    @api.model
    def Folios(self):
        return Folios()

    @api.model
    def Pago(self):
        return Pago()

    @api.model
    def Pagos(self):
        return Pagos()

    @api.model
    def DoctoRelacionado(self):
        return DoctoRelacionado()

    @api.model
    def Complemento(self):
        return Complemento()

    @api.model
    def CfdiRelacionados(self):
        return CfdiRelacionados()

    @api.model
    def CfdiRelacionado(self):
        return CfdiRelacionado()
