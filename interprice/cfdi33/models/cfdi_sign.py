# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import fields, models, api, _
from openerp.exceptions import MissingError
import base64
import urllib
from xml.dom import minidom
from openerp.addons.l10n_mx_invoice_amount_to_text import amount_to_text_es_MX


class CfdiSign(models.Model):
    """new model for register signs"""
    _name = 'cfdi.sign'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'uuid'
    state_list = [
        ('not_signed', _('Not signed')),
        ('signed', _('Signed')),
        ('pending', _('Pending')),
        ('cancelled', _('Canceled')),
    ]
    original_string = fields.Text(string='Original string')
    certificate_number = fields.Char(string='Certificate number')
    certificate = fields.Text(string='Certificate')
    stamp = fields.Text(string='Stamp')
    num_partiality = fields.Integer(
        help="indicates the sequence of the payment bias number")
    signature_cancellation_date = fields.Datetime(string='Signature cancellation date')
    signature_date = fields.Datetime(string='Signature date')
    signed_original_string = fields.Text('Signed original string')
    uuid = fields.Char(string='Uuid')
    # certificate_number_sing = fields.Char(string='Certificate number sign')
    stamp_signed = fields.Text(string='Stamp signed')
    xml = fields.Binary(string='Xml')
    xml_signed = fields.Binary(string='Xml signed')
    pdf_signed = fields.Binary(string='Pdf')
    move_id = fields.Many2one(comodel_name='account.move')
    voucher_id = fields.Many2one(
        comodel_name='account.voucher',
        help='Establish relation to the payment')
    xml_filename = fields.Char(_('file name'), invisible=True, compute='_xml_filename')
    pdf_filename = fields.Char(_('file name'), invisible=True, compute='_xml_filename')
    state_document = fields.Selection(selection=state_list, default='not_signed', )
    account_invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Invoice',
        help='Establish relation to Invoice')
    pac_id = fields.Many2one(
        comodel_name='params.pac',
        string='Pac',
        readonly=True
    )

    # send_by_email = fields.Boolean(string='Send by email')

    @api.multi
    def _pdf_filename(self):
        pass

    @api.multi
    @api.onchange('xml_signed', 'pdf_signed')
    def _xml_filename(self):
        for record in self:
            _name = self._filename(record=record)
            record.xml_filename = str(_name) + ".xml"
            record.pdf_filename = str(_name) + ".pdf"

    @api.model
    def _filename(self, record):
        return record.uuid

    # Params for send email
    @api.multi
    def send_by_email(self):
        email_template_ids = self.env['email.template'].search([
            ('model_id.model', '=', 'cfdi.sign'),
            ('name', '=', 'Doc cfdi signed')
        ])

        context = self._context.copy()
        if context is None:
            context = {}
        for record in self:
            _company = record._company()
            if _company:
                context.update(_company)

            _partner = record._partner()
            if _partner:
                context.update(_partner)

            if len(email_template_ids) == 1:
                attachment_list = []

                if record.xml_signed:
                    att_xml = {
                        'name': record.xml_filename,
                        'datas_fname': record.xml_filename,
                        'datas': record.xml_signed
                    }
                    attachment_xml = self.env['ir.attachment'].create(att_xml)
                    attachment_list.append(attachment_xml.id)

                if record.pdf_signed:
                    att_pdf = {
                        'name': record.pdf_filename,
                        'datas_fname': record.pdf_filename,
                        'datas': record.pdf_signed
                    }
                    attachment_pdf = self.env['ir.attachment'].create(att_pdf)
                    attachment_list.append(attachment_pdf.id)

                values = self.pool('email.template').generate_email(
                    self.env.cr,
                    self.env.uid,
                    template_id=email_template_ids.id,
                    res_id=email_template_ids.id,
                    context=context
                )
                values.update({'attachment_ids': [(6, 0, attachment_list)]})
                values['res_id'] = record.id
                # print values
                mail_mail_id = self.env['mail.mail'].create(values)
                if len(mail_mail_id):
                    mail_mail_id.send()
            elif len(email_template_ids) > 1:
                raise MissingError(_('Exist many templates with model cfdi.sign, will been only one template. Please remove others.'))
            else:
                raise MissingError(_('Not exist template with ref-model cfdi.sign.'))

    @api.model
    def _company(self):
        company_id = self.get_company()
        _to_return = {
            'company_name': company_id.name or None,
            'company_email': company_id.email or None,
            'company_web': None
        }

        return _to_return

    @api.model
    def get_company(self, filter=None):
        return self.env.user.company_id

    @api.model
    def _partner(self):
        _to_return = {
            'partner_email': None,
            'partner_lang': None,
        }
        partner_id = None

        if self.account_invoice_id:
            partner_id = self.env['account.invoice'].\
                browse(self.account_invoice_id.id).partner_id
        elif self.voucher_id:
            partner_id = self.voucher_id.partner_id

        if partner_id:
            _to_return.update({
                'partner_email': partner_id.email or None,
                'partner_lang': partner_id.lang or None,
            })
        return _to_return

    @api.multi
    def refresh_pdf(self):
        for record in self:
            if record.xml_signed:
                try:
                    result = self.env['report'].get_pdf(record, 'cfdi33.cfdi_signed_report_pdf_id')
                    pdf64 = base64.encodestring(result)
                    record.update({'pdf_signed': pdf64})
                except Exception as e:
                    print e
            else:
                raise MissingError(_("Doesn't exist an xml file signed."))

    @api.multi
    def refresh_pdf_payment(self):
        for record in self:
            if record.xml_signed:
                result = self.env['report'].get_pdf(record, 'cfdi33.cfdi_signed_report_pdf_id_payment')
                pdf64 = base64.encodestring(result)
                record.update({'pdf_signed': pdf64})
            else:
                raise MissingError(_("Doesn't exist an xml file signed."))

    @api.multi
    def xml_dom_obj(self):
        cfdi = self.env['cfdi33']
        xml_cfdi = cfdi.XmlCfdi()
        cfdi_path = cfdi.tmp_file()
        cfdi_print = xml_cfdi.decode64(self.xml_signed)
        xml_cfdi.write_file(cfdi_print, cfdi_path)
        data_xml = minidom.parseString(cfdi_print)

        return data_xml


    @api.multi
    def qr_code_make(self):
        data_xml = self.xml_dom_obj()
        url = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx%3F"
        node1 = data_xml.firstChild
        if node1:
            total = node1.getAttributeNode('Total').value.encode('utf-8')
            rfc_emisor = ''
            node_emisor = node1.getElementsByTagName('cfdi:Emisor')
            if node_emisor:
                rfc_emisor = node_emisor[0].getAttributeNode('Rfc').value.encode('utf-8')

            rfc_receptor = ''
            node_receptor = node1.getElementsByTagName('cfdi:Receptor')
            if node_emisor:
                rfc_receptor = node_receptor[0].getAttributeNode('Rfc').value.encode('utf-8')

            uuid = ''
            sello_sat = ''
            node_complemento = node1.getElementsByTagName('cfdi:Complemento')
            if node_complemento:
                node_tfd = node_complemento[0].getElementsByTagName('tfd:TimbreFiscalDigital')
                if node_tfd:
                        uuid = node_tfd[0].getAttributeNode('UUID').value.encode('utf-8')
                        sello_sat = node_tfd[0].getAttributeNode('SelloSAT').value.encode('utf-8')
                        sello_sat = sello_sat[-8:]

        qr_str = url + 're=' + rfc_emisor + '&rr=' + rfc_receptor + '&tt=' +\
            total + '&id=' + uuid + '&fe=' + sello_sat
        qr_str_encode = urllib.quote_plus(qr_str)

        return qr_str_encode

    @api.model
    def typeDocument(self):
        _type_document = ''

        for record in self:
            if self.voucher_id:
                type_doc = self.voucher_id.journal_id.\
                    account_account_apply_id.code
                if type_doc == 'advance':
                    _type_document = 'Nota de Anticipo'
                else:
                    _type_document = 'Pago'

            elif self.advance_payment_id:
                _type_document = 'Anticipo'

            elif self.account_invoice_id:
                invoice_id = self.env['account.invoice'].\
                    browse(record.account_invoice_id.id)
                if invoice_id.type == 'out_refund':
                    _type_document = 'Nota de crédito'
                elif invoice_id.type == 'out_invoice':
                    _type_document = 'Factura'

        return _type_document

    @api.model
    def getRegimenFiscal(self, regimen):
        _regimen_fiscal = self.env['regimen.fiscal'].\
            search([('code', '=', regimen)]).name
        return _regimen_fiscal

    @api.model
    def hasAttribute(self, dom, attribute, default=''):
        _value = default
        if dom is not None and dom.hasAttribute(attribute):
            _value = dom.getAttributeNode(attribute).value
        return _value

    def addCommaToMoney(self, cantidad):
        if cantidad:
            _value = '$ {:,.2f}'.format(float(cantidad))
        return _value

    def _amount_to_text(self, num, currency):
        """Function to parse the total amount to text.

        Arguments:
            num {float} -- Data type to can pass the number
                            to text with decimal.
            currency {text} -- Name currency

        Returns:
            [text] -- data with the number pass to text
        """
        num = float(num)
        partner = self._partner().get('partner_lang')
        if partner:
            partner_lang = partner.encode('utf-8')
        else:
            partner_lang = self._context.get('lang')

        amount_to_text = amount_to_text_es_MX.\
            get_amount_to_text(self, num, partner_lang, currency)
        #validate type data string amount
        if type(amount_to_text) == unicode:
            amount = amount_to_text.encode('utf-8')
        elif type(amount_to_text) == str:
            amount = amount_to_text
        else:
            amount = amount_to_text
        # replace USD or MXN
        if 'USD' in (amount):
            amount_to = amount.replace('USD', '').replace(
                'M. E.', 'USD')
        else:
            amount_to = amount.replace('PESOS', '').replace(
                'M. N.', 'MXN')
        return amount_to
