# -*- coding: utf-8 -*-
# Copyright  2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _
from openerp.exceptions import MissingError, Warning
import re
from openerp.tools import float_round
from decimal import Decimal, ROUND_DOWN
# Here are all your options for rounding:
# This one offers the most out of the box control
# ROUND_05UP       ROUND_DOWN       ROUND_HALF_DOWN  ROUND_HALF_UP
# ROUND_CEILING    ROUND_FLOOR      ROUND_HALF_EVEN  ROUND_UP
from lxml import etree
import time

class AccountVoucher(models.Model):
    """ Model extend for add use cfdi field"""

    _inherit = 'account.voucher'

    cfdi = None
    docs_rel = None

    def onchange_partner_id(
            self, cr, uid, ids, partner_id, journal_id, amount,
            currency_id, ttype, date, context=None):
        # partner_accounts = [account.id for account in self.partner_id.res.partner.bank]
        if context is None:
            context = dict()
        res = super(AccountVoucher, self).onchange_partner_id(
            cr, uid, ids, partner_id, journal_id, amount,
            currency_id, ttype, date, context)
        if partner_id:
            partner_obj = self.pool.get('res.partner').browse(
                cr, uid, partner_id)
            partner_banks = partner_obj.bank_ids
            partner_banks_list = [bank.id for bank in partner_banks]
            if isinstance(res, dict) and not res.get('domain', False):
                res.update({'domain': {'account_application': [
                    ('id', 'in', partner_banks_list)]}})
            elif isinstance(res, dict) and res.get('domain', False):
                res['domain'].update({'account_application': [
                    ('id', 'in', partner_banks_list)]})
            else:
                raise TypeError('Module: CFDI33, Error in Account number\
                    super call, res must be a dictionary')
        return res

    def onchange_journal(
        self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id,
        date, amount, ttype, company_id, context=None):
        pay_obj = self.pool.get('account.voucher')
        journal_obj = self.pool.get('account.journal')
        validate_apply_journal = ['advance', 'refund']
        state_jouranl_opt = ['cash', 'refund', 'advance']
        res = super(AccountVoucher, self).onchange_journal(
            cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount,
            ttype, company_id, context=context)
        journal = journal_obj.browse(cr, uid, journal_id, context=context)
        if journal:
            apply_id_code = journal.account_account_apply_id.code
            if apply_id_code not in state_jouranl_opt:
                res['value'].update({'state_journal': True,})
            else:
                res['value'].update({'state_journal': False,})
            if apply_id_code in validate_apply_journal:
                res['value'].update({'apply_complement': True,})
            else:
                res['value'].update({'apply_complement': False,})
        return res

    @api.model
    def fields_view_get(
        self, view_id=None, view_type=False,
            toolbar=False, submenu=False):
        res = super(AccountVoucher, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            company_banks = self.env.user.company_id.bank_ids
            company_bank_ids = [bank.id for bank in company_banks]
            # validate when in form view
            doc = etree.XML(res['arch'])
            nodes = doc.xpath("//field[@name='bank_dst']")
            # get field from view
            for node in nodes:
                # set domain value on view
                node.set('domain', "[('id', '=', {})]".format(
                    company_bank_ids))
            # write node in view xml and return view
            res['arch'] = etree.tostring(doc)
        return res

    state_list = [
        ('not_signed', _('Not signed')),
        ('signed', _('Signed')),
        ('cancelled', _('Canceled')),
    ]

    state_journal = fields.Boolean(store=True)
    cfdi_sign_ids = fields.One2many(
        'cfdi.sign', 'voucher_id')

    account_application = fields.Many2one(
        comodel_name="res.partner.bank",
        required=False,
        default=False,
        string=_("Partner bank"),
        help=_("Select account"),
        copy=False,
    )

    bank_dst = fields.Many2one(
        comodel_name="res.partner.bank",
        required=False,
        string=_("Destination bank"),
        help=_("Select destination bank"),
        copy=False,
    )

    is_spei = fields.Boolean(
        string=_('Is SPEI'),
        required=False,
        readonly=False,
        index=False,
        default=False,
        help=_("Select if the transfer is SPEI type")
    )

    spei_certificate = fields.Text(
        string=_('SPEI certificate'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        size=512,
        help=_("Input SPEI certificate")
    )

    spei_chain = fields.Text(
        string=_('SPEI chain'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        size=512,
        help=_("Input SPEI chain")
    )

    spei_signature = fields.Text(
        string=_('SPEI signature'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        size=512,
        help=_("Input SPEI signature")
    )
    cfdi_version = fields.Many2one(comodel_name="cfdi.version")
    apply_complement = fields.Boolean(store=True)
    state_document = fields.Selection(selection=state_list, default='not_signed', nocopy=True)
    cancel_wcfdi = fields.Boolean(
        string="Not Use CFDI", help="Set True to don't use CFDI sign"
    )

    @api.model
    def comprobante_node_xml(self):
        comprobante_node = self.cfdi.Comprobante()
        subtotal = "0"
        currency = "XXX"
        total = "0"
        voucher_type = "P"
        certificate_dic = self.cfdi.get_certifacates_company()
        serie = self.journal_id.sequence_id.approval_id.serie
        date_now = fields.Datetime.now()
        tz = self.env.context.get('tz')
        date_sign = self.cfdi.issue_datetime(date_now, tz)
        expedition_place = self.cfdi.expedition_place(self.journal_id)
        no_certificado = self.company_id.certificate_id.serial_number
        version = self.journal_id.cfdi_version.version
        comprobante_node.Certificado.value = certificate_dic.get(
            'certificate_str') or None
        comprobante_node.Version.value = version or None
        comprobante_node.Serie.value = serie or None
        comprobante_node.Folio.value = self.number or None
        comprobante_node.Fecha.value = date_sign or None
        comprobante_node.NoCertificado.value = no_certificado or None
        comprobante_node.SubTotal.value = subtotal or None
        comprobante_node.Moneda.value = currency or None
        comprobante_node.Total.value = total or None
        comprobante_node.TipoDeComprobante.value = voucher_type or None
        comprobante_node.LugarExpedicion.value = expedition_place or None
        return comprobante_node

    @api.model
    def get_emisor_node_xml(self):
        emisor_node = self.cfdi.issuing(self.company_id.partner_id)
        # set regimen fiscal
        emisor_node.RegimenFiscal.value = "601"
        return emisor_node

    @api.model
    def get_receptor_node_xml(self):
        receptor_node = self.cfdi.receiver(self.partner_id)
        receptor_node.UsoCFDI.value = "P01"
        return receptor_node

    @api.model
    def nodo_conceptos_xml(self):
        nodo_concepto = self.cfdi.Concepto()
        nodo_conceptos = self.cfdi.Conceptos()
        clave_prod_serv = "84111506"
        cantidad = "1"
        clave_unidad = "ACT"
        descripcion = "Pago"
        valor_unitario = "0"
        importe = "0"
        nodo_concepto.ClaveProdServ.value = clave_prod_serv
        nodo_concepto.Cantidad.value = cantidad
        nodo_concepto.ClaveUnidad.value = clave_unidad
        nodo_concepto.Descripcion.value = descripcion
        nodo_concepto.ValorUnitario.value = valor_unitario
        nodo_concepto.Importe.value = importe
        nodo_conceptos.Concepto.value = [nodo_concepto]
        return nodo_conceptos

    @api.model
    def complement_node(self):
        complement_node = self.cfdi.Complemento()
        node_payments = [self.get_payments_node()]
        complement_node.Pagos.value = node_payments
        return complement_node

    @api.model
    def get_payments_node(self):
        cfdi_sign = self.env['cfdi.sign']
        invoice_obj = self.env['account.invoice']
        tz = self.env.context.get('tz')
        pay_node = self.cfdi.Pago()
        payments_node_xml = self.cfdi.Pagos()
        payments_node_xml.Version.value = "1.0"
        if not self.journal_id.payment_method:
            raise Warning(_('Configure payment method in journal to proceed to sign the pay'))
        if not self.journal_id.account_account_apply_id:
            raise Warning(_('Configure journal apply in the journal to proceed to sign pay'))
        payments_forms_cta_ord_rfc = ['03', '04', '02', '99']
        payments_forms_cta_ord = ['02', '03', '04', '05', '06', '28', '29']
        payments_forms_cta_ben = ['02', '03', '04', '05', '28', '29']
        forma_de_pago = self.journal_id.payment_method.code
        if not forma_de_pago:
            raise Warning(_('Configure pay method in journal!'))
        moneda_p = self.journal_id.currency or self.company_id.currency_id
        rfc_emisor_cta_ord = self.account_application.bank.rfc
        nom_banco_ord_ext = self.account_application.bank_name
        cta_ordenante = self.account_application.acc_number
        pay_node.CtaOrdenante.value = cta_ordenante or None
        rfc_emisor_cta_ben = self.bank_dst.bank.rfc
        pay_node.CtaBeneficiario.value = self.bank_dst.acc_number or None
        if self.journal_id.payment_method.code in payments_forms_cta_ord_rfc:
            pay_node.RfcEmisorCtaOrd.value = rfc_emisor_cta_ord or None
            if pay_node.RfcEmisorCtaOrd.value == 'XEXX010101000':
                pay_node.NomBancoOrdExt.value = nom_banco_ord_ext or None
        if self.journal_id.payment_method.code in payments_forms_cta_ben:
            pay_node.RfcEmisorCtaBen.value = rfc_emisor_cta_ben or None
        if not self.received_third_check_ids:
            num_operacion = self.name
        else:
            for l in self.received_third_check_ids:
                if l.number:
                    num_operacion = l.number
                    break
                else:
                    pass
        pay_node.NumOperacion.value = num_operacion or None
        monto = self.amount_readonly
        pay_node.FormaDePagoP.value = forma_de_pago or None
        pay_node.MonedaP.value = moneda_p.name or None
        rate_document=0.0
        rate_pay=0.0
        if moneda_p.id == self.company_id.currency_id.id:
            monto = "{0:.2f}".format(float_round(monto, 6))
            pay_node.TipoCambioP.value = None
        else:
            monto = "{0:.2f}".format(float_round(monto, 6))
            for rate in moneda_p.rate_ids:
                if rate.name == self.move_id.create_date:
                    rate_document = float_round(1 / rate.rate_sale, 6)
                    pay_node.TipoCambioP.value = rate_document
                    break
                else:
                    pay_node.TipoCambioP.value = float_round(1 / moneda_p.rate_sale_silent, 6)
                    break
        pay_node.Monto.value = monto or None
        if pay_node.FormaDePagoP.value == '03':
            if self.is_spei:
                pay_node.TipoCadPago.value = '01'
                pay_node.CertPago.value = self.spei_certificate or None
                pay_node.CadPago.value = self.spei_chain or None
                pay_node.SelloPago.value = self.spei_signature or None
        nodo_docto_relacionado = []
        document_no_sign = []
        move_line_obj = self.env['account.move.line']
        if self.line_cr_ids:
            for line in self.line_cr_ids:
                doc_rel_node = self.cfdi.DoctoRelacionado()
                if line.amount:
                    document_id = cfdi_sign.search([
                        ('move_id', '=', line.move_line_id.move_id.id)], limit=1)
                    if document_id:
                        line_balance = 0.0
                        invoice_id = invoice_obj.search([
                            ('move_id', '=', document_id.move_id.id)], limit=1)
                        # validate currency doc rel
                        if not line.original_currency:
                            moneda_dr = self.company_id.currency_id
                        else:
                            moneda_dr = line.original_currency
                        if moneda_p.id == moneda_dr.id:
                            doc_rel_node.TipoCambioDR.value = None
                            if moneda_p.id!= self.company_id.currency_id.id:
                                if invoice_id.pay_method_sat_id.code == 'PPD':
                                    num_pay = document_id.num_partiality
                                    if num_pay > 0:
                                        num_pay = num_pay + 1
                                        document_id.num_partiality = num_pay
                                        doc_rel_node.NumParcialidad.value = str(num_pay)
                                        doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line.original_balance, 6))) or None
                                        doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(line.amount, 6))) or None
                                        doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round(line.original_balance - line.amount, 6))) or None
                                    else:
                                        num_pay = 1
                                        document_id.num_partiality = num_pay
                                        doc_rel_node.NumParcialidad.value = str(num_pay)
                                        doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line.original_balance, 6))) or None
                                        doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(line.amount, 6))) or None
                                        doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round(line.original_balance - line.amount, 6))) or None
                                else:
                                    doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(line.amount,6))) or None
                            else:
                                if invoice_id.pay_method_sat_id.code == 'PPD':
                                    num_pay = document_id.num_partiality
                                    if num_pay > 0:
                                        num_pay = num_pay + 1
                                        document_id.num_partiality = num_pay
                                        doc_rel_node.NumParcialidad.value = str(num_pay)
                                        doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line.original_balance, 6))) or None
                                        doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(line.amount, 6))) or None
                                        doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round(line.original_balance - line.amount, 6))) or None
                                    else:
                                        num_pay = 1
                                        document_id.num_partiality = num_pay
                                        doc_rel_node.NumParcialidad.value = str(num_pay)
                                        doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line.original_balance, 6))) or None
                                        doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(line.amount, 6))) or None
                                        doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round(line.original_balance - line.amount, 6))) or None
                                else:
                                    doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(line.amount,6))) or None
                        #    moneda p       moneda comapa�ia                      moneda dr
                        elif moneda_p.id != self.company_id.currency_id.id and moneda_dr.id == self.company_id.currency_id.id:
                            doc_rel_node.TipoCambioDR.value = "1"
                            tip_cambio_p = pay_node.TipoCambioP.value
                            if invoice_id.pay_method_sat_id.code == 'PPD':
                                    num_pay = document_id.num_partiality
                                    if num_pay > 0:
                                        num_pay = num_pay + 1
                                        document_id.num_partiality = num_pay
                                        imp_pay = tip_cambio_p * float("{0:.2f}".format(line.amount))
                                        pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                        doc_rel_node.NumParcialidad.value = str(num_pay)
                                        doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line.original_balance, 6))) or None
                                        doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(float(pay), 6))) or None
                                        doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round((line.original_balance - float(pay)),6))) or None
                                    else:
                                        num_pay = 1
                                        document_id.num_partiality = num_pay
                                        # pay to currency rate
                                        imp_pay = tip_cambio_p * float("{0:.2f}".format(line.amount))
                                        pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                        doc_rel_node.NumParcialidad.value = str(num_pay)
                                        doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line.original_balance, 6))) or None
                                        doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(float(pay), 6))) or None
                                        doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round(line.original_balance - float(pay), 6))) or None
                            else:
                                imp_pay = tip_cambio_p * float("{0:.2f}".format(line.amount))
                                pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(float(round(pay,2)), 2))) or None
                        elif moneda_p.id == self.company_id.currency_id.id and moneda_dr.id != self.company_id.currency_id.id:
                            pay_node.Monto.value = monto = "{0:.2f}".format(float_round(float(monto), 6))
                            import_pay = float_round(line.amount, 6)
                            if self.payment_rate > 1:
                                rate_pay = self.payment_rate
                                tip_pay = rate_pay
                                rate = 1 / tip_pay
                                rate_document = rate
                            else:
                                if invoice_id.currency_rate_alter > 1:
                                    rate_invoice = invoice_id.currency_rate_alter
                                    tip_inv = rate_invoice
                                    rate = 1 / tip_inv
                                    rate_document = rate
                                else:
                                    rate_lines = line.original_amount / line.amount_original
                                    rate_line = 1 / rate_lines
                                    tip_dr = rate_line
                                    rate = 1 / tip_dr
                                    rate_document = rate
                            doc_rel_node.TipoCambioDR.value = float_round(rate_document,5)
                            if invoice_id.pay_method_sat_id.code == 'PPD':
                                num_pay = document_id.num_partiality
                                if num_pay > 0:
                                    num_pay = num_pay + 1
                                    document_id.num_partiality = num_pay
                                    rate_pay = rate_document
                                    imp_pay = import_pay * rate_pay
                                    pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                    doc_rel_node.NumParcialidad.value = str(num_pay)
                                    line_balance = float_round(line.original_balance,6)
                                    doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round(line_balance,6))) or None
                                    doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round(float(pay),6))) or None
                                    doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round((line_balance - float(pay)),6))) or None
                                else:
                                    num_pay = 1
                                    document_id.num_partiality = num_pay
                                    rate_pay = rate_document
                                    imp_pay = import_pay * rate_pay
                                    pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                    doc_rel_node.NumParcialidad.value = str(num_pay)
                                    line_balance = line.original_balance
                                    doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round((line_balance),6))) or None
                                    doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round((float(pay)),6))) or None
                                    doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round((line_balance - float(pay)),6))) or None
                            else:
                                rate_pay = rate_document
                                imp_pay = import_pay * rate_pay
                                pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round((float(pay)),6))) or None
                        elif moneda_p.id != self.company_id.currency_id.id and moneda_dr.id != self.company_id.currency_id.id:
                            import_pay = float_round(line.amount, 6)
                            if self.payment_rate > 1:
                                rate_pay = self.payment_rate
                                tip_pay = rate_pay
                                rate = 1 / tip_pay
                                rate_document = rate
                            else:
                                if invoice_id.currency_rate_alter > 1:
                                    rate_invoice = invoice_id.currency_rate_alter
                                    tip_inv = rate_invoice
                                    rate = 1 / tip_inv
                                    rate_document = rate
                                else:
                                    rate_lines = line.original_amount / line.amount_original
                                    rate_line = 1 / rate_lines
                                    tip_dr = rate_line
                                    rate = 1 / tip_dr
                                    rate_document = rate
                            doc_rel_node.TipoCambioDR.value = rate_document
                            if invoice_id.pay_method_sat_id.code == 'PPD':
                                num_pay = document_id.num_partiality
                                if num_pay > 0:
                                    num_pay = num_pay + 1
                                    document_id.num_partiality = num_pay
                                    rate_pay = rate_document
                                    imp_pay = float("{0:.2f}".format(import_pay)) * rate_pay
                                    pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                    doc_rel_node.NumParcialidad.value = str(num_pay)
                                    line_balance = line.original_balance
                                    doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round((line_balance),6))) or None
                                    doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round((float(pay)),6))) or None
                                    doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round((line_balance - float(pay)),6))) or None
                                else:
                                    num_pay = 1
                                    document_id.num_partiality = num_pay
                                    rate_pay = rate_document
                                    imp_pay = float("{0:.2f}".format(import_pay)) * rate_pay
                                    pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                    doc_rel_node.NumParcialidad.value = str(num_pay)
                                    line_balance = line.original_balance
                                    doc_rel_node.ImpSaldoAnt.value = str("{0:.2f}".format(float_round((line_balance),6))) or None
                                    doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round((pay),6))) or None
                                    doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round((line_balance - float(pay)),6))) or None
                            else:
                                rate_pay = rate_document
                                imp_pay =  float("{0:.2f}".format(import_pay)) * rate_pay
                                pay = Decimal(imp_pay).quantize(Decimal('.01'), rounding=ROUND_DOWN)
                                doc_rel_node.ImpPagado.value = str("{0:.2f}".format(float_round((float(pay)),2))) or None
			if doc_rel_node.ImpSaldoInsoluto.value:
                            if float(doc_rel_node.ImpSaldoInsoluto.value) > (-0.1) and float(doc_rel_node.ImpSaldoInsoluto.value) <= 0:
                                doc_rel_node.ImpSaldoInsoluto.value = str("{0:.2f}".format(float_round(0.00,6))) or None
                        date_pay = self.move_id.date+'T00:00:00'
                        date_pay = self.cfdi.issue_datetime(date_pay, tz)
                        pay_node.FechaPago.value = date_pay
                        if not invoice_id.pay_method_sat_id:
                            raise Warning(_('Configure method pay in document' + ' ' + invoice_id.number))
                        doc_rel_node.IdDocumento.value = document_id.uuid or None
                        doc_rel_node.Serie.value = invoice_id.journal_id.invoice_sequence_id.approval_id.serie or invoice_id.number or None
                        try:
                            folio = str(line.move_line_id.ref).split(" - ")[-1] or None
                        except Exception as e:
                            folio = line.move_line_id.ref or None
                        doc_rel_node.Folio.value = folio
                        doc_rel_node.MonedaDR.value = moneda_dr.name or None
                        doc_rel_node.MetodoDePagoDR.value = invoice_id.pay_method_sat_id.code or None
                        self.docs_rel.append({
                            'document_id': invoice_id.id,
                            'document_obj': invoice_id._inherit,
                            'partner_folio': doc_rel_node.Folio.value,
                            'move': 'relacionado',
                            'uuid': document_id.uuid})
                        nodo_docto_relacionado.append(doc_rel_node)
                    else:
                        invoice_id = invoice_obj.search([('move_id', '=', line.move_line_id.move_id.id)], limit=1)
                        if invoice_id:
                            document_no_sign.append(invoice_id)
                    if document_no_sign:
                        facts = [inv.number.encode('utf-8') for inv in document_no_sign]
                        raise Warning(
                            _('Can not perform operation, the following documents have not been signed:'),
                            _(facts)
                        )
                    else:
                        check_invoice = invoice_obj.search([('move_id', '=', line.move_line_id.move_id.id)])
                        if check_invoice:
                            if check_invoice.state_document == "signed" or check_invoice.state not in ("cancel",):
                                pass
                            else:
                                facts = [line.move_line_id.move_id.ref.encode('utf-8')]
                                raise Warning(
                                    _('Can not perform operation, the following documents have not been signed:'),
                                    _(facts)
                                )
                        else:
                            facts = [line.move_line_id.move_id.ref.encode('utf-8')]
                            raise Warning(
                                _('Can not perform operation, the following documents have not been signed:'),
                                _(facts)
                            )
            pay_node.DoctoRelacionado.value = nodo_docto_relacionado
            payments_node_xml.Pago.value = [pay_node]
            return payments_node_xml

    @api.model
    def voucher_structure_xml(self):
        validate_apply_journal = ['advance', 'refund']
        cfdi_version = self.journal_id.cfdi_version.version
        xml_class = self.cfdi.XmlCfdi()
        node_comprobante = self.comprobante_node_xml()
        node_comprobante.Emisor = self.get_emisor_node_xml()
        node_comprobante.Receptor = self.get_receptor_node_xml()
        node_comprobante.Conceptos = self.nodo_conceptos_xml()
        # include complement node?
        if self.journal_id.account_account_apply_id.code not in validate_apply_journal:
            node_comprobante.Complemento = self.complement_node()
        file_globals = self.cfdi.certificate()
        xml_obj = xml_class.object2xml(node_comprobante)
        xml_path = "{}{}.xml".format(self.cfdi.path_tmp(), self.cfdi.uuid())
        xml_class.xml2file(xml_obj, filename=xml_path)
        pem_key = file_globals.get('pem_key')
        sello = self.cfdi.sign(cfdi_version, xml_path, pem_key)
        node_comprobante.Sello.value = sello or None
        original_string = xml_class.xml_transform_xslt(cfdi_version, xml_path, to_return='path')
        original_string = xml_class.read_file(original_string)
        doc_xml = xml_class.object2xml(node_comprobante)
        xml_class.xml2file(doc_xml, filename=xml_path)
        xml_validate = xml_class.xml_validate_xsd(cfdi_version, xml_path)
        return {
            'doc_xml': doc_xml,
            'cad':original_string,
            'cfdi': node_comprobante,
            'xml_file': xml_path,
            'pem_key': pem_key}

    @api.model
    def sign_voucher_complement(self):
        cfd_obj = self.env['cfdi33']
        self.cfdi = cfd_obj
        self.docs_rel = []
        message_obj = self.env['mail.message']
        xml_class = self.cfdi.XmlCfdi()
        cfdi_version = self.journal_id.cfdi_version
        action = 'sign'
        signature_date = fields.Datetime.now()
        type_document = 'customer_payment+complement'
        cfdi_sign = self.voucher_structure_xml()
        xml_sign = cfdi_sign.get('doc_xml')
        cad_original = cfdi_sign.get('cad')
        pem_key = cfdi_sign.get('pem_key')
        sos_path = self.cfdi.tmp_file()
        xml_file = cfdi_sign.get('xml_file')
        comprobante = cfdi_sign.get('cfdi')
        response = self.cfdi.sign_cfdi_xml(cfdi_version, action, xml_sign, type_document)
        xml_class.write_file(response.get('contentXml'), sos_path)
        signed_original_string = xml_class.xml_transform_xslt(cfdi_version.version, sos_path, _type='tfd', to_return='content')
        xml_string_without_sign = xml_class.read_file(xml_file)
        xml_signed = response.get('contentDom')
        tfd = xml_signed.getElementsByTagName('tfd:TimbreFiscalDigital')[0]
        sello_sat = tfd.getAttribute('SelloSAT')
        UUID = response.get('uuid')
        self.docs_rel.append(
            {
                'document_id': self.id,
                'document_obj': self._inherit,
                'partner_folio': comprobante.Folio.value,
                'move': 'origen',
                'uuid': UUID})
        self.move_id.write({'cfdi_table_ids': [(0, 0, x) for x in self.docs_rel]})
        cfdi_sign_params = {
            'original_string': cad_original,
            'certificate_number': comprobante.NoCertificado.value,
            'certificate': comprobante.Certificado.value,
            'stamp': comprobante.Sello.value,
            'signature_date': signature_date,
            'signed_original_string': signed_original_string,
            'stamp_signed': sello_sat,
            'uuid': UUID,
            'xml': xml_class.encode64(xml_string_without_sign),
            'xml_signed': xml_class.encode64(xml_signed.toxml(encoding='utf-8')),
            #'pdf_signed': '',
            'voucher_id': self.id,
            'move_id': self.move_id.id,
            'state_document': 'signed',
        }
        cfdi_sign_id = self.env['cfdi.sign'].create(cfdi_sign_params)
        cfdi_sign_id.refresh_pdf_payment()
        self.state_document = 'signed'
        self.cfdi_version = cfdi_version
        body = _("Payment signed correctly with the SAT")
        message_values = {
            'type': 'notification',
            'res_id': self.id,
            'model': 'account.voucher',
            'subject': _('Payment signed'),
            'body': body
            }
        message_obj.create(message_values)

    @api.multi
    def cancel_voucher(self):
        message_obj = self.env['mail.message']
        if not self.cancel_wcfdi:
            if not (self.state_document=='not_signed'):
                if self.cfdi_version:
                    cfdi_version = self.cfdi_version
                    if len(self.cfdi_sign_ids) > 0:
                        cfdi = self.env['cfdi33']
                        xml_cfdi = cfdi.XmlCfdi()
                        for cfdi_sign_id in self.cfdi_sign_ids:
                            cancel_path = cfdi.tmp_file()
                            cfdi_cancel = xml_cfdi.decode64(cfdi_sign_id.xml_signed)
                            xml_cfdi.write_file(cfdi_cancel, cancel_path)
                            signature_cancellation_date = fields.Datetime.now()
                            data = xml_cfdi.str2domXml(cancel_path)
                            data = data.getroot()
                            cfdi.sign_cfdi_xml(cfdi_version, 'cancel', data, 'customer_payment+complement')
                            cfdi_sign_id.update(
                                {
                                'signature_cancellation_date': signature_cancellation_date,
                                'state_document': 'cancelled'})
                            body = _("Payment canceled correctly with the SAT")
                            message_values = {
                                'type': 'notification',
                                'res_id': self.id,
                                'model': 'account.voucher',
                                'subject': _('Payment canceled with the SAT'),
                                'body': body
                                }
                            message_obj.create(message_values)
        self.state_document = 'cancelled'
        #SE SOBREESCRIBIO LA FUNCION NATIVA PARA EL MEJORAMIENTO DE RENDIMIENTO
        #import pdb; pdb.set_trace()
        move_pool = self.env['account.move']
        move_line_pool = self.env['account.move.line']
        invoice_pool = self.env['account.invoice']
        for voucher in self:
            # refresh to make sure you don't unlink an already removed move
            # voucher.refresh()
            # <Update TODO: Reconcile amount on cancellation>
            if voucher.move_id:
                move_obj = voucher.move_id
                #new_move = voucher.move_id.copy()
                new_ref = voucher.move_id.ref + \
                    " Pago cancelado " + voucher.number

                # TODO:UPDATE:BEGIN Fix date and period on cancelation.
                current_date = fields.Date.context_today(self) or openerp.fields.Date.today()
                currency_period = move_obj._get_period()
                if not currency_period:
                    raise openerp.exceptions.Warning(_('No period found. Please configure the current period at configuration/periods'))

                new_move = move_pool.create({
                    'partner_id': voucher.partner_id.id,
                    'journal_id': move_obj.journal_id.id,
                    'ref': new_ref,
                    # Update period
                    'period_id': currency_period,
                    # Update date
                    'date': current_date,
                })
                ref_voucher=voucher.move_id.name + " cancelado"
                voucher.move_id.write({
                    'ref': ref_voucher,
                })
                # TODO:UPDATE:END
                for move in voucher.move_id:
                    if move.amount:
                        line_ids = move.line_id
                        if line_ids:
                            reconcile_id = False
                            for line in line_ids:
                                if line.reconcile_id:
                                    reconcile_id = line.reconcile_id.id
                                    move_lines_reconciled = move_line_pool.search([('reconcile_id', '=', reconcile_id)])
                                    move_line_obj = move_lines_reconciled[-1]
                                    self.env.cr.execute("""
                                    UPDATE account_move_line
                                    SET reconcile_partial_id = %s, reconcile_id = NULL
                                    WHERE reconcile_id = %s
                                    """ % (reconcile_id, reconcile_id))
                                    invoice_id = invoice_pool.search([('move_id', '=', move_line_obj.move_id.id)])
                                    if len(invoice_id) > 0:
                                        invoice = invoice_id or self.env['account.invoice']
                                        if invoice.reconciled:
                                            invoice.write({'reconciled': False, 'state': 'open'})

                self.env.cr.execute("""
                    CREATE TEMPORARY TABLE copy_lines as (SELECT *, debit as d1, credit as c1 from account_move_line where move_id = %s);
                    UPDATE copy_lines set move_id = %s, credit=d1, debit=c1, amount_currency=amount_currency*-1, id=nextval('account_move_line_id_seq'),ref='%s';
                    ALTER TABLE copy_lines drop column d1;
                    ALTER TABLE copy_lines drop column c1;
                    INSERT INTO account_move_line select * from copy_lines;
                    DROP TABLE copy_lines;
                    """ % (str(move_obj.id),str(new_move.id),ref_voucher))


                new_move.button_cancel()
                new_move.button_validate()
        res = {
            'state': 'cancel',
            'move_id': False,
        }
        voucher.move_id.post()
        self.computados_reconciliacion()
        return self.write(res)
        # <EndUpdate TODO>


    @api.v8
    def computados_reconciliacion_new(self):
        return self.computados_reconciliacion(self._cr, self._uid, self)

    @api.multi
    def clean_empty_lines(self):
        for voucher in self:
            if voucher.line_dr_ids:
                for linea_id in voucher.line_dr_ids:
                    if not(linea_id.amount>0):
                        linea_id.unlink()
            if voucher.line_cr_ids:
                for linea_id2 in voucher.line_cr_ids:
                    if not(linea_id2.amount>0):
                        linea_id2.unlink()
        return

    # @api.model
    # def create(self, vals):
    #     line_dr_ids = []
    #     if 'line_dr_ids' in vals:
    #         for line_dr in vals.get('line_dr_ids'):
    #             if line_dr[2].get('amount', 0.00) > 0.00:
    #                 line_dr_ids.append(line_dr)
    #         if line_dr_ids:
    #             vals['line_dr_ids'] = line_dr_ids
    #     line_cr_ids = []
    #     if 'line_cr_ids' in vals:
    #         for line_cr in vals.get('line_cr_ids'):
    #             if line_cr[2].get('amount', 0.00) > 0.00:
    #                 line_cr_ids.append(line_cr)
    #         if line_cr_ids:
    #             vals['line_cr_ids'] = line_cr_ids
    #     return super(AccountVoucher, self).create(vals)

    @api.one
    def sign_document(self):
        if not self.journal_id.cfdi_version:
            raise Warning(_('Configure your journal with version cfdi.'))
        type_sign = self.journal_id.account_account_apply_id.code
        if type_sign not in ['advance', 'refund']:
            self.sign_voucher_complement()
        elif type_sign in ['advance']:
            self.advance_or_refund(type_sign=type_sign)
        else:
            raise MissingError(_('This voucher no is possible sign, only is possible related'))

    @api.model
    def advance_or_refund(self, type_sign):
        doc_credit_list = self.get_lines_apply(type_lines='credit')
        doc_debit_list = self.get_lines_apply(type_lines='debit')
        doc_list = []
        move_list = []
        amount = 0
        for row_debit in doc_debit_list:
            doc_list.append(row_debit.move_line_id.ref or row_debit.move_line_id.name)
            move_list.append(row_debit.move_line_id.move_id.id)
            amount = row_debit.amount

        if len(doc_list) > 1:
            msg = "This document will signed, but process reason only is permit one document %s.\n" \
                  "Please remove some document" % str(doc_list)
            raise MissingError(_(msg))
        elif len(doc_list) < 1:
            msg = "This voucher will related. Please add one document"
            raise MissingError(_(msg))
        move_credit_list = []
        for row_credit in doc_credit_list:
            move_credit_list.append(row_credit.move_line_id.move_id.id)
        account_invoice_list = self.env['account.invoice'].search([('move_id', 'in', move_credit_list)])

        if len(account_invoice_list) != len(move_credit_list):
            raise MissingError(_("Some document is not invoice"))

        account_invoice_signs = self.env['cfdi.sign'].search([('move_id', 'in', move_credit_list)])
        if len(account_invoice_signs):
            account_invoice_signed = []
            for row_invoice in account_invoice_signs:
                account_invoice_signed.append(row_invoice.move_id.ref)

        advance_payment_sings = self.env['cfdi.sign'].search([('move_id', 'in', move_list)])
        if len(advance_payment_sings) != 1:
            msg = _("the document %s has not signed. Please sign for continue" % str(doc_list))
            raise MissingError(msg)

        if type_sign == 'advance':
            rdc = self.env['related.document.cfdi'].search([('voucher_id', '=', self.id)])
            related_document_invoice = []
            uuid_policy = []
            if len(rdc):
                count_signed = count_total = 0

                for row in rdc.related_document_cfdi_line_ids:
                    count_total += 1
                    # print(row.number)
                    if row.state_document == 'signed':
                        count_signed += 1
                        for doc_signed in row.account_invoice_id.cfdi_sign_ids:
                            related_document_invoice.append(doc_signed.uuid)
                            uuid_policy.append({
                                'document_id': row.account_invoice_id.id,
                                'document_obj': row.account_invoice_id._model,
                                'partner_folio': row.account_invoice_id.number,
                                'move': 'relacionado',
                                'uuid': doc_signed.uuid
                            })
                    elif row.state_document != 'cancelled':
                        row.sign_document()

                if count_signed != count_total:
                    raise MissingError(_('Is necessary sign all related documents, for generate the sign.'))
            else:
                raise MissingError(_('This voucher no is possible sign, because not has documents related signed'))
                # self.action_view_related_documents()
            cfdi = self.env['cfdi33']
            certificate = cfdi.certificate()
            advance_payment = advance_payment_sings.advance_payment_id
            advance_payment_date = advance_payment.register_date
            for inv in account_invoice_list:
                if inv.invoice_datetime <= advance_payment_date:
                    msg = _("you can not sign the down payment application the date of the invoice %s is less or equalthan the advance %s date") % (inv.number, advance_payment.number)
                    raise MissingError(msg)
            comprobante = cfdi.Comprobante()
            version = self.journal_id.cfdi_version
            comprobante.Version.value = version.version
            comprobante.NoCertificado.value = certificate.get('serial_number')
            comprobante.Certificado.value = certificate.get('cer_base64')
            comprobante.TipoDeComprobante.value = 'E'
            comprobante.Serie.value, comprobante.Folio.value = self.get_serial_and_folio()
            comprobante.Fecha.value = cfdi.issue_datetime(fields.Datetime.now(self))
            comprobante.MetodoPago.value = 'PUE'
            comprobante.FormaPago.value = "30"
            comprobante.LugarExpedicion.value = cfdi.expedition_place(self.journal_id)
            comprobante.Moneda.value = advance_payment.currency_id.name

            _amount = _subtotal = _tax = _rate = None
            # usd ap
            if advance_payment.currency_id.id != cfdi.company().currency_id.id:
                _rate = advance_payment.rate
                # mxn vou
                if self.currency_id.id == cfdi.company().currency_id.id:
                    _amount = amount / _rate
                else:
                    # usd vou
                    _amount = amount
                    if self.payment_rate > 1:
                        _rate = self.payment_rate
            else:
                # mxn ap
                if self.currency_id.id != cfdi.company().currency_id.id:
                    # usd vou
                    if self.payment_rate > 1:
                        _rate = self.payment_rate
                    else:
                        for row in self.move_id.line_id:
                            if row.amount_currency:
                                _rate = abs(row.debit) or abs(row.credit) / abs(row.amount_currency)
                                break
                    _amount = amount * _rate
                    _rate = None
                else:
                    _amount = amount

            comprobante.TipoCambio.value = _rate

            # comprobante.CondicionesDePago.value = 'Anticipo'
            _amount = float("{0:.2f}".format(_amount))
            comprobante.Total.value = "{0:.2f}".format(_amount)
            _subtotal = _amount / 1.16
            _tax = _amount - _subtotal
            comprobante.SubTotal.value = "{0:.2f}".format(_subtotal)

            comprobante.schemaLocation.value = [
                "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd"
            ]
            emisor = cfdi.issuing()
            emisor.RegimenFiscal.value = '601'
            comprobante.Emisor = emisor

            receptor = cfdi.receiver(self.partner_id)
            receptor.UsoCFDI.value = 'P01'
            comprobante.Receptor = receptor

            product = self.env['product.template'].search([('default_code', '=', 'APANT')])
            if product:
                concepto = cfdi.Concepto()
                concepto.ClaveProdServ.value = product.line.sat_product.code
                concepto.NoIdentificacion.value = product.default_code
                concepto.Cantidad.value = 1.0
                concepto.ClaveUnidad.value = product.uom_id.code
                concepto.Unidad.value = product.uom_id.name
                concepto.Descripcion.value = product.name

                concepto.ValorUnitario.value = float("{0:.2f}".format(_subtotal))
                concepto.Importe.value = concepto.Cantidad.value * concepto.ValorUnitario.value

                concepto.Impuestos = cfdi.Impuestos()

                traslado = cfdi.Traslado()
                traslado.Base.value = concepto.Importe.value

                traslado.Importe.value = "{0:.2f}".format(_tax)
                traslado.Impuesto.value = '002'
                traslado.TasaOCuota.value = '0.160000'
                traslado.TipoFactor.value = 'Tasa'

                concepto.Impuestos.Traslados = cfdi.Traslados()
                concepto.Impuestos.Traslados.Traslado.value = [traslado]

                conceptos = cfdi.Conceptos()
                conceptos.Concepto.value = [concepto]
                comprobante.Conceptos = conceptos

                comprobante.Impuestos = cfdi.Impuestos()
                comprobante.Impuestos.Traslados = cfdi.Traslados()

                traslado_base = cfdi.Traslado()
                traslado_base.Importe.value = "{0:.2f}".format(_tax)
                traslado_base.Impuesto.value = '002'
                traslado_base.TasaOCuota.value = '0.160000'
                traslado_base.TipoFactor.value = 'Tasa'
                comprobante.Impuestos.Traslados.Traslado.value = [traslado_base]
                comprobante.Impuestos.TotalImpuestosTrasladados.value = "{0:.2f}".format(_tax)

                addenda = cfdi.Addenda()

                folios = cfdi.Folios()
                folios.folioERP.value = comprobante.Folio.value
                folios.codigoCliente.value = self.partner_id.client_number
                addenda.Folios = folios

                addenda.DomicilioEmisor = cfdi.issuing_address(
                    self.journal_id.address_invoice_company_id)

                addenda.DomicilioReceptor = cfdi.receiver_address(self.partner_id)

                comprobante.Addenda = addenda
                cfdi_relacionados = cfdi.CfdiRelacionados()
                cfdi_relacionados.TipoRelacion.value = "07"
                doc_rel_list = []
                for doc_rel_uuid in related_document_invoice:
                    cfdi_relacionado = cfdi.CfdiRelacionado()
                    cfdi_relacionado.UUID.value = doc_rel_uuid
                    doc_rel_list.append(cfdi_relacionado)

                cfdi_relacionados.CfdiRelacionado.value = doc_rel_list

                comprobante.CfdiRelacionados = cfdi_relacionados
                ### end added nodes

                xml_cfdi = cfdi.XmlCfdi()

                # Generate dom Xml
                dom_ap = xml_cfdi.object2xml(comprobante)

                path_xml_ap = "{}{}.xml".format(cfdi.path_tmp(), cfdi.uuid())
                dom_ap, node = xml_cfdi.rm_node_xml(dom_ap, 'cfdi:Addenda')
                xml_cfdi.xml2file(dom_ap, filename=path_xml_ap)

                original_string = xml_cfdi.xml_transform_xslt(version.version, path_xml_ap,
                                                              to_return='path')
                comprobante.Sello.value = xml_cfdi.xml_sign(original_string, certificate.get('pem_key'))
                comprobante.Addenda = None
                dom_ap = xml_cfdi.object2xml(comprobante)
                # Convert Dom Xml to String
                xml_cfdi.xml2file(dom_ap, filename=path_xml_ap)
                xml_string_without_sign = xml_cfdi.read_file(path_xml_ap)

                xml_cfdi.xml2file(dom_ap, filename=path_xml_ap)
                ok = xml_cfdi.xml_validate_xsd(version.version, path_xml_ap)
                if ok:
                    signature_date = fields.Datetime.now()
                    result_sign = cfdi.sign_cfdi_xml(version, 'sign', dom_ap,
                                                     'customer_advance_payment')
                    sos_path = cfdi.tmp_file()
                    xml_cfdi.write_file(result_sign.get('contentXml'), sos_path)
                    signed_original_string = xml_cfdi.xml_transform_xslt(version.version, sos_path,
                                                                         _type='tfd',
                                                                         to_return='content')
                    dom_ap.append(node)
                    node_addenda = xml_cfdi.str2dom(xml_cfdi.domXml2str(dom_ap))
                    node_addenda = node_addenda.firstChild.getElementsByTagName('cfdi:Addenda')
                    xml_signed = result_sign.get('contentDom')
                    if len(node_addenda) > 0:
                        xml_signed.firstChild.appendChild(node_addenda[0])
                    cfdi_sign_params = {
                        'original_string': xml_cfdi.read_file(original_string),
                        'certificate_number': comprobante.NoCertificado.value,
                        'certificate': comprobante.Certificado.value,
                        'stamp': comprobante.Sello.value,

                        'signature_date': signature_date,
                        'move_id': self.move_id.id,
                        'signed_original_string': signed_original_string,
                        'uuid': result_sign.get('uuid'),
                        'xml': xml_cfdi.encode64(xml_string_without_sign),
                        'xml_signed': xml_cfdi.encode64(xml_signed.toxml(encoding="utf-8")),
                        # 'pdf_signed': '',
                        'voucher_id': self.id,
                        'state_document': 'signed'
                    }

                    self.cfdi_version = version.id
                    self.state_document = 'signed'
                    rdc.update({'number': comprobante.Folio.value})
                    cfdi_sign_id = self.env['cfdi.sign'].create(cfdi_sign_params)
                    cfdi_sign_id.refresh_pdf_payment()
                    uuid_policy.append({
                            'document_id': self.id,
                            'document_obj': self._inherit,
                            'partner_folio': self.number,
                            'move': 'origen',
                            'uuid': cfdi_sign_params.get('uuid')
                    })
                    self.move_id.write({'cfdi_table_ids': [(0, 0, x) for x in uuid_policy]})
                    refund_ap_id = rdc
            else:
                msg = 'Not exists the product with default code "APANT", is necessary config'
                raise MissingError(_(msg))
        elif type_sign == 'advance_related':
            advance_payment = advance_payment_sings.advance_payment_id
            refund_ap = {
                # 'number': comprobante.Folio.value,
                'number': self.number,
                'voucher_id': self.id,
                'type': 'customer',
                'document': 'advance_payment',
                'advance_payment_id': advance_payment.id,
                'related_document_cfdi_line_ids': [
                    (0, 0, {
                        'account_invoice_id': ai.id,
                    }) for ai in account_invoice_list]
            }
            refund_ap_id = self.env['related.document.cfdi'].create(refund_ap)
        elif type_sign == 'refund':
            if len(account_invoice_signs):
                msg = "Not is possible related ncc -> fac , because these documents are signed %s " % str(account_invoice_signed)
                raise MissingError(_(msg))
            refund_ap = {
                'number': self.number,
                'voucher_id': self.id,
                'type': 'customer',
                'document': 'refund',
                'account_invoice_id': advance_payment_sings.account_invoice_id.id,
                'related_document_cfdi_line_ids': [
                    (0, 0, {
                        'account_invoice_id': ai.id,
                    }) for ai in account_invoice_list]
            }
            refund_ap_id = self.env['related.document.cfdi'].create(refund_ap)
        else:
            refund_ap_id = None

        return refund_ap_id

    @api.model
    def get_serial_and_folio(self):
        if self.journal_id.invoice_sequence_id:
            _serie = self.journal_id.invoice_sequence_id.prefix
            if _serie:
                _serie = re.sub(r'\W+', '', _serie)
            _folio = self.env['ir.sequence'].next_by_id(self.journal_id.invoice_sequence_id.id)
        else:
            raise MissingError(_('Is necessary config sequence of document in journal'))
        return _serie, _folio

    @api.multi
    def action_view_related_documents(self):
        if self.state == 'posted':
            type_sign = self.journal_id.account_account_apply_id.code

            if type_sign in ['advance', 'refund']:
                rdc = self.env['related.document.cfdi'].search([('voucher_id', '=', self.id)])
                ok = len(rdc)
                if type_sign == 'advance' and not ok:
                    rdc = self.advance_or_refund(type_sign='advance_related')
                elif type_sign == 'refund' and not ok:
                    rdc = self.advance_or_refund(type_sign='refund')
                if len(rdc):
                    _return_values = {
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'related.document.cfdi',
                        'type': 'ir.actions.act_window',
                        'nodestroy': True,
                        'target': 'current',
                        'res_id': rdc.id,
                    }
                    return _return_values
                else:
                    raise Warning(_("This voucher not has related documents"))
            else:
                raise Warning(_("This voucher not has related documents"))
        else:
            raise Warning(_("Is necessary posted this voucher for view related documents."))

    @api.model
    def get_lines_apply(self, type_lines='credit'):
        if self.type == 'receipt':  # Section customer
            _credits = self.line_cr_ids
            _debits = self.line_dr_ids

        else:  # if self.type == 'payment': #supplier
            _credits = self.line_dr_ids
            _debits = self.line_cr_ids

        if type_lines == 'credit':
            records = _credits
        else:
            records = _debits
        list_docs = []
        for row in records:
            if row.amount > 0:
                list_docs.append(row)
        return list_docs
