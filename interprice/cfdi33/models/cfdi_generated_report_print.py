# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import _, api, exceptions, fields, models
from openerp.osv import osv, fields as fieldsv7
from xml.dom import minidom
import qrcode
import tempfile
import base64
import os
import sys
import urllib


class Cfdi33PrintReport(models.Model):
    """
    """
    _inherit = ['cfdi.sign']


    @api.multi
    def get_comprobante_inf(self, data_xml):
        # import pdb; pdb.set_trace()
        comprobante_node = data_xml.getElementsByTagName('ns0:Comprobante')[0]
        certificado = comprobante_node.getAttributeNode('Certificado').value.encode('utf-8')
        if comprobante_node.getAttributeNode('CondicionesDePago'):
            condiciones_pago = comprobante_node.getAttributeNode('CondicionesDePago').value.encode('utf-8')
        else:
            condiciones_pago = '-'
        fecha = comprobante_node.getAttributeNode('Fecha').value.encode('utf-8')
        folio = comprobante_node.getAttributeNode('Folio').value.encode('utf-8')
        
        if comprobante_node.getAttributeNode('FormaPago'):
            forma_pago = comprobante_node.getAttributeNode('FormaPago').value.encode('utf-8')
        else:
            forma_pago = '-'
        
        lugar_expedicion = comprobante_node.getAttributeNode('LugarExpedicion').value.encode('utf-8')
        
        if comprobante_node.getAttributeNode('MetodoPago'):
            metodoPago = comprobante_node.getAttributeNode('MetodoPago').value.encode('utf-8')
        else:
            metodoPago = '-'

        moneda = comprobante_node.getAttributeNode('Moneda').value.encode('utf-8')
        no_certificado = comprobante_node.getAttributeNode('NoCertificado').value.encode('utf-8')
        sello = comprobante_node.getAttributeNode('Sello').value.encode('utf-8')
        if comprobante_node.getAttributeNode('Serie'):
            serie = comprobante_node.getAttributeNode('Serie').value.encode('utf-8')
        else:
            serie = '-'
        subtotal = comprobante_node.getAttributeNode('SubTotal').value.encode('utf-8')
        if comprobante_node.getAttributeNode('TipoDeComprobante'):
            tipo_comprobante = comprobante_node.getAttributeNode('TipoDeComprobante').value.encode('utf-8')
        else:
            tipo_comprobante = '-'

        total = comprobante_node.getAttributeNode('Total').value.encode('utf-8')
        version = comprobante_node.getAttributeNode('Version').value.encode('utf-8')
 
        comprobante_inf = {
                            'Certificado': certificado,
                            'CondicionesDePago': condiciones_pago,
                            'Fecha': fecha,
                            'Folio': folio,
                            'FormaPago': forma_pago,
                            'LugarExpedicion': lugar_expedicion,
                            'MetodoPago': metodoPago,
                            'Moneda': moneda,
                            'NoCertificado': no_certificado,
                            'Sello': sello,
                            'Serie': serie,
                            'SubTotal': subtotal,
                            'TipoDeComprobante': tipo_comprobante,
                            'Total': total,
                            'Version': version
                            } 

        return comprobante_inf

    @api.multi
    def get_cfdi_relacionados(self, data_xml):
        # import pdb; pdb.set_trace()
        uuids = []
        cfdis_relacionados = {}
        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:CfdiRelacionados':
                        tipo_relacion = node2.getAttributeNode('TipoRelacion').value.encode('utf-8')
                        for node3 in node2.childNodes:
                            if node3.tagName == 'ns0:CfdiRelacionado':
                                UUID = node3.getAttributeNode('UUID').value.encode('utf-8')
                                uuids.append(UUID)
        return {
            'uuids': uuids, 
            'TipoRelacion': tipo_relacion
            }



    @api.multi
    def get_emisor_info(self, data_xml):
        emisor_node = data_xml.getElementsByTagName('ns0:Emisor')[0]
        # import pdb; pdb.set_trace()
        nombre_emisor = emisor_node.getAttributeNode('Nombre').value.encode('utf-8')
        regimen_fiscal = emisor_node.getAttributeNode('RegimenFiscal').value.encode('utf-8')
        rfc_emisor = emisor_node.getAttributeNode('Rfc').value.encode('utf-8')

        return {
            'Nombre': nombre_emisor,
            'RegimenFiscal': regimen_fiscal,
            'Rfc': rfc_emisor
            }

    @api.multi
    def get_receptor_info(self, data_xml):
        # import pdb; pdb.set_trace()
        receptor_node = data_xml.getElementsByTagName('ns0:Receptor')[0]

        nombre_receptor = receptor_node.getAttributeNode('Nombre').value.encode('utf-8')
        rfc_receptor = receptor_node.getAttributeNode('Rfc').value.encode('utf-8')
        usoCFDI_receptor = receptor_node.getAttributeNode('UsoCFDI').value.encode('utf-8')
        
        return {
            'Nombre': nombre_receptor,
            'Rfc': rfc_receptor,
            'UsoCFDI': usoCFDI_receptor
            }

    @api.multi
    def get_conceptos_info(self, data_xml):
        
        # import pdb; pdb.set_trace()
        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:Conceptos':
                        for node3 in node2.childNodes:
                            # import pdb; pdb.set_trace()
                            if node3.tagName == 'ns0:Concepto':
                                cantidad = node3.getAttributeNode('Cantidad').value.encode('utf-8')
                                claveprod = node3.getAttributeNode('ClaveProdServ').value.encode('utf-8')
                                clave_unidad = node3.getAttributeNode('ClaveUnidad').value.encode('utf-8')
                                descripcion = node3.getAttributeNode('Descripcion').value.encode('utf-8')
                                importe = node3.getAttributeNode('Importe').value.encode('utf-8')
                                if node3.getAttributeNode('NoIdentificacion'):
                                    no_identificacion = node3.getAttributeNode('NoIdentificacion').value.encode('utf-8')
                                else:
                                    no_identificacion = '-'
                                
                                if node3.getAttributeNode('Unidad'):
                                    unidad = node3.getAttributeNode('Unidad').value.encode('utf-8')
                                else:
                                    unidad = '-'
                                valor_unitario = node3.getAttributeNode('ValorUnitario').value.encode('utf-8')
                                if node3.childNodes:
                                    for node4 in node3.childNodes:
                                        if node4.tagName == 'ns0:Impuestos':
                                            for node5 in node4.childNodes:
                                                if node5.tagName == 'ns0:Traslados':
                                                    for node6 in node5.childNodes:
                                                        if node6.tagName == 'ns0:Traslado':
                                                            if node6.getAttributeNode('Base'):
                                                                base = node6.getAttributeNode('Base').value.encode('utf-8')
                                                            else:
                                                                base = '-'
                                                            importe_imp = node6.getAttributeNode('Importe').value.encode('utf-8')
                                                            impuestos_imp = node6.getAttributeNode('Impuesto').value.encode('utf-8')
                                                            tasa_cuota_imp = node6.getAttributeNode('TasaOCuota').value.encode('utf-8')
                                                            tipo_factor_imp = node6.getAttributeNode('TipoFactor').value.encode('utf-8')
                                else:
                                    base = 0
                                    importe_imp = 0
                                    impuestos_imp = 0
                                    tasa_cuota_imp = '-'
                                    tipo_factor_imp = '-'
        inf_concepts = {
                    'Cantidad': cantidad,
                    'ClaveProdServ': claveprod,
                    'ClaveUnidad': clave_unidad,
                    'Descripcion': descripcion,             
                    'Importe': importe,
                    'NoIdentificacion': no_identificacion,
                    'Unidad': unidad,
                    'ValorUnitario': valor_unitario,
                    'Base': base,
                    'Importe_imp': importe_imp,
                    'Impuestos_imp': impuestos_imp,
                    'TasaOCuota_imp': tasa_cuota_imp,
                    'TipoFactor_imp': tipo_factor_imp
                    }

        return inf_concepts 

    @api.multi
    def get_impuestos_inf(self, data_xml):

        # import pdb; pdb.set_trace()

        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:Impuestos':
                        total_impuestos = node2.getAttributeNode('TotalImpuestosTrasladados').value.encode('utf-8')
                        for node3 in node2.childNodes:
                            if node3.tagName == 'ns0:Traslados':
                                for node4 in node3.childNodes:
                                    if node4.tagName == 'ns0:Traslado':
                                        importe = node4.getAttributeNode('Importe').value.encode('utf-8')
                                        impuestos = node4.getAttributeNode('Impuesto').value.encode('utf-8')
                                        tasa_cuota = node4.getAttributeNode('TasaOCuota').value.encode('utf-8')
                                        tipo_factor = node4.getAttributeNode('TipoFactor').value.encode('utf-8')
        
        return {
            'TotalImpuestosTrasladados': total_impuestos,
            'Importe': importe,
            'Impuesto': impuestos,
            'TasaOCuota': tasa_cuota,
            'TipoFactor': tipo_factor
        }



    @api.multi
    def get_complemento_inf(self, data_xml):
        pay_dic = {}
        doc_rel = []
        doc_rel_dic = {}
        timbre_fiscal = {}
        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:Complemento':
                        for node3 in node2.childNodes:
                            if node3.nodeName != '#text':
                                if node3.tagName == 'ns2:Pagos':
                                    # import pdb; pdb.set_trace()
                                    for node4 in node3.childNodes:
                                        if node4.tagName == 'ns2:Pago':
                                            if node4.getAttributeNode('CadPago'):
                                                pay_dic['cadena_pago'] = node4.getAttributeNode('CadPago').value.encode('utf-8')
                                            else:
                                                pay_dic['cadena_pago'] = '-'
                                            if node4.getAttributeNode('CertPago'):
                                                pay_dic['certificado_pago'] = node4.getAttributeNode('CertPago').value.encode('utf-8')
                                            else:
                                                pay_dic['certificado_pago'] = '-'
                                            pay_dic['fecha_pago'] = node4.getAttributeNode('FechaPago').value.encode('utf-8')
                                            pay_dic['forma_pago'] = node4.getAttributeNode('FormaDePagoP').value.encode('utf-8')
                                            pay_dic['moneda_pago'] = node4.getAttributeNode('MonedaP').value.encode('utf-8')
                                            pay_dic['monto_pago'] = node4.getAttributeNode('Monto').value.encode('utf-8')
                                            if node4.getAttributeNode('SelloPago'):
                                                pay_dic['sello_pago'] = node4.getAttributeNode('SelloPago').value.encode('utf-8')
                                            else:
                                                pay_dic['sello_pago'] = '-'
                                            if node4.getAttributeNode('TipoCadPago'):
                                                pay_dic['tipo_cad_pago'] = node4.getAttributeNode('TipoCadPago').value.encode('utf-8')
                                            else:
                                                pay_dic['tipo_cad_pago'] = '-'
                                            for node5 in node4.childNodes:
                                                if node5.tagName == 'ns2:DoctoRelacionado':
                                                    doc_rel_dic['folio_docto_rel'] = node5.getAttributeNode('Folio').value.encode('utf-8')
                                                    doc_rel_dic['id_documento'] = node5.getAttributeNode('IdDocumento').value.encode('utf-8')
                                                    doc_rel_dic['imp_pagado'] = node5.getAttributeNode('ImpPagado').value.encode('utf-8')
                                                    doc_rel_dic['metodo_pago_docto_rel'] = node5.getAttributeNode('MetodoDePagoDR').value.encode('utf-8')
                                                    doc_rel_dic['moneda_docto_rel'] = node5.getAttributeNode('MonedaDR').value.encode('utf-8')
                                                    doc_rel_dic['serie_docto'] = node5.getAttributeNode('Serie').value.encode('utf-8')
                                                    doc_rel_dic['tipo_cambio_docto_rel'] = node5.getAttributeNode('TipoCambioDR').value.encode('utf-8')
                                                    doc_rel.append(doc_rel_dic)

                                elif node3.tagName == 'ns2:TimbreFiscalDigital':
                                    timbre_fiscal['FechaTimbrado'] = node3.getAttributeNode('FechaTimbrado').value.encode('utf-8')
                                    timbre_fiscal['NoCertificadoSAT'] = node3.getAttributeNode('NoCertificadoSAT').value.encode('utf-8')
                                    timbre_fiscal['RfcProvCertif'] = node3.getAttributeNode('RfcProvCertif').value.encode('utf-8')
                                    timbre_fiscal['SelloCFD'] = node3.getAttributeNode('SelloCFD').value.encode('utf-8')
                                    timbre_fiscal['SelloSAT'] = node3.getAttributeNode('SelloSAT').value.encode('utf-8')
                                    timbre_fiscal['UUID'] = node3.getAttributeNode('UUID').value.encode('utf-8')
                                    timbre_fiscal['Version'] = node3.getAttributeNode('Version').value.encode('utf-8')
                            else:
                              pass
        return {
            'Pago': pay_dic,
            'Timbre': timbre_fiscal,
            'DocRel': doc_rel,
        }

    @api.multi
    def get_addenda(self, data_xml):

        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:Addenda':
                        for node3 in node2.childNodes:
                            if node3.tagName == 'ns0:Folios':
                                codigo_cliente = node3.getAttributeNode('codigoCliente').value.encode('utf-8')
                                folio_erp = node3.getAttributeNode('folioERP').value.encode('utf-8')
                            if node3.tagName == 'ns0:DomicilioEmisor':
                                calleE = node3.getAttributeNode('calle').value.encode('utf-8')
                                codigo_postalE = node3.getAttributeNode('codigoPostal').value.encode('utf-8')
                                coloniaE = node3.getAttributeNode('colonia').value.encode('utf-8')
                                estadoE = node3.getAttributeNode('estado').value.encode('utf-8')                                
                                paisE = node3.getAttributeNode('pais').value.encode('utf-8')
                            if node3.tagName == 'ns0:DomicilioReceptor':
                                if node3.getAttributeNode('calle'):
                                    calleR = node3.getAttributeNode('calle').value.encode('utf-8')
                                else:
                                    calleR = '-'
                                if node3.getAttributeNode('codigoPostal'):
                                    codigo_postalR = node3.getAttributeNode('codigoPostal').value.encode('utf-8')
                                else:
                                    codigo_postalR = '-'
                                if node3.getAttributeNode('colonia'):
                                    coloniaR = node3.getAttributeNode('colonia').value.encode('utf-8')
                                else:
                                    coloniaR = '-'
                                if node3.getAttributeNode('estado'):
                                    estadoR = node3.getAttributeNode('estado').value.encode('utf-8')                                
                                else:
                                    estadoR = '-'
                                if node3.getAttributeNode('pais'):
                                    paisR = node3.getAttributeNode('pais').value.encode('utf-8')
                                else:
                                    paisR = '-'

        addenda = {
                'codigoCliente': codigo_cliente,
                'folioERP': folio_erp,
                'calleE': calleE,
                'codigoPostalE': codigo_postalE,
                'coloniaE': coloniaE,
                'estadoE': estadoE,
                'paisE': paisE,
                'calleR': calleR,
                'codigo_postalR': codigo_postalR,
                'coloniaR': coloniaR,
                'estadoR': estadoR,
                'paisR': paisR,
                } 
        
        return addenda

    @api.multi
    def get_xml_information(self):
        cfdi = self.env['cfdi33']
        xml_cfdi = cfdi.XmlCfdi()
        cfdi_path = cfdi.tmp_file()
        cfdi_print = xml_cfdi.decode64(self.xml_signed)
        xml_cfdi.write_file(cfdi_print, cfdi_path)
        data_xml = minidom.parseString(cfdi_print)
        
        # import pdb; pdb.set_trace()
        info_xml_signed = {}
        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                info_comprobante = self.get_comprobante_inf(data_xml)
                info_xml_signed['Comprobante'] = info_comprobante
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:CfdiRelacionados':
                        info_cfdi_relacionados = self.get_cfdi_relacionados(data_xml)
                        info_xml_signed['CfdiRelacionados'] = info_cfdi_relacionados
                    
                    if node2.tagName == 'ns0:Emisor':
                        info_emisor = self.get_emisor_info(data_xml)
                        info_xml_signed['Emisor'] = info_emisor
                    
                    if node2.tagName == 'ns0:Receptor':
                        info_receptor = self.get_receptor_info(data_xml)
                        info_xml_signed['Receptor'] = info_receptor
                    
                    if node2.tagName == 'ns0:Conceptos':
                        info_conceptos = self.get_conceptos_info(data_xml)
                        info_xml_signed['Conceptos'] = info_conceptos
                    
                    if node2.tagName == 'ns0:Impuestos':
                        info_impuestos = self.get_impuestos_inf(data_xml)
                        info_xml_signed['Impuestos'] = info_impuestos
                    
                    if node2.tagName == 'ns0:Complemento':
                        info_complemento = self.get_complemento_inf(data_xml)
                        info_xml_signed['Complemento'] = info_complemento
                    
                    if node2.tagName == 'ns0:Addenda':
                        info_addenda = self.get_addenda(data_xml)
                        info_xml_signed['Addenda'] = info_addenda

                info_xml_signed['QR'] = self.qr_code_make(data_xml)
            # else:
        return info_xml_signed

    @api.multi
    def xml_dom_obj(self):
        cfdi = self.env['cfdi33']
        xml_cfdi = cfdi.XmlCfdi()
        cfdi_path = cfdi.tmp_file()
        cfdi_print = xml_cfdi.decode64(self.xml_signed)
        xml_cfdi.write_file(cfdi_print, cfdi_path)
        data_xml = minidom.parseString(cfdi_print)
        # import pdb; pdb.set_trace()

        return data_xml

    @api.multi
    def qr_code_make(self):
        # import pdb; pdb.set_trace()
        data_xml = self.xml_dom_obj()
        url = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx%3F"

        for node1 in data_xml.childNodes:
            if node1.tagName == 'ns0:Comprobante':
                total = node1.getAttributeNode('Total').value.encode('utf-8')
                for node2 in node1.childNodes:
                    if node2.tagName == 'ns0:Emisor':
                        rfc_emisor = node2.getAttributeNode('Rfc').value.encode('utf-8')
                    
                    if node2.tagName == 'ns0:Receptor':
                        rfc_receptor = node2.getAttributeNode('Rfc').value.encode('utf-8')
                    
                    if node2.tagName == 'ns0:Complemento':
                        # import pdb; pdb.set_trace()
                        for node3 in node2.childNodes:
                            if node3.nodeName != '#text':
                                if node3.tagName in ['ns2:TimbreFiscalDigital', 'ns3:TimbreFiscalDigital']:
                                    uuid = node3.getAttributeNode('UUID').value.encode('utf-8')
                                    sello_sat = node3.getAttributeNode('SelloSAT').value.encode('utf-8')
                                    sello_sat = sello_sat[-8:]

        qr_str = url+'re='+rfc_emisor+'&rr='+rfc_receptor+'&tt='+total+'&id='+uuid+'&fe='+sello_sat
        qr_str_encode = urllib.quote_plus(qr_str)
        return qr_str_encode 
    
    @api.multi
    def typeDocument(self):
        _type_document=''
        
        for record in self:
            if record.voucher_id:
                type_doc = record.voucher_id.journal_id.account_account_apply_id.code
                if type_doc == 'advance':
                    _type_document = 'Nota de Anticipo'
                else:
                    _type_document = 'Pago'

            elif record.advance_payment_id:
                _type_document = 'Anticipo'

            elif record.account_invoice_id:
                invoice_id = self.env['account.invoice'].browse(record.account_invoice_id.id)
                if invoice_id.type == 'out_refund':
                    _type_document = 'Nota de credito'
                elif invoice_id.type == 'out_invoice':
                    _type_document = 'Factura'

        return _type_document


    @api.model
    def hasAttribute(self, dom, attribute, default=''):

        _value = default
        if dom.hasAttribute(attribute):
            _value = dom.getAttributeNode(attribute).value
        return _value


