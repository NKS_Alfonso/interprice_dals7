# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models


class StockPicking(models.Model):
    """ Model extend for add use cfdi field """

    # odoo model properties
    _inherit = 'stock.picking'

    # General (Public) methods
    def _get_invoice_vals(self, cr, uid, key, inv_type,
                          journal_id, move, context=None):
        res = super(StockPicking, self)._get_invoice_vals(
            cr, uid, key, inv_type, journal_id, move, context)
        if res:
            res.update({'use_cfdi': key[0].use_cfdi.id})
        return res
