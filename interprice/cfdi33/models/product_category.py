# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class ProductCategory(models.Model):
    """ Model extend for add use cfdi field """

    # odoo model properties
    _inherit = 'product.category'

    # initial methods
    @api.one
    def _get_hide_sat_product(self):
        hide = True
        for types in self.types:
            if types.type == 'Línea':
                hide = False
                break
        self.hide_sat_product = hide

    # custom fields
    sat_product = fields.Many2one(
        comodel_name="sat.product",
        required=False,
        string=_("Sat product"),
        help=_("Key of sat product."),
        copy=False
    )
    hide_sat_product = fields.Boolean(
        compute=_get_hide_sat_product,
        default=True,
        copy=False
    )

    # Public Methods
    @api.one
    @api.onchange('types')
    def onchange_types(self):
        hide = True
        for types in self.types:
            if types.type == 'Línea':
                hide = False
                break
        if hide:
            self.sat_product = False
        self.hide_sat_product = hide
