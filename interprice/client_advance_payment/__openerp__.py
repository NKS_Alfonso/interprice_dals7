# -*- coding: utf-8 -*-
{
    'name': 'Client Advance Payments',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Accounting & Finance',
    'version': '1.0',
    'depends': [
        'account',
        'account_check',
        'configuracion',
        'account_voucher',
        'account_cost_center',
        'centro_de_costos',
        'l10n_es_account_invoice_sequence',
        'l10n_mx_facturae',
        'account_voucher_tax',
        'cfdi33',
        'client',
        'cash_back',
    ],
    'application': True,
    'data': [
            'views/advance_payment_view.xml',
            'views/advance_payment_sequence.xml',
            'views/advance_payment_message.xml',
            'views/account_check_view.xml',
            'views/res_company.xml',
            'reports/views/advance_payment_report_view.xml',
            'reports/views/advance_payment_report_pdf.xml',
            'reports/wizard/views/advance_payment_report_wizard_view.xml',
            'security/security.xml',
            'security/ir.model.access.csv',
            'views/advance_payment_workflow.xml',
            #'data/product_product_data.xml'
    ]
}
