# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, tools, api


class AdvancePaymentReportView(models.Model):
    """Advance Payment report"""

    # odoo model properties
    _name = 'advance.payment.report.view'
    _table = 'advance_payment_report_view'
    _description = 'Advance Payment report'
    _auto = False  # Don't let odoo create table
    _order = 'id DESC'

    # initial methods
    @api.one
    def _get_cost_center(self):
        cost_center = ""
        i = 0
        cost_center_list = [x.name for x in self.document.centro_costos]
        for center in cost_center_list:
            cost_center += center
            i += 1
            if i < len(cost_center_list):
                cost_center += ", "
        self.cost_center = cost_center

    # View fields
    document = fields.Many2one(
        comodel_name='account.journal',
        help=_("Payment advance journal"),
        readonly=True,
        string=_("Document"),
    )
    folio = fields.Char(
        help=_("Advance Payment folio"),
        readonly=True,
        string=_("Folio"),
    )
    register_date = fields.Datetime(
        help=_("Advance payment register date"),
        readonly=True,
        string=_("Register date"),
    )
    client = fields.Many2one(
        comodel_name='res.partner',
        help=_("Client name"),
        readonly=True,
        string=_("Client"),
    )
    client_number = fields.Char(
        help=_("Client number"),
        readonly=True,
        string=_("Client Number"),
    )
    payment_type_id = fields.Many2one(
        comodel_name='account.account.apply',
        readonly=True,
        string=_("Payment Type"),
        help=_("Payment type."),
    )
    reference_folio = fields.Char(
        help=_("Reference folio"),
        readonly=True,
        string=_("Reference folio"),
    )
    amount = fields.Float(
        help=_("Advance Payment amount"),
        readonly=True,
        string=_("Amount"),
    )
    currency = fields.Many2one(
        comodel_name='res.currency',
        readonly=True,
        string=_("Currency"),
        help=_("Currency of advance payment."),
    )
    current_balance = fields.Float(
        string=_("Current Balance"),
        readonly=True,
        help=_("Current Balance of advance payment."),
    )
    cost_center = fields.Char(
        compute=_get_cost_center,
        help=_("Cost centers allowed by journal"),
        readonly=True,
        string=_("Cost Center"),
    )
    rate = fields.Float(
        string=_("Advance rate"),
        readonly=True,
        help=_("Rate of advance payment."),
    )
    state = fields.Char(
        help=_("Advance payment state"),
        readonly=True,
        string=_("State"),
    )
    user_generated = fields.Many2one(
        comodel_name='res.users',
        readonly=True,
        string=_("User generated"),
        help=_("The user who generates the document.")
    )
    cancellation_date = fields.Datetime(
        readonly=True,
        string=_("Cancellation date"),
        help=_("RDate of cancellation of the document."),
    )
    user_canceled = fields.Many2one(
        comodel_name='res.users',
        required=False,
        readonly=True,
        string=_("User Canceled"),
        help=_("The user who canceled the document.")
    )

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'advance_payment_report_view')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW advance_payment_report_view
                AS(
                    SELECT
                        ap.id,
                        ap.journal_id AS document,
                        ap.number AS folio,
                        ap.register_date,
                        ap.partner_id AS client,
                        rp_client.client_number AS client_number,
                        ap.payment_type_id,
                        CASE WHEN ap.reference_folio IS NOT NULL
                            THEN ap.reference_folio
                        ELSE CAST(ap.check_number AS TEXT)
                            END AS reference_folio,
                        ap.amount,
                        ap.currency_id AS currency,
                        CASE WHEN ROUND(CAST((ap.initial_balance - (SELECT SUM(apl.amount)
                                    FROM advance_payment_line apl WHERE
                                    advance_payment_id = ap.id)) AS NUMERIC), 4) IS NULL
                                    AND ap.state NOT IN ('draft', 'canceled')
                            THEN ap.initial_balance
                        ELSE ROUND(CAST((ap.initial_balance - (SELECT SUM(apl.amount)
                                FROM advance_payment_line apl WHERE
                                advance_payment_id = ap.id)) AS NUMERIC), 4)
                            END AS current_balance,
                        '' AS cost_center,
                        ap.rate,
                        CASE WHEN ap.state = 'draft'
                                THEN 'Borrador'
                            WHEN ap.state = 'generated'
                                THEN 'Generado'
                            WHEN ap.state = 'applied partial'
                                THEN 'Aplicado parcial'
                            WHEN ap.state = 'applied'
                                THEN 'Aplicado'
                            WHEN ap.state = 'canceled'
                                THEN 'Cancelado'
                            END AS state,
                        ap.user_generated AS user_generated,
                        ap.cancellation_date,
                        ap.user_canceled
                    FROM advance_payment AS ap
                    JOIN res_partner rp_client
                        ON (rp_client.id = ap.partner_id)
                    ORDER BY ap.id DESC
                )
            """
        )
