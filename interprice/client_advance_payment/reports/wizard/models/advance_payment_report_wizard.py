# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import _, api, exceptions, fields, models


class AdvancePaymentReportWizard(models.TransientModel):
    """This wizard is meant to filter the results for the
    Advance Payment report (Show info about client advance payments).
    """
    _name = 'advance_payment_report_wizard'
    _description = 'Advance Payment report Wizard'

    # domain methods
    @api.model
    def get_document_domain(self):
        """Simple method to get journal domain"""
        journal_ids = []
        domain = [('id', 'in', journal_ids)]
        # Performs the journal search by advance code.
        account_apply_ids = self.env[
            'account.account.apply'].search(
            [('code', 'in', ['advance'])])
        if account_apply_ids:
            apply_id_list = [aa.id for aa in account_apply_ids]
            account_journal_ids = self.env[
                'account.journal'].search(
                [('account_account_apply_id', 'in', apply_id_list)])
            journal_ids = [aj.id for aj in account_journal_ids]
            domain = [('id', 'in', journal_ids)]
        return domain

    @api.model
    def _get_payment_domain(self):
        """Simple method to get payment type domain"""
        payment_ids = []
        account_apply_ids = self.env[
            'account.account.apply'].search(
            [('code', 'in', [
                'credit_card',
                'deposit',
                'check',
                'cash'])
             ])
        if account_apply_ids:
            payment_ids = [tp.id for tp in account_apply_ids]
        return [('id', 'in', payment_ids)]

    # Fields
    document = fields.Many2one(
        comodel_name='account.journal',
        domain=get_document_domain,
        string=_("Document"),
        help=_("Journal where the advance payment was written"),
    )
    payment_type = fields.Many2one(
        comodel_name='account.account.apply',
        domain=_get_payment_domain,
        string=_("Payment Type"),
        help=_("The type of payment of the Advance."),
    )
    start_date = fields.Datetime(
        help=_("Initial date to filter advance payment"),
        string=_("Initial date")
    )
    end_date = fields.Datetime(
        help=_("Final date to filter advance payment"),
        string=_("Final date")
    )
    start_client = fields.Many2one(
        comodel_name='res.partner',
        domain=[('is_company', '=', True),
                ('customer', '=', True)],
        help=_("Initial client to filter advance payment"),
        string=_("Initial Client")
    )
    end_client = fields.Many2one(
        comodel_name='res.partner',
        domain=[('is_company', '=', True),
                ('customer', '=', True)],
        help=_("Final client to filter advance payment"),
        string=_("Final Client")
    )
    currency = fields.Many2one(
        comodel_name='res.currency',
        domain=[('id', 'in', [3, 34])],
        string=_("Currency"),
        help=_("Currency of advance payment."),
    )

    @api.multi
    def screen(self):
        ids = self._get_ids()
        return {
            'name': _('Advance payment reports'),
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': 'advance.payment.report.view',
            'target': 'current',
            'context': {},
            'domain': [('id', 'in', ids)]
        }

    @api.multi
    def show_pdf(self):
        return self.env['report'].get_action(
            self.env['advance.payment.report.view'].browse(self._get_ids()),
            'client_advance_payment.advance_payment_report_pdf'
        )

    @api.multi
    def show_xls(self):
        ids = self._get_ids()
        csv_id = self.csv_by_ids(ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        df = self.start_date
        dt = self.end_date

        if df and dt and df > dt:
            self.start_date = False
            self.end_date = False
            raise exceptions.Warning(
                _("'Date to' must be higher than 'Date from'."))

    def _get_ids(self):
        # Get filter values
        document = self.document
        currency = self.currency
        s_date = self.start_date
        e_date = self.end_date
        s_client = self.start_client
        e_client = self.end_client

        query = "SELECT DISTINCT aprv.id\n"\
                "FROM advance_payment_report_view aprv\n"\
                "WHERE aprv.id IS NOT NULL\n"
        if document:
            query += "\tAND aprv.document = CAST(%s AS INT)\n" % (document.id)
        if currency:
            query += "\tAND aprv.currency = CAST(%s AS INT)\n" % (currency.id)
        if s_date:
            query += "\tAND aprv.register_date >= CAST('%s' AS date)\n" % (s_date)
        if e_date:
            query += "\tAND aprv.register_date <= CAST('%s' AS date)\n" % (e_date)
        if s_client:
            query += "\tAND aprv.client >= CAST(%s AS INT)\n" % (s_client.id)
        if e_client:
            query += "\tAND aprv.client <= CAST(%s AS INT)\n" % (e_client.id)

        query += "\tORDER BY aprv.id DESC;"

        self.env.cr.execute(query)

        if self.env.cr.rowcount:
            ids = [id[0] for id in self.env.cr.fetchall()]
        else:
            raise exceptions.Warning(_("No records found!"))

        return ids

    def csv_by_ids(self, _ids):
        data = self.env['advance.payment.report.view'].browse(_ids)

        data_csv = [
            [
                _('Document'),
                _('Folio'),
                _('Register date'),
                _('Client'),
                _('Client Number'),
                _('Payment Type'),
                _('Reference folio'),
                _('Amount'),
                _('Currency'),
                _('Current Balance'),
                _('Cost Center'),
                _('Advance rate'),
                _('State'),
                _('User generated'),
                _('Cancellation date'),
                _('User Canceled'),
            ]
        ]

        for row in data:
            data_csv.append([
                unicode(self._set_default(row.document.name)).encode('utf8'),
                unicode(self._set_default(row.folio)).encode('utf8'),
                unicode(self._set_default(row.register_date)).encode('utf8'),
                unicode(self._set_default(row.client.name)).encode('utf8'),
                unicode(self._set_default(row.client_number)).encode('utf8'),
                unicode(self._set_default(row.payment_type_id.name)).encode('utf8'),
                unicode(self._set_default(row.reference_folio)).encode('utf8'),
                unicode(self._set_default(row.amount, 0)).encode('utf8'),
                unicode(self._set_default(row.currency.name)).encode('utf8'),
                unicode(self._set_default(row.current_balance, 0)).encode('utf8'),
                unicode(self._set_default(row.cost_center)).encode('utf8'),
                unicode(self._set_default(row.rate, 0)).encode('utf8'),
                unicode(self._set_default(row.state)).encode('utf8'),
                unicode(self._set_default(row.user_generated.name)).encode('utf8'),
                unicode(self._set_default(row.cancellation_date)).encode('utf8'),
                unicode(self._set_default(row.user_canceled.name)).encode('utf8')
            ])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'AdvancePaymentReport.csv',
                'type_file': 'csv',
                'file': file,
            })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
