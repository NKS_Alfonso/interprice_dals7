# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class AccountCheck(models.Model):
    """Extended class for overriding methods and fields for advance payment"""

    # odoo model properties
    _inherit = 'account.check'

    # custom fields
    advance_payment = fields.Many2one(
        comodel_name='advance.payment',
        required=False,
        readonly=True,
        ondelete='cascade',
        string=_("Advance Number"),
        help=_("This field is only displayed if the check is created from an advance."),
    )
