# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
from openerp.exceptions import Warning
from datetime import datetime
import math


class AdvancePaymentLine(models.Model):
    """Added model to register the applied of advance payment."""

    # odoo model properties
    _name = 'advance.payment.line'
    _inherit = ['mail.thread']
    _rec_name = 'number'
    _description = _("Advance Payment Line")
    _order = 'create_date desc'

    # field selection options
    state_list = [
        ('draft', _('Draft')),
        ('accepted', _('Accepted')),
        ('canceled', _('Cancelled'))
    ]

    # custom fields
    number = fields.Many2one(
        comodel_name='account.voucher',
        string=_("Number Payment"),
        help=_("Number Payment."),
    )
    state = fields.Selection(
        default="draft",
        selection=state_list,
        string=_("Change history"),
        help=_("Change history of payment."),
    )
    payment_date = fields.Datetime(
        default=fields.Datetime.now,
        string=_("Payment date"),
        help=_("Payment date."),
    )
    amount = fields.Float(
        digits=(12, 2),
        string=_("Debited amount"),
        help=_("Amount of advance payment applied in the payment."),
    )
    payment_amount = fields.Float(
        digits=(12, 2),
        string=_("Payment amount"),
        help=_("Payment amount."),
    )
    payment_rate = fields.Float(
        digits=(12, 2),
        string=_("Payment rate"),
        help=_("Rate applied in the payment."),
    )
    currency = fields.Many2one(
        comodel_name='res.currency',
        string=_("Payment currency"),
        help=_("Payment currency."),
    )
    policy = fields.Many2one(
        comodel_name='account.move',
        string=_("Related Policy"),
        help=_("Related Policy."),
    )
    user_applied = fields.Many2one(
        comodel_name='res.users',
        string=_("User Applied"),
        help=_("The user who applied the payment.")
    )
    user_canceled = fields.Many2one(
        comodel_name='res.users',
        string=_("User Canceled"),
        help=_("The user who cancelled the payment.")
    )
    advance_payment_id = fields.Many2one(
        comodel_name='advance.payment',
        string=_("Number Advance Payment"),
        help=_("Number Advance Payment."),
    )
