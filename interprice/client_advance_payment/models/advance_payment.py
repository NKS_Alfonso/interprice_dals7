# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
from openerp.exceptions import Warning, MissingError, ValidationError
from datetime import datetime
import re


class CfdiSign(models.Model):
    _inherit = 'cfdi.sign'

    advance_payment_id = fields.Many2one(comodel_name='advance.payment')

    @api.model
    def _partner(self):
        _to_return = super(CfdiSign,self)._partner()
        if self.advance_payment_id:
            _to_return.update({
                'partner_email': self.advance_payment_id.partner_id.email or None,
                'partner_lang': self.advance_payment_id.partner_id.lang or None,
            })
        return _to_return


class AdvancePayment(models.Model):
    """Added model to create client advance payments"""

    # odoo model properties
    _name = 'advance.payment'
    _inherit = ['mail.thread']
    _rec_name = 'number'
    _description = _("Advance Payment")
    _order = 'create_date desc'

    # field selection options
    state_list = [
        ('draft', _('Draft')),
        ('generated', _('Generated')),
        ('applied partial', _('Applied Partial')),
        ('applied', _('Applied')),
        ('canceled', _('Cancelled'))
    ]
    # Global variables
    currency_company = []
    currency_journal = []

    # initial domain methods
    @api.model
    def _get_payment_domain(self):
        """Simple method"""
        payment_ids = []
        account_apply_ids = self.env[
            'account.account.apply'].search(
            [('code', 'in', [
                'credit_card',
                'deposit',
                'check',
                'cash'])
             ])
        if account_apply_ids:
            payment_ids = [tp.id for tp in account_apply_ids]
        return [('id', 'in', payment_ids)]

    @api.depends('amount')
    @api.onchange('amount')
    def _compute_tax(self):
        company_id = self.env.user.company_id
        if company_id.advance_payments_tax:
            for tax in self:
                if tax.amount > 0:
                    tax.tax = self._get_tax_from_amount(tax.amount)
        else:
            for tax in self:
                tax.tax = 0.0
    @api.model
    def _get_tax_from_amount(self, amount):
        return round(amount - (amount / (1 + self._get_tax().amount)), 4)

    @api.depends('account_bank')
    def _compute_currency(self):
        """Simple method"""
        for aqui in self:
            if aqui.journal_id:
                currency = aqui.journal_id.currency.id
                if not currency:
                    currency = aqui.journal_id.company_id.currency_id.id
                aqui.currency_id = currency

    @api.depends('tax', 'amount')
    def _compute_balance(self):
        """Simple method"""
        for ap in self:
            total_amount = 0.0
            for line in ap.line_ids:
                if line.state == 'accepted':
                    total_amount += line.amount
            ap.current_balance = ap.amount - total_amount
            ap.initial_balance = ap.amount - ap.tax

    @api.one
    def _compute_initial_balance(self):
        """False  method"""
        pass

    @api.depends('payment_type_id')
    def _compute_hide(self):
        """Simple method"""
        if self.payment_type_id.code == 'check':
            self.hide_account_bank = True
            self.hide_customer_account = True
            self.hide_customer_bank = True
            self.hide_check_number = True
        elif self.payment_type_id.code == 'deposit':
            self.hide_account_bank = True
            self.hide_customer_account = True
            self.hide_customer_bank = True
            self.hide_reference_folio = True
        elif self.payment_type_id.code == 'credit_card':
            self.hide_account_bank = True
            self.hide_customer_bank = True
            self.hide_reference_folio = True
        # hide page of applications..
        for line in self.line_ids:
            if line:
                self.hide_line_ids = True
                break
        # hide page of policy..
        for policy in self.related_policy:
            if policy:
                self.hide_policy_ids = True
                break

    # custom fields
    number = fields.Char(
        default=_("/"),
        required=True,
        readonly=True,
        string=_("Advance"),
        help=_("Number advance payment."),
        copy=False,
    )
    payment_type_id = fields.Many2one(
        comodel_name='account.account.apply',
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        domain=_get_payment_domain,
        string=_("Payment Type"),
        help=_("The type of payment of the Advance."),
    )
    journal_id = fields.Many2one(
        comodel_name='account.journal',
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        domain=[('id', 'in', [])],
        string=_("Journal"),
        help=_("Journal."),
    )
    register_date = fields.Datetime(
        default=fields.Datetime.now,
        required=True,
        readonly=True,
        string=_("Register date"),
        states={'draft': [('readonly', False)]},
        help=_("Register date."),
        copy=False,
    )
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        domain=[('is_company', '=', True),
                ('customer', '=', True)],
        string=_("Customer"),
        help=_("Customer."),
    )
    company_currency_amount = fields.Float(
        digits=(12, 2),
        string=_("Company Currency amount"),
        help=_("Company Currency amount."),
    )
    amount = fields.Float(
        digits=(12, 2),
        required=True,
        readonly=True,
        string=_("Amount with tax"),
        states={'draft': [('readonly', False)]},
        help=_("Amount of advance payment with tax."),
    )
    tax = fields.Float(
        digits=(12, 2),
        readonly=True,
        compute=_compute_tax,
        store=True,
        string=_("- Tax of advance"),
        help=_("Tax of advance payment."),
    )
    rate = fields.Float(
        digits=(12, 2),
        readonly=True,
        string=_("Rate of advance"),
        help=_("Rate of advance payment."),
    )
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        readonly=True,
        compute=_compute_currency,
        store=True,
        domain=[('id', 'in', [])],
        string=_("Currency"),
        help=_("Currency in that makes of advance payment."),
    )
    customer_bank = fields.Many2one(
        comodel_name='res.partner.bank',
        required=False,
        readonly=True,
        string=_("Bank of Customer"),
        states={'draft': [('readonly', False)]},
        help=_("Bank of Customer."),
    )

    account_bank = fields.Many2one(
        comodel_name='res.partner.bank',
        required=False,
        readonly=True,
        domain=[('id', 'in', [])],
        string=_("Account Bank"),
        states={'draft': [('readonly', False)]},
        help=_("Account Bank of Company."),
    )
    check_number = fields.Integer(
        required=False,
        readonly=True,
        string=_("Check number"),
        states={'draft': [('readonly', False)]},
        help=_("Check number."),
    )
    related_check = fields.Many2one(
        comodel_name='account.check',
        required=False,
        readonly=True,
        string=_("Folio check"),
        states={'draft': [
            ('readonly', False),
            ('required', False)]},
        help=_("Relationship to the third party check."),
    )
    related_policy = fields.One2many(
        comodel_name='account.move',
        inverse_name='advance_payment',
        required=False,
        readonly=True,
        string=_("Related Policy"),
        help=_("Policies to create and cancel the advance payment."),
        copy=False,
    )
    reference_folio = fields.Char(
        required=False,
        readonly=True,
        string=_("Reference folio"),
        states={'draft': [
            ('readonly', False),
            ('required', False)]},
        help=_("Reference folio."),
    )
    initial_balance = fields.Float(
        readonly=True,
        compute=_compute_initial_balance,
        store=True,
        string=_("Initial Balance"),
        help=_("Initial Balance of advance payment."),
    )
    line_ids = fields.One2many(
        comodel_name='advance.payment.line',
        inverse_name='advance_payment_id',
        required=False,
        readonly=True,
        string=_("Advance payment applications"),
        help=_("Advance payment applications."),
        copy=False,
    )
    current_balance = fields.Float(
        readonly=True,
        compute=_compute_balance,
        string=_("Current Balance"),
        help=_("Current balance, this is a result of debiting "
               "the applications of the advance payment to the opening balance."),
    )
    state = fields.Selection(
        default="draft",
        selection=state_list,
        track_visibility='onchange',
        string=_("State"),
        help=_("Document state."),
    )
    observations = fields.Text(
        size=300,
        required=False,
        readonly=True,
        string=_("Observations"),
        states={'draft': [('readonly', False)]},
        help=_("Advance payment observations."),
    )
    user_generated = fields.Many2one(
        comodel_name='res.users',
        required=False,
        readonly=True,
        string=_("User generated"),
        help=_("The user who generates the document.")
    )
    user_canceled = fields.Many2one(
        comodel_name='res.users',
        required=False,
        readonly=True,
        string=_("User who canceled"),
        help=_("The user who canceled the document.")
    )
    cancellation_date = fields.Datetime(
        required=False,
        readonly=True,
        string=_("Cancellation date"),
        help=_("Date of cancellation of the document."),
    )
    # computed fields
    hide_check_number = fields.Boolean(
        string="Hide Check Number",
        compute=_compute_hide
    )
    hide_customer_account = fields.Boolean(
        string="Hide Customer Account",
        compute=_compute_hide
    )
    hide_customer_bank = fields.Boolean(
        string="Hide Customer Bank",
        compute=_compute_hide
    )
    hide_reference_folio = fields.Boolean(
        string="Hide Reference Folio",
        compute=_compute_hide
    )
    hide_account_bank = fields.Boolean(
        string="Hide Account Bank",
        compute=_compute_hide
    )
    hide_related_check = fields.Boolean(
        string="Hide Check",
        compute=_compute_hide
    )
    hide_line_ids = fields.Boolean(
        string="Hide line",
        compute=_compute_hide,
    )
    hide_policy_ids = fields.Boolean(
        string="Hide policy",
        compute=_compute_hide,
    )

    way_to_payment_id = fields.Many2one('pay.method', string='Way to payment', required=True, states={'draft': [('readonly', False)]})
    cfdi_version = fields.Many2one(
        comodel_name="cfdi.version",
        required=False,
        string=_("CFDI Version"),
        help=_("Assign to CFDI version."),
        copy=False,
    )

    cfdi_sign_ids = fields.One2many('cfdi.sign', 'advance_payment_id')
    state_document_list = [
        ('not_signed', _('Not signed')),
        ('signed', _('Signed')),
        ('cancelled', _('Canceled')),
    ]
    state_document = fields.Selection(selection=state_document_list, string=_('State document'), default='not_signed', nocopy=True)
    sign_state = fields.Boolean(store=True)

    # General (Public) methods
    def get_company(self):
        """Returns the company."""
        company = self.env[
            'res.company']._company_default_get(
            'advance.payment')
        return self.env.user.company_id

    def get_current_period(self):
        """Returns fiscal period of date with only
        month and year."""
        month_year = datetime.now().strftime("%m/%Y")
        return self.env['account.period'].search(
            [('name', '=', month_year)])

    def _get_tax(self):
        """Returns the tax established in the accounting
        configuration of the company."""
        sale_tax = self.env.user.company_id.tax_provision_customer
        if len(sale_tax) == 0:
            raise Warning(
                _("Is necessary save the configuration account config settings")
            )
        return sale_tax

    @api.multi
    def button_generate(self):
        """Generates the down payment and creates the policy or
        check according to the type of payment."""
        #self.cancel_cfdi()
        #return True
        self.currency_company = self.get_company().currency_id
        self.currency_journal = self.currency_id
        if self.journal_id.cfdi_version and self.journal_id.invoice_sequence_id:
            self.number = self.env['ir.sequence'].next_by_id(self.journal_id.invoice_sequence_id.id)
        else:
            self.number = self.env['ir.sequence'].get('advance.payment')
        self.user_generated = self._uid
        if self.payment_type_id.code == 'check':
            # Create the check of advance payment
            check = self._create_check()
            if check:
                self._restructure_check(check)
                check.ensure_one()
                check.signal_workflow('draft_router')
                self.related_check = check.id
            else:
                return False
        # Create the policy of advance payment
        policy = self._create_policy()
        if policy is None:
            return False
        self.generate()
        return True

    @api.multi
    def sign_cfdi(self):
        if self.journal_id.cfdi_version:
            cfdi = self.env['cfdi33']
            certificate = cfdi.certificate()
            move_id = self.related_policy[0]
            comprobante = cfdi.Comprobante()
            version = self.journal_id.cfdi_version
            comprobante.Version.value = version.version
            comprobante.NoCertificado.value = certificate.get('serial_number')
            comprobante.Certificado.value = certificate.get('cer_base64')
            comprobante.TipoDeComprobante.value = 'I'
            comprobante.Serie.value = self.get_serie()
            comprobante.Folio.value = self.number
            comprobante.Fecha.value = cfdi.issue_datetime(self.register_date)
            comprobante.MetodoPago.value = 'PUE'
            comprobante.FormaPago.value = self.way_to_payment_id.code
            comprobante.LugarExpedicion.value = cfdi.expedition_place(
                self.journal_id)
            comprobante.Moneda.value = self.currency_id.name
            comprobante.TipoCambio.value = self.get_rate()
            comprobante.CondicionesDePago.value = 'Anticipo'
            comprobante.Total.value = self.amount
            comprobante.SubTotal.value = "{0:.2f}".format(self.initial_balance)
            comprobante.schemaLocation.value = [
                "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd"
            ]
            emisor = cfdi.issuing()
            emisor.RegimenFiscal.value = '601'
            comprobante.Emisor = emisor

            receptor = cfdi.receiver(self.partner_id)
            receptor.UsoCFDI.value = 'P01'
            comprobante.Receptor = receptor

            product = self.env['product.template'].search(
                [('default_code', '=', 'Anticipo')])
            if product:
                concepto = cfdi.Concepto()
                concepto.ClaveProdServ.value = product.line.sat_product.code
                concepto.NoIdentificacion.value = product.default_code
                concepto.Cantidad.value = 1.0
                concepto.ClaveUnidad.value = product.uom_id.code
                concepto.Unidad.value = product.uom_id.name
                concepto.Descripcion.value = product.name
                concepto.ValorUnitario.value = float("{0:.2f}".format(
                    self.initial_balance))
                concepto.Importe.value = concepto.Cantidad.value * concepto.ValorUnitario.value

                concepto.Impuestos = cfdi.Impuestos()

                traslado = cfdi.Traslado()
                traslado.Base.value = concepto.Importe.value
                traslado.Importe.value = "{0:.2f}".format(self.tax)
                traslado.Impuesto.value = '002'
                traslado.TasaOCuota.value = '0.160000'
                traslado.TipoFactor.value = 'Tasa'

                concepto.Impuestos.Traslados = cfdi.Traslados()
                concepto.Impuestos.Traslados.Traslado.value = [traslado]

                conceptos = cfdi.Conceptos()
                conceptos.Concepto.value = [concepto]
                comprobante.Conceptos = conceptos

                comprobante.Impuestos = cfdi.Impuestos()
                comprobante.Impuestos.Traslados = cfdi.Traslados()

                traslado_base = cfdi.Traslado()
                traslado_base.Importe.value = "{0:.2f}".format(self.tax)
                traslado_base.Impuesto.value = '002'
                traslado_base.TasaOCuota.value = '0.160000'
                traslado_base.TipoFactor.value = 'Tasa'
                comprobante.Impuestos.Traslados.Traslado.value = [traslado_base]
                comprobante.Impuestos.TotalImpuestosTrasladados.value = "{0:.2f}".format(self.tax)

                addenda = cfdi.Addenda()

                folios = cfdi.Folios()
                folios.folioERP.value = self.number
                folios.codigoCliente.value = self.partner_id.client_number
                addenda.Folios = folios

                addenda.DomicilioEmisor = cfdi.issuing_address(
                    self.journal_id.address_invoice_company_id)

                addenda.DomicilioReceptor = cfdi.receiver_address(
                    self.partner_id)

                comprobante.Addenda = addenda

                xml_cfdi = cfdi.XmlCfdi()

                # Generate dom Xml
                dom_ap = xml_cfdi.object2xml(comprobante)

                path_xml_ap = "{}{}.xml".format(cfdi.path_tmp(), cfdi.uuid())

                dom_ap, node = xml_cfdi.rm_node_xml(dom_ap, 'cfdi:Addenda')
                xml_cfdi.xml2file(dom_ap, filename=path_xml_ap)

                original_string = xml_cfdi.xml_transform_xslt(
                    version.version, path_xml_ap, to_return='path')
                comprobante.Sello.value = xml_cfdi.xml_sign(
                    original_string, certificate.get('pem_key'))
                comprobante.Addenda = None
                dom_ap = xml_cfdi.object2xml(comprobante)
                # Convert Dom Xml to String
                xml_cfdi.xml2file(dom_ap, filename=path_xml_ap)
                xml_string_without_sign = xml_cfdi.read_file(path_xml_ap)

                xml_cfdi.xml2file(dom_ap, filename=path_xml_ap)
                ok = xml_cfdi.xml_validate_xsd(version.version, path_xml_ap)
                if ok:
                    signature_date = fields.Datetime.now()
                    result_sign = cfdi.sign_cfdi_xml(
                        version, 'sign', dom_ap, 'customer_advance_payment')
                    sos_path = cfdi.tmp_file()
                    xml_cfdi.write_file(result_sign.get(
                        'contentXml'), sos_path)
                    signed_original_string = xml_cfdi.xml_transform_xslt(
                        version.version, sos_path, _type='tfd',
                        to_return='content')
                    dom_ap.append(node)
                    node_addenda = xml_cfdi.str2dom(xml_cfdi.domXml2str(
                        dom_ap))
                    node_addenda = node_addenda.firstChild.getElementsByTagName('cfdi:Addenda')
                    xml_signed = result_sign.get('contentDom')
                    if len(node_addenda) > 0:
                        xml_signed.firstChild.appendChild(node_addenda[0])
                    cfdi_sign_params = {
                        'original_string': xml_cfdi.read_file(original_string),
                        'certificate_number': comprobante.NoCertificado.value,
                        'certificate': comprobante.Certificado.value,
                        'stamp': comprobante.Sello.value,

                        'signature_date': signature_date,
                        'move_id': move_id.id,
                        'signed_original_string': signed_original_string,
                        'uuid': result_sign.get('uuid'),
                        'xml': xml_cfdi.encode64(xml_string_without_sign),
                        'xml_signed': xml_cfdi.encode64(xml_signed.toxml(
                            encoding="utf-8")),
                        'advance_payment_id': self.id,
                        'state_document': 'signed'
                    }
                    self.cfdi_version = version.id
                    self.state_document = 'signed'
                    cfdi_sign_id = self.env['cfdi.sign'].create(
                        cfdi_sign_params)
                    cfdi_sign_id.refresh_pdf_payment()
                    uuid_policy = []
                    self.sign_state = True
                    uuid_policy.append({
                        'document_id': self.id,
                        'document_obj': self._model,
                        'partner_folio': self.number,
                        'move': 'origen',
                        'uuid': cfdi_sign_params.get('uuid')
                    })
                    move_id.write(
                        {'cfdi_table_ids': [(0, 0, x) for x in uuid_policy]})
            else:
                raise MissingError(_('Not exists the product with default code "Anticipo", is necessary config'))
        else:
            raise Warning(_('Configure journal to sign document\
                CFDI version 3.3.'))

    @api.model
    def cancel_cfdi(self):
        if self.cfdi_version:
            if len(self.cfdi_sign_ids) > 0:
                cfdi = self.env['cfdi33']
                xml_cfdi = cfdi.XmlCfdi()
                for cfdi_sign_id in self.cfdi_sign_ids:
                    signature_cancellation_date = fields.Datetime.now()
                    cancel_path = cfdi.tmp_file()
                    cfdi_cancel = xml_cfdi.decode64(cfdi_sign_id.xml_signed)
                    xml_cfdi.write_file(cfdi_cancel, cancel_path)
                    data = xml_cfdi.str2domXml(cancel_path)
                    data = data.getroot()
                    cfdi.sign_cfdi_xml(self.cfdi_version, 'cancel', data, 'customer_advance_payment')
                    cfdi_sign_id.update({'signature_cancellation_date': signature_cancellation_date,
                                         'state_document': 'cancelled'})
                    self.state_document = 'cancelled'


    @api.model
    def get_rate(self):
        _rate = None
        if self.currency_id.id != self.get_company().currency_id.id:
            _rate = self.rate
        return _rate

    @api.model
    def get_serie(self):
        if self.journal_id.invoice_sequence_id:
            _serie = self.journal_id.invoice_sequence_id.prefix
            if _serie:
                _serie = re.sub(r'\W+', '', _serie)
        else:
            raise MissingError(_('Is necessary config sequence of document in journal'))
        return _serie or None

    @api.multi
    def button_cancel(self):
        """This method cancels the advance payment."""
        if self.state not in ('generated', 'draft'):
            raise Warning(
                _("In order to cancel an advance you have to cancel the payments related to this.")
            )
        self.cancel()
        self.user_canceled = self._uid
        self.cancellation_date = fields.Datetime.now()
        creation = None
        cancellation = None
        for policy in self.related_policy:
            creation = policy
            # copy the creation policy
            cancellation = policy.copy()
        # Reverse the creation policy
        self.create_cancel_policy(creation, cancellation)
        if self.payment_type_id.code == 'check':
            self.related_check.action_return()
            # self.related_check.action_cancel()

        self.cancel_cfdi()
        return True

    @api.multi
    def create_cancel_policy(self, creation_policy, cancellation_policy):
        """Reverse the creation policy."""
        cancellation_policy.button_cancel()
        for l in cancellation_policy.line_id:
            if l.amount_currency and l.currency_id:
                if l.amount_currency < 0:
                    amount_currency = abs(l.amount_currency)
                else:
                    amount_currency = - (l.amount_currency)
            else:
                amount_currency = l.amount_currency
            self._cr.execute(
                "UPDATE account_move_line SET debit=%s,"
                "credit=%s, amount_currency=%s WHERE move_id=%s and id=%s",
                (l.credit, l.debit, amount_currency,
                 cancellation_policy.id,
                 l.id))
        ref_canceled = _("Cancelado ") + (
            self.number + " " + creation_policy.name)
        cancellation_policy.ref = ref_canceled
        cancellation_policy.period_id = self.get_current_period().id
        cancellation_policy.date = fields.Datetime.now()
        cancellation_policy.button_validate()
        creation_policy.ref = _("Cancelado ") + self.number
        return True

    @api.one
    def write(self, vals):
        if self.state == 'draft':
            if 'amount' in vals:
                ib = vals.get('amount') - self._get_tax_from_amount(vals.get('amount'))
                vals.update({'initial_balance': ib})
        return super(AdvancePayment, self).write(vals)

    @api.model
    def create(self, values):
        """Override original create method"""
        self.validate_check_number(values)
        if 'amount' in values:
            if values['amount'] <= 0:
                raise Warning(_("The amount must not be less than or equal to zero."))
        if 'state' in values:
            if values.get('state', '') == 'draft':
                ib = values.get('amount') - self._get_tax_from_amount(values.get('amount'))
                values.update({'initial_balance': ib})
        return super(AdvancePayment, self).create(values)

    @api.multi
    def unlink(self):
        """Override original create method"""
        if self.state not in ('draft',):
            raise Warning(
                _("You cannot delete an advance payment that is in different state to draft.")
            )
        return super(AdvancePayment, self).unlink()

    @api.onchange('payment_type_id')
    def onchange_payment_type(self):
        """Returns the domain of journal, depending of
        the cost center assigned to user."""
        self._clean_fields()
        if not self.payment_type_id:
            return {'domain': {
                'journal_id': [('id', 'in', [])]}
            }
        return self.get_journal_domain()

    @api.onchange('journal_id')
    def onchange_journal_id(self):
        """Returns the domain of bank, depending of
            the currency of journal."""
        currency = []
        if not self.journal_id:
            return {'domain': {'account_bank': [('currency2_id', 'in', currency), ('journal_id', '!=', None)]}}
        else:
            currency = self.journal_id.currency.id or \
                       self.journal_id.company_id.currency_id.id
            domain = {'account_bank': [('currency2_id', 'in', [currency]), ('journal_id', '!=', None)]}
            warning = {}
            if currency != self.currency_id.id:
                if self.amount > 0:
                    self.amount = 0.0
                if self.account_bank:
                    self.account_bank = False
            self.currency_id = currency
            if self.payment_type_id.code == 'cash':
                d = self.journal_id.default_debit_account_id.id
                c = self.journal_id.default_credit_account_id.id
                if d == c:
                    self.currency_id = False
                    self.journal_id = False
                    self.amount = 0.0
                    warning = {
                            'title': _("Journal Config"),
                            'message': _("The accounts credit and debit be the same, select a journal valid for cash..")}
                    domain = {}
        return {'domain': domain, 'warning': warning, 'value': {'currency_id': currency}}

    @api.multi
    def get_journal_domain(self):
        # type_journal = ""
        # if self.payment_type_id.code in ('credit_card', 'cash'):
        #     type_journal = 'cash'
        # elif self.payment_type_id.code in ('deposit', 'check'):
        #     type_journal = 'bank'
        # Performs the journal search by cost center and type.
        journal_ids = self.env['account.journal'].search(
            [self.journal_domain(),
             #('type', '=', type_journal),
            # ('account_account_apply_id', '=', self.payment_type_id.id),
             ('centro_costos', 'in', [
                 x.id for x in self.env['res.users'].browse(
                     self._uid).centro_costo_id])])
        domain = {'domain': {'journal_id': [
            ('id', 'in', [ji.id for ji in journal_ids])]
        }}
        return domain

    @api.multi
    def journal_domain(self):
        journal_ids = []
        domain = [('id', 'in', journal_ids)]
        # Performs the journal search by advance code.
        account_apply_ids = self.env[
            'account.account.apply'].search(
            [('code', 'in', ['advance'])])
        if account_apply_ids:
            apply_id_list = [
                aa.id for aa in account_apply_ids
                ]
            account_journal_ids = self.env[
                'account.journal'].search(
                [('account_account_apply_id', 'in', apply_id_list)])
            journal_ids = [aj.id for aj in account_journal_ids]
            domain = ('id', 'in', journal_ids)
        return domain

    @api.one
    def _clean_fields(self):
        """This method is called to clear fields when
        changing the payment type."""
        self.journal_id = False
        self.amount = 0
        self.tax = 0
        self.currency_id = False
        self.customer_bank = False
        self.customer_account_number = ""
        self.account_bank = False
        self.check_number = False
        self.reference_folio = ""

    @api.multi
    def _create_policy(self):
        """This method create the policy of movements"""
        vals = {}
        vals['advance_payment'] = self.id
        vals['journal_id'] = self.journal_id.id
        vals['period_id'] = self.get_current_period().id
        vals['date'] = fields.Datetime.now()
        vals['ref'] = self.number
        vals['line_id'] = [(0, 0, x) for x in self.prepare_policy_lines()]
        new_id = self.env['account.move'].create(vals)
        if new_id.journal_id.entry_posted:
            new_id.post()
            if not self.journal_id.cfdi_version:
                self.update({'number': new_id.name})
            # new_id.line_id.write({'ref': new_id.name})
            new_id.update({'ref': self.number})
            for move_line in new_id.line_id:
                if move_line.name != '/':
                    move_line.update({'name': new_id.name})
        return new_id

    @api.multi
    def prepare_policy_lines(self):
        """Method created to prepare the lines of the policy with taxes."""
        lines_vals = []
        d = self.account_bank.journal_id.default_debit_account_id.id
        c = self.journal_id.default_credit_account_id.id
        tax_account = self._get_tax().account_paid_voucher_id.id
        if self.payment_type_id.code == 'cash':
            d = self.journal_id.default_debit_account_id.id
            c = self.journal_id.default_credit_account_id.id
        debit_account = d
        credit_account = c
        if self.currency_company != self.currency_journal:
            amount = self.company_currency_amount = (
                self.currency_journal.with_context(
                    todo_ttype='payment').compute(
                    self.amount, self.currency_company, True)
            )
            tax = self.currency_journal.with_context(
                todo_ttype='payment'
            ).compute(self.tax, self.currency_company, True)
            self.rate = self.currency_journal.with_context(
                todo_ttype='payment').compute(
                1.0, self.currency_company, True
            )
            lines_vals.append({
                'name': '/',
                'partner_id': self.partner_id.id,
                'account_id': debit_account,
                'debit': amount,
                'credit': 0.00,
                'amount_currency': self.amount
            }
            )
            lines_vals.append({
                'name': '',
                'partner_id': self.partner_id.id,
                'account_id': credit_account,
                'debit': 0.00,
                'credit': amount - tax,
                'amount_currency': - (self.amount - self.tax)
            }
            )
            lines_vals.append({
                'name': '',
                'partner_id': self.partner_id.id,
                'account_id': tax_account,
                'debit': 0.00,
                'credit': tax,
                'amount_currency': - self.tax
            }
            )
        elif self.currency_company == self.currency_journal:
            self.rate = 1.0
            lines_vals.append({
                'name': '/',
                'partner_id': self.partner_id.id,
                'account_id': debit_account,
                'debit': self.amount,
                'credit': 0.00,
            }
            )
            lines_vals.append({
                'name': '',
                'partner_id': self.partner_id.id,
                'account_id': credit_account,
                'debit': 0.00,
                'credit': self.amount - self.tax,
            }
            )
            lines_vals.append({
                'name': '',
                'partner_id': self.partner_id.id,
                'account_id': tax_account,
                'debit': 0.00,
                'credit': self.tax,
            }
            )
        return lines_vals

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            partner_id = self.partner_id
            partner_banks = partner_id.bank_ids
            partner_banks_list = [bank.id for bank in partner_banks]
            return {
                'domain': {
                    'customer_bank': [('id', 'in', partner_banks_list)]}}

    @api.model
    def _create_check(self):
        """ This method creates a check
        and returns the check object generated."""
        payment_date = (
            self.register_date if
            self.register_date else
            fields.Date.context_today(self)
        )
        if self.currency_journal != self.currency_company:
            self.company_currency_amount = (
                self.currency_journal.with_context(
                    todo_ttype='payment'
                ).compute(self.amount, self.currency_company, True))
            self.rate = (
                self.currency_journal.with_context(
                    todo_ttype='payment'
                ).compute(1.0, self.currency_company, True))
        else:
            self.company_currency_amount = None
            self.rate = 1.0
        check = {
            'number': self.check_number,
            'amount': self.amount,
            'company_currency_amount': self.company_currency_amount,
            'other_partner_id': self.partner_id.id,
            'type': 'issue_check',
            'issue_date': fields.Date.context_today(self),
            'payment_date': payment_date,
            'advance_payment': self.id,
            'company_id': self.get_company().id,
            'bank_id': self.customer_bank.bank.id,
        }
        return self.env['account.check'].create(check)

    @api.multi
    def _restructure_check(self, check):
        """ This method restructures a check to be
        third party."""
        if check:
            query = """
            UPDATE account_check SET journal_id = %s,
                                    type = 'third_check',
                                    destiny_partner_id = NULL,
                                    source_partner_id = %s,
                                    other_partner_id = NULL
                                    WHERE id = %s """
        self._cr.execute(query, (
            self.journal_id.id,
            self.partner_id.id,
            check.id)
                         )
        return True

    @api.one
    def generate(self):
        """Write the generated state of the down payment."""
        self.state = 'generated'
        return True

    @api.one
    def apply_partial(self):
        """Write the applied partial state of the down payment."""
        self.state = 'applied partial'
        return True

    @api.one
    def apply(self):
        """Write the applied state of the down payment."""
        self.state = 'applied'
        return True

    @api.one
    def cancel(self):
        """Write the cancelled state of the down payment."""
        self.state = 'canceled'
        return True

    @api.model
    def validate_check_number(self, values):
        account_apply = self.env[
            'account.account.apply'].search(
            [('code', 'in', ['check'])])
        if values['payment_type_id'] == account_apply.id:
            if values['check_number'] <= 0:
                raise Warning(
                    _("The check number it's not correct.")
                )
        return True
