# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class AccountMove(models.Model):
    """Extended model to register the applied of advance payment."""

    # odoo model properties
    _inherit = 'account.move'

    # custom fields
    advance_payment = fields.Many2one(
        comodel_name='advance.payment',
        required=False,
        readonly=True,
        ondelete='cascade',
        string=_("Advance Number"),
        help=_("Number advance payment."),
    )
