# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api


class AccountVoucher(models.Model):
    """Extended model to register the applied of advance payment."""

    # odoo model properties
    _inherit = 'account.voucher'

    # General (Public) methods
    @api.multi
    def action_move_line_create(self):
        res = super(AccountVoucher, self).action_move_line_create()
        if res:
            company_id = self.env.user.company_id
            line_ids = [line for line in self.line_dr_ids if line.amount > 0]
            for line in line_ids:
                policy_id = line.move_line_id.move_id.id
                advance_payment = self.env['advance.payment'].search([
                    ('related_policy', '=', policy_id)])
                if advance_payment and advance_payment.state != 'cancel' and line.amount > 0.0:
                    journal_currency = (self.journal_id.currency or
                                        self.journal_id.company_id.currency_id)
                    advance_currency = (line.move_line_id.journal_id.currency or
                                        line.move_line_id.journal_id.company_id.currency_id)
                    payment_rate = 1.0
                    if journal_currency != advance_currency:
                        if journal_currency.id != company_id.currency_id.id and advance_currency.id == company_id.currency_id.id:
                            amount = journal_currency.with_context(
                                todo_ttype='receipt'
                            ).compute(line.amount, advance_currency, True)
                            payment_rate = journal_currency.with_context(
                                todo_ttype='receipt'
                            ).compute(1.0, advance_currency, True)
                        elif journal_currency.id == company_id.currency_id.id and advance_currency.id == company_id.currency_id.id:
                            amount = journal_currency.with_context(
                                todo_ttype='payment'
                            ).compute(line.amount, advance_currency, True)
                            payment_rate = advance_currency.with_context(
                                todo_ttype='payment'
                            ).compute(1.0, journal_currency, True)
                        elif journal_currency.id == company_id.currency_id.id and advance_currency.id != company_id.currency_id.id:
                            amount = journal_currency.with_context(
                                todo_ttype='payment'
                            ).compute(line.amount, advance_currency, True)
                            payment_rate = advance_currency.with_context(
                                todo_ttype='payment'
                            ).compute(1.0, journal_currency, True)
                    else:
                        amount = line.amount
                    vals = {}
                    vals.update({
                        'advance_payment_id': advance_payment.id,
                        'number': self.id,
                        'state': 'accepted',
                        'amount': amount,
                        'policy': self.move_id.id,
                        'payment_amount': line.amount,
                        'payment_rate': payment_rate,
                        'currency': journal_currency.id,
                        'payment_date': fields.Datetime.now(),
                        'user_applied': self._uid,
                    })
                    if self.env['advance.payment.line'].create(vals):
                        if line.reconcile:
                            advance_payment.apply()
                        else:
                            advance_payment.apply_partial()
        return res

    @api.multi
    def cancel_voucher(self):
        res = super(AccountVoucher, self).cancel_voucher()
        if res:
            for vl in self.line_dr_ids:
                policy_id = vl.move_line_id.move_id.id
                advance_payment = self.env['advance.payment'].search([
                    ('related_policy', '=', policy_id)])
                for apl in advance_payment.line_ids:
                    if apl.number.id == self.id:
                        apl.state = 'canceled'
                        apl.amount = 0.0
                        apl.user_canceled = self._uid
                for l in advance_payment.line_ids:
                    if l.state == 'accepted':
                        advance_payment.apply_partial()
                        break
                    else:
                        advance_payment.generate()
        return res


    def recompute_voucher_lines(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date,
                                context):

        res = super(AccountVoucher, self).recompute_voucher_lines \
                (cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date,
                                            context)
        invoice_obj = self.pool.get('account.invoice')
        acc_move_line_obj = self.pool.get('account.move.line')
        cur_obj = self.pool.get('res.currency')
        uid_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company_iva = uid_obj.company_id.tax_provision_customer.amount
        if journal_id:
            journal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
            if journal.account_account_apply_id.code == 'advance':
                if res['value']['line_dr_ids']:
                    for move_line in res['value']['line_dr_ids']:
                        if 'move_line_id' in move_line:
                            acc_move_line_id = acc_move_line_obj.browse(cr, uid, move_line['move_line_id'], context=context)
                            if acc_move_line_id.reconcile_partial_id:
                                for payment_line in acc_move_line_id.reconcile_partial_id.line_partial_ids:
                                    if payment_line.id == acc_move_line_id.id:
                                        continue
                                    rate_sql = "select get_rate('sale', '{}', '{}')".format(acc_move_line_id.currency_id.name,\
                                                                                                       payment_line.date_created)
                                    cr.execute(rate_sql)
                                    rate = 1 / cr.fetchone()[0]

                                    current_balance = acc_move_line_id.move_id.advance_payment.current_balance
                                    if not current_balance:
                                        invoice_sql = "select id from account_invoice where move_id='{}'".format(acc_move_line_id.move_id.id)
                                        cr.execute(invoice_sql)
                                        invoice = cr.fetchone()

                                        if invoice:
                                            invoice_id=invoice_obj.browse(cr, uid, invoice[0], context=context)
                                            if invoice_id:
                                                current_balance = invoice_id.residual
                                        
                                    if acc_move_line_id.currency_id.id == journal.currency.id and journal.currency.id != uid_obj.company_id.currency_id.id:
                                        move_line.update(
                                            {
                                                'original_amount': round(move_line['original_amount'], 1),
                                                'amount_original': round(move_line['amount_original'], 1),
                                                'original_balance': round(current_balance, 1),
                                                'amount_unreconciled': round(current_balance, 1)
                                            })
                                    elif acc_move_line_id.currency_id.id != journal.currency.id and journal.currency.id != uid_obj.company_id.currency_id.id:
                                        if not acc_move_line_id.currency_id.id:
                                            payment_currency = uid_obj.company_id.currency_id
                                        else:
                                            payment_currency = acc_move_line_id.currency_id
                                        if not journal.currency:
                                            journal_currency = uid_obj.company_id.currency_id
                                        else:
                                            journal_currency = journal.currency
                                        move_line.update(
                                            {
                                                'original_amount': round(move_line['original_amount'], 1),
                                                'amount_original': round(move_line['amount_original'], 1),
                                                'original_balance': round(current_balance, 1),
                                                'amount_unreconciled': cur_obj._compute(cr,uid,payment_currency,journal_currency,current_balance,True,context)
                                            })

                                    elif not acc_move_line_id.currency_id.id and not journal.currency.id:
                                        move_line.update(
                                            {
                                                'original_amount': round(move_line['original_amount'], 1),
                                                'amount_original': round(move_line['amount_original'], 1),
                                                'original_balance': round(new_residual, 1),
                                                'amount_unreconciled': round(new_residual, 1)
                                            })

                                    elif acc_move_line_id.currency_id.id and not journal.currency.id:
                                        move_line.update(
                                            {
                                                'original_amount': round(move_line['original_amount'], 1),
                                                'amount_original': round(move_line['amount_original'], 1),
                                                'original_balance': new_usd_residual,
                                                'amount_unreconciled': round(new_residual, 1)
                                            })

                            else:
                                move_line.update(
                                    {
                                        'original_amount': round(move_line['original_amount'], 1),
                                        'amount_original': round(move_line['amount_original'], 1),
                                        'original_balance': round(move_line['original_balance'], 1),
                                        'amount_unreconciled': round(move_line['amount_unreconciled'], 1)
                                    }
                                )

        return res
