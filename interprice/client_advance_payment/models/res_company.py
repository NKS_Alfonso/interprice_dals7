# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class ResCompanyInherit(models.Model):
    _inherit = 'res.company'


    advance_payments_tax = fields.Boolean(string=_("Use tax in advance payments"),
                                          help=_("If selected then the sales tax will be applied to advanced payments"))
