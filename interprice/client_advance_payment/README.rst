Anticipo Clientes
=================

v1.0.3
====
Se agrego botón para timbrado manual.

* Se agrego un botón para que el timbrado del anticipo se haga de manera manual y no en automatico.
* Se elimino el campo cuenta del clienta.
.. figure:: ../client_advance_payment/static/img/button_sign.png
    :alt: Nuevo botón de timbrado. 
    :width: 80%

v1.0.2
====
Timbrado de anticipos

* Si el diario tiene definido 'cfdi_version' sera timbrado de lo contrario no.
* Depende del modulo de l10n_es_account_invoice_sequence, para que tenga un secuencia diferente
la  poliza y el documento de anticipo.

v1.0.1
=====
- **Ubicación del nuevo menú dentro de Contabilidad Clientes.**

.. figure:: ../client_advance_payment/static/img/advance_paymnet.png
    :alt: Nuevo campo de ubicación
    :width: 80%

- **Reporte Informes/Contabilidad/ menú Anticipo clientes.**

.. figure:: ../client_advance_payment/static/img/Report_advance_payment.png
    :alt: Nuevo campo de ubicación
    :width: 80%

- **Accesos Configuración/Usuarios menú Anticipo.**

.. figure:: ../client_advance_payment/static/img/Access_users.png
    :alt: Nuevo campo de ubicación
    :width: 80%