# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, _


class ResCountry(models.Model):
    _inherit = 'res.country'


    required_settlement = fields.Boolean(
        string=_("Required Settlement"),
        help=_("""If this is checked, then the settlement and it's type field are gonna be required
            if not, the the settlement field and type of settlement will be invisible""")
    )
