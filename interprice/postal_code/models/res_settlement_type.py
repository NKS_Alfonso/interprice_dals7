# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, _


class ResSettlementType(models.Model):
    _inherit = 'mail.thread'
    _name = 'res.settlement.type'
    _rec_name = 'name'

    name = fields.Char(
        string=_('Type of Settlement'),
        help=_('Type of Settlement')
        )
