# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _
from lxml import etree
import logging
_logger = logging.getLogger(__name__)

class InheritResPartner(models.Model):
    _inherit = 'res.partner'

    street2 = fields.Many2one(
        comodel_name='res.state.city.colony',
        string=_('Settlement'),
        help=_('Name of Settlement')
        )
    zip = fields.Many2one(
        comodel_name='res.postal.code',
        string=_('Zip code'),
        help=_('Number of zip code')
        )
    settlement_type = fields.Many2one(
        comodel_name='res.settlement.type',
        string=_('Type of Settlement'),
        help=_('Type of Settlement')
        )
    street2_id = fields.Integer(
        string='street2_id'
    )
    zip_code_id = fields.Integer(
        string='zip_code_id'
    )
    country_required_settlement = fields.Boolean(
        string="required settlement",
        compute="_get_country_settings"
    )

    @api.one
    @api.depends('country_id')
    def _get_country_settings(self):
        if self.country_id:
            if self.country_id.required_settlement:
                self.update({'country_required_settlement': True})
                self.street2 = False
                self.settlement_type = False
            else:
                return False
        else:
            return False


    def _address_fields(self, cr, uid, context=None):
        """Returns the list of the address fields that synchronizes
        from the parent when the flag is set use_parent_address."""
        res = super(
            InheritResPartner, self)._address_fields(cr, uid, context=None)
        res.extend(['street2', 'zip', 'settlement_type'])
        return res

    def fields_view_get_address(self, cr, uid, arch, context=None):
        street2 = _('Colony...')
        zip = _('ZIP...')
        settlement_type = _('Type of settlement...')
        country_id = _('Country...')
        state_id = _('State...')
        city_id = _('City/Municipality...')
        from lxml import etree
        res = super(
            InheritResPartner, self).\
            fields_view_get_address(cr, uid, arch, context=context)
        doc = etree.fromstring(res)
        node1 = doc.xpath(
            '//form/sheet/group/group/div/field[@name="street2"]')[0]
        node1.getparent().replace(
            node1 ,etree.fromstring("""
                <field name="street2" placeholder="%s"
                on_change="on_change_street2(street2, zip, city_id)"
                modifiers="{&quot;invisible&quot;: [[&quot;country_required_settlement&quot;,&quot;=&quot;,false]],
                &quot;required&quot;: [[&quot;country_required_settlement&quot;,&quot;=&quot;,true]]}"
                options="{'no_open':True, 'no_create':True}"/>
                """ % street2))
        node2 = doc.xpath(
            '//form/sheet/group/group/div/div/field[@name="zip"]')[0]
        node2.getparent().replace(
            node2, etree.fromstring("""
                <field name="zip" placeholder="%s" style="width: 23%%"
                on_change="on_change_zip(zip, street2, city_id)"
                modifiers="{&quot;required&quot;: true}"
                options="{'no_open':True, 'no_create':True}"/>
                """ % zip))
        node3 = doc.xpath(
            '//form/sheet/group/group/div/field[@name="street2"]')[0]
        node3.getparent().\
        insert(3, etree.\
        fromstring("""
            <field name="settlement_type" placeholder="%s"
            options="{'no_open':True, 'no_create':True}"
            modifiers="{&quot;invisible&quot;: [[&quot;country_required_settlement&quot;,&quot;=&quot;,false]],
            &quot;required&quot;: [[&quot;country_required_settlement&quot;,&quot;=&quot;,true]]}"/>
            """ % settlement_type))
        node4 = doc.xpath(
            '//field[@name="country_id"]')[0]
        node4.getparent().replace(
            node4, etree.fromstring(
                """
                <field name="country_id" class="oe_no_button" placeholder="%s"
                options="{'no_open':True, 'no_create':True}"
                on_change="onchange_country_id(country_id, zip)"
                modifiers="{&quot;readonly&quot;: false}"/>
                """ % country_id))
        node5 = doc.xpath(
            '//field[@name="state_id"]')[0]
        node5.getparent().replace(
            node5, etree.fromstring(
                """
                <field name="state_id" class="oe_no_button" placeholder="%s"
                on_change="onchange_state_id(state_id, zip)" style="width: 37%%"
                options="{'no_open':True, 'no_create':True}"
                modifiers="{&quot;readonly&quot;: false}"/>
                """ % state_id))
        node6 = doc.xpath(
            '//field[@name="city_id"]')[0]
        node6.getparent().replace(
            node6, etree.fromstring(
                """
                <field name="city_id" placeholder="%s"
                 style="width: 40%%;" modifiers="{&quot;readonly&quot;: false}"
                 options="{'no_open':True, 'no_create':True}"
                 on_change="onchange_city_id(city_id, zip)"/>
                """ % city_id))
        node7 = doc.xpath(
            '//field[@name="l10n_mx_city2"]')[0]
        node7.getparent().replace(
            node7, etree.fromstring(
                """
                <field name="l10n_mx_city2" modifiers="{&quot;invisible&quot;: true}"/>
                """))
        res = etree.tostring(doc)
        return res

    def fields_view_get(self, cr, user, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if (not view_id) and (view_type == 'form') and context and context.get('force_email', False):
            view_id = self.pool.get('ir.model.data').get_object_reference(
                cr, user, 'base', 'view_pasudo rtner_simple_form')[1]
        res = super(InheritResPartner, self).fields_view_get(
            cr, user, view_id, view_type,
            context, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            fields_get = self.fields_get(
                cr, user, ['street2', 'zip', 'settlement_type'], context)
            res['fields'].update(fields_get)
        return res

    def on_change_zip(self, cr, uid, ids, zip, street2, city_id, context=None):
        city = []
        state = []
        country = []
        zip_code = []
        records = []
        x = []

        if zip and street2:
            #x = self.pool.get('res.state.city.colony').search(cr, uid, ['|',('id', '=', street2),('postal_id', '=', zip)])
            set_record_ids = self.pool.get('res.state.city.colony').search(cr, uid, [('id', '=', street2)])
            set_record_ids1 = self.pool.get('res.state.city.colony').search(cr, uid, [('postal_id', '=', zip)])
            x = set_record_ids + set_record_ids1
            records = self.pool.get('res.state.city.colony').browse(cr, uid, x)

            if records:
                zip_code = records[0].postal_id.id
            # for record in records:
            #     zip_code = record.postal_id.id
            #     break

            if street2 and zip != zip_code:
                set_record_ids = self.pool.get('res.state.city.colony').search(cr, uid, [('id', '=', street2)])
                set_record_ids1 = self.pool.get('res.state.city.colony').search(cr, uid, [('postal_id', '=', zip)])
                x = set_record_ids1 + set_record_ids
                records = self.pool.get('res.state.city.colony').browse(cr, uid, x)

                if records:
                    city = records[0].city_id.id
                    state = records[0].state_id.id
                    country = records[0].country_id.id
                # for record in records:
                #     city = record.city_id.id
                #     state = record.state_id.id
                #     country = record.country_id.id
                #     break

                return {
                    'domain': {
                        'street2': [('postal_id', '=', zip)],
                    },
                    'value': {
                        'city_id': city,
                        'state_id': state,
                        'country_id': country,
                        'street2': False,
                        'settlement_type': False,
                    }
                }


        elif zip and street2 == False:
            set_record_ids = self.pool.get('res.state.city.colony').search(cr, uid, [('postal_id', '=', zip)])
            records = self.pool.get('res.state.city.colony').browse(cr, uid, set_record_ids)

        else:
            return {}

        if records:
            city = records[0].city_id.id
            state = records[0].state_id.id
            country = records[0].country_id.id
            zip_code = records[0].postal_id.id
        # for record in records:
        #     city = record.city_id.id
        #     state = record.state_id.id
        #     country = record.country_id.id
        #     zip_code = record.postal_id.id
        #     break

        if zip == False:
            return {'value': {'zip': False}}

        elif street2 == False and zip:
            return {
                'domain': {
                    'street2': [('postal_id', '=', zip)],
                },
                'value': {
                    'city_id': city,
                    'state_id': state,
                    'country_id': country,
                }
            }
        elif street2 and zip != zip_code:
            return {
                'domain': {
                    'street2': [('postal_id', '=', zip)],
                },
                'value': {
                    'city_id': city,
                    'state_id': state,
                    'country_id': country,
                    'street2': False,
                    'settlement_type': False,
                }
            }
        elif street2 and zip and city_id == False:
            return {
                'domain': {
                    'street2': [('postal_id', '=', zip)],
                },
                'value': {
                    'city_id': city,
                    'state_id': state,
                    'country_id': country,
                }
            }

        elif street2 and zip and city_id:
            return {
                'domain': {
                    'street2': [('postal_id', '=', zip)],
                },
                'value': {
                    'city_id': city,
                    'state_id': state,
                    'country_id': country,
                }
            }
        else:
            return {}

    def on_change_street2(self, cr, uid, ids, street2, zip, city_id, context=None):
        settlement_type = False
        colony = False
        zip_code = []
        records = []
        x = []

        if zip and street2:
            #x = self.pool.get('res.state.city.colony').search(cr, uid, ['|',('id', '=', street2),('postal_id', '=', zip)])
            set_record_ids = self.pool.get('res.state.city.colony').search(cr, uid, [('id', '=', street2)])
            set_record_ids1 = self.pool.get('res.state.city.colony').search(cr, uid, [('postal_id', '=', zip)])
            x = set_record_ids + set_record_ids1
            records = self.pool.get('res.state.city.colony').browse(cr, uid, x)

        elif street2 and zip == False:
            set_record_ids = self.pool.get('res.state.city.colony').search(cr, uid, [('id', '=', street2)])
            records = self.pool.get('res.state.city.colony').browse(cr, uid, set_record_ids)


        if records:
            settlement_type = records[0].settlement_id.id
            colony = records[0].id
            zip_code = records[0].postal_id.id
        # for record in records:
        #     settlement_type = record.settlement_id.id
        #     colony = record.id
        #     zip_code = record.postal_id.id
        #     break

        if street2 == False:
            return {
                'value':
                {
                    'street2': False,
                    'settlement_type': False,
                }
            }

        elif street2 and zip and city_id:
            return {
                'value':
                {
                    'zip': zip,
                    'settlement_type': settlement_type,
                }
            }

        elif street2 and zip == False and city_id:
            return {
                'value':
                {
                    'zip': zip_code,
                    'settlement_type': settlement_type,
                }
            }

        elif street2 == False and city_id == False:
            return {
                'value':
                {
                    'zip': city,
                    'settlement_type': settlement_type,
                }
            }

        elif street2 == False and zip:
            return {
                'value':
                {
                    'zip': zip,
                    'settlement_type': settlement_type,
                }
            }

        elif street2 != colony and zip:
            return {
                'value':
                {
                    'settlement_type': settlement_type,
                }
            }

        elif street2 and city_id == False:
            return {
                'value':
                {
                    'zip': zip_code,
                    'settlement_type': settlement_type,
                }
            }

        else:
            return {}


    def onchange_country_id(self, cr, uid, ids, country_id, zip, context=None):
        if country_id:
            return {'domain': {'state_id': [('country_id', '=', country_id)]}}
        else:
            return {'domain': {'state_id': []}}

    def onchange_state_id(self, cr, uid, ids, state_id, context=None):
        if state_id:
            return {'domain': {'city_id': [('state_id', '=', state_id)]}}
        else:
            return {'domain': {'city_id': []}}

    def onchange_city_id(self, cr, uid, ids, city_id, zip, context=None):
        if city_id and zip == False:
            return {'domain': {'street2': [('city_id', '=', city_id)]}}
        elif city_id and zip:
            return {'domain': {'street2': [('postal_id', '=', zip)]}}
        else:
            return {'domain': {'street2': []}}

    @api.model
    def create(self,vals):
        msg = ""
        res_postal_code = self.env['res.postal.code']
        res_settlement_type = self.env['res.settlement.type']
        res_city = self.env['res.country.state.city']
        res_state = self.env['res.country.state']
        res_country = self.env['res.country']
        v_zip_id = vals.get('zip')
        # Get country required field settings
        postal_code = res_postal_code.browse(v_zip_id)
        if postal_code:
            country_settings = res_country.browse(postal_code.country_id.id)
            required_settlement = country_settings.required_settlement
            v_city_id = vals.get('city_id')
            v_state_id = vals.get('state_id')
            vals_country_id = vals.get('country_id')
            if required_settlement:
                v_street_id = vals.get('street2')
                v_settlement_type_id = vals.get('settlement_type')
                vals_records = [v_street_id, v_settlement_type_id, v_city_id,
                v_state_id, vals_country_id]
                for line in postal_code.line_ids:
                    if vals_records[0] == line.id:
                        if vals_records[1] != line.settlement_id.id:
                            settlement_type = res_settlement_type.browse(vals_records[1])
                            msg += settlement_type.name + '\n'
                        if vals_records[2] != line.city_id.id:
                            city = res_city.browse(vals_records[2])
                            msg += city.name + '\n'
                        if vals_records[3] != line.state_id.id:
                            state = res_state.browse(vals_records[3])
                            msg += state.name + '\n'
                        if vals_records[4] != line.country_id.id:
                            country = res_country.browse(vals_records[4])
                            msg += country.name + '\n'
            else:
                v_street_id = False
                v_settlement_type_id = False
                vals_records = [v_street_id, v_settlement_type_id, v_city_id,
                v_state_id, vals_country_id]
                for line in postal_code.line_ids:
                    if vals_records[0] is False and vals_records[0] is False:
                        if vals_records[2] != line.city_id.id:
                            city = res_city.browse(vals_records[2])
                            msg += city.name + '\n'
                        if vals_records[3] != line.state_id.id:
                            state = res_state.browse(vals_records[3])
                            msg += state.name + '\n'
                        if vals_records[4] != line.country_id.id:
                            country = res_country.browse(vals_records[4])
                            msg += country.name + '\n'
                    else:
                        continue
            if msg:
                throw_msg = _("""The following data does not belong to the
                    zip code, please verify again:
                    %s"""%msg)
                if throw_msg:
                    raise exceptions.Warning(throw_msg)
            vals['street2_id'] = v_street_id
            vals['zip_code_id'] = v_zip_id
        return super(InheritResPartner, self).create(vals)

    @api.multi
    def write(self,vals):
        msg = ""
        res_postal_code = self.env['res.postal.code']
        res_settlement_type = self.env['res.settlement.type']
        res_city = self.env['res.country.state.city']
        res_state = self.env['res.country.state']
        res_country = self.env['res.country']
        v_zip_id = vals.get('zip') if vals.get('zip') else self.zip.id
        # Get country required field settings
        postal_code = res_postal_code.browse(v_zip_id)
        if postal_code:
            country_settings = res_country.browse(postal_code.country_id.id)
            required_settlement = country_settings.required_settlement
            v_city_id = vals.get('city_id') if vals.get('city_id') else \
                self.city_id.id
            v_state_id = vals.get('state_id') if vals.get('state_id') else \
                self.state_id.id
            vals_country_id = vals.get('country_id') if vals.get('country_id') else \
                self.country_id.id
            if required_settlement:
                v_street_id = vals.get('street2') if vals.get('street2') else \
                self.street2.id
                v_settlement_type_id = vals.get('settlement_type') if \
                    vals.get('settlement_type') else self.settlement_type.id
                vals_records = [v_street_id, v_settlement_type_id, v_city_id,
                v_state_id, vals_country_id]
                for line in postal_code.line_ids:
                    if vals_records[0] == line.id:
                        if vals_records[1] != line.settlement_id.id:
                            settlement_type = res_settlement_type.browse(vals_records[1])
                            msg += settlement_type.name + '\n'
                        if vals_records[2] != line.city_id.id:
                            city = res_city.browse(vals_records[2])
                            msg += city.name + '\n'
                        if vals_records[3] != line.state_id.id:
                            state = res_state.browse(vals_records[3])
                            msg += state.name + '\n'
                        if vals_records[4] != line.country_id.id:
                            country = res_country.browse(vals_records[4])
                            msg += country.name + '\n'
            else:
                v_street_id = False
                v_settlement_type_id = False
                vals_records = [v_street_id, v_settlement_type_id, v_city_id,
                v_state_id, vals_country_id]
                for line in postal_code.line_ids:
                    if vals_records[0] is False and vals_records[0] is False:
                        if vals_records[2] != line.city_id.id:
                            city = res_city.browse(vals_records[2])
                            msg += city.name + '\n'
                        if vals_records[3] != line.state_id.id:
                            state = res_state.browse(vals_records[3])
                            msg += state.name + '\n'
                        if vals_records[4] != line.country_id.id:
                            country = res_country.browse(vals_records[4])
                            msg += country.name + '\n'
                    else:
                        continue
            if msg:
                throw_msg = _("""The following data does not belong to the
                    zip code, please verify again:
                    %s"""%msg)
                if throw_msg:
                    raise exceptions.Warning(throw_msg)
            vals['street2_id'] = v_street_id
            vals['zip_code_id'] = v_zip_id
        return super(InheritResPartner, self).write(vals)

    def _display_address(self, cr, uid, address, without_company=False, context=None):

        '''
        The purpose of this function is to build and return an address formatted accordingly to the
        standards of the country where it belongs.

        :param address: browse record of the res.partner to format
        :returns: the address formatted in a display that fit its country habits (or the default ones
            if not country is specified)
        :rtype: string
        '''

        # get the information that will be injected into the display format
        # get the address format
        # address_format = # address.country_id.address_format or \
        address_format = """%(street)s %(l10n_mx_street3)s %(l10n_mx_street4)s\n
        %(street2_name)s %(city_name)s\n
        %(state_code)s %(country_name)s %(zip_name)s"""
        args = {
            'city_name': address.city_id.name or '',
            'state_code': address.state_id.code or '',
            'state_name': address.state_id.name or '',
            'country_code': address.country_id.code or '',
            'country_name': address.country_id.name or '',
            'company_name': address.parent_name or '',
            'zip_name': address.zip.name or '',
            'street2_name': address.street2.name or '',
        }
        for field in self._address_fields(cr, uid, context=context):
            args[field] = getattr(address, field) or ''
        if without_company:
            args['company_name'] = ''
        elif address.parent_id:
            address_format = '%(company_name)s\n' + address_format
        return address_format % args
