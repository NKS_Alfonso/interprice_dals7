# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import openerp.tools as tools
from openerp import fields, models, api


class SqlQuery(models.Model):
    """ We declare this class/function to execute a query which renames
        some states/cities when installing.

    Extends:
        models.Model

    Variables:
        _name {str} -- Model name
    """
    _name = 'sql.query'

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'sql.query')
        #This adds the ids to street2_id and zip_code_id to prevent user from "re-saving" data
        get_partner_obj = self.pool.get('res.partner')
        partner_obj = get_partner_obj.browse(cr, 1, [])
        partner_ids = partner_obj.search(
            [
                '|',
                ('street2','!=',False),
                ('zip','!=',False)
            ]
        )
        if partner_ids:
            sql = """
                    UPDATE
                        res_partner
                    SET
                        street2_id = (SELECT street2
                                        FROM res_partner rp2
                                        WHERE res_partner.id = rp2.id
                                        AND street2 IS NOT NULL),
                        zip_code_id = (SELECT zip
                                        FROM res_partner rp2
                                        WHERE res_partner.id = rp2.id
                                        AND zip IS NOT NULL)
                    WHERE id IN %s
                """
            cr.execute(sql,[tuple(partner_ids.ids)])

        # This updates street2 and zip because they get deleted when updating base
        partner_ids = partner_obj.search(
            [
                '|',
                ('street2_id','!=',0),
                ('zip_code_id','!=',0)
            ]
        )
        if partner_ids:
            sql = """
                    UPDATE
                        res_partner
                    SET
                        street2 = (SELECT street2_id
                                    FROM res_partner rp2
                                    WHERE res_partner.id = rp2.id
                                    AND street2_id != 0),
                        zip = (SELECT zip_code_id
                                FROM res_partner rp2
                                WHERE res_partner.id = rp2.id
                                AND zip_code_id != 0)
                    WHERE id IN %s
                """
            cr.execute(sql,[tuple(partner_ids.ids)])

        cr.execute("""
                UPDATE res_country_state
                SET name = 'Ciudad de México'
                WHERE id = 68""")
        cr.execute("""
                UPDATE res_country_state
                SET code = 'CMX'
                WHERE id = 68""")
        cr.execute("""
                UPDATE res_country_state
                SET name = 'Veracruz de Ignacio de la Llave'
                WHERE id = 89""")
        cr.execute("""
                UPDATE res_country_state
                SET name = 'Baja California'
                WHERE id = 61""")
        cr.execute("""
                UPDATE res_country_state
                SET name = 'Coahuila de Zaragoza'
                WHERE id = 66""")
        cr.execute("""
                UPDATE res_country_state
                SET name = 'Michoacán de Ocampo'
                WHERE id = 75""")
        cr.execute("""
                UPDATE res_country_state
                SET name = 'México'
                WHERE id = 74""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'Batopilas de Manuel Gómez Morín'
                WHERE id = 261""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'Acambay de Ruíz Castañeda'
                WHERE id = 658""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'Silao de la Victoria'
                WHERE id = 322""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'San Mateo Yucutindoo'
                WHERE id = 1486""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'Medellín de Bravo'
                WHERE id = 2199""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'Zacualpan de Amilpas'
                WHERE id = 921""")
        cr.execute("""
                UPDATE res_country_state_city
                SET name = 'Jonacatepec de Leandro Valle'
                WHERE id = 924""")
