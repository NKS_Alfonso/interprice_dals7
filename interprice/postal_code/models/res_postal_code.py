# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, _
from openerp.exceptions import Warning


class ResPostalCode(models.Model):
    _inherit = 'mail.thread'
    _name = 'res.postal.code'
    _rec_name = 'name'

    name = fields.Char(
        string=_('Zip code'),
        required=True,
        help=_('Number of zip code')
        )
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=_('Country'),
        store=True,
        help=_('Name of Country')
        )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        string=_('State'),
        store=True,
        help=_('Name of State')
        )
    city_id = fields.Many2one(
        comodel_name='res.country.state.city',
        string=_('City/Municipality'),
        store=True,
        help=_('Name of City/Municipality')
        )
    line_ids = fields.One2many(
        'res.state.city.colony',
        'postal_id',
        _('Settlements'),
        )
    tax_prod_ids = fields.Many2many(
        comodel_name='account.tax',
        relation='postal_prod_tax_rel',
        column1='postal_code_id',
        column2='tax_id',
        string=_('General Product Taxes'),
        help='Field used for define the general taxes default by zip, having more priority at product taxes defined in product'
    )
    account_prod_id = fields.Many2one(
        comodel_name='account.account',
        string=_('General Product Account'),
        help=_('General Product account defined by zip')
        )
    customer_account_id = fields.Many2one(
        comodel_name='account.account',
        string=_('General Customer Account'),
        help=_('General Customer account defined by zip')
        )

    @api.multi
    def name_get(self):
        res = []
        for code in self:
            if code.country_id:
                res.append((code.id, code.name + ' ' + \
                            '({})'.format(code.country_id.code)))
            else:
                res.append((code.id, code.name + '()'))
        return res

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            return {
                'domain': {
                    'state_id': [('country_id', '=', self.country_id.id)]
                    }
                }
        else:
            return {'domain': {'state_id': []}}

    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.state_id:
            return {
                'domain': {
                    'city_id': [('state_id', '=', self.state_id.id)]
                    }
                }
        else:
            return {'domain': {'city_id': []}}

    @api.onchange('city_id')
    def _onchange_city_id(self):
        if self.city_id:
            return {
                'domain': {
                    'colony_id': [('city_id', '=', self.city_id.id)]
                    }
                }
        else:
            return {'domain': {'colony_id': []}}

    @api.model
    def create(self, vals):
        if vals.get('name') and vals.get('colony_id'):
            self.verify_code(vals.get('name'), vals.get('colony_id'))
        elif 'name' in vals and 'country_id' in vals and 'state_id' in vals and\
            'city_id' in vals:
            country_id = vals.get('country_id')
            state_id = vals.get('state_id')
            city_id = vals.get('city_id')
            if country_id or state_id or city_id:
                values = []
                values.append((0, 0, {
                        'city_id': vals.get('city_id'),
                        'state_id': vals.get('state_id'),
                        'country_id': vals.get('country_id')
                    }))
                vals['line_ids'] = values
        return super(ResPostalCode, self).create(vals)

    @api.one
    def write(self, vals):
        if vals.get('colony_id'):
            self.verify_code(self.name, vals.get('colony_id'))
        return super(ResPostalCode, self).write(vals)

    @api.model
    def verify_code(self, name, colony_id):
        dom_test_id = []
        col_id_user = self.env[
            'res.state.city.colony'].search([('id', '=', colony_id)])
        pc_records = self.env['res.postal.code'].search([('name', '=', name)])
        for record_id in pc_records:
            dom_test_id.append(record_id.id)
            if record_id.colony_id.id == col_id_user.id:
                raise Warning(
                    _(
                        "The colony (%s) is already assigned"
                    ) % (col_id_user.name) +
                    _(" for this postal code (%s)") % (name))
