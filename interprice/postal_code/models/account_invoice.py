# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _
from openerp.tools import float_round

from openerp.tools.misc import DATETIME_FORMATS_MAP
from datetime import datetime
from openerp.exceptions import MissingError, Warning


class AccountInvoice(models.Model):
    """ Model extend for add use cfdi field"""

    # Odoo properties
    _inherit = 'account.invoice'

    res_shipping = fields.Many2one(comodel_name='res.partner', string='Partner to Ship')
