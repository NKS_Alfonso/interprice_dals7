��    >        S   �      H  �   I          0     >     C     K     ]  	   r  	   |     �  
   �  
   �  
   �  .   �  ,   �  R     7   ^     �  	   �  �   �     3  /   6  �   f     	     	     .	     >	     N	  "   W	     z	     �	     �	     �	     �	     �	     �	     �	     
     
  
   %
     0
     C
     O
     U
  	   ^
     h
  �   p
  #     �   &  s   �  �   /     �     �     �     �     �       
             &     :  C  M  �   �     e     �     �  	   �     �     �  
   �     �     �     �  
   �  
   �  .     4   4  d   i  A   �       	   !  �   +     �  /   �  �   �     �     �     �     �     �  "   �     �               6     H     b     j     y     �     �     �     �     �     �  	   �            �     !   �  �   �  |   q  �   �     �     �     �     �     �     �  
   �     �     �                    &                     )            $   /          +   -             ;   ,          0       =         :   %      (              9          6             4   "   *   #       
   8   '       .       <            1   2          5   3             !      	             >       7                   
                                                    This settlement already exists
                                                    in zip code: %s
                                                     for this postal code (%s) Bank Accounts City City... City/Municipality City/Municipality... Colony... Companies Country Country... Created by Created on Date of the last message posted on the record. Display and manage the list of all colonies. Display and manage the list of all postal codes that can be assigned to a contact. Display and manage the list of all type of settlements. Email Thread Followers Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. If this is checked, then the settlement and it's type field are gonna be required
            if not, the the settlement field and type of settlement will be invisible Is a Follower Last Message Date Last Updated by Last Updated on Messages Messages and communication history Name of City/Municipality Name of Country Name of Settlement Name of State Number of zip code Partner Postal Code Postal Code Search view Postal Codes Required Settlement Settlement Settlement Type... Settlements State State... Street... Summary The city you selected cannot
                                           be assigned to the zip code
                                           %s The colony (%s) is already assigned The country you selected cannot
                                           be assigned to the zip code
                                           %s The following data does not belong to the
                    zip code, please verify again:
                    %s The state you selected cannot
                                           be assigned to the zip code
                                           %s Type of Settlement Type of settlement... Unread Messages ZIP ZIP... Zip code street2_id zip_code_id {'invisible': True} {'readonly': True} Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-11 22:45+0000
PO-Revision-Date: 2019-02-11 16:48-0600
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 2.2.1
 
                                                    Este asentamiento ya existe
                                                    en este código postal: %s
                                                     para este código postal (%s) Cuentas bancarias Ciudad Ciudad... Ciudad/Municipio Ciudad/Municipio... Colonia... Empresas País País... Created by Created on Date of the last message posted on the record. Muestra y administra la lista de todas las colonias. Muestra y administra la lista de todos los códigos postales que pueden ser asignados a un contacto. Muestra y administra la lista de todos los tipos de asentamiento. Hilo de mensajes Followers Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. Si es activo, el asentamiento y su tipo de asentamiento serán requeridos en la vista del cliente
            si no, el asentamiento y su tipo serán invisibles. Is a Follower Last Message Date Last Updated by Last Updated on Messages Messages and communication history Nombre de Ciudad/Municipio Nombre del País Nombre del Asentamiento Nombre del Estado Número de Código Postal Empresa Código Postal Vista de búsqueda de códigos Códigos Postales Asentamiento Requerido Asentamiento Tipo de asentamiento... Asentamientos Estado Estado... Calle... Summary La ciudad que seleccionaste
                                           no puede ser asignada al código postal
                                           %s La colonia (%s) ya está asignada El país que seleccionaste
                                           no puede ser asignado al código postal
                                           %s Los siguientes datos no pertenecen
                    al código postal, porfavor verifique otravez:
                    %s El estado que seleccionaste
                                           no puede ser asignado al código postal
                                           %s Tipo de Asentamiento Tipo de asentamiento... Unread Messages CP CP... Código Postal street2_id zip_code_id {'invisible': True} {'readonly': True} 