# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class ProductPriceLevel(models.Model):
    _inherit = 'product.price.level'


    # price = fields.Float(track_visibility='onchange')
    # price_level = fields.Many2one(track_visibility='onchange')
    # price_updated_field = fields.Float(track_visibility='onchange')

    @api.multi
    def write(self, vals):
        res = super(ProductPriceLevel, self).write(vals)
        message = ''
        product_id = self[0].product
        for level in self: 
            message += """Level %s %s -> %s \n """%(level.price_level.id, level.price, vals.get('price'))
            # level.product.message_post(
            # )
        if message:
            product_id.message_post(
                subject=_('• Price levels has been changed'),
                type='notification',
                body=message,
                content_subtype='plaintext',
                author_id=self.env.user.partner_id.id,
            )
        return res

