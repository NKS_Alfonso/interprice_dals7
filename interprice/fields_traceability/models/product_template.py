# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
import ast
import operator


class ProductTemplate(models.Model):
    _inherit = ['product.template']

    # Main view
    name = fields.Char(track_visibility='onchange')
    # image_medium = fields.Binary(track_visibility='onchange')
    sale_ok = fields.Boolean(track_visibility='onchange')
    purchase_ok = fields.Boolean(track_visibility='onchange')
    web_ok = fields.Boolean(track_visibility='onchange')
    #  Information
    type = fields.Selection(track_visibility='onchange')
    uom_id = fields.Many2one(track_visibility='onchange')
    list_price_currency = fields.Many2one(track_visibility='onchange')
    list_price = fields.Float(track_visibility='onchange')
    reposition_cost_currency = fields.Many2one(track_visibility='onchange')
    reposition_cost = fields.Float(track_visibility='onchange')
    active = fields.Boolean(track_visibility='onchange')
    ean13 = fields.Char(track_visibility='onchange')
    clave_fabricante = fields.Char(track_visibility='onchange')
    default_code = fields.Char(track_visibility='onchange')
    amazonAsin = fields.Char(track_visibility='onchange')
    section = fields.Many2one(track_visibility='onchange')
    line = fields.Many2one(track_visibility='onchange')
    brand = fields.Many2one(track_visibility='onchange')
    serial = fields.Many2one(track_visibility='onchange')
    # Supplies
    cost_method = fields.Selection(track_visibility='onchange')
    standard_price = fields.Float(track_visibility='onchange')
    uom_po_id = fields.Many2one(track_visibility='onchange')
    route_ids = fields.Many2many(track_visibility='onchange')
    seller_ids = fields.One2many(track_visibility='onchange')
    description_purchase = fields.Text(track_visibility='onchange')
    # Warehouse
    qty_available = fields.Float(track_visibility='onchange')
    incoming_qty = fields.Float(track_visibility='onchange')
    virtual_available = fields.Float(track_visibility='onchange')
    check_no_negative = fields.Boolean(track_visibility='onchange')
    track_all = fields.Boolean(track_visibility='onchange')
    track_incoming = fields.Boolean(track_visibility='onchange')
    track_outgoing = fields.Boolean(track_visibility='onchange')
    track_production = fields.Boolean(track_visibility='onchange')
    state = fields.Selection(track_visibility='onchange')
    product_manager = fields.Many2one(track_visibility='onchange')
    property_stock_procurement = fields.Many2one(track_visibility='onchange')
    property_stock_production = fields.Many2one(track_visibility='onchange')
    property_stock_inventory = fields.Many2one(track_visibility='onchange')
    volume = fields.Float(track_visibility='onchange')
    weight = fields.Float(track_visibility='onchange')
    weight_net = fields.Float(track_visibility='onchange')
    warehouse_ids = fields.One2many(track_visibility='onchange')
    # Sales
    garantia = fields.Many2one(track_visibility='onchange')
    sale_delay = fields.Float(track_visibility='onchange')
    produce_delay = fields.Float(track_visibility='onchange')
    description_sale = fields.Text(track_visibility='onchange')
    # Price Levels
    list_price_ro = fields.Float(track_visibility='onchange')
    reposition_cost_ro = fields.Float(track_visibility='onchange')
    average_cost = fields.Float(track_visibility='onchange')
    last_po_cost = fields.Float(track_visibility='onchange')
    # product_price_levels_nopop = fields.One2many(track_visibility='onchange')
    # Variants
    attribute_line_ids = fields.One2many(track_visibility='onchange')
    # Accounting
    categ_id = fields.Many2one(track_visibility='onchange')
    valuation = fields.Selection(track_visibility='onchange')
    property_stock_account_input = fields.Many2one(track_visibility='onchange')
    property_stock_account_output = fields.Many2one(track_visibility='onchange')
    property_account_income = fields.Many2one(track_visibility='onchange')
    taxes_id = fields.Many2many(track_visibility='onchange')
    property_account_expense = fields.Many2one(track_visibility='onchange')
    supplier_taxes_id = fields.Many2many(track_visibility='onchange')



    @api.multi
    def write(self, vals):
        if 'image_medium' in vals:
            self.message_post(
                body=_('The image has been changed or deleted'),
                subject=_('• Image'),
                type='notification',
                content_subtype='plaintext',
                author_id=self.env.user.partner_id.id,
            )
        res = super(ProductTemplate, self).write(vals)
        return res
