# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Fields Traceability',
    'version': '0.1',
    'author': 'Copyright © 2019 TO-DO - All Rights Reserved',
    'category': '',
    'description': """
        Module to can see the field's traceability
         """,
    'website': 'http://www.grupovadeto.com',
    'license': 'AGPL-3',
    'depends': [
        'product',
        'price_list',
        'mail',
    ],
    'data': [
        # 'views/product_template.xml'
    ],
    'installable': True,
    'active': False,
}
