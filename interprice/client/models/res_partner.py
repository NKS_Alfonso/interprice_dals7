# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
import re
# related third party imports
# local application/library specific imports
from openerp import (models, fields, api, exceptions, _)


class ResPartner(models.Model):
    """Extended class for adding fields for clients"""

    # odoo model properties
    _inherit = 'res.partner'
    """_sql_constraints = [
        ('client_number_uniq', 'unique(client_number)',
            _("Can't be duplicate value for client number"))
    ]"""

    # adds cash back reference
    branch_office = fields.Many2one(
        comodel_name='account.cost.center',
        help="Branch office client",
        string="Branch office",
        readonly=False,
        required=False,
        domain="[('active', '=', True)]"
    )
    client_number = fields.Char(
        help="Client number",
        string="Client number",
        copy=False,
    )
    supplier_number = fields.Char(
        help="Supplier number",
        string="Supplier number",
        copy=False,
    )
    password = fields.Char(
        string="Client password",
        help="Client password",
        copy=False,
    )

    client_is_saved = fields.Boolean(
        default=False,
        string="Is saved",
        readonly=True,
    )
    supplier_is_saved = fields.Boolean(
        default=False,
        string="Is saved",
        readonly=True,
    )

    @api.model
    def create(self, vals):
        """Override original create method"""
        client_number = vals.get('client_number', False)
        supplier_number = vals.get('supplier_number', False)
        email = vals.get('email', False)
        if client_number:
            self.env.cr.execute(
                """
                SELECT client_number FROM res_partner
                    WHERE client_number = %s
                LIMIT 1;
                """, (client_number,))
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _('Client number %s already exists '
                      'and can not be duplicated') % (client_number)
                )
            if re.match("^-?[0-9]+$", client_number):
                vals['client_is_saved'] = True
            else:
                raise exceptions.Warning(
                    _('Client number must have only numbers'))

        if supplier_number:
            self.env.cr.execute(
                """
                SELECT supplier_number FROM res_partner
                    WHERE supplier_number = %s
                LIMIT 1;
                """, (supplier_number,))
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _('Supplier number %s already exists '
                      'and can not be duplicated') % (supplier_number)
                )
            if re.match("^-?[0-9]+$", supplier_number):
                vals['supplier_is_saved'] = True
            else:
                raise exceptions.Warning(
                    _('Supplier number must have only numbers'))
        if email:
            self.env.cr.execute(
                """
                SELECT email FROM res_partner
                    WHERE email = %s
                LIMIT 1;
                """, (email,))
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _('Email  %s already exists '
                      'and can not be duplicated') % (email)
                )
        return super(ResPartner, self).create(vals)

    @api.multi
    def write(self, vals):
        """Override original write method"""
        client_number = vals.get('client_number', False)
        supplier_number = vals.get('supplier_number', False)
        email = vals.get('email', False)
        if client_number:
            self.env.cr.execute(
                """
                SELECT client_number FROM res_partner
                    WHERE client_number = %s
                LIMIT 1;
                """, (client_number,))
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _('Client number %s already exists '
                      'and can not be duplicated') % (client_number)
                )
            if re.match("^-?[0-9]+$", client_number):
                vals['client_is_saved'] = True
            else:
                raise exceptions.Warning(
                    _('Client number must have only numbers'))

        if supplier_number:
            self.env.cr.execute(
                """
                SELECT supplier_number FROM res_partner
                    WHERE supplier_number = %s
                LIMIT 1;
                """, (supplier_number,))
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _('Supplier number %s already exists '
                      'and can not be duplicated') % (supplier_number)
                )
            if re.match("^-?[0-9]+$", supplier_number):
                vals['supplier_is_saved'] = True
            else:
                raise exceptions.Warning(
                    _('Supplier number must have only numbers'))
        if email:
            self.env.cr.execute(
                """
                SELECT email FROM res_partner
                    WHERE email = %s
                LIMIT 1;
                """, (email,))
            if self.env.cr.rowcount:
                raise exceptions.Warning(
                    _('Email  %s already exists '
                      'and can not be duplicated') % (email)
                )
        return super(ResPartner, self).write(vals)

    @api.onchange('client_number', 'supplier_number')
    def validate_number(self):
        if self.client_number:
            if re.match("^-?[0-9]+$", self.client_number):
                pass
            else:
                self.client_number = False
                return {
                    'warning': {
                        'title': 'warning',
                        'message': _('Client number must have only numbers')
                    }
                }

        if self.supplier_number:
            if re.match("^-?[0-9]+$", self.supplier_number):
                pass
            else:
                self.supplier_number = False
                return {
                    'warning': {
                        'title': 'warning',
                        'message': _('Supplier number must have only numbers')
                    }
                }
