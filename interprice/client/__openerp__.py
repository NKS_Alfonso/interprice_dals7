# -*- coding: utf-8 -*-
{
    'name': 'Client',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Sales',
    'description': 'Res partner new fields',
    'depends': ['base','purchase'],
    'application': True,
    'data': [
        'views/res_partner_view.xml',
        # 'security/ir.model.access.csv',
    ]
}
