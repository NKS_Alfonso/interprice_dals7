# -*- coding: utf-8 -*-

#############################################################################
#  @version     : 1.0                                                       #
#  @autor       : Viridiana Cruz Santos(gvadeto)2016                        #
#  @linea       : Maximo 79 chars                                           #
#############################################################################

# OpenERP imports
from openerp.osv import fields, osv


class account_invoice(osv.osv):
	_inherit = 'account.invoice'
	
	_columns = {
		'gv_factura_x_servicio_id': fields.many2one(
			'gv.factura.x.servicio',
			'Factura'),
			}

account_invoice()

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
