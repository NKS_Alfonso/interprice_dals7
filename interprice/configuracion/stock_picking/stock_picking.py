# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, exceptions, _, api
from openerp.osv import osv


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def _get_invoice_vals(self,
                          cr,
                          uid,
                          key,
                          inv_type,
                          journal_id,
                          move,
                          context=None):
        res = super(StockPicking, self)._get_invoice_vals(cr,
                                                          uid,
                                                          key,
                                                          inv_type,
                                                          journal_id,
                                                          move,
                                                          context=context)
        journal = self.pool['account.journal'].browse(cr, uid, [journal_id])
        if journal:
            if 'account_id' in res:
                    if not journal.default_credit_account_id:
                        raise exceptions.Warning(_('Define default credit account for journal'))
                    res['account_id'] = journal.default_credit_account_id.id
            if 'currency_id' in res:
                res['currency_id'] = journal.currency.id or journal.company_id.currency_id.id
        return res

    def _prepare_shipping_invoice_line(self, cr, uid, picking, invoice, context=None):
        res = super(StockPicking, self)._prepare_shipping_invoice_line(cr, uid, picking, invoice, context)

        if invoice.type == 'out_refund':
            res = {}
        return res

    @api.multi
    def back_to_albaran(self):
        form_id = self.env['ir.model.data'].get_object_reference('stock', 'view_picking_form')[1]
        return{
            'view_mode': 'form',
            'view_id': form_id,
            'view_type': 'form',
            'res_model': 'stock.picking',
            'res_id': self.id,
            'type': 'ir.actions.act_window',
            'domain': '[]',
        }



class StockInvoiceOnShipping(osv.osv_memory):
    _inherit = 'stock.invoice.onshipping'

    def onchange_journal_id(self, cr, uid, ids, journal_id, context=None):
        context2 = dict(context)
        stock_picking_id = context2.get('active_id', False)
        res = super(StockInvoiceOnShipping, self).onchange_journal_id(cr, uid, ids, journal_id, context)
        if stock_picking_id:
            qry_get_type_move = """SELECT
              spt.code, so.name so_name, po.name po_name, sppo.origin
            FROM
              stock_picking sp INNER JOIN
              stock_picking_type spt ON sp.picking_type_id = spt.id
              LEFT JOIN sale_order so ON so.name=sp.origin
              LEFT JOIN purchase_order po ON po.name=sp.origin
              LEFT JOIN stock_picking sppo ON sppo.name = sp.origin
              WHERE sp.id = {}
              """.format(stock_picking_id)
            cr.execute(qry_get_type_move)
            if cr.rowcount:
                _res_model = None
                _document = None
                for code, so_name, po_name, origin in cr.fetchall():
                    if so_name:
                        _res_model = 'sale.order'
                        _document = so_name
                    elif po_name:
                        _res_model = 'purchase.order'
                        _document = po_name
                    elif origin:
                        _document = origin
                        if code == 'incoming':# so
                            _res_model = 'sale.order'
                        elif code == 'outgoing':# po
                            _res_model = 'purchase.order'
                if _res_model and _document:
                    document = self.pool[_res_model].browse(
                        cr,
                        uid,
                        self.pool[_res_model].search(
                            cr,
                            uid,
                            [('name', '=', _document)]
                        )
                    )
                    if document:
                        document.ensure_one()
                        journal_filter_by_currency = None
                        if _res_model == 'sale.order':
                            journal_filter_by_currency = document.currency.id
                        elif _res_model == 'purchase.order':
                            journal_filter_by_currency = document.currency_id.id

                        if journal_filter_by_currency:
                            _currency = False
                            if document.company_id.currency_id.id != journal_filter_by_currency:
                                _currency = journal_filter_by_currency
                            if 'domain' in res and 'journal_id' in res['domain']:
                                res['domain']['journal_id'].append(('currency', '=', _currency))
        return res

    def _get_journal(self, cr, uid, context=None):
        res = super(StockInvoiceOnShipping, self)._get_journal(cr, uid, context)
        return None

    _defaults = {
        'journal_id': _get_journal
    }
