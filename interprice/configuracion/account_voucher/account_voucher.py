# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models
import re


# ARRANGOIZ COMPUTACION SA DE CV, SUCURSAL VERACRUZ
class AccountVoucher(models.Model):
    _inherit = 'account.voucher'

    def onchange_amount(self, cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date,
                        payment_rate_currency_id, company_id, context=None):
        res = super(AccountVoucher, self).onchange_amount(cr, uid, ids, amount, rate, partner_id, journal_id,
                                                          currency_id, ttype, date, payment_rate_currency_id,
                                                          company_id, context=None)
        res = self.cr_dr_remove_canceled(cr, res, ttype)
        return res

    def get_account_move(self, cr, _in=False):

        where_in_invoice = where_in_voucher = ''
        if _in and len(_in) > 0:
            where_in_invoice = 'WHERE ai.internal_number in (%s)' % (str(_in)[1:-1])
            where_in_voucher = 'WHERE am.name in (%s)' % (str(_in)[1:-1])
        _query = """
                    SELECT
                      *
                    FROM (
                      SELECT
                        ai.id,
                        ai.internal_number,
                        ai.state,
                        am.id   AS move_id,
                        am.name AS move_name,
                        am.ref
                      FROM
                        account_invoice ai LEFT JOIN account_move am ON am.ref LIKE '%%' || ai.move_name || '%%'
                    --   WHERE ai.internal_number  in ('000050', '000052', '000054', 'B4439/000013')
                      %s
                      UNION ALL

                      SELECT
                        av.id,
                        am.name, --av.number,
                        av.state,
                        am.id,
                        am.name,
                        am.ref
                      FROM account_move am LEFT JOIN account_voucher av  ON am.ref LIKE '%%' || av.number || '%%'
                    --   WHERE am.name  in ('000050', '000052', '000054', 'B4439/000013')
                    %s
                    )T1;
                """ % (where_in_invoice, where_in_voucher)
        # print _query
        cr.execute(_query)
        account_moves = {}
        if cr.rowcount:
            for (id, internal_number, state, move_id, move_name, ref) in cr.fetchall():
                if internal_number not in account_moves:
                    if not state:
                        if ref:
                            # print internal_number, re.search(r'(C|c)ancelad(O|o|A|a)', ref, re.IGNORECASE), ref
                            if re.search(r'(C|c)ancelad(O|o|A|a)', ref, re.IGNORECASE):
                                state = 'cancel'
                    account_moves[internal_number] = {
                        'account_invoice_id': id,
                        'state': state,
                        'move_ids': [{'id': move_id, 'name': move_name, 'ref': ref}]
                    }
                else:
                    account_moves[internal_number]['move_ids'].append({'id': move_id, 'name': move_name, 'ref': ref})
        # print account_moves
        return account_moves

    def onchange_partner_id(self, cr, uid, ids, partner_id, journal_id, amount, currency_id, ttype, date, context=None):
        res = super(AccountVoucher, self).onchange_partner_id(cr, uid, ids, partner_id, journal_id, amount, currency_id,
                                                              ttype, date, context=context)
        res = self.cr_dr_remove_canceled(cr, res, ttype)
        return res

    def cr_dr_remove_canceled(self, cr, res, ttype):
        if ttype == 'receipt':  # Section customer
            _credits = 'line_cr_ids'
            _debits = 'line_dr_ids'
        elif ttype == 'payment':
            _credits = 'line_dr_ids'
            _debits = 'line_cr_ids'
        else:
            return res

        if 'value' not in res:
            return res

        search_cr = []
        line_cr_ids = []
        account_invoice_to_cancel = []
        if _credits in res['value']:
            for line_cr_id in res['value'][_credits]:
                if isinstance(line_cr_id, dict):
                    search_cr.append(str(line_cr_id['name']))
            if search_cr:
                account_move_cr = self.get_account_move(cr, search_cr)
            else:
                account_move_cr = {}
            for line_cr_id in res['value'][_credits]:
                if isinstance(line_cr_id, dict):
                    if line_cr_id['name'] in account_move_cr:
                        account_move_invoice = account_move_cr[line_cr_id['name']]
                        if account_move_invoice['state'] == 'cancel':
                            # print 'Inv canceled', line_cr_id['name']
                            for move_id in account_move_invoice['move_ids']:
                                if move_id['ref']:
                                    if re.search(r'(C|c)ancelad(O|o|A|a)', move_id['ref'], re.IGNORECASE):
                                        # print move_id['name']
                                        account_invoice_to_cancel.append(move_id['name'])
                        else:
                            line_cr_ids.append(line_cr_id)
                    else:
                        line_cr_ids.append(line_cr_id)
                else:
                    line_cr_ids.append(line_cr_id)
            res['value'][_credits] = line_cr_ids

        search_dr = []
        line_dr_ids = []
        if _debits in res['value']:
            for line_dr_id in res['value'][_debits]:
                if isinstance(line_dr_id, dict):
                    search_dr.append(str(line_dr_id['name']))
            if search_dr:
                account_move_dr = self.get_account_move(cr, search_dr)
            else:
                account_move_dr = {}
            for line_dr_id in res['value'][_debits]:
                if isinstance(line_dr_id, dict):
                    # Remove move if invoice is canceled
                    if line_dr_id['name'] not in account_invoice_to_cancel:
                        if line_dr_id['name'] in account_move_dr:
                            account_move_invoice = account_move_dr[line_dr_id['name']]
                            if account_move_invoice['state'] == 'cancel':
                                pass
                            else:
                                line_dr_ids.append(line_dr_id)
                        else:
                            line_dr_ids.append(line_dr_id)
                else:
                    line_dr_ids.append(line_dr_id)
            res['value'][_debits] = line_dr_ids
        return res