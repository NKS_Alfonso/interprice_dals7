* Coloca las fechas de la factura de clientes y proveedores con fecha del día y de solo lectura.
* Del módulo de productos, se agrego la validación única de default_code.

Cuenta de descuento
------------------
* Al crear una nota de crédito de cliente los productos tomarán la cuenta de descuento que tiene
  asignada en la categoría interna, misma que se considera en la póliza.
* No se esta considerando el campo de Transportista, si la  nota de crédito de cliente,
  se crea desde una devolución.

Ubicación de configuración de la cuenta de descuento

.. figure:: ../configuracion/static/img/discount_account.png
    :alt: Configuración de la cuenta de descuento - Categoría producto
    :width: 100%


Cuenta del producto en nota de crédito del cliente

.. figure:: ../configuracion/static/img/product_account.png
    :alt: Ticket a imprimir
    :width: 100%


Campo a no considerar cuando se crea una nota de crédito del cliente / devolución de mercancia

.. figure:: ../configuracion/static/img/carrier_picking.png
    :alt: Ticket a imprimir
    :width: 100%


Botón para regresar al albarán en los detalles del producto y series en stock picking

.. figure:: ../configuracion/static/img/pack_operation_odoo.png
    :alt: Regresar al albarán
    :width: 100%
