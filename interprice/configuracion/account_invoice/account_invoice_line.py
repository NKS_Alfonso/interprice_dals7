# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, api, _, exceptions


class account_invoice_line(models.Model):
    _inherit = "account.invoice.line"

    @api.multi
    def product_id_change(self, product, uom_id, qty=0, name='',
                          type='out_invoice', partner_id=False,
                          fposition_id=False, price_unit=False,
                          currency_id=False, company_id=None):

        res = super(account_invoice_line, self).product_id_change(product, uom_id,
                                                                  qty=qty, name=name,
                                                                  type=type,
                                                                  partner_id=partner_id,
                                                                  fposition_id=fposition_id,
                                                                  price_unit=price_unit,
                                                                  currency_id=currency_id,
                                                                  company_id=company_id)

        if type == 'out_refund':
            product = self.env['product.product'].browse(product)

            if product and not product.categ_id.property_account_discount_categ:
                raise exceptions.Warning(
                    _("La categoría interna del prodcuto '%s' no tiene definida una cuenta de descuento." %
                      (product.name,)))
            account = product.categ_id.property_account_discount_categ

            if 'value' in res and 'account_id' in res['value']:
                res['value'].update({
                    'account_id': account})
            else:
                res['value']['account_id'] = account

        return res
