# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from lxml import etree
from openerp import _, models, api, fields, tools
from openerp.exceptions import except_orm, RedirectWarning, Warning
from openerp.tools import float_compare
import itertools
import openerp.addons.decimal_precision as dp
import time
class AccountInvoice(models.Model):
    _inherit = "account.invoice"


    _defaults = {
        'date_invoice': fields.Datetime.now,
        'invoice_datetime': fields.Datetime.now,
    }

    @api.multi
    def onchange_journal_id(self, journal_id=False):
        res = super(AccountInvoice, self).onchange_journal_id(
            journal_id=journal_id
        )
        if res and 'value' in res:
            journal = self.env['account.journal'].browse(journal_id)
            res['value'].update({
                'account_id': journal.default_credit_account_id.id
            })
            res.update({
                'domain': {
                      'account_id': [(
                          'id', 'in', (res['value']['account_id'],)
                      )],
                      'currency_id': [(
                          'id', 'in', (res['value']['currency_id'],)
                      )]
                  }
            })
        return res

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
                          payment_term=False, partner_bank_id=False, company_id=False
                            ):
        result = super(AccountInvoice, self).onchange_partner_id(
          type, partner_id, date_invoice, payment_term,
          partner_bank_id, company_id
        )
        if result and 'value' in result and 'account_id' in result['value']:
            del result['value']['account_id']
        return result

    @api.onchange('invoice_datetime')
    def onchange_invoice_datetime(self):
        # TODO: the inherit field invoice_datetime is in
        # fe_ce_8\odoo-mexico_v8\l10n_mx_invoice_datetime\invoice.py
        invoice_datetime_tz = tools.server_to_local_timestamp(self.invoice_datetime,
            tools.DEFAULT_SERVER_DATETIME_FORMAT,
            tools.DEFAULT_SERVER_DATE_FORMAT,
            self._context.get('tz', 'America/Mexico_City'))
        self.date_invoice = invoice_datetime_tz

    @api.multi
    def action_cancel(self):
        number = self.number
        move_iddd=0
        moves = self.env['account.move']
        for inv in self:
            if inv.move_id:
                moves += inv.move_id
            if inv.payment_ids:
                for move_line in inv.payment_ids:
                    if move_line.reconcile_partial_id.line_partial_ids:
                        raise except_orm(_('Error!'), _('You cannot cancel an invoice which is partially paid. You need to unreconcile related payment entries first.'))

        # First, set the invoices as cancelled and detach the move ids
        self.write({'state': 'cancel', 'move_id': False})
        if moves:
            new_poliza=moves.copy()
            move_iddd=int(new_poliza[0])
            time.sleep(1)

        seq_reconcile = self.env['ir.sequence'].get('account.reconcile') or '/'
        new_reconcile = self.env['account.move.reconcile'].create(vals={'name': seq_reconcile, 'type': 'auto'})
        for i in moves.line_id:
            i.reconcile_id = new_reconcile.id

        object_move = self.env['account.move'].browse(move_iddd)
        for i in object_move.line_id:
            debit = i.debit
            credit = i.credit
            i.reconcile_id = new_reconcile.id
            self._cr.execute("update account_move_line set debit=%s, credit=%s WHERE move_id=%s and id=%s",(credit,debit,move_iddd,i.id))
            self._cr.execute("update account_move set invoice_id=%s WHERE id=%s",(self.id,move_iddd,))
            self._cr.execute("update account_invoice set number=%s WHERE id=%s",(number,self.id,))
            cad = number+str(" Fac. Cancelada")
            # <TODO: modification of query, add current date and period>
            period_id = self.env['account.voucher']._get_period()
            if not period_id:
                raise Warning(
                    _("The period not exists. Configuration / Periods ")
                )
            date = fields.Date.context_today(self) or fields.Date.today()

            # self._cr.execute('update account_move set ref=%s where id=%s',(cad,move_iddd,))
            self._cr.execute(""" UPDATE account_move SET
                                 ref=%s, date=%s, period_id=%s
                                 WHERE id=%s """,
                             (cad, date, period_id, move_iddd,))
            # <TODO>
            uid = self._uid
            cr = self._cr
            context = self._context

        self.asentar_poliza()
        self._log_event(-1.0, 'Cancel Invoice')

        return True

    def asentar_poliza(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids,context=context)
        id_polizas = self.pool.get('account.move').search(cr,uid, [('id','=',data.move_id.id)], context=context)
        if id_polizas:
            for j in id_polizas:
                self.pool.get('account.move').button_validate(cr,uid, [j], context=context)