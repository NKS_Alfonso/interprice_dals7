# -*- coding: utf-8 -*-

##############################################################################
#  @version     : 1.0                                                        #
#  @autor       : ICJ                                                        #
#  @creacion    :  (aaaa/mm/dd)                                              #
#  @linea       : Maximo 79 chars                                            #
##############################################################################

# OpenERP imports
# from osv import fields, osv
from openerp import models, fields, api
import time
from openerp.tools.translate import _
from openerp.osv import fields, osv
from datetime import datetime, date
from openerp.addons.payment.models.payment_acquirer import ValidationError
# Modulo :: Vendedor

class stock_inventory(osv.osv):
	""" FUNCION PARA LLENAR LOS DATOS DE PEDIMENTO,
	ADUANA Y FECHA, EN STOCK_ONVENTORY_LINE """
	def funcion_auxliar(self, cr, uid, ids, context=None):
		data = self.browse(cr, uid, ids)
		lot_id = self.pool.get('stock.production.lot')
		inventory_line_obj = self.pool.get('stock.inventory.line')
		if data.line_ids:
			for this in data.line_ids:
				if this.prod_lot_id:
					obj_lote = lot_id.browse(cr, uid, this.prod_lot_id.id)
					inventory_line_obj.write(
						cr, uid, this.id, {
							'pedimento': obj_lote.pedimento,
							'aduana': obj_lote.aduana,
							'fecha': obj_lote.fecha_pedimento
							}, context=context)

	# Nombre del modelo
	_inherit = 'stock.inventory'
	# funciones para crar lotes con datos de pedimento desde el ajuste
	def _lote_automatico(self, cr, uid, ids, name, args, context):
		"""Genera el lote en automatico"""
		if not ids : return {}
		result = {}
		lote = ""
		subfijo="AI000"
		subfijo_100 ="AI00"
		subfijo_1000 ="AI0"
		subfijo_mas_1000 ="AI"
		try:
			for this in self.browse(cr, uid, ids):
				if this.state != 'draft':
					if this.id < 10:
						lote = subfijo+str(this.id)
					elif (this.id >= 10 and this.id < 100):
						lote = subfijo_100 + str(this.id)
					elif (this.id >= 100 and this.id < 1000):
						lote = subfijo_1000 + str(this.id)
					elif(this.id >= 1000):
						lote = subfijo_mas_1000 + str(this.id)
					result[this.id] = lote
			return result
		except:
			return {}

	def _attrs_fields_lotes(self, cr, uid, ids, name, args, context):
		"""Genera el lote en automatico"""
		if not ids : return {}
		result = {}
		ban = False
		try:
			for this in self.browse(cr,uid,ids):
				if this.state=='confirm':
					if this.line_ids:
						cr.execute("""SELECT count(*)
							FROM stock_inventory_line 
							WHERE inventory_id=%s
							AND prod_lot_id is null""", (this.id,))
						count = cr.fetchone()[0]
						if count > 0:
							ban = True
			result[this.id] = ban
			return result
		except:
			return {}

	def create_lot(self, cr, uid, ids, context=None):
		data = self.browse(cr, uid, ids)
		lot_id = self.pool.get('stock.production.lot')
		inventory_line_obj = self.pool.get('stock.inventory.line')
		if data.line_ids:
			for this in data.line_ids:
				if not this.prod_lot_id:
					val = {'name': data.lote,
						'origin_document': data.name,
						'pedimento': this.pedimento,
						'fecha_pedimento': this.fecha,
						'aduana': this.aduana,
						'product_id': this.product_id.id,
					}
				lot = lot_id.create(cr, uid, val, context=context)
				inventory_line_obj.write(
					cr, uid, this.id, {'prod_lot_id': lot}, context=context)

	# end#funciones para crar lotes con datos de pedimento desde el ajuste
	def action_done(self, cr, uid, ids, context=None):
		""" Finish the inventory
		@return: True
		"""
		""" Buscar sk iguales o id de productos iguales o 
		repetidos en las lineas del ajuste""" 
		data = self.browse(cr, uid, ids)
		for this in data.line_ids:
			cr.execute("""SELECT COUNT(*) FROM stock_inventory AS sv
							INNER JOIN stock_inventory_line AS siv
							ON sv.id=siv.inventory_id
							WHERE sv.id=%s and siv.product_id=%s""",
							(data.id,this.product_id.id,))
			count = cr.fetchone()[0]
			if count > 1:
				raise osv.except_osv(
					('Alerta!'), ("Hay SKU'S repetidos favor de verificar"))
		################################################################
		self.create_lot(cr, uid,ids,context=context)
		self.history_ajustes(cr, uid,ids,context=context)
		super(stock_inventory, self).action_done(
			cr, uid, ids, context=context)
		# return super(stock_inventory, self).action_done(
			# cr, uid, ids, context=context)
		self.polizas(cr, uid, ids, context=context)

	def polizas(self, cr, uid, ids, context=None):
		pass
		# data = self.browse(cr, uid, ids)
		# count = 0
		# if data.line_ids:
		#   for account_id in data.line_ids:
		#       if count == 0:
		#         cuenta = account_id.product_id.property_account_expense.id
		#         count+=1
		#
		# name = "FV:"+data.name
		# obj_account = self.pool.get('account.account')
		# obj_account_line = self.pool.get('account.move.line')
		# obj_account_move = self.pool.get('account.move')
		# cuenta_compra_id =  obj_account.search(cr, uid, [('code','=','108-0003')])[0]
		# cuenta_venta_id =  obj_account.search(cr, uid, [('code','=','504-0000')])[0]
		# cuenta_almacen_id =  obj_account.search(cr, uid, [('code','=','108-0001')])[0]
		# obj_move = self.pool.get('account.move')
		# cr.execute("""SELECT av.id FROM account_move AS av INNER JOIN account_move_line AS avl ON av.id=avl.move_id WHERE avl.name=%s""",(name,))
		# polizas = cr.fetchall()
		# #cuenta_almacen = obj_account.browse(cr, uid, cuenta_almacen_id, context=context)
		# for this in polizas:
		#   move = obj_move.browse(cr, uid, this,context=context)
		#   for line in move.line_id:
		#     #if line.account_id.id!=cuenta_almacen_id and line.account_id.id!=cuenta:
		#      if line.account_id.id==cuenta_compra_id:
		#       obj_account_move.button_cancel(cr,uid, this, context)
		#       obj_account_line.write(cr, uid, line.id,{'account_id':cuenta_venta_id},context=context)
		#       obj_account_move.button_validate(cr, uid, this,context)
		#   obj_move.write(cr, uid, move.id,{'ref':"Ajuste "+data.name}, context=context)


	###########history.stock###############################################
	def history_ajustes(self, cr, uid, ids, context=None):
		"""Funcion que recorre la lista de productos de 
			albaranes de entrada(compras)"""
		# product_qty_move = Cantidad del producto que entra o 
		# sale (ventas o compras etc.)
		lista_ids = []
		qty_out = 0.00
		qty_in = 0.00
		obj_history_stock = self.pool.get('history.stock')
		obj_product = self.pool.get('product.product')
		data = self.browse(cr, uid, ids)
		document_origen = data.name
		warehouse_id = self.pool.get(
			'stock.warehouse').search(
				cr, uid, [('lot_stock_id', '=', data.location_id.id)])[0]
		location_id = data.location_id.id
		cr.execute("""SELECT distinct(product_id)
					FROM stock_inventory_line
					WHERE inventory_id=%s""",(data.id,))
		lines_ids = cr.fetchall()
		for this in lines_ids:
			cr.execute("""SELECT sum(product_qty)
						FROM stock_inventory_line
						WHERE inventory_id=%s and product_id=%s""",
						(data.id, this,))
			qty_enytry = cr.fetchone()[0]
			cr.execute("""SELECT count(sq.qty),sum(qty)
						FROM stock_quant AS sq
						INNER JOIN stock_location AS sl
						INNER JOIN stock_warehouse AS sw
						ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						WHERE (
							sq.product_id=%s
							AND sl.id=%s
							AND sl.usage='internal')""", (this, location_id,))
			for (qty, sm) in cr.fetchall():
				if qty == 0: product_qty = 0
				elif (qty > 0): product_qty = sm

			for dat in data.line_ids:
				product = obj_product.browse(cr, uid, this, context=context)
				if product.id == dat.product_id.id:
					if dat.theoretical_qty > dat.product_qty:
						qty_out += (dat.theoretical_qty - dat.product_qty)
					elif dat.theoretical_qty < dat.product_qty:
						qty_in += (dat.product_qty - dat.theoretical_qty)

			if (qty_in > qty_out):
				write_vals = {
					'product_id': this,
					'warehouse_id': warehouse_id,
					'location_id': location_id,
					'ajuste_id': data.id,
					'product_qty_move': qty_in - qty_out,
					'existencia_anterior': product_qty,
					'existencia_posterior': (qty_in - qty_out) + product_qty,
					'document_origin': document_origen,
				}
				obj_history_stock.create(cr, uid, write_vals, context=context)
				qty_out = 0.00
				qty_in = 0.00
			if (qty_in < qty_out):
				res = qty_out - qty_in
				write_vals = {
					'product_id': this,
					'warehouse_id': warehouse_id,
					'location_id': location_id,
					'ajuste_id': data.id,
					'product_qty_move': -(qty_out - qty_in),
					'existencia_anterior': product_qty,
					'existencia_posterior': product_qty - res,
					'document_origin': document_origen,
				}
				obj_history_stock.create(cr, uid, write_vals, context=context)
				qty_out = 0.00
				qty_in = 0.00
		# code

	@api.one
	@api.constrains('name', 'description')
	def _check_description(self):
		self.env.cr.execute("""SELECT COUNT(*)
							FROM  stock_inventory
							WHERE name=%s""",(self.name,))
		count = self.env.cr.fetchone()[0]
		if count > 1:
			raise ValidationError(
				"El nombre del ajuste debe ser unico (!ya existe¡)")

	_columns = {
			'lote': fields.function(_lote_automatico,type="char",),
			'attrs_fields': fields.function(
				_attrs_fields_lotes,type="boolean"),
			}

stock_inventory()


class stock_inventory_line(osv.osv):
	_inherit = "stock.inventory.line"

	_columns = {
		'aduana': fields.char('Aduana',size=254),
		'pedimento': fields.char('Pedimento',size=254),
		'fecha': fields.date('Fecha Pedimento',size=254),
	}

stock_inventory_line()


class wizard(osv.osv_memory):
	_name = "wizard.inventory"

	def write_datos_pedimento(self, cr, uid, ids, context=None):
		""" 
			Escribe los datos de pedimento en las lineas 
				del ajuste de inventario.
			Siempre y cuando esten vacios los campos de pedimento,
				aduana, fecha y lote
			@return: True
		"""
		inventory_line_obj = self.pool.get('stock.inventory.line')
		data = self.browse(cr, uid, ids)
		val = {'aduana': data.aduana,
				'pedimento': data.pedimento,
				'fecha': data.fecha
				}
		if data.inventory_id.line_ids:
			for prod in data.inventory_id.line_ids:
				if not prod.aduana and not prod.pedimento and not \
						prod.fecha and not prod.prod_lot_id:
					inventory_line_obj.write(
						cr, uid,prod.id,val, context=context)
		else:
			raise osv.except_osv(
				('Alerta!'), (
					'El ajuste %s no tiene lineas con productos'
					% data.inventory_id.name))

	_columns = {
		'inventory_id': fields.many2one('stock.inventory','Ajuste'),
		'aduana': fields.char('Aduana', size=254, required=True),
		'pedimento': fields.char('Pedimento', size=254,required=True),
		'fecha': fields.date('Fecha', size=254,required=True),
	}
	_defaults = {
		'inventory_id': lambda self, cr, uid, context=None: self.pool.get(
			'stock.inventory').browse(
			cr, uid, context['active_id'], context=context)
	}
wizard()
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
