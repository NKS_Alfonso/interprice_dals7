# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, _


class product_category(models.Model):
    _inherit = 'product.category'

    property_account_discount_categ = fields.Many2one(comodel_name='account.account',
                                                      string="Cuenta de descuento",)
