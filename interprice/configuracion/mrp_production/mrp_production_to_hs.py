# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import fields, osv


class mrp_production(osv.osv):
	_inherit = 'mrp.production' 

	def action_production_end(self, cr, uid, ids, context=None):
		"""This function inherit of mrp.production model to add on _history_stock
		table and show the correct information on reports out
		"""
		res = super(mrp_production, self).action_production_end( cr, uid, ids, context=None)
		
		obj_history_stock = self.pool.get('history.stock')
		data = self.browse(cr, uid, ids)
		document_origen =  data.project_id.name
		picking_origin = data.location_src_id.name 

		for this in data:
			#Agregar manufactura a history_stock (entrada)
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
							INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
							WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(this.product_id.id,this.location_dest_id.id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								'product_id':this.product_id.id,
								'warehouse_id':self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id', '=', this.location_dest_id.id)])[0],
								'location_id':this.location_dest_id.id,
								'product_qty_move':this.product_qty,
								'existencia_anterior': product_qty,
								'existencia_posterior':product_qty+this.product_qty,
								'document_origin': document_origen,
								'picking_origin':'',
								'mrp_picking_type_code': 'incoming',
								}
					obj_history_stock.create(cr, uid, write_vals, context=context)

		warehouse_id = self.pool.get('stock.warehouse').search(cr, uid,[('lot_stock_id', '=', data.location_src_id.id)])[0]
		document_origen =  data.project_id.name
		location_id = data.location_src_id.id
		picking_origin = data.location_src_id.name
		data_product = data.product_lines

		for product in data_product:
			product_id = product.product_id.id
			cr.execute("""SELECT count(sq.qty),sum(qty)FROM stock_quant AS sq INNER JOIN stock_location AS sl
						INNER JOIN stock_warehouse AS sw ON sl.id= sw.lot_stock_id  ON sq.location_id=sl.id
						WHERE (sq.product_id=%s AND sl.id=%s AND sl.usage='internal')""",(product_id,location_id,))
			if cr.rowcount:
				for (qty, sm) in cr.fetchall():
					if qty==0: product_qty = 0
					elif(qty>0): product_qty = sm
					write_vals = {
								'product_id':product_id,
								'warehouse_id':warehouse_id,
								'location_id':location_id,
								'product_qty_move':-(product.product_qty),
								'existencia_anterior': product_qty,
								'existencia_posterior':product_qty-product.product_qty,
								'document_origin': document_origen,
								'picking_origin':'',
								'mrp_picking_type_code': 'outgoing',
								}
					obj_history_stock.create(cr, uid, write_vals, context=context)

		return res

