# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _, exceptions


class mrpProduction(models.Model):
    _inherit = 'mrp.production'

    def action_confirm(self, cr, uid, ids, context=None):
        """This function inherit of mrp.production model to add validation on
        the quantity available on stock before to confirm the production
        """
        mrp_id = ids[0]
        mrp_obj = self.pool.get('mrp.production').browse(cr, uid, mrp_id)
        location_id = mrp_obj.location_src_id
        move_lines_ids = mrp_obj.bom_id.bom_line_ids

        for product in move_lines_ids:
            product_id = product.product_id.id
            mrp_product_qty = product.product_qty

            cr.execute("""
                    SELECT SUM(sq.qty)
                    FROM stock_warehouse sw
                    INNER join stock_location sl
                    ON sw.lot_stock_id = sl.id
                    INNER JOIN stock_quant sq
                    ON sl.id=sq.location_id AND reservation_id is null
                    WHERE sl.usage='internal' and sw.lot_stock_id= %s
                    AND sq.product_id = %s""", (location_id.id, product_id))
            if cr.rowcount:
                total_qty_available = cr.fetchone()[0]
                if mrp_product_qty > total_qty_available:
                    raise exceptions.Warning(
                        _("No hay disponibilidad para este producto[%s] en esta ubicación %s") %\
                            (product.product_id.default_code, location_id.name)
                        )

        res = super(mrpProduction, self).action_confirm(cr, uid, ids, context=None)

        return res

    def action_assign(self, cr, uid, ids, context=None):
        """This function inherit of mrp.production model add validation on
        the quantity available on stock after confirm the production
        """
        mrp_id = context.get('params').get('id') or ids[0]
        mrp_obj = self.pool.get('mrp.production').browse(cr, uid, mrp_id)
        location_id = mrp_obj.location_src_id
        move_lines_ids = mrp_obj.move_lines
        for product in move_lines_ids:
            product_id = product.product_id.id
            mrp_product_qty = product.product_qty

            cr.execute("""
                    SELECT SUM(sq.qty)
                    FROM stock_warehouse sw
                    INNER join stock_location sl
                    ON sw.lot_stock_id = sl.id
                    INNER JOIN stock_quant sq
                    ON sl.id=sq.location_id AND reservation_id is null
                    WHERE sl.usage='internal' and sw.lot_stock_id= %s
                    AND sq.product_id = %s""", (location_id.id, product_id))
            if cr.rowcount:
                total_qty_available = cr.fetchone()[0]
                if mrp_product_qty > total_qty_available:
                    raise exceptions.Warning(
                        _("No hay disponibilidad para este producto[%s] en esta ubicación %s")%\
                            (product.product_id.default_code, location_id.name))

        res = super(mrpProduction, self).action_assign(cr, uid, ids, context=None)

        return res
