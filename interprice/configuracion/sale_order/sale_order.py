# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, fields, exceptions, models


class SaleOrder(models.Model):
    _inherit = 'sale.order' 

    order_is_web = fields.Boolean(
        default=False,
        string='Via Web',
        readonly=True,
        help="Todos los pedidos que sean realizados por medio del ecommerce \
              via API, activara este campo para distinguirlos de los pedidos \
              realizados por algun operador interno."
    )