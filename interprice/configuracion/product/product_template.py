# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, fields, exceptions, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    product_is_saved = fields.Boolean(
        default=False,
        string='Is saved',
        readonly=True,
        copy=False
    )

    web_ok = fields.Boolean(
        default=False,
        string='Puede ser Web'
    )

    @api.multi
    def write(self, vals):
        if 'default_code' in vals and vals['default_code']:
            vals['default_code'] = vals['default_code'].strip()
            self.default_code_unique(vals['default_code'])
            vals['product_is_saved'] = True
        else:
            if self.default_code and self.product_is_saved == False:
                vals['product_is_saved'] = True

        return super(ProductTemplate, self).write(vals)

    @api.model
    def create(self, vals):
        if 'default_code' in vals and vals['default_code']:
            vals['default_code'] = vals['default_code'].strip()
            self.default_code_unique(vals['default_code'])

        vals['product_is_saved'] = True

        return super(ProductTemplate, self).create(vals)

    def default_code_unique(self, default_code):
        prod_obj = self.env['product.product']
        id = prod_obj.search([('default_code', '=', default_code),('product_is_saved','=',True)])
        if id and id[0]:
            raise exceptions.Warning('La referencia interna %s del producto ya está registrado,'
                                     'genere o cambie por una nueva.' % default_code)
        return True
