# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#by Israel Cabrera Juarez

from openerp import api, models
from openerp.fields import Integer, One2many, Html
from openerp.osv import fields, osv
from openerp.osv import orm, fields
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class sale_order(models.Model):
    _inherit = 'sale.order'

    def action_button_confirm(self):
        """comprobando si la venta se genero a partir de una venta 
            consignacion o venta normal"""
        res = super(sale_order, self).action_button_confirm()
        self.env.cr. execute("""
                SELECT COUNT(origin)
                FROM sale_order
                WHERE id=%s""",(self.id,))
        origen = self.env.cr.fetchone()[0]
        if (origen > 0 ) :
            # #obteniendo el documento origen del pedido
            # cr. execute("""SELECT origin FROM sale_order WHERE id=%s""",(id,))
            # origen = cr.fetchone()[0]
            # #obteniendo el albaran del documento origen
            # cr.execute("""SELECT location_id FROM _res_configuracion""")
            # location_id = cr.fetchone()[0]
            # cr.execute("""SELECT sp.name FROM stock_picking AS sp INNER JOIN stock_move AS sm 
            #                              ON sp.id=sm.picking_id 
            #                              WHERE sm.location_dest_id=%s and sp.origin=%s""",(location_id,origen,))
            # picking_id = cr.fetchone()[0]
            # cr.execute("""SELECT state FROM stock_picking WHERE name=%s""",(picking_id,))
            # estado = cr.fetchone()[0]
            # # # # raise orm.except_orm(_('Alerta:'), _(" -- ")+str(estado))
            producto_id = self.env['gv.producto'].search(
                [('sale_order_id', '=', self.id)])[0]
            if producto_id:
                if (producto_id.stock_piking_id.state == 'done') :
                    # assert len(ids) == 1, 'This option should only be used for a single id at a time.'
                    # self.signal_workflow(cr, uid, ids, 'order_confirm')
                    self.action_confirm_sale()
                elif(producto_id.stock_piking_id.state != 'done'):
                    raise orm.except_orm(_('Alerta:'), _("Para poder confirmar este pedido  es necesario que se libere primero el albaran ")+str(obj_producto.stock_piking_id.name)+" Que se genero de la venta consignacion con el folio "+str(data.origin))
        else:
            assert len(self) == 1, 'This option should only be used for a single id at a time.'
            self.signal_workflow('order_confirm')
        return True
    
    def action_confirm_sale(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.signal_workflow(cr, uid, ids, 'order_confirm')
        if context.get('send_email'):
            self.force_quotation_send(cr, uid, ids, context=context)
        return True

    def create(self, cr, uid, vals, context=None):
        _sequence_obj = self.pool['ir.sequence']
        next_number = _sequence_obj.next_by_code(cr, uid, 'sale.order', context=None)
        order = None
        if next_number:
            vals['name'] = next_number
            if vals.get('partner_id') and any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id', 'fiscal_position']):
               defaults = self.onchange_partner_id(cr, uid, [], vals['partner_id'], context=context)['value']
               if not vals.get('fiscal_position') and vals.get('partner_shipping_id'):
                   delivery_onchange = self.onchange_delivery_id(cr, uid, [], vals.get('company_id'), None, vals['partner_id'], vals.get('partner_shipping_id'), context=context)
                   defaults.update(delivery_onchange['value'])
               vals = dict(defaults, **vals)
            ctx = dict(context or {}, mail_create_nolog=True)
            order = super(sale_order, self).create(cr, uid, vals, context=ctx)
            self.message_post(cr, uid, [order], body=_("Quotation created"), context=ctx)
        else:
            raise osv.except_osv(_('Error!'), (_('No se pudo generar la secuencia, favor de verificar la configuración!')))
        return order
