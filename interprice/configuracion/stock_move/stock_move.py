# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, _, exceptions


class stock_move(models.Model):
    _inherit = "stock.move"

    def _get_invoice_line_vals(self, cr, uid, move, partner, inv_type, context=None):
        res = super(stock_move, self)._get_invoice_line_vals(cr=cr,
                                                             uid=uid,
                                                             move=move,
                                                             partner=partner,
                                                             inv_type=inv_type,
                                                             context=context)

        if inv_type == 'out_refund':
            if not move.product_id.categ_id.property_account_discount_categ:
                raise exceptions.Warning(
                    _("La categoría interna del producto '%s' no tiene definida una cuenta de descuento." %
                      (move.product_id.name,)))
            account = move.product_id.categ_id.property_account_discount_categ.id
            if 'account_id' in res:
                res.update({
                    'account_id': account})
            else:
                res['account_id'] = account

        return res
