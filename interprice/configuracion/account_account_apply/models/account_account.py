# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp import fields, models, _


class AccountJournal(models.Model):
    _inherit = 'account.journal'
    account_account_apply_id = fields.Many2one('account.account.apply', string=_('Journal apply'))


class AccountAccount(models.Model):
    _inherit = 'account.account'
    account_account_apply_id = fields.Many2one('account.account.apply', string=_('Account apply'))
