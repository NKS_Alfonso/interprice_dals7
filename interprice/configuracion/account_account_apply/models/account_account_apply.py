# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp import fields, models, _


class AccountAccountApply(models.Model):
    _name = 'account.account.apply'
    _rec_name = 'name'

    name = fields.Char(string=_('Name'))
    code = fields.Char(string=_('Code'),
                       help='Default documents that will be processing cash,'
                            'bank, refund, unidentified_deposit and advance')
    _sql_constraints = [
        #('name_uniq', 'unique(name)', _("Can't be duplicate value for field name!"))
    ]
