# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# standard library imports
import time
# related third party imports
# local application/library specific imports
from openerp import models, api,fields, exceptions, _
import logging
_logger = logging.getLogger(__name__)

class sale_order(models.Model):
    """Extended model to add budget on change automatically"""

    # odoo model properties, Esta Heredando de la tabla sale_order
    _inherit = 'sale.order'

    def get_delivery_line(self):
        return self.env['sale.order.line'].search(
            [
                '&',
                ('order_id', '=', self.id),
                ('is_delivery', '=', True)
            ]
        )

    def onchange_delivery_set(self, carrier, is_change=False):
        parcel = carrier.grid_get(contact_id=self.partner_shipping_id.id)
        line = []
        grid = self.env['delivery.grid'].browse(carrier.grid_get(contact_id=self.partner_shipping_id.id))
        if not grid:
            raise exceptions.Warning(
                _('No Grid Available!'),
                _('No grid matching for this carrier!')
            )

        if self.state not in ('draft', 'sent'):
            raise exceptions.Warning(
                _('Order not in Draft State!'),
                _('The order state have to be draft to add delivery lines.')
            )

        taxes = grid.carrier_id.product_id.taxes_id.filtered(lambda t: t.company_id.id == self.company_id.id)
        taxes_ids = self.env['account.fiscal.position'].map_tax(taxes)
        taxes_ids = [tax.id for tax in taxes_ids]
        price = self.get_grid_price(
            grid=grid,
            dt=time.strftime('%Y-%m-%d')
        )
        list_price = price.get('list_price', 0.00)
        standard_price = price.get('standard_price', 0.00)

        if self.company_id.currency_id.id != self.currency.id:
            list_price = self.company_id.currency_id.with_context(
                todo_ttype='receipt'
            ).compute(
                list_price,
                self.currency,
                True,
            )
            standard_price = self.company_id.currency_id.with_context(
                todo_ttype='receipt'
            ).compute(
                standard_price,
                self.currency,
                True,
            )
            if list_price<0.01:
                list_price=0.01
            if standard_price<0.01:
                standard_price=0.01
        values = {
            'order_id': self.id,
            'name': grid.carrier_id.name,
            'product_uom_qty': 1,
            'product_uom': grid.carrier_id.product_id.uom_id.id,
            'product_id': grid.carrier_id.product_id.id,
            'price_unit': list_price,
            'purchase_price': standard_price,
            'tax_id': [(6, 0, taxes_ids)],
            'is_delivery': True,
        }

        if self.order_line:
            values['sequence'] = self.order_line[-1].sequence + 1

        delivery_line = self.get_delivery_line()

        if is_change and delivery_line:
            delivery_line.write(values)
        elif is_change and not delivery_line:
            # delivery_line.unlink()  # Delete current delivery line
            line = [(0, False, values)]
        elif not is_change and not delivery_line:
            line = [(0, False, values)]
        elif not is_change and delivery_line:
            delivery_line.write(values)
        return line or []

    @api.model
    def api_get_price_from_picking(self, carrier_id, total, weight, volume, quantity):
        grid = self.env['delivery.grid'].search([('carrier_id', '=', carrier_id)])
        result = self.get_price_from_picking(grid, total, weight, volume, quantity, create_by_api=True)
        if result:
            return result
        return False

    def get_grid_price(self, grid, dt):
        total = 0
        weight = 0
        volume = 0
        quantity = 0
        total_delivery = 0.0

        for line in self.order_line:
            if line.state == 'cancel':
                continue
            if line.is_delivery:
                total_delivery += line.price_subtotal + self._amount_line_tax(line)
            if not line.product_id or line.is_delivery:
                continue
            q = self.env['product.uom']._compute_qty(line.product_uom.id,line.product_uom_qty,line.product_id.uom_id.id)
            weight += (line.product_id.weight or 0.0) * q
            volume += (line.product_id.volume or 0.0) * q
            quantity += q
        total = (self.amount_total or 0.0) - total_delivery

        return self.get_price_from_picking(
            grid=grid,
            total=total,
            weight=weight,
            volume=volume,
            quantity=quantity
        )


    def get_price_from_picking(self, grid, total, weight, volume, quantity, create_by_api=False):
        model_name = 'state_amounts_delivery'
        test = False
        price = {
            'list_price': 0.01,
            'standard_price': 0.01
        }

        if not create_by_api:
            total = self.currency.with_context(
                todo_ttype='receipt'
            ).compute(
                total,
                self.company_id.currency_id,
                True,
            )
        price_dict = {
            'price': total,
            'volume': volume,
            'weight': weight,
            'wv': volume * weight,
            'quantity': quantity
        }

        price_set = False
        self.env.cr.execute(
            """
            SELECT MAX(priority_id) FROM delivery_grid_line_priority;
            """
        )
        prev_priority = self.env.cr.fetchone()[0]

        # Funciones nuevas para la optencion de los datos solicitados
        amount_partne_ids = []
        amount_states_ids = []
        partner_customers = []
        price_fixed = False
        partner = self.partner_shipping_id
        partner_states = partner.state_id

        for partners in grid.partner_ids:
            amount_partne_ids.append(partners.partner_mounts.id)

        for states in grid.state_delivery:
            amount_states_ids.append(states.state_id.id)

        if partner.id in amount_partne_ids:
            for partner_for in grid.partner_ids:
                if partner.id==partner_for.id:
                    if total >= partner_for.rode_mounts:
                        price['list_price'] = 0.01
                        price_set = True
                        break

        elif partner_states.id in amount_states_ids:
            for state_for in grid.state_delivery:
                if partner_states.id==state_for.state_id.id:
                    if total >= state_for.rode_mounts:
                        price['list_price'] = 0.01
                        price_set = True
                        break
            return price
        if price_set==False:
            for line in grid.line_ids:
                test = eval(line.type + line.operator + str(line.max_value), price_dict)
                # No entra a esta funcion no escha los parametros que manda o se optienen
                if (test and line.priority.priority_id <= prev_priority) or \
                        (test and not price_set):

                    if line.price_type == 'variable':
                        price['list_price'] = line.list_price * \
                            price_dict[line.variable_factor]
                        price['standard_price'] = line.standard_price * \
                            price_dict[line.variable_factor]
                    else:
                        price['list_price'] = line.list_price
                        price['standard_price'] = line.standard_price
                        price_set = True
                    # break
                prev_priority = line.priority.priority_id
            if not price_set:
                raise exceptions.Warning(
                    _("Unable to fetch delivery method!"),
                    _("Selected product in the delivery method doesn't"
                        " fulfill any of the delivery grid(s) criteria.")
                )
            return price



    def validate_unique_line_by_product(self, vals):
        exists_product = []
        # Valida los productos de la venta
        def validate_product():
            if product_id in exists_product:
                raise exceptions.MissingError(
                    _('Not is possible add more of once a product.\n Duplicate %s ') % (product_name)
                )
            else:
                exists_product.append(product_id)

        if isinstance(vals, dict):
            order_line = vals.get('order_line', None)
            if order_line and len(order_line) > 0:
                for line in order_line:
                    if isinstance(line[2], dict) and line[2].get('product_id', None):
                        product_id = line[2].get('product_id', -1)
                        product_name = line[2].get('name', '')
                        validate_product()
                    else:
                        if line[0] not in (2, 3, 5):
                            for oline in self.order_line:
                                if oline.id == line[1]:
                                    product_id = oline.product_id.id
                                    product_name = oline.name
                                    validate_product()

    @api.model
    def create(self, vals):
        self.validate_unique_line_by_product(vals)
        order = super(sale_order, self).create(vals)
        order.write({'carrier_id': vals.get('carrier_id', False)})
        return order

    @api.multi
    def write(self, vals):
        self.validate_unique_line_by_product(vals)
        new_vals = {}
        is_change = False
        super(sale_order, self).write(vals)
        if self.state == 'draft':
            if vals.get('carrier_id', False):
                carrier = self.env['delivery.carrier'].browse(
                    vals.get('carrier_id'))
                is_change = True
            else:
                carrier = self.carrier_id
                is_change = False
            if carrier:
                line = self.onchange_delivery_set(carrier, is_change)

                if line:
                    if new_vals.get('order_line', False):
                        new_vals['order_line'].append(line)
                    else:
                        new_vals['order_line'] = line
        return super(sale_order, self).write(new_vals)
