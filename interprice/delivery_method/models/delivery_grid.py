# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp.osv import osv
from openerp import models, fields, api, exceptions, _


class delivery_grid_line(models.Model):
    """Extended model to add priorities to delivery carrier"""

    # odoo model properties
    _inherit = 'delivery.grid.line'

    # custom fields
    priority = fields.Many2one(
        comodel_name="delivery.grid.line.priority",
        help=_("Delivery carrier condition priority"),
        require=True,
        string=_("Priority"),
    )


    # """@api.onchange('priority')
    # def onchange_priority(self):
    #     import pdb
    #     pdb.set_trace()
    #     """

    @api.model
    def create(self, vals):
        grid_id = vals.get('grid_id', False)
        if grid_id:
            grid = self.env['delivery.grid'].browse([grid_id])
        priority = vals.get('priority', False)
        cond_type = vals.get('type', False) or self.type
        priorities = [
            line.priority.id
            for line
            in grid.line_ids
            if line.priority != self.priority and
            line.type == cond_type
        ]
        if priority in priorities:
            raise exceptions.Warning(
                _("Can't set the same priority to different operators"
                    " of the same variable type"))
        return super(delivery_grid_line, self).create(vals)

    # """@api.multi
    # def write(self, vals):
    #     import pdb
    #     pdb.set_trace()
    #     priority = vals.get('priority', False)
    #     cond_type = vals.get('type', False) or self.type
    #     priorities = [
    #         line.priority.id
    #         for line
    #         in self.grid_id.line_ids
    #         if line.priority != self.priority and
    #         line.type == cond_type
    #     ]
    #     if priority in priorities:
    #         raise exceptions.Warning(
    #             _("Can't set the same priority to different operators"
    #                 " of the same variable type"))
    #     return super(delivery_grid_line, self).write(vals)"""


class delivery_operator_priority(models.Model):
    """Defines default priorities"""

    # odoo model properties
    _name = 'delivery.grid.line.priority'
    _rec_name = 'priority_id'

    # custom fields
    priority_id = fields.Integer(
        help=_("Delivery carrier condition priority"),
        string=_("Priority"),
        unique=True,
    )


class delivery_carrier(osv.osv):
    _inherit = "delivery.carrier"
    # _description = "state.amounts.delivery"

    def grid_get(self, cr, uid, ids, contact_id, context=None):
        contact = self.pool.get('res.partner').browse(cr, uid, contact_id, context=context)
        contry_obj = self.pool.get('state.amounts.delivery')

        for carrier in self.browse(cr, uid, ids, context=context):

            for grid in carrier.grids_id:
                get_id = lambda x: x.id
                country_ids = map(get_id, grid.country_ids) # Contiene el id pais
                state_ids = map(get_id, grid.state_ids) # contienen los id de los Estados

                if country_ids and not contact.country_id.id in country_ids:
                    continue
                if state_ids and not contact.state_id.id in state_ids:
                        continue
                if grid.zip_from and (contact.zip or '')< grid.zip_from:
                    continue
                if grid.zip_to and (contact.zip or '')> grid.zip_to:
                    continue
                return grid.id

        return False

    def create_grid_lines(self, cr, uid, ids, vals, context=None):
        cont = 0
        if context is None:
            context = {}
        grid_line_pool = self.pool.get('delivery.grid.line')
        grid_pool = self.pool.get('delivery.grid')
        for record in self.browse(cr, uid, ids, context=context):
            # if using advanced pricing per destination: do not change
            if record.use_detailed_pricelist:
                continue

            # not using advanced pricing per destination: override grid
            grid_id = grid_pool.search(
                cr, uid, [('carrier_id', '=', record.id)], context=context)
            if grid_id and not (record.normal_price is not False or record.free_if_more_than):
                grid_pool.unlink(cr, uid, grid_id, context=context)
                grid_id = None

            if not (record.normal_price is not False or record.free_if_more_than):
                continue

            if not grid_id:
                grid_data = {
                    'name': record.name,
                    'carrier_id': record.id,
                    'sequence': 10,
                }
                grid_id = [grid_pool.create(
                    cr, uid, grid_data, context=context)]

            lines = grid_line_pool.search(
                cr, uid, [('grid_id', 'in', grid_id)], context=context)
            if lines:
                grid_line_pool.unlink(cr, uid, lines, context=context)

            # create the grid lines
            if record.free_if_more_than:
                cont += 1
                line_data = {
                    'grid_id': grid_id and grid_id[0],
                    'name': _('Free if more than %.2f') % record.amount,
                    'type': 'price',
                    'operator': '>=',
                    'max_value': record.amount,
                    'standard_price': 0.0,
                    'list_price': 0.0,
                    'priority': self.pool.get(
                        'delivery.grid.line.priority').search(
                        cr, uid, [('priority_id', '=', cont)]
                    )[0]
                }
                grid_line_pool.create(cr, uid, line_data, context=context)
            if record.normal_price is not False:
                cont += 1
                line_data = {
                    'grid_id': grid_id and grid_id[0],
                    'name': _('Default price'),
                    'type': 'price',
                    'operator': '>=',
                    'max_value': 0.0,
                    'standard_price': record.normal_price,
                    'list_price': record.normal_price,
                    'priority': self.pool.get(
                        'delivery.grid.line.priority').search(
                        cr, uid, [('priority_id', '=', cont)]
                    )[0]
                }
                grid_line_pool.create(cr, uid, line_data, context=context)
        return True
