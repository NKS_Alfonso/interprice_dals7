# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import osv
from openerp import models, fields, api, exceptions, _
from openerp.exceptions import except_orm, Warning, RedirectWarning

class delivery_grid_segment(models.Model):

    _inherit = "delivery.grid"

    segment_id = fields.Many2one('product.segment', string="Customer Segment")
    partner_id = fields.Many2one('res.partner',string="Cliente")
    contry_id = fields.Many2one('res.country',string="Contry")
    state_delivery = fields.One2many(comodel_name='state.amounts.delivery',inverse_name='delivery_grid_id',string="States")
    zip_from_delivery = fields.Many2one('res.country.state',string = "C.Postal inicial")
    zip_to_delivery = fields.Many2one('res.country.state',string = "C.Postal final")
    partner_ids = fields.One2many(comodel_name = 'partner.amounts',inverse_name = 'delivery_grid_id',string="customer")

    @api.onchange('segment_id')
    def delete_fields(self):
        # self.partner_ids.unlink()
        warning = ''
        for recordself in self:
            if recordself.partner_ids:
                for partner in recordself.partner_ids:
                    partner.partner_mounts = False
                    partner.segment = recordself.segment_id.id
                warning = {
                    'title': _('Partner Warning!'),
                    'message' : _('If you change the segment of this delivery, you will search partners again.')
                }
        self.write({'segment_id':self.segment_id.id})
        return {'warning': warning}

    @api.multi
    def write(self, vals):
        arr_index = []
        index_arr = []
        res = super(delivery_grid_segment, self).write(vals)
        for recordself in self:
            if recordself.state_delivery:
                for state in recordself.state_delivery:
                    index_arr.append(state.state_id.id)
                    if len(index_arr) != len(set(index_arr)):
                        raise Warning(_('You cannot insert the state twice, Please change the existing.'))
        return res

    @api.onchange('state_delivery')
    def change_state_delivery(self):
        # self.partner_ids.unlink()
        index_arr = []
        warning = ''
        for recordself in self:
            if recordself.state_delivery:
                for state in recordself.state_delivery:
                    index_arr.append(state.state_id.id)
                    if len(index_arr) != len(set(index_arr)):
                        warning = {
                            'title': _('States Warning!'),
                            'message' : _('You cannot insert the state twice, Please change the existing.')
                        }
        return {'warning': warning}
                        # raise Warning(_('You cannot insert the state twice, Please delete or change the existing.'))

class delivery_grid_amounts(models.Model):
    _name = "partner.amounts"

    @api.multi
    def _get_active_id(self):
        active_id = self.env.context.get("id")
        return self.env.context.get("id")
    
    @api.multi
    def _get_active_id_seg(self):
        active_id = self.env.context.get("id")
        ssegment = self.env.context.get("segment_id")
        del_obj = self.env['delivery.grid']
        deli = del_obj.search([('id','=',active_id)])
        segment = 0
        print self.env.context
        if deli.segment_id:
            segment = deli.segment_id.id
        if not segment:
            segment = ssegment
        return segment

    partner_mounts = fields.Many2one('res.partner',string="Customers", required=True)
    rode_mounts = fields.Float(string="amount")
    delivery_grid_id = fields.Many2one('delivery.grid',default=_get_active_id,string="Delivery Grid")
    segment = fields.Many2one('product.segment',string="Segment",default=_get_active_id_seg)


class delivery_state_amounts(models.Model):
    _name = "state.amounts.delivery"

    @api.multi
    def _get_active_id(self):
        active_id = self.env.context.get("id")
        return self.env.context.get("id")

    state_id = fields.Many2one('res.country.state',string="Country", required=True)
    rode_mounts = fields.Float(string="amounts")
    delivery_grid_id = fields.Many2one('delivery.grid',default=_get_active_id,string="Delivery Grid")
    states = fields.Many2many('res.country.state',related='delivery_grid_id.state_ids',string="States", store=False)
