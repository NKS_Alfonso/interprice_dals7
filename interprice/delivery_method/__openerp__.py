# -*- coding: utf-8 -*-
{
    'name': 'Delivery method',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Sales',
    'description': 'Adds budget when delivery method is selected',
    'depends': ['base', 'sale', 'delivery','postal_code'],
    'application': True,
    'data': [
        'views/sale_order_view.xml',
        'views/delivery_grid_line_view.xml',
        'views/delivery_grid_segment_view.xml',
        'security/ir.model.access.csv',
        'data/default_priority_data.xml'
    ]
}
