# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _, exceptions

class catalogo_garantias(models.Model):
    _name = 'catalogo.garantias'
    _inherit = 'mail.thread'
    _description = u'Garantía'
    _rec_name = 'garantia_en_catalogo'
    _defaults = {}
    _sql_constraints = [('nombre_unico', 'UNIQUE(garantia_en_catalogo)',
         "El nombre de la garantía debe ser unico"),]
    
    garantia_en_catalogo = fields.Char(string='Garantía', required=True)
    descripcion_de_garantia = fields.Text(string='Descripcion', required=False)
    # fecha = fields.Date(String='Fecha', required=True)
    # dias_en_garantia = fields.Float(string="Dias en garantia")
