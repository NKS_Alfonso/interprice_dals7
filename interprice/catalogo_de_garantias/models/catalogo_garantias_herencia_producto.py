# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _, exceptions


class catalogo_garantias_herencia_producto(models.Model):
    #_name = 'catalogo.garantias.herencia.producto'
    _inherit = 'product.template'
    _defaults = {}

    _sql_constraints = []
    
    # Campo original en la vista padre product template
    # warranty = fields.Char(help="What needs to be done?")
    garantia = fields.Many2one(
        comodel_name='catalogo.garantias',
        String='Garantía'
    )
    
