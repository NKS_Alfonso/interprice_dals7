# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author TO-DO Developers
#

{
    'name': 'Catalogo de Garantias',

    'description': 
        """
        Modulo para la creacion de un catalogo de garantías,
        el cual agrega una nueva opcion en el menu:
        Compras --> Configuracion --> Producto --> Garantías
        en el cual se crearan las garantías para el catalogo.
        """,

    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',

    'website': 'http://www.grupovadeto.com',

    'category': 'Uncategorized',

    'version': '0.1',

    'depends': [
        'base',
        'purchase',
        'sale',
        ],

    'data': [
        'views/catalogo_de_garantias_view.xml',
        'views/catalogo_de_garantias_product_template_inherit_view.xml',
    ]
}
