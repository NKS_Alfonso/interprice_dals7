# -*- coding: utf-8 -*-
{
    'name': 'Payment Task for Magento',

    'summary': "Prepare environment for task received by web magento",
    'author': 'Copyright © 2017 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    'category': 'Project',
    'version': '1.0',

    'depends': [
        'base',
        'project',
    ],
    'data': [
        'views/res_config_view.xml',
        'views/project_task_view.xml',
    ],
    'installable': True,
}
