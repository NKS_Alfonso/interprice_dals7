# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, _

class ResCompanyInherit(models.Model):
    _inherit = 'res.company'

    project_id = fields.Many2one('project.project', _('Project name'))
    project_user_id = fields.Many2one('res.users', _('Username'))
