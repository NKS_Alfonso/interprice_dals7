# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import fields, osv
from openerp.tools.translate import _

class project_configuration(osv.osv_memory):
    _inherit = 'project.config.settings'

    _columns = {
        'project_name': fields.many2one('project.project',
            string=_('Default project'),
            help=_('This feature helps to define a default project')),
        'project_user_name': fields.many2one('res.users',
            string=_('Responsible user'),
            help=_('This feature sets a default username to be assigned when creating a task'))
    }

    def get_default_project_name(self, cr, uid, fields, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return {'project_name': user.company_id.project_id.id}

    def set_project_name(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        user.company_id.write({'project_id': config.project_name.id})

    def get_default_project_user_name(self, cr, uid, fields, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return {'project_user_name': user.company_id.project_user_id.id}

    def set_project_user_name(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        user.company_id.write({'project_user_id': config.project_user_name.id})
