# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import datetime

from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _

class task(osv.osv):
    _inherit = "project.task"

    _columns = {
        'web_task': fields.boolean(_('Magento Task'),help=_("If checked, this task is created to manage and assing payment traking")),
        'company_currency_id': fields.related('company_id','currency_id',type="many2one",relation="res.currency",string=_("Company Currency"),readonly=True,store=True),
        'sale_order_id': fields.many2one('sale.order', _('Sale Order'), track_visibility='onchange'),
        'date_order': fields.date(_('Date Order'), copy=False),
        'amount_order': fields.float(_('Amount Order'), digits=(16,2), help=_("Total of sale order amount")),
        'int_number_mag': fields.char(_('Magento number'), help=_("Its the assigned number in magento DB")),
        'paymethod_id': fields.many2one('piramide_de_pago', _('Pay method'), track_visibility='onchange')
    }
