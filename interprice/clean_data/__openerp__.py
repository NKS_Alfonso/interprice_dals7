# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Clean data',
    'version': '8.0.0.1',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Setting',
    'license': 'AGPL-3',
    'sequence': 2,
    'summary': 'Clean modules data.',
    'description': """

Clean data
============
Clean data

    """,
    'images': [],
    'depends': ['base'],
    'data': [
        'views/clean_models_data_view.xml',
        'views/res_users_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
