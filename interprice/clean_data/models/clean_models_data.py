# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, models, _, exceptions


class CleanModelsData(models.Model):
    _name = 'clean.models.data'
    _description = u"Reset data"

    @api.multi
    def remove_all(self):
        try:
            self.remove_account(full=True)
            self.remove_sales(full=True)
            self.remove_project()
            self.remove_devolution()
            self.remove_purchase(full=True)
            self.remove_mrp(full=True)
            self.remove_inventory(full=True)
            self.remove_fleet()
            self.remove_document()
            self.remove_configs_data()
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True
    @api.multi
    def remove_sales(self, full=None):
        if full == True:
            to_removes = [
                # remove sales
                ['sale.order.line', ],
                ['sale.order', ],
                ['crm.lead', ],
                ['crm.case.section', ],
                ['account.analytic.line', ],
                ['account.analytic.invoice.line', ],
                ['account.analytic.account', ],
                ['crm.phonecall', ],
                ['price.list', ],
                ['res.partner.bank', ],
                ['res.partner.bank.type', ],
                ['res.bank', ],
                ['regimen.fiscal', ],
                ['pay.method', ],
                ['crm.case.categ', ],
                ['crm.case.stage', ],
                ['crm.tracking.source', ],
                ['crm.tracking.campaign', ],
                ['crm.payment.mode', ],
            ]
        else:
            to_removes = [
                # Models Sale
                ['sale.order.line', ],
                ['sale.order', ],
                ['crm.lead', ],
            ]
        try:
            for line in to_removes:
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
            # Reset sequence initial
            seqs = self.env['ir.sequence'].search([('code', '=', 'sale.order')])
            for seq in seqs:
                seq.write({
                    'number_next': 1,
                })
            sql = "update ir_sequence set number_next=1 where code ='sale.order';"
            self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_project(self):
        to_removes = [
            # Clear production project
            ['project.project', ],
            ['project.task', ],
            ['project.category', ],
            ['project.task.work', ],
        ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_account(self, full=None):
        if full == True:
            to_removes = [
                # Clear financial accounting documents
                ['account.voucher.line', ],
                ['account.voucher', ],
                ['account.bank.statement.line', ],
                ['account.bank.statement', ],
                ['account.analytic.line', ],
                ['account.invoice.line', ],
                ['account.invoice', ],
                ['account.move.line', ],
                ['account.move', ],
                ['account.invoice.tax', ],
                ['cfdi.sign', ],
                ['advance.payment', ],
                ['deposit.file', ],
                ['gv.saldos.cliente', ],
                ['gv.cuenta.cliente', ],
                ['gv.saldos.proveedor', ],
                ['gv.cuenta.proveedor', ],
                ['dni.dni', ],
                ['documentos.bancarios', ],
                ['ingresos.egresos', ],
                ['cfdi.table', ],
                ['related.document.cfdi', ],
                ['cash.back', ],
                ['inter_pagos.pago', ],
                ['account.check.deposit', ],
                ['account.check', ],
                ['account.bank_transfert', ],
                ['payment.order', ],
                ['payment.line', ],
                ['bank.payment.line', ],
                ['account.asset.asset', ],
                ['account.subscription.line', ],
                ['account.subscription', ],
                ['account.model', ],
                ['ifrs.lines', ],
                ['ifrs.ifrs', ],
                ['account_followup.followup', ],
                ['account.journal', ],
                ['account.common.report', ],
                ['account.analytic.journal', ],
                ['payment.mode', ],
                ['payment.mode.type', ],
                ['account.statement.operation.template', ],
                ['accounting.report', ],
                ['account.account', ],
            ]
        else:
            to_removes = [
                # Clear financial accounting documents
                ['account.voucher.line', ],
                ['account.voucher', ],
                ['account.bank.statement', ],
                ['account.bank.statement.line', ],
                ['account.analytic.line', ],
                ['account.invoice.line', ],
                ['account.invoice', ],
                ['account.move.line', ],
                ['account.move', ],
                ['account.invoice.tax', ],
                ['cfdi.sign', ],
                ['advance.payment', ],
                ['deposit.file', ],
                ['gv.saldos.cliente', ],
                ['gv.cuenta.cliente', ],
                ['gv.saldos.proveedor', ],
                ['gv.cuenta.proveedor', ],
                ['dni.dni', ],
                ['documentos.bancarios', ],
                ['ingresos.egresos', ],
                ['cfdi.table', ],
            ]
        try:
            for line in to_removes:
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
                    # Update serial number
                    seqs = self.env['ir.sequence'].search([
                        '|', ('code', '=', 'account.reconcile'),
                        '|', ('code', '=', 'account.payment.customer.invoice'),
                        '|', ('code', '=', 'account.payment.customer.refund'),
                        '|', ('code', '=', 'account.payment.supplier.invoice'),
                        '|', ('code', '=', 'account.payment.supplier.refund'),
                        '|', ('code', '=', 'account.payment.transfer'),
                        '|', ('prefix', 'like', 'BNK1/'),
                        '|', ('prefix', 'like', 'CSH1/'),
                        '|', ('prefix', 'like', 'INV/'),
                        '|', ('prefix', 'like', 'EXCH/'),
                        '|', ('prefix', 'like', 'MISC/'),
                        '|', ('prefix', 'like', u'BILL/'),
                        ('prefix', 'like', u'MISC/')
                    ])

                    for seq in seqs:
                        seq.write({
                            'number_next': 1,
                        })
                    #     todo: BILL/%
                    sql = "update ir_sequence set number_next=1 where (" \
                          "code ='account.reconcile' " \
                          "or code ='account.payment.customer.invoice' " \
                          "or code ='account.payment.customer.refund' " \
                          "or code ='account.payment.supplier.invoice' " \
                          "or code ='account.payment.supplier.refund' " \
                          "or prefix like 'BNK1/%'" \
                          "or prefix like 'CSH1/%'" \
                          "or prefix like 'INV/%'" \
                          "or prefix like 'EXCH/%'" \
                          "or prefix like 'MISC/%'" \
                          "or prefix like 'BILL/%'" \
                          ");"
                    self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_devolution(self):
        to_removes = [
            # Clear devolutions
            ['gv.devolucion', ],
        ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_purchase(self, full=None):
        if full == True:
            to_removes = [
                # Clear purchase documents
                ['purchase.order.line', ],
                ['purchase.order', ],
                ['purchase.requisition.line', ],
                ['purchase.requisition', ],
                ['catalogo.garantias', ],
                ['product.pricelist', ],
                ['product.pricelist.version', ],
                ['product.price.type', ],
            ]
        else:
            to_removes = [
                # Clear purchase documents
                ['purchase.order.line', ],
                ['purchase.order', ],
                ['purchase.requisition.line', ],
                ['purchase.requisition', ],
            ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    print obj._table_exist
                    self._cr.execute(sql)
            # Update serial number
            seqs = self.env['ir.sequence'].search([('code', '=', 'purchase.order')])
            for seq in seqs:
                seq.write({
                    'number_next': 1,
                })
            sql = "update ir_sequence set number_next=1 where code ='purchase.order';"
            self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_inventory(self, full=None):
        if full == True:
            to_removes = [
                # Clear inventory documents
                ['stock.quant', ],
                ['stock.quant.package', ],
                ['stock.quant.move.rel', ],
                ['stock.move', ],
                ['stock.pack.operation', ],
                ['stock.picking', ],
                ['stock.picking.type', ],
                ['stock.scrap', ],
                ['stock.inventory.line', ],
                ['stock.inventory', ],
                ['stock.production.lot', ],
                ['stock.fixed.putaway.strat', ],
                ['make.procurement', ],
                ['procurement.order', ],
                ['procurement.group', ],
                ['document.inventory.line', ],
                ['document.inventory.type', ],
                ['document.inventory', ],
                ['document.reference', ],
                ['history.stock', ],
                ['account.cost.center', ],
                ['stock.warehouse', ],
                ['stock.location', ],
                ['configuration.assortment.type', ],
                ['configuration.assortment', ],
                ['stock.warehouse.orderpoint', ],
                ['procurement.rule', ],
                ['stock.location.route', ],
                ['stock.location.path', ],
                ['configuration.document.type', ],
                ['configuration.delivery.carrier', ],
                ['delivery.carrier', ],
                ['delivery.grid.line', ],
                ['delivery.grid', ],
            ]
        else:
            to_removes = [
                # Clear inventory documents
                ['stock.quant', ],
                ['stock.quant.package', ],
                ['stock.quant.move.rel', ],
                ['stock.move', ],
                ['stock.pack.operation', ],
                ['stock.picking', ],
                ['stock.scrap', ],
                ['stock.inventory.line', ],
                ['stock.inventory', ],
                ['stock.production.lot', ],
                ['stock.fixed.putaway.strat', ],
                ['make.procurement', ],
                ['procurement.order', ],
                ['procurement.group', ],
                ['document.inventory', ],
                ['document.inventory.line', ],
                ['document.reference', ],
                ['history.stock', ],
                #['stock.history', ],
            ]
        try:
            # For optimization, increase processing
            self._cr.execute("update stock_move set split_from=NULL;")
            self._cr.execute("update stock_move set origin_returned_move_id=NULL;")
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
            # Update serial number
            seqs = self.env['ir.sequence'].search([
                '|', ('code', '=', 'stock.lot.serial'),
                '|', ('code', '=', 'stock.lot.tracking'),
                '|', ('code', '=', 'stock.orderpoint'),
                '|', ('code', '=', 'stock.picking'),
                '|', ('code', '=', 'stock.quant.package'),
                '|', ('code', '=', 'stock.scrap'),
                '|', ('code', '=', 'stock.picking'),
                '|', ('prefix', '=', 'WH/IN/'),
                '|', ('prefix', '=', 'WH/INT/'),
                '|', ('prefix', '=', 'WH/OUT/'),
                '|', ('prefix', '=', 'WH/PACK/'),
                ('prefix', '=', 'WH/PICK/')
            ])

            for seq in seqs:
                seq.write({
                    'number_next': 1,
                })
            sql = "update ir_sequence set number_next=1 where (" \
                  "code ='stock.lot.serial' " \
                  "or code ='stock.lot.tracking' " \
                  "or code ='stock.orderpoint'" \
                  "or code ='stock.picking'" \
                  "or code ='stock.quant.package'" \
                  "or code ='stock.scrap'" \
                  "or code ='stock.picking'" \
                  "or prefix ='WH/IN/'" \
                  "or prefix ='WH/INT/'" \
                  "or prefix ='WH/OUT/'" \
                  "or prefix ='WH/PACK/'" \
                  "or prefix ='WH/PICK/'" \
                  ");"
            self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_mrp(self, full=None):
        if full == True:
            to_removes = [
                # Clear production documents
                ['mrp.workcenter.productivity', ],
                ['mrp.production.workcenter.line', ],
                ['mrp.production', ],
                ['mrp.production.product.line', ],
                ['mrp.unbuild', ],
                ['change.production.qty', ],
                ['operation.time.line', ],
                ['mrp.repair', ],
                ['mrp.subproduct', ],
                ['mrp.bom.line', ],
                ['mrp.bom', ],
                ['mrp.operation.workcenter', ],
                ['mrp.routing.workcenter', ],
                ['mrp.routing.operation', ],
                ['mrp.routing', ],
                ['mrp.workcenter', ],
                ['mrp.property.group', ],
            ]
        else:
            to_removes = [
                # Clear production documents
                ['mrp.workcenter.productivity', ],
                ['mrp.production.workcenter.line', ],
                ['mrp.production', ],
                ['mrp.production.product.line', ],
                ['mrp.unbuild', ],
                ['change.production.qty', ],
                ['operation.time.line', ],
                ['mrp.repair', ],
                ['mrp.subproduct', ],
            ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    print obj._table_exist
                    self._cr.execute(sql)
            # Update serial number
            seqs = self.env['ir.sequence'].search(['|', ('code', '=', 'mrp.production'), ('code', '=', 'mrp.unbuild')])
            for seq in seqs:
                seq.write({
                    'number_next': 1,
                })
            sql = "update ir_sequence set number_next=1 where (code ='mrp.production' or code ='mrp.unbuild');"
            self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True


    @api.multi
    def remove_fleet(self):
        to_removes = [
            # Clear fleets documents
            ['fleet.vehicle.model', ],
            ['fleet.vehicle.model.brand', ],
            ['fleet.service.type', ],
        ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_document(self):
        to_removes = [
            # Clear documents
            ['document.directory', ],
        ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    sql = "delete from %s" % obj._table
                    print obj._table_exist
                    self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True

    @api.multi
    def remove_configs_data(self):
        to_removes = [
            # Clear configs data
            ['res.company.facturae.certificate', ],
            ['payment.acquirer', ],
            ['payment.transaction', ],
            ['params.pac', ],
            ['mail.message.subtype', ],
            ['mail.message', ],
            ['mail.mail', ],
            ['mail.notification', ],
            ['ir.mail_server', ],
            ['fetchmail.server', ],
            ['mail.followers', ],
            ['ir.attachment', ],
            ['res.users', ],
            ['res.partner', ],
            ['piramide_de_pago', ],
            ['product.template', ],
            ['product.category', ],
            ['product.segment', ],

        ]
        try:
            for line in to_removes :
                obj_name = line[0]
                obj = self.pool.get(obj_name)
                if obj and obj._table_exist:
                    if obj_name == 'res.users':
                        sql = "delete from %s where id!=1" % obj._table
                    elif obj_name == 'res.partner':
                        sql = """delete from {} where id not in(
                        select partner_id from res_users where id=1
                        union
                        select partner_id from res_company where id=1)""".format(
                            obj._table)
                    else:
                        sql = "delete from %s" % obj._table
                    self._cr.execute(sql)
        except Exception, e:
            raise exceptions.Warning(_(e))
        return True
