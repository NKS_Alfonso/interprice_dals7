# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
from openerp import api, models, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.one
    def _get_user_to_clean(self):
        if self.id == 1 and self._context['uid'] == 1:
            self.user_to_clean = True
        else:
            self.user_to_clean = False

    user_to_clean = fields.Boolean(
        compute='_get_user_to_clean',
        string="User to clean",
        store=False
    )
    show_button_delete =fields.Boolean(string='Show button delete')
    show_delete_all =fields.Boolean(string='Show button delete all')



    @api.multi
    def open_view(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'clean.models.data',
            'view_id':
                self.env.ref('clean_data.iuv_todo_clean_all_data_form', False).id,
            'view_mode': 'form',
            'target': 'new',
        }
