# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api
from lxml import etree
import json


class DocumentInventoyType(models.Model):
    _inherit = "document.inventory.type"

    group_ids = fields.Many2many('res.groups', string='assign groups')


class DocumentInventory(models.Model):
    _inherit = 'document.inventory'

    @api.model
    def fields_view_get(
        self, view_id=None, view_type=False,
            toolbar=False, submenu=False):
        res = super(DocumentInventory, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            # validate when in form view
            document_type_ids = []
            user_group_ids = []
            # get groups from user login
            user_group_ids = [g.id for g in self.env[
                'res.users'].search([('id', '=', self._uid)]).groups_id]
            if user_group_ids:
                # validation and match of user groups and groups in document type
                document_type_ids = [d.id for d in self.env[
                    'document.inventory.type'].search(
                        [('group_ids', 'in', user_group_ids)])]
            doc = etree.XML(res['arch'])
            nodes = doc.xpath("//field[@name='type_document']")
            # get field from view
            for node in nodes:
                # set domain value on view
                node.set('domain', "[('id', 'in', {})]".format(
                    document_type_ids))
            # write node in view xml and return view
            res['arch'] = etree.tostring(doc)
        return res

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def fields_view_get(
        self, view_id=None, view_type=False,
            toolbar=False, submenu=False):
        res = super(StockPicking, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            if not self.env.user.has_group('permisos.stock_picking_editable_field'):
                doc = etree.XML(res['arch'])
                nodes = doc.xpath("//field[@name='partner_id']")
                # get field from view
                for node in nodes:
                    if 'modifiers' in node.attrib:
                        modifiers = json.loads(node.attrib["modifiers"])
                        modifiers['readonly'] = 'True'
                        node.attrib['modifiers'] = json.dumps(modifiers)
                # write node in view xml and return view
                res['arch'] = etree.tostring(doc)

            if self.env.user.has_group('permisos.iuv_todo_stock_picking_button_force_Availability'):
                doc = etree.XML(res['arch'])
                nodes = doc.xpath("//button[@name='force_assign']")
                for node in nodes:
                    if 'modifiers' in node.attrib:
                        modifiers = json.loads(node.attrib["modifiers"])
                        modifiers['invisible'] = False
                        node.attrib['modifiers'] = json.dumps(modifiers)
                # write node in view xml and return view
                res['arch'] = etree.tostring(doc)


        return res
