# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp import fields, api, models


class LineasFacturaProveedor(models.Model):
    _inherit = 'account.invoice.line'

    @api.model
    def create(self, vals):
        if 'product_id' in vals:
            product_id = vals['product_id']
            product = self.env['product.product'].browse(product_id)
            if 'name' and 'uos_id' not in vals:
                vals['name'] = product.display_name
                vals['uos_id'] = product.uom_id.id
            else:
                vals.update({'name': product.display_name,
                             'uos_id': product.uom_id.id})
        res = super(LineasFacturaProveedor, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if 'product_id' in vals:
            product_id = vals['product_id']
            product = self.env['product.product'].browse(product_id)
            if 'name' and 'uos_id' not in vals:
                vals['name'] = product.display_name
                vals['uos_id'] = product.uom_id.id
            else:
                vals.update({'name': product.display_name,
                             'uos_id': product.uom_id.id})
        res = super(LineasFacturaProveedor, self).write(vals)
        return res

