# -*- coding: utf-8 -*-
{
    'name': "Permisos",

    'description': """
        * Permite asignar grupos a usuarios para poder restringuir la edicion de campos en Cotizaciones y Pedidos de ventas.
        * Ventas/Cotizaciones/Pedidos/Lectura, usuarios agregados a este grupo no podran editar campos.
        * Ventas/Cotizaciones/Pedidos/Invisible, usuarios agregados a este grupo no podran ver campos.
        * Ventas/Cotizaciones/Pedidos/Precio Unitario/Lectura, usuario que no podran editar el precio unitario.
        * Ventas/Clientes-Proveedores/no crear, usuarios que no podran crear/editar/eliminar clientes y proveedores.
        * Ventas/Cotizaciones/Pedidos/Precio coste/invisible, usuario que no podra ver el precio coste
        * Ventas/Cotizaciones/Pedidos/Confirmados/Lectura, Usuario no podrá editar campos en estado confirmado.
        * Permite que en facturas de proveedor/cliente no se puedan editar campos, crear o acceder a registros.
        * Valida la creación de movimientos de inventario, configurando los grupos a los que pertenece el usuario y el grupo configurado en el tipo de movimiento.
        * Valida si el usuario pertenece al grupo Almacen/Albaranes/Escritura para poder seleccionar un cliente en el albaran.
    """,

    'author': "Copyright © 2017 TO-DO - All Rights Reserved",
    'website': "http://www.gvadeto.com",

    'category': 'settings',
    'version': '0.1',

    'depends': [
        'base',
        'sale',
        'sale_margin',
        'centro_de_costos',
        'account_payment_partner',
        'account',
        'documents_inventory',
        'centro_de_costos',
        'stock'
    ],

    'data': [
        'ventas/ventas_cotizaciones_pedidos_invisible.xml',
        'ventas/ventas_cotizaciones_pedidos_lectura.xml',
        'ventas/ventas_clientes_proveedores_nocrear.xml',
        'ventas/ventas_cotizaciones_pedidos_confirmados_lectura.xml',
        'factura/factura_proveedor_no_crear_editar.xml',
        'factura/factura_cliente_no_crear_editar.xml',
        'almacen/inherit_document_inventory_type_view.xml',
        'almacen/stock_picking.xml',
    ],
}
