# -*- coding: utf-8 -*-
{
    'name': 'Factura PAC Diverza',
    'version': '1.0',
    'description': """
        Diverza

        * http://desarrolladores.diverza.com/timbre/endpoints-legacy.html
        * http://desarrolladores.diverza.com/timbre/cancelacion/cancel-legacy.html
        Lib install

        * pip install pyopenssl ndg-httpsclient pyasn1
        * Install Package SOAPpy with the command "sudo apt-get install python-soappy
    """,
    'author': 'Jose J. H. Bautista (gvadeto)',
    'depends': [
        'base',
        'l10n_mx_ir_attachment_facturae'
    ],
    'data': [
        # 'ir_attachment_facturae_mx.xml',
        'params_pac.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}