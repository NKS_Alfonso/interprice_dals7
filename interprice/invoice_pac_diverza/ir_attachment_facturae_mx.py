# -*- coding: utf-8 -*-
__author__ = 'Jose J. H. Bautista'
__email__ = 'jose.bautista@gvadeto.com'
__copyright__ = 'Copyright 2016, GVadeto'

from openerp import fields, models, api


class IrAttachmentFacturaeMx(models.Model):
  _inherit = 'ir.attachment.facturae.mx'
  file_xml_sign_diverza = fields.Many2one('ir.attachment', 'Xml Diverza', readonly=True)


