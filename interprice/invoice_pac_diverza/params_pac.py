# -*- coding: utf-8 -*-
__author__ = 'Jose J. H. Bautista'
__email__ = 'jose.bautista@gvadeto.com'
__copyright__ = 'Copyright 2016, GVadeto'
from openerp import models, api, _, fields
from openerp.osv import osv, fields as fieldsv7

class ParamsPac (models.Model):
    _inherit ='params.pac'

    @api.model
    def services_providers(self):
        return [
            ('solucion_factible', 'Solución Factible'),
            ('diverza', 'Diverza'),
            ('smarter_web', 'Smarter Web')
        ]

    select_pac = fields.Selection(services_providers,
        string='Proveedores Autorizados de Certificación (PAC)',
        required=True,)


class ParamsPacV7(osv.Model):
    _inherit = 'params.pac'

    def _get_method_type_selection(self, cr, uid, context=None):
        types = super(ParamsPacV7, self)._get_method_type_selection(cr, uid, context=context)
        return [
            ('cancel', _('Cancel')),
            ('sign', _('Sign')), ]

    _columns = {
        'method_type': fieldsv7.selection(_get_method_type_selection,
                                        "Process to perform", type='char', size=64, required=True,)
    }
