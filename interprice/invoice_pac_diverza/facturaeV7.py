# -*- coding: utf-8 -*-
__author__ = 'Jose J. H. Bautista'
__email__ = 'jose.bautista@gvadeto.com'
__copyright__ = 'Copyright 2016, GVadeto'

from openerp.osv import fields, osv


class IrAttachmentFacturaeMx(osv.Model):
  _inherit = 'ir.attachment.facturae.mx'

  def _get_type(self, cr, uid, ids=None, context=None):
    if context is None:
      context = {}
    _types = super(IrAttachmentFacturaeMx, self)._get_type(cr, uid, ids, context=context)
    _types.extend([
      ('cfdi32_pac_sf', 'CFDI 3.2 Diverza'),
    ])
    return _types

  _columns = {
          'type': fields.selection(_get_type, 'Type', type='char', size=64, required=True,
                                   readonly=True, help="Type of Electronic Invoice"),
      }


# class IrSequenceApproval(osv.Model):
#   _inherit = 'ir.sequence.approval'
#
#   def _get_type(self, cr, uid, ids=None, context=None):
#       types = super(ir_sequence_approval, self)._get_type(
#           cr, uid, ids, context=context)
#       types.extend([
#           ('cfdi32_pac_sf', 'CFDI 3.2 Diverza'),
#       ])
#       return types
#
#   _columns = {
#       'type': fields.selection(_get_type, 'Type', type='char', size=64,
#                                required=True, help="Type of Electronic Invoice"),
#   }


