Presupuesto
------------------

v1.0.1
-------------
Se agrego un permiso para la cancelación forzada de los PO,
la cancelacion solo cambia el estatus del PO a "Cancelado"
los documentos relacionados (albaranes, facturas) no los cancela.

Permiso
.. figure:: ../presupuesto/static/img/Usuario_Odoo.png
    :alt: Permiso
    :width: 100%

Pedido cancelado
.. figure:: ../presupuesto/static/img/Pedido_de_compra_Odoo.png
    :alt: Pedido cancelado
    :width: 100%

v1.0
-------------
Bloqueo de creación y edición, permiso de solo lectura en los campos de solicitud de presupuesto (compras)..