# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp.osv import fields, osv
from openerp import api, exceptions,_
from operator import attrgetter

class SolicitudPresupuesto(osv.osv):

    _inherit = "purchase.order"

    def onchange_pricelist(self, cr, uid, ids, pricelist_id, context=None):
        value = super(SolicitudPresupuesto, self).onchange_pricelist(
            cr, uid, ids, pricelist_id, context=context
        )
        if 'value' in value:
            value['value'].update({'order_line': False})
        return value

    def validate_unique_line_by_product(self, vals):
        exists_product = []

        def validate_product():
            if product_id in exists_product:
                raise exceptions.MissingError(
                    _('Not is possible add more of once a product.\n Duplicate %s ') % (product_name)
                )
            else:
                exists_product.append(product_id)

        if isinstance(vals, dict):
            order_line = vals.get('order_line', None)
            if order_line and len(order_line) > 0:
                for line in order_line:
                    if isinstance(line[2], dict) and line[2].get('product_id', None):
                        product_id = line[2].get('product_id', -1)
                        product_name = line[2].get('name', '')
                        validate_product()
                    else:
                        if line[0] not in (2, 3, 5):
                            for oline in self.order_line:
                                if oline.id == line[1]:
                                    product_id = oline.product_id.id
                                    product_name = oline.name
                                    validate_product()
    
    @api.model
    def create(self, vals):
        self.validate_unique_line_by_product(vals)
        return super(SolicitudPresupuesto, self).create(vals=vals)
    
    @api.one
    def write(self, vals):
        self.validate_unique_line_by_product(vals)
        return super(SolicitudPresupuesto, self).write(vals=vals)

    def action_cancel(self, cr, uid, ids, context=None):
        flag = self.pool.get('res.users').has_group(cr, uid, 'presupuesto.purchase_force_cancel')
        if not flag:
            for purchase in self.browse(cr, uid, ids, context=context):
                for pick in purchase.picking_ids:
                    for move in pick.move_lines:
                        if pick.state == 'done':
                            raise osv.except_osv(
                                _('Unable to cancel the purchase order %s.') % (purchase.name),
                                _('You have already received some goods for it.  '))
                self.pool.get('stock.picking').action_cancel(cr, uid, [x.id for x in purchase.picking_ids if x.state != 'cancel'], context=context)
                for inv in purchase.invoice_ids:
                    if inv and inv.state not in ('cancel', 'draft'):
                        raise osv.except_osv(
                            _('Unable to cancel this purchase order.'),
                            _('You must first cancel all invoices related to this purchase order.'))
                self.pool.get('account.invoice') \
                    .signal_workflow(cr, uid, map(attrgetter('id'), purchase.invoice_ids), 'invoice_cancel')
        else:
            user_name = self.pool.get('res.users').browse(cr, uid, uid, context=context).name
            self.write(cr, uid, ids, {'observation': _('Forced canceled by {}'.format(user_name))}, context=context)
        self.signal_workflow(cr, uid, ids, 'purchase_cancel')
        return True


class PurchaseOrderLine(osv.osv):

    _inherit = "purchase.order.line"

    _columns = {
        'product_uom': fields.many2one(
            'product.uom', 'Product Unit of Measure',
            domain=[('id', '=', [])], required=True
        ),
    }

    def onchange_product_id(
            self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False,
            date_planned=False, name=False, price_unit=False,
            state='draft', context=None):
        res = super(PurchaseOrderLine, self).onchange_product_id(
            cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order, fiscal_position_id, date_planned,
            name, price_unit, state, context)
        if res:
            res.update({
                'domain': {'product_uom': [
                    ('id', 'in', [res['value']['product_uom']])
                ]}
            })
        return res
