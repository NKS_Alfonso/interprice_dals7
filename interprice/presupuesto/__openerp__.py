# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Presupuesto',
    'version': '1.0',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'category': 'Compras',

    'website': 'http://www.grupovadeto.com',
    'license': 'AGPL-3',
    'depends': ['purchase', 'series', 'account_payment_purchase'],
    'data': [
        'data/purchase_report_paperformat.xml',
    ],
    'init_xml': [],
    'demo_xml': [],
    'update_xml': [
                'view/presupuesto_view.xml',
                 ],
    'installable': True,
    'active': False,
}
