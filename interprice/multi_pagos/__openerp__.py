# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation
{
    'name': "Multi pagos",

    'summary': """
        Actualización para conciliación a selección.
        """,

    'description': """
        Funcionalidad incluida en este módulo:\n

        \t[*] Consolidación del uso del tipo de cambio alterno.\n
        \t[*] Desglose de IVA cuando es necesario.\n
        \t[*] Cálculo de pérdida o ganancia cambiaria.\n
        \t[*] Conciliación de movimientos a selección.
    """,

    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Contabilidad',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/multi_pagos_view.xml'
    ]
}
