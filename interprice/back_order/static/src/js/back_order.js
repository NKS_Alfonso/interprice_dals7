openerp.back_order = function(instance, local) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    local.AddBackOrder = instance.web.form.AbstractField.extend({
        events: {
            "click .back_order_assign": "back_order_assign",
        },
        init: function() {
            this._super.apply(this, arguments);
        },
        start: function() {
            this.display_field();
            return this._super();
        },
        display_field: function() {
            var self = this;
            var value = this.get("value");
            var info = JSON.parse(value);
            if (!info) {
                this.$el.html('');
                return;
            }
            this.$el.html(QWeb.render("ShowBackOrderInfo", {
            content: info.content,
            purchase: info.purchase_id,
            title: info.title
            }));
        },
        back_order_assign: function(event) {
            var self = this;
            var bo_id = $(event.currentTarget).data('bo_id') || false;
            var po_id = JSON.parse(this.get("value")).purchase_id;
            var model = new instance.web.Model("purchase.order");
            model.call('assign_back_order', [po_id, bo_id]).then(function () {
                            location.reload(true)
            })
        },
    });

    instance.web.form.widgets.add('add_back_order', 'instance.back_order.AddBackOrder');

    local.ShowBackOrder = instance.web.form.AbstractField.extend({
        events: {
            "click .back_order_show": "back_order_show",
        },
        init: function() {
            this._super.apply(this, arguments);
        },
        start: function() {
            this.display_field();
            return this._super();
        },
        display_field: function() {
            this.$el.html(QWeb.render("ActionShowBackOrder", {}));
        },
        back_order_show: function(event) {
            location.reload(true)
        },
    });

    instance.web.form.widgets.add('show_back_order', 'instance.back_order.ShowBackOrder');

}
