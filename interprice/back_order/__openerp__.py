# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': 'Back Order',
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Sales',
    'summary': 'Manage back orders transactions',
    'depends': ['base', 'sale', 'purchase', 'stock', 'cotizaciones',
                'series', 'secuencias', 'presupuesto', 'piramide_de_pago','clients_administration'],
    'application': True,
    'data': [
        'security/security.xml',
        'views/back_order_view.xml',
        'views/back_order_sequence.xml',
        'views/purchase_view.xml',
        'wizard/views/existing_products_view.xml'
    ],
    'qweb': ['static/src/xml/*.xml'],
}
