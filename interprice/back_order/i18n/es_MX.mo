��    B      ,  Y   <      �     �  L  �  L    R   T
  T   �
  U   �
  Q   R  ?   �     �            
        *     8  
   M  
   X  &   c  ,   �  $   �  %   �  G        J     ^     o     �     �     �  
   �     �     �     �     �     �          "  r   (     �     �     �     �     �     �     �          "     +     >  	   Q  
   [     f     r     �     �     �  �   �  *   �     �  S   �  >   .  G   m     �  l   �  /   =  *   m  R   �    �       \  !  `  ~  Y   �  S   9  X   �  S   �  L   :     �     �     �     �     �     �  
   �  
   �  &   	  /   0  '   `  &   �  <   �     �               (     A     _  
   t       	   �     �     �     �     �     �  y   �     j     y     �     �     �     �     �     �               +  
   @     K     \     m     �  '   �  )   �  �   �  6   �     �  W      =   g   P   �      �   l   !  /   ~!  *   �!  R   �!         +                	       5           $         @         (   B           =       .                 ,   '   1   7             ;                       %                     &      "   >       A                    /   -   <   )          8   !   ?   6      4         2           :   *         3          #   
   0       9    : Not available products <p class="oe_view_nocontent_create">
                Click to create a Back Order, the first step of a new sale.
              </p><p>
                Odoo will help you handle efficiently the complete sale flow:
                from the quotation to the sales order, the
                delivery, the invoicing and the payment collection.
              </p><p>
                The social feature helps you organize discussions on each sales
                order, and allow your customers to keep track of the evolution
                of the sales order.
              </p>
             <p class="oe_view_nocontent_create">
                Click to create a Sale Order, the first step of a new sale.
              </p><p>
                Odoo will help you handle efficiently the complete sale flow:
                from the quotation to the sales order, the
                delivery, the invoicing and the payment collection.
              </p><p>
                The social feature helps you organize discussions on each sales
                order, and allow your customers to keep track of the evolution
                of the sales order.
              </p>
             <p><strong>All products were already added to a purchase order.</strong></p> <br/> <p><strong>One or more products do not have an assigned supplier.</strong></p> <br/> <p><strong>The following products are already in a purchase order.</strong></p> <br/> <p><strong>The following products are already in a sale order.</strong></p> <br/> <p><strong>The next products was deleted of the purchase order  <p><strong>The purchase order Add Assign to purchase order Associated Authorization Authorize Back Order Authorized Back Order Back Order <strong>Authorized</strong> Back Order <strong>Partial Referral</strong> Back Order <strong>Referral</strong> Back Order <strong>Requested</strong> Back Order <strong>Some sales of this order have been canceled</strong> Back Order Canceled Back Order Count Back Order Number Back Order Related Back Order associated state Back Order state Back order Cancel Canceled Cantidad Create Purchase Order Create Sale Order Creation of document Draft Error adding products.

 All the products were already added to a purchase order or they were processor in a sale. Not Referral Not exists back orders Partial Referral Partial associate Pending Back orders Producto Purchase Order Purchase Order Line Referral Related Sale Order Request Back Order Requested Sale Order Sales Order Sales Order Line Show Back Order and products Sorry! this partner its blocked Sorry, this customer is blocked There are products from this supplier assigned in some Back Orders. If you want to visualize,
            after saving click on this legend. If you can see the products, skip the message. There is not enough stock for any product. Total Tax Included You can not delete a back order in 'authorized', 'associated' or 'referral' status. You to communicate with an administrator of this configuration was canceled or deleted affecting the next products.</strong></p> <br/> {'back_order': back_order} {'invisible': ['|', ('back_order', '=', True),
                        ('state','not in',('draft','sent'))]} {'invisible': ['|', ('back_order', '=', True)]} {'invisible': [('back_order', '=', True)]} {'readonly': [
                        ('state_back_order', 'not in', ('draft'))]} Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-10 19:07-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 2.0.6
 : Productos no disponibles <p class="oe_view_nocontent_create">
                Haga clic para crear una Back Order, el primer paso de una nueva venta.
              </p><p>
                Odoo le ayuda a gestionar eficientemente el flujo completo de ventas:
                desde el presupuesto al pedido de venta, el envío, la
                facturación y el pago.
              </p><p>
                Las características sociales le ayudan a organizar discusiones
                en cada pedido de venta, y permiten a su cliente seguir la
                evolución de los pedidos de venta.
              </p>
             <p class="oe_view_nocontent_create">
                Haga clic para crear una Orden de venta, el primer paso de una nueva venta.
              </p><p>
                Odoo le ayuda a gestionar eficientemente el flujo completo de ventas:
                desde el presupuesto al pedido de venta, el envío, la
                facturación y el pago.
              </p><p>
                Las características sociales le ayudan a organizar discusiones
                en cada pedido de venta, y permiten a su cliente seguir la
                evolución de los pedidos de venta.
              </p>
             <p><strong>Todos los productos fueron agregados a una orden de compra.</strong></p> <br/> <p><strong>Uno o más productos no tienen un proveedor asignado.</strong></p> <br/> <p><strong>Los siguientes productos ya están en una orden de compra.</strong></p> <br/> <p><strong>Los siguientes productos ya están en orden de venta.</strong></p> <br/> <p><strong>Los siguientes productos fueron eliminados de la orden de compra  <p><strong>La orden de compra Agregar Agregar a orden de compra Asociado Autorización Autorizar Back Order Autorizado Back Order Back Order <strong>Autorizado</strong> Back Order <strong>Remisionado parcial</strong> Back Order <strong>Remisionado</strong> Back Order <strong>Solicitado</strong> Back Order <strong>Se han cancelado algunas ventas.</strong> Back Order Cancelada Back Order Count Número de Back Order Back orders relacionadas Estado de Back Order asociado Estado de Back Order Back order Cancelar Cancelado Cantidad Crear pedido de compra Crear orden de venta Creación de documento (Ventas) Borrador Error al agregar productos.

 Todos los productos ya se agregaron a una orden de compra o fueron procesador en una venta. No Remisionado No existen pedidos pendientes Remisionado parcial Asociado parcial Back orders pendientes Producto Orden de Compra Línea pedido de compra Remisionado Orden de venta relacionada Solicitar Back Order Solicitado Pedidos de Venta Pedidos de Venta Línea pedido de venta Mostrar Back orders y productos ¡Lo siento! este socio está bloqueado Lo sentimos, este cliente está bloqueado Hay productos de este proveedor asignados en algunas Back Orders. Si quieres visualizarlos,
             después de guardar haga clic en esta leyenda. Si puede ver los productos, omita el mensaje. No hay suficiente stock para los productos pendientes. Total (impuestos incluidos) No puede eliminar un Back Order con el estado 'autorizado', 'asociado' o 'remisionado'. Usted debe comunicarse con un administrador de configuración fue cancelada o eliminada afectando los siguientes productos.</strong></p> <br/> {'back_order': back_order} {'invisible': ['|', ('back_order', '=', True),
                        ('state','not in',('draft','sent'))]} {'invisible': ['|', ('back_order', '=', True)]} {'invisible': [('back_order', '=', True)]} {'readonly': [
                        ('state_back_order', 'not in', ('draft'))]} 