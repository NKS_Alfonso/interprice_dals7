# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, fields, api


class SaleOrderLine(models.Model):

    _inherit = 'sale.order.line'

    @api.one
    def _get_created_purchase_order(self):
        if self.purchase_order_id:
            self.created_purchase_order = True
        else:
            self.created_purchase_order = False

    @api.one
    def _get_created_sale_order(self):
        if self.product_uom_qty == self.qty_remitted:
            self.created_sale_order = True
        elif self.product_uom_qty > self.qty_remitted:
            self.created_sale_order = False

    purchase_order_id = fields.Many2one(
        comodel_name='purchase.order',
        string='Back Order',
        copy=False
    )
    created_purchase_order = fields.Boolean(
        string='Created PO',
        compute='_get_created_purchase_order',
        readonly=True,
        store=False,
        default=False
    )
    sale_order_list = fields.Char(
        string='Sale Order List',
        copy=False
    )
    full_sale_order = fields.Boolean(
        string='Created Sale Order',
        default=False,
        copy=False
    )
    created_sale_order = fields.Boolean(
        string='Created SO',
        compute='_get_created_sale_order',
        readonly=True,
        store=False,
        default=False
    )
    qty_remitted = fields.Float(
        string='Quantity remitted',
        copy=False
    )

    def product_id_change_with_wh(self, cr, uid, ids, pricelist, product, qty=0,
                                  uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                                  lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False,
                                  flag=False, warehouse_id=False, context=None):
        context = context or {}
        product_uom_obj = self.pool.get('product.uom')
        product_obj = self.pool.get('product.product')
        warning = {}
        warning_msgs = ''
        error = ''
        # UoM False due to hack which makes sure uom changes price, ... in product_id_change
        res = self.product_id_change(cr, uid, ids, pricelist, product, qty=qty,
                                     uom=False, qty_uos=qty_uos, uos=uos, name=name, partner_id=partner_id,
                                     lang=lang, update_tax=update_tax, date_order=date_order, packaging=packaging,
                                     fiscal_position=fiscal_position, flag=flag, context=context)
        if not product:
            res['value'].update({'product_packaging': False})
            return res

        # set product uom in context to get virtual stock in current uom
        if not context.get('back_order'):
            if 'product_uom' in res.get('value', {}):
                # use the uom changed by super call
                context = dict(context, uom=res['value']['product_uom'])
            elif uom:
                # fallback on selected
                context = dict(context, uom=uom)

            # update of result obtained in super function
            product_obj = product_obj.browse(cr, uid, product, context=context)
            res['value'].update(
                {'product_tmpl_id': product_obj.product_tmpl_id.id, 'delay': (product_obj.sale_delay or 0.0)})

            # Calling product_packaging_change function after updating UoM
            res_packing = self.product_packaging_change(cr, uid, ids, pricelist, product, qty, uom, partner_id, packaging,
                                                        context=context)
            res['value'].update(res_packing.get('value', {}))
            warning_msgs = res_packing.get('warning') and res_packing['warning']['message'] or ''
            error = ('¡Error de configuración!')

            if product_obj.type == 'product':
                # determine if the product needs further check for stock availibility
                is_available = self._check_routing(cr, uid, ids, product_obj, warehouse_id, context=context)
                uom_record = False
                if uom:
                    uom_record = product_uom_obj.browse(cr, uid, uom, context=context)
                    if product_obj.uom_id.category_id.id != uom_record.category_id.id:
                        uom_record = False
                if not uom_record:
                    uom_record = product_obj.uom_id
                # <Update TODO: Product fix>
                if not warehouse_id:
                    error = ('No ha seleccionado ningún almacén')
                    warning_msgs += ('Seleccione un almacén')
                if warehouse_id and product:
                    cr.execute("""SELECT SUM(sq.qty)
                                FROM stock_warehouse AS sw INNER JOIN
                                stock_location AS sl ON sw.lot_stock_id = sl.id
                                INNER JOIN stock_quant sq ON sl.id=sq.location_id
                                AND reservation_id is null
                                WHERE sl.usage='internal' and sw.id=%s
                                and product_id=%s""", (warehouse_id, product))
                    if cr.rowcount:
                        quants = cr.fetchone()[0]
                        if quants is None or quants is False:
                            quants = 0.0
                        if quants < qty:
                            disponible = quants
                            if not is_available:
                                res['value'].update({'product_uom_qty': quants, })
                                faltante = qty - disponible
                                warn_msg = (
                                               '¡Prevé vender %.2f %s pero sólo %.2f %s están disponibles!\nLa(s) %.2f %s faltante(s) únicamente podrá venderla(s) cuando se encuntre(n)\nen stock,'
                                               ' para esto cree una Back Order en el menú Ventas/Back Order.') % \
                                           (qty, uom_record.name,
                                            max(0, disponible), uom_record.name,
                                            max(0, faltante), uom_record.name)
                                warning_msgs += ('El stock real es de: %.2f %s.\n\n') % \
                                                (max(0, product_obj.qty_available), uom_record.name) + warn_msg + "\n\n"
                                error = ('¡Producto sobre existencias de almacén!')
                            else:
                                res['value'].update({'product_uom_qty': qty, })
                                faltante = qty - disponible
                                warn_msg = (
                                               '¡Prevé vender %.2f %s pero sólo %.2f %s están disponibles!\nLa(s) %.2f %s faltante(s) únicamente podrá venderla(s) bajo pedido.') % \
                                           (qty, uom_record.name,
                                            max(0, disponible), uom_record.name,
                                            max(0, faltante), uom_record.name)
                                warning_msgs += ('El stock real es de: %.2f %s.\n\n') % \
                                                (max(0, product_obj.qty_available), uom_record.name) + warn_msg + "\n\n"
                                error = ('¡Producto sobre pedido!')
                        else:
                            if quants > 0 and qty < quants:
                                pass
                            elif (quants > quants):
                                res['value'].update({'product_uom_qty': quants, })
                            elif (qty == 0.0 or qty == False or qty == None):
                                res['value'].update({'product_uom_qty': quants, })
                # <EndUpdate TODO>

        if warning_msgs:
            warning = {
                'title': error,  # ('¡Error de configuración!'),
                'message': warning_msgs
            }
        res.update({'warning': warning})
        return res
