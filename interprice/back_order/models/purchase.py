# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports

from openerp import models, api, fields, _
import json


class PurchaseOrder(models.Model):

    _inherit = "purchase.order"

    @api.one
    def _get_pending_back_orders_json(self):
        self.info_pending_back_orders = json.dumps(False)
        if self.state == 'draft' and self.id:
            partner_id = self.nacional_partner_id.id or self.internacional_partner_id.id or self.partner_id
            if partner_id:
                bo_ids = self.get_domain_back_orders(partner_id)
                back_orders = self.env['sale.order'].browse(bo_ids)
                info = {'content': [], 'title': '', 'purchase_id': 0}
                for bo in back_orders:
                    info['content'].append({
                        'name': bo.name,
                        'bo_id': bo.id,
                        'products': self._get_info_products_back_order([bo.id])
                    })
                info['title'] = _("Pending Back orders")
                info['purchase_id'] = self.id
                if info.get('content', False):
                    self.info_pending_back_orders = json.dumps(info)
                    self.show_back_orders = True
                    print self.info_pending_back_orders
                    print self.show_back_orders

    @api.one
    @api.depends('pricelist_id')
    def _get_info_to_back_order(self):
        supplier_id = (self.nacional_partner_id.id or
                       self.internacional_partner_id.id)
        if supplier_id:
            self._cr.execute(
                """
                    SELECT SOL.name, SUM(SOL.product_uom_qty) FROM sale_order AS SO
                    INNER JOIN sale_order_line AS SOL ON SOL.order_id = SO.id
                    INNER JOIN product_product AS PP ON PP.id = SOL.product_id
                    INNER JOIN product_template AS PT ON PT.id = PP.product_tmpl_id
                    INNER JOIN product_supplierinfo AS PSI ON PSI.product_tmpl_id = PT.id
                    WHERE PSI.name in (%s) AND PSI.sequence = 1 AND SO.state_back_order IN 
                    ('authorized','partial_associate') AND SO.state_back_order_referral IN 
                    ('partial_referral', 'not_referral')
                    AND SOL.purchase_order_id IS Null AND SOL.full_sale_order = False
                    GROUP BY SOL.name, SOL.product_uom_qty
                """, (supplier_id,)
            )
            if self._cr.rowcount:
                order_lines = self._cr.fetchall()
                if order_lines:
                    self.show_back_orders = True

    @api.multi
    def _get_info_products_back_order(self, back_order):
        supplier_id = (self.nacional_partner_id.id or
                       self.internacional_partner_id.id)
        if supplier_id:
            products = []
            self._cr.execute(
                """
                    SELECT SOL.name, SOL.product_uom_qty, SOL.qty_remitted FROM sale_order AS SO
                    INNER JOIN sale_order_line AS SOL ON SOL.order_id = SO.id
                    INNER JOIN product_product AS PP ON PP.id = SOL.product_id
                    INNER JOIN product_template AS PT ON PT.id = PP.product_tmpl_id
                    INNER JOIN product_supplierinfo AS PSI ON PSI.product_tmpl_id = PT.id
                    WHERE PSI.name in (%s) AND PSI.sequence = 1 AND SO.state_back_order IN 
                    ('authorized','partial_associate') AND SO.state_back_order_referral IN 
                    ('partial_referral', 'not_referral')
                    AND SO.id in %s AND SOL.purchase_order_id IS Null AND SOL.full_sale_order = False
                    GROUP BY SOL.name, SOL.product_uom_qty, SOL.qty_remitted
                """, (supplier_id, tuple(back_order))
            )
            if self._cr.rowcount:
                order_lines = self._cr.fetchall()
                for (product, quantity, qty_remitted) in order_lines:
                    products.append({
                        'product_name': product,
                        'product_qty': quantity - qty_remitted
                    })
            return products

    @api.one
    def _rebuild_back_order_field(self):
        back_order_ids = self.order_line.mapped('back_order')
        self.back_orders_related = ', '.join(back_order_ids.mapped('name'))

    back_orders_related = fields.Char(
        string=_("Back Order Related"),
        compute="_rebuild_back_order_field"
    )
    info_pending_back_orders = fields.Text(
        compute="_get_pending_back_orders_json"
    )
    show_back_orders = fields.Boolean(
        compute="_get_info_to_back_order"
    )

    @api.model
    def wkf_action_cancel(self):
        back_order_product = {}
        back_order_obj = self.env['sale.order']
        back_order_line_obj = self.env['sale.order.line']
        back_order_line = back_order_line_obj.search([('purchase_order_id', '=', self.id)])
        for order_line in back_order_line:
            order_line.purchase_order_id = False
            back_id = str(order_line.order_id.id)
            if back_id in back_order_product:
                products = back_order_product.get(back_id, [])
                products.append(order_line.name)
                back_order_product.update({back_id: products})
            else:
                products = back_order_product.get(back_id, [])
                products.append(order_line.name)
                back_order_product.update({back_id: products})
        for key in back_order_product.keys():
            products = back_order_product.get(key, [])
            message = _('<p><strong>The purchase order') + ' ' + self.name + ' ' + \
                      _('was canceled or deleted affecting the next products.</strong></p> <br/>')
            for product in products:
                message += '<p>' + product + '.<p>'
            back_order_obj.browse(int(key)).message_post(
                body=message
            )

            sale_lines_complete = back_order_line_obj.search([
                ('order_id', '=', int(key)),
                ('is_delivery', '=', False),
                ('purchase_order_id', '=', False),
                ('full_sale_order', '=', False)])
            if sale_lines_complete:
                back_order_obj.browse(int(key)).state_back_order = 'partial_associated'
            sale_lines_total = back_order_line_obj.search([
                ('order_id', '=', int(key)),
                ('is_delivery', '=', False)])
            if len(sale_lines_complete) == len(sale_lines_total):
                back_order_obj.browse(int(key)).state_back_order = 'authorized'

        return super(PurchaseOrder, self).wkf_action_cancel()

    @api.multi
    def assign_back_order(self, back_order_js=False):
        back_order_ids = [back_order_js]
        if not back_order_ids:
            back_order_ids = [x.id for x in self.back_order_ids]
        if back_order_ids:
            supplier_id = self.nacional_partner_id.id or self.internacional_partner_id.id
            company = self.env['res.users'].browse(self._uid).company_id
            nacional_internacional = self.nacional_internacional

            if supplier_id:
                self._cr.execute(
                    """
                    SELECT SOL.id, SO.id FROM sale_order AS SO
                    INNER JOIN sale_order_line AS SOL ON SOL.order_id = SO.id
                    INNER JOIN product_product AS PP ON PP.id = SOL.product_id
                    INNER JOIN product_template AS PT ON PT.id = PP.product_tmpl_id
                    INNER JOIN product_supplierinfo AS PSI ON PSI.product_tmpl_id = PT.id
                    WHERE PSI.name in (%s) AND PSI.sequence = 1 AND SO.state_back_order IN 
                    ('authorized','partial_associate') AND SO.state_back_order_referral IN 
                    ('partial_referral', 'not_referral')
                    AND SO.id in %s AND SOL.purchase_order_id IS Null AND SOL.full_sale_order = False
                    """, (supplier_id, tuple(back_order_ids))
                )
                if self._cr.rowcount:
                    back_orders = self._cr.fetchall()
                    sale_order_line = self.env['sale.order.line']
                    pol_objects_list = []
                    pol_objects = {}
                    for purchase_line in self.order_line:
                        product_id = str(purchase_line.product_id.id)
                        pol_objects.update({product_id: purchase_line})
                        pol_objects_list.append(purchase_line.id)
                    # Test
                    back_order_active_obj = {}
                    # End Test
                    for (back_order_id, order_id) in back_orders:
                        sale_line = sale_order_line.browse(back_order_id)
                        taxes_id = sale_line.product_id.product_tmpl_id.supplier_taxes_id
                        sale_line.purchase_order_id = self.id

                        sol_product_id = sale_line.product_id.id

                        if str(sol_product_id) in pol_objects:
                            purchase_line_obj = pol_objects.get(str(sol_product_id))
                            purchase_line_obj.product_qty += sale_line.product_uom_qty - sale_line.qty_remitted
                            bo_ids = [bo.id for bo in purchase_line_obj.back_order] or []
                            # Placing the lines in PO
                            bo_ids.append(order_id)
                            purchase_line_obj.back_order = bo_ids
                            # Placing the names of back orders
                            back_order_ids = self.order_line.mapped('back_order')
                            self.back_orders_related = ', '.join(back_order_ids.mapped('name'))
                        else:
                            value_line = {
                                'product_uom': sale_line.product_id.product_tmpl_id.uom_id.id,
                                'price_unit': 0.0,
                                'product_qty': sale_line.product_uom_qty - sale_line.qty_remitted,
                                'partner_id': supplier_id,
                                'invoiced': False,
                                'company_id': company.id,
                                'state': 'draft',
                                'name': sale_line.name,
                                'taxes_id': [x.id for x in taxes_id],
                                'product_id': sale_line.product_id.id,
                                'date_planned': fields.Date.today(),
                                'nacional_internacional': nacional_internacional,
                                'back_order': [[6, False, [order_id]]]
                            }
                            pol_objects_list.append(value_line)

                        # Test
                        if order_id in back_order_active_obj:
                            back_order_active = back_order_active_obj.get(order_id, [])
                            back_order_active.append(sale_line.name)
                            back_order_active_obj.update({order_id: back_order_active})
                        else:
                            back_order_active_obj.update({
                                order_id: [sale_line.name]
                            })
                    self.message(back_order_active_obj)
                    # End test

                    self.order_line = pol_objects_list
        self.back_order_ids = False
        return {}

    @api.multi
    @api.onchange('nacional_partner_id')
    def onchage_nacional_partner(self, nacional_partner_id):
        value = super(PurchaseOrder, self).onchage_nacional_partner(nacional_partner_id)
        if 'value' not in value:
            value.update({'value': {'order_line': []}})
        else:
            value['value'].update({'order_line': []})
        return value

    @api.multi
    @api.onchange('internacional_partner_id')
    def onchage_internacional_partner(self, internacional_partner_id):
        value = super(PurchaseOrder, self).onchage_nacional_partner(internacional_partner_id)
        if 'value' not in value:
            value.update({'value': {'order_line': []}})
        else:
            value['value'].update({'order_line': []})
        return value

    def get_domain_back_orders(self, partner_id):
        list_back_order = []
        if partner_id:
            self._cr.execute(
                """
                    SELECT SO.id FROM sale_order AS SO
                    INNER JOIN sale_order_line AS SOL ON SOL.order_id = SO.id
                    INNER JOIN product_product AS PP ON PP.id = SOL.product_id
                    INNER JOIN product_template AS PT ON PT.id = PP.product_tmpl_id
                    INNER JOIN product_supplierinfo AS PSI ON PSI.product_tmpl_id = PT.id
                    WHERE PSI.name in (%s) AND PSI.sequence = 1 AND SO.state_back_order IN
                    ('authorized','partial_associate') AND SO.state_back_order_referral IN 
                    ('partial_referral', 'not_referral')
                    AND SOL.purchase_order_id IS Null AND SOL.full_sale_order = False
                """, (partner_id,)
            )
            if self._cr.rowcount:
                back_orders = self._cr.fetchall()
                for (back_order_id,) in back_orders:
                    if back_order_id not in list_back_order:
                        list_back_order.append(back_order_id)
        return list_back_order

    @api.multi
    @api.onchange('nacional_internacional')
    def onchage_nacional_internacional(self, nacional_internacional, order_line):
        value = super(PurchaseOrder, self).onchage_nacional_internacional(
            nacional_internacional, order_line)
        if 'value' in value:
            value['value'].update({'internacional_partner_id': False,
                                   'nacional_partner_id': False,
                                   'info_to_bo': False,
                                   'back_order_ids': False,
                                   'order_line': False,
                                   'pricelist_id': False,
                                   'currency_id': False})
        return value

    @api.one
    def message(self, back_order_active_obj):
        back_order_obj = self.env['sale.order']
        for key in back_order_active_obj.keys():
            message = _('<p><strong>The following products are already in a purchase order.</strong></p> <br/>')
            for product_name in back_order_active_obj.get(key):
                message += '<p>' + product_name + '</p>'
            self.env['sale.order'].browse(key).message_post(body=_(message))
            back_order_obj.browse(int(key)).state_back_order = 'associated'
