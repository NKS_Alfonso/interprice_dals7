# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports

from openerp import models, api, _


class SaleOrder(models.Model):

    _inherit = 'sale.order'

    @api.one
    def action_cancel(self):
        back_order_line_obj = self.env['sale.order.line']
        if self.back_order_related_id:
            back_order = self.browse(self.back_order_related_id)
            back_order.count_sale_order -= 1
            lines_ids = [x.id for x in self.order_line.mapped('product_id')]

            back_order_lines = back_order_line_obj.search([
                ('order_id', '=', back_order.id),
                ('is_delivery', '=', False),
                ('product_id', 'in', lines_ids)])
            for bo_line in back_order_lines:
                sale_line = self.order_line.search([('order_id', '=', self.id), ('product_id', '=', bo_line.product_id.id)])
                bo_line.qty_remitted -= sale_line.product_uom_qty
                bo_line.full_sale_order = False

            validate_all = back_order.order_line.search([
                ('order_id', '=', back_order.id),
                ('is_delivery', '=', False)])
            validate_full = back_order.order_line.search([
                ('order_id', '=', back_order.id),
                ('is_delivery', '=', False),
                ('full_sale_order', '=', False)])
            if validate_full:
                back_order.state_back_order_referral = 'partial_referral'
            if len(validate_all) == len(validate_full):
                validate = False
                for line in validate_full:
                    if line.qty_remitted > 0.00:
                        validate = True
                        break
                if not validate:
                    back_order.state_back_order_referral = 'not_referral'
            back_order.message_post(body=_("Back Order <strong>Some sales of this order have been canceled</strong>"))
            self.back_order_related_id = False
        return super(SaleOrder, self).action_cancel()
