# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports

from openerp import models, api, fields, _


class PurchaseOrderLine(models.Model):

    _inherit = "purchase.order.line"

    back_order = fields.Many2many(
        comodel_name="sale.order",
        relation='po_line_back_order_rel',
        column1='po_line_id',
        column2='back_order_id',
        string="Back Order",
        copy=False
    )

    @api.one
    def unlink(self):
        if self.back_order:
            back_order_line_obj = self.env['sale.order.line']
            for bo in self. back_order:
                message = _('<p><strong>The next products was deleted '
                            'of the purchase order ') + ' ' + self.order_id.name + '.</strong></p> <br/>'
                message += '<p>' + self.name + '.<p>'
                bo.message_post(body=message)
                back_order_line = back_order_line_obj.search([
                    ('purchase_order_id', '=', self.order_id.id),
                    ('product_id', '=', self.product_id.id),
                    ('order_id', '=', bo.id)])
                for order_line in back_order_line:
                    order_line.purchase_order_id = False

                sale_lines_complete = back_order_line_obj.search([
                    ('order_id', '=', bo.id),
                    ('is_delivery', '=', False),
                    ('purchase_order_id', '=', False),
                    ('full_sale_order', '=', False)])
                if sale_lines_complete:
                    bo.state_back_order = 'partial_associated'
                sale_lines_total = back_order_line_obj.search([
                    ('order_id', '=', bo.id),
                    ('is_delivery', '=', False)])
                if len(sale_lines_complete) == len(sale_lines_total):
                    bo.state_back_order = 'authorized'
        return super(PurchaseOrderLine, self).unlink()
