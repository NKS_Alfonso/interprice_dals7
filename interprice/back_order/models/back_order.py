# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class SaleOrder(models.Model):

    _inherit = 'sale.order'

    _selection_list = [
        ('draft', _('Draft')),
        ('requested', _('Requested')),
        ('authorized', _('Authorized')),
        ('partial_associated', _('Partial associate')),
        ('associated', _('Associated')),
        ('cancel', _('Canceled'))
    ]

    _selection_list_referral = [
        ('not_referral', _('Not Referral')),
        ('partial_referral', _('Partial Referral')),
        ('referral', _('Referral'))
    ]

    def _get_default(self):
        back_order = self._context.get('back_order', False)
        if back_order:
            return 'draft'
        else:
            return ''

    @api.depends('order_line.product_id')
    def _compute_alert_info(self):
        pass

    back_order = fields.Boolean(
        string="Back Order"
    )
    back_order_related_id = fields.Integer(
        string="Related Sale Order",
        copy=False
    )
    count_sale_order = fields.Integer(
        string="Back Order Count",
        copy=False
    )
    state_back_order = fields.Selection(
        selection=_selection_list,
        default=_get_default,
        string="Back Order state",
        copy=False
    )
    state_back_order_referral = fields.Selection(
        selection=_selection_list_referral,
        string="Back Order associated state",
        default="not_referral",
        copy=False
    )
    alert_info = fields.Html(
        string="Create Back Order",
        compute="_compute_alert_info"
    )

    @api.model
    def create(self, vals):
        new_id = super(SaleOrder, self).create(vals)
        if vals['back_order']:
            new_id.name = self.env['ir.sequence'].get('back.order') or '/'
        if self._context.get('default_back_order_related_id', False):
            count = self.browse(self._context.get('active_id')).count_sale_order
            self.browse(self._context.get('active_id')).count_sale_order = count + 1
        return new_id

    @api.one
    def back_order_requested_button(self):
        self.state_back_order = 'requested'
        self.message_post(body=_("Back Order <strong>Requested</strong>"))
        return True

    @api.one
    def unlink(self):
        if self.back_order:
            if self.state_back_order in ('authorized', 'associated', 'referral'):
                raise Warning(
                    _("You can not delete a back order in 'authorized', 'associated' or 'referral' status.")
                )
        if self.back_order_related_id:
            back_order = self.browse(self.back_order_related_id)
            back_order.count_sale_order -= 1
        return super(SaleOrder, self).unlink()

    @api.one
    def back_order_authorize(self):
        self.state_back_order = 'authorized'
        self.message_post(body=_("Back Order <strong>Authorized</strong>"))
        # Create Cron to Back Order
        cron = self.env['ir.cron'].sudo().search([
            '|', ('active', '=', True), ('active', '=', False),
            ('model', '=', 'sale.order'), ('function', '=', 'cron_back_order_to_sale_order')])
        if cron:
            pass
        else:
            cron_data = {
                'name': 'Reserve and create SO to BO',
                'interval_number': 1,
                'interval_type': 'minutes',
                'numbercall': -1,
                'nextcall': fields.Datetime.now(),
                'model': self._name,
                'function': 'cron_back_order_to_sale_order',
                'priority': 5,
                'user_id': 1,
                'active': False
            }
            self.env['ir.cron'].sudo().create(cron_data)
        return True

    @api.multi
    def back_order_to_sale_order(self):
        self = self.with_context(default_back_order=self.id)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'existing_products_wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': self.env.context,
        }

    @api.one
    def back_order_to_sale_order_old(self):
        products_available_to_back_order = {}
        products_available_to_back_order.update({str(self.id): []})
        if self.partner_id.status_client:
            raise Warning(
                _("Sorry, this customer is blocked"),
                _("You to communicate with an administrator of this configuration")
            )
        for back_order_line in self.order_line:
            if not back_order_line.created_sale_order:
                product = back_order_line.product_id.product_tmpl_id
                if product.type not in ('service',):
                    product_id = back_order_line.product_id.id
                    product_qty = back_order_line.product_uom_qty
                    warehouse_id = self.warehouse_id.id
                    self._cr.execute("""
                            SELECT COALESCE(SUM(sq.qty), 0.0)
                            FROM stock_warehouse AS sw
                            INNER JOIN stock_location AS sl ON sw.lot_stock_id = sl.id
                            INNER JOIN stock_quant sq ON sl.id=sq.location_id AND reservation_id IS NULL
                            WHERE sl.usage='internal' AND sw.id=%s
                            AND product_id=%s""", (warehouse_id, product_id))
                    if self._cr.rowcount:
                        (quants,) = self._cr.fetchone()
                        if product_qty < quants:
                            lines = products_available_to_back_order.get(str(self.id), [])
                            lines.append(back_order_line)
                            products_available_to_back_order.update({str(self.id): lines})
        available_lines = products_available_to_back_order.get(str(self.id))
        if available_lines:
            self.back_order = False
            sale_order_copy = self.copy()
            sale_order_copy.back_order_related_id = 0
            for line in sale_order_copy.order_line:
                if not line.is_delivery:
                    line.unlink()
            products_to_sale = []
            for line in available_lines:
                new_line = line.copy()
                new_line.order_id = sale_order_copy.id
                line.full_sale_order = sale_order_copy.id
                products_to_sale.append(line.name)
            try:
                check_stock = sale_order_copy.check_stock()
                if check_stock:
                    res = True
                    if res:
                        message = ""
                        sale_order_copy.name = self.env['ir.sequence'].get('sale.order') or '/'
                        self.back_order = True
                        sale_order_copy.back_order_related_id = self.id
                        sale_order_copy.button_dummy()
                        sale_order_copy.action_button_confirm()
                        self.count_sale_order += 1
                        message += _('<p><strong>The following products are already in a sale order.</strong></p> <br/>')
                        for product in products_to_sale:
                            message += '<p>' + product + '</p>'
                        self.message_post(body=message)
                    else:
                        self.back_order = True
                        sale_order_copy.unlink()
                else:
                    self.back_order = True
                    sale_order_copy.unlink()
            except Exception as e:
                error = list(e)
                raise Warning(error[1])
        else:
            raise Warning(
                _("There is not enough stock for any product.")
            )
        validate_done = self.order_line.search([
            ('order_id', '=', self.id),
            ('is_delivery', '=', False),
            ('full_sale_order', '=', False)])
        validate_not_done = self.order_line.search([
            ('order_id', '=', self.id),
            ('is_delivery', '=', False),
            ('full_sale_order', '=', False)])
        if not validate_done:
            self.state_back_order_referral = 'referral'
            self.message_post(body=_("Back Order <strong>Referral</strong>"))
        if validate_not_done:
            self.state_back_order_referral = 'partial_referral'
            self.message_post(body=_("Back Order <strong>Partial Referral</strong>"))
        return True

    @api.multi
    def create_purchase_order(self):
        vals = {}
        suppliers_purchase = {}
        products_without_supplier = []
        products_in_purchase = []
        message = ""

        for sale_line in self.order_line:
            product_tmpl = sale_line.product_id.product_tmpl_id
            if not sale_line.created_purchase_order and not sale_line.created_sale_order:
                purchase_obj = self.env['purchase.order']
                purchase_line_obj = self.env['purchase.order.line']
                product = sale_line.product_id
                suppliers = product_tmpl.seller_ids
                company = self.env['res.users'].browse(self._uid).company_id
                if not suppliers:
                    if product_tmpl.type not in ('service',):
                        products_without_supplier.append(sale_line.name)
                else:
                    for first_supplier in suppliers:
                        supplier = first_supplier.name
                        break
                    currency = supplier.property_product_pricelist_purchase.currency_id.id
                    if str(supplier.id) not in suppliers_purchase:
                        purchase_order = purchase_obj.search([
                            ('state', 'in', ('draft', 'sent', 'bid')),
                            ('partner_id', '=', supplier.id)],
                            order='create_date desc', limit=1)
                        if purchase_order:
                            suppliers_purchase.update(
                                {str(supplier.id): purchase_order}
                            )
                    if str(supplier.id) in suppliers_purchase:
                        purchase = suppliers_purchase.get(str(supplier.id))
                        purchase_line = purchase_line_obj.search([
                            ('order_id', '=', purchase.id),
                            ('product_id', '=', product.id)])
                        if purchase_line:
                            purchase_line.product_qty += sale_line.product_uom_qty - sale_line.qty_remitted
                            back_order_list = purchase_line.back_order.mapped('id') or []
                            back_order_list.append(self.id)
                            purchase_line.back_order = back_order_list
                            purchase.back_orders_related = ', '.join(self.name)
                        else:
                            purchase_line = purchase_line_obj.search([('order_id', '=', purchase.id)], limit=1)
                            if purchase_line:
                                context = self.env.context
                                purchase_line.env.context = {}
                                new_purchase_line = purchase_line.sudo().copy()
                                self.env.context = context
                                new_purchase_line.product_id = sale_line.product_id.id
                                new_purchase_line.name = sale_line.name
                                new_purchase_line.product_qty = sale_line.product_uom_qty - sale_line.qty_remitted
                                new_purchase_line.price_unit = 0.0
                                back_order_list = new_purchase_line.back_order.mapped('id') or []
                                back_order_list.append(self.id)
                                new_purchase_line.back_order = back_order_list
                            else:
                                value_line = {
                                                'product_uom': product_tmpl.uom_id.id,
                                                'price_unit': 0.0,
                                                'product_qty': sale_line.product_uom_qty - sale_line.qty_remitted,
                                                'partner_id': supplier.id,
                                                'invoiced': False,
                                                'company_id': company.id,
                                                'state': 'draft',
                                                'order_id': purchase.id,
                                                'name': sale_line.name,
                                                'product_id': product.id,
                                                'date_planned': fields.Date.today(),
                                                'nacional_internacional': 'N',
                                                'back_order': [[6, False, [self.id]]]
                                            }
                                new_purchase_line = purchase_line_obj.create(value_line)
                                new_purchase_line.taxes_id = [taxes.id for taxes in product_tmpl.supplier_taxes_id]
                        sale_line.purchase_order_id = purchase.id
                        products_in_purchase.append(sale_line.name)
                        # Placing the names of back orders
                        back_order_ids = purchase.order_line.mapped('back_order')
                        purchase.back_orders_related = ', '.join(back_order_ids.mapped('name'))
                    else:
                        picking_type_id = self.get_picking_type_id(self.warehouse_id.id)
                        location_id = purchase_obj.onchange_picking_type_id(picking_type_id).get('value').get('location_id')
                        vals.update(
                            {
                                'company_id': company.id,
                                'currency_id': currency,
                                'date_order': fields.Datetime.now(),
                                'partner_id': supplier.id,
                                'picking_type_id': picking_type_id,
                                'location_id': location_id,
                                'state': 'draft',
                                'pricelist_id': supplier.property_product_pricelist_purchase.id,
                                'nacional_partner_id': supplier.id,
                                'nacional_internacional': 'N',
                                'supplier_partner_bank_id': self.get_partner_bank_id(supplier, currency),
                                'back_orders_related': self.name,
                                'order_line': [
                                    [0, False, {
                                        'product_uom': product_tmpl.uom_id.id,
                                        'price_unit': 0.0,
                                        'product_qty': sale_line.product_uom_qty - sale_line.qty_remitted,
                                        'partner_id': supplier.id,
                                        'invoiced': False,
                                        'company_id': company.id,
                                        'state': 'draft',
                                        'name': sale_line.name,
                                        'product_id': product.id,
                                        'date_planned': fields.Date.today(),
                                        'nacional_internacional': 'N',
                                        'back_order': [[6, False, [self.id]]]
                                    }
                                    ]
                                ]
                            }
                        )
                        new_po = purchase_obj.create(vals)
                        for purchase_line in new_po.order_line:
                            purchase_line.taxes_id = [
                                taxes.id for taxes in purchase_line.product_id.product_tmpl_id.supplier_taxes_id
                                ]
                        sale_line.purchase_order_id = new_po.id
                        products_in_purchase.append(sale_line.name)
            else:
                pass
        if products_in_purchase:
            message += _('<p><strong>The following products are already in a purchase order.</strong></p> <br/>')
            self.state_back_order = 'partial_associated'
            for product in products_in_purchase:
                message += '<p>' + product + '</p>'
        if products_without_supplier:
            if message:
                message += '<br/> '
            message += _('<p><strong>One or more products do not have an assigned supplier.</strong></p> <br/>')
            for product in products_without_supplier:
                message += '<p>' + product + '</p>'
        sale_lines_complete = self.order_line.search([
            ('order_id', '=', self.id),
            ('is_delivery', '=', False),
            ('purchase_order_id', '=', False),
            ('full_sale_order', '=', False)])
        if not sale_lines_complete and \
                not products_in_purchase and \
                not products_without_supplier:
            raise Warning(_("Error adding products.\n\n "
                            "All the products were already added to a purchase order or they were processor in a sale.")
                          )
        if not sale_lines_complete:
            if message:
                message += '<br/> '
            message += _('<p><strong>All products were already added to a purchase order.</strong></p> <br/>')
            self.state_back_order = 'associated'
        self.message_post(body=message)
        return True

    def get_picking_type_id(self, warehouse_id):
        res = 0
        self._cr.execute(
            """
            SELECT spt.id FROM stock_picking_type spt
            INNER JOIN stock_warehouse sw ON sw.id=spt.warehouse_id
            WHERE sw.id = %s AND spt.code='incoming'
            """, (warehouse_id,)
        )
        if self._cr.rowcount:
            (res,) = self._cr.fetchone()
        return res

    def get_partner_bank_id(self, supplier, currency_id):
        bank_id = 0
        for account_bank in supplier.bank_ids:
            if account_bank.currency2_id.id == currency_id:
                bank_id = account_bank.id
                break
        return bank_id

    @api.model
    def cron_back_order_to_sale_order(self):
        back_orders = self.search([
            ('state_back_order', 'in', ('authorized', 'partial_associated', 'associated')),
            ('state_back_order_referral', 'in', ('not_referral', 'partial_referral')),
            ('back_order', '=', True)], order='create_date asc')
        if not back_orders:
            print _('Not exists back orders')
        for back_order in back_orders:
            if back_order.partner_id.status_client:
                print _('Sorry! this partner its blocked')
                continue
            products_available_to_back_order = {}
            products_available_to_back_order.update({str(back_order.id): []})
            for back_order_line in back_order.order_line:
                if not back_order_line.created_sale_order:
                    product = back_order_line.product_id.product_tmpl_id
                    if product.type not in ('service',):
                        product_id = back_order_line.product_id.id
                        product_qty = back_order_line.product_uom_qty
                        qty_remitted = back_order_line.qty_remitted
                        warehouse_id = back_order.warehouse_id.id
                        self._cr.execute("""
                                SELECT COALESCE(SUM(sq.qty), 0.0)
                                FROM stock_warehouse AS sw
                                INNER JOIN stock_location AS sl ON sw.lot_stock_id = sl.id
                                INNER JOIN stock_quant sq ON sl.id=sq.location_id AND reservation_id IS NULL
                                WHERE sl.usage='internal' AND sw.id=%s
                                AND product_id=%s""", (warehouse_id, product_id))
                        if self._cr.rowcount:
                            (quants,) = self._cr.fetchone()
                            if quants > 0.00:
                                if quants >= product_qty - qty_remitted:
                                    quantity_to_remitted = product_qty - qty_remitted
                                else:
                                    quantity_to_remitted = quants

                                lines = products_available_to_back_order.get(str(back_order.id), [])
                                lines.append({'line': back_order_line, 'quantity_to_remitted': quantity_to_remitted})
                                products_available_to_back_order.update({str(back_order.id): lines})
            available_lines = products_available_to_back_order.get(str(back_order.id))
            if available_lines:
                back_order.back_order = False
                sale_order_copy = back_order.copy()
                sale_order_copy.back_order_related_id = 0
                for line in sale_order_copy.order_line:
                    if not line.is_delivery:
                        line.unlink()
                for line in available_lines:
                    new_line = line.get('line').copy()
                    new_line.order_id = sale_order_copy.id
                    new_line.product_uom_qty = line.get('quantity_to_remitted')
                    new_line.product_uos_qty = line.get('quantity_to_remitted')
                    line.get('line').qty_remitted += line.get('quantity_to_remitted')
                    if line.get('line').created_sale_order:
                        line.get('line').full_sale_order = True
                try:
                    check_stock = sale_order_copy.check_stock()
                    if check_stock:
                        res = True
                        if res:
                            sale_order_copy.name = self.env['ir.sequence'].get('sale.order') or '/'
                            back_order.back_order = True
                            sale_order_copy.back_order_related_id = back_order.id
                            back_order.count_sale_order += 1
                            sale_order_copy.button_dummy()
                            sale_order_copy.action_button_confirm()
                        else:
                            back_order.back_order = True
                            sale_order_copy.unlink()
                    else:
                        back_order.back_order = True
                        sale_order_copy.unlink()
                except Exception as e:
                    print list(e)
                    return False
                validate_done = self.order_line.search([
                    ('order_id', '=', back_order.id),
                    ('is_delivery', '=', False),
                    ('full_sale_order', '=', False)])
                if not validate_done:
                    back_order.state_back_order_referral = 'referral'
                    back_order.message_post(body=_("Back Order <strong>Referral</strong>"))
                else:
                    back_order.state_back_order_referral = 'partial_referral'
                    back_order.message_post(body=_("Back Order <strong>Partial Referral</strong>"))
            else:
                print back_order.name + _(": Not available products")
        return True

    @api.one
    def cancel(self):
        self.state_back_order = 'cancel'
        self.message_post(body=_("Back Order Canceled"))
        return True

    @api.one
    def back_order_to_sale_order_from_wizard(self, lines_to_apply):
        products_available_to_back_order = {}
        products_available_to_back_order.update({str(self.id): []})
        if self.partner_id.status_client:
            raise Warning(
                _("Sorry, this customer is blocked"),
                _("You to communicate with an administrator of this configuration")
            )
        for (back_order_line, quantity_to_remitted)in lines_to_apply:
            if not back_order_line.created_sale_order:
                product = back_order_line.product_id.product_tmpl_id
                if product.type not in ('service',):
                    product_id = back_order_line.product_id.id
                    warehouse_id = self.warehouse_id.id
                    self._cr.execute("""
                        SELECT COALESCE(SUM(sq.qty), 0.0)
                        FROM stock_warehouse AS sw
                        INNER JOIN stock_location AS sl ON sw.lot_stock_id = sl.id
                        INNER JOIN stock_quant sq ON sl.id=sq.location_id AND reservation_id IS NULL
                        WHERE sl.usage='internal' AND sw.id=%s
                        AND product_id=%s""", (warehouse_id, product_id))
                    if self._cr.rowcount:
                        (quants,) = self._cr.fetchone()
                        if quants > 0.00:
                            if quants >= quantity_to_remitted:
                                quantity_to_remitted = quantity_to_remitted
                            else:
                                quantity_to_remitted = quants

                            lines = products_available_to_back_order.get(str(self.id), [])
                            lines.append({'line': back_order_line, 'quantity_to_remitted': quantity_to_remitted})
                            products_available_to_back_order.update({str(self.id): lines})
        available_lines = products_available_to_back_order.get(str(self.id))
        if available_lines:
            self.back_order = False
            sale_order_copy = self.copy()
            sale_order_copy.back_order_related_id = 0
            for line in sale_order_copy.order_line:
                if not line.is_delivery:
                    line.unlink()
            products_to_sale = []
            for line in available_lines:
                new_line = line.get('line').copy()
                new_line.order_id = sale_order_copy.id
                new_line.product_uom_qty = line.get('quantity_to_remitted')
                new_line.product_uos_qty = line.get('quantity_to_remitted')
                line.get('line').qty_remitted += line.get('quantity_to_remitted')
                line.get('line').sale_order_list = ''.join(str(sale_order_copy.id))
                if line.get('line').created_sale_order:
                    line.get('line').full_sale_order = True
                products_to_sale.append(line.get('line').name)
            try:
                check_stock = sale_order_copy.check_stock()
                if check_stock:
                    res = True
                    if res:
                        message = ""
                        sale_order_copy.name = self.env['ir.sequence'].get('sale.order') or '/'
                        self.back_order = True
                        sale_order_copy.back_order_related_id = self.id
                        sale_order_copy.button_dummy()
                        sale_order_copy.action_button_confirm()
                        self.count_sale_order += 1
                        message += _(
                            '<p><strong>The following products are already in a sale order.</strong></p> <br/>')
                        for product in products_to_sale:
                            message += '<p>' + product + '</p>'
                        self.message_post(body=message)
                    else:
                        self.back_order = True
                        sale_order_copy.unlink()
                else:
                    self.back_order = True
                    sale_order_copy.unlink()
            except Exception as e:
                error = list(e)
                raise Warning(error[1])
        else:
            raise Warning(
                _("There is not enough stock for any product.")
            )
        validate_done = self.order_line.search([
            ('order_id', '=', self.id),
            ('is_delivery', '=', False),
            ('full_sale_order', '=', False)])
        if not validate_done:
            self.state_back_order_referral = 'referral'
            self.message_post(body=_("Back Order <strong>Referral</strong>"))
        else:
            self.state_back_order_referral = 'partial_referral'
            self.message_post(body=_("Back Order <strong>Partial Referral</strong>"))
        return True
