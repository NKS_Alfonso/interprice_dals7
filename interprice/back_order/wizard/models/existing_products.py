# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# local application/library specific imports
from openerp import _, api, fields, models
from openerp.exceptions import Warning


class ExistingProductsWizard(models.TransientModel):
    """
    This wizard displays the products available of the Back Order
    """
    _name = 'existing_products_wizard'
    _description = 'Existing Products Wizard'

    @api.onchange('back_order')
    def _onchange_back_order(self):
        back_order_id = self.env.context.get('default_back_order', False)
        back_order = self.env['sale.order'].browse(back_order_id)
        value = []
        if back_order.partner_id.status_client:
            raise Warning(
                _("Sorry, this customer is blocked"),
                _("You to communicate with an administrator of this configuration")
            )
        for back_order_line in back_order.order_line:
            if not back_order_line.created_sale_order:
                if back_order_line.qty_remitted < back_order_line.product_uom_qty:
                    product = back_order_line.product_id.product_tmpl_id
                    if product.type not in ('service',):
                        product_id = back_order_line.product_id.id
                        product_qty = back_order_line.product_uom_qty
                        qty_remitted = back_order_line.qty_remitted
                        warehouse_id = back_order.warehouse_id.id
                        self._cr.execute("""
                            SELECT COALESCE(SUM(sq.qty), 0.0)
                            FROM stock_warehouse AS sw
                            INNER JOIN stock_location AS sl ON sw.lot_stock_id = sl.id
                            INNER JOIN stock_quant sq ON sl.id=sq.location_id AND reservation_id IS NULL
                            WHERE sl.usage='internal' AND sw.id=%s
                            AND product_id=%s""", (warehouse_id, product_id))
                        if self._cr.rowcount:
                            (quants,) = self._cr.fetchone()
                            if quants >= product_qty - qty_remitted:
                                quantity_to_remitted = product_qty - qty_remitted
                                quantity_available = product_qty - qty_remitted
                            else:
                                quantity_to_remitted = quants
                                quantity_available = quants

                            value.append(
                                {
                                    'line': back_order_line.id,
                                    'product': product_id,
                                    'quantity_ordered': product_qty,
                                    'quantity_for_remitted': product_qty - qty_remitted,
                                    'quantity_available': quantity_available,
                                    'quantity_to_remitted': quantity_to_remitted,
                                    'wizard_id': self.id
                                }
                            )
        if value:
            self.existing_product_line = value

    back_order = fields.Many2one(
        comodel_name="sale.order",
        string=_("Back Order"),
        readonly=True
    )
    existing_product_line = fields.One2many(
        comodel_name="existing_products_line",
        inverse_name="wizard_id",
        string=_("Existing Products"),
        readonly=False
    )

    @api.multi
    def accept(self):
        message_warning = False
        lines_to_apply = []
        for line in self.existing_product_line:
            if line.quantity_to_remitted > 0.0:
                lines_to_apply.append((line.line, line.quantity_to_remitted))
                message_warning = True
        if not message_warning:
            raise Warning(_("Error"),
                          _("Sorry there is no quantity available for the products of this Back Order"))
        return self.back_order.back_order_to_sale_order_from_wizard(lines_to_apply)


class ExistingProductsLine(models.TransientModel):
    """ wizard lines """

    _name = 'existing_products_line'
    _description = 'Existing Products Line'

    line = fields.Many2one(
        comodel_name="sale.order.line",
        string=_("Line"),
        readonly=True,
    )
    product = fields.Many2one(
        comodel_name="product.product",
        string=_("Product"),
        readonly=True,
    )
    quantity_ordered = fields.Float(
        string=_("Quantity Ordered"),
        readonly=True,
    )
    quantity_for_remitted = fields.Float(
        string=_("Quantity for remitted"),
        readonly=True,
    )
    quantity_available = fields.Float(
        string=_("Quantity Available"),
        readonly=True,
    )
    quantity_to_remitted = fields.Float(
        string=_("Amount to apply"),
        readonly=False,
    )
    wizard_id = fields.Many2one(
        comodel_name="existing_products_wizard",
        string=_("Wizard"),
        readonly=True,
    )

    @api.onchange('quantity_to_remitted')
    def _onchange_quantity_to_remitted(self):
        if self.quantity_to_remitted > 0.00:
            if self.quantity_to_remitted > self.quantity_for_remitted or self.quantity_to_remitted > self.quantity_available:
                self.quantity_to_remitted = self.quantity_available
                return {
                    'warning': {
                        'title': _("Error"),
                        'message': _("The amount to be remitted is greater than the amount available")
                    }
                }
