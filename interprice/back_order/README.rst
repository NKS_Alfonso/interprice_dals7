==========
Back Order
==========

v1.0.1
======

- **Ubicación del nuevo menú Back Order.**


    El menú Back Order se posiciona en Ventas / Ventas / Back Order

.. figure:: ../back_order/static/img/menu_back_order.png
    :alt: menu back order
    :width: 80%

    menú back order.


- **Creando Back Order.**


.. figure:: ../back_order/static/img/creando_back_order.png
    :alt: creando back order
    :width: 80%

    creando back order.

    Al crear una Back Order es necesario,
    Introducir los datos correctos del almacén de donde se dispondrán los productos, con
    esto el cron podrá identificar la disponibilidad, para que esto pase se debe
    confirmar la Back Order.

.. figure:: ../back_order/static/img/confirmando_back_order.png
    :alt: confirmando back order
    :width: 80%

    confirmando back order.

    Al confirmar la Back Order,
    El cron se encargara de verificar la disponibilidad de los productos en el almacén cada minuto,
    si estos están disponibles se creara una Orden de Venta (Se confirmara y se reservaran los productos)
    relacionada a la Back Order, las ventas se relacionaran a la Back Order en el botón Pedidos de venta.

.. figure:: ../back_order/static/img/creando_sale_order.png
    :alt: creando sale order
    :width: 80%

    creando sale order.

    La funcionalidad que realiza el cron se podrá realizar manualmente con el botón Crear Orden de Venta,
    siempre y cuando los productos estén disponibles.

.. figure:: ../back_order/static/img/funcion_manual_del_cron.png
    :alt: creando sale order manual
    :width: 80%

    creando sale order manual.


- **Mensaje para crear Back Order en Orden de Venta.**


    Al agregar un producto a un Pedido de Venta y este no esta disponible en stock, se enviara un
    mensaje al usuario pidiendo crear una Back Order, para la compra del producto.

.. figure:: ../back_order/static/img/creando_back_order_desde_SO.png
    :alt: creando back order desde SO
    :width: 80%

    creando back order desde SO.


- **Back Order en pedido de compra.**


    Al asignar un proveedor al pedido de compra resaltara un mensaje en el formulario, informando que existen
    Back Order(s) con productos del proveedor confirmadas. Para esto se necesita agregar el proveedor a los
    productos correspondieres.

.. figure:: ../back_order/static/img/mensaje_existencia_de_bo_en_po.png
    :alt: mensaje de BO en PO
    :width: 80%

    Mensaje de existencias de productos en BO.

    Después de mostrar el mensaje en el formulario se podrán seleccionar del campo (=> Agregar productos de BO <= )
    las BO que contengan productos del proveedor.
    Nota: solo se asignaran al pedido los productos con referencia al proveedor seleccionado.

.. figure:: ../back_order/static/img/asignar_productos_de_BO_a_PO.png
    :alt: asignar productos de BO a PO
    :width: 80%

    Asignando productos de BO al pedido de compra.

    Al guardar el pedido de compra se informara de manera automática en los logs de mensajes de la BO, los
    productos que fueron agregados al pedido, así de igual manera al eliminarlos de un pedido de compra
    previamente guardado.

.. figure:: ../back_order/static/img/logs_de_mesajes_de_BO.png
    :alt: logs de masajes de BO
    :width: 80%

    Logs de Mesajes de BO


- **Crear Pedido de compra desde Back Order.**


    Un pedido de compra podrá crearse desde una Back Order con el botón (=> Crear pedido de compra <=),
    de igual manera que la explicación anterior se generara en los logs de mensajes los registros de los
    productos anexados a un pedido de compra o los que no fueron agregados por falta de proveedor.

.. figure:: ../back_order/static/img/producto_sin_proveedor.png
    :alt: mensaje de BO sin proveedor
    :width: 80%

    Botón crear pedido de compra

.. figure:: ../back_order/static/img/producto_sin_proveedor_logs.png
    :alt: logs de masajes de BO
    :width: 80%

    Logs de Mesajes de BO

.. figure:: ../back_order/static/img/orden_de_compra_cancelada.png
    :alt: logs de masajes de BO
    :width: 80%

    Logs de Mesajes de BO
