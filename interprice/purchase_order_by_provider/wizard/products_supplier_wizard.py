# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, _, fields, exceptions, models
from openerp.exceptions import except_orm, Warning
from suds.client import Client
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class ProductsSupplierWizard(models.TransientModel):
    _name = 'products.supplier.wizard'

    partner_id = fields.Many2one('res.partner', 'Supplier')
    order_id = fields.Many2one('sale.order', 'Order')
    partner_cost_center = fields.Many2one(
                    'supplier.cost.center.line',
                    string='Cost Center'
                )
    lines_ids = fields.One2many(
                    'products.supplier.line',
                    'order_supplier_id',
                    'Generate lines'
                )
    products_supplier_ids = fields.One2many(
                    'purchase.supplier.line',
                    'products_supplier_id',
                    'Products Supplier'
                )

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        res = {}
        if self.partner_id:
            cost_center_ids = self.partner_id.api_supplier_center_cost_line.ids
            res['domain'] = {'partner_cost_center': [
                                    ('id', 'in', cost_center_ids)
                                ]}
        return res

    @api.multi
    def search_products(self):
        if self.partner_id:
            service = self.partner_id.api_service_type
            if 'soap' in service:
                url = self.partner_id.api_manage_docs_url
                if url:
                    # Open service SOAP
                    try:
                        self.client = Client(url)
                    except Exception as e:
                        raise Warning(e)
                    result_view = self.view_products_supplier(
                                        self.lines_ids,
                                        supplier=self.partner_id,
                                        order=self.order_id,
                                        warehouse=self.partner_cost_center
                                    )
                else:
                    raise Warning(""" The partner don´t have configured Url of
                                  API service. """)
            elif 'rest' in service:
                # TODO Later, work will be done on a REST Service
                pass
            else:
                raise Warning('The partner don´t have configured API service.')
        return result_view

    def view_products_supplier(self, lines_ids, supplier=None, order=None, warehouse=None):
        result = {}
	product = {}
        line_list = []
        not_found_list = []
        cost_center = []
        # location_ids = self.env['stock.location'].search(
        #                                 [('name', 'ilike', 'Existencias')]
        #                             )
        sql = """
                SELECT id
                FROM stock_location
                WHERE name ILIKE 'Existencias'
            """
        execute_sql = self.env.cr.execute(sql)
        sql_result = self.env.cr.dictfetchall()
        location_ids = []

        for record in sql_result:
            location_ids.append(str(record['id']))
        if 'PCH' in supplier.name:
            for line in lines_ids:
                data = {
                        'cliente': supplier.api_user,
                        'llave': supplier.api_password,
                        'sku': line.product_id.product_tmpl_id.clave_fabricante
                }
                try:
                    result = self.client.service.ObtenerArticulo(**data)
                    """ To get the All Item list
                    # result=self.client.service.ObtenerListaArticulos(**data)
                    # _logger.warning('---------------> res {}'.format(result))
                    # raise Warning('lista de articulos completa')
                    """
                except Exception as e:
                    raise Warning(e)
                if result.estatus:
                    for inventory in result.datos.inventario:
                        if inventory['almacen'] == int(warehouse.identification_warehouse):
                            cost_center = warehouse.id
                            product = {
                                'product_sku': result.datos.sku,
                                'product_description':result.datos.descripcion,
                                'product_quantity': line.product_qty,
                                'product_stock_internal': self.get_quants(line.product_id.id, location_ids),
                                'product_stock_supplier': inventory.existencia,
                                'product_price': result.datos.precio,
                                'product_id': line.product_id.id,
                                'product_currency': result.datos.moneda
                            }
                            line_list.append(product)
                        else:
                            not_found_list.append(line.product_id.name)
                else:
                    raise Warning("""El producto {} no se puede comprar con
                                    este proveedor. """.format(
                                            line.product_id.default_code
                                    ))
            if not product:
                products = ''
                for product in not_found_list:
                    products += str(product) + ', '
                raise Warning(_("""The following products were not found in the
                warehouse you specified (%s): \n %s"""%(self.partner_cost_center.name, str(products))))
        context = {
                    'default_products_supplier_ids': line_list,
                    'default_partner_id': supplier.id,
                    'default_order_id': order.id,
                    'default_partner_cost_center': cost_center
                }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'products.supplier.wizard',
            'view_id': self.env.ref("purchase_order_by_provider.iuv_todo_purchase_supplier_wizard", False).id,
            'view_mode': 'form',
            'target': 'new',
            'context': context
        }

    @api.multi
    def generate_purchase(self):
        result = {}
        purchase_obj = self.env['purchase.order']
        nacional = 'N' if 'México' in self.partner_id.country_id.name else 'I'

        if self.partner_id.api_currency_list:
            if self.products_supplier_ids:
                api_currency_list = self.partner_id.api_currency_list.split(',')
                for api_currency in api_currency_list:
                    line_list = []
                    order_lines = []
                    # Remove possible spaces between comas
                    api_currency = api_currency.replace(' ', '')
                    for line in self.products_supplier_ids:
                        # Validate that all products have quantity
                        if line.product_quantity <= 0:
                            raise Warning('An amount greater than 0 is needed')
                        tax_id = line.product_id.product_tmpl_id.supplier_taxes_id
                        # Get dict to create purchase_order_line
                        data_line = {
                            'product_uom': line.product_id.product_tmpl_id.uom_id.id,
                            'price_unit': line.product_price,
                            'product_qty': line.product_quantity,
                            'partner_id': self.partner_id.id,
                            'name': line.product_description,
                            'product_id': line.product_id.id,
                            'date_planned': datetime.now(),
                            'nacional_internacional': nacional,
                            # This is a make to ingress a Many2many
                            # in a created method
                            'taxes_id': [(6, 0, [x.id for x in tax_id])]
                        }
                        if api_currency in line.product_currency:
                            currency = line.product_id.product_tmpl_id.list_price_currency
                            order_lines.append((0, False, data_line))
                            # Create List to send Webservice PCH
                            line_list.append({
                                'strSku': line.product_sku,
                                'iCantidad': line.product_quantity,
                            })
                    if order_lines:
                        pricelist_ids = self.env['product.pricelist'].search([
                                                ('currency_id', '=', currency.id)
                                                ])
                        if len(pricelist_ids) > 1:
                            pricelist_id = pricelist_ids[0].id
                        else:
                            pricelist_id = pricelist_ids.id
                        stock_wh_obj = self.env['stock.warehouse']
                        stock_wh_id = stock_wh_obj.search([(
                            'id',
                            '=',
                            self.env.context.get('warehouse_id')
                        )])
                        purchase_data = {
                                'state': 'draft',
                                'nacional_internacional': nacional,
                                'nacional_partner_id': self.partner_id.id,
                                'pricelist_id': pricelist_id,
                                'currency_id': currency.id,
                                'date_order': datetime.now(),
                                'picking_type_id': stock_wh_id.in_type_id.id,
                                'location_id': stock_wh_id.lot_stock_id.id,
                                'sale_order_id': self.order_id.id,
                                'order_line': order_lines
                            }
                        result = self.make_order_supplier(api_currency, line_list)
                        if result:
                            # Create a Purchase Order
                            purchase_order = purchase_obj.create(purchase_data)
                            if purchase_order.id:
                                purchase_order.write({
                                        'supplier_invoice': result.get('factura'),
                                        'supplier_store': result.get('sucursal'),
                                        'supplier_signature': result.get('remision'),
                                        'supplier_invoice_check': True
                                    })
                                print("PURCHASE ORDER DONE")
                                # self.order_id.write({'purchase_id': purchase_order.id})

                return {
                            'type': 'ir.actions.act_window',
                            'res_model': 'sale.order',
                            'res_id': self.order_id.id,
                            'view_id': self.env.ref('sale.view_order_form', False).id,
                            'view_mode': 'form',
                            'target': 'current'
                        }
            return False
        raise exceptions.Warning(_('The supplier does not have a currency type list set.'))

    def make_order_supplier(self, currency, line_list):
        # Send Data to Webservice PCH, generate Remission Note and Invoice
        data_remission = {
                'cliente': self.partner_id.api_user,
                'llave': self.partner_id.api_password,
                'almacen': self.partner_cost_center.identification_warehouse,
                'moneda': currency,
                'articulos': line_list,
                # 'incluirSeguro': 'si',
                # 'folioOC': 'si',
            }
        try:
            self.client = Client(self.partner_id.api_manage_docs_url)
            remission = self.client.service.GenerarRemision(**data_remission)
            if 'estatus' in remission:
                if remission.estatus:
                    data_invoice = {
                            'cliente': self.partner_id.api_user,
                            'llave': self.partner_id.api_password,
                            'remision': remission.datos,
                            'nombre': self.order_id.partner_shipping_id.name,
                            'calle': self.order_id.partner_shipping_id.street,
                            'numero': self.order_id.partner_shipping_id.l10n_mx_street3,
                            'colonia': self.order_id.partner_shipping_id.street2,
                            'ciudad': self.order_id.partner_shipping_id.city_id.name,
                            'estado': self.order_id.partner_shipping_id.state_id.name,
                            'cp': self.order_id.partner_shipping_id.zip
                       }
                    invoice = self.client.service.GenerarFactura(**data_invoice)
                    if 'estatus' in invoice:
                        if invoice.estatus:
                            response = {
                                'remision': remission.datos,
                                'factura': invoice.datos.iFactura,
                                'sucursal': invoice.datos.strSucursal
                            }
                else:
                    raise Warning(remission.mensaje)
            elif remission[0] == 500:
                raise Warning('Data missing to connect with PHC')
            else:
                raise Warning('Server PCH response Fail ')
        except Exception as e:
            raise Warning(e)
        return response

    def get_quants(self, product_id, location_id):
        product_stock = None
        cursor = self.env.cr
        # Get all products not reserved by Product
        location_id=','.join(location_id)
        sql = """
                SELECT  qty
                FROM    stock_quant
                WHERE   product_id = %s
                    AND reservation_id is Null
                    AND location_id IN (%s)
            """
        cursor.execute(sql % (product_id, location_id))
        if cursor.rowcount:
            quants_stock = cursor.fetchall()
            if len(quants_stock) > 1:
                product_stock = len(quants_stock)
            else:
                product_stock = quants_stock[0][0]
        else:
            product_stock = 0
        return product_stock
