# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, _, fields, exceptions, models
from openerp.exceptions import except_orm, Warning
import logging
_logger = logging.getLogger(__name__)


class ProductsDinamycO2m(models.TransientModel):
    _name = 'products.dinamyc.O2m'

    name = fields.Char('name')

    def create(self, order, api_warehouse, items_list):
        """
            when we create a looking products per warehouse supplier, 
            we add one2many field to purchase.supplier.line
        """
        o2m_field = {
                # fields created using orm method must start with x_
                "name": "x_suplier_SO%s_%s" % (order, api_warehouse),
                "field_description": "SO %s-%s" % (order, api_warehouse),
                "ttype": "one2many",
                "relation": "purchase.supplier.line",
                "relation_field": "products_supplier_id",
                "stored": True,
                # "domain": "[('mark_id','=', %s)]"%rec_id.id,
                "model_id": self.env.ref('purchase_order_by_provider.model_products_supplier_wizard').id,
                }
        # add on2many field to ir.model.fields
        o2m_obj = self.env["ir.model.fields"].create(o2m_field)
        _logger.warning('---------> o2m_obj {}'.format(o2m_obj))
        for items in items_list:
            _logger.warning('---------> items {}'.format(items))
            o2m_obj.create(items)
        _logger.warning('---------> o2m_obj {}'.format(o2m_obj))
        return o2m_obj
