# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models, _


class ProductsSupplierLine(models.TransientModel):
    _name = 'products.supplier.line'

    order_supplier_id = fields.Many2one('products.supplier.wizard', 'supplier')
    product_id = fields.Many2one('product.product', 'Product')
    product_clave = fields.Char('Clave', size=200)
    product_qty = fields.Integer('Quantity')
    product_uom = fields.Many2one('product.uom', 'unit of measure')
    product_price = fields.Float(_('Placement Cost'))
    product_cost = fields.Float('Price Cost')
    sub_total = fields.Float('Sub-Total')
