# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models


class PurchaseSupplierLine(models.TransientModel):
    _name = 'purchase.supplier.line'

    products_supplier_id = fields.Many2one(
        comodel_name='products.supplier.wizard',
        string='supplier'
    )
    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product'
    )
    product_sku = fields.Char(
        string='Sku',
        size=50
    )
    product_description = fields.Char(
        string='Description',
        size=200
    )
    product_quantity = fields.Integer(
        string='Quantity'
    )
    product_stock_internal = fields.Integer(
        string='Stock Internal'
    )
    product_stock_supplier = fields.Integer(
        string='Stock Supplier'
    )
    product_price = fields.Float(
        string='Price Unit'
    )
    product_currency = fields.Char(
        string='Currency',
        size=20
    )
    sub_total = fields.Float(
        string='Sub-Total'
    )
