# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': 'Purchase Order by Supplier',
    'depends': ['base', 'sale', 'sale_stock', 'purchase'],
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Uncategorized',
    'version': '0.1',
    'data': [
        'wizard/products_supplier_view.xml',
        'wizard/purchase_supplier_view.xml',
        'views/purchase_order_view.xml',        
        'views/res_partner_view.xml',
        'views/sale_order_view.xml'
    ],
    'application': True,
}
