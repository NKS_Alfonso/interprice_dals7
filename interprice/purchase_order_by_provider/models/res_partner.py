# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    api_user = fields.Char(
        string=_('User'),
        size=100
    )

    api_password = fields.Char(
        string=_('Password'),
        size=100
    )

    api_token = fields.Char(
        string=_('Token'),
        size=255
    )

    api_refesh_token = fields.Char(
        string=_('Refresh Token'),
        size=255
    )

    api_service_type = fields.Selection(
        [('none', 'Elegir'), ('soap', 'SOAP'), ('rest', 'REST')],
        string=_('Type Service'),
        default='none'
    )

    api_manage_name = fields.Char(
        string=_('Contact Name'),
        size=100,
        help=_('The Contact(Name) provider that Manage the API service')
    )

    api_manage_departament = fields.Char(
        string=_('Departament'),
        size=200,
        help=_('The Contact(Departament) provider that Manage the API service')
    )

    api_manage_email = fields.Char(
        string=_('Email'),
        size=200,
        help=_('The Contact(Email) provider that Manage the API service')
    )

    api_manage_phone = fields.Char(
        string=_('Phone'),
        size=200,
        help=_('The Contact(Phone) provider that Manage the API service')
    )

    api_manage_docs_url = fields.Char(
        string=_("Url Documentation"),
        size=255,
        help=_("Url where the documentation is located")
    )

    api_manage_description = fields.Text(
        string=_('Observations'),
        help=_('Descriptions General over The API service')
    )

    api_supplier_center_cost_line = fields.One2many(
        comodel_name='supplier.cost.center.line',
        inverse_name='supplier_cost_center_partner_id',
        string=_('Center Cost per Supplier')
    )

    api_currency_list = fields.Char(
        string='Currency List',
        size=255,
        help="A list separated by comma of received currency"
    )