# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, tools, conf, _


class SupplierCostCenterLine(models.Model):
    _name = 'supplier.cost.center.line'

    name = fields.Char(
        string=_('Cost Center'),
        size=255
    )
    supplier_cost_center_partner_id = fields.Many2one(
        comodel_name='res.partner',
        string=_('Partner')
    )
    identification_warehouse = fields.Char(
        string=_('Identification Warehouse'),
        size=20
    )
