# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, tools, conf, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    purchase_ids = fields.One2many(
            comodel_name='purchase.order',
            inverse_name='sale_order_id',
            string=_('Purchase List')
            )
    purchase_count = fields.Integer(
            compute='_count_purchase',
            string=_('Quantity Purchase')
            )

    @api.multi
    def generate_purchase_order(self):
        context = {}
        line_list = []
        order_line_obj = self.env['sale.order.line']
        line_exists = order_line_obj.search([("order_id", "=", self.ids[0])])
        if line_exists:
            for line in line_exists:
                if 'service' not in line.product_id.product_tmpl_id.type:
                    data = {
                            'order_supplier_id': line.id,
                            'product_id': line.product_id.id,
                            'product_clave': line.product_id.product_tmpl_id.clave_fabricante,
                            'product_qty': line.product_uom_qty,
                            'product_uom': line.product_uom.id,
                            'product_price': line.price_unit,
                            'product_cost': line.purchase_price,
                            'sub_total': line.price_subtotal
                    }
                    line_list.append(data)
            context = {
                        'default_lines_ids': line_list,
                        'default_order_id': self.ids[0],
                        'warehouse_id': self.warehouse_id.id
                      }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'products.supplier.wizard',
            'view_id':
                self.env.ref('purchase_order_by_provider.iuv_todo_products_supplier_wizard', False).id,
            'view_mode': 'form',
            'target': 'new',
            'context': context
        }

    @api.depends('purchase_ids')
    def _count_purchase(self):
        if len(self.purchase_ids) > 0:
            self.purchase_count = len(self.purchase_ids)
        else:
            self.purchase_count = 0

    @api.multi
    def view_purchase(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'domain': [('id', 'in', [po.id for po in self.purchase_ids])]
        }
