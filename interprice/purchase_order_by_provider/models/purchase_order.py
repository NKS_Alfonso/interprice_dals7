# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    sale_order_id = fields.Many2one('sale.order', 'Sale Order', readonly=True)

    supplier_invoice = fields.Char(
        string='Supplier Invoice',
        size=50
    )

    supplier_store = fields.Char(
        string='Supplier Store',
        size=250
    )

    supplier_signature = fields.Char(
        string='Suppler signature',
        size=50
    )

    supplier_invoice_check = fields.Boolean(
        string='Check Purchase',
        default=False
    )
