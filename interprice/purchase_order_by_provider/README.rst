Module Purchase Order By Provider
=================================
V 1.0.03
--------

This module looking automatize process of purchase just after build  a sales order from the API service
This module install one suite in res_partner with 11 fields to configurate the API Service

Is easy:

- Press the button "Generate Purchase Order"
- Choose supplier in the first wizard to looking products in database of supplier
- Change the quantity of products need to purchase in the second wizard


Button Generate
---------------

.. figure:: ../purchase_order_by_provider/static/img/module_supplier_button.png
    :alt: Lista de Usuarios
    :width: 80%


Wizard Choose supplier
----------------------

.. figure:: ../purchase_order_by_provider/static/img/module_supplier_wizard1.png
    :alt: Lista de Usuarios
    :width: 80%


Wizard Changed quantity
-----------------------

.. figure:: ../purchase_order_by_provider/static/img/module_supplier_wizard2.png
    :alt: Lista de Usuarios
    :width: 80%


Response Data Supplier Invoice
------------------------------

.. figure:: ../purchase_order_by_provider/static/img/module_supplier_purchase.png
    :alt: Lista de Usuarios
    :width: 100%

Once the sale is created, it will redirect you to this and you can visualize it
