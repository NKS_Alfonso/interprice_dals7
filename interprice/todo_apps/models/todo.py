# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields


class TodoApps(models.Model):
    _name = 'todo.apps'

    dbname = fields.Char(string='db name')
    dbhost = fields.Char(string='host name')
    dbuser = fields.Char(string='user name')
    dbpassword = fields.Char(string='password name')
    dbport = fields.Integer(string='Port')

    apphost = fields.Char(string='app host')
    appname = fields.Char(string='app name')
    applogo = fields.Char(string='app logo')
    appactive = fields.Boolean(string='app active')


class TodoOauth2(models.Model):
    _name = 'todo.oauth2'

    token = fields.Char(string='Token')
    params = fields.Char(string='parameter')
    done_date = fields.Datetime(string='parameter')
    traspaso = fields.Char(string='Traspaso')