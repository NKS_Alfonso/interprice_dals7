# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': "Integration Odoo to To-Do",

    'summary': "Integration Odoo to To-Do",
    'author': 'Copyright © 2017 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    'category': 'Configuration',
    'version': '0.1',

    'depends': ['base'],

    'data': [
        # 'data/demo.xml',
        'views/todo_apps_view.xml'
    ],

    'instalable': True,
}