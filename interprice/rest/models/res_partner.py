# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    webservice_client_default = fields.Boolean(
        string='webservice Default',
        help="""This Field is to utility when is necessary a client to take
                default configurations to webservices, Example shipping cost"""
    )
