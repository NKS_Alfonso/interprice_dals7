# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, tools


class RestfulGetStockProducts():

    # Create PostgreSQL view
    @staticmethod
    def create_restful_get_stock_products_view(cr):

        tools.sql.drop_view_if_exists(cr, 'restful_get_stock_product')

        # Create view
        cr.execute("""

        /* Function to get Stock_location and stock_warehouse by products*/

        CREATE OR REPLACE FUNCTION restful_get_stock_product(
                                                    IN id_products integer
                                            )RETURNS TABLE (
                                                    loc_name varchar,
                                                    cantidad double precision) AS $$
        BEGIN
        RETURN QUERY SELECT cc.name loc_name,
                            SUM(sq.qty)
                    FROM    stock_quant sq
                    LEFT JOIN stock_location sl
                         ON sl.id = sq.location_id
                    LEFT JOIN stock_warehouse sw
                         ON sw.lot_stock_id = sl.id
                    LEFT JOIN stock_warehouse_type swt
                         ON swt.id = sw.stock_warehouse_type_id
                    LEFT JOIN account_cost_center cc
                         ON cc.id = sw.centro_costo_id
                    WHERE sq.product_id = id_products
                         AND swt.code = 'fac'
                         AND sq.reservation_id is null
                    GROUP BY loc_name;
        RETURN;
        END;
        $$
        LANGUAGE 'plpgsql';

        """)
