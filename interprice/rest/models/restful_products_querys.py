# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# from openerp import _, models, tools


# class RestfulProductsQuerys(models.Model):
#     _name = 'restful.products.querys'
#     _auto = False

#     def init(self, cr):
#         # Drop view
#         tools.sql.drop_view_if_exists(cr, 'restful_get_products')
#         # Create view and function
#         cr.execute("""
#         CREATE OR REPLACE FUNCTION calc_price_unit(IN client_ids integer,
#                                            IN currency_ids integer,
#                                            IN currency_names varchar(6),
#                                            IN product_ids integer,
#                                            IN product_codes varchar(20))
#                                            RETURNS TABLE (precio numeric) AS $$
#         BEGIN
#         RETURN QUERY SELECT DISTINCT ON(prod_template.id)
#                     CASE
#                     WHEN prod_template.list_price_currency != res_currency.id AND rate_prod.rate_sale < rate_sale.rate_sale
#                         THEN prod_price_level.price / rate_prod.rate_sale
#                     WHEN prod_template.list_price_currency != res_currency.id AND rate_prod.rate_sale > rate_sale.rate_sale
#                         THEN prod_price_level.price * rate_sale.rate_sale
#                     ELSE prod_price_level.price
#                     END
#                     FROM product_template prod_template
#                     INNER JOIN product_product product ON (product.product_tmpl_id = prod_template.id)
#                     INNER JOIN res_partner ON (res_partner.id IN (client_ids))
#                     LEFT JOIN LATERAL (SELECT rel.price_list
#                                         FROM client_price_list_rel rel
#                                         JOIN price_list pl
#                                         ON (pl.id = rel.price_list)
#                                         WHERE partner = res_partner.id
#                                             AND pl.brand = prod_template.brand
#                                         ORDER BY price_list DESC
#                                         LIMIT 1)
#                         AS brand_pl
#                         ON (brand_pl IS NOT NULL)
#                     LEFT JOIN price_list
#                         ON (price_list.id =
#                             CASE WHEN brand_pl IS NULL
#                                 THEN res_partner.segment
#                             ELSE brand_pl.price_list
#                             END AND (price_list.initial_date <= now()
#                             AND price_list.final_date >= now()))
#                     INNER JOIN price_level
#                         ON (price_level.id =
#                             CASE WHEN price_list.id IS NOT NULL
#                                 THEN price_list.level
#                             ELSE res_partner.base
#                             END)
#                     INNER JOIN product_price_level prod_price_level
#                         ON (prod_price_level.product = prod_template.id
#                             AND prod_price_level.price_level = price_level.level_id)
#                     JOIN res_currency
#                         ON (res_currency.id IN (currency_ids)
#                             OR res_currency.name IN (currency_names))
#                     LEFT JOIN res_currency_rate rate_sale
#                         ON (rate_sale.id = (SELECT id FROM res_currency_rate rcr
#                                             WHERE rcr.currency_id = res_currency.id
#                                                 AND rcr.name <= now()
#                                                 AND rcr.rate_sale > 0
#                                             ORDER BY name DESC
#                                             LIMIT 1)
#                             )
#                     LEFT JOIN res_currency_rate rate_prod
#                         ON (rate_prod.id = (SELECT id
#                                             FROM res_currency_rate rcr
#                                             WHERE rcr.currency_id = prod_template.list_price_currency
#                                                 AND rcr.name <= now()
#                                                 AND rcr.rate_sale > 0
#                                             ORDER BY name DESC
#                                             LIMIT 1))
#                     WHERE product.id IN (product_ids)
#                         OR product.default_code IN (product_codes);

#         RETURN;
#         END;
#         $$
#         LANGUAGE 'plpgsql';



#          /* Function to get Stock_location and stock_warehouse by products*/

#         CREATE OR REPLACE FUNCTION get_all_stock_product(
#                                                     IN cost_center integer
#                                             )RETURNS TABLE (
#                                                     sku varchar,
#                                                     precio bigint) AS $$
#         BEGIN
#         RETURN QUERY SELECT pp.default_code as sku,
#                             count(sq.qty)
#                     FROM    stock_quant sq
#                     LEFT JOIN product_product pp
#                          ON pp.id = sq.product_id
#                     LEFT JOIN product_template pt
#                          ON pt.id = pp.product_tmpl_id
#                     LEFT JOIN stock_location sl
#                          ON sl.id = sq.location_id
#                     LEFT JOIN stock_warehouse sw
#                          ON sw.lot_stock_id = sl.id
#                     LEFT JOIN account_cost_center cc
#                          ON cc.id = sw.centro_costo_id
#                     WHERE sq.location_id = 12
#                          AND sq.reservation_id is null
#                          AND cc.id = cost_center
#                     GROUP BY sku;
#         RETURN;
#         END;
#         $$
#         LANGUAGE 'plpgsql';



#         /* Function to get Stock_location and stock_warehouse by products*/

#         CREATE OR REPLACE FUNCTION get_stock_product(
#                                                     IN id_products integer
#                                             )RETURNS TABLE (
#                                                     loc_name varchar,
#                                                     cantidad bigint) AS $$
#         BEGIN
#         RETURN QUERY SELECT cc.name loc_name,
#                             count(sq.qty)
#                     FROM    stock_quant sq
#                     LEFT JOIN stock_location sl
#                          ON sl.id = sq.location_id
#                     LEFT JOIN stock_warehouse sw
#                          ON sw.lot_stock_id = sl.id
#                     LEFT JOIN account_cost_center cc
#                          ON cc.id = sw.centro_costo_id
#                     WHERE sq.product_id = id_products
#                          AND sq.location_id = 12
#                          AND sq.reservation_id is null
#                     GROUP BY loc_name;
#         RETURN;
#         END;
#         $$
#         LANGUAGE 'plpgsql';



#         /* View to get products data */


#         CREATE OR REPLACE VIEW restful_get_products AS
#         SELECT
#            pp.id,
#            pt.id product_t_id,
#            pp.default_code sku,
#            pt.name nombre,
#            pt.clave_fabricante clave,           
#            pp.ean13,
#            pt.volume,
#            pt.weight,
#            pt.weight_net,
#            rc.name moneda,
#            pt.list_price,
#            pc1.id Id_section,
#            pc1.name section,
#            pc2.id Id_line,
#            pc2.name line,
#            pc3.id Id_brand,
#            pc3.name brand,
#            pc4.id Id_serial,
#            pc4.name serial,
#            rc.id currency_id
#         FROM product_product pp
#         LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id
#         LEFT JOIN product_category pc1 ON pc1.id = pt.section
#         LEFT JOIN product_category pc2 ON pc2.id = pt.line
#         LEFT JOIN product_category pc3 ON pc3.id = pt.brand
#         LEFT JOIN product_category pc4 ON pc4.id = pt.serial
#         LEFT JOIN res_currency rc ON rc.id = pt.list_price_currency
#         WHERE pt.id = pp.product_tmpl_id
#             AND pt.active = true
#             AND pt.sale_ok = true
#             AND pt.web_ok = true;
#         """)
