# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, tools


class RestfulGetProducts():

    # Create PostgreSQL view
    # @staticmethod
    def create_restful_get_products_view(self, cr):
        tools.sql.drop_view_if_exists(cr, 'restful_get_products')

        # Create view
        cr.execute("""

            /* View to get products data */

            CREATE OR REPLACE VIEW restful_get_products AS
            SELECT
            pp.id,
            pt.id product_t_id,
            pp.default_code sku,
            pt.name nombre,
            pt.clave_fabricante clave,
            pp.ean13,
            pt.volume,
            pt.weight,
            pt.weight_net,
            rc.name moneda,
            pt.list_price,
            pc1.id Id_section,
            pc1.name section,
            pc2.id Id_line,
            pc2.name line,
            pc3.id Id_brand,
            pc3.name brand,
            pc4.id Id_serial,
            pc4.name serial,
            rc.id currency_id
            FROM product_product pp
            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id
            LEFT JOIN product_category pc1 ON pc1.id = pt.section
            LEFT JOIN product_category pc2 ON pc2.id = pt.line
            LEFT JOIN product_category pc3 ON pc3.id = pt.brand
            LEFT JOIN product_category pc4 ON pc4.id = pt.serial
            LEFT JOIN res_currency rc ON rc.id = pt.list_price_currency
            WHERE pt.id = pp.product_tmpl_id
                AND pt.active = true
                AND pt.sale_ok = true
                AND pt.web_ok = true;

        """)
