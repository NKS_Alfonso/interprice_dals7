# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api
import string
import random
import logging
_logger = logging.getLogger(__name__)


class RestfulUsers(models.Model):
    _name = 'restful.users'
    _rec_name = 'partner'
    _sql_constraints = [                #This Partner have a profile active consuming our services, Please ingress a different partner.
     ('users_uniq', 'unique(partner)', ' Este usuario ya cuenta con un perfil consumiendo nuestros servicios, Ingrese un Usuario diferente.'),
     ]

    partner = fields.Many2one('res.partner', string='Cliente', help="Field Relational many2one to know what client consume our service Rest API")
    clientid = fields.Char('Client Id', size=200, help="A Client Id field is one credential need to get tokens and consume our service")
    secretid = fields.Char('Secret Id', size=200, help="A Secret Id field is other credential need to get tokens")
    last_login = fields.Datetime('Last Conection')
    active = fields.Boolean('Active')

    ##These fields are required by Djnago Res_Framework API
    password = fields.Char('Password', size=200, help="A password field")
    is_staff = fields.Boolean('Staff')

    @api.one
    def credentials(self):
        chars = string.ascii_uppercase + string.digits
        _clientid = ''.join(random.choice(chars) for _ in range(24))
        _secretid = ''.join(random.choice(chars) for _ in range(32))
        self.write({'clientid': _clientid,
                    'secretid': _secretid,
                    'active': True
                  })
        return {}
