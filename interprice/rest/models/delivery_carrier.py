# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, exceptions, models

class DeliveryCarrier(models.Model):
    _inherit = "delivery.carrier"

    delivery_is_web = fields.Boolean(
        default=False,
        string='Delivery Is Web',
        help="activara este campo para distinguirlos de los metodos que  \
              pueden ser web."
    )
