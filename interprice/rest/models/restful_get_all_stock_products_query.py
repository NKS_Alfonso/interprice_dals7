# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, tools


class RestfulGetAllStockProducts():

    # Create PostgreSQL view
    @staticmethod
    def create_restful_get_all_stock_products_view(cr):

        tools.sql.drop_view_if_exists(cr, 'restful_get_all_stock_product')

        # Create view
        cr.execute("""

         /* Function to get Stock_location and stock_warehouse by products*/

        CREATE OR REPLACE FUNCTION restful_get_all_stock_product(
                                                    IN cost_center integer
                                            )RETURNS TABLE (
                                                    sku varchar,
                                                    precio bigint) AS $$
        BEGIN
        RETURN QUERY SELECT pp.default_code as sku,
                            count(sq.qty)
                    FROM    stock_quant sq
                    LEFT JOIN product_product pp
                         ON pp.id = sq.product_id
                    LEFT JOIN product_template pt
                         ON pt.id = pp.product_tmpl_id
                    LEFT JOIN stock_location sl
                         ON sl.id = sq.location_id
                    LEFT JOIN stock_warehouse sw
                         ON sw.lot_stock_id = sl.id
                    LEFT JOIN account_cost_center cc
                         ON cc.id = sw.centro_costo_id
                    WHERE sq.location_id = 12
                         AND sq.reservation_id is null
                         AND cc.id = cost_center
                    GROUP BY sku;
        RETURN;
        END;
        $$
        LANGUAGE 'plpgsql';

        """)
