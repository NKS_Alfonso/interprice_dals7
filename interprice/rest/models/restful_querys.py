# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, models, tools
from .restful_get_products_query import RestfulGetProducts
from .restful_get_stock_products_query import RestfulGetStockProducts
from .restful_get_all_stock_products_query import RestfulGetAllStockProducts


class RestfulQuerys(models.Model):
    _name = 'restful.querys'
    _auto = False

    def init(self, cr):
        # Drop view
        # tools.sql.drop_view_if_exists(cr, 'restful_get_products')
        # Create view and function
        cr.execute("""
        CREATE OR REPLACE FUNCTION restful_calc_price_unit(
                                           IN client_ids integer,
                                           IN currency_ids integer,
                                           IN currency_names varchar(6),
                                           IN product_ids integer,
                                           IN product_codes varchar(20)
                                           )RETURNS TABLE (
                                               precio numeric
                                           ) AS $$
        BEGIN
        RETURN QUERY SELECT DISTINCT ON(prod_template.id)
                    CASE
                    WHEN prod_template.list_price_currency != res_currency.id AND rate_prod.rate_sale < rate_sale.rate_sale
                        THEN prod_price_level.price / rate_prod.rate_sale
                    WHEN prod_template.list_price_currency != res_currency.id AND rate_prod.rate_sale > rate_sale.rate_sale
                        THEN prod_price_level.price * rate_sale.rate_sale
                    ELSE prod_price_level.price
                    END
                    FROM product_template prod_template
                    INNER JOIN product_product product ON (product.product_tmpl_id = prod_template.id)
                    INNER JOIN res_partner ON (res_partner.id IN (client_ids))
                    LEFT JOIN LATERAL (SELECT rel.price_list
                                        FROM client_price_list_rel rel
                                        JOIN price_list pl
                                        ON (pl.id = rel.price_list)
                                        WHERE partner = res_partner.id
                                            AND pl.brand = prod_template.brand
                                        ORDER BY price_list DESC
                                        LIMIT 1)
                        AS brand_pl
                        ON (brand_pl IS NOT NULL)
                    LEFT JOIN price_list
                        ON (price_list.id =
                            CASE WHEN brand_pl IS NULL
                                THEN res_partner.segment
                            ELSE brand_pl.price_list
                            END AND (price_list.initial_date <= now()
                            AND price_list.final_date >= now()))
                    INNER JOIN price_level
                        ON (price_level.id =
                            CASE WHEN price_list.id IS NOT NULL
                                THEN price_list.level
                            ELSE res_partner.base
                            END)
                    INNER JOIN product_price_level prod_price_level
                        ON (prod_price_level.product = prod_template.id
                            AND prod_price_level.price_level = price_level.level_id)
                    JOIN res_currency
                        ON (res_currency.id IN (currency_ids)
                            OR res_currency.name IN (currency_names))
                    LEFT JOIN res_currency_rate rate_sale
                        ON (rate_sale.id = (SELECT id FROM res_currency_rate rcr
                                            WHERE rcr.currency_id = res_currency.id
                                                AND rcr.name <= now()
                                                AND rcr.rate_sale > 0
                                            ORDER BY name DESC
                                            LIMIT 1)
                            )
                    LEFT JOIN res_currency_rate rate_prod
                        ON (rate_prod.id = (SELECT id
                                            FROM res_currency_rate rcr
                                            WHERE rcr.currency_id = prod_template.list_price_currency
                                                AND rcr.name <= now()
                                                AND rcr.rate_sale > 0
                                            ORDER BY name DESC
                                            LIMIT 1))
                    WHERE product.id IN (product_ids)
                        OR product.default_code IN (product_codes);

        RETURN;
        END;
        $$
        LANGUAGE 'plpgsql';

        """)

        restful_get_products = RestfulGetProducts()
        restful_get_products.create_restful_get_products_view(cr)
        RestfulGetStockProducts.create_restful_get_stock_products_view(cr)
        RestfulGetAllStockProducts.create_restful_get_all_stock_products_view(cr)
