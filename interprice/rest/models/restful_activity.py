# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models


class RestfulActivity(models.Model):
    _name = 'restful.activity'

    activity_token = fields.Many2one('restful.tokens', 'Token', help="Field Relational many2one to know activitys of our service Rest API")
    resource = fields.Char('Resource', size=255, help="to know the Resource http request")
    metodo = fields.Char('Method HTTP', size=10, help="to know the http method type")
    date_entry = fields.Datetime('Date Request')
