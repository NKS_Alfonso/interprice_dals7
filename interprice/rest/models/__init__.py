# -*- encoding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author To-Do Developers

import restful_users
import restful_activity
import restful_tokens
# import restful_products_querys
import restful_querys
import restful_get_products_query
import restful_get_stock_products_query
import restful_get_all_stock_products_query
import restful_res_partner_info
import res_partner
import sale_order
import delivery_carrier
