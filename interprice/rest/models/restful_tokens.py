# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models


class RestfulTokens(models.Model):
    _name = 'restful.tokens'

    user_tokens = fields.Many2one('restful.users', string='User', help="Field Relational many2one of the user who consumes our service Rest API")
    token_use = fields.Char('Token activo', size=255, help="A token field active")
    token_live = fields.Datetime('Vida del token', help="Fecha cuando expira el token")
    refresh_token = fields.Char('Refresh Token', size=200)
    date_live = fields.Datetime('Fecha de creacion', help="Fecha cuando fue creado el token")
    active = fields.Boolean('Active')
