# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, tools


class RestfulResPartnerInfo(models.Model):
    _name = 'restful.res.partner.info'
    _auto = False

    def init(self, cr):
        # Drop view
        tools.sql.drop_view_if_exists(cr, 'restful_res_partner_info')
        # Create postgresql view
        cr.execute(
            """
                CREATE OR REPLACE VIEW restful_res_partner_info AS
                    SELECT
                        RP.id AS id,
                        RP.parent_id AS parent_id,
                        RP.name AS customer,
                        RP.type AS address_type,
                        RP.street AS street,
                        RP.l10n_mx_street3 AS no_int,
                        RP.l10n_mx_street4 AS no_ext,
                        -- RP.street2 AS street2,
                        RST.name AS settlement,
                        RSCC.name AS colony,
                        RCSC.name AS city,
                        -- RP.l10n_mx_city2 AS city2,
                        RCS.name AS state,
                        RC.name AS country,
                        RPC.name AS postal_code,
                        -- RP.ZIP AS zip,
                        ACC.name AS cost_center,
                        PDP.str_nombre AS pay_type
                    FROM res_partner AS RP
                        LEFT JOIN piramide_de_pago AS PDP ON RP.forma_pago = PDP.id
                        LEFT JOIN account_cost_center AS ACC ON RP.branch_office = ACC.id
                        LEFT JOIN res_settlement_type AS RST ON RP.settlement_type = RST.id
                        LEFT JOIN res_state_city_colony AS RSCC ON RP.street2 = RSCC.id
                        LEFT JOIN res_country_state_city AS RCSC ON RP.city_id = RCSC.id
                        LEFT JOIN res_country_state AS RCS ON RP.state_id = RCS.id
                        LEFT JOIN res_country AS RC ON RP.country_id = RC.id
                        LEFT JOIN res_postal_code AS RPC ON RP.zip = RPC.id;
            """
        )
