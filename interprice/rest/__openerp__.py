# -*- encoding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author To-Do Developers
{
    'name': 'Restful API',
    'depends': [
                'base',
                'product',
                'postal_code',
                'delivery_method',
                'clients_administration',
                'task_web_payment'
            ],
    'author': 'Copyright © 2017 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Uncategorized',
    'version': '0.1',
    'data': [
        'views/restful_users_views.xml',
        'views/restful_activity_views.xml',
        'views/restful_tokens_views.xml',
        'views/res_partner_view.xml',
        'views/sale_order_view.xml',
        'views/delivery_carrier_view.xml'
    ],
    'application': True,
}
