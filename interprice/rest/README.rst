Modulo de administracion de Usuarios y tokens
para el servicio Restful.

V 1.0.03

Instalar y verlo en Configuracion -> Menu Izquierdo -> Tecnico

.. figure:: ../rest/static/img/menu_restfulAPI.png
    :alt: Lista de Usuarios
    :width: 30%

Resumen de Usuaurios que consumen el API

.. figure:: ../rest/static/img/restful_users.png
    :alt: Lista de Usuarioshost
    :width: 100%

Resumen de Tokens creados

.. figure:: ../rest/static/img/restful_tokens.png
    :alt: Lista de Tokens Creados
    :width: 100%
