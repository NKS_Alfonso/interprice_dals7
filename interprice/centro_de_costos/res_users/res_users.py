# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# __copyright__ = 'GVADETO'
# __author__ = 'Enrique Avendaño Trujillo'
# __email__ = 'enrique.avendano@gvadeto.com'
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _



class res_users(osv.osv):

    _inherit = 'res.users'
    _columns = {
        'centro_costo_id': fields.many2many('account.cost.center', string='Centros asignados',
                help='Selecciona los centros de costos que asignaras al usuario'),
        }

res_users()


class sale_order(osv.osv):

    def _get_default_warehouse(self, cr, uid, context=None):
        cr.execute("""SELECT id FROM stock_warehouse WHERE \
        stock_warehouse_type_id=(select id from stock_warehouse_type where code = 'fac') \
         AND centro_costo_id in (SELECT
        account_cost_center_id FROM account_cost_center_res_users_rel WHERE res_users_id=%s) """, [uid])
        warehouse = cr.fetchall()
        if not warehouse:
            return False
        return warehouse[0]

    def _get_domain_warehouse(self, cr, uid, context=None):
        cr.execute("""SELECT id FROM stock_warehouse WHERE \
        stock_warehouse_type_id=(select id from stock_warehouse_type where code = 'fac') \
         AND centro_costo_id in (SELECT
        account_cost_center_id FROM account_cost_center_res_users_rel WHERE res_users_id=%s) """, [uid])
        warehouse = cr.fetchall()
        if warehouse:
            lids = warehouse
        if not warehouse:
            return {'domain': {'warehouse_id': [('id', 'in', [])]}, 'value': {'warehouse_id': None}}
        return {'domain': {'warehouse_id': [('id', 'in', lids)]}}

    def onchange_warehouse_id(self, cr, uid, ids, warehouse_id, context=None):
        res = super(sale_order, self).onchange_warehouse_id(cr, uid, ids, warehouse_id, context)
        warehouse = self._get_domain_warehouse(cr, uid, context)
        if 'domain' in res:
            res['domain'].update(warehouse['domain'])
        else:
            res['domain'] = warehouse['domain']

        if 'value' in warehouse:
            if 'value' in res:
                res['value'].update(warehouse['value'])
            else:
                res['value'] = warehouse['value']
        return res

    _inherit = 'sale.order'

    _defaults = {
        'warehouse_id': _get_default_warehouse,
    }

sale_order()


class sale_order_line(osv.osv):
    _inherit = "sale.order.line"

    def product_id_change_with_wh(self, cr, uid, ids, pricelist, product, qty=0,
                                  uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                                  lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False,
                                  flag=False, warehouse_id=False, context=None):
        cr.execute("""SELECT id FROM stock_warehouse WHERE \
         stock_warehouse_type_id=(select id from stock_warehouse_type where code = 'fac') \
         AND centro_costo_id in (SELECT
        account_cost_center_id FROM account_cost_center_res_users_rel WHERE res_users_id=%s) """, [uid])
        warehouse = cr.fetchall()
        if not warehouse:
            raise osv.except_osv(_('Error!'), _('¡No tienes un almacén asignado!'))
        res = super(sale_order_line, self).product_id_change_with_wh(cr, uid, ids, pricelist, product, qty,
                                  uom, qty_uos, uos, name, partner_id,
                                  lang, update_tax, date_order, packaging, fiscal_position,
                                  flag, warehouse_id, context)
        return res

sale_order_line()


class PurchaseOrder(osv.osv):

    def onchage_nacional_internacional(self, cr, uid, ids,
                                       nacional_internacional,
                                       order_line, context=None
                                       ):
        value = super(PurchaseOrder, self).onchage_nacional_internacional(
            cr, uid, ids, nacional_internacional,
            order_line, context=context
        )
        cr.execute(
            """select spt.id from stock_picking_type spt
            inner join stock_warehouse sw on sw.id=spt.warehouse_id
            inner join account_cost_center acc on acc.id=sw.centro_costo_id
            where sw.stock_warehouse_type_id=(select id from stock_warehouse_type where code = 'fac') \
             and spt.code='incoming' and spt.warehouse_id in (
            SELECT id FROM stock_warehouse WHERE stock_warehouse_type_id=(select id from stock_warehouse_type where code = 'fac') \
             AND centro_costo_id in (SELECT
            account_cost_center_id FROM account_cost_center_res_users_rel WHERE res_users_id=%s)) """,
            [uid]
        )
        picking_type_id = cr.fetchall()
        if picking_type_id:
            if 'value' in value:
                value.update(
                    {
                        'domain': {
                            'picking_type_id': [('id', 'in', picking_type_id)],
                            'location_id': [('id', 'in', [])],
                            'centro_costo_id': [('id', 'in', [])]
                        }
                    }
                )
            value['value'].update(
                {'picking_type_id': 0,
                 'location_id': 0,
                 'centro_costo_id': 0
                 }
            )
        else:
            if 'value' in value:
                value.update(
                    {
                        'domain': {
                            'picking_type_id': [('id', 'in', [])],
                            'location_id': [('id', 'in', [])],
                            'centro_costo_id': [('id', 'in', [])]
                        }
                    }
                )
                value['value'].update(
                    {'picking_type_id': 0,
                     'location_id': 0,
                     'centro_costo_id': 0
                     }
                )
        return value

    def onchange_picking_type_id(self, cr, uid, ids,
                                 picking_type_id, context=None
                                 ):
        value = super(PurchaseOrder, self).onchange_picking_type_id(
            cr, uid, ids, picking_type_id, context=context
        )
        if picking_type_id:
            cr.execute(
                """select acc.id from account_cost_center acc
                inner join stock_warehouse sw on sw.centro_costo_id=acc.id
                inner join stock_picking_type spt on spt.warehouse_id=sw.id
                where sw.stock_warehouse_type_id=(select id from stock_warehouse_type where code = 'fac') \
                 and spt.code='incoming' and spt.id=%s """,
                [picking_type_id]
            )
            centro_costo_ids = cr.fetchall()
            if centro_costo_ids:
                lids = centro_costo_ids[0]
                if 'value' in value:
                    value['value'].update(
                        {'centro_costo_id': lids[0]}
                    )
        return value

    _inherit = 'purchase.order'
    _columns = {
        'picking_type_id': fields.many2one(
            'stock.picking.type', string='Deliver To',
            help="This will determine picking type of incoming shipment",
            required=True, readonly=True, states={'draft': [
                ('readonly', False)
            ]}
        ),
        'centro_costo_id': fields.many2one(
            'account.cost.center', 'Centro de Costo', ondelete="cascade",
            required=True, readonly=True, states={'draft': [
                ('readonly', False)
            ]}
        ),
    }
