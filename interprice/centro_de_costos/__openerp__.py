# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# __copyright__ = 'GVADETO'
# __author__ = 'Enrique Avendaño Trujillo'
# __email__ = 'enrique.avendano@gvadeto.com'
##############################################################################

{
    'name': 'Centro de costos para usuarios',
    'version': '1.1',
    'author': 'Enrique Avendaño Trujillo(GVadeto)',
    'category': 'Almacen',
    'description': """
        Configuración de los centros de costos en los usuarios.
         """,
    'website': 'https://',
    'license': 'AGPL-3',
    'depends': ['base', 'sale', 'purchase', 'sale_stock', 
                'l10n_mx_company_multi_address', 'cotizaciones', 'series',
                ],
    'init_xml': [],
    'demo_xml': [],
    'update_xml': [
                'res_users/res_users_view.xml',
                 ],
    'installable': True,
    'active': False,
}

