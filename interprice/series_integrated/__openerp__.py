# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': "Series Integrated Odoo",

    'author': "Copyright © 2018 TO-DO - All Rights Reserved",
    'website': 'http://www.to-do.mx',
    'category': 'Purchase / Sales',
    'version': '2.0',
    'depends': ['base', 'stock', 'reports','documents_inventory'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/stock_picking_series_view.xml',
        'views/button_transfer_new_view.xml',
        'views/confirm_dialog_view.xml',
        'views/product_view.xml',
        'views/document_inventory_view.xml'
    ],
    'application': True,
}
