# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import logging
import base64
import re
from olib.oCsv.oCsv import OCsv
from .series_conector_db import SeriesConectorDB
from openerp.tools.config import config
from openerp import api, fields, models, tools, _, conf
from openerp.exceptions import Warning, except_orm
from datetime import datetime
from time import time
_logger = logging.getLogger(__name__)
import csv
from io import BytesIO
from StringIO import StringIO
import sys
from datetime import datetime as dt
from dateutil import tz


_array = []
_prods = []
dataarray = []
data = []
cadinsertser = False
split_reception = False
remove_move_picking = []
_conn = None
kind = False

class StockPickingSeries(models.TransientModel):
    _name = 'stock.picking.series'

    series_picking = fields.Many2one('stock.picking')
    product_cant_visible = fields.Boolean('Many Products',computed='_get_many')
    upload_csv = fields.Binary('Upload (CSV)')
    upload_series_mode = fields.Selection([('screen', 'Generate on screen'),
                                           ('csv', 'Upload Csv File')],
                                          "Insert series")

    ingress_series = fields.One2many('ingress.series.display', 'series_ids',
                                     'Generate series')
    # error = fields.Boolean('Error', default = False)
    # message = fields.Text('Message')

    def convert_date_timezone(self, date):
        if date:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz(self.env.user.partner_id.tz)
            utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S')
            utc = utc.replace(tzinfo=from_zone)
            central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
            return central
        else:
            return ''

    @api.multi
    def _get_many(self):
        self.product_cant_visible = self._context.get('default_product_cant_visible',False)

    @api.multi
    def call_csv(self):
        time_init = time()
        csv_id = self.generated_csv()
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    def generated_csv(self):
        data_csv = []
        context = self.env.context
        active_id = [context.get('active_id')]
        response = self._get_data_generated_csv(picking_id=active_id)
        data_csv.append([
                    _('Document'),
                    _('Product_Id'),
                    _('SKU_Product'),
                    _('Product_name'),
                    _('Quantity'),
                    _('Series')
                ])

        for row in response:
            data = list(row)
            doc_origin = data[0]
            if not data[5]:
                data[5] = 'S/N'
                data_csv.append([data[0], data[1], data[2],
                                 data[3], data[4], data[5]])
            else:
                for i in range(int(data[4])):
                    data[4] = 1
                    data[5] = ''
                    data_csv.append([data[0], data[1], data[2],
                                     data[3], data[4], data[5]])
        if data_csv:
            _file = OCsv().csv_base64(data_csv)
            return self.env['r.download'].create(
                        vals={
                                'file_name': _('FormatSeries-{}.csv'.format(
                                                                   doc_origin
                                                                )),
                                'type_file': 'csv',
                                'file': _file,
                            }
                        )
        else:
            # self.error = True
            # self.message = _("No records found!")
            raise Warning(_("No records found!"))

    @api.multi
    def button_load_series_v2(self):
        series_list = []
        time_gral_init = time()
        context = self.env.context
        active_id = [context.get('active_id')]
        if not self.upload_series_mode:
            raise Warning(_('Select some valid option'))
        if active_id:
            global data
            data = self.get_data_sql(picking_id=active_id)
            global kind
            kind = self.determinate_kind(data[0])[0]
            if 'screen' in self.upload_series_mode:
                del _array[:]
                del _prods[:]
                del dataarray[:]
                global cadinsertser
                cadinsertser=''
                for i, line in enumerate(self.ingress_series):
                    data_line = [line.document_origin,
                                str(line.product_id),
                                line.product_sku,
                                line.product_name,
                                str(line.product_cant),
                                line.product_series]
                    result_line = self.validate(i, data_line, type_ln='screen')
                    if result_line:
                        series_list.append(result_line)
                result = self.proccess_data(series_list)
            if 'csv' in self.upload_series_mode:
                if self.upload_csv:
                    series_list = self.validate_csv(self.upload_csv)
                    #_logger.warning('----------------> TIEMPO CARGA DE ARCHIVO :{}'.format(time() - time_gral_init))
                    result = self.proccess_data(series_list[0])
                else:
                    raise Warning(_('You must choose a CSV File to process'))
        #_logger.warning('----------------> TIEMPO TOTAL TOTAL :{}'.format(time() - time_gral_init))
        return result

    @api.one
    def determinate_kind(self, data):
        kindd = False
        source_location = data.get('location_id',False)
        dest_location = data.get('location_dest_id',False)
        origin_returned = data.get('origin_returned_move',False)
        is_return = data.get('is_return',False)
        source_usage = 'internal' if self.env['stock.location'].search([('id','=',source_location)]).usage in ('internal','inventory','procurement','production','transit') else 'external'
        dest_usage = 'internal' if self.env['stock.location'].search([('id','=',dest_location)]).usage in ('internal','inventory','procurement','production','transit') else 'external'
        if source_usage=='external' and dest_usage=='internal':
            if origin_returned==None:
                if is_return:
                    kindd='client_return'
                else:
                    kindd='purchase'
            else:
                kindd='client_return'
        elif source_usage=='internal' and dest_usage=='external':
            if origin_returned==None:
                if is_return:
                    kindd='supplier_return'
                else:
                    kindd='sale'
            else:
                kindd='supplier_return'
        elif source_usage=='internal' and dest_usage=='internal':
            kindd = 'internal'
        else:
            raise Warning(_('This specific case is missing from the rules on the system, please check this with Continous Improvement Department'))
        return kindd
    @api.one
    def validate_csv(self, csvf):
        time_gral_initC = time()
        #_logger.warning('----------------> INICIA CARGA DE ARCHIVO :{}'.format(0))
        series_list = []
        data_file = base64.b64decode(csvf)
        bufferf = StringIO().truncate(0)
        bufferf = StringIO(data_file)
        data_csv = csv.reader(bufferf,dialect='excel')
        i=0
        #_logger.warning('----------------> TERMINA LECTURA DE ARCHIVO :{}'.format(time() - time_gral_initC))
        del _array[:]
        del _prods[:]
        del dataarray[:]
        global cadinsertser
        cadinsertser=''
        #_logger.warning('----------------> INICIA ITERACIONES DE ARCHIVO :{}'.format(time() - time_gral_initC))
        for i, line in enumerate(data_csv):
            result_line = self.validate(i, line)
            #_logger.warning('----------------> ITERACION {} :{}'.format(i,time() - time_gral_initC))
            if result_line:
                series_list.append(result_line)
        #_logger.warning('----------------> TERMINA ITERACIONES DE ARCHIVO :{}'.format(time() - time_gral_initC))
        return series_list
    def validate(self, i, line, type_ln=None):
        global cadinsertser
        cols = len(line)
        if 'Document' in line[0]:
            line = False
        # validate one line and if not data, so continue
        # if search data in the line, the line is incomplete
        elif cols == 1:
            if line[0] == '':
                line = False
            else:
                raise Warning(_('The row No {} of CSV file is incomplete please check').format(i+1))
        # We validate that rows dont have more records
        elif cols > 6:
            raise Warning(_('The CSV file have more columns of the expected please check'))
        # We validate that rows doesn't have fewer columns than expected
        elif cols < 6:
            raise Warning(_('The CSV file contains fewer columns than expected please check'))
        # we Validate that all rows have series
        elif line[5] == '' or line[5] == 'False' or line[5] is None or line[5] == False:
            raise Warning(_('The row No {} of document dont contain a serie valid, maybe this row is empty. Please Check').format(i+1))
        else:
            # We validate that all columns is complete
            for l in line:
                if l == '' or l == 'False' or l == '0':
                    raise Warning(_('The row No {} of document is incomplete. Please Ckeck').format(i+1))
            # we validate that the document is equal in the stock_picking
            if data[0].get('picking_origin') != line[0]:
                raise Warning(_('The document {} of CSV file does not correspond to this order. Please Check').format(line[0]))
            # We validate that the products is same in stock_picking
            _is = False
            unique_serie = False
            source_location = False
            dest_location = False
            move_id = False
            for code in data:
                if line[2]==code.get('default_code'):
                    _is = True
                    unique_serie=code.get('product_lot_uniq_ok')
                    source_location = code.get('location_id')
                    dest_location = code.get('location_dest_id')
                    break
            if not _is:
                raise Warning(_('The product {} in row {} it is not part of this order').format(line[2], i+1))
            if unique_serie and float(line[4])!=1.00:
                    raise Warning(_('The amount in row {} must be 1 because it is a product with series, please check').format(i+1))
            prod_id = line[1]
            if prod_id in _prods:
                element = _prods.index(prod_id)
                #_array[element][1] = _array[element][1]+"'"+line[5]+"'"+','
                _array[element][2] = int(_array[element][2])+1
                _array[element][3] = round(float(line[4]))
            else:
                _array.append([prod_id,"'"+line[5]+"'"+',',int(float(line[4])),round(float(line[4]),2),unique_serie,source_location,dest_location])
                _prods.append(prod_id)
            porcion = "({},'{}',{})".format(prod_id,line[5].strip().upper(),line[4])+","
            cadinsertser += porcion
        return line
    @api.multi
    def proccess_data(self, data_csv):
        global _conn
        _conn = SeriesConectorDB(dbname=self.env.cr.dbname)
        global cadinsertser
        cadinsertser=cadinsertser[:-1]
        time_gral_init2 = time()
        #_logger.warning('----------------> INICIA PROCESS :{}'.format(time() - time_gral_init2))
        _conn.execute("""
            DROP TABLE IF EXISTS temp_insert_series;
            CREATE TEMPORARY TABLE
                temp_insert_series (
                    prod_id integer,
                    serie character varying,
                    cantidad integer,
                    serie_id integer,
                    location_id integer
                );
            INSERT INTO
                temp_insert_series (
                    prod_id,
                    serie,
                    cantidad
                )
            VALUES """+cadinsertser+""";""")
        # look out for duplicated series in csv file using the work arrays
        _conn.execute("""
            SELECT
                serie,
                prod_id,
                count(*)
            FROM
                temp_insert_series
            GROUP BY
                serie,
                prod_id
            HAVING
                count(*)>1;""")
        seen_twice = _conn.fetchall()
        if len(seen_twice)>0:
            cad = ''
            for repeated in seen_twice:
                cad += "'"+repeated[0]+"'"+'\n'
            raise Warning(_('There are repeated series {}').format(cad))

        #  We iterate the data to validate some feactures of stock_picking
        del dataarray[:]
        array_split_picking = []
        global remove_move_picking
        del remove_move_picking[:]
        split_reception = False
        #get the current picking to work with it
        current_picking=self.env['stock.picking'].search([('id','=',data[0].get('picking_id'))]) #VERIFY
        for picking in data:
            # We validate that it has not been transferred before
            # in stock_picking
            # Obtain the real amount of product to transfer
            texto = """
                SELECT
                    coalesce(sum(cantidad)::numeric,0)
                FROM
                    temp_insert_series
                WHERE
                    prod_id = """+str(picking.get('product_p_id'))
            _conn.execute(texto)
            result = _conn.fetchall()
            #QUERY TO GET QUANTITY ALREADY MOVED ON PREVIUS PICKINGS
            if kind=='purchase':
                if picking.get('purchase_line_id')!=None:
                    texto = """
                        SELECT
                            coalesce(sum(sm.product_uom_qty),0.00)
                        FROM
                            stock_move sm
                        WHERE
                            sm.purchase_line_id="""+str(picking.get('purchase_line_id'))+"""
                            AND state='done';"""
                    _conn.execute(texto)
                    result2 = _conn.fetchall()
                    qty_moved = result2[0][0]
                else:
                    qty_moved = 0
            else:
                qty_moved = 0
            dataarray.append([picking.get('product_p_id'),picking.get('ultcost'),picking.get('location_dest_id'),picking.get('company_id'), \
                            picking.get('move_id'),picking.get('location_id'),picking.get('uom_id'),picking.get('picking_id'), \
                            picking.get('picking_name'),picking.get('picking_origin'),picking.get('move_product_qty'),picking.get('warehouse_dest_id'), \
                            picking.get('product_t_id'),picking.get('move_price_unit'),float(result[0][0]),0,float(qty_moved), \
                            float(picking.get('pol_qty',0.00)-qty_moved),picking.get('pol_qty',0.00),picking.get('procurement_id'),picking.get('sale_line_id'), \
                            picking.get('sol_qty',0.00),picking.get('warehouse_origin_id'),picking.get('document_inventory_id'),picking.get('is_return')])
            if picking.get('picking_state') == 'done':
                raise Warning(_('ERROR: {} is already transferred').format(picking.get('picking_id')))
            # We validate product is active
            if not picking.get('product_active'):
                raise Warning(_('The product {} is inactive. Please Check').format(picking.get('default_code')))
            # We validate if is series or S/N,
            prodtovalidate=picking.get('product_p_id')
            #Validate if series for the product were provided on the csv and the quantity is correct
            if str(prodtovalidate) in _prods:
                element = _prods.index(str(prodtovalidate))
                if kind=='purchase':
                    if _array[element][2]>(dataarray[-1][17] if dataarray[-1][17]>0 else dataarray[-1][10]):
                        raise Warning(_('The quantity of series provided for the product {} does not match the quantity of the document').format(picking.get('default_code')+' '+picking.get('product_name')))
                    to_move = dataarray[-1][17] if dataarray[-1][17]>0 else dataarray[-1][10]
                elif kind=='sale':
                    if _array[element][2]>(dataarray[-1][21] if dataarray[-1][21]>0 else dataarray[-1][10])-dataarray[-1][16]:
                        raise Warning(_('The quantity of series provided for the product {} does not match the quantity of the document').format(picking.get('default_code')+' '+picking.get('product_name')))
                    to_move = (dataarray[-1][21] if dataarray[-1][21]>0 else dataarray[-1][10])-dataarray[-1][16]
                elif kind in ('client_return','supplier_return','internal'):
                    if _array[element][2]!=dataarray[-1][10]:
                        raise Warning(_('The quantity of series provided for the product {} does not match the quantity of the document').format(picking.get('default_code')+' '+picking.get('product_name')))
                    to_move = dataarray[-1][10]
                #Obtain the product qty to move without this movement
                if _array[element][2]<to_move:
                    remaining = to_move - _array[element][2]
                    dataarray[-1][15] = remaining
                    split_reception = True
            else:
                #Series were not provided on the csv file or screen for this product
                split_reception = True
                dataarray[-1][15]=dataarray[-1][17]
                remove_move_picking.append(dataarray[-1][4])
        if split_reception:
            if kind == 'purchase' and dataarray[-1][23]==None:
                new_message = self.env['confirm.dialog'].create({'text':_('This is a partial reception, Do you wanna proccess?')})
                return {
                    'type': 'ir.actions.act_window',
                    'name': _('Confirm Dialog'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'confirm.dialog',
                    'res_id' : new_message.id,
                    'target': 'new',
                    'context': {'method_execute':'final_process','split_reception_ctx':split_reception},
                }
            else:
                raise Warning(_('This is a partial reception/delivery/transfer, please check the provided series and try again'))
        self.final_process()

    @api.multi
    def final_process(self):
        #time_gral_init3 = time()
        #_logger.warning('----------------> INICIA FINAL PROCESS :{}'.format(time() - time_gral_init3))
        sr_ctx = self._context.get('split_reception_ctx',False)
        current_picking=self.env['stock.picking'].search([('id','=',dataarray[0][7])])
        for i in range(len(_array)):
            _array[i][1]=_array[i][1][:-1]
        if kind=='purchase':
            result = self._get_data_series_exist()
        if kind in ('sale','internal'):
            result = self._get_data_series_valid_to_move()
        if kind in ('client_return','supplier_return'):
            result = self._series_valid_to_refund()
        try:
            #insert series
            movs_to_done = []
            time_sql_inserts = time()
            #start iterations by series block
            for element in _array:
                series = element[1].split(',')
                posiciond = [col[0] for col in dataarray].index(int(element[0]))
                cadena_stock_pack_operation = ''
                #IF PICKING IS PURCHASE KIND WE NEED TO CREATE ALL THE SERIES
                if kind=='purchase':
                    cadena_stock_production_lot = """
                        INSERT INTO
                            stock_production_lot (
                                create_date,
                                name,
                                create_uid,
                                write_uid,
                                write_date,
                                ref,
                                product_id,
                                available,
                                last_location_id
                            )
                        SELECT
                            now() at time zone 'UTC',
                            serie,
                            """+str(self.env.uid)+""",
                            """+str(self.env.uid)+""",
                            now() at time zone 'UTC',
                            serie,
                            prod_id,
                            true,
                            """+str(dataarray[posiciond][5])+"""
                        FROM
                            temp_insert_series
                        WHERE
                            prod_id="""+str(element[0])+""";"""
                #IF THE PICKING IS NOT A PURCHASE KIND WE NEED TO UPDATE THE LOCATION FOR THE SERIES
                elif kind in ('sale','client_return','supplier_return','internal'):
                    cadena_stock_production_lot = """
                        UPDATE
                            stock_production_lot
                        SET
                            last_location_id="""+str(dataarray[posiciond][5])+"""
                        WHERE
                            id in (
                                SELECT
                                    serie_id
                                FROM
                                    temp_insert_series
                                WHERE
                                    prod_id="""+str(element[0])+"""
                            );"""
                #NOW WE EVALUATE IF THE PRODUCT NEEDS UNIQUE SERIES OR NOT
                if element[4]==True:
                    #UNIQUE SERIES AND PURCHASE KIND INSERT THE QUANTS FOR THE SERIES
                    if kind=='purchase':
                        cadena_stock_quant = """
                            INSERT INTO
                                stock_quant (
                                    create_date,
                                    qty,cost,
                                    lot_id,
                                    create_uid,
                                    location_id,
                                    company_id,
                                    write_date,
                                    write_uid,
                                    product_id,
                                    in_date
                                )
                            SELECT
                                now() at time zone 'UTC',
                                t2.cantidad,
                                """+str(dataarray[posiciond][1])+""",
                                t1.id,
                                """+str(self.env.uid)+""",
                                """+str(dataarray[posiciond][2])+""",
                                """+str(dataarray[posiciond][3])+""",
                                now() at time zone 'UTC',
                                """+str(self.env.uid)+""",
                                """+str(dataarray[posiciond][0])+""",
                                now()
                            FROM
                                stock_production_lot t1
                            INNER JOIN
                                temp_insert_series t2 ON t2.serie=t1.name AND t2.prod_id=t1.product_id
                            WHERE
                                t2.prod_id="""+str(element[0])+""";"""
                    #UNIQUE SERIES AND NOT PURCHASE KIND CREATE A TEMPORARY TABLE WITH QUANTS THAT BELONGS TO THE SERIES AND UPDATE LOCATION TO THOSE QUANTS
                    elif kind in ('sale','client_return','supplier_return','internal'):
                        cadena_stock_quant = """
                            DROP TABLE IF EXISTS temp_quants;
                            CREATE TEMPORARY TABLE
                                temp_quants (
                                    id integer,
                                    qty double precision,
                                    lot_id integer,
                                    reservation_id integer,
                                    location_id integer,
                                    product_id integer
                                );
                            INSERT INTO
                                temp_quants
                            SELECT
                                sq.id,
                                sq.qty,
                                sq.lot_id,
                                sq.reservation_id,
                                sq.location_id,
                                sq.product_id
                            FROM
                                stock_quant sq
                            INNER JOIN
                                stock_production_lot spl ON spl.id = sq.lot_id
                            INNER JOIN
                                temp_insert_series tis ON tis.serie = spl.name
                            WHERE
                                tis.prod_id = """+str(element[0])+""";"""
                        cadena_stock_quant += """
                            UPDATE
                                stock_quant
                            SET
                                location_id="""+str(dataarray[posiciond][2])+"""
                            WHERE
                                id in (
                                    SELECT id
                                    FROM temp_quants
                                );"""
                    #INSERT STOCK PACK OPERATION
                    cadena_stock_pack_operation += """
                        DELETE FROM
                            stock_pack_operation
                        WHERE
                            picking_id="""+str(dataarray[posiciond][7])+"""
                            AND product_id="""+str(element[0])+""";
                        INSERT INTO
                            stock_pack_operation (
                                create_date,
                                write_uid,
                                product_qty,
                                lot_id,
                                location_id,
                                create_uid,
                                qty_done,
                                write_date,
                                date,
                                product_id,
                                product_uom_id,
                                location_dest_id,
                                processed,
                                picking_id
                            )
                        SELECT
                            now() at time zone 'UTC',
                            """+str(self.env.uid)+""",
                            t2.cantidad,
                            t1.id,
                            """+str(dataarray[posiciond][5])+""",
                            """+str(self.env.uid)+""",
                            t2.cantidad,
                            now() at time zone 'UTC',
                            now(),
                            """+str(dataarray[posiciond][0])+""",
                            """+str(dataarray[posiciond][6])+""",
                            """+str(dataarray[posiciond][2])+""",
                            true,
                            """+str(dataarray[posiciond][7])+"""
                        FROM
                            stock_production_lot t1
                        INNER JOIN
                            temp_insert_series t2 ON t2.serie=t1.name AND t2.prod_id=t1.product_id
                        WHERE
                            t2.prod_id="""+str(element[0])+""";"""
                    # CREATE RELATIONS BETWEEN MOVED QUANTS AND PICKING MOVE
                    cadena_stock_quant_move_rel = """
                        INSERT INTO
                            stock_quant_move_rel (
                                quant_id,
                                move_id
                            )
                        SELECT
                            sq.id,
                            """+str(dataarray[posiciond][4])+"""
                        FROM
                            stock_quant sq
                        INNER JOIN
                            stock_production_lot spl ON spl.id=sq.lot_id
                        INNER JOIN
                            temp_insert_series tis ON tis.serie=spl.name
                        WHERE
                            spl.product_id="""+str(element[0])+"""; """
                #NOT UNIQUE SERIES NEEDED
                else:
                    #IF PICKING PURCHASE KIND JUST INSERT THE QUANT
                    if kind == 'purchase':
                        cadena_stock_quant = """
                            INSERT INTO
                                stock_quant (
                                    create_date,
                                    qty,
                                    cost,
                                    lot_id,
                                    create_uid,
                                    location_id,
                                    company_id,
                                    write_date,
                                    write_uid,
                                    product_id,
                                    in_date
                                )
                            SELECT
                                now() at time zone 'UTC',
                                t2.cantidad,
                                """+str(dataarray[posiciond][1])+""",
                                null,
                                """+str(self.env.uid)+""",
                                """+str(dataarray[posiciond][2])+""",
                                """+str(dataarray[posiciond][3])+""",
                                now() at time zone 'UTC',
                                """+str(self.env.uid)+""",
                                """+str(dataarray[posiciond][0])+""",
                                now()
                            FROM
                                temp_insert_series t2
                            WHERE
                                t2.prod_id="""+str(element[0])+"""; """
                    else:
                        #VALIDATE IF EXISTS RESERVED QUANT TO THIS PICKING MOVE
                        _conn.execute("""
                            SELECT
                                id
                            FROM
                                stock_quant
                            WHERE
                                reservation_id="""+str(dataarray[posiciond][4])+""";
                                      """)
                        result = _conn.fetchall()
                        #IF NO EXISTS QUANTS RESERVED WE DO THE RESERVATION TO MOVE THE QUANT
                        if len(result)==0:
                            current_picking.sudo().do_unreserve()
                            current_picking.sudo().rereserve_pick()
                        #NOW WE CAN MOVE THE QUANT
                        cadena_stock_quant = """
                            UPDATE
                                stock_quant
                            SET
                                location_id = """+str(dataarray[posiciond][2])+""",
                                write_date = now() at time zone 'UTC',
                                write_uid = """+str(self.env.uid)+"""
                            WHERE
                                reservation_id = """+str(dataarray[posiciond][4])+""";
                        """
                    #INSERT PACK OPERATION FOR ALL CASES
                    cadena_stock_pack_operation = """
                        DELETE FROM
                            stock_pack_operation
                        WHERE
                            picking_id="""+str(dataarray[posiciond][7])+"""
                            AND product_id="""+str(element[0])+""";
                        INSERT INTO
                            stock_pack_operation (
                                create_date,
                                write_uid,
                                product_qty,
                                lot_id,
                                location_id,
                                create_uid,
                                qty_done,
                                write_date,
                                date,
                                product_id,
                                product_uom_id,
                                location_dest_id,
                                processed,
                                picking_id
                            )
                        SELECT
                            now() at time zone 'UTC',
                            """+str(self.env.uid)+""",
                            t2.cantidad,null,
                            """+str(dataarray[posiciond][5])+""",
                            """+str(self.env.uid)+""",
                            t2.cantidad,
                            now() at time zone 'UTC',
                            now(),
                            """+str(dataarray[posiciond][0])+""",
                            """+str(dataarray[posiciond][6])+""",
                            """+str(dataarray[posiciond][2])+""",
                            true,
                            """+str(dataarray[posiciond][7])+"""
                        FROM
                            temp_insert_series t2
                        WHERE
                            t2.prod_id="""+str(element[0])+""";"""
                    #CREATE RELATION BETWEEN QUANT AND PICKING MOVE
                    if kind == 'purchase':
                        cadena_stock_quant_move_rel = """
                            INSERT INTO
                                stock_quant_move_rel (
                                    quant_id,
                                    move_id
                                )
                            SELECT
                                id,
                                """+str(dataarray[posiciond][4])+"""
                            FROM
                                stock_quant
                            WHERE
                                product_id="""+str(element[0])+"""
                                AND qty="""+str(element[3])+"""
                                AND cost="""+str(dataarray[posiciond][1])+"""
                                AND location_id="""+str(dataarray[posiciond][2])+"""
                                AND company_id="""+str(dataarray[posiciond][3])+"""
                                AND create_uid="""+str(self.env.uid)+"""
                                AND id not in (
                                    SELECT
                                        quant_id
                                    FROM
                                        stock_quant_move_rel
                                ); """
                    else:
                        cadena_stock_quant_move_rel = """
                            INSERT INTO
                                stock_quant_move_rel (
                                    quant_id,
                                    move_id
                                )
                            SELECT
                                id,
                                """+str(dataarray[posiciond][4])+"""
                            FROM
                                stock_quant
                            WHERE
                                reservation_id="""+str(dataarray[posiciond][4])+""";
                        """
                #destino posicion 2
                #origen posicion 5
                #WH destino 11
                #WH origen 22
                # INSERT RECORDS IN _HISTORY_STOCK TO ANALITIC INVENTORY REPORT
                #FIRST VALIDATE IF THERE ARE RECORDS IN _HISTORY_STOCK FOR THIS LOCATION, WAREHOUSE AND PRODUCT
                validate_hs = """
                    SELECT
                        coalesce(existencia_posterior,0)
                    FROM
                        _history_stock
                    WHERE
                        product_id="""+str(dataarray[posiciond][0])+"""
                        AND warehouse_id="""+str(dataarray[posiciond][11] if kind in ('purchase','client_return','internal') else dataarray[posiciond][22])+"""
                        AND location_id="""+str(dataarray[posiciond][2] if kind in ('purchase','client_return','internal') else dataarray[posiciond][5])+"""
                    ORDER BY
                        create_date desc
                    LIMIT 1;
                """
                _conn.execute(validate_hs)
                exists_hs=_conn.fetchall()
                if len(exists_hs)>0:
                    cadena_history_stock = """
                        INSERT INTO
                            _history_stock (
                                picking_origin,
                                create_uid,
                                document_origin,
                                create_date,
                                product_id,
                                write_uid,
                                product_qty_move,
                                write_date,
                                picking_id,
                                existencia_anterior,
                                warehouse_id,
                                location_id,
                                existencia_posterior
                            )
                        SELECT
                            '"""+str(dataarray[posiciond][8])+"""',
                            """+str(self.env.uid)+""",
                            '"""+str(dataarray[posiciond][9])+"""',
                            now() at time zone 'UTC',
                            """+str(dataarray[posiciond][0])+""",
                            """+str(self.env.uid)+""",
                            """+str(dataarray[posiciond][14] if kind in ('purchase','client_return','internal') else dataarray[posiciond][14]*-1)+""",
                            now() at time zone 'UTC',
                            """+str(dataarray[posiciond][7])+""",
                            coalesce(existencia_posterior,0),
                            """+str(dataarray[posiciond][11] if kind in ('purchase','client_return','internal') else dataarray[posiciond][22])+""",
                            """+str(dataarray[posiciond][2] if kind in ('purchase','client_return','internal') else dataarray[posiciond][5])+""",
                            coalesce(existencia_posterior,0)+"""+str(dataarray[posiciond][14] if kind in ('purchase','client_return','internal') else dataarray[posiciond][14]*-1)+"""
                        FROM
                            _history_stock
                        WHERE
                            product_id="""+str(dataarray[posiciond][0])+"""
                            AND warehouse_id="""+str(dataarray[posiciond][11] if kind in ('purchase','client_return','internal') else dataarray[posiciond][22])+"""
                            AND location_id="""+str(dataarray[posiciond][2] if kind in ('purchase','client_return','internal') else dataarray[posiciond][5])+"""
                        ORDER BY
                            create_date desc
                        LIMIT 1; """
                    #WHEN PICKING KIND IS INTERNAL WE NEED AN EXTRA RECORD IN _HISTORY_STOCK THE COMPLEMENTARY FOR THE FIRST ONE BECAUSE THIS KIND AFFECTS 2 INTERNAL LOCATIONS
                    if kind=='internal':
                        cadena_history_stock += """
                            INSERT INTO
                                _history_stock (
                                    picking_origin,
                                    create_uid,
                                    document_origin,
                                    create_date,
                                    product_id,
                                    write_uid,
                                    product_qty_move,
                                    write_date,
                                    picking_id,
                                    existencia_anterior,
                                    warehouse_id,
                                    location_id,
                                    existencia_posterior
                                )
                            SELECT
                                '"""+str(dataarray[posiciond][8])+"""',
                                """+str(self.env.uid)+""",
                                '"""+str(dataarray[posiciond][9])+"""',
                                now() at time zone 'UTC',
                                """+str(dataarray[posiciond][0])+""",
                                """+str(self.env.uid)+""",
                                """+str(dataarray[posiciond][14]*-1)+""",
                                now() at time zone 'UTC',
                                """+str(dataarray[posiciond][7])+""",
                                coalesce(existencia_posterior,0),
                                """+str(dataarray[posiciond][22])+""",
                                """+str(dataarray[posiciond][5])+""",
                                coalesce(existencia_posterior,0)+"""+str(dataarray[posiciond][14]*-1)+"""
                            FROM
                                _history_stock
                            WHERE
                                product_id="""+str(dataarray[posiciond][0])+"""
                                AND warehouse_id="""+str(dataarray[posiciond][22])+"""
                                AND location_id="""+str(dataarray[posiciond][5])+"""
                            ORDER BY
                                create_date desc
                            LIMIT 1;
                        """
                else:
                    #DOESNT EXISTS RECORD IN _HISTORY_STOCK FOR THIS PRODUCT, LOCATION AND WAREHOUSE, SO WE INSERT DIRECTLY THE RECORD(S)
                    cadena_history_stock = """
                        INSERT INTO
                            _history_stock (
                                picking_origin,
                                create_uid,
                                document_origin,
                                create_date,
                                product_id,
                                write_uid,
                                product_qty_move,
                                write_date,
                                picking_id,
                                existencia_anterior,
                                warehouse_id,
                                location_id,
                                existencia_posterior
                            )
                        VALUES (
                            '"""+str(dataarray[posiciond][8])+"""',
                            """+str(self.env.uid)+""",
                            '"""+str(dataarray[posiciond][9])+"""',
                            now() at time zone 'UTC',
                            """+str(dataarray[posiciond][0])+""",
                            """+str(self.env.uid)+""",
                            """+str(dataarray[posiciond][14] if kind in ('purchase','client_return','internal') else dataarray[posiciond][14]*-1)+""",
                            now() at time zone 'UTC',
                            """+str(dataarray[posiciond][7])+""",
                            0,
                            """+str(dataarray[posiciond][11] if kind in ('purchase','client_return','internal') else dataarray[posiciond][22])+""",
                            """+str(dataarray[posiciond][2] if kind in ('purchase','client_return','internal') else dataarray[posiciond][5])+""",
                            0+"""+str(dataarray[posiciond][14] if kind in ('purchase','client_return','internal') else dataarray[posiciond][14]*-1)+"""
                        ); """
                    #WHEN PICKING KIND IS INTERNAL WE NEED AN EXTRA RECORD IN _HISTORY_STOCK THE COMPLEMENTARY FOR THE FIRST ONE BECAUSE THIS KIND AFFECTS 2 INTERNAL LOCATIONS
                    if kind=='internal':
                        cadena_history_stock += """
                            INSERT INTO
                                _history_stock (
                                    picking_origin,
                                    create_uid,
                                    document_origin,
                                    create_date,
                                    product_id,
                                    write_uid,
                                    product_qty_move,
                                    write_date,
                                    picking_id,
                                    existencia_anterior,
                                    warehouse_id,
                                    location_id,
                                    existencia_posterior
                                )
                            VALUES(
                                '"""+str(dataarray[posiciond][8])+"""',
                                """+str(self.env.uid)+""",
                                '"""+str(dataarray[posiciond][9])+"""',
                                now() at time zone 'UTC',
                                """+str(dataarray[posiciond][0])+""",
                                """+str(self.env.uid)+""",
                                """+str(dataarray[posiciond][14]*-1)+""",
                                now() at time zone 'UTC',
                                """+str(dataarray[posiciond][7])+""",
                                0,
                                """+str(dataarray[posiciond][22])+""",
                                """+str(dataarray[posiciond][5])+""",
                                0+"""+str(dataarray[posiciond][14]*-1)+"""
                            );
                        """
                #WE UPDATE THE QUANTITIES AND STATE FOR THE PICKING MOVE PURCHASE KIND
                if kind=='purchase':
                    cadena_stock_move = """
                        UPDATE
                            stock_move
                        SET
                            state = 'done',
                            product_uos_qty="""+str(dataarray[posiciond][14])+""",
                            product_uom_qty="""+str(dataarray[posiciond][14])+""",
                            product_qty="""+str(dataarray[posiciond][14])+"""
                        WHERE
                            id = """+str(dataarray[posiciond][4])+""";"""
                else:
                    cadena_stock_move = ''
                    movs_to_done.append(dataarray[posiciond][4])
                if element[4]==True:
                    cadena_query = cadena_stock_production_lot+cadena_stock_pack_operation+cadena_stock_quant+cadena_stock_quant_move_rel+cadena_history_stock+cadena_stock_move
                else:
                    cadena_query = cadena_stock_pack_operation+cadena_stock_quant+cadena_stock_quant_move_rel+cadena_history_stock+cadena_stock_move
                cadena_query = cadena_query.replace("None","null")
                #EXECUTE ALL THE PREPARED QUERYS
                _conn.execute(cadena_query)
                #FOR PICKING KIND PURCHASE WE NEED TO EVALUATE IF COSTS CHANGE AND DO THE UPDATE
                if kind=='purchase':
                    prodtemp = self.env['product.template'].search([('id','=',dataarray[posiciond][12])])
                    if prodtemp.cost_method=='average' and prodtemp.standard_price!=dataarray[posiciond][13] and dataarray[posiciond][13]!=0.00:
                        product_tmpl = self.pool.get('product.template')
                        product_tmpl.do_change_standard_price(
                            self._cr,
                            self.env.uid,
                            [dataarray[posiciond][12]],
                            dataarray[posiciond][13],
                            self.env.context
                        )
                #AFTER THAT WE NEED TO REMOVE ALL THE RESERVATIONS FOR THIS MOVE AND
                #ALSO OBTAIN THOSE QUANTS WE MOVED THAT WERE RESERVED FOR OTHER MOVES AND
                #SUPPLY NEW ONES FOR THOSE
                elif kind in ('sale','client_return','supplier_return','internal'):
                    if element[4]==True:
                        query = """
                            UPDATE
                                stock_quant
                            SET
                                reservation_id = null
                            WHERE
                                reservation_id = """+str(dataarray[posiciond][4])+""";
                            SELECT
                                *
                            FROM
                                temp_quants
                            WHERE
                                reservation_id notnull
                                and reservation_id!="""+str(dataarray[posiciond][4])+""";"""
                        _conn.execute(query)
                        quantstorepo = _conn.fetchall()
                        for quant in quantstorepo:
                            query = """
                                UPDATE
                                    stock_quant
                                SET
                                    reservation_id="""+str(quant[3])+"""
                                WHERE
                                    id = (SELECT id
                                          FROM
                                            stock_quant
                                          WHERE
                                            reservation_id isnull
                                            AND location_id="""+str(dataarray[posiciond][5])+"""
                                            AND product_id="""+str(dataarray[posiciond][0])+"""
                                          ORDER BY
                                            create_date,
                                            lot_id
                                          LIMIT 1
                                    );
                                UPDATE
                                    stock_quant
                                SET
                                    reservation_id = null
                                WHERE
                                    id="""+str(quant[0])+""";"""
                            _conn.execute(query)
                    else:
                        query = """
                            UPDATE
                                stock_quant
                            SET
                                reservation_id = null
                            WHERE
                                reservation_id = """+str(dataarray[posiciond][4])+""";"""
                        _conn.execute(query)
                    #CHANGE STATE TO DONE FOR THE MOVE NOT PURCHASE KIND
                    query = """
                        UPDATE
                            stock_move
                        SET
                            state='done'
                        WHERE
                            picking_id = """+str(dataarray[posiciond][7])+""";"""
                    _conn.execute(query)
            # We verify if it was partial reception and it is necessary to create a new picking
            if sr_ctx:
                #create the new picking based on the origin
                if kind=='purchase':
                    origin_doc = self.env['purchase.order'].search([('name','=',current_picking.origin)])
                    np_created=origin_doc.action_picking_create()
                    new_picking = self.env['stock.picking'].search([('id','=',np_created)])
                    for move in new_picking.move_lines:
                        remaining = 0.0
                        texto = """
                            SELECT
                                coalesce(sum(sm.product_uom_qty),0.00)
                            FROM
                                stock_move sm
                            WHERE
                                sm.purchase_line_id="""+str(move.purchase_line_id.id)+"""
                                AND state='done';"""
                        _conn.execute(texto)
                        moved = _conn.fetchall()
                        remaining = move.product_uom_qty - moved[0][0]
                        sf_var = False
                        for old_move in current_picking.move_lines:
                            if move.product_id.id==old_move.product_id.id:
                                sf_var = old_move.id
                                break
                        if remaining > 0:
                            if sf_var:
                                move.write({'split_from':old_move.id,'product_uos_qty':remaining,'product_uom_qty':remaining})
                            else:
                                move.write({'product_uos_qty':remaining,'product_uom_qty':remaining})
                        else:
                            move.write({'state':'draft'})
                            move.unlink()
                    obj_mov = self.env['stock.move']
                    ids_to_remove = "("+','.join(str(e) for e in remove_move_picking)+")"
                    if len(remove_move_picking)>0:
                        self.env.cr.execute("""
                            DELETE FROM
                                stock_move
                            WHERE
                                id in {}""".format(ids_to_remove))
                    # else:
                    #     raise Warning(_('There was a problem during the picking split, please contact Help Desk'))
                    #current_picking.write({'state':'done'})
                    new_picking.write({'backorder_id':current_picking.id})
                    new_picking.action_confirm()
                    self.env.cr.execute("""
                        UPDATE
                            stock_picking
                        SET
                            state = 'done',
                            date_done = now() at time zone 'UTC'
                        WHERE
                            id = """+str(current_picking.id)+""";""")
                # BEGINNING TO DO SPLIT ON SALES (I LET THIS CODE JUST IN CASE)
                # if current_picking.picking_type_id.inventory_treatment=='sale':
                #     query_update_move = ''
                #     #CREATE JSON WITH THE VALUES TO CREATE THE NEW PICKING
                #     raise UserWarning('s')
                #     vals = {
                #         'origin' : current_picking.origin,
                #         'partner_id' : current_picking.partner_id.id,
                #         'priority' : current_picking.priority,
                #         'backorder_id' : current_picking.id,
                #         'picking_type_id' : current_picking.picking_type_id.id,
                #         'move_type' : current_picking.move_type,
                #         'company_id' : current_picking.company_id.id,
                #         'state' : 'assigned',
                #         'min_date' : current_picking.min_date,
                #         'date' : current_picking.date,
                #         'recompute_pack_op' : current_picking.recompute_pack_op,
                #         'max_date' : current_picking.max_date,
                #         'group_id' : current_picking.group_id.id,
                #         'invoice_state' : current_picking.invoice_state,
                #         'reception_to_invoice' : False,
                #         'origen_nacional_internacional' : current_picking.origen_nacional_internacional,
                #         'carrier_id' : current_picking.carrier_id.id,
                #         'weight_uom_id' : current_picking.weight_uom_id.id,
                #     }
                #     # Create the new picking
                #     new_picking = self.env['stock.picking'].create(vals)
                #     # Assign the moves that no had changes on the current picking to the new picking
                #     if cadena_move_picking!='':
                #         query = """
                #             UPDATE
                #                 stock_move
                #             SET
                #                 picking_id = {}
                #             WHERE
                #                 id in ({}); """.format(new_picking.id,cadena_move_picking[:-1])
                #     # Do the iterations to create the new moves with the remaining qtys
                #     if len(array_split_picking)>0:
                #         query += """
                #             INSERT INTO
                #                 stock_move (
                #                     origin,
                #                     product_uos_qty,
                #                     create_date,
                #                     product_uom,
                #                     price_unit,
                #                     product_uom_qty,
                #                     company_id,
                #                     date,
                #                     product_qty,
                #                     product_uos,
                #                     location_id,
                #                     priority,
                #                     picking_type_id,
                #                     partner_id,
                #                     state,
                #                     date_expected,
                #                     procurement_id,
                #                     name,
                #                     create_uid,
                #                     warehouse_id,
                #                     partially_available,
                #                     propagate,
                #                     procure_method,
                #                     write_uid,
                #                     group_id,
                #                     product_id,
                #                     split_from,
                #                     picking_id,
                #                     location_dest_id,
                #                     write_date,
                #                     rule_id,
                #                     invoice_state,
                #                     weight_uom_id
                #                 )
                #             VALUES """
                #     for element in array_split_picking:
                #         old_move = self.env['stock.move'].search([('id','=',element[0])])
                #         query += """
                #             ('{0}',{1},{27},{3},{4},{1},{5},{27},{1},{6},{7},
                #             '{8}',{9},{10},'assigned','{11}',{12},'{13}',{14},
                #             {15},{16},{17},'{18}',{14},{19},{20},{21},{22},
                #             {23},{27},{24},'{25}',{26}),
                #             """.format(old_move.origin,element[1],old_move.move_dest_id.id,old_move.product_uom.id,old_move.price_unit,\
                #                         old_move.company_id.id,old_move.product_uos.id,old_move.location_id.id,old_move.priority,\
                #                         old_move.picking_type_id.id,old_move.partner_id.id,old_move.date_expected,old_move.procurement_id.id,\
                #                         old_move.name,self.env.uid,old_move.warehouse_id.id,old_move.partially_available,old_move.propagate,\
                #                         old_move.procure_method,old_move.group_id.id,old_move.product_id.id,old_move.id,new_picking.id,\
                #                         old_move.location_dest_id.id,old_move.rule_id.id,old_move.invoice_state,old_move.weight_uom_id.id,"now() at time zone 'UTC'")
                #         query_update_move += """
                #             UPDATE
                #                 stock_move
                #             SET
                #                 product_uos_qty = product_uos_qty-{0},
                #                 product_uom_qty = product_uom_qty-{0},
                #                 product_qty = product_qty-{0}
                #             WHERE
                #                 id = {1};""".format(element[1],old_move.id)
                #     if len(array_split_picking)>0:
                #         query = query[:-1]+'; '
                #     query += query_update_move + """
                #         UPDATE
                #             stock_picking
                #         SET
                #             state = 'done'
                #         WHERE
                #             id = {0};
                #         UPDATE
                #             stock_move
                #         SET
                #             state = 'done'
                #         WHERE
                #             picking_id = {0} {1}""".format(str(current_picking.id),'AND id NOT IN ('+cadena_move_picking[:-1]+')' if len(cadena_move_picking)>0 else '')
                #     _conn.execute(query)
                #     #raise UserWarning('HASTA AQUI')
            else:
                #NO SPLIT, SO WE CHANGE THE PICKING TO DONE
                cadena2 ="""
                    UPDATE
                        stock_picking
                    SET
                        state = 'done',
                        date_done = now() at time zone 'UTC'
                    WHERE
                        id = """+str(current_picking.id)+""";"""
                _conn.execute(cadena2)
                post_data = """
                    SELECT
                        state, date_done
                    FROM
                        stock_picking
                    WHERE
                        id = {}
                """.format(current_picking.id)
                _conn.execute(post_data)
                result = _conn.fetchall()
                fields = current_picking.fields_get()
                for key, val in fields['state']['selection']:
                    if key == result[0][0]:
                        state_description = str(val)
                str_state = fields['state']['string']
                str_date_done = fields['date_done']['string']
                date_done = result[0][1]
                date_w_seconds = datetime.strptime(date_done, "%Y-%m-%d %H:%M:%S.%f")
                date_without_seconds = datetime.strftime(date_w_seconds, "%Y-%m-%d %H:%M:%S")
                date_timezone = self.convert_date_timezone(date_without_seconds)
                post_msg = _("""
                    <ul>
                        <li><b>{0}:</b> {2}</li>
                        <li><b>{1}:</b> {3},<b>Timezone:</b> {4}</li>
                    </ul>
                """).format(str_state,str_date_done,state_description,date_timezone,self.env.user.partner_id.tz)
                current_picking.message_post(
                    body=post_msg,
                    partner_ids=[],
                    subject=_("Transfer Done"),
                    type='notification',
                    author_id=self.env.user.partner_id.id
                )
            #IF EXISTS A DOCUMENT INVENTORY WE CHANGE THE STATE TO SUPPLIED
            if dataarray[0][23]!=None:
                query = """
                    UPDATE
                        document_inventory
                    SET
                        state = 'supplied'
                    WHERE
                        id = {}
                """.format(dataarray[0][23])
                self.env.cr.execute(query)
            #IF WE DO SPLIT FOR THE PICKING LETS DO A RESERVATION TO
            if sr_ctx:
                if kind=='sale':
                    new_picking.sudo().do_unreserve()
                    new_picking.sudo().rereserve_pick()
        except Exception as e:
            raise Warning('{}'.format(e))
        _conn.commit()
        _conn.close()
        return True

    def get_data_sql(self, picking_id=None):
        # Get Data stock_picking, stock_move and relations of product
        data = []
        cr = self.env.cr
        try:
            cr.execute("""
                SELECT
                    *
                FROM
                    get_data_products_series(%s);
                """, tuple(picking_id))
            response = cr.dictfetchall()
            for res in response:
                data.append(
                    {
                        'picking_id': res['picking_id'],#res[6],
                        'picking_origin': res['pickingorigin'],#res[0],
                        'picking_state': res['pickingstate'],#res[1],
                        'picking_name': res['pickingname'],#res[2],
                        'document_inventory_id': res['pickingdocumentinventory_id'],#res[3],
                        'company_id': res['pickingcompany_id'],#res[4],
                        'picking_type_code': res['pickingtypecode'],#res[5],
                        'move_id': res['move_id'],#res[7],
                        'move_product_qty': int(res['moveproductqty']),#int(res[8]),
                        'move_price_unit': res['movepriceunit'],#res[9],
                        'move_invoice_state': res['moveinvoicestate'],#res[10],
                        'purchase_line_id': res['movepurchaseline_id'],#res[11],
                        'location_id': res['movelocation_id'],#res[12],
                        'location_dest_id': res['movelocation_dest_id'],#res[13],
                        'product_p_id': res['moveproduct_id'],#res[14],
                        'group_id': res['movegroup_id'],#res[15],
                        'move_split_form': res['movesplitfrom'],#res[16],
                        'ultcost': res['ulcostcost'],#res[17],
                        'warehouse_origin_id': res['iidwarehouseorigen'],#res[18],
                        'warehouse_dest_id': res['iidwarehousedestino'],#res[19],
                        'product_t_id': res['ppproduct_tmpl_id'],#res[20],
                        'default_code': res['ppdefault_code'],#res[21],
                        'product_name': res['ppname_template'],#res[22],
                        'product_track_all': res['pttrack_all'],#res[23],
                        'uom_id': res['ptuom_id'],#res[24],
                        'product_lot_uniq_ok': res['ptlot_unique_ok'],#res[25],
                        'product_active': res['ptactive'],#res[26],
                        'stock_counter': res['icontador'],#res[27],
                        'sum': res['isumatoria'],#res[28],
                        'standar_price_id': res['imsp2idstandarprice'],#res[29],
                        'standar_price2_id': res['imspidproductstandarprice'],#res[30],
                        'pol_qty': res['porderlineqty'],#res[45],
                        'procurement_id': res['moveprocurementid'],#res[46],
                        'sale_line_id': res['sorderlineid'],#res[47],
                        'sol_qty': res['sorderlineqty'],#res[48],
                        'origin_returned_move': res['moveoriginreturnedmoveid'],
                        'document_origin': res['gdorder_id'],
                        'is_return': res['tdtisreturn']
                    })
            return data
        except Exception as e:
            raise except_orm(_('Error while get data stock_picking, stock_move and relations of product'),
                             'ERROR: {}'.format(e))

    def _get_data_series_exist(self, serie=None, product_id=None):
        # Check if exists the serie in stock_production_lot
        i = 0
        for elemento in _array:
            i+=1
            if elemento[4]==True:
                _conn.execute("""
                    SELECT
                        tis.serie
                    FROM
                        temp_insert_series AS tis
                    INNER JOIN
                        stock_production_lot AS spl ON trim(spl.name)=tis.serie AND
                        spl.product_id=tis.prod_id
                    WHERE
                        tis.prod_id = {};
                """.format(elemento[0]))
                result=_conn.fetchall()
                seriesr = '\n'
                if len(result)>0:
                    for serie in result:
                        seriesr += serie[0]+'\n'
                    raise Warning(_('The following series already exist within the system, please verify: {}').format(seriesr))

    def _get_data_series_valid_to_move(self):
        # Check if exists the serie in stock_production_lot and be in the rigt location
        i = 0
        for elemento in _array:
            i+=1
            if elemento[4]==True:
                query = """
                    UPDATE
                        temp_insert_series
                    SET
                        serie_id = stock_production_lot.id
                    FROM
                        stock_production_lot
                    WHERE
                        trim(stock_production_lot.name)=serie AND
                        stock_production_lot.product_id=prod_id;
                    SELECT
                        serie
                    FROM
                        temp_insert_series
                    WHERE
                        serie_id ISNULL AND
                        prod_id = {};
                """.format(elemento[0])
                _conn.execute(query)
                result = _conn.fetchall()
                seriesr = '\n'
                if len(result)>0:
                    for serie in result:
                        seriesr += serie[0]+'\n'
                    raise Warning(_('The following series do not exist within the system, please verify: {}').format(seriesr))
                query = """
                    UPDATE
                        temp_insert_series
                    SET
                        location_id = stock_quant.location_id
                    FROM
                        stock_quant
                    WHERE
                        stock_quant.lot_id = serie_id AND
                        stock_quant.product_id = prod_id;
                    SELECT
                        serie
                    FROM
                        temp_insert_series
                    WHERE
                        location_id != {};
                """.format(elemento[5])
                _conn.execute(query)
                result = _conn.fetchall()
                seriesr = '\n'
                if len(result)>0:
                    for serie in result:
                        seriesr += serie[0]+'\n'
                    raise Warning(_('The following series are not located in the right location to move: {}').format(seriesr))
                #Finally we validate if this picking is an internal return
                posiciond = [col[0] for col in dataarray].index(int(elemento[0]))
                query = """
                    SELECT
                        origin_returned_move_id
                    FROM
                        stock_move
                    WHERE
                        id = {}""".format(dataarray[posiciond][4])
                _conn.execute(query)
                result = _conn.fetchall()
                ormi = result[0][0]
                if ormi != None:
                    query = """
                        SELECT
                            tis.serie, sp.origin, sp.name, sm.id
                        FROM
                            temp_insert_series tis
                        INNER JOIN
                            stock_quant sq on sq.lot_id = tis.serie_id
                        INNER JOIN
                            stock_quant_move_rel sqmr ON sqmr.quant_id = sq.id
                        INNER JOIN
                            stock_move sm ON sm.id=sqmr.move_id
                        INNER JOIN
                            stock_picking sp ON sp.id = sm.picking_id
                        WHERE
                            sqmr.move_id > {0} AND
                            sm.location_dest_id = {2} AND
                            tis.prod_id = {1}
                        ORDER BY
                            tis.serie, sm.id desc;""".format(ormi,elemento[0],elemento[5])
                    _conn.execute(query)
                    result = _conn.fetchall()
                    if len(result)>0:
                        prevs = ''
                        for serie in result:
                            if serie[0]!=prevs:
                                seriesr += serie[0]+' '+serie[1]+' '+serie[2]+'\n'
                                prevs = serie[0]
                        raise Warning(_('The following series were moved on another document, please verify: {}').format(seriesr))
    def _series_valid_to_refund(self):
        # Check if exists the serie in stock_production_lot, the location is right and belongs to the specified SO
        i = 0
        for elemento in _array:
            i+=1
            #First validate if exists
            if elemento[4]==True:
                query = """
                    UPDATE
                        temp_insert_series
                    SET
                        serie_id = stock_production_lot.id
                    FROM
                        stock_production_lot
                    WHERE
                        upper(trim(stock_production_lot.name))=serie AND
                        stock_production_lot.product_id=prod_id;
                    SELECT
                        serie
                    FROM
                        temp_insert_series
                    WHERE
                        serie_id ISNULL AND
                        prod_id = {};
                """.format(elemento[0])
                _conn.execute(query)
                result = _conn.fetchall()
                seriesr = '\n'
                if len(result)>0:
                    for serie in result:
                        seriesr += serie[0]+'\n'
                    raise Warning(_('The following series do not exist within the system, please verify: {}').format(seriesr))
                #Then validate if the serie is on the right warehouse
                query = """
                    UPDATE
                        temp_insert_series
                    SET
                        location_id = stock_quant.location_id
                    FROM
                        stock_quant
                    WHERE
                        stock_quant.lot_id = serie_id AND
                        stock_quant.product_id = prod_id;
                    SELECT
                        serie
                    FROM
                        temp_insert_series
                    WHERE
                        location_id != {};
                """.format(elemento[5])
                _conn.execute(query)
                result = _conn.fetchall()
                seriesr = '\n'
                if len(result)>0:
                    for serie in result:
                        seriesr += serie[0]+'\n'
                    raise Warning(_('The following series are not located in the selected location: {}').format(seriesr))
                posiciond = [col[0] for col in dataarray].index(int(elemento[0]))
                #Now evaluate if this movement is return but with no origin document
                is_return_withoutd = dataarray[posiciond][24]
                if is_return_withoutd == False:
                    #Finally validate if belongs to the specified SO
                    #First we get the origin returned move id from the move
                    query = """
                        SELECT
                            origin_returned_move_id
                        FROM
                            stock_move
                        WHERE
                            id = {}""".format(dataarray[posiciond][4])
                    _conn.execute(query)
                    result = _conn.fetchall()
                    ormi = result[0][0]
                    query = """
                        SELECT
                            serie
                        FROM
                            temp_insert_series tis
                        WHERE
                            serie_id NOT IN (
                                SELECT
                                    spl.id
                                FROM
                                    stock_production_lot spl
                                INNER JOIN
                                    stock_quant sq ON sq.lot_id = spl.id
                                INNER JOIN
                                    stock_quant_move_rel sqmr ON sqmr.quant_id = sq.id
                                WHERE
                                    sqmr.move_id = {}
                            )
                            AND prod_id = {}; """.format(result[0][0],elemento[0])
                    _conn.execute(query)
                    result = _conn.fetchall()
                    if len(result)>0:
                        for serie in result:
                            seriesr += serie[0]+'\n'
                        raise Warning(_('There are series that do not belong to the source document, please verify: {}').format(seriesr))
                    #Finally we validate if the quant is on the right location trough the same origin document and no a new one
                    query = """
                        SELECT
                            tis.serie, sp.origin, sp.name, sm.id
                        FROM
                            temp_insert_series tis
                        INNER JOIN
                            stock_quant sq on sq.lot_id = tis.serie_id
                        INNER JOIN
                            stock_quant_move_rel sqmr ON sqmr.quant_id = sq.id
                        INNER JOIN
                            stock_move sm ON sm.id=sqmr.move_id
                        INNER JOIN
                            stock_picking sp ON sp.id = sm.picking_id
                        WHERE
                            sqmr.move_id > {0} AND
                            sm.location_dest_id = {2} AND
                            tis.prod_id = {1}
                        ORDER BY
                            tis.serie, sm.id desc;""".format(ormi,elemento[0],elemento[5])
                    _conn.execute(query)
                    result = _conn.fetchall()
                    if len(result)>0:
                        prevs = ''
                        for serie in result:
                            if serie[0]!=prevs:
                                seriesr += serie[0]+' '+serie[1]+' '+serie[2]+'\n'
                                prevs = serie[0]
                        raise Warning(_('The following series were moved on another document, please verify: {}').format(seriesr))

    def _get_data_generated_csv(self, picking_id=None):
        # Get data to generate csv file
        cr = self.env.cr
        try:
            cr.execute("""
                        SELECT  SP.origin,
                                PP.id,
                                PP.default_code,
                                PT.name,
                                SM.product_qty,
                                PT.lot_unique_ok
                        FROM    stock_picking AS SP
                                JOIN stock_move AS SM
                                ON SP.id = SM.picking_id
                                JOIN product_product AS PP
                                ON SM.product_id = PP.id
                                JOIN product_template AS PT
                                ON PT.id = PP.product_tmpl_id
                        WHERE   SP.id IN (%s);""", tuple(picking_id))
            return cr.fetchall()
        except Exception as e:
            raise except_orm(_('Error while get data to generate csv file'),
                             'ERROR: {}'.format(e))
