# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import psycopg2
from openerp import _
from openerp.tools.config import config
from openerp.exceptions import except_orm

import logging
_logger = logging.getLogger(__name__)


class SeriesConectorDB():
    _host = config.get("db_host")
    _user = config.get("db_user")
    _pass = config.get("db_password")
    _dbname = None
    _connexion = None
    _cursor = None

    def __init__(self, dbname=None):
        self._dbname = dbname
        try:
            self._connexion = psycopg2.connect("dbname='{}' user='{}' \
                                     host='{}' password='{}'".format(
                                                            self._dbname,
                                                            self._user,
                                                            self._host,
                                                            self._pass,
                                                            'PGSQL_CONNECT_FORCE_NEW'
                                                            ))
            self.connexion()
        except Exception as e:
            raise except_orm(_('Error of DB credentials Fail'),
                             'ERROR: {}'.format(e))

    def connexion(self):
        try:
            self._cursor = self._connexion.cursor()
            return self._cursor
        except Exception as e:
            raise except_orm(_('Error of DB connection Fail'),
                             'ERROR: {}'.format(e))

    def execute(self, query):
        try:
            self._cursor.execute(query)
        except Exception as e:
            raise except_orm(_('Error execute Querys Fail'),
                             'ERROR: {}'.format(e))

    def fetchall(self):
        try:
            return self._cursor.fetchall()
        except Exception as e:
            raise except_orm(_('Error Fetchall Fail'),
                             'ERROR: {}'.format(e))
        
    def fetchone(self):
        try:
            return self._cursor.fetchone()
        except Exception as e:
            raise except_orm(_('Error Fetchone Fail'),
                             'ERROR: {}'.format(e))

    def commit(self):
        try:
            return self._connexion.commit()
        except Exception as e:
            raise except_orm(_('Error Commit Fail'),
                             'ERROR: {}'.format(e))

    def close(self):
        try:
            self._cursor.close()
            self._connexion.close()
        except Exception as e:
            raise except_orm(_('Error Closing Sesion'),
                             'ERROR: {}'.format(e))
