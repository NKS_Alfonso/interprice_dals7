# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import logging
import sys
from openerp import api, fields, models, tools, _, conf
from openerp.exceptions import Warning, except_orm
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

class ConfirmDialog(models.TransientModel):
    _name = 'confirm.dialog'
    _description = 'Confirm Dialog'
    
    text = fields.Text(readonly=True)
    
    @api.multi
    def btn_yes(self):
        model_name = self._context.get('active_model',False)
        record_id = self._context.get('active_ids',False)
        method_execute = self._context.get('method_execute',False)
        if model_name==False or record_id==False or method_execute==False:
            return False
        record_todo = self.env[model_name].search([('id','=',record_id[0])])
        func =  getattr(record_todo,method_execute)
        return func()

    # @api.multi
    # def get_message(self):
    #     for record in self:
    #         record.text = self._context.get('dialog_message','There is no message')