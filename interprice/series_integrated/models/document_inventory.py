# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import logging
from openerp import api, fields, models, tools, _, conf
_logger = logging.getLogger(__name__)

class document_inventory_type(models.Model):
    _inherit = 'document.inventory.type'
    
    is_return = fields.Boolean(string='Is return?', default=False)