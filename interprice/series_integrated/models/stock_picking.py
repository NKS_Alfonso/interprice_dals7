# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import logging
import re
from openerp import api, fields, models, tools, conf

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def upload_series_odoo_integrated(self):
        context = {}
        cant_visible = True
        viability_create = True
        in_series_dict = {}
        in_series_list = []
        in_series_obj = self.env['ingress.series.display']
        picking_series_obj = self.env['stock.picking.series']
        response = picking_series_obj.get_data_sql(picking_id=self.ids)
        # We check if exists more of 1000 items
        # or more of 20 products, too don't viability create records
        # they should upload csv file
        if len(response) <= 1000:
            quantity = 0
            for data in response:
                if data.get('product_lot_uniq_ok'):
                    quantity += int(data.get('move_product_qty'))
                else:
                    quantity += 1
            if quantity > 1000:
                viability_create = False
                cant_visible = False
        else:
            viability_create = False
            cant_visible = False

        series_exists = in_series_obj.search([("picking_id", "=", self.ids[0])])
        if len(series_exists) >= 0:
            # If viability, we create records in ingress_series_display
            if viability_create:
                for data in response:
                    data_series = {
                            'document_origin': data.get('picking_origin'),
                            'product_id': data.get('product_p_id'),
                            'product_sku': data.get('default_code'),
                            'product_name': data.get('product_name'),
                            'picking_id': self.ids[0],
                            'product_uniq_ok': data.get('product_lot_uniq_ok')
                    }
                    if data.get('product_lot_uniq_ok'):
                        for i in range(data.get('move_product_qty')):
#                            _logger.warning('-----------------> create serie %s' %(i))
                            data_series['product_cant'] = 1
                            in_series_dict = data_series.copy()
                            id_in_series = in_series_obj.create(data_series)
                            in_series_dict['series_ids'] = id_in_series.id
                            # in_series_list.append([0, 0, in_series_dict])
                            in_series_list.append(in_series_dict)
                    else:
#                        _logger.warning('-----------------> create S/N')
                        data_series['product_cant'] = data.get('move_product_qty')
                        data_series['product_series'] = 'S/N'
                        id_in_series = in_series_obj.create(data_series)
                        in_series_dict = data_series.copy()
                        in_series_dict['series_ids'] = id_in_series.id
                        # in_series_list.append([0, 0, in_series_dict])
                        in_series_list.append(in_series_dict)
            context = {
                        'default_ingress_series': in_series_list,
                        'default_product_cant_visible': cant_visible
                      }
        else:
            for series in series_exists:
                data_series = {
                        'series_ids': series.id,
                        'document_origin': series.document_origin,
                        'product_id': series.product_id,
                        'product_sku': series.product_sku,
                        'product_name': series.product_name,
                        'picking_id': series.picking_id.id,
                        'product_cant': series.product_cant,
                        'product_series': series.product_series,
                        'product_uniq_ok': series.product_uniq_ok
                }
                # in_series_list.append([0, 0, data_series])
                in_series_list.append(data_series)
            context = {
                        'default_ingress_series': in_series_list,
                        'default_product_cant_visible': cant_visible
                      }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'stock.picking.series',
            'view_mode': 'form',
            'target': 'new',
            'context': context
        }
