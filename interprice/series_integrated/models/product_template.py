# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

import logging
from openerp import api, fields, models, tools, _, conf
from openerp.exceptions import Warning, except_orm
_logger = logging.getLogger(__name__)


class product_template(models.Model):
    _inherit = 'product.template'
    
    @api.onchange('track_all')
    def onchange_track_all(self):
        self.lot_unique_ok = False