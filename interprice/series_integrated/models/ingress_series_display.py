# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, tools, conf
from openerp.exceptions import Warning

import logging
_logger = logging.getLogger(__name__)


class IngressSeriesDisplay(models.TransientModel):
    _name = 'ingress.series.display'
    series_ids = fields.Many2one('stock.picking.series', 'Series')
    picking_id = fields.Many2one('stock.picking', 'Picking')
    document_origin = fields.Char('Document', size=20)
    product_id = fields.Integer('Product Id')
    product_sku = fields.Char('Product SKU', size=30)
    product_name = fields.Char('Product Name', size=150)
    product_cant = fields.Integer('Quantity')
    product_series = fields.Char('Serie', size=35)
    product_uniq_ok = fields.Boolean('series Unique')