# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, models, tools


class StockPickingSeriesQuerys(models.Model):
    _name = 'stock.picking.series.querys'
    _auto = False

    def init(self, cr):
        # Drop view
        # tools.sql.drop_view_if_exists(cr, 'get_products_series')
        # Create view and function
        # SELECT * FROM get_products_series(663)
        cr.execute("""DROP FUNCTION IF EXISTS get_data_products_series(integer);
           CREATE OR REPLACE
           FUNCTION get_data_products_series(IN idpicking integer)
                    RETURNS TABLE (
                        PickingOrigin varchar,
                        PickingState varchar,
                        PickingName varchar,
                        PickingDocumentInventory_id integer,
                        PickingCompany_id integer,
                        PickingTypeCode varchar,
                        Picking_id integer,
                        Move_id integer,
                        MoveProductQty numeric,
                        MovePriceUnit double precision,
                        MoveInvoiceState varchar,
                        MovePurchaseLine_id integer,
                        MoveLocation_id integer,
                        MoveLocation_dest_id integer,
                        MoveProduct_id integer,
                        MoveGroup_id integer,
                        MoveSplitFrom integer,
                        ULCOSTCost double precision,
                        iIdWarehouseOrigen integer,
                        iIdWarehouseDestino integer,
                        PPproduct_tmpl_id integer,
                        PPdefault_code varchar,
                        PPname_template varchar,
                        PTtrack_all boolean,
                        PTuom_id integer,
                        PTlot_unique_ok boolean,
                        PTactive boolean,
                        iContador bigint,
                        iSumatoria double precision,
                        IMSP2idstandarprice integer,
                        IMSPidproductstandarprice integer,
                        IMSPidstandarpricename varchar,
                        IMCMidCostMethod integer,
                        IMCMvalue_text text,
                        GDorder_id varchar,
                        TDTstrEstado varchar,
                        TDTiIdDocumentoEnTransito integer,
                        TDTiIdTipoInventarioDocumento integer,
                        TDTtype_document_form_transit_to_dest_id integer,
                        TDTwarehouse_id integer,
                        TDTwarehouse_dest_id integer,
                        TDTdefault_location_id integer,
                        TDTedit_cost boolean,
                        TDTlocation_dest_id integer,
                        TDTstate_document_to_transfer varchar,
                        POrderLineQty numeric,
                        MoveProcurementId integer,
                        SOrderLineId integer,
                        SOrderlineQty numeric,
                        MoveOriginReturnedMoveId integer,
                        TDTIsReturn boolean

                    )
                    AS $$
        BEGIN
        RETURN QUERY SELECT SP.origin, SP.state, SP.name,
                            SP.document_inventory_id, SP.company_id, SPT.code,
                            SM.picking_id, SM.id, SM.product_qty,
                            SM.price_unit, SM.invoice_state,
                            SM.purchase_line_id,
                            SM.location_id, SM.location_dest_id, SM.product_id,
                            SM.group_id, SM.split_from,
                            ULCOST.cost AS fUltimoCosto,
                            LOC.iIdAlmacen AS iIdWarehouseOrigen,
                            LOC2.iIdAlmacen AS iIdWarehouseDestino,
                            PP.product_tmpl_id,
                            PP.default_code,
                            PP.name_template,
                            PT.track_all,
                            PT.uom_id,
                            PT.lot_unique_ok,
                            PT.active,
                            CASE WHEN SUB.iContador ISNULL
                                 THEN 0 ELSE SUB.iContador END AS iContador,
                            CASE WHEN SUB.iSumatoria ISNULL
                                 THEN 0 ELSE SUB.iSumatoria END AS iSumatoria,
                            IMSP2.idstandarprice,
                            IMSP.idproductstandarprice,
                            IMSP.idstandarpricename,
                            IMCM.idCostMethod,
                            IMCM.value_text,
                            GD.order_id AS strDocumentoOrigen,
                            TDT.strEstado, TDT.iIdDocumentoEnTransito,
                            TDT.iIdTipoInventarioDocumento,
                            TDT.type_document_form_transit_to_dest_id,
                            TDT.warehouse_id, TDT.warehouse_dest_id,
                            TDT.default_location_id AS iLocacionOrigenTransferencia,
                            TDT.edit_cost,
                            TDT.location_dest_id AS iLocacionDestinoTransferencia,
                            TDT.state_document_to_transfer,
                            COALESCE(POL.product_qty,0.00) as product_qty,
                            SM.procurement_id,
                            SOL.id as sale_line_id,
                            COALESCE(SOL.product_uom_qty,0.00) as product_uos_qty,
                            SM.origin_returned_move_id,
                            TDT.is_return
                    FROM    stock_picking AS SP
                            JOIN stock_picking_type as SPT
                            ON SP.picking_type_id = SPT.id
                            JOIN stock_move AS SM
                            ON SP.id = SM.picking_id
                            JOIN product_product AS PP
                            ON SM.product_id = PP.id
                            JOIN product_template AS PT
                            ON PT.id = PP.product_tmpl_id
                            JOIN (SELECT     PPH.product_template_id, PPH.cost
                                    FROM     product_price_history AS PPH
                                            JOIN (SELECT    product_template_id,
                                                            MAX(id) AS id
                                                    FROM    product_price_history
                                                    GROUP BY product_template_id) AS SUB1
                                            ON PPH.id = SUB1.id
                                    ORDER BY PPH.product_template_id  ) ULCOST
                            ON    PP.product_tmpl_id = ULCOST.product_template_id
                            LEFT JOIN (SELECT count(sq.qty) AS iContador,
                                              sum(qty) AS iSumatoria,
                                              sq.product_id
                                        FROM stock_quant AS sq
                                        INNER JOIN stock_location AS sl
                                        ON sq.location_id = sl.id
                                        WHERE sl.usage = 'internal'
                                        group by sq.product_id) AS SUB
                            ON SM.product_id = SUB.product_id
                            LEFT JOIN (SELECT     SL.id AS iIdLocation,
                                            SW.id AS iIdAlmacen
                                    FROM stock_location AS SL
                                    LEFT JOIN stock_warehouse AS SW
                                    ON SL.id = SW.lot_stock_id) AS LOC
                            ON LOC.iIdLocation = SM.location_id
                            LEFT JOIN (SELECT     SL.id AS iIdLocation,
                                            SW.id AS iIdAlmacen
                                    FROM stock_location AS SL
                                    LEFT JOIN stock_warehouse AS SW
                                    ON SL.id = SW.lot_stock_id) AS LOC2
                            ON LOC2.iIdLocation = SM.location_dest_id
                            LEFT JOIN (SELECT    IMF.id as idstandarprice,
                                                IP.res_id as idstandarpricename,
                                                IP.company_id,
                                                IP.id as idproductstandarprice
                                        FROM    ir_model_fields as IMF
                                                JOIN ir_property as IP
                                                ON IMF.id = IP.fields_id
                                                AND IMF.model='product.template'
                                                AND IMF.name='standard_price') AS IMSP
                            ON IMSP.idstandarpricename = concat('product.template,',PP.product_tmpl_id)
                            AND IMSP.company_id = SP.company_id
                            LEFT JOIN (SELECT   IMF.id as idstandarprice,
                                                IP.company_id
                                        FROM    ir_model_fields as IMF
                                                JOIN ir_property as IP
                                                ON IMF.id = IP.fields_id
                                                AND IMF.model='product.template'
                                                AND IMF.name='standard_price'
                                      GROUP BY IMF.id, IP.company_id) AS IMSP2
                            ON 1 = 1
                            AND IMSP2.company_id = SP.company_id
                            LEFT JOIN (SELECT   IMF.id AS idCostMethod,
                                                IP.value_text,
                                                IP.company_id,
                                                IP.res_id as idstandarpricename
                                        FROM    ir_model_fields as IMF
                                                JOIN ir_property as IP
                                                ON IMF.id = IP.fields_id
                                                AND IMF.model='product.template'
                                                AND IMF.name='cost_method') AS IMCM
                            ON IMCM.idstandarpricename = concat('product.template,',PP.product_tmpl_id)
                            AND IMCM.company_id = SP.company_id
                            LEFT JOIN _gv_devolucion AS GD
                            ON GD.picking_id = SP.id
                            LEFT JOIN (SELECT     DI.id AS iIdInventarioDocumento,
                                                DI.document_in_transit_id AS iIdDocumentoEnTransito,
                                                DI.state AS strEstado,
                                                DI.type_document AS iIdTipoInventarioDocumento,
                                                DI.warehouse_id,
                                                DI.warehouse_dest_id,
                                                DIT.default_location_id,
                                                DIT.state_document_to_transfer,
                                                DI.location_dest_id,
                                                DIT.type_document_form_transit_to_dest_id,
                                                DI.edit_cost,
                                                DIT.is_return
                                        FROM    document_inventory AS DI
                                                JOIN document_inventory_type AS DIT
                                                ON DI.type_document = DIT.ID ) AS TDT
                            ON TDT.iIdInventarioDocumento = SP.document_inventory_id
                            LEFT JOIN purchase_order_line POL
                            ON POL.id = SM.purchase_line_id
                            LEFT JOIN procurement_order PROCOR
                            ON PROCOR.id = SM.procurement_id
                            LEFT JOIN sale_order_line SOL
                            on SOL.id = PROCOR.sale_line_id
                    WHERE   SP.id IN (idpicking);

        RETURN;
        END;
        $$
        LANGUAGE 'plpgsql';

        """
        )
