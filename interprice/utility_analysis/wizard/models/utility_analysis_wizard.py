# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import api, exceptions, fields, models, _, tools
# creator excel file lib


class UtilityAnalysisWizard(models.TransientModel):
    """This wizard sample the Utility Analysis on screen, pdf and csv."""
    _name = 'utility_analysis_wizard'
    _description = _('Utility Analysis Wizard')

    # field selection options

    # Global variables
    _type_document = ''
    _doc_ids = None

    # domain methods

    # Fields
    center_cost = fields.Many2one(
        comodel_name='account.cost.center',
        string=_("Branch offices"),
        help=_("Branch offices selection"),
    )
    start_date = fields.Datetime(
        required=True,
        help=_("Initial date to filter"),
        string=_("Initial date")
    )
    end_date = fields.Datetime(
        required=True,
        help=_("Final date to filter"),
        string=_("Final date")
    )
    section = fields.Many2one(
        comodel_name='product.category',
        domain="[('types.type', '=', 'Sección')]",
        string=_("Section"),
        help=_("Section of product")
    )
    brand = fields.Many2one(
        comodel_name='product.category',
        domain="[('id', 'in', [])]",
        string=_("Brand"),
        help=_("Brand of product")
    )
    line = fields.Many2one(
        comodel_name='product.category',
        domain="[('id', 'in', [])]",
        string=_("Line"),
        help=_("Line of product")
    )
    product_code = fields.Many2one(
        comodel_name='product.product',
        string=_("Product Code"),
        help=_("Code of product")
    )
    product_detail = fields.Boolean(
        string=_("Product Detail"),
        help=_("Product detail")
    )
    deactivate_fields = fields.Boolean(
        string=_("Active Fields"),
        default=False
    )
    clean_product_code = fields.Boolean(
        string=_("Reset values"),
        default=False
    )

    # onchange methods
    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        df = self.start_date
        dt = self.end_date

        if df and dt and df > dt:
            self.start_date = False
            self.end_date = False
            raise exceptions.Warning(
                _("'Date to' must be higher than 'Date from'."))

    @api.onchange('product_code')
    def product_code_change(self):
        if self.product_code:
            self.center_cost = False
            self.section = False
            self.brand = False
            self.line = False
            self.deactivate_fields = True
        elif not self.product_code:
            self.deactivate_fields = False

    @api.onchange('clean_product_code')
    def clean_product_field(self):
        if self.clean_product_code:
            self.product_code = False
            self.clean_product_code = False

    @api.onchange('section')
    def _onchange_section(self):
        res = {}
        self.brand = False
        self.line = False
        if self.section:
            self.env.cr.execute(
                """
                SELECT brand FROM product_template
                WHERE section = %s GROUP BY brand
                """, (self.section.id,)
            )
            if self.env.cr.rowcount:
                ids = [id[0] for id in self.env.cr.fetchall()]
                res['domain'] = {'brand': [('id', '=', ids)]}
        else:
            res['domain'] = {'brand': [('id', '=', [])],
                             'line': [('id', '=', [])]}
        return res

    @api.onchange('brand')
    def _onchange_brand(self):
        res = {}
        self.line = False
        if self.brand:
            self.env.cr.execute(
                """
                SELECT line FROM product_template
                WHERE section = %s and brand = %s GROUP BY line
                """, (self.section.id, self.brand.id)
            )
            if self.env.cr.rowcount:
                ids = [id[0] for id in self.env.cr.fetchall()]
                res['domain'] = {'line': [('id', '=', ids)]}
        else:
            res['domain'] = {'line': [('id', '=', [])]}
        return res

    # methods
    @api.multi
    def screen(self):
        self._get_ids()
        if not self.product_detail:
            self._get_group_ids()
            res_model = 'r.utility.analysis.group'
            name = _('Utility analysis without detail')
        elif self.product_detail:
            self._get_group_detail_ids()
            res_model = 'r.utility.analysis.group.detail'
            name = _('Utility analysis with detail')
        return {
            'name': name,
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': res_model,
            'target': 'current',
            'context': {}
        }

    @api.multi
    def show_xls(self):
        """Download the document, XLS"""
        self._get_ids()
        if not self.product_detail:
            self._get_group_ids()
            self._type_document = 'group_ids'
        elif self.product_detail:
            self._get_group_detail_ids()
            self._type_document = 'group_detail_ids'
        csv_id = self.csv_by_ids()
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    def _get_ids(self):
        # Get filter values
        center_cost = self.center_cost
        section = self.section
        brand = self.brand
        line = self.line
        product_code = self.product_code
        s_date = self.start_date
        e_date = self.end_date

        query = "SELECT rua.id FROM r_utility_analysis rua\n"

        if s_date:
            query += "\tWHERE rua.transaction_date >= CAST('%s' AS TIMESTAMP)\n" % (s_date)
        if e_date:
            query += "\tAND rua.transaction_date <= CAST('%s' AS TIMESTAMP)\n" % (e_date)
        if center_cost:
            query += "\tAND rua.center_cost = %s\n" % (center_cost.id)
        if section:
            query += "\tAND rua.section = %s\n" % (section.id)
        if brand:
            query += "\tAND rua.brand = %s\n" % (brand.id)
        if line:
            query += "\tAND rua.line = %s\n" % (line.id)
        if product_code:
            query += "\tAND rua.product_id = %s\n" % (product_code.id)
        print query
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            if self.env.cr.rowcount == 1:
                self._doc_ids = '(' + str(self.env.cr.fetchone()[0]) + ')'
            elif self.env.cr.rowcount > 1:
                self._doc_ids = tuple(int(id[0]) for id in self.env.cr.fetchall())
        else:
            raise exceptions.Warning(_("No records found!"))

    def _get_group_ids(self):
        # Get filter values
        tools.sql.drop_view_if_exists(self.env.cr, 'r_utility_analysis_group')

        # Create view
        query = """
            CREATE OR REPLACE VIEW r_utility_analysis_group AS (
                SELECT
                    row_number() OVER (ORDER BY month) AS id,
                    month_year,
                    month,
                    year,
                    section,
                    brand,
                    sum(sale) as sale,
                    sum(cost) as cost,
                    sum(quantity) as quantity,
                    round(sum(sale), 2) - round(sum(cost), 2) as utility,
                    sum(utility)/sum(sale)*100 as percentage_utility
                FROM r_utility_analysis WHERE id in %s
                    GROUP BY month, year, section, brand, month_year
                    ORDER BY year, month, section, brand);
            """ % str(self._doc_ids)
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            pass
        else:
            raise exceptions.Warning(_("No records found!"))

    def _get_group_detail_ids(self):
        # Get filter values
        tools.sql.drop_view_if_exists(self.env.cr, 'r_utility_analysis_group_detail')

        # Create view
        query = """
            CREATE OR REPLACE VIEW r_utility_analysis_group_detail AS (
                SELECT
                    row_number() OVER (ORDER BY product_id) AS id,
                    product_id,
                    month_year,
                    month,
                    year,
                    product,
                    code,
                    section,
                    brand,
                    line,
                    sum(sale) as sale,
                    sum(cost) as cost,
                    sum(quantity) as quantity,
                    round(sum(sale), 2) - round(sum(cost), 2) as utility,
                    sum(utility)/sum(sale)*100 as percentage_utility
                FROM r_utility_analysis WHERE id in %s
                    GROUP BY month, year, product_id, product, code, section, brand, line, month_year
                    ORDER BY year, month, section, brand, line, code);
            """ % str(self._doc_ids)
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            pass
        else:
            raise exceptions.Warning(_("No records found!"))

    def csv_by_ids(self):
        data_csv = []
        if self._type_document == 'group_ids':
            data = self.env['r.utility.analysis.group'].search(
                [('id', '>=', 0)], order='year, month, section, brand')
            if data:
                data_csv.append(
                    [
                        _(''),
                        _('Type Document'),
                        _('Utility Analysis report'),
                    ]
                )
                data_csv.append([])
                data_csv.append([
                    _('Month'),
                    _('Section'),
                    _('Brand'),
                    _('Sales'),
                    _('Cost'),
                    _('% Utility'),
                    _('Utility $'),
                    _('Quantity')
                ])

                for row in data:
                    data_csv.append([
                        unicode(self._set_default(_(row.month), '')).encode('utf8'),
                        unicode(self._set_default(_(row.section.name), '')).encode('utf8'),
                        unicode(self._set_default(_(row.brand.name), '')).encode('utf8'),
                        unicode(self._set_default(_(row.sale), '')).encode('utf8'),
                        unicode(self._set_default(_(row.cost), '')).encode('utf8'),
                        unicode(self._set_default(_(row.percentage_utility), '')).encode('utf8'),
                        unicode(self._set_default(_(row.utility), '')).encode('utf8'),
                        unicode(self._set_default(_(row.quantity), '')).encode('utf8')
                    ])
        elif self._type_document == 'group_detail_ids':
            data = self.env['r.utility.analysis.group.detail'].search(
                [('id', '>=', 0)], order='year, month, section, brand')
            if data:
                data_csv.append(
                    [
                        _(''), _(''), _(''), _(''),
                        _('Type Document'),
                        _('Utility Analysis Detail report'),
                    ]
                )
                data_csv.append([])
                data_csv.append([
                    _('Month'),
                    _('Section'),
                    _('Brand'),
                    _('Line'),
                    _('Product code'),
                    _('Product'),
                    _('Sales'),
                    _('Cost'),
                    _('% Utility'),
                    _('Utility $'),
                    _('Quantity')
                ])

                for row in data:
                    data_csv.append([
                        unicode(self._set_default(_(row.month), '')).encode('utf8'),
                        unicode(self._set_default(_(row.section.name), '')).encode('utf8'),
                        unicode(self._set_default(_(row.brand.name), '')).encode('utf8'),
                        unicode(self._set_default(_(row.line.name), '')).encode('utf8'),
                        unicode(self._set_default(_(row.code), '')).encode('utf8'),
                        unicode(self._set_default(_(row.product), '')).encode('utf8'),
                        unicode(self._set_default(_(row.sale), '')).encode('utf8'),
                        unicode(self._set_default(_(row.cost), '')).encode('utf8'),
                        unicode(self._set_default(_(row.percentage_utility), '')).encode('utf8'),
                        unicode(self._set_default(_(row.utility), '')).encode('utf8'),
                        unicode(self._set_default(_(row.quantity), '')).encode('utf8')
                    ])
        if data_csv:
            file = OCsv().csv_base64(data_csv)
            return self.env['r.download'].create(
                vals={
                    'file_name': _('UtilityAnalysis.csv'),
                    'type_file': 'csv',
                    'file': file,
                }
            )
        else:
            raise exceptions.Warning(_("No records found!"))

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
