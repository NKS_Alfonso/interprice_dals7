# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, tools


class UtilityAnalysisGroupDetail(models.Model):
    """Utility Analysis Detail report"""

    # odoo model properties
    _name = 'r.utility.analysis.group.detail'
    _table = 'r_utility_analysis_group_detail'
    _description = 'Utility Analysis detail report'
    _auto = False  # Don't let odoo create table
    _order = 'year, month, section, brand, line, code'

    # initial methods

    # Selection list

    # View fields
    product_id = fields.Integer(
        readonly=True,
        string=_("Product ID"),
    )
    month_year = fields.Char(
        readonly=True,
        string=_("Month/Year"),
    )
    month = fields.Char(
        readonly=True,
        string=_("Month"),
    )
    year = fields.Char(
        readonly=True,
        string=_("Year"),
    )
    product = fields.Char(
        readonly=True,
        string=_("Product"),
    )
    code = fields.Char(
        readonly=True,
        string=_("Product code"),
    )
    section = fields.Many2one(
        comodel_name='product.category',
        readonly=True,
        string=_("Section"),
    )
    brand = fields.Many2one(
        comodel_name='product.category',
        readonly=True,
        string=_("Brand"),
    )
    line = fields.Many2one(
        comodel_name='product.category',
        readonly=True,
        string=_("Line"),
    )
    sale = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("Sales"),
    )
    cost = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("Cost"),
    )
    quantity = fields.Integer(
        readonly=True,
        string=_("Quantity"),
    )
    utility = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("Utility $"),
    )
    percentage_utility = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("% Utility"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_r_utility_analysis_group_detail(cr):
        tools.sql.drop_view_if_exists(cr, 'r_utility_analysis_group_detail')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_utility_analysis_group_detail AS (
                SELECT
                    row_number() OVER (ORDER BY product_id) AS id,
                    product_id,
                    month_year,
                    month,
                    year,
                    product,
                    code,
                    section,
                    brand,
                    line,
                    sum(sale) as sale,
                    sum(cost) as cost,
                    sum(quantity) as quantity,
                    round(sum(sale), 2) - round(sum(cost), 2) as utility,
                    sum(utility)/sum(sale)*100 as percentage_utility
                FROM r_utility_analysis
                    GROUP BY month, year, product_id, product, code, section, brand, line, month_year
                    ORDER BY year, month, section, brand, line, code);
            """
        )
