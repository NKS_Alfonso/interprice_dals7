# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import models, tools
from .utility_analysis_group import UtilityAnalysisGroup
from .utility_analysis_group_detail import UtilityAnalysisGroupDetail


class UtilityAnalysis(models.Model):
    """Utility Analysis report"""
    _auto = False
    _name = 'r_utility_analysis'
    _order = 'id ASC'

    def init(self, cr):
        # Drop view
        tools.sql.drop_view_if_exists(cr, 'r_utility_analysis')
        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_utility_analysis AS (
                SELECT
                    row_number() OVER (ORDER BY transaction_date) AS id,
                    line_id,
                    to_char(transaction_date, 'TMMonth') || '/' || date_part('year', transaction_date) as month_year,
                    CASE date_part('month', transaction_date)
                    WHEN 1 THEN 'Enero'
                    WHEN 2 THEN 'Febrero'
                    WHEN 3 THEN 'Marzo'
                    WHEN 4 THEN 'Abril'
                    WHEN 5 THEN 'Mayo'
                    WHEN 6 THEN 'Junio'
                    WHEN 7 THEN 'Julio'
                    WHEN 8 THEN 'Agosto'
                    WHEN 9 THEN 'Septiembre'
                    WHEN 10 THEN 'Octubre'
                    WHEN 11 THEN 'Noviembre'
                    WHEN 12 THEN 'Diciembre' END as month,
                    date_part('year', transaction_date) as year,
                    product_id,
                    product,
                    code,
                    section,
                    brand,
                    line,
                    sale,
                    quantity,
                    CAST(cost AS NUMERIC),
                    transaction_date,
                    rate,
                    sale - cost as utility,
                    center_cost
                FROM (
                    SELECT
                        ail.id as line_id,
                        ail.product_id as product_id,
                        pt.name as product,
                        CASE WHEN pp.default_code IS NOT NULL THEN pp.default_code ELSE
                        '' END as code,
                        pcs.id as section,
                        pcb.id as brand,
                        pcl.id as line,
                        CASE WHEN rc.name = 'USD' THEN ail.price_subtotal ELSE
                        ail.price_subtotal / get_rate('purchase', 'USD', CASE WHEN
                        ai.picking_dev_id IS NOT NULL THEN sp.date ELSE ai.invoice_datetime END) END as sale,
                        ail.quantity as quantity,
                        ail.quantity *
                        CASE WHEN ai.picking_dev_id IS NOT NULL THEN sm.price_unit / get_rate('purchase', 'USD', sp.date)
                        ELSE (Select cost from product_price_history where product_template_id = pt.id and datetime <
                        ai.invoice_datetime order by datetime desc limit 1) / get_rate('purchase', 'USD', ai.invoice_datetime) END as cost,
                        CASE WHEN ai.picking_dev_id IS NOT NULL THEN sp.date
                        ELSE ai.invoice_datetime END as transaction_date,
                        get_rate('purchase', 'USD', CASE WHEN ai.picking_dev_id IS NOT NULL THEN sp.date ELSE ai.invoice_datetime END) as rate,
                        0.0 as utility,
                        acc.id as center_cost
                    FROM account_invoice_line as ail
                        INNER JOIN product_product as pp ON ail.product_id = pp.id
                        INNER JOIN product_template as pt ON pp.product_tmpl_id = pt.id
                        INNER JOIN product_category as pcs ON pt.section = pcs.id
                        INNER JOIN product_category as pcb ON pt.brand = pcb.id
                        INNER JOIN product_category as pcl ON pt.line = pcl.id
                        INNER JOIN account_invoice as ai ON ail.invoice_id = ai.id
                        LEFT JOIN stock_picking as sp ON ai.picking_dev_id = sp.id
                        LEFT JOIN stock_move as sm ON sm.picking_id = sp.id AND sm.product_id = pp.id
                        INNER JOIN res_currency as rc ON ai.currency_id = rc.id
                        LEFT JOIN account_cost_center as acc ON ai.cost_center_id = acc.id
                    WHERE pt.type NOT IN ('service') AND ai.type IN ('out_invoice', 'out_refund') AND ai.state IN ('open', 'paid')
                        ) T10
            );
        """
        )
        UtilityAnalysisGroup.create_r_utility_analysis_group(cr)
        UtilityAnalysisGroupDetail.create_r_utility_analysis_group_detail(cr)
