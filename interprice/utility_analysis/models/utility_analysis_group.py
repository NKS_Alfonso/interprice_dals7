# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, tools


class UtilityAnalysisGroup(models.Model):
    """Utility Analysis report"""

    # odoo model properties
    _name = 'r.utility.analysis.group'
    _table = 'r_utility_analysis_group'
    _description = 'Utility Analysis report'
    _auto = False  # Don't let odoo create table
    _order = 'year, month, section, brand'

    # initial methods

    # Selection list

    # View fields
    month_year = fields.Char(
        readonly=True,
        string=_("Month/Year"),
    )
    month = fields.Char(
        readonly=True,
        string=_("Month"),
    )
    year = fields.Char(
        readonly=True,
        string=_("Year"),
    )
    section = fields.Many2one(
        comodel_name='product.category',
        readonly=True,
        string=_("Section"),
    )
    brand = fields.Many2one(
        comodel_name='product.category',
        readonly=True,
        string=_("Brand"),
    )
    sale = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("Sales"),
    )
    cost = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("Cost"),
    )
    quantity = fields.Integer(
        readonly=True,
        string=_("Quantity"),
    )
    utility = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("Utility $"),
    )
    percentage_utility = fields.Float(
        digits=(10, 2),
        readonly=True,
        string=_("% Utility"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_r_utility_analysis_group(cr):
        tools.sql.drop_view_if_exists(cr, 'r_utility_analysis_group')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_utility_analysis_group AS (
                SELECT
                    row_number() OVER (ORDER BY month) AS id,
                    month_year,
                    month,
                    year,
                    section,
                    brand,
                    sum(sale) as sale,
                    sum(cost) as cost,
                    sum(quantity) as quantity,
                    round(sum(sale), 2) - round(sum(cost), 2) as utility,
                    sum(utility)/sum(sale)*100 as percentage_utility
                FROM r_utility_analysis
                    GROUP BY month, year, section, brand, month_year
                    ORDER BY year, month, section, brand);
            """
        )
