# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': 'Utility Analysis Report',

    'summary': "Genera reporte xls y csv.",
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    'category': 'Report',
    'version': '1.0',

    'depends': [
        'base',
        'product',
        'account',
        'account_voucher',
        'account_accountant',
        'account_check',
        'account_check_deposit',
        'account_financial_report_webkit',
        'stock',
        'centro_de_costos',
        'price_list',
        # 'client_advance_payment',
        'dev_notas_credito',
        # 'cfdi33',
        'reports'
    ],
    'data': [
        'wizard/views/utility_analysis_wizard_view.xml',
        'views/utility_analysis_group_view.xml',
        'views/utility_analysis_group_detail_view.xml',
        'security/security.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
}
