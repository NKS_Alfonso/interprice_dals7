** Analisis de Utilidad **
==========================

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Genera el reporte de utilidad por sección,
""""""""""""""""""""""""""""""""""""""""""
marca, linea o producto.
""""""""""""""""""""""""

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Permiso:
********

.. figure:: ../utility_analysis/static/img/permissions_utility_analysis.png
    :alt: Permisos
    :width: 100%

Menú:
*****

.. figure:: ../utility_analysis/static/img/menu_utility_analysis.png
    :alt: Menu
    :width: 100%

Filtros Aplicables:
*******************

.. figure:: ../utility_analysis/static/img/filter_utility_analysis.png
    :alt: Filtros Aplicables
    :width: 100%
