��    5      �  G   l      �  	   �  *   �     �     �     �     �     �            
     
   $  
   /     :  
   A     L     a     d     q     �     �     �     �     �  
   �     �     �     �     �  
                  +     4     A     M     S     Z  $   x     �     �     �  	   �     �     �                0     O     g     �     �     �  E  �  
   	  /   	  
   >	     I	     b	     h	     z	     �	     �	  
   �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     2
     L
     S
     f
     j
     s
     �
     �
     �
  
   �
     �
     �
     �
                     $      -  *   N     y     �     �  
   �     �     �     �  "         #     C     Y     y     �     �     
                       !   )                           $            3                 +   4              5                                "      1   #   ,                         	   -           2   *      .      &   (       '         /   %   0        % Utility 'Date to' must be higher than 'Date from'. Branch offices Branch offices selection Brand Brand of product Cancel Code of product Cost Created by Created on Excel File Filter Final date Final date to filter ID Initial date Initial date to filter Last Updated by Last Updated on Line Line of product Month Month/Year No records found! Product Product Code Product Detail Product ID Product code Product detail Quantity Reset values Responsible Sales Screen Search utility analysis group Search utility analysis group detail Section Section of product Type Document Utility $ Utility Analysis Utility Analysis Detail report Utility Analysis Report Utility Analysis Wizard Utility Analysis detail report Utility Analysis report Utility analysis with detail Utility analysis without detail UtilityAnalysis.csv Year Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-09 18:16+0000
PO-Revision-Date: 2018-02-14 14:31-0600
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
 % Utilidad 'Fecha inicial' debe ser mayor a 'Fecha Final'. Sucursales Selección de sucursales Marca Marca de producto Cancelar Código de producto Costo Creado por Creado en Archivo CSV Filtros Fecha final Fecha final para filtrar ID Fecha inicial Fecha inicial para filtrar Última actualización por Última actualización en Línea Línea de producto Mes Mes/año No se encontrarón registros! Producto Código de producto Detalle del producto Product ID Código de producto Detalle del producto Cantidad Resetear valores Responsable Ventas Pantalla Búsqueda, análisis de utilidad Búsqueda, análisis de utilidad detallado Sección Sección de producto Tipo de Documento Utilidad $ Análisis de utilidad Análisis de utilidad detallado Análisis de utilidad Asistente de análisis de utilidad Análisis de utilidad detallado Análisis de utilidad Análisis de utilidad detallado Análisis de utilidad Análisis_de_utilidad.csv Año 