# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from openerp import api
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_categories(http.Controller):
    
    @http.route(['/categorias'], type='json', auth="public", website=True, methods=['GET','POST'])
    def categories(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        list_result = None
        queryset = {}

        try:
            request.env.cr.execute("SELECT type FROM type_product")
            list_type1 = request.env.cr.fetchall()
            for l_type in list_type1:
                request.env.cr.execute(
                    """
                        SELECT
                            pc.id,
                            pc.name
                        FROM
                            product_category pc
                        LEFT JOIN
                            product_category_type_rel pct ON pct.product_category = pc.id 
                        LEFT JOIN
                            type_product ptype ON ptype.id = pct.type_product 
                        WHERE
                            ptype.type = '{}'
                    """.format(l_type[0])
                )
                categoria = l_type[0].encode("utf8", "ignore")
                list_result = request.env.cr.fetchall()
                if len(list_result) <= 0:
                    raise Exception(
                            {
                                'data': {},
                                'message': 'No se encuentran Categorias',
                                'message_detail': 'Categorias tipo {} no encontradas'.format(categoria),
                                'status': 404
                            }
                    )
                if 'Sección' in categoria:
                    queryset['seccion'] = dict(list_result)
                elif 'Línea' in categoria:
                    queryset['linea'] = dict(list_result)
                elif 'Marca' in categoria:
                    queryset['marca'] = dict(list_result)
                elif 'Serie' in categoria:
                    queryset['serie'] = dict(list_result)
                else:
                    raise Exception(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Categoria {} no se encuentra en la lista'.format(categoria),
                                'message_detail': 'Categorias tipo {} no encontradas'.format(categoria),
                                'status': 404
                            }
                    )

            # if success make response in data
            data = {
                'to_root': True,
                'data': {
                            'categorias': queryset
                        },
                'message': 'exitoso',
                'status': 200
            }

        except Exception as e:
            # Products not found
            data = list(e)[0]

        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }

        return data