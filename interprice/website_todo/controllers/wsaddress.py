# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_address(http.Controller):
    
    _client = {}
    _request = None

    @http.route(['/clientes/<pk>/direcciones/'], type='json', auth="public", website=True, methods=['GET'])
    def addressbyclient(self, **kwargs):
        address = {}
        address_invoice = {}
        address_delivery = []
        data = {}

        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data

        # Get params client_id
        client_id = kwargs.get('pk', False)
        cursor = request.env.cr
        try:
            if client_id.isdigit():
                cursor.execute(
                    """
                        SELECT
                            rrpi.*
                        FROM
                            restful_res_partner_info AS rrpi
                        WHERE
                            rrpi.id = {0}
                            OR
                            rrpi.parent_id = {0}
                    """.format(client_id)
                )
                result = cursor.fetchall()
                if len(result)>0:
                    for (id, parent_id, customer, address_type, street, no_int,
                         no_ext, settlement, colony, city, state, country,
                         postal_code, cost_center, pay_type) in result:
                        if not parent_id:
                            address_invoice.update(
                                {
                                    'calle': street,
                                    'n_externo': no_int,
                                    'n_interno': no_ext,
                                    'asentamieno': settlement,
                                    'colonia': colony,
                                    'ciudad': city,
                                    'estado': state,
                                    'c_postal': postal_code,
                                    'pais': country,
                                    'id': id,
                                    'tipo_pago': pay_type,
                                    'centro_costo': cost_center
                                }
                            )
                        else:
                            address_delivery.append(
                                {
                                    'calle': street,
                                    'n_externo': no_int,
                                    'n_interno': no_ext,
                                    'colonia': colony,
                                    'ciudad': city,
                                    'estado': state,
                                    'c_postal': postal_code,
                                    'pais': country,
                                    'id': id,
                                }
                            )
                    address['direcciones'] = {
                        'direccion_facturacion': address_invoice,
                        'direccion_envio': address_delivery
                    }
                else:
                    data.update(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'Sin direcciones',
                            'message_detail': 'Cliente no encontrado',
                            'status': 404
                        }
                    )
            else:
                tools.response_error(
                    expected='cliente',
                    data=client_id,
                    type_error='int'
                )
        except:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()),
                'status': 400
            }
        if not data:
            data = {
                'to_root': True,
                'data': address,
                'message': 'exitoso',
                'status': 200
            }
        return data
    
    @http.route(['/clientes/<pk>/direcciones/'], type='json', auth="public", website=True, methods=['POST'])
    def newaddrestoclient(self, request, **kwargs):
        data = {}
        registry = {}
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        pk = kwargs.get('pk', False)
        self._request = request
        if pk:
            if pk.isdigit():
                client = request.jsonrequest.get('cliente', {})
                city = client.get('ciudad', None)
                state = client.get('estado', None)
                country = client.get('pais', None)
                self._client = client
                validated = self.validate_post(client)
                if validated.get('validated', False):
                    is_company = False
                    name = self._client.get('nombre', '')                   # Nombre
                    display_name = self._client.get('nombre', '')           # Nombre
                    email = self._client.get('correo', '')                  # Correo
                    phone = self._client.get('telefono', '')                # Telefono
                    mobile = self._client.get('movil', '')                  # Movil
                    use_parent_address = False                              # Utilizar la dirección de la empresa
                    address_type = 'delivery'                               # Tipo_direccion
                    street = self._client.get('calle', '')                  # Calle
                    l10n_mx_street3 = self._client.get('n_externo', '')     # N°_Externo
                    l10n_mx_street4 = self._client.get('n_interno', '')     # N°_Interno
                    settlement_id = self._client.get('asentamiento', None)  # Asentamiento
                    street2 = self._client.get('colonia', None)             # Colonia
                    city_id = self._client.get('ciudad', None)              # Ciudad
                    state_id = self._client.get('estado', None)             # Estado
                    zip = self._client.get('c_postal', None)                # CP
                    country_id = self._client.get('pais', None)             # Pais
                    create_date = 'current_timestamp'                       # Fecha_creacion
                    write_date = 'current_timestamp'                        # Fecha_modificacion
                    parent_id = int(pk)                                     # Contacto
                    notify_email = 'always'                                 # Notificación
                    customer = False                                        # Cliente
                    active = True                                           # Activo
                    tz = 'America/Mexico_City'                              # Tz
                    try:
                        self._validate_exists_parent_id(pk)
                        if city and not city_id:
                            data = {
                                'to_root': True,
                                'data': {},
                                'message': 'error',
                                'message_detail': 'No se recibió una Ciudad valida.',
                                'status': 204
                            }
                            raise Exception(data)
                        if state and not state_id:
                            data = {
                                'to_root': True,
                                'data': {},
                                'message': 'error',
                                'message_detail': 'No se recibió un Estado valido.',
                                'status': 204
                            }
                            raise Exception(data)
                        if country and not country_id:
                            data = {
                                'to_root': True,
                                'data': {},
                                'message': 'error',
                                'message_detail': 'No se recibió un País valido.',
                                'status': 204
                            }
                            raise Exception(data)
    
                        # self.validate_email(email)
                        query = """
                            INSERT INTO
                                res_partner (
                                    is_company,         -- Es_compañia
                                    name,               -- Nombre
                                    display_name,       -- Nombre
                                    email,              -- Correo
                                    phone,              -- Telefono
                                    mobile,             -- Movil
                                    use_parent_address, -- Utilizar la dirección de la empresa
                                    type,               -- Tipo_direccion
                                    street,             -- Calle
                                    l10n_mx_street3,    -- Numero Externo
                                    l10n_mx_street4,    -- Numero Interno
                                    settlement_type,     -- Tipo asentamiento
                                    street2,            -- Colonia
                                    city,               -- Ciudad
                                    city_id,            -- Ciudad
                                    state_id,           -- Estado
                                    zip,                -- CP
                                    country_id,         -- Pais
                                    create_date,        -- Fecha_creacion
                                    write_date,         -- Fecha_modificacion
                                    parent_id,          -- Contacto
                                    notify_email,       -- Notificación
                                    customer,           -- Cliente
                                    active,             -- Activo
                                    tz                  -- Tz
                                )
                            VALUES
                                (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                                 %s,{},{},%s,%s,%s,%s,%s
                                )
                            RETURNING
                                id;
                        """.format(
                            create_date,       # Fecha_creacion
                            write_date,        # Fecha_modificacion
                        )
                        self._request.env.cr.execute(query, (
                            is_company,         # Es_compañia
                            name,               # Nombre
                            display_name,       # Nombre
                            email,              # Correo
                            phone,              # Telefono
                            mobile,             # Movil
                            use_parent_address,  # Utilizar la dirección de la empresa
                            address_type,       # Tipo_direccion
                            street,             # Calle
                            l10n_mx_street3,    # N°_Externo
                            l10n_mx_street4,    # N°_Interno
                            settlement_id,      # Tipo de asentamiento
                            street2,            # Colonia
                            city,               # Ciudad
                            city_id,            # Ciudad
                            state_id,           # Estado
                            zip,                # CP
                            country_id,         # Pais
                            parent_id,          # Contacto
                            notify_email,       # Notificación
                            customer,           # Cliente
                            active,             # Activo
                            tz                  # Tz
                            )
                        )
                        result = self._request.env.cr.fetchall()
                        if len(result) > 0:
                            registry.update({'registro': {'id': result[0][0]}})
                        else:
                            data.update(
                                {
                                    'to_root': True,
                                    'data': {},
                                    'message': 'No creado',
                                    'message_detail': 'Registro no creado',
                                    'status': 400
                                }
                            )
                    except Exception as e:
                        data = list(e)[0]
                    except:
                        data = {
                            'to_root': True,
                            'data': {},
                            'message': 'error',
                            'message_detail': '{}'.format(sys.exc_info()),
                            'status': 400
                        }
                else:
                    if validated.get('required', False):
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Campos obligatorios',
                                'message_detail': validated.get('fields', 'Uno o mas campos son obligatorios'),
                                'status': 406
                            }
                        )
                    else:
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'No se recibieron datos para el registro',
                                'message_detail': {
                                    'necessary_structure': {
                                       "cliente": {
                                           "nombre": "Requerido",
                                           "correo": "Requerido",
                                           "telefono": "No requerido",
                                           "movil": "No requerido",
                                           "calle": "Requerido",
                                           "n_externo": "Requerido",
                                           "n_interno": "Requerido",
                                           "colonia": "Requerido",
                                           "ciudad": "Requerido",
                                           "estado": "Requerido, ('Sonora', 'Michoacán')",
                                           "c_postal": "Requerido",
                                           "pais": "Requerido, ('México')"
                                       }
                                    }
                                },
                                'status': 204
                            }
                        )
                if not data:
                    data = {
                        'to_root': True,
                        'data': registry,
                        'message': 'exitoso',
                        'status': 200
                    }
            else:
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': 'Se esperaba un id de cliente entero',
                    'status': 400
                }
        else:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'No se recibió identificador para asociar el contacto y la dirección',
                'status': 204
            }
        return data
    
    @http.route(['/clientes/<pk>/direcciones/'], type='json', auth="public", website=True, methods=['PUT'])
    def editaddrestoclient(self, request, **kwargs):
        data = {}
        registry = {}

        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data

        cursor = request.env.cr
        pk = kwargs.get('pk', False)
        self._request = request
        if pk:
            if pk.isdigit():
                client = request.jsonrequest.get('cliente', {})
                city = client.get('ciudad', None)                           
                state = client.get('estado', None)
                country = client.get('pais', None)
                self._client = client
                validated = self.validate_put(client)
                if validated.get('validated', False):
                    name = self._client.get('nombre', '')                   
                    display_name = self._client.get('nombre', '')           
                    vat = self._client.get('rfc', '')                       
                    email = self._client.get('correo', '')                  
                    phone = self._client.get('telefono', '')                
                    mobile = self._client.get('movil', '')                  
                    street = self._client.get('calle', '')                  
                    l10n_mx_street3 = self._client.get('n_externo', '')     
                    l10n_mx_street4 = self._client.get('n_interno', '')     
                    settlement_id = self._client.get('asentamiento', None)  
                    street2 = self._client.get('colonia', None)             
                    city_id = self._client.get('ciudad', None)              
                    state_id = self._client.get('estado', None)             
                    zip = self._client.get('c_postal', None)                
                    country_id = self._client.get('pais', None)             
                    write_date = 'current_timestamp'                        
                    query = ""
                    query_0 = ""
                    if self._client or city:
                        query_0 = "UPDATE res_partner SET write_date = %s" % write_date
                        if name:
                            query += ", name = '%s'" % name
                        if display_name:
                            query += ", display_name = '%s'" % display_name
                        if vat:
                            query += ", vat = '%s'" % vat
                        if email:
                            query += ", email = '%s'" % email
                        if phone:
                            query += ", phone = '%s'" % phone
                        if mobile:
                            query += ", mobile = '%s'" % mobile
                        if street:
                            query += ", street = '%s'" % street
                        if l10n_mx_street3:
                            query += ", l10n_mx_street3 = '%s'" % l10n_mx_street3
                        if l10n_mx_street4:
                            query += ", l10n_mx_street4 = '%s'" % l10n_mx_street4
                        if settlement_id:
                            query += ", settlement_type = '%s'" % settlement_id
                        if street2:
                            query += ", street2 = '%s'" % street2
                        if city:
                            query += ", city = '%s'" % city
                        if city_id:
                            query += ", city_id = %s" % city_id
                        if state_id:
                            query += ", state_id = %s" % state_id
                        if zip:
                            query += ", zip = '%s'" % zip
                        if country_id:
                            query += ", country_id = %s" % country_id
                    if query:
                        query += " WHERE id = %s RETURNING id;" % int(pk)
                        query_0 += query
                        try:
                            self.validate_invoicing_address(pk, vat)
                            if city and not city_id:
                                data = {
                                    'to_root': True,
                                    'data': {},
                                    'message': 'error',
                                    'message_detail': 'No se recibió una Ciudad valida.',
                                    'status': 204
                                }
                                raise Exception(data)
                            if state and not state_id:
                                data = {
                                    'to_root': True,
                                    'data': {},
                                    'message': 'error',
                                    'message_detail': 'No se recibió un Estado valido.',
                                    'status': 204
                                }
                                raise Exception(data)
                            if country and not country_id:
                                data = {
                                    'to_root': True,
                                    'data': {},
                                    'message': 'error',
                                    'message_detail': 'No se recibió un País valido.',
                                    'status': 204
                                }
                                raise Exception(data)
                            cursor.execute(query_0)
                            if cursor.rowcount:
                                registry.update(
                                    {
                                        'actualizado': {'id': cursor.fetchone()[0]}
                                    }
                                )
                            else:
                                data.update(
                                    {
                                        'to_root': True,
                                        'data': {},
                                        'message': 'No actualizado',
                                        'message_detail': 'Registro no actualizado',
                                        'status': 400
                                    }
                                )
                        except Exception as e:
                            data = list(e)[0]
                        except:
                            data = {
                                'to_root': True,
                                'data': {},
                                'message': 'error',
                                'message_detail': '{}'.format(sys.exc_info()),
                                'status': 400
                            }
                    else:
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'No actualizado',
                                'message_detail': 'Registro no actualizado, campos vacíos',
                                'status': 400
                            }
                        )
                else:
                    if validated.get('required', False):
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Campos obligatorios',
                                'message_detail': validated.get('fields', 'Uno o mas campos son obligatorios'),
                                'status': 406
                            }
                        )
                    else:
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'No se recibieron datos para actualizar el registro',
                                'message_detail': {
                                    'necessary_structure': {
                                        "cliente": {
                                            "nombre": "No Requerido",
                                            "rfc": "No Requerido",
                                            "telefono": "No requerido",
                                            "movil": "No requerido",
                                            "calle": "No Requerido",
                                            "n_externo": "No Requerido",
                                            "n_interno": "No Requerido",
                                            "colonia": "No Requerido",
                                            "ciudad": "No Requerido, ('Agua Prieta', 'Apatzingán')",
                                            "estado": "No Requerido, ('Sonora', 'Michoacán')",
                                            "c_postal": "No Requerido",
                                            "pais": "No Requerido, ('México')"
                                        }
                                    }
                                },
                                'status': 204
                            }
                        )
                if not data:
                    data = {
                        'to_root': True,
                        'data': registry,
                        'message': 'exitoso',
                        'status': 200
                    }
            else:
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': 'Se esperaba un id de cliente entero',
                    'status': 400
                }
        else:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'No se recibió identificador para actualizar el registro',
                'status': 204
            }
        return data
    
    @http.route(['/clientes/<pk>/direcciones/'], type='json', auth="public", website=True, methods=['DELETE'])
    def removeaddressbyclient(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        self._request = request
        pk = kwargs.get('pk', False)
        if pk.isdigit():
            validate = self.validate_delete(pk)
            if validate.get('delete', False):
                cursor = request.env.cr
                try:
                    cursor.execute(
                        """
                            DELETE FROM
                                res_partner
                            WHERE
                                id = {}
                            RETURNING
                                id;
                        """.format(pk)
                    )
                    if cursor.rowcount:
                        data.update(
                            {
                                'to_root': True,
                                'data': {'eliminado': {'id': cursor.fetchone()[0]}},
                                'message': 'exitoso',
                                'status': 200
                            }
                        )
                except:
                    data.update(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'error',
                            'message_detail': '{}'.format(sys.exc_info()),
                            'status': 400
                        }
                    )
            else:
                if validate.get('not_result', False):
                    data.update(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'error',
                            'message_detail': 'No se encontró registro a eliminar',
                            'status': 406
                        }
                    )
                else:
                    data.update(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'error',
                            'message_detail': 'No se puede eliminar un cliente activo',
                            'status': 406
                        }
                    )
        else:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'Se esperaba un id de cliente entero',
                'status': 400
            }
        return data
        
    def _validate_exists_parent_id(self, pk):
        data = {}
        tools = WSAPITools()
        if pk:
            parent_id = int(pk)
            
            self._request.env.cr.execute(
                """
                    SELECT
                        id
                    FROM
                        restful_res_partner_info
                    WHERE
                        id = {}
                """.format(parent_id)
            )
            result = self._request.env.cr.fetchall()
            if len(result)>0:
                return True
            else:
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'No encontrado',
                        'message_detail': 'Cliente principal no encontrado',
                        'status': 404
                    }
                )
                raise Exception(data)
        else:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'No se recibió identificador para asociar el contacto y la dirección',
                'status': 204
            }
            raise Exception(data)
    
    def validate_post(self, client):
        validated = {}
        if not client:
            validated.update(
                {
                    'validated': False,
                    'required': False
                }
            )
        if client:
            validated.update({'validated': True})
            fields = [
                self.validate_field('nombre', client.get('nombre', ''), None, 'char', None, True),                                 # Nombre    ==> Requerido
                self.validate_field('correo', client.get('correo', ''), None, 'char', None, False),                                # Correo
                self.validate_field('telefono', client.get('telefono', ''), None, 'char', None, False),                            # Telefono
                self.validate_field('movil', client.get('movil', ''), None, 'char', None, False),                                  # Movil
                self.validate_field('calle', client.get('calle', ''), None, 'char', None, True),                                   # Calle     ==> Requerido
                self.validate_field('n_externo', client.get('n_externo', ''), None, 'char', None, True),                           # Calle     ==> Requerido
                self.validate_field('n_interno', client.get('n_interno', ''), None, 'char', None, False),                          # Numer
                self.validate_field('asentamiento', client.get('asentamiento', ''), 'res_settlement_type', 'm2o', 'id', True),     # Asentamiento ==> Requerido
                self.validate_field('colonia', client.get('colonia', ''), 'res_state_city_colony', 'm2o', 'id', True),             # Colonia   ==> Requerido
                self.validate_field('ciudad', client.get('ciudad', ''), 'res_country_state_city', 'm2o', 'id', True),              # Ciudad ==> Requerido
                self.validate_field('estado', client.get('estado', ''), 'res_country_state', 'm2o', 'id', True),                   # Estado  ==> Requerido
                self.validate_field('c_postal', client.get('c_postal', ''), 'res_postal_code', 'm2o', 'id', True),                 # CP      ==> Requerido
                self.validate_field('pais', client.get('pais', ''), 'res_country', 'm2o', 'id', True),                             # Pais      ==> Requerido
            ]
            requred_fiels = {}
            for field in fields:
                if isinstance(field, dict):
                    validated.update(
                        {
                            'validated': False,
                            'required': True
                        }
                    )
                    requred_fiels.update(field)
            if validated.get('required', False):
                validated.update({'fields': requred_fiels})
        return validated
    
    def validate_put(self, client):
        validated = {}        
        if not client:
            validated.update(
                {
                    'validated': False,
                    'required': False
                }
            )
        if client:
            validated.update({'validated': True})
            fields = [
                self.validate_field('nombre', client.get('nombre', ''), None, 'char', None, False),                                # Nombre    ==> Requerido
                self.validate_field('rfc', client.get('rfc', ''), None, 'char', None, False),                                      # RFC
                self.validate_field('correo', client.get('correo', ''), None, 'char', None, False),                                # Correo
                self.validate_field('telefono', client.get('telefono', ''), None, 'char', None, False),                            # Telefono
                self.validate_field('movil', client.get('movil', ''), None, 'char', None, False),                                  # Movil
                self.validate_field('calle', client.get('calle', ''), None, 'char', None, False),                                  # Calle     ==> Requerido
                self.validate_field('n_externo', client.get('n_externo', ''), None, 'char', None, False),                          # Calle     ==> Requerido
                self.validate_field('n_interno', client.get('n_interno', ''), None, 'char', None, False),                          # Calle     ==> Requerido
                self.validate_field('asentamiento', client.get('asentamiento', ''), 'res_settlement_type', 'm2o', 'id', False),     # Asentamiento ==> Requerido
                self.validate_field('colonia', client.get('colonia', ''), 'res_state_city_colony', 'm2o', 'id', False),            # Colonia   ==> Requerido
                self.validate_field('ciudad', client.get('ciudad', ''), 'res_country_state_city', 'm2o', 'id', False),             # Ciudad ==> Requerido
                self.validate_field('estado', client.get('estado', ''), 'res_country_state', 'm2o', 'id', False),                  # Estado  ==> Requerido
                self.validate_field('c_postal', client.get('c_postal', ''), 'res_postal_code', 'm2o', 'id', False),                # CP      ==> Requerido
                self.validate_field('pais', client.get('pais', ''), 'res_country', 'm2o', 'id', False),                            # Pais      ==> Requerido
            ]
            for field in fields:
                if isinstance(field, dict):
                    validated.update(
                        {
                            'validated': False,
                            'required': True
                        }
                    )
                    validated.update({'fields': field})
        return validated
    
    def validate_delete(self, ids):
        result = {}
        if ids:
            tools = WSAPITools()
            cursor = self._request.env.cr
            try:
                cursor.execute(
                    """
                        SELECT
                            parent_id,
                            customer
                        FROM
                            res_partner
                        WHERE
                            id = {};
                    """.format(ids)
                )
                if cursor.rowcount:
                    parent, customer = cursor.fetchone()
                    if parent and not customer:
                        result.update({
                            "delete": True
                        })
                    else:
                        result.update({
                            "delete": False
                        })
                else:
                    result.update({
                        "not_result": True
                    })
            except:
                result.update({
                    "delete": False
                })
        return result
    
    def validate_field(self, field, value, table, type_field, search_field, required):
        if not isinstance(value, int):
            value = unicode(value).encode('utf8')
        new_value = None
        if type_field not in ('m2o', 'select'):
            new_value = value
        elif type_field == 'select':
            pass
        elif type_field == 'm2o':
            if value:
                new_value = self.search_value(search_field, table, value)
                if not new_value:
                    self._client[field] = None
        if new_value:
            self._client[field] = new_value
        else:
            if required:
                new_value = {}
                new_value.update({field: 'requerido'})
        return new_value
    
    def search_value(self, search_field, table, value):
        new_value = None
        tools = WSAPITools()
        cursor = self._request.env.cr
        query = "SELECT id FROM {} WHERE {} = {}".format(table, search_field,value)
        cursor.execute(query)
        if cursor.rowcount:
            new_value = cursor.fetchone()[0]
        return new_value

    def validate_email(self, email):
        registry = {}
        tools = WSAPITools()
        cursor = self._request.env.cr
        cursor.execute(
            """
                SELECT
                    id,
                    name,
                    email
                FROM
                    res_partner
                WHERE
                    email = '{}'
                LIMIT 1;
            """.format(email)
        )
        if cursor.rowcount:
            registry.update({'registro': {'id': cursor.fetchone()[0]}})
            data = {
                'to_root': True,
                'data': registry,
                'message': 'error',
                'message_detail': 'El correo ya existe y debe ser único'.decode('utf8'),
                'status': 400
            }
            raise Exception(data)
        else:
            return True

    def validate_invoicing_address(self, ids, vat):
        if vat:
            registry = {}
            tools = WSAPITools()
            cursor = self._request.env.cr
            cursor.execute(
                """
                    SELECT
                        id,
                        name,
                        type
                    FROM
                        res_partner
                    WHERE
                        id = {};
                """.format(ids)
            )
            if cursor.rowcount:
                partner_id, partner_name, type_address = cursor.fetchone()
                if type_address == 'invoice':
                    return True
                else:
                    registry.update({'registro': {'id': partner_id}})
                    data = {
                        'to_root': True,
                        'data': registry,
                        'message': 'error',
                        'message_detail': 'El registro no es de facturación'.decode('utf8'),
                        'status': 400
                    }
                    raise Exception(data)
        else:
            return True