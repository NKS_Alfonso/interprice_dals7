# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
import math
_logger = logging.getLogger(__name__)

class ws_todo_orders(http.Controller):
    
    _tools = None
    _request = None
    _sequence = 0
    
    @http.route(['/clientes/<pk>/pedidos/'], type='json', auth="public", website=True, methods=['GET'])
    def ordersbyclient(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        queryset = []
        results = None
        state = None
        self._tools = WSAPITools()

        # Get Cursor of the DB
        cursor = request.env.cr
        pk = kwargs.get('pk', False)
        try:
            if pk.isdigit():
                # We validate client exists in DB
                pk = self._tools.validate_identificator(
                    'cliente',
                    'res_partner',
                    'id',
                    pk,
                    cursor
                )
                query_get_order = """
                    SELECT
                        so.id,
                        so.name,
                        so.date_order,
                        rc.name,
                        so.amount_total,
                        so.amount_untaxed,
                        so.amount_tax,
                        so.state,
                        pt.name
                    FROM
                        sale_order as so
                    LEFT JOIN
                        res_partner as pt ON pt.id = so.partner_id
                    LEFT JOIN
                        res_currency as rc ON rc.id = so.currency
                    WHERE
                        so.partner_id = {}
                """.format(pk)
                cursor.execute(query_get_order)
                results = cursor.fetchall()
                if len(results) <= 0:
                    info = 'Pedidos no encontrados'
                    message = 'El cliente no tiene Pedidos'
                    self._tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                for (order_id,
                     order_no,
                     order_date,
                     curren,
                     order_total,
                     order_untaxed,
                     order_tax, order_state, partner) in results:
                    invoice_dict = []
                    # Save amount total of the sale_order
                    total = float(("{:.2f}".format(order_total)))
                    # we Verify state of sales_order to looking a invoice,
                    # if state is complete
                    query_get_invoice = """
                        SELECT
                            ai.id,
                            ai.number,
                            ai.amount_total,
                            ai.state
                        FROM
                            account_invoice as ai
                        LEFT JOIN
                            stock_picking as sp ON sp.id = ai.picking_dev_id
                        WHERE
                            ai.partner_id = {}
                            AND
                            sp.origin = '{}'
                            AND
                            ai.state_document = 'signed'
                    """.format(pk,order_no)
                    cursor.execute(query_get_invoice)
                    if cursor.rowcount:
                        invoice = cursor.fetchall()
                        if invoice:
                            for (invoice_id, invoice_number, invoice_amount,
                                 invoice_state) in invoice:
                                total = float(("{:.2f}".format(invoice_amount)))
                                invoice_dict.append(
                                    {
                                        'id': invoice_id,
                                        'factura_no': invoice_number,
                                        'estatus': invoice_state
                                    }
                                )
                    if 'draft' in order_state:
                        state = 'cotizacion'
                    elif 'progress' in order_state:
                        state = 'apartado'
                    elif 'cancel' in order_state:
                        state = 'cancelado'
                    elif 'done' in order_state:
                        state = 'completado'
                    else:
                        state = 'inseguro'
                    queryset.append(
                        {
                            'id': order_id,
                            'cliente': partner,
                            'identificador': order_no,
                            'fecha_realizado': order_date,
                            'moneda': curren,
                            'estatus': state,
                            'monto_total_sin_iva': float(("{:.2f}".format(order_untaxed))),
                            'monto_total_iva': float(("{:.2f}".format(order_tax))),
                            'monto_total': total,
                            'factura': invoice_dict
                        }
                    )
                # if success make response in data
                data = {
                    'to_root': True,
                    'data': {
                                'pedidos': queryset
                            },
                    'message': 'exitoso',
                    'status': 200
                }
            else:
                self._tools.response_error(
                    expected='cliente',
                    data=pk,
                    type_error='int'
                )
        except Exception as e:
            # Products not found
            data = list(e)[0]
        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }
        return data
    
    @http.route(['/clientes/<pk>/pedidos/'], type='json', auth="public", website=True, methods=['POST'])
    def createorder(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        products = None
        type_pay = None
        shipping = None
        reference = None
        self._request = request
        self._tools = WSAPITools()
        # Get Cursor of the DB
        cursor = request.env.cr
        # Get data via POST
        order = request.jsonrequest.get('pedido', {})
        pk = kwargs.get('pk', False)
        try:
            if pk.isdigit():
                # We validate client exists in DB
                pk = self._tools.validate_identificator(
                    'cliente',
                    'res_partner',
                    'id',
                    pk,
                    cursor
                )
                # We validate if client is unlocked
                self._tools.validate_unlock_client(pk, cursor)
                # We validate integrity data receive
                if not order:
                    info = 'Peticion sin Informacion'
                    message = """
                        La peticion no contiene informacion para realizar el pedido
                    """
                    self._tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                else:
                    products = self._validate_order(
                        'productos',
                        order.get('productos', None),
                        cursor
                    )
                    type_pay = self._validate_order(
                        'forma de pago',
                        order.get('tipo_pago', None),
                        cursor
                    )
                    send_address = self._validate_order(
                        'direccion de envio',
                        order.get('direccion_envio', None),
                        cursor,
                        pk=pk
                    )
                    shipping = self._validate_order(
                        'metodo de envio',
                        order.get('metodo_envio', None),
                        cursor
                    )
                    price_shipp = float(order.get('precio_envio', 0.0))
                    reference = order.get('referencia', None)
    
                # Get data Products
                products = self.get_data_products(cursor, products, pk)
                # Add the shipping to order_lines
                query_method_shipping = """
                    SELECT
                        pp.id,
                        pt.id,
                        pt.uom_id,
                        dc.name,
                        pt.sale_delay,
                        pp.default_code
                    FROM
                        delivery_carrier as dc
                    LEFT JOIN
                        product_product as pp ON pp.id = dc.product_id
                    LEFT JOIN
                        product_template as pt ON pt.id = pp.product_tmpl_id
                    WHERE
                        dc.id = {}
                """.format(shipping)
                cursor.execute(query_method_shipping)
                (shi_product_id,
                 shi_product_tmpl_id,
                 shi_product_uom,
                 shi_product_name,
                 shi_product_delay, shi_product_sku) = cursor.fetchone()
                sequence_number = self._sequence + 1
                products.get('order_lines').append(
                    [
                        shi_product_id,
                        shi_product_tmpl_id,
                        shi_product_uom,
                        shi_product_name,
                        price_shipp,
                        shi_product_delay,
                        1,
                        shi_product_sku,
                        0,
                        0,
                        True,
                        sequence_number
                    ]
                )
                # We calculate the amounts of the products plus shipping
                amount_untaxed = products.get('amount_untaxed') + ((float("{:.2f}".format(price_shipp))))
                # We Calculate the amount Tax of the products plus shipping
                amount_tax = products.get('amount_tax') + ((float("{:.2f}".format(price_shipp * 0.16))))
                # We Calculate the amount total of the products
                amount_total = amount_untaxed + amount_tax
    
                # -------------------------------------------------------------
                # Set data values to save in the inserts
                values = {
                    'create_date': "now() at time zone 'UTC'",
                    'date_order': "now() at time zone 'UTC'",
                    'm_date': "current_timestamp + interval '{} days'".format(products.get('delay')),
                    'partner_id': int(pk),
                    'company_id': 1,
                    'order_state': 'draft',
                    'pricelist_id': 1,
                    'amount_untaxed': amount_untaxed,
                    'amount_tax': amount_tax,
                    'amount_total': amount_total,
                    'write_date': "now() at time zone 'UTC'",
                    'partner_shipping_id': send_address,
                    'order_policy': 'picking',
                    'picking_policy': 'one',
                    'warehouse_id': self.get_warehouse(cursor),
                    'forma_pago': type_pay,
                    'currency': self.get_currency(cursor),
                    'carrier_id': shipping,
                    'user_id': 1,
                    'order_is_web': True,
                    'picking_type': 2,
                    'move_type': 'one',
                    'picking_state': 'assigned',
                    'priority': 1,
                    'invoice_state': '2binvoiced',
                    'weight_uom_id': 3,
                    'weight': 0.00,
                    # 'location_id': 12,
                    # 'location_dest_id': 9,
                    'procure_method': 'make_to_stock',
                    'client_order_ref': reference,
                    'create_uid': 1,
                    'shipped': False
                }
                # -------------------------------------------------------------
                # Create a sale_order
                values = self._insert_order(
                    cursor,
                    values,
                    products.get('order_lines')
                )
                # --------------------- ONLY PROCESS DIA
                # Validamos si existe logbook
                logbookQuery = """
                    SELECT
                        *
                    FROM
                        pg_tables
                    WHERE
                        tablename='sale_order_logbook'
                """
                cursor.execute(logbookQuery)
                if cursor.rowcount:
                    values = self._insert_dia_process_logbook(cursor, values)
    
                # if values['order_lines']:
                #     # Insert data to create a procurement_group
                #     values = self._insert_procurement_group(cursor, values)
                #     # Insert data to create a stock_picking
                #     values = self._insert_picking(cursor, values, move_lines)
                #     if values['move_lines']:
                #         self._update_state_order(cursor,
                #                                  values['order_id'],
                #                                  values['group_id'])
    
                # If success
                data.update(
                    {
                        'to_root': True,
                        'data': {
                            'pedido': {
                                'id': values.get('order_id'),
                                'identificador': values.get('order_name')
                             }
                        },
                        'message': 'exitoso',
                        'status': 200
                    }
                )
            else:
                self._tools.response_error(
                    expected='cliente',
                    data=pk,
                    type_error='int'
                )
        except Exception as e:
            if e.message != '':
                data = list(e)[0]
            elif hasattr(e, 'strerror'):
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'error de conexion con odoo',
                        'message_detail': e.strerror,
                        'status': 400
                    }
                )
            elif hasattr(e, 'faultString'):
                if isinstance(e.faultString, str):
                    result = e.faultString
                else:
                    result = eval(e.faultString)
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message_detail': result,
                        'status': 400
                    }
                )
            else:
                data = e
        except:
            # Error
            data.update(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(sys.exc_info()[1]),
                    'status': 400
                }
            )
        return data

    def next_sequence(self, number):
        next = int(number) + 1
        self._sequence = next
        return next

    def get_warehouse(self, cursor):
        warehouse_id = False
        warehouse_query = """
            SELECT
                sw.id
            FROM
                stock_warehouse as sw
            LEFT JOIN
                stock_warehouse_type as swt ON swt.id = sw.stock_warehouse_type_id
            WHERE
                swt.code = 'fac'
            LIMIT 1;
        """
        cursor.execute(warehouse_query)
        if cursor.rowcount:
            warehouse_id = cursor.fetchone()[0]
        else:
            warehouse_id = 1
        return warehouse_id

    def get_currency(self, cursor):
        currency_id = False
        currency_query = """
            SELECT
                currency_id
            FROM
                res_company
            LIMIT 1;
        """
        cursor.execute(currency_query)
        if cursor.rowcount:
            currency_id = cursor.fetchone()[0]
        else:
            currency_id = 1
        return currency_id

    def get_data_products(self, cursor, products, pk):
        order_lines = []
        not_availability = []
        delay = 0
        qty_total = 0
        amount_tax = 0
        amount_untaxed = 0
        weight_total = 0
        volume_total = 0
        for product in products:
            sku = product.get('sku')
            qty = product.get('cantidad')
            price = float(product.get('precio'))
            # Get sequence to position a sale_order_line
            sequence_number = self.next_sequence(self._sequence)
            # check availability of products to proccess sale_order
            quants_availability = self.check_availability(sku, qty, cursor)
            if not quants_availability:
                not_availability.append(sku)
            # Get data products
            query_get_product = """
                SELECT  pp.id,
                        pt.id,
                        pt.uom_id,
                        pt.name,
                        pt.sale_delay,
                        pp.default_code,
                        pt.weight,
                        pt.volume
                FROM
                    product_product as pp
                LEFT JOIN
                    product_template as pt ON pt.id = pp.product_tmpl_id
                WHERE
                    pp.default_code = '{}';
            """.format(sku)
            cursor.execute(query_get_product)
            (product_id,
             product_tmpl_id,
             product_uom,
             product_name,
             product_delay,
             product_sku,
             product_weight, product_volume) = cursor.fetchone()
            order_lines.append(
                [
                    product_id,
                    product_tmpl_id,
                    product_uom,
                    product_name,
                    price,
                    product_delay,
                    qty,
                    product_sku,
                    product_weight,
                    product_volume,
                    False,
                    sequence_number
                ]
            )
            # Calculate amount total without tax
            price_quantity = price * qty
            amount_untaxed += float("{:.2f}".format(price_quantity))
            # Calculate amount tax total
            amount_tax += float("{:.2f}".format(price_quantity * 0.16))
            # Calculate delay days
            if product_delay > delay:
                delay = product_delay
            # Calculate totals to calculate shipping_cost
            product_weight = product_weight if product_weight is not None else 0.0
            product_volume = product_volume if product_volume is not None else 0.0
            weight_total += float(float(("{:.2f}".format(product_weight))) or 0.0) * qty
            volume_total += float(float(("{:.2f}".format(product_volume))) or 0.0) * qty
            # Calculate totals quantity
            qty_total += qty

        return {
            'delay': delay,
            'amount_tax': amount_tax,
            'amount_untaxed': amount_untaxed,
            'qty_total': qty_total,
            'weight_total': weight_total,
            'volume_total': volume_total,
            'not_availability': not_availability,
            'order_lines':  order_lines
        }

    # ----------------------
    # Validate data
    def _validate_order(self, expected, data, cursor, pk=None, not_price=False):
        self._tools = WSAPITools()
        if not data:
            info = 'Datos incompletos en peticion Post'
            message = """
                La peticion no contiene {}, es necesaria
            """.format(expected)
            self._tools.response_error(
                info,
                type_error='custom',
                message=message
            )
        if 'productos' in expected:
            for product in data:
                missing_data = False
                product_missing = False
                sku = product.get('sku')
                qty = product.get('cantidad')
                price = product.get('precio')
                if not sku:
                    missing_data = True
                    product_missing = 'el SKU'
                if not qty:
                    missing_data = True
                    product_missing = 'la cantidad'
                if not price and not not_price:
                    missing_data = True
                    product_missing = 'el precio'
                if missing_data:
                    info = 'Faltan datos en los productos'
                    message = """
                        Falta {} en la lista de productos
                    """.format(product_missing)
                    self._tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                sku = "'"+sku+"'"
                sku = sku.decode('unicode-escape')
                if not isinstance(sku, unicode):
                    self._tools.response_error(
                        expected,
                        data=sku,
                        type_error='str'
                    )
                if not isinstance(qty, int):
                    self._tools.response_error(
                        expected,
                        data=qty,
                        type_error='int'
                    )
                self.exists_data(
                    expected,
                    sku,
                    'product_product',
                    'default_code',
                    cursor
                )
        elif 'forma de pago' in expected:
            data = self.exists_data(
                expected,
                data,
                'piramide_de_pago',
                'id',
                cursor
            )
        elif 'direccion de envio' in expected:
            data = self.exists_data(
                expected,
                data,
                'res_partner',
                'id',
                cursor
            )
            parent = self.verify_parent(cursor, data, pk)
            if not parent:
                info = 'Id de direccion de envio no Valido'
                message = """
                    La direccion de envio no es valida para este cliente
                """
                self._tools.response_error(
                    info,
                    type_error='custom',
                    message=message
                )
        elif 'metodo de envio' in expected:
            data = self.exists_data(
                expected,
                data,
                'delivery_carrier',
                'id',
                cursor
            )
        else:
            self._tools.response_error(
                expected,
                data=data,
                type_error='post_fail'
            )
        return data

    def exists_data(self, expected, data, model, field, cursor):
        self._tools = WSAPITools()
        if 'product' not in expected:
            if not isinstance(data, int):
                self._tools.response_error(
                    expected,
                    data=data,
                    type_error='int'
                )
        query = """
            SELECT
                id
            FROM
                {}
            WHERE
                {} = {}
        """.format(model, field,data)
        cursor.execute(query)
        if cursor.rowcount:
            response = data
        else:
            self._tools.response_error(expected, data=data)
        return response

    def verify_parent(self, cursor, partner_id, pk):
        data = True
        query_verify_parent = """
            SELECT
                parent_id
            FROM
                res_partner
            WHERE
                id = {}
        """.format(partner_id)
        cursor.execute(query_verify_parent)
        if cursor.rowcount:
            parent_id = cursor.fetchone()[0]
            if parent_id != int(pk):
                data = False
        return data

    def check_availability(self, sku, qty, cursor):
        result = None
        ########OJOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
        query_quants = """
            SELECT
                SQ.id
            FROM
                stock_quant as SQ
            LEFT JOIN
                product_product as PP ON PP.id = SQ.product_id
            WHERE
                PP.default_code = '{}'
                AND
                SQ.reservation_id IS NULL
                AND
                SQ.location_id = 12
        """.format(sku)
        cursor.execute(query_quants)
        if cursor.rowcount >= qty:
            result = cursor.fetchall()
        else:
            result = False
        return result

    # ----------------------
    #  Inserts to DB
    def _insert_order(self, cursor, data, order_lines):
        # Looking a valid secuence to sale_order
        tools = WSAPITools()
        data['order_name'] = tools.get_sequence_odoo(self._request, 'sale.order')
        # ---------------------------------------------------------
        # Insert data in sale_order
        try:
            query_create_sale_order = """
                INSERT INTO
                    sale_order (
                        create_date,
                        date_order,
                        partner_id,
                        amount_tax,
                        amount_untaxed,
                        company_id,
                        state,
                        pricelist_id,
                        write_date,
                        partner_invoice_id,
                        amount_total,
                        name,
                        partner_shipping_id,
                        order_policy,
                        picking_policy,
                        warehouse_id,
                        forma_pago,
                        currency,
                        carrier_id,
                        user_id,
                        order_is_web,
                        client_order_ref,
                        create_uid,
                        shipped
                    )
                VALUES (
                    now() at time zone 'UTC',
                    now() at time zone 'UTC',
                    {}, {}, {}, {} , '{}', {},
                    now() at time zone 'UTC',
                    {}, {}, '{}',
                    {}, '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}
                )
                RETURNING
                    id;
            """.format(
                data.get('partner_id'),         # Id del Cliente
                data.get('amount_tax'),         # monto del IVA
                data.get('amount_untaxed'),     # Monto total sin IVA
                data.get('company_id'),         # Id de la compañia
                data.get('order_state'),        # Status
                data.get('pricelist_id'),       # Id de Pricelist
                data.get('partner_id'),         # Id partner de Factura
                data.get('amount_total'),       # Monto total
                data.get('order_name'),         # Numero Identificacion
                data.get('partner_shipping_id'),  # Id Partner de Envio
                data.get('order_policy'),       # Politicas del pedido
                data.get('picking_policy'),     # Politicas de Envio
                data.get('warehouse_id'),       # Almacen
                data.get('forma_pago'),         # Id Forma de Pago
                data.get('currency'),           # Id de tipo de Moneda
                data.get('carrier_id'),         # Id tipo de Envio
                data.get('user_id'),            # Id Usuario
                data.get('order_is_web'),       # Es un pedido web
                data.get('client_order_ref'),   # Referencia
                data.get('create_uid'),         # creado por
                data.get('shipped')    
            )
            cursor.execute(query_create_sale_order)
            if cursor.rowcount:
                order_id = cursor.fetchone()[0]
                in_workflow = self._tools.instance_workflow_odoo(self._request, order_id)
                if in_workflow:
                    data['order_id'] = order_id
                    # Create the lines of sale_order
                    lines_ids = self._insert_order_line(cursor, data, order_lines)
                    data['order_lines'] = lines_ids
                    return data
        except Exception as e:
            raise Exception(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'Falla al insertar en sale_order',
                    'message_detail': e.message,
                    'status': 404
                }
            )
        return False

    def _insert_order_line(self, cursor, data, order_lines):
        # ---------------------------------------------------------
        # Insert data in sale_order_line
        line_ids = []
        price = 0
        try:
            for (product_id,
                 product_tmpl_id,
                 product_uom,
                 product_name,
                 price,
                 product_delay,
                 product_qty,
                 product_sku,
                 product_weight,
                 product_volume,
                 is_delivery, sequence_number) in order_lines:
                query_create_sale_order_line = """
                    INSERT INTO
                        sale_order_line (
                            create_date,
                            product_uos_qty,
                            product_uom,
                            price_unit,
                            product_uom_qty,
                            invoiced,
                            company_id,
                            name,
                            delay,
                            state,
                            order_partner_id,
                            order_id,
                            write_date,
                            product_id,
                            is_delivery,
                            sequence
                        )
                    VALUES (
                        {}, %s, %s, %s, %s, %s, %s, %s,
                        %s, %s, %s, %s, {}, %s, %s, %s
                    )
                    RETURNING id ;
                """.format(data.get('create_date'), data.get('write_date'))
                cursor.execute(query_create_sale_order_line,
                    [
                        product_qty,                  # Cantidad de Productos
                        product_uom,                  # Id uom del Producto
                        price,                        # Precio Unico
                        product_qty,                  # Cantidad de Productos
                        False,                        # Invoiced
                        data.get('company_id'),       # Id de la compañia
                        product_name,                 # Nombre del producto
                        product_delay,                # Dias en llegar
                        data.get('order_state'),      # Status
                        data.get('partner_id'),       # Id del cliente
                        data.get('order_id'),         # Id del Order
                        product_id,                   # Id del Producto
                        is_delivery,                         # Is delivery
                        sequence_number,
                    ]
                )
                if cursor.rowcount:
                    sale_order_line_id = cursor.fetchone()[0]
                    line_ids.append(sale_order_line_id)
                    product_tax_query = """
                        SELECT
                            tax_id
                        FROM
                            product_taxes_rel
                        WHERE
                            prod_id = {};
                    """.format(product_tmpl_id)
                    cursor.execute(product_tax_query)
                    if cursor.rowcount:
                        tax_id = cursor.fetchone()
                        for tax in tax_id:
                            sale_order_tax_query = """
                                INSERT INTO
                                    sale_order_tax(
                                        order_line_id,
                                        tax_id
                                    )
                                    VALUES(
                                        {},{}
                                    );
                            """.format(sale_order_line_id,tax)
                            cursor.execute(sale_order_tax_query)
                else:
                    line_ids = False
        except Exception as e:
            # if except so delete
            if data.get('order_id'):
                self._delete_table(
                    cursor,
                    table='sale_order',
                    pk=data.get('order_id')
                )
                if len(line_ids) > 0:
                    for line in line_ids:
                        self._delete_table(
                            cursor,
                            table='sale_order_line',
                            pk=line
                        )
            raise Exception(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'Falla al insertar en sale_order_line',
                    'message_detail': e.message,
                    'status': 404
                })
        return line_ids

    def _insert_dia_process_logbook(self, cursor, data):
        # Looking a valid secuence
        tools = WSAPITools()
        data['process_dia_name'] = tools.get_sequence_odoo(self._request,
                                                           'process.dia')
        process_dia_query = """
            INSERT INTO
                process_dia(
                    order_id,
                    name,
                    process_date_start,
                    state
                )
            VALUES (
                {}, '{}', {}, '{}'
            )
            RETURNING id;
        """.format(
            data.get('order_id'),
            data.get('process_dia_name'),
            data.get('create_date'),
            'starter'
        )
        cursor.execute(process_dia_query)
        if cursor.rowcount:
            data['process_dia_id'] = cursor.fetchone()[0]
            partner_name = 'Webservice Default'
            process_type_query = """
                SELECT
                    id
                FROM
                    process_dia_type
                WHERE
                    app_identification = 'createso'
            """
            cursor.execute(process_type_query)
            if cursor.rowcount:
                process_type = cursor.fetchone()[0]
                # Creamos el logbook createso
                logbookcreate = """
                    INSERT INTO
                        sale_order_logbook(
                            sale_order_id,
                            process_type,
                            date_time_start,
                            date_time_end,
                            user_code,
                            user_code_finish,
                            check_to_validate,
                            state,
                            process_dia_id
                        )
                    VALUES (
                        {}, {}, {}, {}, '{}',
                        '{}', {}, '{}', {}
                    )
                    RETURNING id;
                """.format(
                    data.get('order_id'),
                    process_type,
                    data.get('create_date'),
                    data.get('create_date'),
                    partner_name,
                    partner_name,
                    True,
                    'done',
                    data.get('process_dia_id')
                )
                cursor.execute(logbookcreate)
                if cursor.rowcount:
                    data['logbook_id'] = cursor.fetchone()[0]
        return data

    def _insert_procurement_group(self, cursor, data):
        query_group = """
            INSERT INTO
                procurement_group(
                    create_date,
                    name,
                    move_type,
                    write_date,
                    partner_id
                )
            VALUES(
                {0}, '{2}', '{3}', {1}, {}
            )
            RETURNING id;
        """.format(
            data.get('create_date'),
            data.get('write_date'),
            data.get('order_name'),
            data.get('move_type'),
            data.get('partner_id')
        )
        cursor.execute(query_group)
        if cursor.rowcount:
            group_id = cursor.fetchone()[0]
            data['group_id'] = group_id
            return data
        return False

    def _insert_picking(self, cursor, data, move_lines):
        # Looking a valid secuence
        tools = WSAPITools()
        data['picking_name'] = tools.get_sequence_odoo(self._request,
                                                       'stock.picking.out')
        # ---------------------------------------------------------
        # Insert data in stock_picking
        try:
            query_stock_picking = """
                INSERT INTO
                    stock_picking (
                        origin,
                        create_date,
                        partner_id,
                        priority,
                        picking_type_id,
                        move_type,
                        company_id,
                        state,
                        min_date,
                        write_date,
                        date,
                        name,
                        max_date,
                        group_id,
                        invoice_state,
                        weight,
                        carrier_id,
                        weight_uom_id,
                        weight_net,
                        origen_nacional_internacional
                    )
                VALUES (
                    %s, {}, %s, %s, %s, %s, %s, %s,
                    {}, {}, {}, %s, {}, %s, %s, %s, %s, %s, %s, %s
                )
                RETURNING id ;
            """.format(
                data.get('create_date'),
                data.get('m_date'),
                data.get('write_date'),
                data.get('date_order'),
                data.get('m_date')
            )
            cursor.execute(query_stock_picking,
                [
                    data.get('order_name'),      # Numero de origen SO
                    data.get('partner_id'),      # Numero de cliente
                    data.get('priority'),        # Prioridad
                    data.get('picking_type'),    # Tipo de Albaran
                    data.get('move_type'),       # Tipo de movimiento
                    data.get('company_id'),      # Id de la compañia
                    data.get('picking_state'),   # status del albaran
                    data.get('picking_name'),    # Numero de Identificador
                    data.get('group_id'),        # Id grupo confirmar order
                    data.get('invoice_state'),   # esta en none
                    data.get('weight'),          # Peso
                    data.get('carrier_id'),      #
                    data.get('weight_uom_id'),   #
                    data.get('weight'),          # peso neto
                    False
                ]
            )
            if cursor.rowcount:
                picking_id = cursor.fetchone()[0]
                data['picking_id'] = picking_id
                # Create the lines of sale_order
                move_ids = self._insert_stock_move(cursor, data, move_lines)
                data['move_lines'] = move_ids
                return data
        except Exception as e:
            # if except so delete
            if data.get('order_id'):
                self._delete_table(
                    cursor,
                    table='sale_order',
                    pk=data.get('order_id')
                )
                if len(data.get('order_lines')) > 0:
                    for line in data.get('order_lines'):
                        self._delete_table(
                            cursor,
                            table='sale_order_line',
                            pk=line
                        )
                if data.get('group_id'):
                    self._delete_table(
                        cursor,
                        table='procurement_group',
                        pk=data.get('group_id')
                    )
            raise Exception(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'Falla al insertar en stock_picking',
                    'message_detail': e.message,
                    'status': 404
                }
            )
        return False

    def _insert_stock_move(self, cursor, data, move_lines):
        # ---------------------------------------------------------
        # Insert data in stock_move
        move_ids = []
        price = 0
        try:
            for (product_id,
                 product_tmpl_id,
                 product_uom,
                 product_name,
                 price,
                 product_delay,
                 product_qty,
                 product_sku,
                 product_weight,
                 product_volume, is_delivery) in move_lines:
                query_stock_move = """
                    INSERT INTO
                        stock_move (
                            origin,
                            product_uos_qty,
                            create_date,
                            product_uom,
                            price_unit,
                            product_uom_qty,
                            company_id,
                            date,
                            product_qty,
                            location_id,
                            picking_type_id,
                            partner_id,
                            state,
                            date_expected,
                            name,
                            warehouse_id,
                            procure_method,
                            product_id,
                            picking_id,
                            location_dest_id,
                            write_date,
                            invoice_state,
                            weight,
                            weight_net,
                            weight_uom_id
                        )
                    VALUES (
                        %s, %s, {}, %s, %s, %s, %s, {}, %s, %s,
                        %s, %s, %s, {}, %s, %s, %s, %s, %s, %s,
                        {}, %s, %s, %s, %s
                    )
                    RETURNING id ;
                """.format(
                    data.get('create_date'),
                    data.get('m_date'),
                    data.get('m_date'),
                    data.get('write_date')
                )
                cursor.execute(query_stock_move,
                    [
                        data.get('order_name'),     # Nombre origen SO
                        product_qty,                # Cantidad de Productos
                        product_uom,                # Id uom del Producto
                        price,                      # Precio Unico
                        product_qty,                # Cantidad de Productos
                        data.get('company_id'),     # Id de la compañia
                        product_qty,                # Cantidad de Productos
                        data.get('location_id'),    # Ubicacion Actual
                        data.get('picking_type'),   # Tipo de Albaran
                        data.get('partner_id'),     # Id del cliente
                        data.get('picking_state'),  # status del albaran
                        product_name,               # Nombre del producto
                        data.get('warehouse_id'),   # Id del Almacen
                        data.get('procure_method'),  #
                        product_id,                 # Id del Product
                        data.get('picking_id'),     # Id del albaran
                        data.get('location_dest_id'),  # Ubicacion destino
                        data.get('invoice_state'),  # esta en none
                        data.get('weight'),         # peso
                        data.get('weight'),         # peso neto
                        data.get('weight_uom_id')   #
                    ]
                )
                if cursor.rowcount:
                    move_id = cursor.fetchone()[0]
                    self._reservated_stock(
                        cursor,
                        product_sku,
                        product_qty,
                        move_id
                    )
                    move_ids.append(move_id)
                else:
                    move_ids = False
        except Exception as e:
            # if except so delete
            if data.get('picking_id'):
                self._delete_table(
                    cursor,
                    table='sale_order',
                    pk=data.get('order_id')
                )
                if len(data.get('order_lines')) > 0:
                    for line in data.get('order_lines'):
                        self._delete_table(
                            cursor,
                            table='sale_order_line',
                            pk=line
                        )
                if data.get('group_id'):
                    self._delete_table(
                        cursor,
                        table='procurement_group',
                        pk=data.get('group_id')
                    )
                self._delete_table(
                    cursor,
                    table='stock_picking',
                    pk=data.get('picking_id')
                )
                if len(data.get('move_ids')) > 0:
                    for move in data.get('move_ids'):
                        self._delete_table(
                            cursor,
                            table='stock_move',
                            pk=move
                        )
            raise Exception(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'Falla al insertar en stock_picking',
                    'message_detail': e.message,
                    'status': 404
                }
            )
        return move_ids

    def _insert_stock_quant_move_rel(self, cursor, sku, qty, move_id):
        quant_ids = self.check_availability(sku, qty, cursor)
        for quant in quant_ids[:qty]:
            query_quant_move = """
                INSERT INTO
                    stock_quant_move_rel(
                        quant_id,
                        move_id
                    )
                VALUES (
                    {}, {}
                );
            """.format(quant, move_id)
            cursor.execute(query_quant_move)
        return True

    def _update_state_order(self, cursor, order_id, group_id):
        query_update_state = """
            UPDATE
                sale_order
            SET
                state = 'progress',
                procurement_group_id = {}
            WHERE
                id = {} ;
        """.format(group_id,order_id)
        cursor.execute(query_update_state)
        if cursor.rowcount:
            return True
        return False

    def _delete_table(self, cursor, table=None, pk=None):
        query_delete = """
            DELETE FROM
                {}
            WHERE
                id = {} ;
        """.format(table,pk)
        cursor.execute(query_delete)
        return True

    def _reservated_stock(self, cursor, sku, qty, move_id):
        quant_ids = self.check_availability(sku, qty, cursor)
        for quant in quant_ids[:qty]:
            query_reservation = """
                UPDATE
                    stock_quant
                SET
                    reservation_id = {}
                WHERE
                    id = {};
            """.format(move_id,quant)
            cursor.execute(query_reservation)
        return True
        
    @http.route(['/pedidos/<sale_order>/detalle'], type='json', auth="public", website=True, methods=['GET'])
    def orderdetail(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        line = []
        # Get Cursor of the DB
        cursor = request.env.cr
        sale_order = kwargs.get('sale_order', False)
        data = {}
        try:
            if sale_order:
                sale_order = "'"+sale_order+"'"
                # We validate sale_order exists in DB
                sale_order_id = tools.validate_identificator(
                    'Pedido',
                    'sale_order',
                    'name',
                    sale_order,
                    cursor
                )
                if not sale_order_id:
                    raise Exception(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'no encontrado',
                            'message_detail': 'No tienes Pedidos',
                            'status': 404
                        }
                    )
                # Get Detail Order
                query_get_sale_order = """
                    SELECT
                        so.id,
                        so.date_order,
                        so.state,
                        rp.name,
                        rp.street calle,
                        rp.street2 calle2,
                        rp.l10n_mx_street3 calle3,
                        rp.l10n_mx_street4 calle4,
                        rcity.name city,
                        rste.name state,
                        rcty.name  country,
                        rp.zip,
                        rp.email,
                        rc.name,
                        so.amount_untaxed,
                        so.amount_tax,
                        so.amount_total,
                        rp.vat,
                        rp.phone,
                        rp.mobile,
                        rp_send.name,
                        rp_send.street calle_send,
                        rp_send.street2 calle2_send,
                        rp_send.l10n_mx_street3 calle3_send,
                        rp_send.l10n_mx_street4 calle4_send,
                        rp_send.city city_send1,
                        rcity_send.name city_send2,
                        rste_send.name state_send,
                        rcty_send.name  country_send,
                        rp_send.zip,
                        rp_send.phone,
                        rp_send.mobile
                    FROM
                        sale_order as so
                    LEFT JOIN
                        res_partner as rp ON rp.id = so.partner_id
                    LEFT JOIN
                        res_partner as rp_send ON rp_send.id = so.partner_shipping_id
                    LEFT JOIN
                        res_currency as rc ON rc.id = so.currency
                    LEFT JOIN
                        res_country_state_city as rcity ON rcity.id = rp.city_id
                    LEFT JOIN
                        res_country_state as rste ON rste.id = rp.state_id
                    LEFT JOIN
                        res_country as rcty ON rcty.id = rp.country_id
                    LEFT JOIN
                        res_country_state_city as rcity_send ON rcity_send.id = rp_send.city_id
                    LEFT JOIN
                        res_country_state as rste_send ON rste_send.id = rp_send.state_id
                    LEFT JOIN
                        res_country as rcty_send ON rcty_send.id = rp_send.country_id
                    WHERE so.name = {}
                """.format(sale_order)
                cursor.execute(query_get_sale_order)
                if cursor.rowcount:
                    (order_id,
                     order_date,
                     order_state,
                     partner_name,
                     partner_street,
                     partner_street2,
                     partner_street3,
                     partner_street4,
                     city_name,
                     state_name,
                     country_name,
                     partner_cp_zip,
                     partner_email,
                     currency_name,
                     amount_untaxed,
                     amount_tax,
                     amount_total,
                     partner_rfc,
                     partner_tel,
                     partner_cel,
                     partner_name_send,
                     partner_street_send,
                     partner_street2_send,
                     partner_street3_send,
                     partner_street4_send,
                     city_send,
                     city_name_send,
                     state_name_send,
                     country_name_send,
                     partner_cp_zip_send,
                     partner_tel_send,
                     partner_cel_send) = cursor.fetchone()
                    # Get Deailt of all product lines
                    query_sale_order_line = """
                        SELECT
                            pp.default_code,
                            pp.name_template,
                            sol.product_uos_qty,
                            sol.price_unit,
                            sol.discount
                        FROM
                            sale_order_line as sol
                        LEFT JOIN
                            product_product as pp ON pp.id = sol.product_id
                        WHERE
                            sol.order_id = {}
                    """.format(order_id)
                    cursor.execute(query_sale_order_line)
                    if cursor.rowcount:
                        sale_lines = cursor.fetchall()
                        for (line_sku, line_description, line_qty, line_price,
                             line_discount) in sale_lines:
                            line.append({
                                'sku': line_sku,
                                'descripcion': line_description,
                                'cantidad': line_qty,
                                'precio_unitario': line_price,
                                'descuentos': line_discount
                            })
    
                        # if success make response in data
                        data = {
                            'to_root': True,
                            'data': {
                                    'pedido': {
                                        'id': order_id,
                                        'fecha_realizada': order_date,
                                        'estatus': order_state,
                                        'moneda': currency_name,
                                        'monto_total_sin_iva': amount_untaxed,
                                        'monto_total_iva': amount_tax,
                                        'monto_total': amount_total,
                                        'cliente': {
                                            'nombre': partner_name,
                                            'email': partner_email,
                                            'rfc': partner_rfc,
                                            'tel': partner_tel,
                                            'cel': partner_cel,
                                            'direccion_facturacion': {
                                                'calle': partner_street,
                                                'no_exterior': partner_street3,
                                                'no_interno': partner_street4,
                                                'colonia': partner_street2,
                                                'municipio': city_name,
                                                'estado': state_name,
                                                'pais': country_name,
                                                'cp': partner_cp_zip
                                            },
                                            'direccion_envio': {
                                                'nombre': partner_name_send,
                                                'calle': partner_street_send,
                                                'no_exterior': partner_street3_send,
                                                'no_interno': partner_street4_send,
                                                'colonia': partner_street2_send,
                                                'municipio': city_send,
                                                'estado': state_name_send,
                                                'pais': country_name_send,
                                                'cp': partner_cp_zip_send,
                                                'tel': partner_tel_send,
                                                'cel': partner_cel_send
                                            }
                                        },
                                        'productos': line
                                    }
                                },
                            'message': 'exitoso',
                            'status': 200
                        }
            else:
                tools.response_error(
                    'No se proporciono la SO',
                    type_error='custom',
                    message='No se proporcionó el dato de la SO'
                )
        except Exception as e:
            # Products not found
            data = list(e)[0]
        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }
        return data
    
    @http.route(['/clientes/<pk>/pedidos/envio/costo'], type='json', auth="public", website=True, methods=['POST'])
    def calculateshipping(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        data_error = {}
        result_carrier = False
        carrier_list = []
        carrier_ids = None
        carrier_found = False
        orderlistObj = ws_todo_orders()
        # Get Cursor of the DB
        cursor = request.env.cr
        # Get data via POST
        order = request.jsonrequest.get('envio', {})
        pk = kwargs.get('pk', False)
        validatepk = tools.validatenumber(pk)
        try:
            if validatepk:
                # We validate client exists in DB
                pk = tools.validate_identificator(
                    'cliente',
                    'res_partner',
                    'id',
                    pk,
                    cursor
                )
                # We validate integrity data receive
                if not order:
                    info = 'Peticion sin Informacion'
                    message = """
                        La peticion no contiene informacion para realizar el pedido
                    """
                    tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                else:
                    products = orderlistObj._validate_order(
                        'productos',
                        order.get('productos', None),
                        cursor
                    )
                # Get list of delivery_carrier ids
                query_delivery_carried = """
                    SELECT
                        id,
                        name
                    FROM
                        delivery_carrier
                    WHERE
                        active = {0}
                        AND
                        delivery_is_web = {0}
                """.format(True)
                cursor.execute(query_delivery_carried)
                if cursor.rowcount:
                    carrier_ids = cursor.fetchall()
                    # Get product data
                    products = orderlistObj.get_data_products(cursor, products, pk)
                    amount_untaxed = products.get('amount_untaxed')
                    amount_tax = products.get('amount_tax')
                    amount_total = amount_untaxed + amount_tax
                    qty_total = products.get('qty_total')
                    weight_total = products.get('weight_total')
                    volume_total = products.get('volume_total')
                    for carrier_id, carrier_name in carrier_ids:
                        result_carrier = tools.get_delivery_price_odoo(
                            grid=carrier_id,
                            total=amount_total,
                            weight=weight_total,
                            volume=volume_total,
                            quantity=qty_total,
                            request=request
                        )
                        if isinstance(result_carrier, dict):
                            carrier_list.append({
                                'id': carrier_id,
                                'nombre': carrier_name,
                                'precio': "{:.2f}".format(result_carrier.get('list_price'))
                            })
                            carrier_found = True
                    if not carrier_found:
                        raise Exception(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'No se puede obtener precio',
                                'message_detail': '{}'.format(result_carrier),
                                'status': 404
                            })
                    if len(carrier_list) > 0:
                        # If success
                        data.update(
                            {
                                'to_root': True,
                                'data': {
                                    'Costo_Envio': carrier_list
                                },
                                'message': 'exitoso',
                                'status': 200
                            }
                        )
                    else:
                        data = data_error
                else:
                    data = {
                        'to_root': True,
                        'data': {},
                        'message': 'Sin Metodos de envio',
                        'message_detail': 'Sin Metodos de envios que mostrar',
                        'status': 400
                    }
            else:
                tools.response_error(
                    expected='cliente',
                    data=pk,
                    type_error='int'    
                )
        except Exception as e:
            # Products not found
            if e.message != '':
                data = list(e)[0]
            elif hasattr(e, 'strerror'):
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'error de conexion con odoo',
                        'message_detail': e.strerror,
                        'status': 400
                    }
                )
            elif hasattr(e, 'faultString'):
                if isinstance(e.faultString, str):
                    result = e.faultString
                else:
                    result = eval(e.faultString)
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message_detail': result,
                        'status': 400
                    }
                )
            else:
                data = e
        except:
            # Error
            data.update(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(sys.exc_info()[1]),
                    'status': 400
                }
            )
        return data
    
    @http.route(['/pedidos/<sale_order>/factura/<invoice>'], type='json', auth="public", website=True, methods=['GET','POST'])
    def orderinvoice(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        queryset = []
        self._tools = WSAPITools()
        # Get Cursor of the DB
        cursor = request.env.cr
        # Get params sku
        sale_order = kwargs.get('sale_order', False)
        invoice = kwargs.get('invoice', False)
        params = request.httprequest.environ['QUERY_STRING'].split('&')
        if len(params) > 1:
            data = {
                'to_root': True,
                'data' : {},
                'message' : 'Se recibieron mas parametros de los esperados',
                'message_detail' : 'Se recibieron mas parametros de los esperados, solo se esperaba un parametro en la URL',
                'status' : 400
            }
            return data
        invoice_type = False
        for param in params:
            if 'tipo' in param:
                types = param.split('=')
                if len(types) > 1:
                    invoice_type = types[1] if types[1] != '' else False
        #invoice_type = kwargs.get('tipo',False)
        sale_order = "'"+sale_order+"'"
        invoice = "'"+invoice+"'"
        try:
            # We validate order and invoice exists in DB
            sale_order = self._tools.validate_identificator(
                'Pedido',
                'sale_order',
                'name',
                sale_order,
                cursor
            )
            invoiceid = self._tools.validate_identificator(
                'Factura',
                'account_invoice',
                'number',
                invoice,
                cursor
            )
            # Get id of invoice
            query_account_invoice = """
                SELECT
                    id
                FROM
                    account_invoice
                WHERE
                    number = {}
            """.format(invoice)
            cursor.execute(query_account_invoice)
            invoice_ids = cursor.fetchall()
            for invoice_id in invoice_ids:
                if invoice_type:
                    if invoice_type == 'xml':
                        invoice_data = self._get_data_invoice_type(
                            'xml_signed',
                            invoice_id[0],
                            cursor,
                            expected=invoice_type
                        )
                        queryset.append(invoice_data)
                    elif invoice_type == 'pdf':
                        invoice_data = self._get_data_invoice_type(
                            'pdf_signed',
                            invoice_id[0],
                            cursor,
                            expected=invoice_type
                        )
                        queryset.append(invoice_data)
                    else:
                        info = 'Tipo de Documento no valido'
                        message = """
                            El tipo de documento {} que envias no es valido
                        """.format(invoice_type)
                        self._tools.response_error(
                            info,
                            type_error='custom',
                            message=message
                        )
                else:
                    invoice_data = self._get_data_invoice_type(
                        'xml_signed, pdf_signed',
                        invoice_id[0],
                        cursor
                    )
                    queryset.append(invoice_data)

            # if success make response in data
            data = {
                'to_root': True,
                'data': {
                            'pedidos': queryset
                        },
                'message': 'exitoso',
                'status': 200
            }
        except Exception as e:
            # Products not found
            data = list(e)[0]
        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }
        return data

    def _get_data_invoice_type(self, fields, _id, cursor, expected=None):
        query_account_invoice_type = """
            SELECT
                {}
            FROM
                cfdi_sign
            WHERE
                account_invoice_id = {}
        """.format(fields,_id)
        cursor.execute(query_account_invoice_type)
        fetch_data = cursor.fetchone()
        if fetch_data!=None:
            if len(fetch_data) == 1:
                response = {
                    'id': _id,
                    expected: str(fetch_data[0])
                }
            elif len(fetch_data) > 1:
                invoice_xml, invoice_pdf = fetch_data
                response = {
                    'id': _id,
                    'xml': str(invoice_xml),
                    'pdf': str(invoice_pdf)
                }
            else:
                response = None
        else:
            response = None
        return response
    
    @http.route(['/pedidos/envio'], type='json', auth="public", website=True, methods=['POST'])
    def boxesshippingorder(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        order_list = ws_todo_orders()
        # get array products
        productos = request.jsonrequest.get('productos', {})
        cursor = request.env.cr
        try:
            if len(productos) > 0:
                #### OJOOOOOOOOOOOOOOOO
                factor = 15
                volumen_total = 0
                peso_total = 0
                total_caja = 0
                products = order_list._validate_order(
                    'productos',
                    productos,
                    cursor,
                    not_price=True
                )
                for product in products:
                    if product.get('sku') and product.get('cantidad'):
                        query_product = """
                            SELECT
                                p.volume,
                                p.weight
                            FROM
                                restful_get_products as p
                            WHERE
                                p.sku = '{}' ;
                        """.format(str(product.get('sku')))
                        cursor.execute(query_product)
                        ####OJOOOOOOO /5000
                        if cursor.rowcount:
                            (volumen, peso) = cursor.fetchone()
                            if volumen <= 0:
                                volumen = 0
                            if peso <= 0:
                                peso = 0
                            volumen_total += (volumen * product.get('cantidad')) / 5000
                            peso_total += peso * product.get('cantidad')
                        else:
                            raise Exception({
                                'to_root': True,
                                'data': {},
                                'message': 'error',
                                'message_detail': 'Productos {} campo volumen vacio o no esta activo para web'.format(products),
                                'status': 404
                            })
                    else:
                        raise Exception({
                            'to_root': True,
                            'data': {},
                            'message': 'error',
                            'message_detail': 'productos no encontrados',
                            'status': 404
                        })
                cajas_por_volumen = volumen_total / factor
                cajas_por_peso = peso_total / factor
                total_cajas_volumen = math.ceil(cajas_por_volumen)
                total_cajas_peso = math.ceil(cajas_por_peso)
                if total_cajas_volumen > total_cajas_peso:
                    total_caja = total_cajas_volumen
                elif total_cajas_volumen < total_cajas_peso:
                    total_caja = total_cajas_peso
                elif total_cajas_volumen == total_cajas_peso:
                    total_caja = total_cajas_volumen
                if total_caja <= 0:
                    total_caja = 1
                data = {
                    'to_root': True,
                    "status": 200,
                    "message": "exitoso",
                    "data": {
                        "registro": {
                            "total_cajas": total_caja
                        }
                    }
                }
            else:
                raise Exception(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'No encontrado',
                        'message_detail': 'No se recibieron productos',
                        'status': 404
                    })
        except Exception as e:
            # Products not found
            data = list(e)[0]
        except:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'No se recibieron productos',
                'status': 400
            }
        return data