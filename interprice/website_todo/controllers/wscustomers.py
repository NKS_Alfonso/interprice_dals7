# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
from wsorders import ws_todo_orders

_logger = logging.getLogger(__name__)

class ws_todo_customers(http.Controller):
    
    _client = {}
    _request = None
    
    @http.route(['/clientes'], type='json', auth="public", website=True, methods=['GET'])
    def customers(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        client_info = {}
        data = {}
        try:
            request.env.cr.execute(
                """
                    SELECT
                        id,
                        name,
                        email,
                        vat
                    FROM
                        res_partner
                    WHERE
                        parent_id IS NULL
                        AND
                        customer = True
                        AND is_company = True
                """
            )
            result = request.env.cr.fetchall()
            if len(result) > 0:
                client_info.update({
                    "clientes": {}
                })
                for (id, name, email, vat) in result:
                    client_info["clientes"].update(
                        {
                            id:
                                {
                                    "id": id,
                                    "nombre": name,
                                    "correo": email,
                                    "rfc": vat,
                                }
                        }
                    )
            else:
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'Sin datos',
                        'message_detail': 'Clientes no encontrados',
                        'status': 404
                    }
                )
        except:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()),
                'status': 400
            }
        if not data:
            data = {
                'to_root': True,
                'data': client_info,
                'message': 'exitoso',
                'status': 200
            }
        return data
    
    def new_number_client(self):
        tools = WSAPITools()
        cursor = self._request.env.cr
        query = """
            SELECT
                client_number
            FROM
                (
                    SELECT
                        to_number(client_number,'999999999999') AS client_number
                    FROM
                        res_partner
                    WHERE
                        client_number IS NOT NULL
                ) AS res_partner
            ORDER BY
                client_number DESC
            LIMIT 1;
        """
        cursor.execute(query)
        if cursor.rowcount:
            new_value = str(cursor.fetchone()[0] + 1)
        else:
            new_value = '1'
        return new_value
    
    def validate_email(self, email):
        registry = {}
        tools = WSAPITools()
        cursor = self._request.env.cr
        cursor.execute(
            """
                SELECT
                    id,
                    name,
                    email
                FROM
                    res_partner
                WHERE
                    email = '{}'
                LIMIT 1;
            """.format(email)
        )
        if cursor.rowcount:
            registry.update({'registro': {'id': cursor.fetchone()[0]}})
            data = {
                'to_root': True,
                'data': registry,
                'message': 'error',
                'message_detail': 'El correo ya existe y debe ser único'.decode('utf8'),
                'status': 400
            }
            raise Exception(data)
        else:
            return True

    def validate(self, client):
        validated = {}
        if not client:
            validated.update({'validated': False,
                              'required': False})
        if client:
            validated.update({'validated': True})
            fields = [
                self.validate_field('nombre', client.get('nombre', None), True),    # Nombre ==> Requerido
                self.validate_field('correo', client.get('correo', None), True),    # Correo ==> Requerido
                self.validate_field('rfc', client.get('rfc', None), True),          # Rfc ==> Requerido
            ]
            for field in fields:
                if isinstance(field, dict):
                    validated.update({'validated': False,
                                      'required': True})
                    validated.update({'fields': field})
        return validated

    def validate_field(self, field, value, required):
        new_value = None
        if value:
            new_value = unicode(value).encode('utf8')
            self._client[field] = new_value
        else:
            if required:
                new_value = {}
                new_value.update({field: 'requerido'})
        return new_value

    def add_account_payable(self, cursor, partner_id):
        response = False
        try:
            # Search property default property_account_payable
            query_account = """
                SELECT
                    fields_id,
                    value_reference
                FROM
                    ir_property
                WHERE
                    name = 'property_account_payable'
                    AND
                    res_id IS NULL
            """
            cursor.execute(query_account)
            if cursor.rowcount:
                field_id, value_ref = cursor.fetchone()
                query_ir_property = """
                    INSERT INTO
                        ir_property(
                            create_date,
                            name,
                            type,
                            company_id,
                            fields_id,
                            write_date,
                            value_reference,
                            res_id
                        )
                    VALUES (
                            now() at time zone 'UTC',
                            'property_account_payable',
                            'many2one',
                            1,
                            %s,
                            now() at time zone 'UTC',
                            %s,
                            %s
                    )
                    RETURNING
                        id;
                """
                cursor.execute(query_ir_property, [
                                        field_id,
                                        value_ref,
                                        'res.partner,{}'.format(partner_id)
                                    ])
                if cursor.rowcount:
                    response = cursor.fetchone()[0]
        except Exception as e:
            query_deleted_partner = """
                DELETE FROM
                    res_partner
                WHERE
                    id = %s
            """
            cursor.execute(query_deleted_partner, [partner_id])
        return response
    
    @http.route(['/clientes'], type='json', auth="public", website=True, methods=['POST'])
    def addcustomers(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        registry = {}
        self._request = request
        client = request.jsonrequest.get('cliente', {})
        self._client = client
        validated = self.validate(client)
        if validated.get('validated', False):
            is_company = True
            name = self._client.get('nombre', None)             # Nombre
            display_name = self._client.get('nombre', None)     # Nombre
            create_date = 'current_timestamp'                   # Fecha_creacion
            email = self._client.get('correo', None)            # Correo
            address_type = 'invoice'                            # Tipo_direccion
            client_segment = 1                                  # Segmento
            lang = 'es_MX'                                      # Idioma
            branch_office = 1                                   # Sucursal
            customer = True                                     # Cliente
            supplier = False                                    # Proveedor
            client_number = self.new_number_client()            # Numero_cliente
            active = True                                       # Activo
            notify_email = 'always'                             # Notificaciones
            vat = self._client.get('rfc', None)                 # Rfc
            pay_method_id = 1                                   # Forma_pago
            forma_pago = 5                                      # Tipo_pago
            use_cfdi = 1                                        # Uso_cfdi
            base = 1                                            # Nivel_base
            write_date = 'current_timestamp'                    # Fecha_modificacion
            tz = 'America/Mexico_City'                          # Tz
            status_client = False                               # Bloqueado de Clientes
            cursor = request.env.cr
            try:
                self.validate_email(email)
                query = """
                    INSERT INTO
                        res_partner (
                            is_company,         -- Es_compañia
                            name,               -- Nombre
                            display_name,       -- Nombre
                            type,               -- Tipo_direccion
                            create_date,        -- Fecha_creacion
                            email,              -- Correo
                            client_segment,     -- Segmento
                            lang,               -- Idioma
                            branch_office,      -- Sucursal
                            customer,           -- Cliente
                            supplier,           -- Proveedor
                            client_number,      -- Numero_cliente
                            active,             -- Activo
                            notify_email,       -- Notificaciones
                            vat,                -- Rfc
                            pay_method_id,      -- Forma_pago
                            forma_pago,         -- Tipo_pago
                            use_cfdi,           -- Uso_cfdi
                            base,               -- Nivel_base
                            write_date,         -- Fecha_modificacion
                            tz,                 -- Tz
                            status_client       -- Bloqueado de Clientes
                        )
                    VALUES (
                        %s,%s,%s,%s,{},%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                        %s,%s,%s,{},%s,%s
                    )
                    RETURNING
                        id;
                """.format(create_date, write_date)
                cursor.execute(query, (
                    is_company,        # Es_compañia
                    name,              # Nombre
                    display_name,      # Nombre
                    address_type,      # Tipo_direccion
                    email,             # Correo
                    client_segment,    # Segmento
                    lang,              # Idioma
                    branch_office,     # Sucursal
                    customer,          # Cliente
                    supplier,          # Proveedor
                    client_number,     # Numero_cliente
                    active,            # Activo
                    notify_email,      # Notificaciones
                    vat,               # Rfc
                    pay_method_id,     # Forma_pago
                    forma_pago,        # Tipo_pago
                    use_cfdi,          # Uso_cfdi
                    base,              # Nivel_base
                    tz,                # Tz
                    status_client     # Bloqueado de Clientes
                    )
                )
                if cursor.rowcount:
                    partner_id = cursor.fetchone()[0]
                    account = self.add_account_payable(cursor, partner_id)
                    if account:
                        registry.update({'registro': {'id': partner_id}})
                    else:
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'No creado',
                                'message_detail': 'No se agrego la cuenta',
                                'status': 400
                            }
                        )
                else:
                    data.update(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'No creado',
                            'message_detail': 'Registro no creado',
                            'status': 400
                        }
                    )
            except Exception as e:
                data = list(e)[0]
            except:
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(sys.exc_info()),
                    'status': 400
                }
        else:
            if validated.get('required', False):
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'Campos obligatorios',
                        'message_detail': validated.get('fields', 'Uno o mas campos son obligatorios'),
                        'status': 406
                    }
                )
            else:
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'No se recibieron datos para el registro',
                        'message_detail': {
                            'necessary_structure': {
                               "cliente": {
                                    "nombre": "Requerido",
                                    "correo": "Requerido",
                                    "rfc": "Requerido",
                               }
                            }
                        },
                        'status': 204
                    }
                )
        if not data:
            data = {
                'to_root': True,
                'data': registry,
                'message': 'exitoso',
                'status': 200
            }
        return data
        
    @http.route(['/clientes/<pk>'], type='json', auth="public", website=True, methods=['GET','POST'])
    def customerbyid(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        client_info = {}
        data = {}
        # Get params client_id
        client_id = kwargs.get('pk', False)
        if client_id.isdigit():            
            try:
                if client_id:
                    request.env.cr.execute(
                        """
                            SELECT
                                id,
                                name,
                                email,
                                vat
                            FROM
                                res_partner
                            WHERE
                                parent_id IS NULL
                                AND
                                customer = True
                                AND
                                is_company = True
                                AND
                                id = {}
                        """.format(client_id)
                    )
                    result = request.env.cr.fetchall()
                    if len(result)>0:
                        for (id, name, email, vat) in result:
                            client_info.update({
                                "cliente":
                                        {
                                            "id": id,
                                            "nombre": name,
                                            "correo": email,
                                            "rfc": vat,
                                        }
                            })
                    else:
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Sin datos',
                                'message_detail': 'Cliente no encontrado',
                                'status': 404
                            }
                        )
                else:
                    request.env.cr.execute(
                        """
                            SELECT
                                id,
                                name,
                                email,
                                vat
                            FROM
                                res_partner
                            WHERE
                                parent_id IS NULL
                                AND
                                customer = True
                                AND
                                is_company = True
                        """
                    )
                    result = request.env.cr.fetchall()
                    if len(result) > 0:
                        client_info.update({
                            "clientes": {}
                        })
                        for (id, name, email, vat) in result:
                            client_info["clientes"].update({
                                    id:
                                        {
                                            "id": id,
                                            "nombre": name,
                                            "correo": email,
                                            "rfc": vat,
                                        }
                                }
                            )
                    else:
                        data.update(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Sin datos',
                                'message_detail': 'Clientes no encontrados',
                                'status': 404
                            }
                        )
            except:
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(sys.exc_info()),
                    'status': 400
                }
            if not data:
                data = {
                    'to_root': True,
                    'data': client_info,
                    'message': 'exitoso',
                    'status': 200
                }
        else:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'The Id is not an integer',
                'status': 400
            }
        return data

    @http.route(['/clientes/<pk>/pedidos/<sale_order>/pago'], type='json', auth="public", website=True, methods=['POST'])
    def newtask(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        type_pay = None
        missin_data = False
        order_list_obj = ws_todo_orders()
        # Get Cursor of the DB
        cursor = request.env.cr
        # Get data via POST
        payment = request.jsonrequest.get('revision_pago', {})
        pk = kwargs.get('pk', False)
        sale_order = kwargs.get('sale_order', False)
        sale_order = "'"+sale_order+"'"
        try:
            if pk.isdigit():
                # We validate client exists in DB
                pk = tools.validate_identificator(
                    'cliente',
                    'res_partner',
                    'id',
                    pk,
                    cursor
                )
                # We validate Sale_Order exists in DB
                sale_order_id = tools.validate_identificator(
                    'Pedido de Venta',
                    'sale_order',
                    'name',
                    sale_order,
                    cursor
                )
                # We validate integrity data receive
                if not payment:
                    info = 'Peticion sin Informacion'
                    message = """La Peticion no recibe ningua informacion"""
                    tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                else:
                    type_pay = order_list_obj._validate_order(
                        'forma de pago',
                        payment.get('tipo_pago', None),
                        cursor
                    )
                expected = ''
                if not payment.get('fecha_pedido'):
                    missin_data = True
                    expected = 'fecha_pedido'
                if not payment.get('monto'):
                    missin_data = True
                    expected = 'monto'
                if not payment.get('id_magento'):
                    missin_data = True
                    expected = 'id_magento'
                if not payment.get('descripcion_pago'):
                    missin_data = True
                    expected = 'Descripcion del Pago'
                if missin_data:
                    info = 'Datos incompletos en peticion Post'
                    message = """
                        La peticion no contiene {}, es necesario
                    """.format(expected)
                    tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                # Looking company to get data and Create Task Payment
                cursor.execute("""
                    SELECT
                        project_id,
                        project_user_id,
                        currency_id
                    FROM
                        res_company
                """)
                if cursor.rowcount:
                    for (project_id, project_user_id,
                         currency_id) in cursor.fetchall():
                        if not project_id or not project_user_id:
                            info = 'Faltan algunas configuraciones'
                            message = """Falta configurar el proyecto o el usuario
                                          del modulo project"""
                            tools.response_error(
                                info,
                                type_error='custom',
                                message=message
                            )
                        # Looking Task Types
                        stage = 10000
                        stage_id = 0
                        query_types_rel = """
                            SELECT
                                type_id
                            FROM
                                project_task_type_rel
                            WHERE
                                project_id = %s;
                        """
                        cursor.execute(query_types_rel, [project_id])
                        if cursor.rowcount:
                            type_ids = cursor.fetchall()                        
                            query_task_types = """
                                SELECT
                                    id,
                                    sequence
                                FROM
                                    project_task_type
                                WHERE
                                    id IN %s;
                            """
                            cursor.execute(query_task_types, [tuple(type_ids)])
                            if cursor.rowcount:                          
                                for _type_id, secuence in cursor.fetchall():
                                    if secuence < stage:
                                        stage = secuence
                                        stage_id = _type_id
                        # Create Task Project Payment
                        query_payment = """
                            INSERT INTO
                                project_task(
                                    name,
                                    web_task,
                                    project_id,
                                    user_id,
                                    company_currency_id,
                                    sale_order_id,
                                    date_order,
                                    amount_order,
                                    int_number_mag,
                                    paymethod_id,
                                    description,
                                    stage_id,
                                    sequence
                                )
                            VALUES(
                                {}, {}, %s, %s, %s, %s, %s,
                                %s, %s, %s, %s, %s, {}
                            )
                            RETURNING
                                id;
                        """.format(sale_order, True, 10)
                        cursor.execute(query_payment,
                            [
                                project_id,
                                project_user_id,
                                currency_id,
                                sale_order_id,
                                payment.get('fecha_pedido', None),
                                payment.get('monto', None),
                                payment.get('id_magento', None),
                                type_pay,
                                payment.get('descripcion_pago', None),
                                stage_id
                            ]
                        )
                        if cursor.rowcount:
                            task_created = cursor.fetchone()[0]
                            data = {
                                'to_root': True,
                                'data': {'tarea_id': task_created},
                                'message': 'exitoso',
                                'status': 200
                            }
            else:
                tools.response_error(
                    expected='cliente',
                    data=pk,
                    type_error='int'
                )
        except:
            # Error
            data.update(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(sys.exc_info()[1]),
                    'status': 400
                }
            )
        return data