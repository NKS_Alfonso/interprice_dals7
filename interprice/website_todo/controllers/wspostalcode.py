# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_postalcode(http.Controller):
    
    @http.route(['/clientes/direcciones/cp/<pk>'], type='json', auth="public", website=True, methods=['GET','POST'])
    def getbypostalcode(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        queryset = []
        colony_list = []
        pk = kwargs.get('pk', False)
        if pk:
            pk = "'"+pk+"'"
        try:
                # We validate client exists in DB
                postal = tools.validate_identificator(
                    'Codigo Postal',
                    'res_postal_code',
                    'name',
                    str(pk),
                    request.env.cr
                )
                query_postal = """
                    SELECT
                        pc.id,
                        pc.name
                    FROM
                        res_postal_code as pc
                    WHERE
                        pc.id = {}
                """.format(postal)
                request.env.cr.execute(query_postal)
                results = request.env.cr.fetchall()
                if len(results) <= 0:
                    info = 'Código Postal no encontrado'
                    message = 'No podemos encontrar el código postal'
                    tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                for (code_id, code_name) in results:
                    query_get_colonys = """
                        SELECT
                            rstcc.id,
                            rstcc.name,
                            ct.id,
                            ct.name,
                            st.id,
                            st.name,
                            ctry.id,
                            ctry.name,
                            stment.id
                        FROM
                            res_state_city_colony as rstcc
                        LEFT JOIN
                            res_country_state_city as ct ON ct.id = rstcc.city_id
                        LEFT JOIN
                            res_country_state as st ON st.id = rstcc.state_id
                        LEFT JOIN
                            res_country as ctry ON ctry.id = rstcc.country_id
                        LEFT JOIN
                            res_settlement_type as stment ON stment.id = rstcc.settlement_id
                        WHERE
                            rstcc.postal_id = {}
                    """.format(code_id)
                    request.env.cr.execute(query_get_colonys)
                    result = request.env.cr.fetchall()
                    if len(result)>0:
                        for (colony_id, colony_name, city_id, city_name,
                             state_id, state_name, country_id, country_name,
                             settlement_id) in result:
                            colony_list.append({
                                colony_id: colony_name,
                                'asentamiento_id': settlement_id
                            })
                            city_id = city_id
                            city_name = city_name
                            state_id = state_id
                            state_name = state_name
                            country_id = country_id
                            country_name = country_name
                        queryset.append({
                            'codigo_postal': {
                                code_id: code_name
                            },
                            'colonias': colony_list,
                            'municipio': {
                                city_id: city_name
                            },
                            'estado': {
                                state_id: state_name
                            },
                            'pais': {
                                country_id: country_name
                            }
                        })
                data = {
                    'to_root': True,
                    'data': queryset[0],
                    'message': 'exitoso',
                    'status': 200
                }
        except Exception as e:
            # Products not found
            data = list(e)[0]
        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }
        return data