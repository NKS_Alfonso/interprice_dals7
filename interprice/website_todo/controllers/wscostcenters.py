# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_costCenters(http.Controller):
    
    @http.route(['/centro_costos'], type='json', auth="public", website=True, methods=['GET','POST'])
    def costcenters(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        try:
            request.env.cr.execute("SELECT id, name FROM account_cost_center")
            list_cost_center = request.env.cr.fetchall()
            if len(list_cost_center) <= 0:
                raise Exception(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'No encuentra Centros de Costo disponibles',
                        'message_detail': 'No hay datos de Centros de Costo disponibles',
                        'status': 404
                    }
                )

            # if success make response in data
            data = {
                'to_root': True,
                'data': {
                            'centro_costos': dict(list_cost_center)
                        },
                'message': 'exitoso',
                'status': 200
            }

        except Exception as e:
            # Products not found
            data = list(e)[0]

        except:
            # Error
            data = {
                'to_root' : True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }

        return data