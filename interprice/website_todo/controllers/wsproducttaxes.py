# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
import math
_logger = logging.getLogger(__name__)

class ws_todo_orders(http.Controller):
    
    _tools = None
    
    def _get_taxes_by_zip(self, postalcode):
        res = []
        postal_code_obj = request.env['res.postal.code'].browse(postalcode)
        tax_array = []
        zip_taxes_ids = postal_code_obj.tax_prod_ids
        for tax in postal_code_obj.tax_prod_ids:
            tax_array.append(tax.id)
        if tax_array: 
            res = tax_array
        return res
    
    def _amount_line_tax(self, lines, postalcode, cursor):
        val = 0.0
        for line in lines:
            price = line.get('precio')
            qty = line.get('cantidad')
            tax_id = self._get_taxes_by_zip(postalcode)
            # for c in request.env['account.tax'].compute_all(
            #         cr, uid, tax_id, price, qty)['taxes']:
            #def compute_all(self, cr, uid, taxes, price_unit, quantity, product=None, partner=None, force_excluded=False)
            #compute_all(cr, uid, [tax], amount, 1)
            if len(tax_id)>0:
                for c in request.env['account.tax'].browse(tax_id).sudo().compute_all(price, qty)['taxes']:
                    val += c.get('amount', 0.0)
            else:
                self._tools.response_error(
                    'No hay configurados impuestos',
                    type_error='custom',
                    message='No existen configurados impuestos para el código postal {}'.format(postalcode)
                )
        return val
    
    def _validate_products(self, expected, productos, cursor):
        respuesta = False
        for product in productos:
            pid = product.get('sku',False)
            pqty = product.get('cantidad',False)
            pprice = product.get('precio',False)
            if pid:
                cursor.execute("""
                    SELECT
                        p.*
                    FROM
                        restful_get_products as p
                    WHERE
                        p.sku = '{}';
                """.format(pid))
                result = cursor.fetchall()
                if len(result)==0:
                    self._tools.response_error(
                        'Producto no existe',
                        type_error='custom',
                        message='El producto {} no existe'.format(pid)
                    )
                if pqty:
                    if not isinstance(pqty, (int,float)):
                        self._tools.response_error(
                            'Cantidad no válida',
                            type_error='custom',
                            message='La cantidad de producto {} para el producto {} no es válida'.format(pqty,pid)
                        )
                    else:
                        if pqty<=0:
                            self._tools.response_error(
                                'Cantidad no válida',
                                type_error='custom',
                                message='La cantidad de producto {} para el producto {} no es válida'.format(pqty,pid)
                            )                            
                    if pprice:
                        if not isinstance(pprice, (int,float)):
                            self._tools.response_error(
                                'Precio no válido',
                                type_error='custom',
                                message='El precio {} para el producto {} no es válido'.format(pprice,pid)
                            )
                        else:
                            if pprice <= 0.00000:
                                self._tools.response_error(
                                    'Precio no válido',
                                    type_error='custom',
                                    message='El precio {} para el producto {} no es válido'.format(pprice,pid)
                                )    
                    else:
                        self._tools.response_error(
                            'Precio no válido',
                            type_error='custom',
                            message='No se proporcionó precio para el producto {}'.format(pid)
                        )
                else:
                    self._tools.response_error(
                        'Cantidad no válida',
                        type_error='custom',
                        message='No se proporcionó cantidad para el producto {}'.format(pid)
                    )
            else:
                self._tools.response_error(
                    'No se proporcionó el sku',
                    type_error='custom',
                    message='No se proporcionó el sku del producto'
                )
            respuesta = True
        return respuesta
    
    @http.route(['/impuestos/<pk>/productos/'], type='json', auth="public", website=True, methods=['GET','POST'])
    def specialtaxes(self, **kwargs):
        self._tools = WSAPITools()
        ipvalid = self._tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        # Get Cursor of the DB
        cursor = request.env.cr
        productos = request.jsonrequest.get('productos', {})
        pk = kwargs.get('pk', False)
        tax_amount = 0.00
        data = {}
        try:
            if pk.isdigit():
                # We validate client exists in DB
                pk = self._tools.validate_identificator(
                    'codigo postal',
                    'res_postal_code',
                    'id',
                    pk,
                    cursor
                )
                # We validate integrity data receive
                if not productos:
                    info = 'Peticion sin Informacion'
                    message = """
                        La peticion no contiene informacion para procesar la petición
                    """
                    self._tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                else:
                    validated = self._validate_products(
                        'productos',
                        productos,
                        cursor
                    )
                if validated:
                    tax_amount = self._amount_line_tax(productos,pk,cursor)
                    data = {
                        'to_root': True,
                        'data' : {
                            'tax_amount' : tax_amount
                        },
                        'message': 'exitoso',
                        'status': 200
                    }
            else:
                data.update(
                    {
                        'to_root': True,
                        'data' : {},
                        'message' : 'error de código postal',
                        'message_detail' : 'El id del código postal proporcionado {} no es un entero'.format(pk),
                        'status' : 400
                    }
                )
        except Exception as e:
            # Products not found
            if e.message != '':
                data = list(e)[0]
            elif hasattr(e, 'strerror'):
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'error de conexion con odoo',
                        'message_detail': e.strerror,
                        'status': 400
                    }
                )
            elif hasattr(e, 'faultString'):
                if isinstance(e.faultString, str):
                    result = e.faultString
                else:
                    result = eval(e.faultString)
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message_detail': result,
                        'status': 400
                    }
                )
            else:
                data = e
        except:
            # Error
            data.update(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(sys.exc_info()[1]),
                    'status': 400
                }
            )
        return data