# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_states(http.Controller):
    
    @http.route(['/estados'], type='json', auth="public", website=True, methods=['GET','POST'])
    def states(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        list_result = None
        try:
            query_country_company = """
                SELECT
                    rp.country_id
                FROM
                    res_company rc
                INNER JOIN
                    res_partner rp ON rp.id=rc.partner_id
                LIMIT 1
            """
            request.env.cr.execute(query_country_company)
            result = request.env.cr.fetchall()
            query_state_city = """
                SELECT
                    id,
                    name
                FROM
                    res_country_state
                WHERE
                    country_id = {}
            """.format(result[0][0])
            request.env.cr.execute(query_state_city)
            list_result = request.env.cr.fetchall()
            if len(list_result)==0:
                info = 'Estados no encontrados'
                message = """
                    Hubo un problema y no hemos podido, encontrar los estados
                """
                tools.response_error(
                    info,
                    type_error='custom',
                    message=message
                )
            # if success make response in data
            data = {
                'to_root': True,
                'data': {
                            'estados': dict(list_result)
                        },
                'message': 'exitoso',
                'status': 200
            }

        except Exception as e:
            data = list(e)[0]

        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }
        return data
    
    @http.route(['/estados/<pk>'], type='json', auth="public", website=True, methods=['GET','POST'])
    def countybystate(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        list_result = None
        pk = kwargs.get('pk', False)
        try:
            if pk.isdigit():
                # We validate client exists in DB
                state = tools.validate_identificator(
                    'estado',
                    'res_country_state',
                    'id',
                    pk,
                    request.env.cr
                )
                query_state_city = """
                    SELECT
                        id,
                        name
                    FROM
                        res_country_state_city
                    WHERE
                        state_id = {}
                """.format(state)
                request.env.cr.execute(query_state_city)
                list_result = request.env.cr.fetchall()
                if len(list_result) == 0:
                    info = 'Estados no encontrados'
                    message = """
                        Hubo un problema y no hemos podido, encontrar los estados
                    """
                    tools.response_error(
                        info,
                        type_error='custom',
                        message=message
                    )
                # if success make response in data
                data = {
                    'to_root': True,
                    'data': {
                                'municipios': dict(list_result)
                            },
                    'message': 'exitoso',
                    'status': 200
                }
            else:
            
                tools.response_error(
                    expected='estado',
                    data=pk,
                    type_error='int'
                )

        except Exception as e:
            # Products not found
            data = list(e)[0]

        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }

        return data