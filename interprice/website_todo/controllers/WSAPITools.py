# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
_logger = logging.getLogger(__name__)

class WSAPITools(object):
    """
       This class is a support to the Class Views to get cursors,
       verify users, verify tokens and some statement more
    """
    #127.0.0.1  LOCAL
    #172.30.200.101 JAZEL
    #192.168.20.230 BETA
    
    #IP_ALLOWED = ['127.0.0.1', '172.30.200.101', '192.168.20.230']
    IP_ALLOWED = ['10.0.1.86']
    def validate_ip(self, request):
        if '*' in self.IP_ALLOWED:
            return True
        else:
            if "HTTP_X_FORWARDED_FOR" in request.httprequest.environ:
                # Virtual host        
                ipreq = request.httprequest.environ["HTTP_X_FORWARDED_FOR"]
            elif "HTTP_HOST" in request.httprequest.environ:
                # Non-virtualhost
                ipreq = request.httprequest.environ["REMOTE_ADDR"]
            else:
                return False
            if ipreq in self.IP_ALLOWED:
                return True
            else:
                return False
            
    def validatenumber(self, pk):
        respuesta = False
        if len(pk) >= 1:
            if pk.isdigit():
                respuesta = True
            else:
                pk1 = pk[0]
                pk2 = pk[1:]
                if pk1 == '-' and  pk2.isdigit():
                    respuesta = True
        return respuesta    
            
    def get_company_currency(self, cursor):
        query = """
            SELECT
                rcurr.name,
                rcurr.id
            FROM
                res_company rc
            INNER JOIN
                res_currency rcurr ON rcurr.id=rc.currency_id
            LIMIT 1    
        """
        cursor.execute(query)
        if cursor.rowcount:
            response = cursor.fetchall()[0]
        else:
            info = 'Empresa no encontrada'
            message = 'Al parecer no se encuentra una empresa de donde obtener la moneda base'
            self.response_error(info=info, type_error='custom', message=message)
        return response
    
    def price_convert(self, cfrom, cto, price, cursor):
        query_currency_convert = """
            SELECT
                currency_convert('{}','{}',{},now() at time zone 'UTC')
        """.format(cfrom, cto,price)
        cursor.execute(query_currency_convert)
        result = cursor.fetchall()
        return result[0][0]
        
    def validate_identificator(self, expected, model, field, data, cursor):
        query = ''
        if data == '-1':
            query = """
                        SELECT
                            id
                        FROM
                            res_partner
                        WHERE
                            webservice_client_default = True
                    """
        else:
            query = """
                        SELECT
                            id
                        FROM
                            {}
                        WHERE
                            {} = {}
                    """.format(model, field, data)
        cursor.execute(query)
        result = cursor.fetchall()
        if len(result) > 0:
            response = result[0][0]
        else:
            info = '{} no encontrado'.format(expected)
            message = """Al parecer el {} {} no se
                         encuentra""".format(expected, data)
            message = self.clean_message(message)
            self.response_error(info, type_error='custom', message=message)
        return response

    def validate_unlock_client(self, data, cursor):
        query = """
                    SELECT
                        status_client
                    FROM
                        res_partner
                    WHERE
                        id = {}
                """.format(data)
        cursor.execute(query)
        result = cursor.fetchall()
        if len(result) > 0:
            status = result[0][0]
            if not status:
                response = data
            else:
                info = 'Cliente Bloqueado '
                message = """Al parecer se encuentra bloqueado, por favor
                            contacte a algun operador"""
                message = self.clean_message(message)
                self.response_error(info, type_error='custom', message=message)
        else:
            info = 'Cliente no encontrado'
            message = """Al parecer el cliente no se encuentra"""
            message = self.clean_message(message)
            self.response_error(info, type_error='custom', message=message)
        return response

    def clean_message(self, message):
        message = message.replace('\n', '')
        message = message.replace('   ', '')
        message = message.replace('  ', ' ')
        return message

    def response_error(self, expected, data=None, type_error=None, message=None):
        me = ''
        me_detail = ''
        if type_error:
            if 'custom' in type_error:
                me = expected
                me_detail = message
            elif 'int' in type_error:
                me = "{} esperaba integer".format(expected)
                me_detail = """Al parecer {} {} no es
                                            integer""".format(expected, data)
            elif 'str' in type_error:
                me = "{} esperaba float".format(expected)
                me_detail = """Al parecer {} {} no es
                                            string""".format(expected, data)
            elif 'float' in type_error:
                me = "{} esperaba float".format(expected)
                me_detail = """Al parecer {} {} no es
                                            float""".format(expected, data)
            elif 'post_fail' in type_error:
                me = "Dato enviado por post no valido"
                me_detail = """Al parecer el dato {} no es valido
                                        ni necesario""".format(data)
            else:
                me = '{} no encontrado'.format(expected)
                me_detail = """Al parecer {} {} no se
                                        encuentra""".format(expected, data)
        else:
            me = '{} no encontrado'.format(expected)
            me_detail = """Al parecer {} {} no se 
                                        encuentra""".format(expected, data)
        me_detail = self.clean_message(me_detail)
        raise Exception(
                {
                    'to_root': True,
                    'data': {},
                    'message': me,
                    'message_detail': me_detail,
                    'status': '400'
                })

    def get_delivery_price_odoo(self, grid=None, total=None, weight=None, volume=None, quantity=None, request=None):
        try:
            price = request.env['sale.order'].sudo().api_get_price_from_picking(grid,total,weight,volume,quantity)
        except Exception as e:
            price = e
        return price

    def get_sequence_odoo(self, request, code):
        sequence_id = request.env['ir.sequence'].sudo().search([('code','=',code)]).id
        if sequence_id:
            name = request.env['ir.sequence'].sudo().next_by_id(sequence_id)
        else:
            raise Exception(
                {
                    'to_root': True,
                    'data': {},
                    'message': 'la secuencia es de tipo {}'.format(code),
                    'message_detail': 'no pudimos obtener el numero'
                                      'de secuencia consecutivo.',
                    'status': '404'
                })
        return name

    def instance_workflow_odoo(self, request, order_id):
        order = request.env['sale.order'].sudo().search([('id','=',order_id)])
        response = order.signal_workflow('act_draft')
        return response