# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_account(http.Controller):
    _request = None

    # Global Variables
    _balance = 0.0
    _past_due_balance = 0.0
    _credit_limit = 0.0
    _invoice_balance = 0.0
    _payment_balance = 0.0
    _credit_note_balance = 0.0
    
    account_status = {}
    
    @http.route(['/clientes/<pk>/cuenta'], type='json', auth="public", website=True, methods=['GET','POST'])
    def accountstatement(self, **kwargs):
        data = {}

        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        pk = kwargs.get('pk',False)
        idate,edate=False,False
        params = request.httprequest.environ['QUERY_STRING'].split('&')
        for param in params:
            if 'fecha_inicial' in param:
                split = param.split('=')
                if len(split) > 1:
                    idate = split[1] if split[1] != '' else False
                else:
                    data = {
                        'to_root': True,
                        'data': {},
                        'message': 'Fecha inicial mal formada',
                        'message_detail': 'La fecha inicial no fue proporcionada correctamente',
                        'status':400
                    }
                    return data
            if 'fecha_final' in param:
                split = param.split('=')
                if len(split) > 1:
                    edate = split[1] if split[1] != '' else False
                else:
                    data = {
                        'to_root': True,
                        'data': {},
                        'message': 'Fecha final mal formada',
                        'message_detail': 'La fecha final no fue proporcionada correctamente',
                        'status':400
                    }
                    return data
        # Get params client_id
        client_id = pk
        # Get Cursor of the DB
        cursor = request.env.cr        
        try:
            if pk.isdigit():
                if client_id:
                    self._validate_exists_parent_id(cursor, client_id)
                    account_status = {
                        'dias_credito': self.get_payment_term(cursor, client_id),
                        'limite_credito': self.get_credit_limit(cursor, client_id),
                        'facturas': self.get_invoices(cursor, client_id, 'Factura', idate, edate),
                        'notas_credito': self.get_invoices(cursor, client_id, 'Nota de credito', idate, edate),
                        'depositos_no_identificados': self.get_unidentified_deposits(cursor, client_id, idate, edate),
                        'anticipos': self.get_advance_payments(cursor, client_id, idate, edate),
                        'pagos': self.get_payments(cursor, client_id, idate, edate),
                        'saldo': self._balance,
                        'saldo_vencido': self._past_due_balance,
                        'credito_disponible': self.credit_available(),
                        'saldo_fatura': self._invoice_balance,
                        'saldo_nota_credito': self._credit_note_balance,
                        'saldo_pagos': self._payment_balance
                    }
            else:
                tools.response_error(
                    expected='cliente',
                    data=pk,
                    type_error='int'    
                )
        except Exception as e:
            data = list(e)[0]
        except:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()),
                'status': 400
            }
        if not data:
            data = {
                'to_root': True,
                'data': account_status,
                'message': 'exitoso',
                'status': 200
            }
        return data

    def _validate_exists_parent_id(self, cursor, client_id):
        data = {}
        if client_id:
            # Get Cursor of the DB
            cursor.execute(
                """
                    SELECT
                        id
                    FROM
                        restful_res_partner_info
                    WHERE
                        id = {}
                """.format(client_id)
            )
            if cursor.rowcount:
                return True
            else:
                data.update(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'No encontrado',
                        'message_detail': 'Cliente no encontrado',
                        'status': 404
                    }
                )
                raise Exception(data)
        else:
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': 'No se recibió identificador de cliente',
                'status': 204
            }
            raise Exception(data)

    def get_payment_term(self, cursor, client_id):
        payment_term = ''
        cursor.execute(
            """
                SELECT
                    value_reference
                FROM
                    ir_property
                WHERE
                    name = 'property_payment_term'
                    AND
                    res_id = 'res.partner,%s';
            """,(int(client_id),)
        )
        if cursor.rowcount:
            value = cursor.fetchone()[0]
            position = string.find(value, ',')
            property_payment_term = value[position + 1:]
            cursor.execute(
                """
                    SELECT
                        name
                    FROM
                        account_payment_term
                    WHERE
                        id = %s;
                """, (property_payment_term,)
            )
            if cursor.rowcount:
                payment_term = cursor.fetchone()[0]
        return payment_term

    def get_credit_limit(self, cursor, client_id):
        credit_limit = 0.0
        cursor.execute(
            """
                SELECT
                    value_float
                FROM
                    ir_property
                WHERE
                    name = 'company_credit_limit'
                    AND
                    res_id = 'res.partner,%s';
            """,(int(client_id),)
        )
        if cursor.rowcount:
            credit_limit = cursor.fetchone()[0]
            self._credit_limit = credit_limit
        return credit_limit

    def get_invoices(self, cursor, client_id, type_doc, idate, edate):
        invoices = []
        query = """
                    SELECT
                        current_date, invoice_issue_date, invoice_date_due, amount_total, invoice_id,
                        invoice_number
                    FROM
                        (SELECT (
                            (invoice_date AT TIME ZONE 'UTC') AT time zone 'America/Mexico_City') AS invoice_issue_date,
                            invoice_date_due,
                            amount_total,
                            invoice_id,
                            invoice_number,
                            customer_id,
                            state,
                            type_doc
                         FROM
                            r_residue_of_customer_data) AS residue_of_customer
                    WHERE
                        type_doc = '%s'
                        AND
                        state = 'Abierto'
                        AND
                        customer_id = %s
                """ % (type_doc, client_id)
        if idate:
            idate += " 00:00:00"
            query += "AND invoice_issue_date >= '%s' " % idate
        if edate:
            edate += " 23:59:59"
            query += "AND invoice_issue_date <= '%s';" % edate
        cursor.execute(query)
        if cursor.rowcount:
            for (current_date, invoice_issue_date, invoice_due_date,
                 amount, invoice_id, folio) in cursor.fetchall():
                balance = float(self.get_invoice_balance(cursor, folio, amount))
                if type_doc == 'Factura':
                    self._balance += balance
                    self._invoice_balance += balance
                    if invoice_due_date < current_date:
                        self._past_due_balance += balance
                else:
                    self._credit_note_balance += balance
                invoice = {
                    'Folio': str(folio),
                    'Referencia': self.get_invoice_reference(cursor, invoice_id),
                    'Emision': str(invoice_issue_date),
                    'Vencimiento': str(invoice_due_date),
                    'Importe': abs(float(amount)),
                    'Saldo': balance
                }
                invoices.append(invoice)
        return invoices

    def get_invoice_reference(self, cursor, invoice_id):
        reference = ''
        cursor.execute(
            """
                SELECT
                    reference
                FROM
                    account_invoice
                WHERE
                    id = %s;
            """, (invoice_id,)
        )
        if cursor.rowcount:
            reference = cursor.fetchone()[0]
        return reference

    def get_invoice_balance(self, cursor, invoice_number, amount):
        balance = 0.0
        cursor.execute(
            """
                SELECT
                    COALESCE(sum(avl.amount),0)
                FROM
                    account_voucher av
                INNER JOIN
                    account_voucher_line avl ON av.id=avl.voucher_id
                INNER JOIN
                    account_move_line aml ON avl.move_line_id=aml.id
                INNER JOIN
                    account_invoice ai ON aml.move_id=ai.move_id
                WHERE
                    ai.number = %s
                    AND
                    av.move_id IS NOT NULL;
            """,(invoice_number,)
        )
        if cursor.rowcount:
            payments = cursor.fetchone()[0]
            balance = abs(amount) - payments
        return balance

    def get_unidentified_deposits(self, cursor, client_id, idate, edate):
        deposits = []
        query = """
                    SELECT
                        name,
                        id,
                        amount,
                        tax,
                        partner_id,
                        deposit_date
                    FROM
                        (
                            SELECT
                                name,
                                id,
                                amount,
                                tax,
                                partner_id,
                                ((deposit_date AT TIME ZONE 'UTC') AT time zone 'America/Mexico_City') AS deposit_date
                            FROM
                                dni_dni
                            WHERE
                                state in ('partial_applied', 'associated')
                        ) AS unidentified_deposits
                    WHERE
                        partner_id = %s
                """ % (client_id,)
        if idate:
            idate += " 00:00:00"
            query += "AND deposit_date >= '%s' " % idate
        if edate:
            edate += " 23:59:59"
            query += "AND deposit_date <= '%s';" % edate
        cursor.execute(query)
        if cursor.rowcount:
            for (folio, deposit_id, amount, tax, partner_id,
                 deposit_date) in cursor.fetchall():
                amount_total = round(float(amount) - float(tax), 2)
                deposit = {
                    'Folio': str(folio),
                    'Referencia': '',
                    'Emision': str(deposit_date),
                    'Vencimiento': '',
                    'Importe': amount_total,
                    'Saldo': self.get_deposit_balance(cursor, deposit_id, amount_total)
                }
                deposits.append(deposit)
        return deposits

    def get_deposit_balance(self, cursor, deposit_id, amount_total):
        balance = 0.0
        cursor.execute(
            """
                SELECT
                    amount,
                    account_voucher_id
                FROM
                    dni_dni_aplicated
                WHERE
                    state = 'aplicated'
                    AND
                    dni_id = %s;
            """, (deposit_id,)
        )
        if cursor.rowcount:
            applied_amoutn = 0.0
            for (amount, voucher_id) in cursor.fetchall():
                applied_amoutn += abs(float(amount))
            balance = amount_total - applied_amoutn
        return round(balance, 2)

    def get_advance_payments(self, cursor, client_id, idate, edate):
        deposits = []
        query = """
                    SELECT
                        register_date,
                        number,
                        id,
                        amount
                    FROM
                        (
                            SELECT
                                ((register_date AT TIME ZONE 'UTC') AT time zone 'America/Mexico_City') AS register_date,
                                number,
                                id,
                                amount,
                                partner_id
                            FROM
                                advance_payment
                            WHERE
                                state in ('generated', 'applied partial')
                        ) AS advance_payment
                    WHERE
                        partner_id = %s
                """ % client_id
        if idate:
            idate += " 00:00:00"
            query += "AND register_date >= '%s' " % idate
        if edate:
            edate += " 23:59:59"
            query += "AND register_date <= '%s';" % edate
        cursor.execute(query)
        if cursor.rowcount:
            for (advance_issue, folio, advance_id, amount) in cursor.fetchall():
                deposit = {
                    'Folio': str(folio),
                    'Referencia': '',
                    'Emision': str(advance_issue),
                    'Vencimiento': '',
                    'Importe': float(amount),
                    'Saldo': self.get_advance_balance(cursor, advance_id, amount)
                }
                deposits.append(deposit)
        return deposits

    def get_advance_balance(self, cursor, advance_id, amount_total):
        balance = 0.0
        cursor.execute(
            """
                SELECT
                    payment_amount,
                    number
                FROM
                    advance_payment_line
                WHERE
                    state = 'accepted'
                    AND
                    advance_payment_id = %s;
            """,(advance_id,)
        )
        if cursor.rowcount:
            applied_amoutn = 0.0
            for (amount, voucher_id) in cursor.fetchall():
                applied_amoutn += float(amount)
            balance = float(amount_total) - applied_amoutn
        return round(balance, 2)

    def get_payments(self, cursor, client_id, idate, edate):
        payments = []
        query = """
                    SELECT
                        av.id,
                        av.number,
                        av.date,
                        av.amount
                    FROM
                        account_voucher av
                    WHERE
                        av.partner_id = %s
                        AND
                        av.state = 'posted'
                """ % client_id
        if idate:
            query += "AND av.date >= '%s' " % idate
        if edate:
            query += "AND av.date <= '%s';" % edate
        cursor.execute(query)
        if cursor.rowcount:
            for (voucher_id, folio, payment_date, amount) in cursor.fetchall():
                balance, amount_total = self.get_payment_balance(cursor, voucher_id, amount)
                self._payment_balance = balance
                payment = {
                    'Folio': str(folio),
                    'Referencia': '',
                    'Emision': str(payment_date),
                    'Vencimiento': '',
                    'Importe': amount_total,
                    'Saldo': balance,
                    'Tipo': self.get_payment_type(cursor, voucher_id)
                }
                payments.append(payment)
        return payments

    def get_payment_balance(self, cursor, client_id, amount):
        amount_cr = 0.0
        amount_dr = 0.0
        cursor.execute(
            """
                SELECT
                    COALESCE(sum(avl.amount),0)
                FROM
                    account_voucher_line avl
                WHERE
                    avl.voucher_id = %s
                    AND
                    type = 'cr';
            """,(client_id,)
        )
        if cursor.rowcount:
            amount_cr = cursor.fetchone()[0]
        cursor.execute(
            """
                SELECT
                    COALESCE(sum(avl.amount),0)
                FROM
                    account_voucher_line avl
                WHERE
                    avl.voucher_id = %s
                    AND
                    type = 'dr';
            """,(client_id,)
        )
        if cursor.rowcount:
            amount_dr = cursor.fetchone()[0]
        amount_total = amount + amount_dr
        balance = (amount + amount_dr) - amount_cr
        return round(balance, 2), round(amount_total, 2)

    def get_payment_type(self, cursor, client_id):
        payment_type = ""
        cursor.execute(
            """
                SELECT
                    aaa.name
                FROM
                    account_account_apply AS aaa
                INNER JOIN
                    account_journal AS aj ON aaa.id = aj.account_account_apply_id
                INNER JOIN
                    account_voucher AS av ON aj.id = av.journal_id
                WHERE av.id = %s;
            """,(client_id,)
        )
        if cursor.rowcount:
            payment_type = cursor.fetchone()[0]
        return payment_type

    def credit_available(self):
        if self._balance > self._credit_limit:
            exceeded = self._balance - self._credit_limit
            result = "Limite exedido por $ %s" % exceeded
        else:
            result = round(self._credit_limit - self._balance, 2)
        return result