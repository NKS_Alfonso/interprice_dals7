# -*- coding: utf-8 -*-
import logging
from openerp import http
from openerp.http import request
import json
import sys
from .. controllers.WSAPITools import WSAPITools
_logger = logging.getLogger(__name__)

class ws_todo_products(http.Controller):
    
    @http.route(['/productos/'], type='json', auth="public", website=True, methods=['GET','POST'])
    def product(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        queryset = []
        skus_add = []
        err_skus = []
        precio_mxn = None
        rate = 0
        params = request.httprequest.environ['QUERY_STRING'].split('&')
        products_skus = False
        for param in params:
            if 'sku' in param:
                skus = param.split('=')
                if len(skus) > 1:
                    products_skus = skus[1] if skus[1] != '' else False
        try:
            company_currency, company_currency_id = tools.get_company_currency(request.env.cr)
            if products_skus:
                products_skus = products_skus.split(',')
                request.env.cr.execute("""
                    SELECT
                        p.*
                    FROM
                        restful_get_products as p
                    WHERE
                        p.sku IN %s;
                """, [tuple(products_skus)])
            else:
                request.env.cr.execute("""
                    SELECT
                        p.*
                    FROM
                        restful_get_products as p;
                """)
            resultq = request.env.cr.fetchall()
            if len(resultq) > 0:
                counter = 1
                for (product_id,
                     product_tmpl_id,
                     product_sku,
                     product_name,
                     product_clave_f,
                     product_ean13,
                     product_volume,
                     product_weight,
                     product_weight_net,
                     currency_name,
                     product_list_price,
                     cat_section_id,
                     cat_section_name,
                     cat_line_id,
                     cat_line_name,
                     cat_brand_id,
                     cat_brand_name,
                     cat_serie_id,
                     cat_serie_name, currency_id) in resultq:
                    # Create a list of sku
                    skus_add.append(product_sku)
                    # Get the current rate
                    request.env.cr.execute("""
                        SELECT
                            rate_sale
                        FROM
                            res_currency_rate
                        WHERE
                            currency_id = {}
                        ORDER BY
                            id DESC
                        LIMIT 1;
                    """.format(currency_id))
                    resultq2 = request.env.cr.fetchall()
                    if len(resultq2):
                        rate = resultq2[0]
                        if float(rate[0]) > 0.000000:
                            rate = (1 / float(rate[0]))
                        else:
                            raise Exception(
                                {
                                    'to_root': True,
                                    'data': {},
                                    'message': 'El rate de venta de la moneda no ha sido definido',
                                    'message_detail': 'El rate de venta de la moneda {} es cero o menor a cero'.format(currency_name),
                                    'status': 404
                                }
                            )
                    else:
                        raise Exception(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Falta configurar Moneda',
                                'message_detail': 'El producto {} no tiene establecida su moneda'.format(product_sku),
                                'status': 404
                            }
                        )
                    # Covert price from product currency to company currency
                    if company_currency == currency_name:
                        precio_mxn = product_list_price
                    else:
                        precio_mxn = tools.price_convert(
                            currency_name,
                            company_currency,
                            product_list_price,
                            request.env.cr
                        )
                    if not precio_mxn:
                        precio_mxn = 0

                    # Get all stock of cost centers by Product
                    request.env.cr.execute("""
                        SELECT
                            *
                        FROM
                            restful_get_stock_product({});
                    """.format(product_id))
                    results_centro_costos = request.env.cr.fetchall()

                    # Make a dict to convert a Json
                    queryset.append({
                        'id': product_tmpl_id,
                        'sku': product_sku,
                        'nombre': product_name,
                        'clave_fabricante': product_clave_f,
                        'UPC': product_ean13,
                        'volumen': product_volume,
                        'peso_bruto': product_weight,
                        'peso_neto': product_weight_net,
                        'moneda': currency_name,
                        'precios': {
                            'precio_base': product_list_price,
                            'precio_base_MXN': float(precio_mxn),
                            # 'niveles_precio':dict(results_price_level)
                        },
                        'categorias': {
                            'seccion': {cat_section_id: cat_section_name},
                            'linea':   {cat_line_id: cat_line_name},
                            'marca':   {cat_brand_id: cat_brand_name},
                            'serie':   {cat_serie_id: cat_serie_name}
                        },
                        'centro_de_costos': dict(results_centro_costos)
                    })
                # Looking products not found
                if products_skus:
                    for products in products_skus:
                        if products not in skus_add:
                            err_skus.append(products)
                # if success building response in data
                data = {
                    'to_root': True,
                    'data': {
                                'productos': queryset,
                                'sku_errores': err_skus
                            },
                    'message': 'exitoso',
                    'status': 200
                }
            else:
                raise Exception(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'no encontrado',
                        'message_detail': 'productos no encontrados',
                        'status': 404
                    }
                )
        except Exception as e:
            if e.message != '':
                if isinstance(e.message, dict):
                    data = {
                        'to_root': True,
                        'data': {},
                        'message': 'error',
                        'message_detail': '{}'.format(e.message.get('message_detail')),
                        'status': 400
                    }
            else:
                # Error
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(list(e)[0]),
                    'status': 400
                }
        #return Response(data,content_type='application/json;charset=utf-8')
        return data
    
    @http.route(['/productos/precios'], type='json', auth="public", website=True, methods=['GET','POST'])
    def productPrices(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        queryset = []
        skus_add = []
        err_skus = []
        precio_mxn = None
        rate = 0
        # Get params sku
        params = request.httprequest.environ['QUERY_STRING'].split('&')
        products_skus = False
        for param in params:
            if 'sku' in param:
                skus = param.split('=')
                if len(skus) > 1:
                    products_skus = skus[1] if skus[1] != '' else False
        try:
            company_currency, company_currency_id = tools.get_company_currency(request.env.cr)
            if products_skus:
                products_skus = products_skus.split(',')
                request.env.cr.execute("""
                    SELECT
                        p.*
                    FROM
                        restful_get_products as p
                    WHERE
                        p.sku IN %s;
                """, [tuple(products_skus)])
            else:
                request.env.cr.execute("""
                    SELECT
                        p.*
                    FROM
                        restful_get_products as p;
                """)
            resultq = request.env.cr.fetchall()
            if len(resultq) > 0:
                for (product_id,
                     product_tmpl_id,
                     product_sku,
                     product_name,
                     product_clave_f,
                     product_ean13,
                     product_volume,
                     product_weight,
                     product_weight_net,
                     currency_name,
                     product_list_price,
                     cat_section_id,
                     cat_section_name,
                     cat_line_id,
                     cat_line_name,
                     cat_brand_id,
                     cat_brand_name,
                     cat_serial_id,
                     cat_serial_name, currency_id) in resultq:
                    # Create a list of sku to check
                    skus_add.append(product_sku)
                    # Get the current rate
                    request.env.cr.execute("""
                        SELECT
                            rate_sale
                        FROM
                            res_currency_rate
                        WHERE
                            currency_id = {}
                        ORDER BY
                            id DESC
                        LIMIT 1;
                    """.format(currency_id))
                    resultq2 = request.env.cr.fetchall()
                    if len(resultq2):
                        rate = resultq2[0]
                        if float(rate[0]) > 0.000000:
                            rate = (1 / float(rate[0]))
                        else:
                            raise Exception(
                                {
                                    'to_root': True,
                                    'data': {},
                                    'message': 'El rate de venta de la moneda no ha sido definido',
                                    'message_detail': 'El rate de venta de la moneda {} es cero o menor a cero'.format(currency_name),
                                    'status': 404
                                }
                            )
                    else:
                        raise Exception(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'Falta configurar Moneda',
                                'message_detail': 'El producto {} no tiene establecida su moneda'.format(product_sku),
                                'status': 404
                            }
                        )
                    # Covert USD to Pesos, only if it comes in dollars
                    if company_currency == currency_name:
                        precio_mxn = product_list_price
                    else:
                        precio_mxn = tools.price_convert(
                            currency_name,
                            company_currency,
                            product_list_price,
                            request.env.cr
                        )

                    if not precio_mxn:
                        precio_mxn = 0
                    # Build dict to convert a Json
                    queryset.append({
                        'id': product_tmpl_id,
                        'sku': product_sku,
                        'moneda': currency_name,
                        'precios': {
                            'precio_base': product_list_price,
                            'precio_base_MXN': float(precio_mxn),
                        }
                    })
                # Looking products not found
                if products_skus:
                    for products in products_skus:
                        if products not in skus_add:
                            err_skus.append(products)
                # if success make response in data
                data = {
                    'to_root': True,
                    'data': {
                                'productos': queryset,
                                'sku_errores': err_skus
                            },
                    'message': 'exitoso',
                    'status': 200
                }
            else:
                raise Exception(
                    {
                        'to_root': True,
                        'data': {},
                        'message': 'no encontrado',
                        'message_detail': 'productos no encontrados',
                        'status': 404
                    }
                )
        except Exception as e:
            if e.message != '':
                if isinstance(e.message, dict):
                    data = {
                        'to_root': True,
                        'data': {},
                        'message': 'error',
                        'message_detail': '{}'.format(e.message.get('message_detail')),
                        'status': 400
                    }
            else:
                # Error
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(list(e)[0]),
                    'status': 400
                }
        return data
    
    @http.route(['/productos/existencias'], type='json', auth="public", website=True, methods=['GET','POST'])
    def productStock(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        queryset = []
        skus_add = []
        err_skus = []
        results = None

        # Get params sku
        params = request.httprequest.environ['QUERY_STRING'].split('&')
        products_skus = False
        for param in params:
            if 'sku' in param:
                skus = param.split('=')
                if len(skus) > 1:
                    products_skus = skus[1] if skus[1] != '' else False

        try:
            if products_skus:
                products_skus = products_skus.split(',')
                request.env.cr.execute(
                    """
                        SELECT
                            p.*
                        FROM
                            restful_get_products as p
                        WHERE
                            p.sku IN %s
                    """, [tuple(products_skus)]
                )
            else:
                request.env.cr.execute("""
                    SELECT
                        p.*
                    FROM
                        restful_get_products as p
                """)

            results = request.env.cr.fetchall()
            if len(results) <= 0:
                raise Exception(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'no encontrado',
                            'message_detail': 'productos no encontrados',
                            'status': 404
                        }
                )
            for res in results:
                # Create a list of sku
                skus_add.append(res[2])

                # Get all stock of cost centers by Product
                request.env.cr.execute(
                    """
                        SELECT
                            * 
                        FROM
                            restful_get_stock_product({})
                    """.format(res[0])
                )
                results_centro_costos = request.env.cr.fetchall()

                # Make a dict to convert a Json
                queryset.append({
                    'id': res[1],
                    'sku': res[2],
                    'centro_de_costos': dict(results_centro_costos)
                })

            if products_skus:
                for products in products_skus:
                    if products not in skus_add:
                        err_skus.append(products)

            # if success make response in data
            data = {
                'to_root': True,
                'data': {
                            'productos': queryset,
                            'sku_errores': err_skus
                        },
                'message': 'exitoso',
                'status': 200
            }

        except Exception as e:
            # Products not found
            data = list(e)[0]

        except:
            # Error
            data = {
                'to_root': True,
                'data': {},
                'message': 'error',
                'message_detail': '{}'.format(sys.exc_info()[1]),
                'status': 400
            }

        return data

    @http.route(['/clientes/<pk>/producto/<sku>/precio'], type='json', auth="public", website=True, methods=['GET'])
    def productpriceforcustomer(self, **kwargs):
        tools = WSAPITools()
        ipvalid = tools.validate_ip(request)
        if not ipvalid:
            data = {
                'to_root': True,
                'data': {},
                'message': 'Request bloqueada por seguridad',
                'message_detail': 'La request no cumple con los requerimientos de seguridad',
                'status': 404
            }
            return data
        data = {}
        queryset = []
        skus_add = []
        err_skus = []
        precio_custom = None
        rate = 0
        pk = kwargs.get('pk', False)
        sku = kwargs.get('sku', False)
        # Get params sku
        products_skus = sku
        # Get Cursor of the DB
        cursor = request.env.cr
        validatepk = tools.validatenumber(pk)
        try:
            if validatepk:
                company_currency, company_currency_id = tools.get_company_currency(cursor)
                # We validate client exists in DB
                partner_id = tools.validate_identificator(
                    'cliente',
                    'res_partner',
                    'id',
                    pk,
                    cursor
                )
                if products_skus:
                    products_skus = products_skus.split(',')
                    query_price_product = """
                        SELECT
                            p.*,
                            (
                                SELECT
                                    *
                                FROM
                                    restful_calc_price_unit(%s, %s, %s, p.id, p.sku)
                            )
                        FROM
                            restful_get_products as p
                        WHERE
                            p.sku IN %s"""
                    cursor.execute(query_price_product,
                        [
                            partner_id,
                            company_currency_id,
                            company_currency,
                            tuple(products_skus)
                        ]
                    )
                    if cursor.rowcount:
                        for (product_id,
                             product_tmpl_id,
                             product_sku,
                             product_name,
                             product_clave_f,
                             product_ean13,
                             product_volume,
                             product_weight,
                             product_weight_net,
                             currency_name,
                             product_list_price,
                             cat_section_id,
                             cat_section_name,
                             cat_line_id,
                             cat_line_name,
                             cat_brand_id,
                             cat_brand_name,
                             cat_serie_id,
                             cat_serie_name,
                             currency_id,
                             product_price_custom) in cursor.fetchall():
                            # Create a list of sku
                            skus_add.append(product_sku)
                            # Get the current rate
                            cursor.execute("""
                                SELECT
                                    rate_sale
                                FROM
                                    res_currency_rate
                                WHERE
                                    currency_id = {}
                                ORDER BY
                                    id DESC
                                LIMIT 1;
                            """.format(currency_id))
                            if cursor.rowcount:
                                rate = cursor.fetchall()[0]
                                if float(rate[0]) > 0.000000:
                                    rate = (1 / float(rate[0]))
                                else:
                                    raise Exception(
                                        {
                                            'to_root': True,
                                            'data': {},
                                            'message': 'El rate de venta de la moneda no ha sido definido',
                                            'message_detail': 'El rate de venta de la moneda {} es cero o menor a cero'.format(currency_name),
                                            'status': 404
                                        }
                                    )
                            else:
                                raise Exception(
                                    {
                                        'to_root': True,
                                        'data': {},
                                        'message': 'Falta configurar Moneda',
                                        'message_detail': 'El producto {} no tiene establecida su moneda'.format(product_sku),
                                        'status': 404
                                    }
                                )
                            ###ojooooooooooooooooooooooooooooooooooooo
                            # Covert USD to Pesos, only if it comes in dollars
                            if company_currency == currency_name:
                                precio_mxn = product_list_price
                            else:
                                precio_mxn = tools.price_convert(
                                    currency_name,
                                    company_currency,
                                    product_list_price,
                                    request.env.cr
                                )
                            # Custom Price to each client
                            if product_price_custom is not None:
                                precio_custom = '{0:.2f}'.format(
                                    float(product_price_custom)
                                )
                            else:
                                precio_custom = product_price_custom
                            # if not price
                            if not precio_mxn:
                                precio_mxn = 0
                            if not precio_custom:
                                precio_custom = 0
                            # Build a dict to convert a Json
                            queryset.append({
                                'id': product_tmpl_id,
                                'sku': product_sku,
                                'precios': {
                                    'precio_base': product_list_price,
                                    'precio_base_MXN': float(precio_mxn),
                                    'precio_cliente_MXN': float(precio_custom)
                                }
                            })
                        # Looking products not found
                        if products_skus:
                            for products in products_skus:
                                if products not in skus_add:
                                    err_skus.append(products)
                        # if success make response in data
                        data = {
                            'to_root': True,
                            'data': {
                                        'productos': queryset,
                                        'sku_errores': err_skus
                                    },
                            'message': 'exitoso',
                            'status': 200
                        }
                    else:
                        raise Exception(
                            {
                                'to_root': True,
                                'data': {},
                                'message': 'no encontrado',
                                'message_detail': 'productos no encontrados',
                                'status': 404
                            }
                        )
                else:
                    raise Exception(
                        {
                            'to_root': True,
                            'data': {},
                            'message': 'Sku´s requeridos',
                            'message_detail': "La peticion para clientes necesita"
                                               "uno o varios Sku´s",
                            'status': 400
                        }
                    )
            else:
                tools.response_error(
                    expected='cliente',
                    data=pk,
                    type_error='int'
                )
        except Exception as e:
            if e.message != '':
                if isinstance(e.message, dict):
                    data = {
                        'to_root': True,
                        'data': {},
                        'message': 'error',
                        'message_detail': '{}'.format(e.message.get('message_detail')),
                        'status': 400
                    }
            else:
                # Error
                data = {
                    'to_root': True,
                    'data': {},
                    'message': 'error',
                    'message_detail': '{}'.format(list(e)[0]),
                    'status': 400
                }
        return data