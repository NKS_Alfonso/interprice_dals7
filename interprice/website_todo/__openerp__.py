# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
{
    'name': 'Website-ToDo',
    'category': 'Website',
    'summary': 'Website ToDo',
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'version': '1.0',
    'description': 'Public actions for ToDo and Magento',
    'depends': ['sale', 'payment'],
    'data': [
        'views/templates.xml',
    ],
    'demo': [

    ],
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'application': True,
}