# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _


class RepeatedProductsList(models.TransientModel):
    _name = 'repeated.products.lines'

    line_id = fields.Many2one(
        comodel_name='repeated.products.window',
        string=_('Line ids'),
    )
    # This is for repeated products only
    name = fields.Char(
        readonly=True,
        string=_('Product name'),
    )

    product_reference = fields.Char(
        readonly=True,
        string=_('Product reference'),
    )

    promotion_name = fields.Char(
        readonly=True,
        string=_('Promotion name'),
    )
