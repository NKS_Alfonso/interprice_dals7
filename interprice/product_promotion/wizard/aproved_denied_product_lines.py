# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _


class AprovedDeniedProductsList(models.TransientModel):
    _name = 'aproved.denied.products.lines'

    line_id = fields.Many2one(
        comodel_name='repeated.products.window',
        string=_('Line ids'),
    )

    name = fields.Char(
        readonly=True,
        string=_('Product name'),
    )

    product_reference = fields.Char(
        readonly=True,
        string=_('Product reference',)
    )

    avg_cost = fields.Char(
        readonly=True,
        string=_('Average cost')
    )
    discount = fields.Char(
        readonly=True,
        string=_('Discount')
    )
    price_list = fields.Char(
        readonly=True,
        string=_('Price List')
    )
    disc_price = fields.Char(
        readonly=True,
        string=_('Discount Price')
    )
    price_listc = fields.Char(
        readonly=True,
        string=_('Price list converted')
    )
    disc_pricec = fields.Char(
        readonly=True,
        string=_('Discount price converted')
    )
    status = fields.Boolean(
        readonly=True,
        string=_("Status"),
    )
