# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _
import logging
_logger = logging.getLogger(__name__)


class ProductLevels(models.TransientModel):
    _name = 'product.levels.window'
    _rec_name = 'promotion_id'

    promotion_id = fields.Many2one(
        comodel_name='product.promotion',
        string=_('Promotion'),
    )

    product_levels_ids = fields.One2many(
        comodel_name='product.levels.lines',
        inverse_name='line_id',
        string=_('Lines'),
    )

    @api.multi
    def level_prices(self, line_ids, apply_levels, level_ids):
        wizard_form = self.env.ref(
            'product_promotion.product_levels_window',
            False
        )

        values = []
        price_values = []
        calc_values = []
        promo_values = []

        if level_ids:
            for record in level_ids:
                for calc_record in record:
                    calc = calc_record.discount
                    calc_values.append(calc)
        if line_ids and apply_levels:
            for record in line_ids:
                y = 0
                x = list(record.product_id.product_price_levels_nopop)
                for price_record in x:
                    price = price_record.price
                    price_values.append(price)
                    discount = calc_values[y] * price / 100
                    promo_price = price - discount
                    promo_values.append(promo_price)
                    y += 1
                values.append((0, 0, {
                    'default_code': record.product_id.default_code,
                    'name': record.product_id.name,
                    'level1_price': price_values[0],
                    'level2_price': price_values[1],
                    'level3_price': price_values[2],
                    'level4_price': price_values[3],
                    'level5_price': price_values[4],
                    'level6_price': price_values[5],
                    'level7_price': price_values[6],
                    'level8_price': price_values[7],
                    'level9_price': price_values[8],
                    'level10_price': price_values[9],
                    'level1_discount': calc_values[0],
                    'level2_discount': calc_values[1],
                    'level3_discount': calc_values[2],
                    'level4_discount': calc_values[3],
                    'level5_discount': calc_values[4],
                    'level6_discount': calc_values[5],
                    'level7_discount': calc_values[6],
                    'level8_discount': calc_values[7],
                    'level9_discount': calc_values[8],
                    'level10_discount': calc_values[9],
                    'level1_promo': promo_values[0],
                    'level2_promo': promo_values[1],
                    'level3_promo': promo_values[2],
                    'level4_promo': promo_values[3],
                    'level5_promo': promo_values[4],
                    'level6_promo': promo_values[5],
                    'level7_promo': promo_values[6],
                    'level8_promo': promo_values[7],
                    'level9_promo': promo_values[8],
                    'level10_promo': promo_values[9],
                }))
                del price_values[:]
                del promo_values[:]

        view_id = self.env['product.levels.window']
        vals = {
            'product_levels_ids': values
        }
        new = view_id.create(vals)
        return {
            'name': 'Product levels prices',
            'type': 'ir.actions.act_window',
            'res_model': 'product.levels.window',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }
