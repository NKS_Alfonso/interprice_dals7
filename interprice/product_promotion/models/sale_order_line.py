# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _
import logging
import struct
_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _name = 'sale.order'


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    _name = 'sale.order.line'

    def calc_price_unit(
            self, cr, uid, ids, client_id=None,
            product_id=None, product_code="", currency_id=None,
            currency_name="", context=None):

        res = super(SaleOrderLine, self).calc_price_unit(
            cr, uid, ids, client_id,
            product_id, product_code, currency_id,
            currency_name, context
        )

        if product_id:
            env = api.Environment(cr, uid, [])
            self.env = env
            partner = env['res.partner'].browse(client_id)
            product = env['product.product'].browse(product_id)
            currency = env['res.currency'].browse(currency_id)
            if not partner.segment and not partner.brand:
                promotion_line = self.has_promotion(product, partner)
                if promotion_line:
                    promotion = promotion_line.promotion_id
                    price_unit = res['value']['price_unit']
                    template = product.product_tmpl_id
                    level = int(partner.base.level_id)
                    product_price_levels = template.product_price_levels_nopop
                    price = template.list_price
                    price_for_level = product_price_levels[level-1].price

                    if currency.name == 'MXN':
                        if template.list_price_currency.name != 'MXN':
                            price = self.convert_usd_to_mxn(price)
                            price_for_level = self.convert_usd_to_mxn(
                                price_for_level
                            )
                    if currency.name == 'USD':
                        if template.list_price_currency.name != 'USD':
                            price = self.convert_mxn_to_usd(price)
                            price_for_level = self.convert_mxn_to_usd(
                                price_for_level
                            )

                    if not promotion.apply_levels:
                        disc, discount_price = promotion_line. \
                            calculate_discount(
                                price,
                                promotion_line.disc_percentage
                            )
                    else:
                        level_promotion_lines = promotion.level_ids
                        level = level_promotion_lines[level-1]
                        disc, discount_price = promotion_line. \
                            calculate_discount(
                                price,
                                level.discount
                            )

                    if discount_price > price_for_level:
                        res['value']['price_unit'] = price_for_level
                    else:
                        res['value']['price_unit'] = discount_price
        return res

    def convert_usd_to_mxn(self, amount):
        usd_currency = self.env['res.currency'].search([('name', '=', 'USD')])
        tc = usd_currency.rate_silent
        usd_price = 1 / tc
        amount_in_mxn = usd_price * (amount)
        return amount_in_mxn

    def convert_mxn_to_usd(self, amount):
        usd_currency = self.env['res.currency'].search([('name', '=', 'USD')])
        tc = usd_currency.rate_silent
        usd_price = 1 / tc
        amount_in_usd = amount / usd_price
        return amount_in_usd

    def has_promotion(self, product, partner):
        product_promotion_obj = self.env['product.promotion.lines']
        results = product_promotion_obj.search([
            ('product_id', '=', product.product_tmpl_id.id),
            ('promotion_id.state', 'in', ['active']),
        ], limit=1)
        if results:
            user = self.env.user
            segment_ids = results.segment.ids
            branch_office_ids = set(results.branch_office.ids)
            user_branch_office_ids = set(user.centro_costo_id.ids)

            if not branch_office_ids:
                if segment_ids:
                    if partner.client_segment.id in segment_ids:
                        return results
                else:
                    return results

            elif user_branch_office_ids & branch_office_ids:
                if segment_ids:
                    if partner.client_segment.id in segment_ids:
                        return results
                else:
                    return results
            else:
                return False
        else:
            return False

    def product_id_change_with_wh(
            self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False,
            packaging=False, fiscal_position=False,
            flag=False, warehouse_id=False, context=None):

        res = super(SaleOrderLine, self).product_id_change_with_wh(
            cr, uid, ids, pricelist, product, qty,
            uom, qty_uos, uos, name, partner_id,
            lang, update_tax, date_order, packaging, fiscal_position,
            flag, warehouse_id, context
        )
        if product:
            x = self.calc_price_unit(
                cr, uid, ids, client_id=partner_id,
                product_id=product, currency_id=context['currency_id'],
                context=context
            )
            if 'price_unit' in x['value']:
                res['value']['price_unit'] = x['value']['price_unit']
        return res

    @api.model
    def create(self, vals):
        res = super(SaleOrderLine, self).create(vals)
        if not res.is_delivery:
            x = self.calc_price_unit(
                client_id=res.order_id.partner_id.id,
                product_id=res.product_id.id,
                currency_id=res.order_id.currency.id,
                context={}
            )
            if 'price_unit' in x['value']:
                res.price_unit = x['value']['price_unit']
        return res
