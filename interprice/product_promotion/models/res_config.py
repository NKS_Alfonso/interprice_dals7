# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class productPromotionSettings(models.TransientModel):
    _name = 'product.promotion.settings'
    _inherit = 'res.config.settings'

    promotion_add_products = fields.Boolean(
        string=(_("Add products to the promotion when it's active")),
        help=(_("""If this is active, then its posible to add products to an
            active promotion""")),
    )

    promotion_limit_discount = fields.Integer(
        string=_('Limit discount'),
        help=_('Allows the maximum discount'),
    )

    @api.model
    def get_default_promotion_values(self, fields):
        obj = self.env['internal.product.promotion.settings'].search(
            [],
            limit=1
        )
        return {
            'promotion_add_products': obj.promotion_add_products,
            'promotion_limit_discount': obj.promotion_limit_discount,
        }

    @api.one
    def set_promotion_values(self):
        self._cr.execute(
            """UPDATE internal_product_promotion_settings
            SET promotion_add_products=%s, promotion_limit_discount='%s'""",
            (self.promotion_add_products, self.promotion_limit_discount)
        )


class InternalProductPromotionSettings(models.Model):
    _name = 'internal.product.promotion.settings'

    promotion_add_products = fields.Boolean()
    promotion_limit_discount = fields.Integer()

    def init(self, cr):
        cr.execute("""
            SELECT * FROM internal_product_promotion_settings LIMIT 1
        """)

        if not cr.fetchone():
            cr.execute("""
                INSERT INTO internal_product_promotion_settings
                (promotion_add_products, promotion_limit_discount)
                VALUES (FALSE,0)
            """)
