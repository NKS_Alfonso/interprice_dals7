# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _
import logging
_logger = logging.getLogger(__name__)


class ProductPromotionLines(models.Model):
    _name = 'product.promotion.lines'
    _rec_name = 'product_id'

    # We validate the user's center costs assigned that he only can assign
    @api.model
    def center_cost_domain(self):
        res = []
        res = self.env.user.centro_costo_id.ids
        return [('id', 'in', res)]

    promotion_id = fields.Many2one(
        comodel_name='product.promotion',
        string=_("Promotion"),
    )
    product_code = fields.Char(
        string=_("Product code"),
        readonly=True,
        store=True,
    )
    product_id = fields.Many2one(
        comodel_name='product.template',
        string=_("Product"),
        required=True,
    )
    cur_cr = fields.Char(
        readonly=True,
        string=_("RC Cur"),
    )
    rep_cost = fields.Float(
        readonly=True,
        string=_("Rep Cost"),
    )
    cur = fields.Char(
        readonly=True,
        string=_("Cur"),
    )
    price = fields.Float(
        digits=(6, 3),
        readonly=True,
        string=_("Price"),
    )
    disc = fields.Float(
        digits=(6, 3),
        readonly=True,
        string=_("Discount"),
    )
    disc_price = fields.Float(
        digits=(6, 3),
        readonly=True,
        string=_("Discount price"),
    )
    disc_percentage = fields.Float(
        readonly=False,
        store=True,
        string=_("Discount (%)"),
    )
    user = fields.Many2one(
        comodel_name='res.users',
        default=lambda self: self.env.user,
        readonly=True,
        string=_("User"),
    )
    creation_date = fields.Datetime(
        string=_("Creation date"),
        readonly=True,
        default=lambda self: fields.datetime.now(),
    )
    branch_office = fields.Many2many(
        comodel_name='account.cost.center',
        domain=center_cost_domain,
        string=_("Branch office"),
    )
    segment = fields.Many2many(
        comodel_name='product.segment',
        string=_("Segment"),
    )
    status = fields.Boolean(
        readonly=True,
        string=_("Status")
    )

    # RELATED FIELDS FOR REPORT
    promotion_folio = fields.Char(
        related="promotion_id.folio",
        string=_("Folio"),
    )
    promotion_name = fields.Char(
        related="promotion_id.name",
        string=_("Promotion Name"),
    )
    promotion_status = fields.Selection(
        related="promotion_id.state",
        string=_("Status"),
    )
    is_level = fields.Char(
        related="promotion_id.is_level",
        string=_("Is level"),
    )
    start_date = fields.Datetime(
        related="promotion_id.start_date",
        string=_("Start Date"),
    )
    end_date = fields.Datetime(
        related="promotion_id.end_date",
        string=_("End Date"),
    )
    global_discount = fields.Float(
        related="promotion_id.discount",
        string=_("Global Discount"),
    )
    level1_price = fields.Char(
        string=_("Lvl 1 Price")
    )
    level1_discount = fields.Char(
        string=_("Lvl 1 Discount")
    )
    level1_promo = fields.Char(
        string=_("Lvl 1 Promo")
    )
    level2_price = fields.Char(
        string=_("Lvl 2 Price")
    )
    level2_discount = fields.Char(
        string=_("Lvl 2 Discount")
    )
    level2_promo = fields.Char(
        string=_("Lvl 2 Promo")
    )
    level3_price = fields.Char(
        string=_("Lvl 3 Price")
    )
    level3_discount = fields.Char(
        string=_("Lvl 3 Discount")
    )
    level3_promo = fields.Char(
        string=_("Lvl 3 Promo")
    )
    level4_price = fields.Char(
        string=_("Lvl 4 Price")
    )
    level4_discount = fields.Char(
        string=_("Lvl 4 Discount")
    )
    level4_promo = fields.Char(
        string=_("Lvl 4 Promo")
    )
    level5_price = fields.Char(
        string=_("Lvl 5 Price")
    )
    level5_discount = fields.Char(
        string=_("Lvl 5 Discount")
    )
    level5_promo = fields.Char(
        string=_("Lvl 5 Promo")
    )
    level6_price = fields.Char(
        string=_("Lvl 6 Price")
    )
    level6_discount = fields.Char(
        string=_("Lvl 6 Discount")
    )
    level6_promo = fields.Char(
        string=_("Lvl 6 Promo")
    )
    level7_price = fields.Char(
        string=_("Lvl 7 Price")
    )
    level7_discount = fields.Char(
        string=_("Lvl 7 Discount")
    )
    level7_promo = fields.Char(
        string=_("Lvl 7 Promo")
    )
    level8_price = fields.Char(
        string=_("Lvl 8 Price")
    )
    level8_discount = fields.Char(
        string=_("Lvl 8 Discount")
    )
    level8_promo = fields.Char(
        string=_("Lvl 8 Promo")
    )
    level9_price = fields.Char(
        string=_("Lvl 9 Price")
    )
    level9_discount = fields.Char(
        string=_("Lvl 9 Discount")
    )
    level9_promo = fields.Char(
        string=_("Lvl 9 Promo")
    )
    level10_price = fields.Char(
        string=_("Lvl 10 Price")
    )
    level10_discount = fields.Char(
        string=_("Lvl 10 Discount")
    )
    level10_promo = fields.Char(
        string=_("Lvl 10 Promo")
    )
    cancel_user = fields.Many2one(
        related="promotion_id.cancel_user"
    )
    cancel_date = fields.Datetime(
        related="promotion_id.cancel_date"
    )
    branch_off = fields.Char(
        string=_('Branch Office'),
    )
    segmnt = fields.Char(
        string=_('Segment'),
    )

    @api.onchange('branch_office')
    def _onchange_branch_office(self):
        branch_office_list = []
        if self.branch_office:
            for record in self.branch_office:
                branch_office_list.append(record.name)
            branch_office_list = [x.encode('UTF8') for x in branch_office_list]
            test = ', '.join(branch_office_list)
            branch_office_list = test
            self.branch_off = branch_office_list

    @api.onchange('segment')
    def _onchange_segment(self):
        segment_list = []
        if self.segment:
            for record in self.segment:
                segment_list.append(record.name)
            segment_list = [x.encode('UTF8') for x in segment_list]
            test = ', '.join(segment_list)
            segment_list = test
            self.segmnt = segment_list

    def calculate_discount(self, price, disc_percentage):
        disc = disc_percentage * price / 100
        disc_price = price - disc
        return disc, disc_price

    @api.onchange('product_id', 'level1_promo')
    def _onchange_product_id(self):
        if self.product_id.id:
            msg = """
                '%s'\n
                Discount price is lower than the average cost of the product\n
                Average cost: '%.2f %s'\n
                Discount: '%.2f%s', Level '%s'\n
                List price: '%.2f %s'\n
                Discount price: '%.2f %s',\n
                List price(converted): '%.2f %s'\n
                Discount price(converted): '%.2f %s'\n
            """
            title = 'Discount price is lower than average cost!!!'
            products = []
            promotions = []

            currency_object = self.env['res.currency']
            promotions_object = self.env['product.promotion']
            promotions = promotions_object.search([
                ('state', 'in', ['draft', 'active'])
            ])
            products_object = self.env['product.promotion.lines']
            products = products_object.search([
                    ('product_id', '=', self.product_id.id),
                    ('promotion_id', 'in', promotions.ids)
                ], limit=1).product_id.id

            company_object = self.env['res.company']
            currency_object = self.env['res.currency']
            currency_rate = currency_object.search([('name', '=', 'USD')])
            company_currency = company_object.search([('id', '=', '1')]).\
                currency_id.name

            percentage_symbol = '%'

            product_currency = self.product_id.list_price_currency.name
            average_cost = self.product_id.standard_price
            average_cost_currency = self.product_id.\
                average_cost_currency_ro.name
            list_price_currency = self.product_id.list_price_currency.name
            list_price = self.product_id.list_price
            """ promotion_name might be changed to "folio"
                once it gets implemented"""
            if self.product_id.id == products:
                promotion_id = self.env['product.promotion.lines']. \
                    search([
                        ('product_id', '=', self.product_id.id)
                    ]).promotion_id

                promotion_name = self.env['product.promotion']. \
                    search([('id', '=', promotion_id.id)])
                warning = {
                    'title': _('Product Warning!'),
                    'message': _("""
                        The following product '%s' is already set in the
                        promotion named '%s'

                    """ % (self.product_id.name, promotion_name.name)
                    )
                }
                self.product_id = False
                return {'warning': warning}

            if self.promotion_id.apply_levels is True:
                price_values = []
                calc_values = []
                promo_values = []
                values = []

                discount_lines = self.promotion_id.level_ids
                list_price = self.product_id.product_price_levels_nopop

                for x in range(0, 9):
                    discount_list_price_calc = list_price[x].price *\
                        discount_lines[x].discount / 100
                    discount_list_price = \
                        list_price[x].price - discount_list_price_calc
                    rounding = currency_rate.rate_sale_silent

                    if product_currency != company_currency:
                        if product_currency == 'USD':
                            conversion = list_price[x].price / rounding
                            calc = \
                                discount_lines[x].discount * conversion / 100
                            price_discount = conversion - calc
                            if price_discount < average_cost:
                                warning = {
                                    'title': _(title),
                                    'message': _(
                                        msg % (self.product_id.name,
                                               average_cost,
                                               average_cost_currency,
                                               discount_lines[x].discount,
                                               percentage_symbol,
                                               discount_lines[x].
                                               level_line_id.id,
                                               list_price[x].price,
                                               list_price_currency,
                                               discount_list_price,
                                               list_price_currency,
                                               conversion,
                                               average_cost_currency,
                                               price_discount,
                                               average_cost_currency))
                                }
                                self.product_id = False
                                return {'warning': warning}

                        elif product_currency == 'MXN':
                            conversion = list_price[x].price * rounding
                            calc = \
                                discount_lines[x].discount * conversion / 100
                            price_discount = conversion - calc
                            if price_discount < average_cost:
                                warning = {
                                    'title': _(title),
                                    'message': _(
                                        msg % (self.product_id.name,
                                               average_cost,
                                               average_cost_currency,
                                               discount_lines[x].discount,
                                               percentage_symbol,
                                               discount_lines[x].
                                               level_line_id.id,
                                               list_price[x].price,
                                               list_price_currency,
                                               discount_list_price,
                                               list_price_currency,
                                               conversion,
                                               average_cost_currency,
                                               price_discount,
                                               average_cost_currency))
                                }
                                self.product_id = False
                                return {'warning': warning}

                    else:
                        if discount_list_price < average_cost:
                            warning = {
                                'title': _(title),
                                'message': _("""
                                    '%s'\n
                                    Discount price is lower than the average
                                    cost of the product\n
                                    Average cost: '%.2f %s'\n
                                    Discount: '%.2f%s', Level: '%s'\n
                                    Price: '%.2f %s',\n
                                    Discount price: '%.2f %s'\n
                                    """ % (self.product_id.name,
                                           average_cost, average_cost_currency,
                                           discount_lines[x].discount,
                                           percentage_symbol,
                                           discount_lines[x].level_line_id.id,
                                           list_price[x].price,
                                           list_price_currency,
                                           discount_list_price,
                                           list_price_currency))
                            }
                            self.product_id = False
                            return {'warning': warning}

                level_ids = self.promotion_id.level_ids
                line_ids = self.promotion_id.line_ids
                apply_levels = self.promotion_id.apply_levels
                for record in level_ids:
                    for calc_record in record:
                        calc = calc_record.discount
                        calc_values.append(calc)
                for record in line_ids:
                    y = 0
                    x = list(record.product_id.product_price_levels_nopop)
                    for price_record in x:
                        price = price_record.price
                        price_values.append(price)
                        discount = calc_values[y] * price / 100
                        promo_price = price - discount
                        promo_values.append(promo_price)
                        y += 1
                    self.level1_price = price_values[0]
                    self.level2_price = price_values[1]
                    self.level3_price = price_values[2]
                    self.level4_price = price_values[3]
                    self.level5_price = price_values[4]
                    self.level6_price = price_values[5]
                    self.level7_price = price_values[6]
                    self.level8_price = price_values[7]
                    self.level9_price = price_values[8]
                    self.level10_price = price_values[9]
                    self.level1_discount = calc_values[0]
                    self.level2_discount = calc_values[1]
                    self.level3_discount = calc_values[2]
                    self.level4_discount = calc_values[3]
                    self.level5_discount = calc_values[4]
                    self.level6_discount = calc_values[5]
                    self.level7_discount = calc_values[6]
                    self.level8_discount = calc_values[7]
                    self.level9_discount = calc_values[8]
                    self.level10_discount = calc_values[9]
                    self.level1_promo = promo_values[0]
                    self.level2_promo = promo_values[1]
                    self.level3_promo = promo_values[2]
                    self.level4_promo = promo_values[3]
                    self.level5_promo = promo_values[4]
                    self.level6_promo = promo_values[5]
                    self.level7_promo = promo_values[6]
                    self.level8_promo = promo_values[7]
                    self.level9_promo = promo_values[8]
                    self.level10_promo = promo_values[9]
                    del price_values[:]
                    del promo_values[:]

            if self.promotion_id.apply_levels is False:
                self.disc_percentage = self.promotion_id.discount
                discount_list_price_calc = list_price *\
                    self.disc_percentage / 100
                discount_list_price = list_price - discount_list_price_calc
                rounding = currency_rate.rate_sale_silent

                if product_currency != company_currency:
                    if product_currency == 'USD':
                        conversion = list_price / rounding
                        calc = self.disc_percentage * conversion / 100
                        price_discount = conversion - calc
                        if price_discount < average_cost:
                            warning = {
                                'title': _(title),
                                'message': _("""
                                    '%s'\n
                                    Discount price is lower than the average
                                    cost of the product\n
                                    Average cost: '%.2f %s'\n
                                    Discount: '%.2f%s'\n
                                    List price: '%.2f %s'\n
                                    Discount price: '%.2f %s'\n
                                    List price(converted): '%.2f %s'\n
                                    Discount price(converted): '%.2f %s'\n
                                    """ % (self.product_id.name,
                                           average_cost,
                                           average_cost_currency,
                                           self.disc_percentage,
                                           percentage_symbol,
                                           list_price,
                                           list_price_currency,
                                           discount_list_price,
                                           list_price_currency,
                                           conversion,
                                           average_cost_currency,
                                           price_discount,
                                           average_cost_currency))
                            }
                            self.product_id = False
                            return {'warning': warning}

                    elif product_currency == 'MXN':
                        conversion = list_price * rounding
                        calc = self.disc_percentage * conversion / 100
                        price_discount = conversion - calc
                        if price_discount < average_cost:
                            warning = {
                                'title': _(title),
                                'message': _("""
                                    '%s'\n
                                    Discount price is lower than the average
                                    cost of the product\n
                                    Average cost: '%.2f %s'\n
                                    Discount: '%.2f%s'\n
                                    List price: '%.2f %s'\n
                                    Discount price: '%.2f %s'\n
                                    List price(converted): '%.2f %s'\n
                                    Discount price(converted): '%.2f %s'\n
                                    """ % (self.product_id.name,
                                           average_cost,
                                           average_cost_currency,
                                           self.disc_percentage,
                                           percentage_symbol,
                                           list_price, list_price_currency,
                                           discount_list_price,
                                           list_price_currency,
                                           conversion,
                                           average_cost_currency,
                                           price_discount,
                                           average_cost_currency))
                            }
                            self.product_id = False
                            return {'warning': warning}

                else:
                    if discount_list_price < average_cost:
                        warning = {
                            'title': _(title),
                            'message': _("""
                                '%s'\n
                                Discount price is lower than the average
                                cost of the product\n
                                Average cost: '%.2f %s'\n
                                Discount: '%.2f%s'\n
                                List price: '%.2f %s'\n
                                Discount price: '%.2f %s'\n
                                """ % (self.product_id.name,
                                       average_cost,
                                       average_cost_currency,
                                       self.disc_percentage,
                                       percentage_symbol,
                                       list_price,
                                       list_price_currency,
                                       discount_list_price,
                                       list_price_currency))
                        }
                        self.product_id = False
                        return {'warning': warning}

                self.product_code = self.product_id.default_code
                self.cur = self.product_id.list_price_currency.name
                self.cur_cr = self.product_id.reposition_cost_currency.name
                self.rep_cost = self.product_id.reposition_cost
                self.price = self.product_id.list_price
                self.disc_percentage = self.promotion_id.discount
                disc, disc_price = self.calculate_discount(
                    self.product_id.list_price, self.promotion_id.discount)
                self.disc = disc
                self.disc_price = disc_price

        else:
            return {}

    @api.onchange('discount')
    def _onchange_discount(self, vals):
        self.disc_percentage = self.promotion_id.discount

    @api.onchange('disc_percentage')
    def _onchange_disc_percentage(self):
        if self.disc_percentage > self.promotion_id.discount:
            self.disc_percentage = self.promotion_id.discount
        disc, disc_price = self.calculate_discount(
            self.product_id.list_price, self.disc_percentage)
        self.disc = disc
        self.disc_price = disc_price

    @api.model
    def create(self, vals):
        if vals.get('product_id'):
            get_product = vals.get('product_id')
            product = self.env['product.template'].browse(get_product)
            default_code = product.default_code
            price = product.list_price
            cur_cr_value = product.reposition_cost_currency.name
            rep_cost_value = product.reposition_cost
            cur_value = product.list_price_currency.name

            get_discount = vals.get('promotion_id')
            discount_value = self.env['product.promotion']. \
                browse(get_discount).discount
            disc, disc_price = self.calculate_discount(price, discount_value)

            promotion_id = vals.get('promotion_id')
            apply_levels = self.env['product.promotion']\
                .browse(promotion_id).apply_levels

            if not apply_levels:
                vals.update({
                    'product_code': default_code,
                    'cur_cr': cur_cr_value,
                    'rep_cost': rep_cost_value,
                    'cur': cur_value,
                    'disc_percentage': discount_value,
                    'price': price,
                    'disc': disc,
                    'disc_price': disc_price,
                })
            else:
                vals.update({
                    'product_code': default_code,
                    'cur_cr': cur_cr_value,
                    'rep_cost': rep_cost_value,
                    'cur': cur_value,
                    'disc_percentage': False,
                    'price': price,
                    'disc': False,
                    'disc_price': False,
                })
        return super(ProductPromotionLines, self).create(vals)

    @api.one
    def unlink(self):
        mail_message = self.env['mail.thread']
        self.promotion_id.message_post(
            body=_('Deleted product %s' % self.product_code),
            subject=_('Deleted'),
            type='notification',
            content_subtype='plaintext'
        )
        return super(ProductPromotionLines, self).unlink()

    @api.one
    def write(self, vals):
        if 'disc_percentage' in vals:
            disc, disc_price = self.calculate_discount(
                self.product_id.list_price, vals['disc_percentage']
            )
            vals.update({
                'disc': disc,
                'disc_price': disc_price
            })

        if vals.get('product_id'):
            get_product = vals.get('product_id')
            product = self.env['product.template'].browse(get_product)
            default_code = product.default_code
            price = product.list_price
            cur_cr_value = product.reposition_cost_currency.name
            rep_cost_value = product.reposition_cost
            cur_value = product.list_price_currency.name

            get_discount = self.promotion_id.id
            discount_value = self.env['product.promotion']. \
                browse(get_discount).discount
            disc, disc_price = self.calculate_discount(price, discount_value)

            promotion_id = vals.get('promotion_id')
            apply_levels = self.env['product.promotion'].\
                browse(promotion_id).apply_levels

            if not apply_levels:
                vals.update({
                    'product_code': default_code,
                    'cur_cr': cur_cr_value,
                    'rep_cost': rep_cost_value,
                    'cur': cur_value,
                    'disc_percentage': discount_value,
                    'price': price,
                    'disc': disc,
                    'disc_price': disc_price,
                })
            else:
                vals.update({
                    'product_code': default_code,
                    'cur_cr': cur_cr_value,
                    'rep_cost': rep_cost_value,
                    'cur': cur_value,
                    'disc_percentage': False,
                    'price': price,
                    'disc': False,
                    'disc_price': False,
                })
        return super(ProductPromotionLines, self).write(vals)
