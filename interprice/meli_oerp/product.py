# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields as F, api, _
from openerp.osv import fields, osv
from openerp.tools.translate import _

from datetime import datetime
from meli_oerp_config import *
from melisdk.meli import Meli
from suds.client import Client
import logging
import requests
import melisdk
import base64
import mimetypes
import urllib2
_logger = logging.getLogger(__name__)


class ProductTemplateV8(models.Model):
    _inherit = 'product.template'

    @api.one
    def _get_shipping(self):
        if self.meli_price >= 549:
            self.meli_flete = 'gratis'
        else:
            self.meli_flete = 'pagado'

    @api.one
    def _compute_quantity(self):
        conf = self.env['ir.config_parameter']
        if self.is_fulfillment:
            list_eval = conf.get_param('mercadolibre.full.locations')
        else:
            list_eval = conf.get_param('mercadolibre.locations')

        take_pch_stock = True
        if list_eval:
            ids = eval(list_eval)
            self.meli_available_quantity = 0
            for warehouse in ids:
                self._cr.execute("""SELECT SUM(sq.qty)
                                    FROM stock_warehouse AS sw INNER JOIN
                                    stock_location AS sl ON sw.lot_stock_id = sl.id
                                    INNER JOIN stock_quant sq ON sl.id=sq.location_id
                                    AND reservation_id is null
                                    WHERE sl.usage='internal' and sw.id=%s
                                    AND product_id=%s""", (int(warehouse), self.product_variant_ids.id))
                if self._cr.rowcount:
                    quants = self._cr.fetchone()[0]
                    if quants:
                        take_pch_stock = False
                        self.meli_available_quantity = quants
                        break
        if self.synchronize_with_pch:
            if take_pch_stock:
                self.meli_available_quantity = self.pch_quantity

    @api.one
    def _has_stock_in_full(self):
        conf = self.env['ir.config_parameter']
        list_eval = conf.get_param('mercadolibre.full.locations')
        self.is_fulfillment = False
        if list_eval:
            ids = eval(list_eval)
            for warehouse in ids:
                self._cr.execute("""SELECT SUM(sq.qty)
                                    FROM stock_warehouse AS sw INNER JOIN
                                    stock_location AS sl ON sw.lot_stock_id = sl.id
                                    INNER JOIN stock_quant sq ON sl.id=sq.location_id
                                    AND reservation_id is null
                                    WHERE sl.usage='internal' and sw.id=%s
                                    AND product_id=%s""", (int(warehouse), self.product_variant_ids.id))
                if self._cr.rowcount:
                    quants = self._cr.fetchone()[0]
                    if quants:
                        self.is_fulfillment = True
                        break

    @api.one
    def _get_active_warehouse(self):
        finded = False
        conf = self.env['ir.config_parameter']
        if self.is_fulfillment:
            list_eval = conf.get_param('mercadolibre.full.locations')
        else:
            list_eval = conf.get_param('mercadolibre.locations')
        if list_eval:
            ids = eval(list_eval)
            for warehouse in ids:
                self._cr.execute("""SELECT SUM(sq.qty)
                                    FROM stock_warehouse AS sw INNER JOIN
                                    stock_location AS sl ON sw.lot_stock_id = sl.id
                                    INNER JOIN stock_quant sq ON sl.id=sq.location_id
                                    AND reservation_id is null
                                    WHERE sl.usage='internal' and sw.id=%s
                                    AND product_id=%s""", (int(warehouse), self.product_variant_ids.id))
                if self._cr.rowcount:
                    quants = self._cr.fetchone()[0]
                    if quants:
                        self.meli_active_warehouse = warehouse
                        finded = True
                        break
            if not finded:
                self.meli_active_warehouse = ids[len(ids)-1]

    # @api.one
    # def _get_logistic(self):
    #     company = self.env['res.users'].browse(self.env.uid).company_id
    #     CLIENT_ID = company.mercadolibre_client_id
    #     CLIENT_SECRET = company.mercadolibre_secret_key
    #     ACCESS_TOKEN = company.mercadolibre_access_token
    #     REFRESH_TOKEN = company.mercadolibre_refresh_token
    #     meli = Meli(
    #         client_id=CLIENT_ID,
    #         client_secret=CLIENT_SECRET,
    #         access_token=ACCESS_TOKEN,
    #         refresh_token=REFRESH_TOKEN
    #     )
    #     if self.meli_id:
    #         response = meli.get('/items/{0}'.format(self.meli_id), {
    #             'access_token': meli.access_token
    #         }).json()
    #
    #         if 'shipping' in response:
    #             logistic_type = response['shipping']['logistic_type']
    #             self.is_fulfillment = True if logistic_type == 'fulfillment' else False

    is_fulfillment = F.Boolean(
        'Gestionado por Mercadolibre',
        compute='_has_stock_in_full'
    )

    meli_available_quantity = F.Integer(
        string='Cantidad disponible',
        compute='_compute_quantity',
        readonly=True
    )

    # pch_quantity = F.Integer(
    #     'Cantidad en PCH',
    #     readonly=True
    # )

    meli_active_warehouse = F.Many2one(
        'stock.warehouse',
        'Almacen activo',
        compute='_get_active_warehouse'
    )

    meli_flete = F.Selection(
        [('gratis', 'Gratis'), ('pagado', 'Pagado')],
        'Método de envio',
        compute='_get_shipping'
    )
    # synchronize_with_pch = F.Boolean('Sincronizar con PCH', default=False, track_visibility='onchange')
    #
    # last_update_stock = F.Datetime('Ultima actualización PCH')

    # def get_pch_stock(self, cr, uid, context={}):
    #     import time
    #     res_partner = self.pool.get('res.partner')
    #     pch_id = res_partner.search(
    #         cr,
    #         uid,
    #         [('supplier', '=', True), ('name', 'ilike', 'pch')]
    #     )
    #     ids = self.search(
    #         cr, uid, [('synchronize_with_pch', '=', True)]
    #     )
    #
    #     if pch_id:
    #         pch = res_partner.browse(cr, uid, pch_id[0])
    #         service = pch.api_service_type
    #         if 'soap' in service:
    #             url = pch.api_manage_docs_url
    #             if url:
    #                 try:
    #                     self.client = Client(url)
    #                 except Exception as e:
    #                     raise Warning(e)
    #             else:
    #                 raise Warning(""" The partner don´t have configured Url of
    #                               API service. """)
    #         else:
    #             raise Warning('The partner don´t have configured API service.')
    #
    #     for _id in ids:
    #         product = self.browse(cr, uid, _id)
    #         data = {
    #             'cliente': pch.api_user,
    #             'llave': pch.api_password,
    #             'sku': product.clave_fabricante
    #         }
    #         try:
    #             warehouse_list = pch.api_supplier_center_cost_line
    #             result = self.client.service.ObtenerArticulo(**data)
    #             encontrado = False
    #             if result.estatus:
    #                 for inventory in result.datos.inventario[::-1]:
    #                     for warehouse in warehouse_list:
    #                         if inventory['almacen'] == int(warehouse.identification_warehouse):
    #                             if inventory.existencia != 0:
    #                                 product.write({
    #                                     'pch_quantity': int(inventory.existencia * 30 / 100),
    #                                     'last_update_stock': time.strftime('%Y-%m-%d %H:%M:%S')
    #                                 })
    #                             encontrado = True
    #                             break
    #                     if encontrado:
    #                         break
    #
    #                 if not encontrado:
    #                     product.write({
    #                         'pch_quantity': 0,
    #                         'last_update_stock': time.strftime('%Y-%m-%d %H:%M:%S')
    #                     })
    #
    #         except Exception as e:
    #             raise Warning(e)


class product_template(osv.osv):
    _inherit = "product.template"


    def product_get_meli_loginstate(self, cr, uid, ids, field_name, attributes, context=None):
        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN)

        ML_state = False
        if ACCESS_TOKEN == '':
            ML_state = True
        else:
            response = meli.get("/users/me/", {'access_token':meli.access_token} )
            rjson = response.json()
            if 'error' in rjson:
                if rjson['message']=='invalid_token' or rjson['message']=='expired_token':
                    ACCESS_TOKEN = ''
                    REFRESH_TOKEN = ''
                    company.write({'mercadolibre_access_token': ACCESS_TOKEN, 'mercadolibre_refresh_token': REFRESH_TOKEN, 'mercadolibre_code': '' } )
                    ML_state = True
                    #raise osv.except_osv( _('MELI WARNING'), _('INVALID TOKEN (must login, go to Edit Company and login):  error: %s, message: %s, status: %s') % ( rjson["error"], rjson["message"],rjson["status"],))

        res = {}
        for product in self.browse(cr,uid,ids):
            res[product.id] = ML_state
        return res

    def product_get_permalink(self, cr, uid, ids, field_name, attributes, context=None):
        ML_permalink = ''

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id

        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        ML_permalink = ""
        if ACCESS_TOKEN == '':
            ML_permalink = ""
        else:
            meli = Meli(client_id=CLIENT_ID,client_secret=CLIENT_SECRET, access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN)
            if product.meli_id:
                response = meli.get("/items/"+product.meli_id, {'access_token': meli.access_token})
                rjson = response.json()
                if "permalink" in rjson:
                    ML_permalink = rjson["permalink"]
                if "error" in rjson:
                    ML_permalink = ""

        res = {}
        for product in self.browse(cr, uid, ids):
            res[product.id] = ML_permalink
        return res

    def product_get_meli_status(self, cr, uid, ids, field_name, attributes, context=None ):

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        warningobj = self.pool.get('warning')

        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        ML_status = "unknown"
        if ACCESS_TOKEN == '':
            ML_status = "unknown"
        else:
            meli = Meli(client_id=CLIENT_ID,client_secret=CLIENT_SECRET, access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN)
            if product.meli_id:
                response = meli.get("/items/"+product.meli_id, {'access_token': meli.access_token})
                rjson = response.json()
                ML_status = rjson["status"]
                if "error" in rjson:
                    ML_status = rjson["error"]
                if "sub_status" in rjson:
                    if len(rjson["sub_status"]) and rjson["sub_status"][0] == 'deleted':
                        pass
                        # product.write({'meli_id': ''})

        res = {}
        for product in self.browse(cr, uid, ids):
            res[product.id] = ML_status
        return res

    _columns = {
        'name': fields.char('Name', size=128, required=True, translate=False, select=True, track_visibility='onchange'),
        'meli_imagen_id': fields.char(string='Imagen Id', size=256, track_visibility='onchange'),
        'meli_post_required': fields.boolean(string='Este producto es publicable en Mercado Libre', track_visibility='onchange'),
        'meli_id': fields.char(string='Id del item asignado por Meli', size=256, track_visibility='onchange'),
        'meli_permalink': fields.function(product_get_permalink, method=True, type='char',  size=256, string='PermaLink in MercadoLibre', track_visibility='onchange'),
        'meli_title': fields.char(string='Nombre del producto en Mercado Libre', size=60, track_visibility='onchange'),
        'meli_description': fields.text(string='Descripción', track_visibility='onchange'),
        'meli_description_banner_id': fields.many2one("mercadolibre.banner", "Banner", track_visibility='onchange'),
        'meli_category': fields.many2one("mercadolibre.category", "Categoría de MercadoLibre", track_visibility='onchange'),
        'meli_listing_type': fields.selection([("free", "Gratis"), ("gold_special", "Clásica"), ("gold_pro", "Premium")], string='Tipo de lista', track_visibility='onchange'),
        'meli_buying_mode': fields.selection([("buy_it_now", "Compre ahora"), ("classified", "Clasificado")], string='Método de compra'),
        'meli_price': fields.float(string='Precio de venta', digits=(8, 2), track_visibility='onchange'),
        'meli_price_fixed': fields.boolean(string='Price is fixed', track_visibility='onchange'),
        'meli_currency': fields.selection([("MXN", "Peso Mexicano (MXN)")],string='Moneda', track_visibility='onchange'),
        'meli_condition': fields.selection([("new", "Nuevo"), ("used", "Usado"), ("not_specified", "No especificado")], 'Condición del producto', track_visibility='onchange'),
        'meli_warranty': fields.char(string='Garantía', size=256, track_visibility='onchange'),
        'meli_imagen_logo': fields.char(string='Imagen Logo', size=256, track_visibility='onchange'),
        'meli_imagen_id': fields.char(string='Imagen Id', size=256, track_visibility='onchange'),
        'meli_imagen_link': fields.char(string='Imagen Link', size=256, track_visibility='onchange'),
        'meli_multi_imagen_id': fields.char(string='Multi Imagen Ids', size=512, track_visibility='onchange'),
        'meli_video': fields.char(string='Video (id de youtube)', size=256, track_visibility='onchange'),
        'meli_state': fields.function(product_get_meli_loginstate, method=True, type='boolean', string="Inicio de sesión requerida", store=False),
        'meli_status': fields.function(product_get_meli_status, method=True, type='char', size=128, string="Estado del producto en MLM", store=False),
    }

    _defaults = {
        'meli_imagen_logo': 'None',
        'meli_video': ''
    }


class product_product(osv.osv):

    _inherit = "product.template"

    @api.one
    @api.onchange('lst_price')
    def check_change_price(self):
        pricelists = self.env['product.pricelist'].search([])
        pricelist = pricelists[0]

        return {}

    @api.onchange('meli_post_required')
    def onchange_meli_post_required(self):
        if self.meli_post_required:
            self.meli_description = self.name
            self.meli_title = self.name
        else:
            self.meli_description = ''
            self.meli_title = ''

    def product_meli_get_products(self, cr, uid, context=None):
        content_file = 'Nombre, SKU, fecha de creación\n'
        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        product_obj = self.pool.get('product.product')
        category_obj = self.pool.get('mercadolibre.category')
        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token
        meli = Meli(client_id=CLIENT_ID, client_secret=CLIENT_SECRET, access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN)

        url_login_meli = meli.auth_url(redirect_URI=REDIRECT_URI)

        results = []
        response = meli.get("/users/"+company.mercadolibre_seller_id+"/items/search", {'access_token':meli.access_token,'limit': 100 })
        rjson = response.json()
        results = rjson['results']

        if 'error' in rjson:
            if rjson['message'] == 'invalid_token' or rjson['message'] == 'expired_token':
                ACCESS_TOKEN = ''
                REFRESH_TOKEN = ''
                company.write({'mercadolibre_access_token': ACCESS_TOKEN, 'mercadolibre_refresh_token': REFRESH_TOKEN, 'mercadolibre_code': '' } )
            return {
                "type": "ir.actions.act_url",
                "url": url_login_meli,
                "target": "new"
            }

        #download?
        if (rjson['paging']['total'] > rjson['paging']['limit']):
            pages = rjson['paging']['total']/rjson['paging']['limit']
            ioff = rjson['paging']['limit']
            condition_last_off = False
            response = meli.get("/users/"+company.mercadolibre_seller_id+"/items/search", {'access_token': meli.access_token, 'limit': 100, 'search_type': 'scan'})
            while (condition_last_off != True):
                rjson2 = response.json()
                scroll_id = rjson2['scroll_id']

                if 'error' in rjson2:
                    if rjson2['message'] == 'invalid_token' or rjson2['message'] == 'expired_token':
                        ACCESS_TOKEN = ''
                        REFRESH_TOKEN = ''
                        company.write({'mercadolibre_access_token': ACCESS_TOKEN, 'mercadolibre_refresh_token': REFRESH_TOKEN, 'mercadolibre_code': ''})
                    condition = True
                else:
                    results += rjson2['results']
                    ioff += rjson['paging']['limit']

                    condition_last_off = (ioff >= rjson['paging']['total'])
                response = meli.get(
                    "/users/"+company.mercadolibre_seller_id+"/items/search",
                    {
                        'access_token': meli.access_token,
                        'limit': 100,
                        'search_type': 'scan',
                        'scroll_id': scroll_id
                    }
                )

            rjson2 = response.json()
            if not 'error' in rjson2:
                results += rjson2['results']

        iitem = 0
        if (results):
            for item_id in results:
                iitem += 1
                response = meli.get("/items/"+item_id, {'access_token': meli.access_token})
                rjson3 = response.json()
                domain = [('meli_id', '=', item_id)]
                if 'seller_custom_field' in rjson3:
                    domain.append(('default_code', '=', rjson3['seller_custom_field']))
                    domain.insert(0, '|')

                posting_id = product_obj.search(
                    cr, uid, domain
                )

                if (posting_id):
                    producto = product_obj.browse(cr, uid, posting_id[0])
                    category_id = rjson3['category_id']
                    ml_cat_id = category_obj.search(cr, uid, [('meli_category_id', '=', category_id)])
                    if (ml_cat_id):
                        mlcatid = ml_cat_id[0]
                    else:
                        response_cat = meli.get("/categories/"+str(category_id), {'access_token': meli.access_token})
                        rjson_cat = response_cat.json()
                        fullname = ""
                        if ("path_from_root" in rjson_cat):
                            path_from_root = rjson_cat["path_from_root"]
                            for path in path_from_root:
                                fullname = fullname + "/" + path["name"]

                        cat_fields = {
                            'name': fullname,
                            'meli_category_id': ''+str(category_id),
                        }
                        ml_cat_id = self.pool.get('mercadolibre.category').create(cr, uid, (cat_fields))
                        if (ml_cat_id):
                            mlcatid = ml_cat_id
                    producto.write({
                        'meli_id': rjson3['id'],
                        'meli_post_required': True,
                        'meli_currency': rjson3['currency_id'],
                        'meli_category': mlcatid,
                        'meli_listing_type': rjson3['listing_type_id'],
                        'meli_buying_mode': rjson3['buying_mode'],
                        'meli_price': rjson3['price'],
                        'meli_condition': rjson3['condition'],
                        'meli_warranty': rjson3['warranty'],
                        'meli_imagen_link': rjson3['thumbnail'],
                    })
                else:
                    content_file += '%s,%s,%s\n' % (rjson3['title'].replace(',',''), rjson3['seller_custom_field'], rjson3['date_created'])
                    # product_uom_obj = self.pool.get('product.uom')
                    # ids_uom = product_uom_obj.search(
                    #     cr, uid,
                    #     [('name', 'like', 'Pieza')],
                    #     context={'lang': 'es_MX'}
                    # )
                    # idcreated = self.pool.get('product.product').create(
                    #     cr, uid,
                    #     {
                    #         'name': rjson3['title'],
                    #         'meli_id': rjson3['id'],
                    #         'uom_id': ids_uom[0],
                    #         'uom_po_id': ids_uom[0]
                    #     }
                    # )
                    # if (idcreated):
                    #     product = product_obj.browse(cr, uid, idcreated)
                    #     self.product_meli_get_product(
                    #         cr, uid, [idcreated],
                    #         context={'from_product_product': True}
                    #     )
        return {}

    def product_meli_get_product(self, cr, uid, ids, context=None):
        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        if context.get('from_product_product', None):
            product_obj = self.pool.get('product.product')
            product = product_obj.browse(cr, uid, ids[0])
            product_template_obj = self.pool.get('product.template')
            product_template = product_template_obj.browse(
                cr, uid, product.product_tmpl_id.id
            )
        else:
            product_template_obj = self.pool.get('product.template')
            product = product_template_obj.browse(cr, uid, ids[0])
            product_template = product

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        response = meli.get("/items/"+product.meli_id, {'access_token': meli.access_token})

        print "product_meli_get_product: " + response.content
        rjson = response.json()

        des = ''
        desplain = ''
        vid = ''
        if 'error' in rjson:
            return {}

        if rjson['descriptions']:
            response2 = meli.get("/items/"+product.meli_id+"/description", {'access_token':meli.access_token})
            rjson2 = response2.json()
            des = rjson2['text']
            desplain = rjson2['plain_text']
            if (len(des) > 0):
                desplain = des

        if rjson['video_id']:
            vid = ''

        pictures = rjson['pictures']
        if pictures and len(pictures):
            thumbnail_url = pictures[0]['url']
            print('URL %s' % thumbnail_url)
            image = urllib2.urlopen(thumbnail_url).read()
            image_base64 = base64.encodestring(image)
            product.image_medium = image_base64

        #categories
        mlcatid = ""
        if ('category_id' in rjson):
            category_id = rjson['category_id']
            ml_cat_id = self.pool.get('mercadolibre.category').search(cr, uid, [('meli_category_id', '=', category_id)])
            if (ml_cat_id):
                mlcatid = ml_cat_id[0]
            else:
                print "Creating category: " + str(category_id)
                response_cat = meli.get("/categories/"+str(category_id), {'access_token': meli.access_token})
                rjson_cat = response_cat.json()
                fullname = ""
                if ("path_from_root" in rjson_cat):
                    path_from_root = rjson_cat["path_from_root"]
                    for path in path_from_root:
                        fullname = fullname + "/" + path["name"]

                cat_fields = {
                    'name': fullname,
                    'meli_category_id': ''+str(category_id),
                }
                ml_cat_id = self.pool.get('mercadolibre.category').create(cr, uid, (cat_fields))
                if (ml_cat_id):
                    mlcatid = ml_cat_id

        imagen_id = ''
        if (len(rjson['pictures']) > 0):
            imagen_id = rjson['pictures'][0]['id']

        meli_fields = {
            'name': str(rjson['title']),
            'meli_imagen_id': imagen_id,
            'meli_post_required': True,
            'meli_id': rjson['id'],
            'meli_permalink': rjson['permalink'],
            'meli_title': rjson['title'],
            'meli_description': desplain,
            'meli_category': mlcatid,
            'meli_listing_type': rjson['listing_type_id'],
            'meli_buying_mode': rjson['buying_mode'],
            'meli_price': str(rjson['price']),
            'meli_price_fixed': True,
            'meli_currency': rjson['currency_id'],
            'meli_condition': rjson['condition'],
            # 'meli_available_quantity': rjson['available_quantity'],
            'meli_warranty': rjson['warranty'],
            'meli_imagen_link': rjson['thumbnail'],
            'meli_video': str(vid)
        }

        tmpl_fields = {
          'name': str(rjson['title']),
        }

        product.write(meli_fields)
        product_template.write(tmpl_fields)
        product_template._compute_product_price_levels_nopop()
        return {}

    def product_meli_login(self, cr, uid, ids, context=None):

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id

        REDIRECT_URI = company.mercadolibre_redirect_uri
        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        meli = Meli(client_id=CLIENT_ID, client_secret=CLIENT_SECRET)

        url_login_meli = meli.auth_url(redirect_URI=REDIRECT_URI)

        return {
            "type": "ir.actions.act_url",
            "url": url_login_meli,
            "target": "new",
        }

    def product_meli_status_close(self, cr, uid, ids, context=None):
        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        response = meli.put(
            "/items/"+product.meli_id,
            {'status': 'closed'},
            {'access_token': meli.access_token}
        )

        return {}

    def post_product(self, cr, uid, ids, context={}):
        company = self.pool.get('res.users').browse(cr, uid, uid).company_id
        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token
        product_obj = self.pool.get('product.template')
        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )
        for product_id in ids:
            product = product_obj.browse(cr, uid, product_id)
            if not product.meli_title:
                product.meli_title = product.name
            if not product.meli_price:
                product.meli_price = product.standard_price

            body = {
                "title": product.meli_title or '',
                "description": {
                    'plain_text': product.meli_description or '',
                },
                "category_id": product.meli_category.meli_category_id,
                "listing_type_id": product.meli_listing_type,
                "buying_mode": product.meli_buying_mode or '',
                "price": "{:.2f}".format(product.meli_price * (1 + product.taxes_id.amount)),
                "currency_id": product.meli_currency,
                'seller_custom_field': product.default_code or '',
                "condition": product.meli_condition or '',
                "available_quantity": product.meli_available_quantity or '0',
                "warranty": product.meli_warranty or '',
                "shipping": {
                    "mode": "me2",
                    "local_pick_up": False,
                    "free_shipping": True,
                    "free_methods": [
                        {
                            "id": 501245,
                            "rule": {
                                "free_mode": "country",
                                "value": None
                            }
                        }
                    ],
                    "dimensions": None,
                },
                "attributes": [
                    {
                        "id": "BRAND",
                        "name": "Marca",
                        "value_name": product.brand.name
                    }
                ]
            }

            if not product.meli_imagen_id:
                resim = product.product_meli_upload_image()
                if "status" in resim:
                    if (resim["status"] == "error" or resim["status"] == "warning"):
                        error_msg = 'MELI: mensaje de error:   ', resim
                        _logger.error(error_msg)

            multi_images_ids = {}
            if product.meli_imagen_id:
                body['pictures'] = [{'id': product.meli_imagen_id}]

            response = meli.post(
                "/items", body, {'access_token': meli.access_token}
            )

            rjson = response.json()

            if "error" in rjson:
                error_msg = 'MELI: mensaje de error:  %s\n, mensaje: %s\n, status: %s\n, cause: %s' % (rjson["error"], rjson["message"], rjson["status"], rjson["cause"])
                _logger.error(error_msg)
                missing_fields = error_msg

            if "id" in rjson:
                product.write({
                    'meli_id': rjson["id"]
                })

            posting_fields = {
                'posting_date': str(datetime.now()),
                'meli_id': rjson['id'],
                'product_id': product.id,
                'name': 'Post: ' + product.meli_title
            }
            posting_id = self.pool.get('mercadolibre.posting').search(
                cr, uid, [('meli_id', '=', rjson['id'])]
            )
            if not posting_id:
                posting_id = self.pool.get('mercadolibre.posting').create(
                    cr, uid, (posting_fields)
                )

    def product_meli_update(self, cr, uid, context={}):
        company = self.pool.get('res.users').browse(cr, uid, uid).company_id
        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )
        ids_to_publish = self.search(
            cr,
            uid,
            [('meli_id', '=', False), ('meli_post_required', '=', True)]
        )

        self.post_product(cr, uid, ids_to_publish, context)

        ids = self.search(cr, uid, [('meli_id', '!=', False)])
        for _id in ids:
            product = self.browse(cr, uid, _id)
            if not product.is_fulfillment:
                body = {
                    'title': product.meli_title,
                    'available_quantity': product.meli_available_quantity,
                    'price': "{:.2f}".format(product.meli_price * (1 + product.taxes_id.amount)),
                }
                response = meli.put(
                    '/items/' + product.meli_id,
                    body,
                    {'access_token': meli.access_token}
                )

    def product_meli_status_pause(self, cr, uid, ids, context=None):
        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        response = meli.put(
            "/items/"+product.meli_id,
            {'status': 'paused'}, {'access_token': meli.access_token}
        )

        return {}

    def product_meli_status_active(self, cr, uid, ids, context=None):
        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        response = meli.put(
            "/items/"+product.meli_id,
            {'status': 'active'},
            {'access_token': meli.access_token}
        )
        return {}

    def product_meli_delete(self, cr, uid, ids, context=None):

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        if product.meli_status != 'closed':
            self.product_meli_status_close(cr, uid, ids, context)

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        response = meli.put(
            "/items/"+product.meli_id,
            {'deleted': 'true'},
            {'access_token': meli.access_token}
        )

        rjson = response.json()
        ML_status = rjson["status"]
        if "error" in rjson:
            ML_status = rjson["error"]
        if "sub_status" in rjson:
            if len(rjson["sub_status"]) and rjson["sub_status"][0] == 'deleted':
                # product.write({'meli_id': ''})
                pass

        return {}

    def product_meli_upload_image(self, cr, uid, ids, context=None):

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id

        product_obj = self.pool.get('product.template')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        #
        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        if product.image == None or product.image == False:
            return {'status': 'error', 'message': 'no image to upload'}

        imagebin = base64.b64decode(product.image)
        imageb64 = product.image
        import StringIO
        from PIL import Image
        image_stream = StringIO.StringIO(imagebin)
        image = Image.open(image_stream)
        filetype = image.format
        files = {'file': ('image.{0}'.format(filetype), imagebin, "image/{0}".format(filetype.lower()))}
        response = meli.upload(
            "/pictures", files, {'access_token': meli.access_token}
        )

        rjson = response.json()
        if ("error" in rjson):
            raise osv.except_osv(_('MELI WARNING'), _('No se pudo cargar la imagen en MELI! Error: %s , Mensaje: %s, Status: %s') % ( rjson["error"], rjson["message"],rjson["status"],))
            return {'status': 'error', 'message': 'not uploaded'}

        if ("id" in rjson):
            # guardar id
            product.write({
                "meli_imagen_id": rjson["id"],
                "meli_imagen_link": rjson["variations"][0]["url"]}
            )
            # asociar imagen a producto
            if product.meli_id:
                response = meli.post(
                    "/items/"+product.meli_id+"/pictures",
                    {'id': rjson["id"]},
                    {'access_token': meli.access_token}
                )
            else:
                return {
                    'status': 'warning',
                    'message': 'uploaded but not assigned'
                }

        return {'status': 'success', 'message': 'uploaded and assigned'}

    def product_meli_upload_multi_images(self, cr, uid, ids, context=None):

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id

        product_obj = self.pool.get('product.product')
        product = product_obj.browse(cr, uid, ids[0])

        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        #
        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        if product.images == None:
            return {'status': 'error', 'message': 'no images to upload'}

        image_ids = []
        c = 0

        #loop over images
        for product_image in product.images:
            if (product_image.image):
                print "product_image.image:" + str(product_image.image)
                imagebin = base64.b64decode(product_image.image)
                files = {'file': ('image.jpg', imagebin, "image/jpeg")}
                response = meli.upload(
                    "/pictures", files, {'access_token': meli.access_token}
                )
                print "meli upload:" + response.content
                rjson = response.json()
                if ("error" in rjson):
                    raise osv.except_osv( _('MELI WARNING'), _('No se pudo cargar la imagen en MELI! Error: %s , Mensaje: %s, Status: %s') % ( rjson["error"], rjson["message"],rjson["status"],))
                else:
                    image_ids += [{'id': rjson['id']}]
                    c = c + 1
                    print "image_ids:" + str(image_ids)

        product.write({"meli_multi_imagen_id": "%s" % (image_ids)})

        return image_ids

    def product_on_change_meli_banner(self, cr, uid, ids, banner_id):

        banner_obj = self.pool.get('mercadolibre.banner')

        # solo para saber si ya habia una descripcion completada
        product_obj = self.pool.get('product.product')
        if len(ids):
            product = product_obj.browse(cr, uid, ids[0])
        else:
            product = product_obj.browse(cr, uid, ids)

        banner = banner_obj.browse(cr, uid, banner_id)

        # banner.description
        _logger.info(banner.description)
        result = ""
        if banner:
            if (banner.description != "" and not banner.description and product.meli_imagen_link != ""):
                imgtag = "<img style='width: 420px; height: auto;' src='%s'/>" % ( product.meli_imagen_link )
                result = banner.description.replace("[IMAGEN_PRODUCTO]", imgtag)
                if (result):
                    _logger.info("result: %s" % (result))
                else:
                    result = banner.description

        return {'value': {'meli_description': result}}

    def product_post(self, cr, uid, ids, context=None):
        product_ids = ids
        product_obj = self.pool.get('product.template')

        user_obj = self.pool.get('res.users').browse(cr, uid, uid)
        company = user_obj.company_id
        warningobj = self.pool.get('warning')

        REDIRECT_URI = company.mercadolibre_redirect_uri
        CLIENT_ID = company.mercadolibre_client_id
        CLIENT_SECRET = company.mercadolibre_secret_key
        ACCESS_TOKEN = company.mercadolibre_access_token
        REFRESH_TOKEN = company.mercadolibre_refresh_token

        meli = Meli(
            client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
            access_token=ACCESS_TOKEN, refresh_token=REFRESH_TOKEN
        )

        if ACCESS_TOKEN == '':
            meli = Meli(client_id=CLIENT_ID, client_secret=CLIENT_SECRET)
            url_login_meli = meli.auth_url(redirect_URI=REDIRECT_URI)
            return {
                "type": "ir.actions.act_url",
                "url": url_login_meli,
                "target": "new",
            }

        for product_id in product_ids:
            product = product_obj.browse(cr, uid, product_id)

            if (product.meli_id):
                response = meli.get(
                    "/items/%s" % product.meli_id,
                    {'access_token': meli.access_token}
                )

            if not product.meli_title:
                product.meli_title = product.name

            if not product.meli_price:
                product.meli_price = product.standard_price

            body = {
                "title": product.meli_title or '',
                "description": product.meli_description or '',
                "category_id": product.meli_category.meli_category_id or '0',
                "listing_type_id": product.meli_listing_type or '0',
                "buying_mode": product.meli_buying_mode or '',
                "price": product.meli_price or '0',
                "currency_id": product.meli_currency or '0',
                "condition": product.meli_condition or '',
                "available_quantity": product.meli_available_quantity or '0',
                "warranty": product.meli_warranty or '',
                # "pictures": [ { 'source': product.meli_imagen_logo} ] ,
                "video_id": product.meli_video or '',
            }

            assign_img = False and product.meli_id

            # publicando imagen cargada en OpenERP
            if product.image==None:
                return warningobj.info(cr, uid, title='MELI WARNING', message="Debe cargar una imagen de base en el producto.", message_html="" )
            elif product.meli_imagen_id==False:
                resim = product.product_meli_upload_image()
                if "status" in resim:
                    if (resim["status"]=="error" or resim["status"]=="warning"):
                        error_msg = 'MELI: mensaje de error:   ', resim
                        _logger.error(error_msg)
                    else:
                        assign_img = True and product.meli_imagen_id

            # modificando datos si ya existe el producto en MLA
            if (product.meli_id):
                body = {
                    "title": product.meli_title or '',
                    # "description": product.meli_description or '',
                    # "category_id": product.meli_category.meli_category_id,
                    # "listing_type_id": product.meli_listing_type,
                    "buying_mode": product.meli_buying_mode or '',
                    "price": product.meli_price or '0',
                    # "currency_id": product.meli_currency,
                    "condition": product.meli_condition or '',
                    "available_quantity": product.meli_available_quantity or '0',
                    "warranty": product.meli_warranty or '',
                    "pictures": [],
                    # "pictures": [ { 'source': product.meli_imagen_logo} ] ,
                    "video_id": product.meli_video or '',
                }

            # publicando multiples imagenes
            multi_images_ids = {}
            if (product.images):
                # print 'website_multi_images presente:   ', product.images
                # recorrer las imagenes y publicarlas
                multi_images_ids = product.product_meli_upload_multi_images()

            # asignando imagen de logo (por source)
            if product.meli_imagen_logo:
                if product.meli_imagen_id:
                    if 'pictures' in body.keys():
                        body["pictures"] += [{'id': product.meli_imagen_id}]
                    else:
                        body["pictures"] = [{'id': product.meli_imagen_id}]

                        if (multi_images_ids):
                            if 'pictures' in body.keys():
                                body["pictures"] += multi_images_ids
                            else:
                                body["pictures"] = multi_images_ids

                    if 'pictures' in body.keys():
                        body["pictures"] += [{'source': product.meli_imagen_logo}]
                    else:
                        body["pictures"] += [{'source': product.meli_imagen_logo}]
                else:
                    imagen_producto = ""
                    if (product.meli_description != "" and not product.meli_description and product.meli_imagen_link != ""):
                        imgtag = "<img style='width: 420px; height: auto;' src='%s'/>" % (product.meli_imagen_link)
                        result = product.meli_description.replace("[IMAGEN_PRODUCTO]", imgtag)
                        if (result):
                            product.meli_description = result
                        else:
                            result = product.meli_description
            else:
                return warningobj.info(cr, uid, title='MELI WARNING', message="Debe completar el campo 'Imagen_Logo' con el url: http://www.nuevohorizonte-sa.com.ar/images/logo1.png", message_html="")

            # check fields
            if not product.meli_description:
                return warningobj.info(cr, uid, title='MELI WARNING', message="Debe completar el campo 'description' (en html)", message_html="")

            # put for editing, post for creating
            if product.meli_id:
                response = meli.put(
                    "/items/"+product.meli_id, body,
                    {'access_token': meli.access_token}
                )
            else:
                assign_img = True and product.meli_imagen_id
                response = meli.post(
                    "/items", body,
                    {'access_token': meli.access_token}
                )

            # check response
            rjson = response.json()

            # check error
            if "error" in rjson:
                error_msg = 'MELI: mensaje de error:  %s , mensaje: %s, status: %s, cause: %s ' % (rjson["error"], rjson["message"], rjson["status"], rjson["cause"])
                _logger.error(error_msg)

                missing_fields = error_msg

                # expired token
                if "message" in rjson and (rjson["message"] == 'invalid_token' or rjson["message"] == "expired_token"):
                    meli = Meli(client_id=CLIENT_ID, client_secret=CLIENT_SECRET)
                    url_login_meli = meli.auth_url(redirect_URI=REDIRECT_URI)
                    # raise osv.except_osv( _('MELI WARNING'), _('INVALID TOKEN or EXPIRED TOKEN (must login, go to Edit Company and login):  error: %s, message: %s, status: %s') % ( rjson["error"], rjson["message"],rjson["status"],))
                    return warningobj.info(cr, uid, title='MELI WARNING', message="Debe iniciar sesión en MELI.  ", message_html="")
                else:
                    # Any other errors
                    return warningobj.info(cr, uid, title='MELI WARNING', message="Completar todos los campos!  ", message_html="<br><br>"+missing_fields)

            # last modifications if response is OK
            if "id" in rjson:
                product.write({'meli_id': rjson["id"]})

            posting_fields = {'posting_date': str(datetime.now()), 'meli_id': rjson['id'], 'product_id': product.id, 'name': 'Post: ' + product.meli_title}

            posting_id = self.pool.get('mercadolibre.posting').search(
                cr, uid, [('meli_id', '=', rjson['id'])])

            if not posting_id:
                posting_id = self.pool.get('mercadolibre.posting').create(
                    cr, uid, (posting_fields))

        return {}
