# -*- coding: utf-8 -*-
##############################################################################
#
#       Pere Ramon Erro Mas <pereerro@tecnoba.com> All Rights Reserved.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'MercadoLibre Publisher',
    'version': '0.1',
    'author': 'Moldeo Interactive',
    'website': 'http://business.moldeo.coop',
    "category": "Sales",
    "depends": ['base', 'product', 'sale', 'stock', 'price_list', 'pch_ws'],
    'data': [
        'company_view.xml',
        'posting_view.xml',
        'product_post.xml',
        'product_view.xml',
        'category_view.xml',
        'banner_view.xml',
        'warning_view.xml',
        'questions_view.xml',
        'orders_error_log_view.xml',
        'orders_view.xml',
        'ir_cron.xml',
        'product_settings_view.xml',
        'ir.model.access.csv',
    ],
    'demo_xml': [],
    'active': False,
    'installable': True,
    'application': True,
}
