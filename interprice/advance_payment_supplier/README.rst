=====
Advance payment supplier
=====


Security
--------

.. figure:: ../advance_payment_supplier/static/img/security.png)
    :width: 100%

.. figure:: ../advance_payment_supplier/static/img/security02.png
    :width: 100%

Account move
--------

.. figure:: ../advance_payment_supplier/static/img/account_move.png
    :width: 100%

Journal
--------

- Check

.. figure:: ../advance_payment_supplier/static/img/journal-check.png
    :width: 100%
- Transfer

.. figure:: ../advance_payment_supplier/static/img/journal-transfer.png
    :width: 100%

Advance payment supplier
--------

- Transfer

.. figure:: ../advance_payment_supplier/static/img/transfer.png
    :width: 100%
- Check

.. figure:: ../advance_payment_supplier/static/img/check.png
    :width: 100%


License
--------

`GNU Lesser General Public License <https://www.gnu.org/licenses/lgpl.html/>`_.


Author
--------

`Yonn, Xyz <https://yonn.xyz/>`_.