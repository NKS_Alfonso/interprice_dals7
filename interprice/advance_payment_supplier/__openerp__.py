# -*- coding: utf-8 -*-
# # © Yonn, Xyz. All rights reserved.
{
    'name': "Advance payment supplier",

    'summary': """
        Management advance payment with supplier,
        check and transfer ... 
        """,

    'author': "Yonn, Xyz",
    'website': "https://yonn.xyz",
    # for the full list
    'category': 'accounting',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'account',
        'account_check'
    ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'views/advance_payment_supplier_view.xml',
        'views/account_check_view.xml',
        'views/advance_payment_supplier_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
}

