# -*- coding: utf-8 -*-
# © Yonn, Xyz. All rights reserved.

from openerp import models, fields, api, _, exceptions

PAYMENT_FORM = [('check', _("Check")), ('bank', _("Transfer"))]
STATE = [('draft', _("Draft")), ('generated', _("Generated")),
         ('applied', _("Applied")), ('cancelled', _("Cancelled"))]


def get_payment_form(**kwargs):
    for pf in PAYMENT_FORM:
        if kwargs.get('key', False):
            if pf[0] == kwargs.get('key'):
                return pf[1]


def get_state(**kwargs):
    for st in STATE:
        if kwargs.get('key', False):
            if st[0] == kwargs.get('key'):
                return st[1]


class AdvancePaymentSupplier(models.Model):
    _name = 'advance.payment.supplier'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'display_name'
    state = fields.Selection(selection=STATE, string=_("State"), default='draft')
    folio = fields.Char(string=_("Folio"), readonly=True)
    supplier_id = fields.Many2one(comodel_name='res.partner', string=_("Supplier"), required=True)
    supplier_number = fields.Char(string=_("Supplier number"), readonly=True, related='supplier_id.supplier_number')
    payment_form = fields.Selection(selection=PAYMENT_FORM, string=_("Payment form"), required=True, default='bank')
    journal_id = fields.Many2one(comodel_name='account.journal', string=_("Journal"), required=True)
    #  this fields is mandatory when payment form is transfer
    company_bank_id = fields.Many2one(comodel_name='res.partner.bank', string=_("Company account"))
    supplier_bank_id = fields.Many2one(comodel_name='res.partner.bank', string=_("Supplier account"))
    reference_folio = fields.Char(string=_("Reference folio"))
    # this field is required only when payment form is check
    checkbook_id = fields.Many2one(comodel_name='account.checkbook', string=_("Checkbook"))

    # General
    date_movement = fields.Datetime(string=_("Date movement"), default=fields.date.today(), required=True)

    amount_untaxed = fields.Float(string=_("Subtotal"), readonly=True, compute='compute_amount_total', store=True)
    amount_tax = fields.Float(string=_("Tax"), readonly=True, compute='compute_amount_total', store=True)
    amount_total = fields.Float(string=_("Total"), track_visibility='onchange')
    balance = fields.Float(string=_("Balance"), compute='_compute_balance')
    currency_id = fields.Many2one(comodel_name='res.currency', string=_("Currency"),
                                  track_visibility='onchange', default='default_currency')
    center_cost = fields.Many2one(comodel_name='account.cost.center', string=_('Center Cost'),
                                    required=True)
    comment = fields.Text(string=_("Comment"))

    display_name = fields.Char(compute='_compute_display_name')
    rate = fields.Float(string=_("Rate"), readonly=True)
    generated_policy = fields.Many2one(comodel_name='account.move', string=_("Generated policy"), readonly=True)
    cancelled_policy = fields.Many2one(comodel_name='account.move', string=_("Cancelled policy"), readonly=True)
    generated_check = fields.Many2one(comodel_name='account.check', string=_("Generated check"), readonly=True)

    user_generated = fields.Many2one(comodel_name='res.users', string=_("User generated"))
    user_cancelled = fields.Many2one(comodel_name='res.users', string=_("User cancelled"))
    date_cancelled = fields.Datetime(string=_("Date cancelled"))

    @api.multi
    @api.depends('generated_policy')
    def _compute_balance(self):
        for row in self:
            if row.generated_policy:
                if row.state in ['generated', 'applied']:
                    row.balance = row.get_balance(doc=row)
                    row._set_state('generated2applied')

    @api.model
    def get_balance(self, doc=None):
        for move_line in doc.generated_policy.line_id:
            if move_line.currency_id and doc.currency_id.id == move_line.currency_id.id:
                amount_original = abs(move_line.amount_currency)
                amount_unreconciled = abs(move_line.amount_residual_currency)
            else:
                amount_original = move_line.debit or move_line.credit
                amount_unreconciled = move_line.amount_residual
            if round(doc.amount_total, 2) == round(amount_original, 2):
                continue
            if round(doc.amount_tax, 2) == round(amount_original, 2):
                continue
            if move_line.reconcile_ref:
                balance = amount_unreconciled
            else:
                balance = amount_original
            if balance > 0:
                tax = round((((doc.amount_tax * 100) / doc.amount_untaxed) / 100) + 1, 2)
                balance = balance * tax
            else:
                balance = 0
            # print doc.amount_total, doc.amount_untaxed, doc.amount_tax, '--', amount_unreconciled , amount_unreconciled , balance, tax
            return round(balance, 2)

    @api.multi
    def _compute_display_name(self):
        for row in self:
            row.display_name = _("Advance payment supplier - ") + (row.folio or '/')

    @api.onchange('payment_form')
    def onchange_payment_form(self):
        self.clean_fields()
        return {
            'domain': {'journal_id': [('id', 'in', self.get_journal_by_payment_form(self.payment_form))]}
        }

    @api.model
    def get_journal_by_payment_form(self, payment_form):
        return self.env['account.journal'].search([('account_account_apply_id.code', '=', payment_form)]).ids or []

    @api.model
    def clean_fields(self, blacklist=[]):
        if 'journal_id' not in blacklist:
            self.journal_id = False
        if self.payment_form == 'check':
            self.supplier_bank_id = False
            self.reference_folio = False
        elif self.payment_form == 'bank':
            self.checkbook_id = False
        self.company_bank_id = False
        self.currency_id = False

    @api.onchange('journal_id')
    def onchange_journal_id(self):
        self.clean_fields(blacklist=['journal_id'])
        company = self.env['res.company'].search([('id', '=', self.env['res.company']._company_default_get())])
        currency = self.journal_id.currency or self.default_currency(need_model=True)
        to_return = {}
        if self.payment_form == 'check':
            to_return = {
                'domain': {
                    'checkbook_id': [('id', 'in',
                                      self.get_records_by_currency('checkbook', self.journal_id.checkbook_ids,
                                                                   currency))]
                }
            }
        elif self.payment_form == 'bank':
            to_return = {
                'domain': {
                    'company_bank_id': [
                        ('id', 'in', self.get_records_by_currency('company_banks', company.bank_ids, currency))],
                    'supplier_bank_id': [('id', 'in',
                                          self.get_records_by_currency('supplier_banks', self.supplier_id.bank_ids,
                                                                       currency))]
                }
            }
        to_return.get('domain').update({'currency_id': [('id', '=', currency.id)]})
        self.currency_id = currency
        return to_return

    @api.model
    def get_records_by_currency(self, _type, elements, currency):
        new_element_ids = []
        for element in elements:
            if _type == 'checkbook':
                element_currency = element.debit_journal_id.currency or self.default_currency(need_model=True)
            elif _type == 'company_banks':
                element_currency = element.journal_id.currency or self.default_currency(need_model=True)
            else:  # supplier_banks
                element_currency = element.currency2_id or self.default_currency(need_model=True)
            if element_currency.id == currency.id:
                new_element_ids.append(element.id)
        return new_element_ids

    @api.model
    def default_currency(self, need_model=None):
        if need_model:
            return self.env.user.company_id.currency_id
        return self.env.user.company_id.currency_id.id

    @api.model
    def default_purchase_tax(self):
        return self.env.user.company_id.tax_provision_supplier

    @api.depends('amount_total')
    @api.multi
    def compute_amount_total(self):
        tax = self.default_purchase_tax()
        for row in self:
            row.amount_untaxed = row.amount_total / (1 + tax.amount)
            row.amount_tax = row.amount_total - row.amount_untaxed

    @api.model
    def _set_state(self, _state):
        if _state == 'generated':
            self.user_generated = self.env.user.id
        elif _state == 'cancelled':
            self.user_cancelled = self.env.user.id
            self.date_cancelled = fields.Datetime.now()
        elif _state == 'generated2applied':
            if self.state == 'generated' and round(self.amount_total, 2) != self.get_balance(doc=self):
                _state = 'applied'
            elif self.state == 'applied' and round(self.amount_total, 2) == self.get_balance(doc=self):
                _state = 'generated'
            else:
                _state = self.state
        self.state = _state

    @api.multi
    def button_confirm(self):
        self.ensure_one()
        if self.amount_total == 0.0:
            raise exceptions.ValidationError(_("Put amount higher that zero, please."))
        if self.currency_id.name != self.journal_id.company_id.currency_id.name:
            date_rate = fields.date.today()
            qry_rate = "select get_rate('purchase','USD', '{}')".format(date_rate)
            self._cr.execute(qry_rate)
            _rate = self._cr.fetchone()[0] or 0
            if not _rate:
                raise exceptions.ValidationError(_("The purchase rate is %s of date %s" % (_rate, date_rate)))
            self.rate = _rate
        if self.journal_id.invoice_sequence_id:
            self.folio = self.env['ir.sequence'].next_by_id(self.journal_id.invoice_sequence_id.id)
        else:
            self.folio = self.env['ir.sequence'].get('advance.payment.supplier')
        if self.payment_form == 'check':
            self.create_check()
        self.create_policy()
        self._set_state('generated')

    @api.model
    def create_policy(self):
        self.generated_policy = self.env['account.move'].create(vals=self.prepare_policy(self.payment_form))
        if self.generated_policy.state == 'draft':
            self.generated_policy.button_validate()
        # self.folio = self.generated_policy.name

    @api.model
    def prepare_policy(self, _type):
        data = self.env['account.move'].account_move_prepare(journal_id=self.journal_id.id)
        acc_period = self.env['account.period']
        period = acc_period.find(dt=self.date_movement)
        data.update(
            {
                'ref': self.folio,
                'advance_payment_supplier': self.id,
                'date': self.date_movement,
                'period_id': period.id,
            }
        )
        data.update({'line_id': self.prepare_policy_lines(_type)})
        return data

    @api.model
    def prepare_policy_lines(self, _type):
        lines = []
        # Debit lines
        if self.currency_id.name != self.journal_id.company_id.currency_id.name:
            _amount_untaxed = self.amount_untaxed * self.rate
            _amount_tax = self.amount_tax * self.rate
            _amount_total = self.amount_total * self.rate
            _currency = self.currency_id.id
            _amount_currency_untaxed = self.amount_untaxed
            _amount_currency_tax = self.amount_tax
            _amount_currency_total = self.amount_total * -1
        else:
            _amount_untaxed = self.amount_untaxed
            _amount_tax = self.amount_tax
            _amount_total = self.amount_total
            _currency = False
            _amount_currency_untaxed = False
            _amount_currency_tax = False
            _amount_currency_total = False

        # if _type == 'bank':
        credit_account_id = self.company_bank_id.journal_id.default_credit_account_id.id
        # else:  # 'check':
        #    credit_account_id = False
        lines.append((0, 0, dict(
            name=_("Supplier Advance payment"),
            partner_id=self.supplier_id.id,
            cost_center_id=self.center_cost.id,
            account_id=self.journal_id.default_debit_account_id.id,
            debit=_amount_untaxed,
            credit=0,
            currency_id=_currency,
            amount_currency=_amount_currency_untaxed,
            ref=self.folio
        )))
        tax = self.default_purchase_tax()
        if tax:
            if not tax.name:
                raise exceptions.Warning(_("""The tax does not have a name,
                    please set up a name first"""))
            else:
                lines.append((0, 0, dict(
                    name=tax.name,
                    partner_id=self.supplier_id.id,
                    cost_center_id=self.center_cost.id,
                    account_id=tax.account_paid_voucher_id.id,
                    debit=_amount_tax,
                    credit=0,
                    currency_id=_currency,
                    amount_currency=_amount_currency_tax,
                    ref=self.folio
                )))
        # credit lines
        lines.append((0, 0, dict(
            name='/',
            partner_id=self.supplier_id.id,
            cost_center_id=self.center_cost.id,
            account_id=credit_account_id,
            debit=0,
            credit=_amount_total,
            currency_id=_currency,
            amount_currency=_amount_currency_total,
            ref=self.folio
        )))
        return lines

    @api.multi
    def button_cancel(self):
        self.ensure_one()
        if self.state == 'generated':
            if round(self.amount_total, 2) == self.get_balance(doc=self):
                if self.payment_form == 'check':
                    if self.generated_check.state != 'handed':
                        raise exceptions.Warning(_(
                            'You can not cancel handed issue checks in states other '
                            'than "handed". First try to change check state.'))
                    self.generated_check.signal_workflow('cancel')
                self.cancelled_policy = self.prepare_policy_to_cancel(self.payment_form)
                self._set_state('cancelled')
            else:
                raise exceptions.Warning(_("You can not cancel this advance payment is applied in a voucher."
                                           "First cancel the voucher related."))

    @api.model
    def prepare_policy_to_cancel(self, _type):
        if self.generated_policy:
            new_policy = self.generated_policy.copy()
            new_policy.button_cancel()
            ref = "Cancelado %s %s" % (self.folio, self.generated_policy.name)
            new_policy = self.prepare_policy_lines_to_cancel(new_policy, ref=ref)
            new_policy.ref = ref
            new_policy.period_id = self.env['account.move']._get_period()
            new_policy.date = fields.datetime.today()
            new_policy.button_validate()
            new_policy.line_id.reconcile()
            self.generated_policy.ref = "Cancelado %s" % self.folio
            self.generated_policy.line_id.reconcile()
            return new_policy
        return False

    @api.model
    def prepare_policy_lines_to_cancel(self, policy, ref):
        for line in policy.line_id:
            if line.debit:
                _debit = line.debit
                line.debit = 0.0
                line.credit = _debit
            elif line.credit:
                _credit = line.credit
                line.credit = 0.0
                line.debit = _credit

            if line.currency_id:  # !MXN
                if line.amount_currency < 0:
                    line.amount_currency = abs(line.amount_currency)
                elif line.amount_currency > 0:
                    line.amount_currency = line.amount_currency * -1
            line.ref = ref
        return policy

    @api.multi
    def unlink(self):
        if self.state == 'draft':
            return super(AdvancePaymentSupplier, self).unlink()
        else:
            raise exceptions.MissingError(
                _("This document is not possible remove. It's unlinked only with status draft."))

    @api.model
    def create_check(self):
        self.generated_check = self.env['account.check'].create(self.prepare_check())
        self.restructure_check()
        self.generated_check.signal_workflow('draft_router')
        self.generated_check.signal_workflow('holding_handed')
        # self.folio = self.generated_check.name

    @api.model
    def prepare_check(self):
        if self.currency_id.name != 'MXN':
            _company_currency_amount = self.amount_total * self.rate
        else:
            _company_currency_amount = None
        check = {
            'number': self.checkbook_id.next_check_number,
            'amount': self.amount_total,
            'company_currency_amount': _company_currency_amount,
            'other_partner_id': self.supplier_id.id,
            # 'type': 'issue_check',
            'issue_date': fields.Date.context_today(self),
            'payment_date': self.date_movement,
            'company_id': self.env['res.company']._company_default_get(),
            # 'bank_id': self.supplier_bank_id.bank.id,
            'checkbook_id': self.checkbook_id.id,
            'journal_id': self.journal_id.id,
            'issue_check_subtype': self.checkbook_id.issue_check_subtype,
            'advance_payment_supplier_id': self.id,

        }
        return check

    @api.model
    def restructure_check(self):
        query = """
            UPDATE account_check SET type = 'issue_check' WHERE id = %s """
        self._cr.execute(query, (self.generated_check.id,))
