# -*- coding: utf-8 -*-
# © Yonn, Xyz. All rights reserved.
from openerp import models, fields, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    advance_payment_supplier = fields.Many2one(
        comodel_name='advance.payment.supplier',
        string=_("Advance payment supplier"),
    )
