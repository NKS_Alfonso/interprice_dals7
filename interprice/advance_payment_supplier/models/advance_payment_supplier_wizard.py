# -*- coding: utf-8 -*-
# Copyright © 2018 Yonn Xyz - All Rights Reserved
from openerp.osv import osv
from openerp import fields, _, api, exceptions
from openerp.tools.misc import DEFAULT_SERVER_DATE_FORMAT
from olib.oCsv.oCsv import OCsv
from .advance_payment_supplier import PAYMENT_FORM, get_payment_form, get_state


class AdvancePaymentSupplierReport(osv.TransientModel):
    _name = 'advance.payment.supplier.report'

    payment_form = fields.Selection(string=_("Payment form"), selection=PAYMENT_FORM)
    journal_id = fields.Many2one(comodel_name='account.journal', string=_("Journal"))

    datetime_from = fields.Datetime(string=_("Datetime from"))
    datetime_to = fields.Datetime(string=_("Datetime to"))
    supplier_from = fields.Many2one(comodel_name='res.partner', string=_("Supplier from"))
    supplier_to = fields.Many2one(comodel_name='res.partner', string=_("Supplier to"))
    currency_id = fields.Many2one(comodel_name='res.currency', string=_("Currency"))

    @api.onchange('payment_form')
    def get_default_journal(self):
        self.journal_id = False
        return {
            'domain': {
                'journal_id': [
                    ('id', 'in', self.env['advance.payment.supplier'].get_journal_by_payment_form(self.payment_form))
                ]
            }
        }

    @api.multi
    def export_csv(self):
        rl = self.apply_filter()
        header = [[_("Journal"),
                   _("Folio"),
                   _("Date movement"),
                   _("Supplier number"),
                   _("supplier"),
                   _("Payment form"),
                   _("Reference folio"),
                   _("Amount total"),
                   _("Currency"),
                   _("Balance"),
                   _("Rate"),
                   _("State"),
                   _("User generated"),
                   _("Date cancelled"),
                   _("User cancelled"),
                   ]]
        rows = header
        self.env.cr.execute("select get_rate('sale', 'USD', '{}') ".format(fields.Datetime.now()))
        rate = self.env.cr.fetchone()[0] or 0
        for r in rl:
            rows.append([r.journal_id.name,
                         r.folio,
                         fields.Datetime.context_timestamp(self, fields.Datetime.from_string(
                             r.date_movement)) if r.date_movement else '',
                         r.supplier_id.supplier_number,
                         r.supplier_id.name,
                         get_payment_form(key=r.payment_form),
                         r.reference_folio or '',
                         r.amount_total,
                         r.currency_id.name if r.currency_id else '',
                         r.balance or 0.0,
                         r.rate if r.rate else rate,
                         get_state(key=r.state),
                         r.user_generated.name if r.user_generated else '',
                         fields.Datetime.context_timestamp(self, fields.Datetime.from_string(
                             r.date_cancelled)) if r.date_cancelled else '',
                         r.user_cancelled.name if r.user_cancelled else '',
                         ])

        obj_osv = OCsv()
        download_id = self.pool['r.download'].create(cr=self.env.cr, uid=self.env.uid, vals={
            'file_name': _(
                "Advance payment supplier - %s.csv" % fields.datetime.today().strftime(DEFAULT_SERVER_DATE_FORMAT)),
            'type_file': 'csv',
            'file': obj_osv.csv_base64(rows),
        })
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % download_id,
            'target': 'self',
        }

    @api.multi
    def screen_show(self):
        ids = self.apply_filter().ids
        return {
            'name': _("Advance payment supplier report"),
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'view_id': self.env.ref('advance_payment_supplier.iuv_todo_tree_advance_payment_supplier_report').id,
            'res_model': 'advance.payment.supplier',
            'target': 'current',
            'domain': [('id', 'in', ids)],
        }

    @api.multi
    def apply_filter(self):
        filters = []
        if self.datetime_from:
            filters.append(('date_movement', '>=', self.datetime_from))
        if self.datetime_to:
            filters.append(('date_movement', '<=', self.datetime_to))
        if self.supplier_from:
            filters.append(('supplier_id', '>=', self.supplier_from.id))
        if self.supplier_to:
            filters.append(('supplier_id', '>=', self.supplier_to.id))
        if self.journal_id:
            filters.append(('journal_id', '=', self.journal_id.id))
        if self.payment_form:
            filters.append(('payment_form', '=', self.payment_form))
        if self.currency_id:
            filters.append(('currency_id', '=', self.currency_id.id))
        results_list = self.env['advance.payment.supplier'].search(filters)
        if not len(results_list):
            raise exceptions.MissingError(_("Does not match any record, Please apply new filters."))
        return results_list
