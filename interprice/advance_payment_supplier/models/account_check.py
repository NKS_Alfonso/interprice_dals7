# -*- coding: utf-8 -*-
# © Yonn, Xyz. All rights reserved.

from openerp import models, fields, api, _


class AccountCheck(models.Model):
    _inherit = 'account.check'

    # custom fields
    advance_payment_supplier = fields.Many2one(
        comodel_name='advance.payment.supplier',
        readonly=True,
        ondelete='cascade',
        string=_("Advance payment supplier"),
    )
