# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp import models, fields, api
from xml.dom import minidom
import base64


class irAttachment(models.Model):
    _inherit = 'ir.attachment'


    @api.multi
    def create_new_line(self, record):
        """This function to get the elements to add on 
        a new line with the uuid and folio on poliza 
        fiscal folio.

        Decorators:
            api.multi

        Arguments:
            record -- object ir attachment model, with the
            attach, get the xml in base64.

        Returns:
            [write] -- return a write sentence with the
            new line to add on fiscal folio
        """

        # file_type = record.file_type
        # res_model = record.res_model
        
        # if res_model == 'account.move' and file_type in ('application/xml', 'text/xml'):
        res_id = record.res_id
        content = record.index_content
        move_id = self.env['account.move'].browse(res_id)
        data_xml = minidom.parseString(content)

        for node1 in data_xml.childNodes:
            if node1.tagName == 'cfdi:Comprobante':
                folio = node1.getAttributeNode('Folio').value.encode('utf-8')
                for node2 in node1.childNodes:
                    if node2.nodeName != '#text':
                        if node2.tagName == 'cfdi:Complemento':
                            for node3 in node2.childNodes:
                                if node3.nodeName != '#text':
                                    if node3.tagName == 'tfd:TimbreFiscalDigital':
                                        uuid = node3.getAttributeNode('UUID').value.encode('utf-8')

        uuids = []
        uuids.append({
            'partner_folio': folio,
            'uuid': uuid,
            'move': 'manual',
            })

        return move_id.write({'cfdi_table_ids': [(0, 0, x) for x in uuids]})

    @api.model
    def create(self, values):
        if '.xml' in values.get('name') and values.get('res_model') == 'account.move':
            xml_encode = values.get('datas')
            xml_decode = base64.b64decode(xml_encode)
            if '<cfdi:Comprobante' in xml_decode:
                if '<tfd:TimbreFiscalDigital' in xml_decode:
                    record = super(irAttachment, self).create(values)
                    self.create_new_line(record)
        else:
            record = super(irAttachment, self).create(values)

        return record
