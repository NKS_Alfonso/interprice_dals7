# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp import models, fields, api
import logging
from openerp.tools.translate import _


_logger = logging.getLogger(__name__)


class CfdiTable(models.Model):
    _name = 'cfdi.table'

    document_id = fields.Integer(string='Factura')
    document_obj = fields.Char(string='Objeto')
    partner_folio = fields.Char(string='Folio')
    uuid = fields.Char(string='UUID')
    # account_move_id = fields.Many2one('account.move', string='Poliza')
    account_move_id = fields.Many2many('account.move', string='Poliza')
    move = fields.Char(string='Movimiento')

    @api.multi
    def doc_view(self):
        _model = self.env[self.document_obj].search([('id', '=', self.document_id)])
        _return_values = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': self.document_obj,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': self.document_id,
        }
        if self.document_obj == 'account.invoice':
            _return_values = self.view_invoice(_model, self.document_id)
        elif self.document_obj == 'account.voucher':
            _return_values = self.view_voucher(_model, self.document_id)
        return _return_values

    @api.multi
    def view_voucher(self, account_voucher, account_voucher_id=False):
        ir_model_data = self.pool.get('ir.model.data')
        res_ir_model_data = False
        if account_voucher.type == 'receipt':
            res_ir_model_data = ir_model_data.get_object_reference(
                self.env.cr,
                self.env.uid,
                'account_voucher',
                'view_vendor_receipt_form')
        else:
            res_ir_model_data = ir_model_data.get_object_reference(
                self.env.cr,
                self.env.uid,
                'account_voucher',
                'view_vendor_payment_form')
        res_id = res_ir_model_data and res_ir_model_data[1]
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': account_voucher_id,
        }

    def view_invoice(self, account_invoice, account_invoice_id=False):
        ir_model_data = self.pool.get('ir.model.data')
        name = False
        res_ir_model_data = False
        if account_invoice.type in ('in_invoice', 'in_refund'):
            res_ir_model_data = ir_model_data.get_object_reference(
                self.env.cr,
                self.env.uid,
                'account',
                'invoice_supplier_form')
            name = _('Supplier Invoices')
        elif account_invoice.type in ('out_invoice', 'out_refund'):
            res_ir_model_data = ir_model_data.get_object_reference(
                self.env.cr,
                self.env.uid,
                'account',
                'invoice_form')
            name = _('Customer Invoices')

        res_id = res_ir_model_data and res_ir_model_data[1] or False

        return {
            'name': name,
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': account_invoice_id,
        }

    def get_doc_ref(self):
        if self.document_obj == 'account.invoice':
            self.env.cr.execute(
                """
                SELECT ai.number FROM account_invoice ai
                    WHERE (ai.id = %s);
                """, (self.document_id,)
            )
        elif self.document_obj == 'account.voucher':
            self.env.cr.execute(
                """
                SELECT av.number FROM account_voucher av
                    WHERE (av.id = %s);
                """, (self.document_id,)
            )
        if self.env.cr.rowcount:
            return self.env.cr.fetchall()[0][0]


class AccountMove(models.Model):
    _inherit = 'account.move'

    # cfdi_table_ids = fields.One2many(comodel_name='cfdi.table',
    #                                  inverse_name='account_move_id',
    #                                  readonly=True,
    #                                  string="UUID")
    cfdi_table_ids = fields.Many2many('cfdi.table', string="UUID")


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    validate_sat = fields.Boolean('Validar SAT', help="SI este campo esta habilitado, entonces se la informacion del"
                                                      " CFDI se valida en el webservice del SAT, al estar adjunto el"
                                                      " XML y se valida la factura")
    validate_xml = fields.Boolean('Validar XML', help="Si este campo esta activo entonces se valida el XML del"
                                                      " proveedor")
    max_diff = fields.Float('Diferencia Maxima', help="Maximas diferencias entre el total calculado en el formulario de"
                                                      " la factura y el total indicado en el XML")


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    docto_referencia = fields.Char(string='Docto Referencia')

# class AccountReportPartnersLedgerWizard(models.Model):
#     _inherit = 'partners.ledger.webkit'
#
#     result_selection = fields.Selection([('customer', 'Receivable Accounts'),
#                                          ('supplier', 'Payable Accounts'),
#                                          ('other', 'Normal'),
#                                          ('customer_supplier', 'Todos')],
#                                         required=True)
