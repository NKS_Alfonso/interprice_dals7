# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models


class AccountVoucher(models.Model):
    _inherit = 'account.voucher'

    def voucher_move_line_create(self, cr, uid, voucher_id, line_total, move_id, company_currency, current_currency, context=None):
        if context is None:
            context = {}
        voucher = self.pool.get('account.voucher').browse(cr, uid, voucher_id, context=context)
        move = self.pool.get('account.move').browse(cr, uid, move_id)
        if voucher.line_ids and voucher.type in (u'payment', u'receipt'):
            cfdi_table_ids = []
            for line in voucher.line_ids:
                if line.amount > 0 and line.move_line_id.move_id.cfdi_table_ids:
                    for line_cfdi in line.move_line_id.move_id.cfdi_table_ids:
                        cfdi_table_ids.append(line_cfdi.id)
            if cfdi_table_ids:
                move.cfdi_table_ids = move.cfdi_table_ids.browse(cfdi_table_ids)
        return super(AccountVoucher, self).voucher_move_line_create(cr, uid, voucher_id, line_total, move_id, company_currency, current_currency, context=context)
