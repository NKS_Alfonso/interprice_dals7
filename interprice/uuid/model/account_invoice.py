# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp import models, fields, api
from openerp import exceptions
import base64
import logging
from suds.client import Client

_logger = logging.getLogger(__name__)


try:
    import xmltodict
except:
    _logger.error(
        'Execute "sudo pip install xmltodict" to use '
        'l10n_mx_facturae_report module.')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    partner_folio = fields.Char(string='Folio')

    # _sql_constraints = [('cfdi_company_uniq', 'unique (cfdi_folio_fiscal, company_id)',
    #                      'El CFDI es unico por compañia'), ]

    @api.multi
    def invoice_validate(self):
        res = super(AccountInvoice, self).invoice_validate()

        if self.type in ('in_invoice', 'in_refund'):

            cfdis_list = []

            def validate_cfdi_folio_fiscal(data):

                if not data:
                    raise exceptions.ValidationError('Datos del CFDI no encontrados\n '
                                                     'Debe adjuntar un archivo de factura XML válido o proporcione '
                                                     'un número válido de CFDI')
                # Webservice parameters
                url = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl'
                client = Client(url)
                # Consume webservice
                result = client.service.Consulta(
                    """"?re=%s&rr=%s&tt=%s&id=%s""" %
                    (data['vat_emitter'] or '', data['vat_receiver'] or '',
                     data['amount'] or 0.0, data['uuid'] or '')
                )
                result = result and result.Estado or ''

                # If webservice does not return 'Vigente' state,
                # raises an exception
                if result != 'Vigente':
                    raise exceptions.ValidationError('El CFDI proporcionado no pasó la validación SAT\n'
                                                     'El estado de UUID en los servidores SAT es: \n' + str(result))

                return True

            for invoice in self:
                if invoice.cfdi_folio_fiscal:
                    if not invoice.partner_id.vat_split:
                        raise exceptions.ValidationError('No incluye el número de RFC proporcionado por este proveedor')
                    data = {
                        'vat_emitter': invoice.partner_id.vat_split,
                        'vat_receiver': invoice.company_id.partner_id.vat_split,
                        'amount': invoice.amount_total,
                        'uuid': invoice.cfdi_folio_fiscal
                    }

                    # Validates the CFDI info if the journal has enabled the
                    # "validate_sat" option
                    validate_cfdi_folio_fiscal(data)
                    continue

                # Validates the XML info if the journal has enabled the
                # "validate_xml" option
                if invoice.journal_id.validate_xml:
                    att_obj = self.env['ir.attachment']
                    att_ids = att_obj.search(
                        [('res_id', '=', invoice.id),
                         ('res_model', '=', 'account.invoice'), '|',
                         ('file_type', '=', 'application/xml'),
                         ('file_type', '=', 'text/plain')])

                    # Process and stores the info of the attachments
                    count = 0
                    for att in att_ids:
                        try:
                            db_datas = att.db_datas or att.datas
                            data_xml = base64.decodestring(db_datas)
                            dict_data = dict(xmltodict.parse(data_xml).get('cfdi:Comprobante', {}))
                        except:
                            # Skip file if any error
                            continue

                        complemento = dict_data.get('cfdi:Complemento', {})
                        emitter = dict_data.get('cfdi:Emisor', {})
                        receiver = dict_data.get('cfdi:Receptor', {})
                        data = {
                            'amount': float(dict_data.get('@Total', 0.0)),
                            'uuid': complemento.get('tfd:TimbreFiscalDigital', {}).get('@UUID', ''),
                            'vat_emitter': emitter.get('@Rfc', ''),
                            'vat_receiver': receiver.get('@Rfc', ''),
                            'folio': dict_data.get('@Folio', '')
                        }

                        count = count + 1

                    # Verify that exists only 1 xml attachment related
                    # to the invoice
                    if count == 0:
                        raise exceptions.ValidationError('Información de factura XML no encontrada.\n'
                                                         'Debe adjuntar un archivo de factura XML válido'
                                                         ' o proporcionar un número de CFDI válido')
                    elif count > 1:
                        raise exceptions.ValidationError('Múltiples archivos XML no válidos.\n'
                                                         'Debe proporcionar sólo un archivo de factura XML')

                    # Verify that VAT number on XML file match company VAT number

                    vat_receiver = data['vat_receiver']
                    company_vat = invoice.company_id.partner_id.vat[2:]
                    if vat_receiver != company_vat:
                        raise exceptions.ValidationError('Número RFC ' + vat_receiver +
                                                         ' en el archivo XML no coincide con el de la empresa')

                    vat_emitter = data['vat_emitter']
                    if invoice.partner_id.vat:
                        partner_vat = invoice.partner_id.vat[2:]
                        if vat_emitter != partner_vat:
                            raise exceptions.ValidationError('Número RFC ' + vat_emitter +
                                                             ' en el archivo XML no coincide con el del proveedor')
                    else:
                        raise exceptions.ValidationError('El proveedor no tiene asignado un número RFC')

                    # Verify that total on XML file match total on current invoice
                    invoice_diff = abs(data['amount'] - invoice.amount_total)
                    if invoice_diff > invoice.journal_id.max_diff:
                        raise exceptions.ValidationError('El importe en el archivo XML no coincide con el total de la '
                                                         'factura actual')

                    # Validates the CFDI info if the journal has enabled the
                    # "validate_sat" option
                    if invoice.journal_id.validate_sat:
                        validate_cfdi_folio_fiscal(data)

                    cfdis_list.append({
                        'document_id': invoice.id,
                        'document_obj': self._inherit,
                        'partner_folio': data['folio'],
                        'move': 'origen',
                        'uuid': data['uuid']})

                    invoice.write({'cfdi_folio_fiscal': data['uuid'], 'partner_folio': data['folio']})
                    invoice.move_id.write({'cfdi_table_ids': [(0, 0, x) for x in cfdis_list]})

        return res
