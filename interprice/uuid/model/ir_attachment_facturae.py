# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers


from openerp.osv import osv


class irAttachmentFacturaeMx(osv.Model):
    _inherit = 'ir.attachment.facturae.mx'

    def action_sign(self, cr, uid, ids, context=None):
        cfdis_list = []
        res = super(irAttachmentFacturaeMx, self).action_sign(cr, uid, ids, context=context)
        attachment = self.browse(cr, uid, ids[0], context=context)

        cfdis_list.append({
            'document_id': attachment.invoice_id.id,
            'document_obj': 'account.invoice',
            'partner_folio': attachment.invoice_id.number,
            'move': 'origen',
            'uuid': attachment.invoice_id.cfdi_folio_fiscal})
        attachment.invoice_id.move_id.write({'cfdi_table_ids': [(0, 0, x) for x in cfdis_list]})

        return res

