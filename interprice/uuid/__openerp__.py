# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': "UUID",

    'summary': " Módulo que agrega el UUID de la factura del proveedor o cliente a la poliza.",
    'author': 'Copyright © 2017 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    'category': 'Contabilidad',
    'version': '0.2',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'account',
                'l10n_mx_facturae_pac'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'view/account_move_view.xml',
        'view/account_journal_view.xml',
        'view/account_move_report.xml',
    ],
    'instalable': True,
}
