# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers


def _migrate_uuids_to_new_m2m_model(cr, version):
    """
    On post installation, from m2o to m2m model it is necesary to migrate CFDI data.

    In order to mantain DB consistency, data needs to be migrated from many2one model
    to many2many model.
    """
    cr.execute("""
               SELECT id
               FROM public.cfdi_table
               WHERE account_move_id IS NOT NULL
               """)
    cfdis = cr.fetchall()
    # Only run if there is data to migrate and previous version 0.1 of
    # the module is currenly installed.
    if cfdis and version == u'8.0.0.1':
        try:
            cr.execute("""
                        INSERT INTO public.account_move_cfdi_table_rel
                        SELECT id, account_move_id
                        FROM public.cfdi_table
                        WHERE account_move_id
                        IS NOT NULL
                        ORDER BY create_date ASC
                        """)
        except Exception as e:
            raise e


def migrate(cr, version):
    """
    migrate(cr, version) must be set in order for the migration manager can run it
    and should be py file inside migrations folder then odoo version (8.0) + module
    version (0.2) e.g.  migrations/8.0.0.2/ then py file should have prefix post-
    or pre- meaning the order of execution after or before installation note that
    one can have one or both files pre-*.py and/or post-*.py.
    """
    if not version:
        return
    _migrate_uuids_to_new_m2m_model(cr, version)
