Reporte de pólizas
------------------

Ubicación del menu

.. figure:: ../uuid/static/img/policy_menu_location.png
    :alt: Menu impresión de póliza
    :width: 100%


Impresión de póliza en formato pdf

.. figure:: ../uuid/static/img/policy_pdf.png
    :alt: Menu impresión de póliza
    :width: 100%