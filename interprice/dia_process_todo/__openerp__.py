# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
{
    'name': "TO-DO DIA Process",

    'summary': """
        Module to adapt the DIA Process in TO-DO.
    """,
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': '',
    'version': '0.1',

    # Module necessary for this one to work correctly
    'depends': [
        'base',
        'mail',
        'account',
        'sale',
        'delivery',
        'piramide_de_pago',
        'stock',
        'delivery_method',
        'web',
        'documents_inventory',
        'delivery',
        'series',
    ],

    # always loaded
    'data': [
        'data/process_dia_type_data.xml',
        'data/route_generator_sequence.xml',
        'security/menu_validate_by_finance.xml',
        'security/ir.model.access.csv',
        'views/todo_sale_order_inherit_add_logbook_page.xml',
        'views/add_menu_invoice_to_validate.xml',
        'views/sale_order_process_dia_conf_menus.xml',
        'views/delivery_carrier_inherit_form_view.xml',
        'views/piramide_pago_inherit_form_view.xml',
        # 'views/add_menu_show_invoice.xml',
        'views/configuration_view.xml',
        'views/invoices.xml',
        'views/pos_visor.xml',
        'views/process_dia_view.xml',
        'views/sale_order_logbook_view.xml',
        'views/dia_process_todo_sequence.xml',
        'views/driver_control_view.xml',
        'views/car_control_view.xml',
        'views/route_generator_view.xml',
        'report/account_invoice_ticket.xml',
        'report/sale_order_ticket.xml',
    ],
    'js': [
        'static/src/js/invoice.js',
        'static/src/js/crypto-js.js'
    ],
    'css': [
        'static/src/css/invoice.css',
    ],
    'qweb': ['static/src/xml/invoice.xml'],

}
