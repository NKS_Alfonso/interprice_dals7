# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class carControl(models.Model):
    _name = 'car.control'
    _rec_name = 'car_brand'
    _description = _('This is to have a control about the cars on the company')

    car_brand = fields.Char(
        string=_('Car brand'),
        help=_("Name of the car brand"),
    )
    car_sub_brand = fields.Char(
        string=_('Car Sub Brand'),
        help=_('Sub brand of the car')
    )
    car_type = fields.Char(
        string=_('Type car'),
        help=('Type of car, coupe, pickup, etc.'),
    )
    car_plate = fields.Char(
        string=_('Plate number'),
        help=_('Car Number Plate'),
    )
    car_model = fields.Char(
        string=_('Car Model'),
        help=_('Model of the car')
    )
    active = fields.Boolean(
        string=_('Active'),
        help=_('Check to active or inactive the car')
    )
    center_cost_id = fields.Many2one(
            comodel_name='account.cost.center',
            string=_('Cost Center'),
            help=_('Cost_Center')
    )
