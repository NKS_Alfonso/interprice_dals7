# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class ProcessDiaTypeDeliveryCarrierRel(models.Model):
    _name = 'process.dia.type.delivery.carrier.rel'

    process_type_id = fields.Many2one('process.dia.type', 'Process Type')
    delivery_id = fields.Many2one('delivery.carrier', 'Carrier')
