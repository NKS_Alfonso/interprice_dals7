# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from openerp import models, fields, _
import datetime


class AccountInvoicePrint(models.Model):
    _name = 'account.invoice.print'

    user_name = fields.Char(
        string=_('User name'),
        required=False,
        readonly=False,
        index=False,
        help=False,
        size=64,
        translate=False
    )

    print_date = fields.Datetime(
        string=_('Print date'),
        required=False,
        readonly=False,
        index=False,
        default=datetime.datetime.utcnow(),
        help=False
    )

    account_invoice_id = fields.Many2one(
        string='Print ids',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='account.invoice',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )

    supplier_user = fields.Many2one(
        string=_('Supplied user'),
        required=False,
        readonly=False,
        index=False,
        default=False,
        help=False,
        comodel_name='res.users',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    print_ids = fields.One2many(
        string=_('Account Invoice id'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='account.invoice.print',
        inverse_name='account_invoice_id',
        domain=[],
        context={},
        auto_join=False,
        limit=None
    )
