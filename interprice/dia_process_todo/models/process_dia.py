# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class ProcessDia(models.Model):
    _name = 'process.dia'
    _description = _('Its have a control steps loogbok')

    name = fields.Char(
        string=_('Number'),
        help=_("Sequence to identificate traceability in Process DIA"),
    )
    state = fields.Selection([
        ('starter', _('Start')),
        ('process', _('Process')),
        ('finished', _('Finished')),
        ('canceled', _('Canceled')),
        ('invoiced', _('Invoiced')),
        ('assortment', _('Assortment')),
        ('ensamble_soporte', _('Entrega a Soporte')),
        ('soporte_operaciones', _('Entrega a Operaciones')),
        ('operacion_auditoria', _('Entrega a Auditoria')),
        ('auditoria', _('En auditoria')),
        ('auditoria_empaque', _('Entrega a Empaque')),
        ('empaque_ventanilla', _('Entrega a Ventanilla')),
        ('venta_entrega_cliente', _('Entrega a Cliente')),
        # ('wait_validation', _('Waiting Validation')),
        ('validate_by_finance',_('Validate')),
    ])
    order_id = fields.Many2one(
        string=_('Sale order number'),
        comodel_name='sale.order',
    )
    sale_order_logbook_line = fields.One2many(
        comodel_name='sale.order.logbook',
        inverse_name='process_dia_id',
        string=_('Logbook lines')
    )
    process_date_start = fields.Datetime(
        string=_('Date Start'),
    )
    process_date_finish = fields.Datetime(
        string=_('Date Finish')
    )
