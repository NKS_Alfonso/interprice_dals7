# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, api, exceptions, fields, models
import openerp.tools as tools
import dateutil.parser


class ConfigurationInvoiceVisor(models.Model):
    """Assortment imprinted model"""

    _name = "configuration.invoice.visor"
    _rec_name = "account_cost_center_id"

    # domain method
    @api.model
    def _get_account_cost_center_domain(self):
        domain = []
        self.env.cr.execute(""" SELECT acc.id FROM account_cost_center acc
                            JOIN account_cost_center_res_users_rel accrur
                            ON accrur.account_cost_center_id=acc.id
                            WHERE accrur.res_users_id=%s """,
                            [self._uid])
        if self.env.cr.rowcount:
            account_cost_center_id = self.env.cr.fetchall()

            if account_cost_center_id:
                domain = [('id', 'in', [tw[0] for tw in account_cost_center_id])]
        else:
            domain = [('id', '=', False)]

        return domain


    # fields declaration
    account_cost_center_id = fields.Many2one(
        domain=_get_account_cost_center_domain,
        comodel_name='account.cost.center',
        help=_("Invoices configuration visor"),
        string=_("Account Cost Center")
    )

    delivery_carrier_ids = fields.One2many(
        comodel_name='delivery.carrier.conf',
        # help=_("Assortment configuration delivery carrier"),
        inverse_name='config_invoice_visor_id',
        string=_("Delivery Method")
    )
    delivery_qty_sum = fields.Float(
        compute='_compute_qty_sum',
        help=_("Assortment configuration delivery quantity"),
        string=_("Sum delivery")
    )
    document_qty_sum = fields.Float(
        compute='_compute_qty_sum',
        help=_("Assortment configuration document quantity"),
        string=_("Sum document")
    )


    # onchange method
    @api.onchange('account_cost_center_id')
    def get_account_cost_center_onchange(self):
        res = {'domain': {
            'account_cost_center_id': self._get_account_cost_center_domain()}}
        return res

    # computed fields methods
    @api.one
    @api.depends('delivery_carrier_ids.delivery_quantity')
    def _compute_qty_sum(self):
        self.delivery_qty_sum = sum(line.delivery_quantity for line in self.delivery_carrier_ids)
        self.document_qty_sum = sum(line.delivery_quantity for line in self.delivery_carrier_ids)

    # button method pickings print
    @api.cr_uid_ids_context
    def open_visor_interface(self, cr, uid, ids, context=None):
        cr.execute(""" SELECT civ.account_cost_center_id
                                from configuration_invoice_visor civ
                                where civ.id=%s """, (ids[0],))
        if cr.rowcount:
            account_cost_centers_ids = cr.fetchone()
        final_url = "/boxcashier/window/#action=invoice.ui&account_cost_center_id=" + str(account_cost_centers_ids[0])
        return {'type': 'ir.actions.act_url', 'url': final_url, 'target': 'self', }
    
    
    # # Create PostgreSQL views
    def init(self, cr):
        tools.drop_view_if_exists(cr, 'invoice_acc_users')
        tools.drop_view_if_exists(cr, 'invoice_acc_data')
        cr.execute("""
                    CREATE OR REPLACE view invoice_acc_users AS
                    (
                    SELECT
                        accrur.res_users_id res_users_id,
                        acc.id cost_center_id,
                        acc.name cost_center_name,
                        ai.id account_invoice_id,
                        ai.number account_invoice_number,
                        ai.internal_number account_invoice_internal_number
                    FROM account_cost_center_res_users_rel accrur
                    JOIN account_cost_center acc
                    ON accrur.account_cost_center_id=acc.id
                    JOIN account_invoice ai ON ai.cost_center_id=acc.id
                    where ai.state= 'open' and ai.type='out_invoice'
                    ) """
                    )

        cr.execute("""
                    CREATE OR REPLACE view invoice_acc_data AS
                    (
                    SELECT
                         T1.invoice_id,
                         T1.invoice_number,
                         T1.invoice_state,
                         T1.picking_origin,
                         T1.creation_date_picking,
                         T1.account_cost_center_id,
                         T1.account_cost_center_name,
                         T1.partner_id,
                         T1.partner_name,
                         T1.picking_id,
                         T1.carrier_id,
                         T1.delivery_name,
                         T1.process_dia_id,
                         T1.process_dia_name,
                         T1.process_dia_state
                    FROM (
                        SELECT
                            ai.id invoice_id,
                            ai.internal_number invoice_number,
                            ai.origin picking_origin,
                            ai.state invoice_state,
                            (cast(ai.invoice_datetime AT TIME ZONE 'utc' AS VARCHAR(19))) creation_date_picking,
                           acc.id account_cost_center_id,
                           acc.name account_cost_center_name,
                           rp.id partner_id,
                           rp.name partner_name,
                           sp.id picking_id,
                           so.carrier_id carrier_id,
                           dc.name delivery_name,
                           pd.id process_dia_id,
                           pd.name process_dia_name,
                           pd.state process_dia_state
                            FROM account_invoice ai
                                JOIN account_cost_center acc ON acc.id=ai.cost_center_id
                                join res_partner rp on rp.id=ai.partner_id
                                join stock_picking sp ON sp.id=ai.picking_dev_id
                                join sale_order so on so.name=sp.origin
                                left join process_dia pd on pd.order_id=so.id
                                join delivery_carrier dc on dc.id=so.carrier_id
                                WHERE ai.state IN ('open') and ai.type in ('out_invoice')
                                order by dc.name, ai.origin
                        ) as T1
                     )"""
                    )

    @api.model
    def get_invoices_ids(self):
        user_id = self._uid

        self.env.cr.execute(""" SELECT iaccu.cost_center_id acc_id
                                    FROM invoice_acc_users iaccu
                                    WHERE iaccu.res_users_id=%s""", [user_id])
        if self.env.cr.rowcount:
            acc_users = self.env.cr.dictfetchall()
            account_cost_center_ids_list = []

            for list in acc_users:
                account_cost_center_id = list['acc_id']
                account_cost_center_ids_list.append(account_cost_center_id)

                self.env.cr.execute("""
                        SELECT T1.acc_id,
                            T1.account_cost_center_name,
                            T1.account_cost_center_id,
                            T1.deliv_car_id,
                            T1.delivery_quantity,
                            T1.delivery_carrier_priority,
                            T1.delivery_carrier,
                            T1.config_invoice_visor_id,
                            T1.conf_invoice_visor_id as conf_invoice_visor_id,
                            T1.delivery_name
                        FROM(
                            SELECT acc.id acc_id, acc.name account_cost_center_name,
                                civ.account_cost_center_id account_cost_center_id, 
                                dcc.delivery_quantity delivery_quantity,
                                COALESCE(dc.id, 0) deliv_car_id,
                                CAST(dcc.delivery_carrier_priority as int) delivery_carrier_priority,
                                dcc.delivery_carrier,
                                dcc.config_invoice_visor_id,
                                civ.id as conf_invoice_visor_id,
                                dc.name as delivery_name
                            FROM account_cost_center acc
                            JOIN configuration_invoice_visor civ on civ.account_cost_center_id=acc.id
                            left JOIN delivery_carrier_conf dcc on dcc.config_invoice_visor_id=civ.id
                            left JOIN delivery_carrier dc on dc.id=dcc.delivery_carrier
                            WHERE acc.id in %s
                        ) as T1 
                    """,(tuple(account_cost_center_ids_list),)) 
                # self.env.cr.execute(""" 
                #         SELECT T1.invoice_id,
                #             T1.invoice_number,
                #             T1.invoice_state,
                #             T1.picking_origin,
                #             T1.account_cost_center_name,
                #             T1.partner_id,
                #             T1.partner_name,
                #             T1.picking_id,
                #             T1.carrier_id,
                #             T1.delivery_carrier_conf_id as delivery_conf_id,
                #             T1.delivery_carrier_priority,
                #             T1.delivery_quantity,
                #             T1.delivery_carrier,
                #             T1.config_invoice_visor_id,
                #             T1.conf_invoice_visor_id as conf_invoice_visor_id,
                #             T1.account_cost_center_id,
                #             CASE WHEN T1.process_dia_name IS NOT NULL THEN T1.process_dia_name
                #                                         ELSE 'No encontrado' END process_dia_name,
                #             T1.process_dia_id,
                #             CASE WHEN T1.process_dia_state IS NOT NULL THEN T1.process_dia_state
                #                                         ELSE 'No encontrado' END process_dia_state,
                #             T1.delivery_name
                #         FROM(
                #             SELECT iad.invoice_id,
                #                 iad.invoice_number,
                #                 iad.invoice_state,
                #                 iad.picking_origin,
                #                 iad.creation_date_picking,
                #                 iad.account_cost_center_id,
                #                 iad.account_cost_center_name,
                #                 iad.partner_id, iad.partner_name,
                #                 iad.picking_id, iad.carrier_id,
                #                 iad.process_dia_id, iad.process_dia_name, iad.process_dia_state,
                #                 dcc.id as delivery_carrier_conf_id,
                #                 CAST (dcc.delivery_carrier_priority as int),
                #                 dcc.delivery_quantity,
                #                 dcc.delivery_carrier,
                #                 dcc.config_invoice_visor_id,
                #                 civ.id as conf_invoice_visor_id,
                #                 civ.account_cost_center_id as civ_acc_cost_center,
                #                 dc.name as delivery_name
                #             FROM invoice_acc_data iad 
                #             JOIN delivery_carrier_conf dcc on dcc.delivery_carrier=iad.carrier_id
                #             JOIN configuration_invoice_visor civ on civ.id=dcc.config_invoice_visor_id
                #             JOIN delivery_carrier dc on dc.id=iad.carrier_id
                #             WHERE iad.account_cost_center_id in %s
                #             --LIMIT 20
                #             ) as T1 
                #             """,(tuple(account_cost_center_ids_list),)) 
                
                if self.env.cr.rowcount:
                    commerce_rules_data = self.env.cr.dictfetchall()

                    invoices_rules_apply = []

                for commerce in commerce_rules_data:

                    config_invoice_visor_id = commerce['config_invoice_visor_id']
                    account_cost_center_id = commerce['account_cost_center_id']
                    conf_doc_type_qty = commerce['delivery_quantity']
                    deliv_car_id = commerce['deliv_car_id']

                    self.env.cr.execute(""" SELECT * FROM invoice_acc_data
                                            WHERE account_cost_center_id = %s
                                            AND carrier_id = %s
                                            limit %s """,
                                        (account_cost_center_id,
                                         deliv_car_id, conf_doc_type_qty)
                                        )
                    if self.env.cr.rowcount:
                        pickings_data = self.env.cr.dictfetchall()

                        for data in pickings_data:
                            account_cost_center_id = data['account_cost_center_id']
                            delivery_carrier_id = data['carrier_id']
                            invoice_id = data['invoice_id']

                            self.env.cr.execute(""" SELECT
                                                     aip.account_invoice_id invoice_id,
                                                     aip.user_name user_id,
                                                     aip.create_date print_date,
                                                     res_users.login login_id
                                                     FROM account_invoice_print aip
                                                     LEFT JOIN res_users ON aip.supplier_user = res_users.id
                                                     WHERE aip.account_invoice_id = %s""",
                                                (invoice_id,)
                                                )

                            if self.env.cr.rowcount:
                                invoice_print_data = self.env.cr.dictfetchall()

                                account_invoice_id = invoice_print_data[0].get('invoice_id')
                                print_date = invoice_print_data[0].get('print_date').split('.')[0]
                                user_id = invoice_print_data[0].get('user_id')
                                supplier_login = invoice_print_data[0].get('login_id', 'No surtido')

                                data['invoice_id'] = account_invoice_id
                                data['print_date'] = print_date
                                data['user_id'] = user_id
                                data['login_id'] = supplier_login

                            delivery_carrier_query = ''
                            if delivery_carrier_id > 0:
                                delivery_carrier_query = 'AND dcc.delivery_carrier = %s' % (delivery_carrier_id,)

                            self.env.cr.execute(""" SELECT
                                                    CAST(dcc.delivery_carrier_priority as int) delivery_carrier_priority
                                                FROM configuration_invoice_visor civ
                                                LEFT JOIN delivery_carrier_conf dcc
                                                ON dcc.config_invoice_visor_id=civ.id
                                                WHERE account_cost_center_id = %s
                                                %s """ % (account_cost_center_id,
                                                delivery_carrier_query,)
                                            )
                            if self.env.cr.rowcount:
                                data_priority = self.env.cr.dictfetchall()
                                delivery_carrier_priority = data_priority[0].get('delivery_carrier_priority')
                                data['delivery_carrier_priority'] = delivery_carrier_priority
                            
                            invoices_rules_apply.append(data)

                # return invoices_rules_apply

                return sorted(invoices_rules_apply, key=lambda pick: dateutil.parser.parse(pick['creation_date_picking']), reverse=False)
