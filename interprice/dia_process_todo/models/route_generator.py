# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class routeGenerator(models.Model):
    _name = 'route.generator'
    _rec_name = 'name'
    _description = _('This is to have a control about the ship route on the company')

    name = fields.Char(
        string=_('Number'),
        help=_("Sequence to identificate ahip route"),
    )
    account_cost_center_id = fields.Many2one(
        comodel_name='account.cost.center',
            string=_('Account Cost Center'),
            help=_('Car'),
    )
    driver_id = fields.Many2one(
        comodel_name='driver.control',
            string=_('Driver'),
            help=_('Driver Name'),
    )
    car_id = fields.Many2one(
        comodel_name='car.control',
            string=_('Car'),
            help=_('Car designed'),
    )

    order_ids = fields.One2many(
        comodel_name='sale.order',
        inverse_name='routes_id',
        string=_('Order lines')
    )

    @api.model
    def create(self, vals):
        res = super(routeGenerator, self).create(vals)
        sequence_id = self.env.ref('dia_process_todo.is_seq_dpt_route_generator').ids
        res.name = self.env['ir.sequence'].get_id(sequence_id[0])
        return res