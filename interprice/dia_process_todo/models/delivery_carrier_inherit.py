# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# related third party imports
# local application/library specific imports

from openerp import models, fields, api, exceptions, _


class deliveryCarrier(models.Model):
    """Extended model to add priorities to delivery carrier"""

    # odoo model properties
    _inherit = 'delivery.carrier'

    # custom fields
    carrier_type = fields.Selection(
        help=_('Field to identify the carrier type'),
        required=True,
        selection=[
            ('local_carrier',_('Local Carrier')),
            ('sent_by_package',_('Sent by Package')),
            ('floor_collect',_('Collect Floor')),
        ],
        string=_('Shipping method')
    )


class deliveryCarrierConf(models.Model):

    _name = "delivery.carrier.conf"

    # fields declaration
    config_invoice_visor_id = fields.Many2one(
        comodel_name='configuration.invoice.visor',
        help=_("Delivery carrier id"),
        string=_("Configuration")
    )
    delivery_carrier = fields.Many2one(
        comodel_name='delivery.carrier',
        help=_("Delivery carrier name"),
        string=_("Delivery Method")
    )
    delivery_quantity = fields.Integer(
        default=1,
        help=_("Delivery carrier quantity"),
        string=_("Quantity")
    )
    delivery_carrier_priority = fields.Selection([
            ('1','1'),('2','2'),('3','3'),
            ('4','4'),('5','5'),('6','6'),
            ('7','7'),('8','8'),('9','9'),
            ('10','10')
        ],
        help=_("Delivery carrier document priority"),
        string=_('Delivery carrier priority')
    )

    # overriden methods
    @api.model
    def create(self, vals):
        if 'delivery_quantity' in vals and vals['delivery_quantity'] <= 0:
            raise exceptions.Warning(_("Quantity must be greater than 0."))

        if 'delivery_carrier' in vals and 'config_invoice_visor_id' in vals:
            for data_id in self.search(
                    [('delivery_carrier', '=', vals.get('delivery_carrier')),
                     ('config_invoice_visor_id', '=', vals.get('config_invoice_visor_id'))
                     ]):

                if data_id:
                    raise exceptions.Warning(
                        _("You can not assign the same delivery method more than once to the document."))
        return super(deliveryCarrierConf, self).create(vals)

    @api.multi
    def write(self, vals):

        if 'delivery_quantity' in vals and vals['delivery_quantity'] <= 0:
            raise exceptions.Warning(_("Quantity must be greater than 0."))

        if 'delivery_carrier' in vals:
            for data_id in self.search(
                    [('delivery_carrier', '=', vals.get('delivery_carrier')),
                     ('config_invoice_visor_id', '=', self.config_invoice_visor_id.id)
                     ]):

                if data_id:
                    raise exceptions.Warning(
                        _("You can not assign the same delivery method more than once to the document."))
        return super(deliveryCarrierConf, self).write(vals)
