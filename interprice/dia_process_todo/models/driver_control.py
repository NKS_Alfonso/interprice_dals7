# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class driverControl(models.Model):
    _name = 'driver.control'
    _rec_name = 'driver_name'
    _description = _('This is to have a control about the drivers on the company')

    driver_name = fields.Char(
        string=_('Driver Name'),
        help=_("Name of Driver"),
    )
    license_type = fields.Char(
        string=_('License Type'),
        help=('Type of license.'),
    )
    active = fields.Boolean(
        string=_('Active'),
        help=_('Check to active or inactive the driver')
    )
    designed_car = fields.Many2one(
            comodel_name='car.control',
            string=_('Car'),
            help=_('Car'),
        )
    center_cost_id = fields.Many2one(
            comodel_name='account.cost.center',
            string=_('Cost Center'),
            help=_('Cost_Center')
    )
