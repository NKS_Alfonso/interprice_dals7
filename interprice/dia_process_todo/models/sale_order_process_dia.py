# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _


class sale_order_process_dia(models.Model):
    _name = "sale.order.process.dia"
    _description = _('Model to configurate products to recover service in \
                      the sale.order inside the process DIA')

    name = fields.Char(
        string=_('Name'),
        help=_('Process')
        )
    process_type = fields.Many2one(
        comodel_name='process.dia.type',
        string=_('Process'),
        )
    product_id = fields.Many2one(
        comodel_name='product.product',
        string=_('Product Id')
        )
    active = fields.Boolean(
        string=_('active')
    )
