# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class ProcessDiaType(models.Model):
    _name = 'process.dia.type'
    _description = _('add process type to configured in sale.order.process.dia')

    name = fields.Char(
        string=_('Type Process'),
        help=_('Process Name'),
        )
    delivery_id = fields.Many2many(
            comodel_name='delivery.carrier',
            relation='process_dia_type_delivery_carrier_rel',
            column1='process_type_id',
            column2='delivery_id',
            string='delivery Methods'
        )
    app_identification = fields.Selection(selection=[
            ('createso', _('Creation SO')),
            ('confirmso', _('Confirm SO')),
            ('assemble', _('Assemble')),
            ('invoiced', _('Invoiced')),
            ('assortment', _('Assortment')),
            ('packing', _('Packing')),
            # ('suport_operation', _('Delivery to Operations')),
            ('operation_audit', _('Delivery to Audit')),
            # ('audit', _('Audit in Process')),
            # ('audit_delivery', _('Delivery to Packaging')),
            ('delivery_window', _('Delivery Window')),
            ('window_costumer', _('Delivery to Costumer')),
            ('validated', _('Validated')),
            ('rejected', _('Rejected')),
            ('surveillance', _('Surveillance')),
            ('get_routes', _('Generator route')),
            ('get_packing', ('Get Packing'))
        ],
        string=_('Process Identification'),
        help=_("""This fields is to Process identification inside 
                  application Django""")
    )
    active = fields.Boolean(
        string=_('Active'),
        help=_('Check to Actived')
        )
