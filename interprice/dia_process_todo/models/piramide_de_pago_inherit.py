# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _
import logging
_logger = logging.getLogger(__name__)


class PiramideDePago(models.Model):
    """ Model extend"""
    _inherit = 'piramide_de_pago'

    # Odoo properties

    required_validation = fields.Boolean(
        string=_('Required Validation?'),
        required=False,
        help=_("""This field is necessary to can identify if this payment type
         requires validation by Finance or Treasury"""
            ),
    )

    validation_type = fields.Selection(
        help=_("This field is necessary to identify which is the department which will validate the Sale Order"),
        # required=True,
        selection=[
            ('finance',_('By Finance')),
            ('accounts_receivable',_('By Accounts Receivable')),
        ],
        # string=_('Shipping method'),
    )