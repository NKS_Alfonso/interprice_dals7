# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, api, exceptions, _


class PosVisor(models.Model):
    """Ticket imprinted model"""

    _name = "pos.visor"

    @api.cr_uid_ids_context
    def open_print_interface(self, cr, uid, ids, context=None):
        final_url = "/boxcashier/window/#action=invoice.ui&ids=" + str(ids[0])
        return {'type': 'ir.actions.act_url', 'url': final_url, 'target': 'self', }

    @api.cr_uid_ids_context
    def open_print_interface(self, cr, uid, ids, context=None):
        cr.execute("""
                   SELECT account_cost_center_id
                   FROM configuration_invoice_visor
                   ORDER BY write_date DESC
                   LIMIT 1
                   """)
        if cr.rowcount:
            account_cost_center_id = cr.fetchone()
            final_url = "/boxcashier/window/#action=invoice.ui&account_cost_center_id=" + str(account_cost_center_id[0])
            return {'type': 'ir.actions.act_url', 'url': final_url, 'target': 'self', }
        else:
            raise exceptions.ValidationError(_("Please configure invoice visor at Sale/Configuration/Process Dia/Invoice visor configuration"))
