# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _
import logging
_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    """ Model extend"""
    _inherit = 'stock.picking'
    # Odoo properties

    @api.multi
    def action_assign(self):
        res = super(StockPicking, self).action_assign()
        if self.picking_type_id.code == 'outgoing':
            logbook = self.env['sale.order.logbook']
            process_dia_id = self.env['process.dia'].search([
                                ('order_id','=',self.sale_id.id)
                                ])

            assortment_created = False
            for logbook_line in self.sale_id.sale_order_logbook_line:
                if not logbook_line.process_type.app_identification == 'assortment':
                    if not assortment_created:
                        process_type = self.env['process.dia.type'].\
                                search([('app_identification', '=', 'assortment')])

                        new_process_line = logbook.create({
                            'process_type': process_type.id,
                            'sale_order_id':self.sale_id.id,
                            'date_time_start': self.date,
                            'user_code': self.env.user.name,
                            'state': 'in_process',
                            })

                        if new_process_line and process_dia_id:
                            process_dia_id.update({'state': 'process'})    
                            new_process_line.process_dia_id = process_dia_id.id 

                        assortment_created = True
        return res