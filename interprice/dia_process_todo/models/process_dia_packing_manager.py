# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class ProcessDiaPackingManager(models.Model):
    _name = 'process.dia.packing.manager'
    _description = _('Model to catch the packing information')

    order_id = fields.Many2one(
        string=_('Sale order number'),
        comodel_name='sale.order',
    )
    boxes_number = fields.Integer(
        string=_('Boxes Number'),
        help=_('Number of boxes to packing'),
    )
    lumps_number = fields.Integer(
        string=_('Lumps Number'),
        help=_('Number of lumps'),
    )
    pallets_number = fields.Integer(
        string=_('Number Pallets'),
        help=_('Number of pallets'),
    )
    chord_c2 = fields.Integer(
        string=_('chord C2'),
    )
    chord_c3 = fields.Integer(
        string=_('chord C3'),
    )
    chord_c4 = fields.Integer(
        string=_('chord C4'),
    )
    chord_c5 = fields.Integer(
        string=_('chord C5'),
    )
    observations = fields.Char(
        string=_('Observations'),
        size=500
    )
    state = fields.Selection([
        ('draft', _('Draft')),
        ('in_process', _('In Process')),
        ('sending', _('Send'))],
        string='Status',
        # required=True,
        # readonly=True,
    )
