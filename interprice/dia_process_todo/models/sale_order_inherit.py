# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _
from openerp.addons.l10n_mx_invoice_amount_to_text import amount_to_text_es_MX

import logging
_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    """ Model extend"""
    _inherit = 'sale.order'
    # Odoo properties

    state = fields.Selection(selection_add=[
                ('invoiced', _('Invoiced')),
                ('assortment', _('Assortment')),
                ('ensamble_soporte', _('Entrega a Soporte')),
                ('soporte_operaciones', _('Entrega a Operaciones')),
                ('operacion_auditoria', _('Entrega a Auditoria')),
                ('auditoria', _('En auditoria')),
                ('auditoria_empaque', _('Entrega a Empaque')),
                ('empaque_ventanilla', _('Entrega a Ventanilla')),
                ('venta_entrega_cliente', _('Entrega a Cliente')),
                ('wait_validation', _('Waiting Validation')),
                # ('validate_by_finance',_('Validate')),
            ])

    brequired_validation = fields.Boolean(
        string=_('Required Validation by finance'),
    )

    # bvalidate_finance = fields.Boolean(
    #     string=_('Validate By Finance')
    #     )
    is_validated = fields.Selection(
        string=_("Validated"),
        selection=[
            ('accepted',_('Accepted')),
            ('rejected',_('Rejected')),
        ])

    sale_order_logbook_line = fields.One2many(
        comodel_name='sale.order.logbook',
        inverse_name='sale_order_id',
        string=_('Logbook lines')
        )

    other_service = fields.Many2one(
        comodel_name='sale.order.process.dia',
        string="Other service"
    )

    routes_id = fields.Many2one(
        string=_('Route number'),
        comodel_name='route.generator',
    )

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        if res.forma_pago.required_validation == True:
            res.brequired_validation = True

        process_type = self.env['process.dia.type'].\
                            search([('app_identification', '=', 'createso')])

        sequence_id = self.env.ref('dia_process_todo.iuv_seq_dia_process_todo').ids
        name_sequence = self.env['ir.sequence'].get_id(sequence_id[0])

        res.sale_order_logbook_line.create({
            'process_type': process_type.id,
            'sale_order_id':res.id,
            'date_time_start': res.date_order,
            'date_time_end': res.create_date,
            'user_code': res.user_id.name,
            'user_code_finish': res.user_id.name,
            'check_to_validate': True,
            'state': 'done',
            })

        process_dia = self.env['process.dia']
        process_dia_id = process_dia.create({
                'order_id': res.id,
                'name': name_sequence,
                'process_date_start':res.create_date,
                'state': 'starter',
                'sale_order_logbook_line': res.sale_order_logbook_line,
                })

        if res.sale_order_logbook_line and process_dia_id:
            res.sale_order_logbook_line.update({
                'process_dia_id': process_dia_id.id
                })

        if res.other_service:
            products = res.other_service.product_id
            self.save_other_service(products,
                res.id, res.partner_id.id, res.order_line)
        return res

    @api.multi
    def write(self, vals):
        if vals.get('forma_pago'):
            forma_pago = self.env['piramide_de_pago'].browse(vals.get('forma_pago'))
            if forma_pago and forma_pago.required_validation == False:
                self.brequired_validation = False
            else:
                self.brequired_validation = True
        if vals.get('other_service'):
            products = self.env['sale.order.process.dia'].browse(
                                                    vals.get('other_service')
                                                )
            self.save_other_service(
                                    products.product_id,
                                    self.id,
                                    self.partner_id.id,
                                    self.order_line
                                )
        elif vals.get('other_service') is None:
            pass
        else:
            if self.other_service:
                self.delete_other_service(self.other_service, self.order_line)
        return super(SaleOrder, self).write(vals)

    @api.multi
    def button_dummy(self):
        record = super(SaleOrder, self).button_dummy()
        if self.other_service:
            products = self.other_service.product_id
            self.save_other_service(products, self.id, self.partner_id.id, self.order_line)
        else:
            if self.other_service:
                self.delete_other_service(self.other_service, self.order_line)
        return record

    def action_button_confirm(self, cr, uid, ids, context=None):
        env = api.Environment(cr, uid, [])
        self.env = env
        
        sale_order_obj = self.env['sale.order'].browse(ids[0])
        process_type = self.env['process.dia.type'].\
                            search([('app_identification', '=', 'confirmso')])
        logbook = sale_order_obj.sale_order_logbook_line

        new_line = logbook.create({
            'sale_order_id': sale_order_obj.id,
            'process_type': process_type.id,
            'date_time_start': logbook[len(logbook)-1].date_time_end,
            'date_time_end': fields.datetime.now(),
            'user_code': sale_order_obj.user_id.name,
            'user_code_finish': sale_order_obj.user_id.name,
            'check_to_validate': True,
            'state': 'done',
            })
        
        process_dia_id = self.env['process.dia'].search([
            ('order_id','=',sale_order_obj.id)
            ])

        if new_line and process_dia_id:
            new_line.update({
                'process_dia_id': process_dia_id.id,
                })
            process_dia_id.state = 'process'


        if sale_order_obj.forma_pago.required_validation:
            if sale_order_obj.forma_pago.validation_type in ['finance', 'accounts_receivable']:
                sale_order_obj.state = 'wait_validation'
            else:
                pass

            # return super(SaleOrder, self).action_button_confirm(cr, uid, ids, context=context)

    # @api.model
    # def get_journal(self, account_id):
    #     if account_id:
    #         self.env.cr.execute("""
    #             SELECT id FROM account_journal
    #             WHERE default_debit_account_id = %s
    #             and type='sale'
    #             """, (account_id,)
    #             )
    #         if self.env.cr.rowcount:
    #             journal_id = self.env.cr.fetchone()[0]
    #     return journal_id

    @api.multi
    def validate_by_finance(self):
        logbook = self.sale_order_logbook_line
        process_dia_id = self.env['process.dia'].search([('order_id', '=', self.id)])

        if self.brequired_validation:
            self.is_validated = 'accepted'
            self.state = 'progress'
            process_type = self.env['process.dia.type'].\
                            search([('app_identification', '=', 'validated')])
            for line in logbook:
                if line.process_type.app_identification == 'confirmso':
                    start_date = line.date_time_end
                    break
                else:
                    start_date = self.create_date

            new_process_line = logbook.create({
                'sale_order_id': self.id,
                'process_type': process_type.id,
                'date_time_start': start_date,
                'user_code': self.env.user.name,
                'date_time_end': fields.datetime.now(),
                'user_code_finish': self.env.user.name,
                'check_to_validate': True,
                'state': 'done',})

            if new_process_line and process_dia_id:
                process_dia_id.update({'state': 'process'})    
                new_process_line.process_dia_id = process_dia_id.id          

        return True

    @api.multi
    def rejected_by_finance(self):
        logbook = self.sale_order_logbook_line
        process_dia_id = self.env['process.dia'].search([('order_id', '=', self.id)])

        process_type = self.env['process.dia.type'].\
                            search([('app_identification', '=', 'rejected')])
        if self.brequired_validation:
            self.is_validated = 'rejected'
            self.state = 'draft'
            new_process_line = logbook.create({
                'sale_order_id': self.id,
                'process_type': process_type.id,
                'date_time_start': self.create_date,
                'date_time_end': fields.datetime.now(),
                'user_code': self.env.user.name,
                'user_code_finish': self.env.user.name,
                'check_to_validate': True,
                'state': 'done',
                })

            if new_process_line and process_dia_id:
                process_dia_id.update({'state': 'canceled'})    
                new_process_line.process_dia_id = process_dia_id.id 

        return True

    def save_other_service(self, product, order_id, partner_id, order_lines):
        product_exists = False
        taxes_ids = product.product_tmpl_id.taxes_id
        values = {
                    'order_id': order_id,
                    'order_partner_id': partner_id,
                    'name': product.name_template,
                    'state': 'draft',
                    'product_uom_qty': 1,
                    'product_uom': product.product_tmpl_id.uom_id.id,
                    'product_id': product.id,
                    'price_unit': product.product_tmpl_id.list_price,
                    'purchase_price': product.product_tmpl_id.standard_price,
                    'tax_id': [(6, 0, [tax.id for tax in taxes_ids])],
                    'is_delivery': False,
            }

        for line in order_lines:
            if line.product_id.id == product.id:
                product_exists = line

        if product_exists:
            product_exists.write(values)
            # values_line = [[1, product_exists, values]]
        else:
            self.env['sale.order.line'].create(values)
            # values_line = [[0, False, values]]
        return True

    def delete_other_service(self, service, order_lines):
        for line in order_lines:
            if line.product_id.id == service.product_id.id:
                line.unlink()
        return True

    def _amount_to_text(self, num, currency):
        """Function to parse the total amount to text.
              
        Arguments:
            num {float} -- Data type to can pass the number 
                            to text with decimal.
            currency {text} -- Name currency
        
        Returns:
            [text] -- data with the number pass to text
        """
        num = float(num)
        partner = self._context.get('lang')
        if partner:
            partner_lang = partner.encode('utf-8')
        else:
            partner_lang = self._context.get('lang')

        amount_to_text = amount_to_text_es_MX.\
            get_amount_to_text(self, num, partner_lang, currency)
        #validate type data string amount
        if type(amount_to_text) == unicode:
            amount = amount_to_text.encode('utf-8')
        elif type(amount_to_text) == str:
            amount = amount_to_text
        else:
            amount = amount_to_text
        # replace USD or MXN
        if 'USD' in (amount):
            amount_to = amount.replace('USD', '').replace(
                'M. E.', 'USD')
        else:
            amount_to = amount.replace('PESOS', '').replace(
                'M. N.', 'MXN')
        return amount_to