# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp.osv import osv
from openerp import models, fields, api, exceptions, _


class delivery_grid_amounts(models.Model):
    _inherit = "partner.amounts"

class delivery_grid_segment(models.Model):
    _inherit = "delivery.grid"

class delivery_state_amounts(models.Model):
    _inherit = "state.amounts.delivery"

class delivery_state_amounts(models.Model):
    _inherit = "delivery.carrier"
