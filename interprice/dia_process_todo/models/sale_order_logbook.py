# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import exceptions, fields, models, api, _


class saleOrderLogbook(models.Model):
    _description = _('Model to show the logbook in sale order')
    _name = 'sale.order.logbook'
    _defaults = {
        'state': 'draft',
    }

    sale_order_id =  fields.Many2one(
        string=_('Sale order number'),
        comodel_name='sale.order',
    )

    process_type = fields.Many2one(
        comodel_name='process.dia.type',
        string=_('Process'),
        store=True
    )

    date_time_start = fields.Datetime(
        default=fields.datetime.now(),
        string=_('Start datetime'),
    )

    date_time_end = fields.Datetime(
        string=_('End datetime'),
    )

    user_code = fields.Char(
        string=_('User number Start'),
        help=_('Name of the user who makes the process'),
    )

    user_code_finish = fields.Char(
        string=_('User number End'),
        help=_('Name of User who finished the process')
    )

    check_to_validate = fields.Boolean(
        string=_('Finished'),
    )

    process_dia_id =  fields.Many2one(
        string=_('Proces DIA number'),
        comodel_name='process.dia',
    )

    state = fields.Selection([
        ('draft', _('Draft')),
        ('cancel', _('Cancel')),
        ('pause', _('Pause')),
        ('in_process', _('In Process')),
        ('validate', _('Validate')),
        ('invalidate', _('Invalidate')),
        ('done', _('Done')),],
        string='Status',
        # required=True,
        # readonly=True,
    )

    user_observations = fields.Char(
        String="User Observations",
        size=500,
        help="""This field is to support departament operator, when finding
        inusual task, the operator  describe the problem"""
    )

