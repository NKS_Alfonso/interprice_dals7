# -*- coding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author      TO-DO Developers

from openerp import http
from openerp.http import request
import logging
import json


_logger = logging.getLogger(__name__)


class InvoiceViewController(http.Controller):

    @http.route(['/boxcashier/window'], type='http', auth='user')
    def account_invoice_visor(self, debug=False, **k):
        # import pdcb; pdb.set_trace()
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/boxcashier/window')
        return request.render('dia_process_todo.todo_tmpl_invoice_visor')

    @http.route(['/boxcashier/process/invoice'], type='http', auth='user')
    def boxcashier_process_invoice(self, invoice_id, user_name, type_req):
        # import pdb; pdb.set_trace()
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/boxcashier/window')
        if user_name == 'Desconocido':
            return json.dumps({'error': {}})
        invoice_id_obj = request.env['account.invoice'].sudo(request.session.uid).browse(int(invoice_id))
        print_invoice_obj = request.env['account.invoice.print'].sudo(request.session.uid)
        resp = {}

        if type_req == u'GET' or invoice_id_obj.print_ids:
            resp = {
                'data': {
                    'user_name': invoice_id_obj.print_ids[0].user_name if invoice_id_obj.print_ids else 'No impreso',
                    'print_date': invoice_id_obj.print_ids[0].print_date if invoice_id_obj.print_ids else 'No impreso',
                    'account_invoice_id': invoice_id_obj.id or 'false',
                    'account_invoice_print_id': invoice_id_obj.print_ids[0].id if invoice_id_obj.print_ids else 'No surtido',
                    # 'supplier_user': invoice_id_obj.print_ids[0].supplier_user.login if invoice_id_obj.print_ids else 'No surtido'
                }
            }
        elif type_req == u'POST' and not invoice_id_obj.print_ids:
            try:
                vals = {
                    'user_name': user_name,
                    'account_invoice_id': str(invoice_id_obj.id)
                }
                account_invoice_print = print_invoice_obj.sudo(request.session.uid).create(vals)
                resp = {
                    'data': {
                        'user_name': user_name,
                        'print_date': account_invoice_print.print_date,
                        'account_invoice_id': account_invoice_print.account_invoice_id.id,
                        'account_invoice_print_id': account_invoice_print.id
                    }
                }
            except Exception as e:
                return e
        return json.dumps(resp or {'error': {}})

    @http.route(['/boxcashier/get/invoice'], type='http', auth='user')
    def assortment_search_ticket(self, folio):
        # import pdb; pdb.set_trace()
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/boxcashier/window')
        invoice_obj = request.env['account.invoice'].sudo(request.session.uid)
        invoice_id = invoice_obj.search([('origin', '=', folio)])
        if invoice_id:
            resp = {
                'data': {
                    'id': invoice_id.id,
                    'doc': invoice_id.name,
                    'create_date': invoice_id.create_date,
                    'user_name': invoice_id.print_ids[0].user_name if invoice_id.print_ids else 'No impreso',
                    'print_date': invoice_id.print_ids[0].print_date if invoice_id.print_ids else 'No impreso',
                    'supplier_user': invoice_id.print_ids[0].supplier_user.login if invoice_id.print_ids else 'No surtido'
                }
            }
        return json.dumps(resp or {'error': {}})
