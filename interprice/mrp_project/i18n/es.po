# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * mrp_project
# 
# Translators:
# Hotellook, 2014
# oihane <oihanecruce@gmail.com>, 2016
# Pedro M. Baeza <pedro.baeza@gmail.com>, 2016
# Rudolf Schnapka <rs@techno-flex.de>, 2016
msgid ""
msgstr ""
"Project-Id-Version: manufacture (8.0)\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-11-16 09:04+0000\n"
"PO-Revision-Date: 2016-10-14 00:55+0000\n"
"Last-Translator: Pedro M. Baeza <pedro.baeza@gmail.com>\n"
"Language-Team: Spanish (http://www.transifex.com/oca/OCA-manufacture-8-0/language/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: mrp_project
#: code:addons/mrp_project/models/mrp_production.py:40
#, python-format
msgid ""
"\n"
"        Manufacturing Order: %s\n"
"        Product to Produce: [%s]%s\n"
"        Quantity to Produce: %s\n"
"        Bill of Material: %s\n"
"        Planned Date: %s\n"
"        "
msgstr "\nOrden de producción: %s\nProducto a fabricar: [%s]%s\nCantidad a fabricar: %s\nLista de materiales: %s\nFecha planificada: %s\n        "

#. module: mrp_project
#: view:account.analytic.line:mrp_project.account_analytic_line_mrp_search_view
msgid "Analytic Account"
msgstr "Cuenta analítica"

#. module: mrp_project
#: model:ir.model,name:mrp_project.model_account_analytic_line
msgid "Analytic Line"
msgstr "Línea analítica"

#. module: mrp_project
#: field:project.project,automatic_creation:0
msgid "Automatic Creation"
msgstr "Creación automática"

#. module: mrp_project
#: view:account.analytic.line:mrp_project.account_analytic_line_mrp_form_view
msgid "General Accounting"
msgstr "Contabilidad general"

#. module: mrp_project
#: view:account.analytic.line:mrp_project.account_analytic_line_mrp_form_view
#: view:project.task:mrp_project.project_task_mrp_form_view
msgid "MRP Data"
msgstr "Información de fabricación"

#. module: mrp_project
#: view:account.analytic.line:mrp_project.account_analytic_line_mrp_search_view
#: view:project.project:mrp_project.project_mrp_procurement_shortcut_form_view
#: view:project.task:mrp_project.project_task_mrp_inh_search_view
msgid "Manufacturing"
msgstr "Fabricación"

#. module: mrp_project
#: field:project.project,production_count:0
msgid "Manufacturing Count"
msgstr "Cuenta de OFs"

#. module: mrp_project
#: field:account.analytic.line,mrp_production_id:0
#: field:hr.analytic.timesheet,mrp_production_id:0
#: model:ir.model,name:mrp_project.model_mrp_production
#: field:project.task,mrp_production_id:0
msgid "Manufacturing Order"
msgstr "Orden de fabricación"

#. module: mrp_project
#: model:ir.actions.act_window,name:mrp_project.act_project_2_production_all
msgid "Manufacturing Orders"
msgstr "Órdenes de fabricación"

#. module: mrp_project
#: view:project.task:mrp_project.project_task_mrp_form_view
msgid "Please go to the manufacturing order to see the products to consume."
msgstr "Por favor vaya a la orden de fabricación para ver los productos a consumir."

#. module: mrp_project
#: view:project.task:mrp_project.project_task_mrp_form_view
#: view:project.task:mrp_project.project_task_mrp_inh_tree_view
#: field:project.task,final_product:0
msgid "Product to Produce"
msgstr "Producto a producir"

#. module: mrp_project
#: model:ir.model,name:mrp_project.model_project_project
#: field:mrp.production,project_id:0
msgid "Project"
msgstr "Proyecto"

#. module: mrp_project
#: field:account.analytic.line,task_id:0 field:hr.analytic.timesheet,task_id:0
msgid "Project Task"
msgstr "Tarea de proyecto"

#. module: mrp_project
#: model:ir.model,name:mrp_project.model_project_task_work
msgid "Project Task Work"
msgstr "Trabajo de tarea de proyecto"

#. module: mrp_project
#: view:project.task:mrp_project.project_task_mrp_form_view
#: field:project.task,production_scheduled_products:0
msgid "Scheduled Products"
msgstr "Productos planificados"

#. module: mrp_project
#: view:project.task:mrp_project.project_task_mrp_inh_search_view
msgid "Stage"
msgstr "Etapa"

#. module: mrp_project
#: model:ir.model,name:mrp_project.model_project_task
msgid "Task"
msgstr "Tarea"

#. module: mrp_project
#: field:mrp.production.workcenter.line,work_ids:0
msgid "Task works"
msgstr "Trabajos de tareas"

#. module: mrp_project
#: field:mrp.production.workcenter.line,task_ids:0
msgid "Tasks"
msgstr "Tareas"

#. module: mrp_project
#: model:ir.model,name:mrp_project.model_hr_analytic_timesheet
msgid "Timesheet Line"
msgstr "Línea de parte de horas"

#. module: mrp_project
#: field:account.analytic.line,workorder:0
#: field:hr.analytic.timesheet,workorder:0
#: model:ir.model,name:mrp_project.model_mrp_production_workcenter_line
#: field:project.task,workorder:0
msgid "Work Order"
msgstr "Orden de trabajo"

#. module: mrp_project
#: field:project.task.work,workorder:0
msgid "Work order"
msgstr "Orden de trabajo"

#. module: mrp_project
#: view:account.analytic.line:mrp_project.account_analytic_line_mrp_search_view
#: view:project.task:mrp_project.project_task_mrp_inh_search_view
msgid "Workorder"
msgstr "Orden de trabajo"

#. module: mrp_project
#: view:account.analytic.line:mrp_project.account_analytic_line_mrp_search_view
msgid "group_analytic_account"
msgstr "group_analytic_account"
