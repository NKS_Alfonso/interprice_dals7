# -*- coding: utf-8 -*-
{
    'name': 'Location record',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Warehouse',
    'summary': 'Adds a specific location of product in each warehouse.',
    'version': '1.0',
    'depends': ['stock', 'sale', 'product'],
    'application': True,
    'data': [
        'views/location_warehouse_view.xml',
        'views/product_template_view.xml',
        'data/default_battery_data.xml',
        'data/default_level_data.xml',
        'data/default_row_data.xml',
    ]
}
