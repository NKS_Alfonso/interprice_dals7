Ubicación de productos
======================

- **Ubicación del nuevo campo dentro de la pestaña Inventario.**

.. figure:: ../location_record/static/img/click_button_update_location.png
    :alt: Nuevo campo de ubicación
    :width: 80%

- **Ventana de ubicación del producto.**

.. figure:: ../location_record/static/img/location_records.png
    :alt: Nuevo campo de ubicación
    :width: 80%