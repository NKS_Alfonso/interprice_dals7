# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class ProductTemplate(models.Model):
    """Extended model to add an specific location of product in each warehouse."""

    # odoo model properties
    _inherit = "product.template"

    # custom fields
    warehouse_ids = fields.One2many(
        'location.warehouse',
        'product_template_id',
        help=_("Assigns the warehouses where this product will be."),
        require=False,
        readonly=False,
        string=_("Warehouses")
    )

    @api.model
    def create(self, vals):
        if 'warehouse_ids' in vals and not vals['warehouse_ids']:
            type_warehouse_id = self.env[
                'stock.warehouse.type'].search(
                [('code', '=', 'fac')])
            warehouse_ids = self.env[
                'stock.warehouse'].search(
                [('stock_warehouse_type_id', '=', type_warehouse_id.id)])
            for warehouse in warehouse_ids:
                vals['warehouse_ids'].append(
                    [0, False, {'warehouse_id': warehouse.id,
                                'location_record_ids': [
                                    [0, False, {
                                        'row_id': 1,
                                        'battery_id': 1,
                                        'Level_id': 1,
                                        'type': 'main'}
                                     ]
                                ]
                                }
                     ]
                )
        return super(ProductTemplate, self).create(vals)
