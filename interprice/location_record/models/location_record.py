# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class LocationRecord(models.Model):
    """Location of product in warehouse."""

    # odoo model properties
    _name = 'location.record'

    # selection field options
    list_types = [
        ('main', _('Main')),
        ('reserve', _('Reserve'))
    ]

    # custom fields
    location_warehouse_id = fields.Many2one(
        'location.warehouse',
        require=True,
        string=_("Warehouse"),
    )
    row_id = fields.Many2one(
        'location.row',
        help=_("Row number where the product is located."),
        require=True,
        string=_("Row"),
    )
    battery_id = fields.Many2one(
        'location.battery',
        help=_("Number battery where the product is located."),
        require=True,
        string=_("Battery"),
    )
    Level_id = fields.Many2one(
        'location.level',
        help=_("Level number where the product is located."),
        require=True,
        string=_("Level"),
    )
    type = fields.Selection(
        selection=list_types,
        help=_("Type of location. Only one main location type must exist."),
        require=True,
        string=_("Type"),
    )

    @api.multi
    def onchange_type(self, type):
        value = {}
        if type == 'reserve' and self.type == 'main':
            value = {'value': {'type': 'main'}}
        if type == 'main':
            exists_ids = self.search(
                [
                    ('location_warehouse_id', '=', self._context[
                        'current_location_warehouse_id']
                     )
                ]
            )
            if exists_ids:
                for location in self.browse(
                        [exists_id.id for exists_id in exists_ids]):
                    if location.type == 'main':
                        value = {'value': {'type': 'reserve'}}
                        break
        return value

    @api.multi
    def write(self, vals):
        if 'type' in vals:
            for data_id in self.search([
                ('location_warehouse_id', '=', vals.get(
                    'location_warehouse_id'
                )
                 ),
                ('type', '=', 'main')]
            ):
                if data_id and vals.get('type') == 'main':
                    raise Warning(
                        _('The Main location should be unique.')
                    )
        return super(LocationRecord, self).write(vals)

    @api.model
    def create(self, vals):
        if 'type' in vals and 'location_warehouse_id' in vals:
            for data_id in self.search([
                ('location_warehouse_id', '=', vals.get(
                    'location_warehouse_id'
                )
                 ),
                ('type', '=', 'main')]
            ):
                if vals.get('type') == 'main' and data_id:
                    raise Warning(
                        _('The Main location should be unique.')
                    )
        return super(LocationRecord, self).create(vals)

