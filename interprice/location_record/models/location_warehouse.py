# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class LocationWarehouse(models.Model):
    """Created model to add an specific location of product in each warehouse."""

    # odoo model properties
    _name = 'location.warehouse'
    _rec_name = 'warehouse_id'
    _inherit = ['mail.thread']
    _description = _("Location")

    # custom fields
    warehouse_id = fields.Many2one(
        'stock.warehouse',
        domain=lambda self: self._get_warehouse_domain(),
        help=_("Warehouse where this product be."),
        require=True,
        readonly=False,
        string=_("Warehouse")
    )
    product_template_id = fields.Many2one(
        'product.template',
        require=True,
        readonly=False,
        string=_("Product")
    )

    location_record_ids = fields.One2many(
        'location.record',
        'location_warehouse_id',
        help=_("Location inside warehouse."),
        require=True,
        string=_("Locations")
    )

    def open_location_record(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(
            cr, uid, 'location_record',
            'iaa_todo_action_window_location_record'
        )
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        inv_ids = []
        inv_ids += ids
        res = mod_obj.get_object_reference(
            cr, uid, 'location_record',
            'iuv_todo_view_location_record_form'
        )
        result['views'] = [(res and res[1] or False, 'form')]
        result['res_id'] = inv_ids and inv_ids[0] or False
        return result

    def onchange_warehouse_id(
            self, cr, uid, ids,
            warehouse_id, context=None):
        if not context:
            context = {}
        data = self.browse(cr, uid, ids,
                           context=context
                           )
        if warehouse_id and data.location_record_ids\
                and warehouse_id != data.warehouse_id.id:
            locations = ''
            for location in self.pool.get(
                    'location.record').browse(
                cr, uid, [l.id for l in data.location_record_ids],
                context=context
            ):
                locations += (
                    _('Row') + ': ' + str(
                        location.row_id.row
                    ) + '  ' +
                    _('Battery') + ': ' + str(
                        location.battery_id.battery
                    ) + '  ' +
                    _('Level') + ': ' + location.Level_id.level
                    + '  ' +
                    _('Type Location') + ': ' + (
                        _('Main') if location.type == 'main' else _(
                            'Reserve'
                        )
                    ) + '\n'
                )
            raise Warning(
                _('The following locations will be updated '
                  'to the new assigned warehouse.') + '\n',
                (locations + '\n' + _(
                    'If you do not want to update '
                    'the store only discard the changes.')
                 )
            )
        type_warehouse_id = self.pool.get(
            'stock.warehouse.type').search(
            cr, uid, [('code', '=', 'fac')],
            context=context
        )
        value = {
            'domain': {'warehouse_id': [
                ('stock_warehouse_type_id', 'in', type_warehouse_id)
            ]
            }
        }
        return value

    @api.multi
    def unlink(self):
        for record in self.location_record_ids:
            if record:
                raise Warning(
                    _('You cannot delete an warehouse which is have '
                      'location of this product. '
                      'You should first delete the location records.')
                )
        return super(LocationWarehouse, self).unlink()

    @api.multi
    def write(self, vals):
        if 'warehouse_id' in vals:
            for data_id in self.search(
                    [
                    ('product_template_id', '=', self.product_template_id.id),
                    ('warehouse_id', '=', vals.get('warehouse_id'))
                    ]
            ):
                if data_id and self.product_template_id.id != vals.get(
                        'warehouse_id'):
                    raise Warning(
                        _('You can not assign the same store more than once to the product.')
                    )
        exits_main = 0
        record_count = 0
        for location_id in self.location_record_ids:
            record_count += 1
            if location_id.type == 'main':
                exits_main += 1
        if 'location_record_ids' in vals:
            delete_ids = []
            for record in vals['location_record_ids']:
                action = record[0]
                id = record[1]
                data = record[2]
                body_message = ""
                message_subject = ""
                if action in [0]:
                    if 'row_id' in data and data['row_id']:
                        body_message += _("Row") + ":" + (
                            str(self.env['location.row'].browse(
                                data['row_id']).row)) + " - "
                    if 'battery_id' in data and data['battery_id']:
                        body_message += _("Battery") + ":" + (
                            str(self.env['location.battery'].browse(
                                data['battery_id']).battery)) + " - "
                    if 'Level_id' in data and data['Level_id']:
                        body_message += _("Level") + ":" + (
                            self.env['location.level'].browse(
                                data['Level_id']).level) + " - "
                    if 'type' in data and data['type']:
                        body_message += _("Type Location") + ":" + \
                                        (_("Main") if data['type'] == 'main' else
                                         _("Reserve")) + " "
                    message_subject += _("Location created")
                if action in [1]:
                    record_data = self.env['location.record'].browse(id)
                    if 'row_id' in data and data['row_id']:
                        body_message += _("Row") + " " + str(
                            record_data.row_id.row) + " --> " + (
                            str(self.env['location.row'].browse(
                                data['row_id']).row)) + " "
                    if 'battery_id' in data and data['battery_id']:
                        body_message += _("Battery") + " " + str(
                            record_data.battery_id.battery) + " --> " + (
                            str(self.env['location.battery'].browse(
                                data['battery_id']).battery)) + " "
                    if 'Level_id' in data and data['Level_id']:
                        body_message += _("Level") + " " + \
                                        record_data.Level_id.level + " --> " + (
                            self.env['location.level'].browse(
                                data['Level_id']).level) + " "
                    if 'type' in data and data['type']:
                        body_message += _("Type Location") + (
                            _("Main") if record_data.type == 'main' else
                            _("Reserve")) + " --> " + \
                                        (_("Main") if data['type'] == 'main' else
                                         _("Reserve")) + " "
                    else:
                        body_message += _("Type Location") + ": " + (
                            _("Main") if record_data.type == 'main' else
                            _("Reserve")) + " "
                    message_subject += _("Location updated")
                if action in [2]:
                    record_data = self.env['location.record'].browse(id)
                    body_message += _("Row") + ":" + str(
                        record_data.row_id.row) + " - "
                    body_message += _("Battery") + ":" + str(
                        record_data.battery_id.battery) + " - "
                    body_message += _("Level") + ":" + \
                        record_data.Level_id.level + " - "
                    body_message += _("Type Location") + ":" + \
                        (_("Main") if record_data.type == 'main' else
                         _("Reserve")) + " "
                    message_subject += _("Location deleted")
                    delete_ids.append(id)
                if action in [0, 1, 2]:
                    self.message_post(subject=message_subject,
                                      body=body_message,
                                      type="notification",
                                      context=None)
            if record_count != len(delete_ids):
                ifToDelte = self.env['location.record'].search([('id', 'in', delete_ids), ('type', '=', 'main')])
                if ifToDelte:
                    raise Warning(
                        _('You can not delete the Main location when you have Reserve locations.')
                    )
        if 'warehouse_id' in vals:
            message_subject = _("Warehouse updated")
            body_message = "" + (self.warehouse_id.name or '') + " --> " + \
                           self.env['stock.warehouse'].browse(
                               vals['warehouse_id']).name + ""
            self.message_post(subject=message_subject,
                              body=body_message,
                              type="notification",
                              context=None)
        if not exits_main:
            raise_val = True
            if 'location_record_ids' in vals:
                for location in vals['location_record_ids']:
                    if location[2]:
                        raise_val = False
                        if location[2]['type'] == 'main':
                            raise_val = True
                            break
                if not raise_val:
                    raise Warning(
                        _('A type of location must be Main.')
                    )
        return super(LocationWarehouse, self).write(vals)

    @api.model
    def create(self, vals):
        if 'product_template_id' in vals and 'warehouse_id' in vals:
            for data_id in self.search(
                [
                    ('product_template_id', '=', vals.get('product_template_id')),
                    ('warehouse_id', '=', vals.get('warehouse_id'))
                ]
            ):
                if data_id:
                    raise Warning(
                        _('You can not assign the same store more than once to the product.')
                    )
        return super(LocationWarehouse, self).create(vals)

    @api.model
    def _get_warehouse_domain(self):
        domain = []
        type_warehouse_id = self.env['stock.warehouse.type'].search(
            [('code', '=', 'fac')])
        if type_warehouse_id:
            domain = [
                ('stock_warehouse_type_id', 'in', [
                    tp.id for tp in type_warehouse_id
                    ]
                 )
            ]
        return domain


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    @api.one
    def unlink(self):
        products = self.env['product.template'].search([('active', '=', True)])
        for product in products:
            for warehouse_id in product.warehouse_ids:
                if self.id == warehouse_id.warehouse_id.id:
                    raise Warning(_("Warehouse is used in the products location.\n") + \
                                  _('For unlink is necessary delete all relations.'))
        return super(StockWarehouse, self).unlink()