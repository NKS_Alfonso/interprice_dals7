# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, _


class LocationBattery(models.Model):
    """Extended model to add battery number to location of product"""

    # odoo model properties
    _name = 'location.battery'
    _rec_name = 'battery'
    _sql_constraints = [
        ('battery_unique', 'unique(battery)',
         _("Can't be duplicate value for this field!"))
    ]

    # custom fields
    battery = fields.Integer(
        require=False,
        readonly=False,
        string=_("Battery")
    )
