# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, _


class LocationRow(models.Model):
    """Extended model to add row number to location of product"""

    # odoo model properties
    _name = 'location.row'
    _rec_name = 'row'
    _sql_constraints = [
        ('row_unique', 'unique(row)',
         _("Can't be duplicate value for this field!"))
    ]

    # custom fields
    row = fields.Integer(
        require=False,
        readonly=False,
        string=_("Row")
    )
