# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, _


class LocationLevel(models.Model):
    """Extended model to add level number to location of product"""

    # odoo model properties
    _name = 'location.level'
    _rec_name = 'level'
    _sql_constraints = [
        ('level_unique', 'unique(level)',
         _("Can't be duplicate value for this field!"))
    ]

    # custom fields
    level = fields.Char(
        help=_("Delivery carrier condition priority"),
        require=False,
        readonly=False,
        string=_("Level"),
    )
