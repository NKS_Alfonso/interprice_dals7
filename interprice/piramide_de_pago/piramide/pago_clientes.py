# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models
from openerp.osv import fields as fields7


class PagosDeClientes(models.Model):
    _inherit = 'account.voucher'
    _columns = {
        'partner_id': fields7.many2one('res.partner',
                                       'Partner',
                                       change_default=1,
                                       readonly=True,
                                       states={'draft': [('readonly', False)]})
    }

    def _get_partner(self, cr, uid, context=None):
        res = super(PagosDeClientes, self)._get_partner(cr, uid, context)
        return res

    _defaults = {
        'journal_id': _get_partner,
    }


class DiariosConCentrosCostos(models.Model):
    _inherit = 'account.journal'

    centro_costos = fields.Many2many('account.cost.center',
                                     string='Centro de Costos')


# class CamposNuevosAccountVoucherLine(models.Model):
#     _inherit = 'account.voucher.line'
#
#     moneda = fields.Char(string='Moneda')
#     importe_original = fields.Float(string='Importe Original')
#     saldo_actual = fields.Float(string='Saldo Actual')
#     importe = fields.Float(string='Importe')
#     saldo = fields.Float(string='Saldo')
