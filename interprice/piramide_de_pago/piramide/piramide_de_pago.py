# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, api, models,_
from openerp.osv import fields as fields7
from openerp.exceptions import Warning


class PiramideDePagos(models.Model):
    _name = 'piramide_de_pago'
    _order = 'str_nombre'

    _rec_name = 'str_nombre'

    str_nombre = fields.Char(string="Nombre", required=True)
    i_prioridad = fields.Integer(string="Prioridad", required=True)

    b_es_prenda = fields.Boolean(string="Es Prenda", required=False)
    b_activo = fields.Boolean(string="Activo", required=False)

    diarios = fields.Many2many('account.journal',
                               string='Diarios',
                               domain=[('type', 'in', ['bank', 'cash'])])


class Partner(models.Model):
    _inherit = 'res.partner'

    forma_pago = fields.Many2one('piramide_de_pago',
                                 string=_('Payment type'),
                                 domain=[
                                    '&',
                                    ('b_es_prenda', '=', True),
                                    ('b_activo', '=', True)
                                    ],
                                 ondelete='cascade')


class OrdenesDeServicio(models.Model):
    _inherit = 'sale.order'
    # _name = 'ordenes_servicio'
    forma_pago = fields.Many2one('piramide_de_pago',
                                 string=_('Payment type'),
                                 required=True,
                                 ondelete='cascade')

    @api.multi
    def onchange_partner_id(self, partner_id):
        res = super(OrdenesDeServicio, self).onchange_partner_id(partner_id)

        partner = self.env['res.partner'].browse(partner_id)
        prioridad = partner.forma_pago.i_prioridad

        dominio = {'forma_pago': [
                                '&',
                                ('i_prioridad', '<=', prioridad),
                                '&',
                                ('b_activo', '=', True),
                                ('b_es_prenda', '=', True)]}

        if 'domain' in res:
            res['domain'].update(dominio)
        else:
            res['domain'] = dominio

        return res


class Factura(models.Model):
    _inherit = 'account.invoice'

    forma_pago = fields.Many2one('piramide_de_pago',
                                 string=_('Payment type'),
                                 domain="['&', ('i_prioridad', '<=', forma_pago),"
                                 "'&', ('b_activo', '=', True),"
                                 "('b_es_prenda', '=', True)]",
                                 required=False,
                                 ondelete='cascade')

    @api.multi
    def onchange_partner_id(self,
                            type,
                            partner_id,
                            date_invoice=False,
                            payment_term=False,
                            partner_bank_id=False,
                            company_id=False):

        res = super(Factura, self).onchange_partner_id(type,
                                                       partner_id,
                                                       date_invoice,
                                                       payment_term,
                                                       partner_bank_id,
                                                       company_id)

        partner = self.env['res.partner'].browse(partner_id)
        prioridad = partner.forma_pago.i_prioridad

        dominio = {'forma_pago': ['&',
                                  ('i_prioridad', '<=', prioridad),
                                  '&',
                                  ('b_activo', '=', True),
                                  ('b_es_prenda', '=', True)]}

        if 'domain' in res:
            res['domain'].update(dominio)
        else:
            res['domain'] = dominio

        return res


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def _get_invoice_vals(self,
                          cr,
                          uid,
                          key,
                          inv_type,
                          journal_id,
                          move,
                          context=None):

        if context is None:
            context = {}
        partner, currency_id, company_id, user_id = key

        res = super(StockPicking, self)._get_invoice_vals(cr,
                                                          uid,
                                                          key,
                                                          inv_type,
                                                          journal_id,
                                                          move,
                                                          context=context)
        if not partner:
          raise Warning(_('There is no customer assigned to create the accounting document'))
        if inv_type in ('out_invoice', 'out_refund'):
            if 'forma_pago' not in res:
                res['forma_pago'] = partner.forma_pago.id,
            else:
                res.update({'forma_pago': partner.forma_pago.id})

            if 'cost_center_id' not in res:
                res['cost_center_id'] = move.picking_id.picking_type_id.\
                                        warehouse_id.centro_costo_id.id
            else:
                res.update({'cost_center_id': move.picking_id.picking_type_id.
                            warehouse_id.centro_costo_id.id})

        return res


class PagosClientes(models.Model):
    _inherit = 'account.voucher'
    _columns = {
        'journal_id': fields7.many2one('account.journal',
                                       'Journal',
                                       required=True,
                                       readonly=True,
                                       states={'draft': [('readonly', False)]},
                                       domain=[('id', 'in', []), '&'])
    }

    def agregar_moneda_saldos(self,
                              cr,
                              uid,
                              tabla_cr_dr,
                              rate,
                              currency_id,
                              context=None):

        # tabla_cr_dr[0]['moneda'] = 'MXN'
        lista_resultado = []
        ids = []
        names = []

        if rate == 0 or rate is None:
            rate = 1

        for linea in tabla_cr_dr:
            ids.append(linea['move_line_id'])
            names.append(linea['name'])

        obj_move_line = self.pool.get(
                                      'account.move.line'
                                      ).browse(cr,
                                               uid,
                                               ids,
                                               context=context)

        # Revizamos si el documento es una Factura
        account_invoice_id = self.pool.get(
                                           'account.invoice'
                                           ).search(cr,
                                                    uid,
                                                    [('number', 'in', names)],
                                                    context=context)
        # Revizamos si el documento es una anticipo
        account_voucher_id = self.pool.get(
                                           'account.voucher'
                                           ).search(cr,
                                                    uid,
                                                    [('number', 'in', names)],
                                                    context=context)

        # Para Documentos de tipo Factura
        obj_account_invoice = self.pool.get(
                                            'account.invoice'
                                            ).browse(cr,
                                                     uid,
                                                     account_invoice_id,
                                                     context=context)

        # Para Documentos de tipo anticipo cliente, (pagos clientes)
        obj_account_voucher = self.pool.get(
                                            'account.voucher'
                                            ).browse(cr,
                                                     uid,
                                                     account_voucher_id,
                                                     context=context)

        # Obtenemos un objeto de la moneda
        obj_currency = self.pool.get(
                                     'res.currency'
                                     ).browse(cr,
                                              uid,
                                              currency_id,
                                              context=context)

        # Falta agregar condiciones para los documentos pendientes que faltan
        # por agregar, de momento solo funciona para facturas y anticipos de
        # clientes.

        for linea in tabla_cr_dr:
            dict_temporal = {}
            importe_original = 0
            saldo_actual = 0
            moneda = ""

            for obj_invoice in obj_account_invoice:
                if linea['name'] == obj_invoice.number:
                    importe_original = obj_invoice.amount_total
                    saldo_actual = obj_invoice.residual
                    moneda = obj_invoice.currency_id.name

            for obj_invoice in obj_account_voucher:
                if linea['name'] == obj_invoice.number:
                    importe_original = obj_invoice.net_amount
                    saldo_actual = obj_invoice.writeoff_amount
                    moneda = obj_invoice.currency_id.name

            for move_line in obj_move_line:
                if linea['move_line_id'] == move_line.id:
                    amount_residual = move_line.amount_residual
                    amount_residual_currency = move_line.amount_residual_currency
                    amount_currency_name = move_line.currency_id.name

            if obj_currency.id == linea['currency_id']:
                importe = importe_original
                if amount_currency_name == 'USD':
                    saldo = amount_residual_currency
                else:
                    saldo = amount_residual
            elif obj_currency.name == 'MXN':  # Pesos
                importe = importe_original * rate
                saldo = amount_residual_currency * rate
            else:
                importe = importe_original / rate
                saldo = amount_residual / rate

            dict_temporal.update(linea)
            dict_temporal['importe_original'] = importe_original
            dict_temporal['saldo_actual'] = saldo_actual
            dict_temporal['moneda'] = moneda
            dict_temporal['importe'] = importe
            dict_temporal['saldo'] = saldo

            lista_resultado.append(dict_temporal)
        return lista_resultado

    def onchange_partner_id(self,
                            cr,
                            uid,
                            ids,
                            partner_id,
                            journal_id,
                            amount,
                            currency_id,
                            ttype,
                            date,
                            context=None):
        res = {}
        diarios_ids = []
        _domain = None
        _value = None
        _journal_to_super = journal_id

        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            obj_user = self.pool.get('res.users').browse(cr, uid, uid, context=context)

            journals_ids = self.pool.get('account.journal').search(cr, uid, [('type', 'in', ['bank', 'cash'])],
                                                                   context=None)

            obj_journals = self.pool.get('account.journal').browse(cr, uid, journals_ids, context=None)

            arr_diarios_centro_costo = []

            # Recorremos obj_journals para obtener los diarios que tienen
            # el/los centros de costo del usuario logeado
            for journals in obj_journals:
                for journal in journals.centro_costos:
                    if journal.id in obj_user.centro_costo_id.ids and \
                            journals.id not in arr_diarios_centro_costo:

                        arr_diarios_centro_costo.append(journals.id)

            if ttype == 'receipt':  # cliente
                if 'clicked_onchange_journal' not in context or  not context['clicked_onchange_journal']:

                    prioridad = partner.forma_pago.i_prioridad

                    piramides_ids = self.pool.get('piramide_de_pago').search(cr, uid,
                                                                             ['&', ('i_prioridad', '<=', prioridad),
                                                                              ('b_activo', '=', True)], context=None)

                    obj_piramide = self.pool.get('piramide_de_pago').browse(cr, uid, piramides_ids, context=None)

                    for piramide in obj_piramide:
                        for diario in piramide.diarios:
                            # Aqui agregaremos la condicion de que solo
                            # añadiremos diarios que existan para el cliente
                            if diario.id in arr_diarios_centro_costo:
                                diarios_ids.append(diario.id)

                    if len(diarios_ids) > 0:
                        if not journal_id or not journal_id in diarios_ids:
                            journal_id = diarios_ids[0]
                    else:
                        journal_id = False

                    _domain = {'journal_id': [('id', 'in', diarios_ids)]}
                    # _value = {'journal_id': journal_id}

            else:  # proveedor
                if 'clicked_onchange_journal' not in context or not context['clicked_onchange_journal']:
                    if len(arr_diarios_centro_costo) > 0:
                        if not journal_id or not journal_id in arr_diarios_centro_costo:
                            journal_id = arr_diarios_centro_costo[0]
                        diarios_ids = arr_diarios_centro_costo
                    else:
                        journal_id = False
                        diarios_ids = []

                    _domain = {'journal_id': [('id', 'in', diarios_ids)]}
                    # _value = {'journal_id': journal_id}

            if _journal_to_super:
                res = super(PagosClientes, self). \
                    onchange_partner_id(cr, uid, ids, partner_id, journal_id, amount,
                                        currency_id, ttype, date, context=context)
            else:
                res = {'value': {}, 'domain': {}}

            if not _journal_to_super:
                if type(res) == dict and 'value' in res:
                    res['value'].update(
                        {'account_id': False,
                         'line_dr_ids': [],
                         'line_cr_ids': []
                         })
            else:
                # rate = res['value']['payment_rate']
                # res = {'value': {'line_cr_ids': self.agregar_moneda_saldos(cr, uid, res['value']['line_cr_ids'], rate, currency_id, context),
                #                  'line_dr_ids': self.agregar_moneda_saldos(cr, uid, res['value']['line_dr_ids'], rate, currency_id, context)}}
                # if type(res) == dict and 'value' in res:
                #     res['value'].update(
                #         {'line_cr_ids': res['value']['line_cr_ids'],
                #          'line_dr_ids': res['value']['line_dr_ids']
                #          })
                _account_id = self.basic_onchange_partner(cr, uid, ids, partner_id, journal_id, ttype, context=None)
                account_id = _account_id.get('value').get('account_id', False)
                if 'value' in res:
                    res['value'].update({'account_id': account_id})

            if _domain:
                if 'domain' in res:
                    res['domain'].update(_domain)
                else:
                    res['domain'] = _domain

            if _value:
                if 'value' in res:
                    res['value'].update(_value)
                else:
                    res['value'] = _value
        else:  # is not selected any partner
            res['domain'] = {'journal_id': [('id', 'in', diarios_ids)]}
        if isinstance(res,dict):
            if 'value' in res:
                if 'payment_rate' in res['value']:
                    del res['value']['payment_rate']
                if 'payment_rate_currency_id' in res['value']:
                    del res['value']['payment_rate_currency_id']
                res['value']['is_multi_currency'] = True
        # import pdb; pdb.set_trace()
        return res

    def onchange_amount(self, cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date, payment_rate_currency_id, company_id, context=None):
        res = super(PagosClientes, self). \
            onchange_amount(cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date, payment_rate_currency_id, company_id, context)
        if 'value' not in res:
            return res
        # if len(res['value']['line_cr_ids']) > 0:
        #     res['value']['line_cr_ids'] = self.agregar_moneda_saldos(cr, uid, res['value']['line_cr_ids'], rate, currency_id, context)
        # if len(res['value']['line_cr_ids']) > 0:
        #     res['value']['line_dr_ids'] = self.agregar_moneda_saldos(cr, uid, res['value']['line_dr_ids'], rate, currency_id, context)

        return res

    def onchange_payment_rate_currency(self, cr, uid, ids, currency_id, payment_rate, payment_rate_currency_id, date, amount, company_id, context=None):
        res = super(PagosClientes, self). \
            onchange_payment_rate_currency(cr, uid, ids, currency_id, payment_rate, payment_rate_currency_id, date, amount, company_id, context)
        return res

    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id,
                         context=None):
        if context:
            context.clicked_onchange_journal = True
        res = super(PagosClientes, self).onchange_journal(cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date,
                                                          amount, ttype, company_id, context)
        return res

    def _get_journal(self, cr, uid, context=None):
        res = super(PagosClientes, self)._get_journal(cr, uid, context)
        return None

    _defaults = {
        'journal_id': _get_journal,
    }
