# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
{
    'name': "Piramide de pago",

    'summary': """
        Modulo de Piramide de Pagos""",

    'description': """
        Modulo para la piramide de pagos
    """,

    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Contabilidad',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'sale', 'l10n_mx_payment_method',
                'alternate_type_of_change',  'l10n_mx_partner_address'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'piramide/piramide_de_pago_view.xml',
        'piramide/pago_clientes_view.xml',
    ],
}