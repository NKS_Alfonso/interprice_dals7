# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
import time
import datetime
import locale
import pytz
from olib.oCsv.oCsv import OCsv
from datetime import datetime
from openerp.osv import osv
from openerp import models, fields, api, exceptions, _
from openerp.exceptions import ValidationError
import re
# from .account_cost_center import name, get_state

class Price_list_report(models.TransientModel):
    # _inherit = 'product_template'
    _name = 'price.list.report'

    cost_center = fields.Many2one('account.cost.center', sting='Cost Center', help="Si se deja en blanco este dato, el reporte se generará con todas las sucursales")
    section = fields.Many2one('product.category', string='Section',required=True)
    line = fields.Many2one('product.category', string='Line',required=True)
    brand = fields.Many2one('product.category', string='Brand',required=True)
    series = fields.Many2one('product.category', string='Serie',required=True)
    state = fields.Selection([('0', 'Reporte de lista de precios'),('1', 'Reporte de lista de precios completo')], required=True)

    @api.multi
    def export_csv(self, cr, context=None):
        cost_centers = self.env['account.cost.center'].search([('id','>=',0)])
        if self.state == '0':
            if self.env['res.users'].has_group('price_list_report.report_price_list') or self.env['res.users'].has_group('base.group_system'):
                if self.cost_center:
                    header = [[_("CODIGO"),# Codigo interno posicion 1
                               _("DESCRIPCION"),# Descripcion del producto posicion 2
                               _("SECCION"),# Seccion del producto posicion 3
                               _("LINEA"),# Linea del producto posicion 4
                               _("MARCA"),#  Marca del producto posicion 5
                               _("SERIE"),# Serie del producto posicion 5
                               _("EXISTENCIAS ") + self.cost_center.name,# Existencias de los productos posicion 6
                               _("APARTADOS ") + self.cost_center.name,# Apartados de los productos posicion 7
                               _("TOTAL DE PIEZAS ") + self.cost_center.name,# Total de piezas posicion 8
                               _("TOTAL EXISTENCIAS"),# Total de existencias posicion 9
                               _("TOTAL APARTADOS"),# Total de apartados posicion 10
                               _("TOTAL PIEZAS"),# total de piezas posicion 11
                               _("PRECIO 1"),# precio 1 posicion 12
                               _("PRECIO 2"),# precio 2 poscion 13
                               _("PRECIO 3"),# precio 3 posicion 14
                               _("PRECIO 4"),# precio 4 posicon 15
                               _("PRECIO 5"),# precio 5 posicion 16
                               _("PRECIO 6"),# precio 5 posicion 16
                               _("PRECIO 7"),# precio 5 posicion 16
                               _("PRECIO 8"),# precio 5 posicion 16
                               _("PRECIO 9"),# precio 5 posicion 16
                               _("PRECIO 10"),# precio 5 posicion 16
                               _("MONEDA"),#  Tipo de moneda posicion 17
                               _("UPC"),#  codigo upc posicion 18
                               _("GARANTIA"),# Garantia posicion 19
                            ]]
                    rows = header
                else:
                    header = []
                    header.extend([_("CODIGO")])# Codigo interno posicion 1
                    header.extend([_("DESCRIPCION")])# Descripcion del producto posicion 2
                    header.extend([_("SECCION")])# Seccion del producto posicion 3
                    header.extend([_("LINEA")])# Linea del producto posicion 4
                    header.extend([_("MARCA")])#  Marca del producto posicion 5
                    header.extend([_("SERIE")])# Serie del producto posicion 5
                    for center in cost_centers:
                        header.extend([_("EXISTENCIAS ") + center.name])# Existencias de los productos posicion 6
                        header.extend([_("APARTADOS ") + center.name])# Apartados de los productos posicion 7
                        header.extend([_("TOTAL DE PIEZAS ") + center.name])# Total de piezas posicion 8]
                    header.extend([_("TOTAL EXISTENCIAS")])# Total de existencias posicion 9
                    header.extend([_("TOTAL APARTADOS")])# Total de apartados posicion 10
                    header.extend([_("TOTAL PIEZAS")])# total de piezas posicion 11
                    header.extend([_("PRECIO 1")])# precio 1 posicion 12
                    header.extend([_("PRECIO 2")])# precio 2 poscion 13
                    header.extend([_("PRECIO 3")])# precio 3 posicion 14
                    header.extend([_("PRECIO 4")])# precio 4 posicon 15
                    header.extend([_("PRECIO 5")])# precio 5 posicion 16
                    header.extend([_("PRECIO 6")])# precio 5 posicion 16
                    header.extend([_("PRECIO 7")])# precio 5 posicion 16
                    header.extend([_("PRECIO 8")])# precio 5 posicion 16
                    header.extend([_("PRECIO 9")])# precio 5 posicion 16
                    header.extend([_("PRECIO 10")])# precio 5 posicion 16
                    header.extend([_("MONEDA")])#  Tipo de moneda posicion 17
                    header.extend([_("UPC")])#  codigo upc posicion 18
                    header.extend([_("GARANTIA")])# Garantia posicion 19
                    rows = [header]
                # Se hace el query para obtener los datos dependiendo de las variantes seleccionadas en el filtro
                cadquery = """
                            CREATE TEMPORARY TABLE rpl_csv AS (
                                    SELECT pp.id, pt.id as product_tmpl_id, pp.default_code, upper(pp.name_template) as name_template, upper(ts.name) as section, upper(tl.name) as line, upper(tm.name) as brand, upper(tser.name) as serie, """
                if self.cost_center:
                    cadquery += """
                            0 as exis"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""", 0 as apart"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""", 0 as tot"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""",
                    """
                else:
                    for center in cost_centers:
                        cadquery += """
                            0 as exis"""+re.sub('[^A-Za-z0-9]+','',center.name)+""", 0 as apart"""+re.sub('[^A-Za-z0-9]+','',center.name)+""", 0 as tot"""+re.sub('[^A-Za-z0-9]+','',center.name)+""",
                    """
                cadquery += """ 0 as exist_total, 0 as apart_total, 0 as tot_total,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=1),0) as level1,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=2),0) as level2,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=3),0) as level3,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=4),0) as level4,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=5),0) as level5,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=6),0) as level6,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=7),0) as level7,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=8),0) as level8,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=9),0) as level9,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=10),0) as level10,
                                rc.name, pp.ean13, upper(cg.garantia_en_catalogo) as garantia_en_catalogo"""
                if self.cost_center:
                    cadquery += ", sl.id as location_id "
                cadquery += """
                                FROM product_product pp
                                LEFT OUTER JOIN product_template pt on pt.id=pp.product_tmpl_id
                                LEFT OUTER JOIN product_category ts on ts.id=pt.section
                                LEFT OUTER JOIN product_category tl on tl.id=pt.line
                                LEFT OUTER JOIN product_category tm on tm.id=pt.brand
                                LEFT OUTER JOIN product_category tser on tser.id=pt.serial
                                LEFT OUTER JOIN res_currency rc on rc.id=pt.list_price_currency
                                LEFT OUTER JOIN catalogo_garantias cg on cg.id=pt.garantia
                                LEFT OUTER JOIN stock_quant sq on sq.product_id=pp.id
                                LEFT OUTER JOIN stock_location sl on sl.id=sq.location_id
                                LEFT OUTER JOIN stock_warehouse sw on sw.lot_stock_id=sl.id
                                INNER JOIN stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id """
                if self.cost_center:
                    cadquery += """WHERE pp.active=true and pt.sale_ok=true and sw.centro_costo_id="""+str(self.cost_center.id)+""" AND swt.code='fac' AND sl.usage='internal' """
                else:
                    cadquery += """WHERE pp.active=true and pt.sale_ok=true and swt.code='fac' AND sl.usage='internal' """
                if self.section.id!=1:
                    cadquery += """AND pt.section="""+str(self.section.id)+""" """
                if self.line.id!=1:
                    cadquery += """AND pt.line="""+str(self.line.id)+""" """
                if self.brand.id!=1:
                    cadquery += """AND pt.brand="""+str(self.brand.id)+""" """
                if self.series.id!=1:
                    cadquery += """AND pt.serial="""+str(self.series.id)+""" """
                if self.cost_center:
                    cadquery += """GROUP BY pp.id,ts.name,tl.name,tm.name,tser.name,rc.name,pp.ean13,cg.garantia_en_catalogo,sl.id, pt.id); """
                else:
                    cadquery += """GROUP BY pp.id,ts.name,tl.name,tm.name,tser.name,rc.name,pp.ean13,cg.garantia_en_catalogo, pt.id); """
                if self.cost_center:
                    cadquery += "UPDATE rpl_csv set exis"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=COALESCE((SELECT sum(qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.location_id=rpl_csv.location_id and sq.reservation_id isnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set apart"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=COALESCE((SELECT sum(qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.location_id=rpl_csv.location_id and sq.reservation_id notnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set tot"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=exis"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"+apart"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"; "
                    cadquery += "UPDATE rpl_csv set exist_total=exis"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+", apart_total=apart"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+", tot_total=tot"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"; "
                else:
                    for center in cost_centers:
                        cadquery += "UPDATE rpl_csv set exis"+re.sub('[^A-Za-z0-9]+','',center.name)+"=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sq.location_id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sw.centro_costo_id="+str(center.id)+" and sq.reservation_id isnull and swt.code='fac' and sl.usage='internal'),0); "
                        cadquery += "UPDATE rpl_csv set apart"+re.sub('[^A-Za-z0-9]+','',center.name)+"=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sq.location_id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sw.centro_costo_id="+str(center.id)+" and sq.reservation_id notnull and swt.code='fac' and sl.usage='internal'),0); "
                        cadquery += "UPDATE rpl_csv set tot"+re.sub('[^A-Za-z0-9]+','',center.name)+"=exis"+re.sub('[^A-Za-z0-9]+','',center.name)+"+apart"+re.sub('[^A-Za-z0-9]+','',center.name)+"; "
                    cadquery += "UPDATE rpl_csv set exist_total=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.reservation_id isnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set apart_total=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.reservation_id notnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set tot_total=exist_total+apart_total;"
                cadquery += "SELECT * from rpl_csv order by section, line, brand, serie, default_code;"
                self.env.cr.execute(cadquery)
                query_set = self.env.cr.fetchall()
                # Fin de las consultas    
                for row in query_set:
                    line = []
                    for i, val in enumerate(row):
                        if i>=2:
                            if self.cost_center:
                                if i<len(row)-1:
                                    if val!=None:
                                        line.extend([str(val).encode("utf-8").decode("utf-8")])
                                    else:
                                        line.extend([''])
                            else:
                                if val!=None:
                                    line.extend([str(val).encode("utf-8").decode("utf-8")])
                                else:
                                    line.extend([''])
                    rows.append(line)

                obj_osv = OCsv()
                download_id = self.pool['r.download'].create(cr=self.env.cr, uid=self.env.uid, vals={
                    'file_name': _(
                        "Reporte lista de precios.csv" ),
                    'type_file': 'csv',
                    'file': obj_osv.csv_base64(rows),
                })
                return {
                    'type': 'ir.actions.act_url',
                    'url': '/web/binary/download/file?id=%s' % download_id,
                    'target': 'self',
                }
            else:
                raise ValidationError('No tiene permiso para ejecutar este reporte')
        if self.state == '1':
            if self.env['res.users'].has_group('price_list_report.report_price_list_full') or self.env['res.users'].has_group('base.group_system'):
                #if self.section.id == 1 and self.line.id == 1 and self.brand.id == 1 and self.series.id == 1:
                if self.cost_center:
                    header = [[_("CODIGO"),
                               _("DESCRIPCION"),
                               _("SECCION"),
                               _("LINEA"),
                               _("MARCA"),
                               _("SERIE"),
                               _("EXISTENCIAS ") + self.cost_center.name,
                               _("APARTADOS ") + self.cost_center.name,
                               _("TOTAL DE PIEZAS ") + self.cost_center.name,
                               _("TOTAL COSTO") + self.cost_center.name,
                               _("TOTAL EXISTENCIAS"),
                               _("TOTAL APARTADOS "),
                               _("TOTAL DE PIEZAS"),
                               _("TOTAL COSTO"),
                               _("COSTO ULTIMA COMPRA"),
                               _("COSTO PROMEDIO"),
                               _("COSTO REPOSICION"),
                               _("PRECIO 1"),
                               _("PRECIO 2"),
                               _("PRECIO 3"),
                               _("PRECIO 4"),
                               _("PRECIO 5"),
                               _("PRECIO 6"),
                               _("PRECIO 7"),
                               _("PRECIO 8"),
                               _("PRECIO 9"),
                               _("PRECIO 10"),
                               _("MONEDA"),
                               _("UPC"),
                               _("GARANTIA"),
                            ]]
                    rows = header
                else:                    
                    header = []
                    header.extend([_("CODIGO")])# Codigo interno
                    header.extend([_("DESCRIPCION")])# Descripcion del producto
                    header.extend([_("SECCION")])# Seccion del producto
                    header.extend([_("LINEA")])# Linea del producto
                    header.extend([_("MARCA")])#  Marca del producto
                    header.extend([_("SERIE")])# Serie del producto
                    for center in cost_centers:
                        header.extend([_("EXISTENCIAS ") + center.name])# Existencias de los productos
                        header.extend([_("APARTADOS ") + center.name])# Apartados de los productos
                        header.extend([_("TOTAL DE PIEZAS ") + center.name])# Total de piezas 
                        header.extend([_("TOTAL COSTO ") + center.name])# Total de piezas por costo promedio
                    header.extend([_("TOTAL EXISTENCIAS")])# Total de existencias
                    header.extend([_("TOTAL APARTADOS")])# Total de apartados
                    header.extend([_("TOTAL PIEZAS")])# total de piezas
                    header.extend([_("COSTO TOTAL")])#Costo total de las piezas
                    header.extend([_("COSTO ULTIMA COMPRA")])# Costo de la última compra
                    header.extend([_("COSTO PROMEDIO")])# Costo promedio del producto
                    header.extend([_("COSTO REPOSICION")])# Costo de reposición del producto
                    header.extend([_("PRECIO 1")])# precio 1
                    header.extend([_("PRECIO 2")])# precio 2
                    header.extend([_("PRECIO 3")])# precio 3
                    header.extend([_("PRECIO 4")])# precio 4
                    header.extend([_("PRECIO 5")])# precio 5
                    header.extend([_("PRECIO 6")])# precio 5
                    header.extend([_("PRECIO 7")])# precio 5
                    header.extend([_("PRECIO 8")])# precio 5
                    header.extend([_("PRECIO 9")])# precio 5
                    header.extend([_("PRECIO 10")])# precio 5
                    header.extend([_("MONEDA")])#  Tipo de moneda
                    header.extend([_("UPC")])#  codigo upc
                    header.extend([_("GARANTIA")])# Garantia
                    rows = [header]
                # Consulta a las tablas
                cadquery = """
                            CREATE TEMPORARY TABLE rpl_csv AS (
                                    SELECT pp.id, pt.id as product_tmpl_id, pp.default_code, upper(pp.name_template) as name_template, upper(ts.name) as section, upper(tl.name) as line, upper(tm.name) as brand, upper(tser.name) as serie, """
                if self.cost_center:
                    cadquery += """
                            0 as exis"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""", 0 as apart"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""", 0 as tot"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""", 0.00 as cost"""+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+""", 
                    """
                else:
                    for center in cost_centers:
                        cadquery += """
                            0 as exis"""+re.sub('[^A-Za-z0-9]+','',center.name)+""", 0 as apart"""+re.sub('[^A-Za-z0-9]+','',center.name)+""", 0 as tot"""+re.sub('[^A-Za-z0-9]+','',center.name)+""", 0.00 as cost"""+re.sub('[^A-Za-z0-9]+','',center.name)+""", 
                    """
                cadquery += """ 0 as exist_total, 0 as apart_total, 0 as tot_total, 0.00 as cost_total, 0.00 as cost_lp, 0.00 as average_cost, pt.reposition_cost,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=1),0) as level1,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=2),0) as level2,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=3),0) as level3,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=4),0) as level4,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=5),0) as level5,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=6),0) as level6,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=7),0) as level7,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=8),0) as level8,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=9),0) as level9,
                                COALESCE((select price from product_price_level where product=pt.id and price_level=10),0) as level10,
                                rc.name, pp.ean13, upper(cg.garantia_en_catalogo) as garantia_en_catalogo"""
                if self.cost_center:
                    cadquery += ", sl.id as location_id "
                cadquery += """
                                FROM product_product pp
                                LEFT OUTER JOIN product_template pt on pt.id=pp.product_tmpl_id
                                LEFT OUTER JOIN product_category ts on ts.id=pt.section
                                LEFT OUTER JOIN product_category tl on tl.id=pt.line
                                LEFT OUTER JOIN product_category tm on tm.id=pt.brand
                                LEFT OUTER JOIN product_category tser on tser.id=pt.serial
                                LEFT OUTER JOIN res_currency rc on rc.id=pt.list_price_currency
                                LEFT OUTER JOIN catalogo_garantias cg on cg.id=pt.garantia
                                LEFT OUTER JOIN stock_quant sq on sq.product_id=pp.id
                                LEFT OUTER JOIN stock_location sl on sl.id=sq.location_id
                                LEFT OUTER JOIN stock_warehouse sw on sw.lot_stock_id=sl.id
                                INNER JOIN stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id """
                if self.cost_center:
                    cadquery += """WHERE pp.active=true and pt.sale_ok=true and sw.centro_costo_id="""+str(self.cost_center.id)+""" AND swt.code='fac' AND sl.usage='internal' """
                else:
                    cadquery += """WHERE pp.active=true and pt.sale_ok=true and swt.code='fac' AND sl.usage='internal' """
                if self.section.id!=1:
                    cadquery += """AND pt.section="""+str(self.section.id)+""" """
                if self.line.id!=1:
                    cadquery += """AND pt.line="""+str(self.line.id)+""" """
                if self.brand.id!=1:
                    cadquery += """AND pt.brand="""+str(self.brand.id)+""" """
                if self.series.id!=1:
                    cadquery += """AND pt.serial="""+str(self.series.id)+""" """
                if self.cost_center:
                    cadquery += """GROUP BY pp.id,ts.name,tl.name,tm.name,tser.name,rc.name,pp.ean13,cg.garantia_en_catalogo,sl.id, pt.reposition_cost, pt.id); """
                else:
                    cadquery += """GROUP BY pp.id,ts.name,tl.name,tm.name,tser.name,rc.name,pp.ean13,cg.garantia_en_catalogo, pt.reposition_cost, pt.id); """
                if self.cost_center:
                    cadquery += "UPDATE rpl_csv set exis"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=COALESCE((SELECT sum(qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.location_id=rpl_csv.location_id and sq.reservation_id isnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set apart"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=COALESCE((SELECT sum(qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.location_id=rpl_csv.location_id and sq.reservation_id notnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set tot"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=exis"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"+apart"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"; "
                    cadquery += "UPDATE rpl_csv set exist_total=exis"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+", apart_total=apart"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+", tot_total=tot"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"; "
                else:
                    for center in cost_centers:
                        cadquery += "UPDATE rpl_csv set exis"+re.sub('[^A-Za-z0-9]+','',center.name)+"=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sw.centro_costo_id="+str(center.id)+" and sq.reservation_id isnull and swt.code='fac' and sl.usage='internal'),0); "
                        cadquery += "UPDATE rpl_csv set apart"+re.sub('[^A-Za-z0-9]+','',center.name)+"=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sw.centro_costo_id="+str(center.id)+" and sq.reservation_id notnull and swt.code='fac' and sl.usage='internal'),0); "
                        cadquery += "UPDATE rpl_csv set tot"+re.sub('[^A-Za-z0-9]+','',center.name)+"=exis"+re.sub('[^A-Za-z0-9]+','',center.name)+"+apart"+re.sub('[^A-Za-z0-9]+','',center.name)+"; "
                    cadquery += "UPDATE rpl_csv set exist_total=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.reservation_id isnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set apart_total=COALESCE((SELECT sum(sq.qty) from stock_quant sq inner join stock_location sl on sl.id=sq.location_id inner join stock_warehouse sw on sw.lot_stock_id=sl.id inner join stock_warehouse_type swt on swt.id=sw.stock_warehouse_type_id where sq.product_id=rpl_csv.id and sq.reservation_id notnull and swt.code='fac' and sl.usage='internal'),0); "
                    cadquery += "UPDATE rpl_csv set tot_total=exist_total+apart_total;"
                cadquery += "UPDATE rpl_csv set cost_lp=COALESCE((SELECT price_unit FROM purchase_order_line pol LEFT JOIN purchase_order po ON (pol.order_id = po.id) WHERE po.shipped = TRUE AND pol.product_id = rpl_csv.id AND pol.invoiced = TRUE ORDER BY pol.write_date DESC LIMIT 1),0.00); "
                cadquery += "UPDATE rpl_csv set average_cost=COALESCE((SELECT round(cost::numeric,2) FROM product_price_history WHERE product_template_id =rpl_csv.product_tmpl_id  AND cost != 0 ORDER BY datetime DESC LIMIT 1),0.00); "
                if self.cost_center:
                    cadquery += "UPDATE rpl_csv set cost"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+"=round(average_cost*tot"+re.sub('[^A-Za-z0-9]+','',self.cost_center.name)+",2);"
                else:
                    for center in cost_centers:
                        cadquery += "UPDATE rpl_csv set cost"+re.sub('[^A-Za-z0-9]+','',center.name)+"=round(average_cost*tot"+re.sub('[^A-Za-z0-9]+','',center.name)+",2);"
                cadquery += "UPDATE rpl_csv set cost_total=round(tot_total*average_cost,2);"
                cadquery += "SELECT * from rpl_csv order by section, line, brand, serie, default_code;"
                self.env.cr.execute(cadquery)
                query_set = self.env.cr.fetchall()
                # Fin de las consultas    
                for row in query_set:
                    line = []
                    for i, val in enumerate(row):
                        if i>=2:
                            if self.cost_center:
                                if i<len(row)-1:
                                    if val!=None:
                                        line.extend([str(val).encode("utf-8").decode("utf-8")])
                                    else:
                                        line.extend([''])
                            else:
                                if val!=None:
                                    line.extend([str(val).encode("utf-8").decode("utf-8")])
                                else:
                                    line.extend([''])
                    rows.append(line)

                obj_osv = OCsv()
                download_id = self.pool['r.download'].create(cr=self.env.cr, uid=self.env.uid, vals={
                    'file_name': _(
                        "Reporte de lista de precios completo.csv" ),
                    'type_file': 'csv',
                    'file': obj_osv.csv_base64(rows),
                })
                return {
                    'type': 'ir.actions.act_url',
                    'url': '/web/binary/download/file?id=%s' % download_id,
                    'target': 'self',
                }
            else:
                raise ValidationError('No tiene permiso para ejecutar este reporte')
        return