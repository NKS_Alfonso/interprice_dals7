# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
{
    'name': "To-Do Price list report",

    'summary': """
        Price List Report
    """,

    'description': 'Manage your list of prices of products, as well see the stock of branches or in general.',
    'author':'Copyright © 2018 TO-DO - All Rights Reserved',
    'website':'http://www.grupovadeto.com',
    'category':'Sales',
    'version': '0.1',
# persona util
# que te diviertas
# que te guste
    # Module necessary for this one to work correctly
    'depends': ['base', 'product', 'sale', 'sale_stock'],

    # always loaded
    'data': [
        'views/price_list_report_views.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
        ],
    'demo': [ ],
    'test': [ ],

    'aplication':True,

}
