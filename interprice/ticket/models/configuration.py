# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, api, exceptions, fields, models
import openerp.tools as tools
import dateutil.parser


class configuration_assortment(models.Model):
    """Assortment imprinted model"""

    _name = "configuration.assortment"
    _rec_name = "warehouse_id"

    # domain method
    @api.model
    def _get_warehouse_domain(self):
        domain = []
        self.env.cr.execute(""" SELECT sw.id FROM stock_warehouse sw
                                JOIN stock_warehouse_type swt
                                ON sw.stock_warehouse_type_id=swt.id
                                LEFT JOIN configuration_assortment ca
                                ON ca.warehouse_id = sw.id
                                JOIN account_cost_center_res_users_rel accru
                                ON accru.account_cost_center_id=sw.centro_costo_id
                                WHERE swt.code='fac' AND ca.id IS null
                                AND accru.res_users_id=%s """,
                            [self._uid])
        if self.env.cr.rowcount:
            warehouse_id = self.env.cr.fetchall()

            if warehouse_id:
                domain = [('id', 'in', [tw[0] for tw in warehouse_id])]
        else:
            domain = [('id', '=', False)]

        return domain

    # computed fields methods
    @api.one
    @api.depends('delivery_carrier_ids.delivery_quantity',
                 'document_inventory_ids.document_quantity')
    def _compute_qty_sum(self):
        self.delivery_qty_sum = sum(line.delivery_quantity for line in self.delivery_carrier_ids)
        self.document_qty_sum = sum(line.document_quantity for line in self.document_inventory_ids
                                    if line.document_type.prefix == 'SO')

    @api.one
    @api.depends('document_inventory_ids.document_type')
    def _compute_delivery_carrier_field(self):
        prefix = [line.document_type.prefix for line in self.document_inventory_ids]
        if 'SO' in prefix:
            self.delivery_carrier_field = True

    # fields declaration
    warehouse_id = fields.Many2one(
        domain=_get_warehouse_domain,
        comodel_name='stock.warehouse',
        help=_("Assortment configuration warehouse"),
        string=_("Warehouse")
    )
    document_inventory_ids = fields.One2many(
        comodel_name='configuration.document.type',
        help=_("Assortment configuration document inventory type"),
        inverse_name='configuration_id',
        string=_("Document type")
    )
    delivery_carrier_ids = fields.One2many(
        comodel_name='configuration.delivery.carrier',
        help=_("Assortment configuration delivery carrier"),
        inverse_name='delivery_conf_id',
        string=_("Delivery Method")
    )
    delivery_carrier_field = fields.Boolean(
        store=True,
        compute='_compute_delivery_carrier_field',
        help=_("Assortment configuration delivery carrier field"),
        string=_("Active Delivery Method")
    )
    document_qty_sum = fields.Float(
        compute='_compute_qty_sum',
        help=_("Assortment configuration document quantity"),
        string=_("Sum document")
    )
    delivery_qty_sum = fields.Float(
        compute='_compute_qty_sum',
        help=_("Assortment configuration delivery quantity"),
        string=_("Sum delivery")
    )

    # onchange method
    @api.onchange('warehouse_id')
    def get_warehouse_onchange(self):
        res = {'domain': {
            'warehouse_id': self._get_warehouse_domain()}}
        return res

    # overridden methods
    @api.model
    def create(self, vals):
        res = super(configuration_assortment, self).create(vals)

        if res.delivery_qty_sum > res.document_qty_sum and res.delivery_carrier_field:
            raise exceptions.Warning(
                _("You can not assign larger delivery method quantities to documents"))

        prefix = [line.document_type.prefix for line in res.document_inventory_ids]
        if 'SO' not in prefix:
            res.delivery_carrier_ids.unlink()

        if 'SO' in prefix and not res.delivery_carrier_ids:
            raise exceptions.Warning(
                _("You can assign delivery carrier for sales document type")
            )

        for line in res.document_inventory_ids:
            if line.document_type.prefix == 'SO':
                for lines in res.delivery_carrier_ids:
                    lines.document_type_sale_id = line.id

        return res

    @api.multi
    def write(self, vals):
        res = super(configuration_assortment, self).write(vals)

        if self.delivery_qty_sum > self.document_qty_sum and self.delivery_carrier_field:
            raise exceptions.Warning(
                _("You can not assign larger delivery method quantities to documents"))

        prefix = [line.document_type.prefix for line in self.document_inventory_ids]
        if 'SO' not in prefix:
            self.delivery_carrier_ids.unlink()

        if 'SO' in prefix and not self.delivery_carrier_ids:
            raise exceptions.Warning(
                _("You can assign delivery carrier for sales document type")
            )

        for line in self.document_inventory_ids:
            if line.document_type.prefix == 'SO':
                for lines in self.delivery_carrier_ids:
                    lines.document_type_sale_id = line.id
            self.onchange_delivery()
            self.onchange_document()
        return res

    # button method pickings print
    @api.cr_uid_ids_context
    def open_print_interface(self, cr, uid, ids, context=None):
        cr.execute(""" select ca.warehouse_id
                                from configuration_assortment ca
                                where ca.id=%s """, (ids[0],))
        if cr.rowcount:
            warehouse_ids = cr.fetchone()
        final_url = "/assortment/imprinted/ticket/#action=ticket.ui&warehouse_id=" + str(warehouse_ids[0])
        return {'type': 'ir.actions.act_url', 'url': final_url, 'target': 'self', }
    
    @api.multi
    def onchange_delivery(self):
        array = []
        for inventory_id in self.delivery_carrier_ids:
            array.append(inventory_id.delivery_carrier_priority)
        for inventory_id2 in self.delivery_carrier_ids:
            occ = array.count(inventory_id2.delivery_carrier_priority)
            if occ >1:
                raise exceptions.Warning(_("The priority on delivery carrier must be unique!."))
            
    @api.multi
    def onchange_document(self):
        array = []
        for document in self.document_inventory_ids:
            array.append(document.document_type_priority)
        for document2 in self.document_inventory_ids:
            occ = array.count(document2.document_type_priority)
            if occ >1:
                raise exceptions.Warning(_("The priority on delivery carrier must be unique!."))
        
    
        
    # Create PostgreSQL views
    def init(self, cr):
        tools.drop_view_if_exists(cr, 'ticket_warehouses_users')
        # tools.drop_view_if_exists(cr, 'ticket_commerce_rules')
        tools.drop_view_if_exists(cr, 'ticket_pickings_data')
        cr.execute("""
                    CREATE OR REPLACE view ticket_warehouses_users AS
                    (
                    SELECT
                        accrur.res_users_id res_users_id,
                        acc.id cost_center_id,acc.name cost_center_name,
                        sw.id warehouse_id,sw.name warehouse_name,
                        swt.id stock_ware_type_id,swt.name stock_ware_type_name,
                        swt.code stock_ware_type_code
                    FROM account_cost_center_res_users_rel accrur
                    JOIN account_cost_center acc
                    ON accrur.account_cost_center_id=acc.id
                    JOIN stock_warehouse sw ON sw.centro_costo_id=acc.id
                    JOIN stock_warehouse_type swt
                    ON swt.id=sw.stock_warehouse_type_id
                    WHERE swt.code='fac'
                    ) """
                   )

        # cr.execute("""
                    # CREATE OR REPLACE view ticket_commerce_rules AS
                    # (
                    # SELECT
                    #     T1.warehouse_id,
                    #     T1.warehouse_name,
                    #     T1.doct_inv_type_id,
                    #     T1.doct_inv_type_name,
                    #     CASE WHEN T1.deliv_car_name IS NOT NULL THEN T1.conf_deliv_car_qty
                    #     ELSE T1.conf_doc_type_qty END conf_doc_type_qty,
                    #     T1.deliv_car_id,
                    #     T1.deliv_car_name,
                    #     T1.conf_deliv_car_qty
                    # FROM (
                    # SELECT sw.id warehouse_id,sw.name warehouse_name,
                    # ca.warehouse_id conf_assort_warehouse_id,
                    # dit.id doct_inv_type_id, dit.name doct_inv_type_name,
                    # cdt.document_quantity conf_doc_type_qty,
                    # COALESCE(dc.id, 0) deliv_car_id,dc.name deliv_car_name,
                    # cdc.delivery_quantity conf_deliv_car_qty
                    # from stock_warehouse sw
                    # JOIN configuration_assortment ca ON ca.warehouse_id=sw.id
                    # JOIN configuration_document_type cdt ON cdt.configuration_id=ca.id
                    # JOIN document_inventory_type dit ON dit.id=cdt.document_type
                    # LEFT JOIN configuration_delivery_carrier cdc
                    # ON cdc.document_type_sale_id=cdt.id
                    # LEFT JOIN delivery_carrier dc ON dc.id=cdc.delivery_carrier
                    # ) AS T1
                    # )"""
        #            )

        cr.execute("""
                    CREATE OR REPLACE view ticket_pickings_data AS
                    (
                    SELECT
                    T1.picking_id,
                    T1.picking_name,
                    T1.picking_origin,
                    T1.creation_date_picking,
                    T1.partner_id,
                    T1.partner_name,
                    T1.warehouse_id,
                    T1.warehouse_name,
                    CASE WHEN T1.sale_name IS NOT NULL THEN T1.doc_inventory_so_id
                         WHEN T1.purchase_name IS NOT NULL THEN T1.doc_inventory_po_id
                         WHEN T1.doc_inventory_number IS NOT NULL THEN T1.doc_inv_id
                         WHEN T1.dev_so_name IS NOT NULL THEN T1.doc_inv_type_dev_so_id
                         WHEN T1.dev_po_name IS NOT NULL THEN T1.doc_inv_type_dev_po_id
                         WHEN T1.dev_doc_inv_number IS NOT NULL THEN T1.doc_inv_type_dev_di_id
                            ELSE 0 END document_inventory_type_id,
                    CASE WHEN T1.sale_name IS NOT NULL THEN T1.doc_inventory_so_name
                         WHEN T1.purchase_name IS NOT NULL THEN T1.doc_inventory_po_name
                         WHEN T1.doc_inventory_number IS NOT NULL THEN T1.doc_inv_name
                         WHEN T1.dev_so_name IS NOT NULL THEN T1.doc_inv_type_dev_so_name
                         WHEN T1.dev_po_name IS NOT NULL THEN T1.doc_inv_type_dev_po_name
                         WHEN T1.dev_doc_inv_number IS NOT NULL THEN T1.doc_inv_type_dev_di_name
                            ELSE '' END document_inventory_type_name,
                    CASE WHEN T1.sale_name IS NOT NULL THEN T1.prefix_so
                         WHEN T1.purchase_name IS NOT NULL THEN T1.prefix_po
                         WHEN T1.doc_inventory_number IS NOT NULL THEN T1.prefix_doc_inv
                         WHEN T1.dev_so_name IS NOT NULL THEN T1.prefix_dev_so
                         WHEN T1.dev_po_name IS NOT NULL THEN T1.prefix_dev_po
                         WHEN T1.dev_doc_inv_number IS NOT NULL THEN T1.prefix_dev_inv
                            ELSE '' END prefix,
                    T1.delivery_car_id,
                    T1.delivery_car_name,
                    T1.picking_state
                    FROM (
                        SELECT
                        sp.id picking_id,sp.name picking_name,sp.origin picking_origin,
                        (cast(sp.date AT TIME ZONE 'utc' AS VARCHAR(19))) creation_date_picking,
                        CASE WHEN rp.id IS NULL THEN (SELECT rc.id FROM res_company rc)
                        ELSE rp.id END partner_id,
                        CASE WHEN rp.name IS NULL THEN (SELECT rc.name FROM res_company rc)
                        ELSE rp.name END partner_name,
                        sw.id warehouse_id,sw.name warehouse_name,
                        COALESCE(dc.id, 0) delivery_car_id, COALESCE(dc.name,'') delivery_car_name,
                        so.id sale_order_id,so.name sale_name,
                        doc_inv_type_so.id doc_inventory_so_id,doc_inv_type_so.name doc_inventory_so_name,
                        doc_inv_type_so.prefix prefix_so,
                        po.id purchase_order_id,po.name purchase_name,
                        doc_inv_type_po.id doc_inventory_po_id,doc_inv_type_po.name doc_inventory_po_name,
                        doc_inv_type_po.prefix prefix_po,
                        doc_inv.number doc_inventory_number,
                        doc_inv_type.id doc_inv_id,doc_inv_type.name doc_inv_name,doc_inv_type.prefix prefix_doc_inv,
                        dev.picking_id dev_picking_id,dev.order_id dev_order_id,
                        dev_so.name dev_so_name, doc_inv_type_dev_so.id doc_inv_type_dev_so_id,
                        doc_inv_type_dev_so.name doc_inv_type_dev_so_name,
                        doc_inv_type_dev_so.prefix prefix_dev_so,
                        dev_po.name dev_po_name,doc_inv_type_dev_po.id doc_inv_type_dev_po_id,
                        doc_inv_type_dev_po.name doc_inv_type_dev_po_name,
                        doc_inv_type_dev_po.prefix prefix_dev_po,
                        dev_di.number dev_doc_inv_number,doc_inv_type_dev_di.id doc_inv_type_dev_di_id,
                        doc_inv_type_dev_di.name doc_inv_type_dev_di_name,
                        doc_inv_type_dev_di.prefix prefix_dev_inv,
                        sp.state picking_state
                        FROM stock_picking sp
                            LEFT JOIN res_partner rp ON rp.id=sp.partner_id
                            LEFT JOIN stock_picking_type spt ON spt.id=sp.picking_type_id
                            LEFT JOIN stock_warehouse sw ON sw.id=spt.warehouse_id
                            LEFT JOIN delivery_carrier dc on dc.id=sp.carrier_id
                            LEFT JOIN sale_order so ON so.name=sp.origin
                            LEFT JOIN document_inventory_type doc_inv_type_so ON doc_inv_type_so.prefix='SO'
                                 AND so.name IS NOT NULL
                            LEFT JOIN purchase_order po ON po.name=sp.origin
                            LEFT JOIN document_inventory_type doc_inv_type_po ON doc_inv_type_po.prefix='PO'
                                 AND po.name IS NOT NULL
                            LEFT JOIN document_inventory doc_inv ON doc_inv.number=sp.origin
                            LEFT JOIN document_inventory_type doc_inv_type
                                 ON doc_inv_type.id=doc_inv.type_document
                            LEFT JOIN _gv_devolucion dev ON dev.picking_id=sp.id
                            LEFT JOIN sale_order dev_so ON dev_so.name=dev.order_id
                            LEFT JOIN document_inventory_type doc_inv_type_dev_so
                                 ON doc_inv_type_dev_so.prefix='DEV-SO' AND dev_so.name IS NOT NULL
                            LEFT JOIN purchase_order dev_po ON dev_po.name=dev.order_id
                            LEFT JOIN document_inventory_type doc_inv_type_dev_po
                                 ON doc_inv_type_dev_po.prefix='DEV-PO' AND dev_po.name IS NOT NULL
                            LEFT JOIN document_inventory dev_di ON dev_di.number=dev.order_id
                            LEFT JOIN document_inventory_type doc_inv_type_dev_di
                                 ON doc_inv_type_dev_di.prefix='DEV-INV' AND dev_di.number IS NOT NULL
                        WHERE sp.state IN ('assigned','confirmed')
                        ) AS T1
                        )"""
                   )

    @api.model
    def get_pickings_ids(self):
        user_id = self._uid

        self.env.cr.execute(""" SELECT twu.warehouse_id warehouse_id
                                    FROM ticket_warehouses_users twu
                                    WHERE twu.res_users_id=%s""", [user_id])
        if self.env.cr.rowcount:
            warehouses_user = self.env.cr.dictfetchall()
            warehouse_ids_list = []

            for list in warehouses_user:
                warehouse_id = list['warehouse_id']
                warehouse_ids_list.append(warehouse_id)

            self.env.cr.execute(""" SELECT
                                        T1.warehouse_id,
                                        T1.warehouse_name,
                                        T1.doct_inv_type_id,
                                        T1.doct_inv_type_name,
                                        CASE WHEN T1.deliv_car_name IS NOT NULL THEN T1.conf_deliv_car_qty
                                        ELSE T1.conf_doc_type_qty END conf_doc_type_qty,
                                        T1.deliv_car_id,
                                        T1.deliv_car_name,
                                        T1.conf_deliv_car_qty,
                                        T1.priority_doc,
                                        T1.priority_ship,
                                        T1.config_assortment_id
                                    FROM (
                                    SELECT sw.id warehouse_id,sw.name warehouse_name,
                                    ca.warehouse_id conf_assort_warehouse_id,
                                    dit.id doct_inv_type_id, dit.name doct_inv_type_name,
                                    cdt.document_quantity conf_doc_type_qty,
                                    COALESCE(dc.id, 0) deliv_car_id,dc.name deliv_car_name,
                                    cdc.delivery_quantity conf_deliv_car_qty,
                                    cdt.document_type_priority priority_doc,
                                    cdc.delivery_carrier_priority priority_ship,
                                    ca.id config_assortment_id
                                    from stock_warehouse sw
                                    JOIN configuration_assortment ca ON ca.warehouse_id=sw.id
                                    JOIN configuration_document_type cdt ON cdt.configuration_id=ca.id
                                    JOIN document_inventory_type dit ON dit.id=cdt.document_type
                                    LEFT JOIN configuration_delivery_carrier cdc
                                    ON cdc.document_type_sale_id=cdt.id
                                    LEFT JOIN delivery_carrier dc ON dc.id=cdc.delivery_carrier
                                    WHERE sw.id IN %s
                                    ) AS T1""",
                                (tuple(warehouse_ids_list),))
            if self.env.cr.rowcount:
                commerce_rules_data = self.env.cr.dictfetchall()

                pickings_rules_apply = []

                for commerce in commerce_rules_data:
                    configutation_assortment_id = commerce['config_assortment_id']
                    warehouse_id = commerce['warehouse_id']
                    doct_inv_type_id = commerce['doct_inv_type_id']
                    conf_doc_type_qty = commerce['conf_doc_type_qty']
                    deliv_car_id = commerce['deliv_car_id']

                    self.env.cr.execute(""" SELECT * FROM ticket_pickings_data
                                            WHERE warehouse_id = %s
                                            AND document_inventory_type_id = %s
                                            AND delivery_car_id = %s
                                            limit %s """,
                                        (warehouse_id, doct_inv_type_id,
                                         deliv_car_id, conf_doc_type_qty)
                                        )

                    if self.env.cr.rowcount:
                        pickings_data = self.env.cr.dictfetchall()

                        for data in pickings_data:
                            picking_id = data['picking_id']
                            warehouse_id = data['warehouse_id']
                            document_inventory_type_id = data['document_inventory_type_id']
                            delivery_carrier_id = data['delivery_car_id']

                            self.env.cr.execute(""" SELECT sum(sm.product_qty)
                                                    FROM stock_picking sp
                                                    INNER JOIN stock_move sm
                                                    ON sm.picking_id=sp.id
                                                    WHERE sp.id in (%s) """,
                                                (picking_id,)
                                                )

                            if self.env.cr.rowcount:
                                picking_product_qty = self.env.cr.fetchone()[0]
                                data['product_qty'] = picking_product_qty

                            self.env.cr.execute(""" SELECT
                                                    spp.stock_picking_id stock_picking_id,
                                                    spp.user_name user_id,
                                                    spp.create_date print_date,
                                                    res_users.login login_id
                                                    FROM stock_picking_print spp
                                                    LEFT JOIN res_users ON spp.supplier_user = res_users.id
                                                    WHERE spp.stock_picking_id = %s""",
                                                (picking_id,)
                                                )

                            if self.env.cr.rowcount:
                                picking_print_data = self.env.cr.dictfetchall()

                                stock_picking_id = picking_print_data[0].get('stock_picking_id')
                                print_date = picking_print_data[0].get('print_date').split('.')[0]
                                user_id = picking_print_data[0].get('user_id')
                                supplier_login = picking_print_data[0].get('login_id', 'No surtido')

                                data['stock_picking_id'] = stock_picking_id
                                data['print_date'] = print_date
                                data['user_id'] = user_id
                                data['login_id'] = supplier_login

                            # pickings_rules_apply.append(data)
                            delivery_carrier_query = ''
                            if delivery_carrier_id > 0:
                                delivery_carrier_query = 'AND cdc.delivery_carrier = %s' % (delivery_carrier_id,)

                            self.env.cr.execute(""" SELECT
                                    cdt.document_type_priority priority_doc,
                                    cdc.delivery_carrier_priority priority_ship
                                    FROM configuration_assortment  ca
                                    JOIN configuration_document_type cdt
                                    ON cdt.configuration_id = ca.id
                                    LEFT JOIN configuration_delivery_carrier cdc
                                    ON cdc.document_type_sale_id = cdt.id
                                    WHERE warehouse_id = %s
                                    AND cdt.document_type = %s
                                    %s """ % (warehouse_id,
                                              document_inventory_type_id,
                                              delivery_carrier_query,)
                            )
                            if self.env.cr.rowcount:
                                data_priority = self.env.cr.dictfetchall()
                                priority_doc = data_priority[0].get('priority_doc')
                                priority_ship = data_priority[0].get('priority_ship')

                                data['priority_doc'] = priority_doc
                                data['priority_ship'] = priority_ship

                            pickings_rules_apply.append(data)

                return sorted(pickings_rules_apply, key=lambda pick: dateutil.parser.parse(pick['creation_date_picking']), reverse=False)
