# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, api, exceptions, _


class pos_ticket(models.Model):
    """Ticket imprinted model"""

    _name = "pos.ticket"

    # @api.cr_uid_ids_context
    # def open_print_interface(self, cr, uid, ids, context=None):
    #     final_url = "/assortment/imprinted/ticket/#action=ticket.ui&ids=" + str(ids[0])
    #     return {'type': 'ir.actions.act_url', 'url': final_url, 'target': 'self', }

    @api.cr_uid_ids_context
    def open_print_interface(self, cr, uid, ids, context=None):
        cr.execute("""
                   SELECT warehouse_id
                   FROM configuration_assortment
                   ORDER BY write_date DESC
                   LIMIT 1
                   """)
        if cr.rowcount:
            warehouse_id = cr.fetchone()
            final_url = "/assortment/imprinted/ticket/#action=ticket.ui&warehouse_id=" + str(warehouse_id[0])
            return {'type': 'ir.actions.act_url', 'url': final_url, 'target': 'self', }
        else:
            raise exceptions.ValidationError(_("Please configure supplier printer at Warehouse/Configuration/Suplier printer"))
