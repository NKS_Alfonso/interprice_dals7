# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, api, exceptions, fields, models


class configuration_document_type(models.Model):

    _name = "configuration.document.type"

    # fields declaration
    configuration_id = fields.Many2one(
        comodel_name='configuration.assortment',
        help=_("Document type id"),
        string=_("Configuration")
    )
    document_type = fields.Many2one(
        comodel_name='document.inventory.type',
        help=_("Document type name"),
        string=_("Document")
    )
    document_quantity = fields.Integer(
        default=1,
        help=_("Document type quantity"),
        string=_("Quantity"),
    )
    document_type_priority = fields.Selection([
            ('1','1'),('2','2'),('3','3'),('4','4'),
            ('5','5'),('6','6'),('7','7'),('8','8'),
            ('9','9'),('10','10')
        ],
        help=_("Priority of document"),
        string=_('Document Priority')
    )

    # overriden methods
    @api.model
    def create(self, vals):
        if 'document_quantity' in vals and vals['document_quantity'] <= 0:
            raise exceptions.Warning(_("Quantity must be greater than 0."))

        if 'document_type' in vals and 'configuration_id' in vals:
            for data_id in self.search(
                    [('document_type', '=', vals.get('document_type')),
                     ('configuration_id', '=', vals.get('configuration_id')),
                     ]):

                if data_id:
                    raise exceptions.Warning(
                        _("You can not assign the same document more than once to the store."))
        return super(configuration_document_type, self).create(vals)

    @api.multi
    def write(self, vals):
        # import pdb; pdb.set_trace()
        if 'document_quantity' in vals and vals['document_quantity'] <= 0:
            raise exceptions.Warning(_("Quantity must be greater than 0."))

        if 'document_type' in vals:
            for data_id in self.search(
                    [('document_type', '=', vals.get('document_type')),
                     ('configuration_id', '=', self.configuration_id.id)
                     ]):

                if data_id:
                    raise exceptions.Warning(
                        _("You can not assign the same document more than once to the store."))
        return super(configuration_document_type, self).write(vals)
