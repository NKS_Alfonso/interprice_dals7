# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, api, exceptions, fields, models


class configuration_delivery_carrier(models.Model):

    _name = "configuration.delivery.carrier"

    # fields declaration
    delivery_conf_id = fields.Many2one(
        comodel_name='configuration.assortment',
        help=_("Delivery carrier id"),
        string=_("Configuration")
    )
    delivery_carrier = fields.Many2one(
        comodel_name='delivery.carrier',
        help=_("Delivery carrier name"),
        string=_("Delivery Method")
    )
    delivery_quantity = fields.Integer(
        default=1,
        help=_("Delivery carrier quantity"),
        string=_("Quantity")
    )
    delivery_carrier_priority = fields.Selection([
            ('1','1'),('2','2'),('3','3'),('4','4'),
            ('5','5'),('6','6'),('7','7'),('8','8'),
            ('9','9'),('10','10')
        ],
        help=_("Delivery carrier document priority"),
        string=_('Delivery carrier priority')
    )
    document_type_sale_id = fields.Integer(
        help=_("Delivery carrier document type sale id"),
        string=_("Document type sale id")
    )

    # overriden methods
    @api.model
    def create(self, vals):
        if 'delivery_quantity' in vals and vals['delivery_quantity'] <= 0:
            raise exceptions.Warning(_("Quantity must be greater than 0."))

        if 'delivery_carrier' in vals and 'delivery_conf_id' in vals:
            for data_id in self.search(
                    [('delivery_carrier', '=', vals.get('delivery_carrier')),
                     ('delivery_conf_id', '=', vals.get('delivery_conf_id'))
                     ]):

                if data_id:
                    raise exceptions.Warning(
                        _("You can not assign the same delivery method more than once to the document."))
        return super(configuration_delivery_carrier, self).create(vals)

    @api.multi
    def write(self, vals):

        if 'delivery_quantity' in vals and vals['delivery_quantity'] <= 0:
            raise exceptions.Warning(_("Quantity must be greater than 0."))

        if 'delivery_carrier' in vals:
            for data_id in self.search(
                    [('delivery_carrier', '=', vals.get('delivery_carrier')),
                     ('delivery_conf_id', '=', self.delivery_conf_id.id)
                     ]):

                if data_id:
                    raise exceptions.Warning(
                        _("You can not assign the same delivery method more than once to the document."))
        return super(configuration_delivery_carrier, self).write(vals)
