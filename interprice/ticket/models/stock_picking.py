# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers
#
# Notes: Check folder docs/ for documentation

from openerp import models, fields, _
import datetime


class StockPickingPrint(models.Model):
    _name = 'stock.picking.print'

    user_name = fields.Char(
        string=_('User name'),
        required=False,
        readonly=False,
        index=False,
        help=False,
        size=64,
        translate=False
    )

    print_date = fields.Datetime(
        string=_('Print date'),
        required=False,
        readonly=False,
        index=False,
        default=datetime.datetime.utcnow(),
        help=False
    )

    stock_picking_id = fields.Many2one(
        string='Print ids',
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='stock.picking',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )

    supplier_user = fields.Many2one(
        string=_('Supplied user'),
        required=False,
        readonly=False,
        index=False,
        default=False,
        help=False,
        comodel_name='res.users',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    print_ids = fields.One2many(
        string=_('Stock picking id'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=False,
        comodel_name='stock.picking.print',
        inverse_name='stock_picking_id',
        domain=[],
        context={},
        auto_join=False,
        limit=None
    )
