# -*- coding: utf-8 -*-
{
    'name': 'Assortment imprinted',
    'author': 'Copyright © 2016 TO-DO - All Rights Reserved',
    'website': 'http://www.grupovadeto.com',
    'category': 'Warehouse',
    'depends': ['base', 'web', 'documents_inventory', 'delivery', 'stock', 'series'],
    'application': True,
    'data': [
        'views/configuration_view.xml',
        'views/tickets.xml',
        'views/pos_ticket.xml',
        'data/document_inventory_type_data.xml'
    ],
    'demo': [],
    'js': [
        'static/src/js/ticket.js',
        'static/src/js/crypto-js.js'
    ],
    'css': [
        'static/src/css/ticket.css',
    ],
    'qweb': ['static/src/xml/ticket.xml'],
}
