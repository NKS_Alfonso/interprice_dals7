Ticket assortment printer
------------------

Menu location to configure the assortment printer

.. figure:: ../ticket/static/img/printer_menu_location.png
    :alt: Menu printer ticket
    :width: 100%


Window to configure the assortment printer

.. figure:: ../ticket/static/img/printer_configuration.png
    :alt: Window to configure the assortment printer
    :width: 100%


Window to view the tickets to print and assort

.. figure:: ../ticket/static/img/printer_window.png
    :alt: Window to print a ticket
    :width: 100%