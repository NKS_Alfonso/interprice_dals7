function openerp_ticket_widgets(instance){

    var module = instance.ticket;
    var _t     = instance.web._t;
    var QWeb   = instance.web.qweb;

    // false this on production
    var DEBUG = false;

    module.MobileWidget = instance.web.Widget.extend({
        start: function(){
            if(!$('#oe-mobilewidget-viewport').length){
                $('head').append('<meta id="oe-mobilewidget-viewport" name="viewport" content="initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">');
            }
            console.log('Funcion start movile');
            return this._super();
           },
        destroy: function(){
            $('#oe-mobilewidget-viewport').remove();
            return this._super();
        },
    });

    // Time to wait between printing.
    var printingTiming = 500;
    var salt = "3d8ae39fb41124d511be912ed13c6e7c";
    // CORS proxy configuration for NGINX
    /*
    location /api/ {
        proxy_pass http://crones.pchm.dev/intranet/RH7/api.php/ObtenerNombreEmpleado;
    }
    */
    var urlProxy = "http://localhost:8888/proxy/http://crones.pchm.dev/intranet/RH7/api.php/ObtenerNombreEmpleado";
    var urlPro = "/api/";
    var requestUrl = DEBUG ? urlProxy : urlPro;
    var requestHeader = {
          "typ": "JWT",
          "alg": "HS256"
        };
    var requestBody = {};

    /* API Uses JSON Web Tokens check
    https://jwt.io/
    https://www.jonathan-petitcolas.com/2014/11/27/creating-json-web-token-in-javascript.html
    */
    function base64url(source) {
        // Encode in classical base64
        encodedSource = CryptoJS.enc.Base64.stringify(source);

        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');

        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');

        return encodedSource;
    }
    // user-id input validation
    function validUseId(userId){
        var re = /^[A-Z][0-9]{4}$/;
        if (re.test(userId)) {
            requestBody = {
                "idEmpleado": userId
            };
            return 1;
        // API call to get user name by user-id
        } else {
            alert("Identificador invalido.\n\nPor favor ingrese un identificador valido e.g. M4021");
            return 0;
        }
    }
    function createRequestToken(userId) {
        var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(requestHeader));
        var encodedHeader = base64url(stringifiedHeader);
        var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(requestBody));
        var encodedData = base64url(stringifiedData);
        var token = encodedHeader + "." + encodedData;
        var signature = CryptoJS.HmacSHA256(token, salt);
        signature = base64url(signature);

        return token + "." + signature;
    }
    function decodeResponseToken(result) {
        var resultHeader = result.split(".")[0].replace(/-/g, "+").replace(/_/g, "/");
        var resultHeaderDecode = window.atob(resultHeader);
        var resultBody = result.split(".")[1].replace(/-/g, "+").replace(/_/g, "/");
        var resultBodyDecode = window.atob(resultBody);
        var resultBodyDecodeJson = self.$.parseJSON(window.atob(resultBody));
        var resultToken = result.split(".")[2].replace(/-/g, "+").replace(/_/g, "/");
        var signature = CryptoJS.HmacSHA256(resultHeaderDecode + '.' + resultBodyDecode, salt);
        signature = base64url(signature);
        return [resultHeaderDecode, resultBodyDecodeJson, resultToken, signature];
    }
    // Write date and user to model, check successCallback for more functionality
    function updateStockPickingModel(stock_picking_id, userName, type_req) {
        self.$.ajax({
            async: true,
            timeout: 3000,
            dataType: "json",
            url: '/assortment/process/ticket?picking_id=' + encodeURIComponent(stock_picking_id) + '&user_name=' + encodeURIComponent(userName) + '&type_req=' + encodeURIComponent(type_req),
            success: function (result, status, xhr) {
                if (DEBUG) {
                    console.log(result);
                }
                if (status == 'success' && result.data.stock_picking_print_id) {
                    docUserName = result.data.user_name;
                    docPrintDate = result.data.print_date;
                    if (stock_picking_id) {
                        $("#search-folio-supplied").text(check_value(docUserName));
                        $("#user-name").text(check_value(docUserName));
                        $("#search-folio-print-date").text(check_value(docPrintDate));
                    }
                }
            }
        });
    }
    function printAndUpdateDoc(stock_picking_id, userName) {
        var printUrl = '/report/html/series.report_picking/' + encodeURIComponent(stock_picking_id);
        var printW = window.open(printUrl, '_blank', 'toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=yes');
        var document_focus = false;
        if (!(printW)) {
            alert("Por favor habilite ventanas emergentes");
            console.log("Por favor habilite ventanas emergentes");
            return false;
        }
        // Prepare opened window to be printed
        self.$(printW.document).ready(function() {
            // printW.window.print();
            if (DEBUG) {
                console.log("Printed");
            }
            document_focus = true;
        });
        this.print = function() {
            if (document_focus === true) {
                printW.window.print();
                printW.window.close();
            }
        };
        this.updateTimer = function() { // This is being called...
            this.timer = setTimeout(this.print.bind(this), 3000);
        };
        this.updateTimer();
        updateStockPickingModel(stock_picking_id, userName, 'POST');
        self.$("#buscar-tabla").val("");
        self.$("#user-id").val("");
        self.$("#user-name").val("");
        self.$("#search-folio-id").val("");
    }
    function check_value(val) {
        return val ? val : 'No encontrado';
    }

    module.TicketMainWidget = module.MobileWidget.extend({
        template: 'TicketMainWidget',
        events: {
            'keyup #buscar-tabla': function (e) {
                var filter, trs, tds;
                filter = self.$("#buscar-tabla")[0].value;
                //console.log(self.$("td"));
                trs = self.$("tr");
                // Loop through all list items, and hide those who don't match the search query
                for (var i = 2; i < trs.length; i++) {
                    tds = trs[i].getElementsByTagName("td");
                    //console.log(tds[0].innerHTML)
                    if (tds[1].innerHTML.indexOf(filter) > -1) {
                        trs[i].classList.remove("hide-item");
                        //console.log(td_all[i].parentElement);
                    } else {
                        trs[i].classList.add("hide-item");
                    }
                }
                if (e.which == 13) {
                    // Modal is empty by default html to be set
                    var html = `
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Documento</label>
                        <div class="col-sm-8">
                          <p id="search-folio-doc" class="form-control-static">No encontrado</p>
                        </div>
                        <label class="col-sm-4 control-label">Generado</label>
                        <div class="col-sm-8">
                          <p id="search-folio-create-date" class="form-control-static">No encontrado</p>
                        </div>
                        <label class="col-sm-4 control-label">Impresion</label>
                        <div class="col-sm-8">
                          <p id="search-folio-printed-date" class="form-control-static">No encontrado</p>
                        </div>
                        <label class="col-sm-4 control-label">Impreso Por</label>
                        <div class="col-sm-8">
                          <p id="search-folio-printed-by" class="form-control-static">No encontrado</p>
                        </div>
                        <label class="col-sm-4 control-label">Surtido Por</label>
                        <div class="col-sm-8">
                          <p id="search-folio-supplier-by" class="form-control-static">No encontrado</p>
                        </div>
                      </div>
                    </form>
                    `;
                    $("#search-folio-body").html(html);
                    var docFolio, docId, docName, docCreateDate, docUserName, docPrintDate, userId, userName;
                    var searchResp = false;
                    userId = self.$("#user-id")[0].value.toUpperCase();
                    var userIdSuccess = function(result, status, xhr) {
                        var responseToken = decodeResponseToken(result);
                        var resultBodyDecodeJson = responseToken[1];

                        if (DEBUG) {
                            var resultHeaderDecode = responseToken[0];
                            var resultToken = responseToken[2];
                            var signature = responseToken[3];
                            console.log('response info:');
                            console.log(resultHeaderDecode, resultBodyDecodeJson, resultToken, signature);
                        }
                        // Verify API response
                        if (resultBodyDecodeJson.estatus == false) {
                            alert(resultBodyDecodeJson.mensaje);
                            $('#search-folio-supplied').prop('disabled', false);
                            return 0;
                        } else if (resultBodyDecodeJson.estatus == true && resultBodyDecodeJson.datos){
                            userName = resultBodyDecodeJson.datos.nombre;
                            $("#user-name").val(userName);
                        }
                    };
                    var userIdError = function(xhr, status, error) {
                        if (DEBUG) {
                            console.log('On error API call');
                        }
                    };
                    if (validUseId(userId)) {
                        var signedToken = createRequestToken(userId);
                        //console.log("AJAX call to raul's api: " + signedToken);
                        self.$.ajax({
                            contentType: "text/plain; charset=utf-8",
                            type: "POST",
                            timeout: 3000,
                            url: requestUrl,
                            data: signedToken,
                            success: userIdSuccess,
                            error: userIdError
                        });
                    } else {
                        return 0;
                    }

                    var getPikingCallback = function(result, status, xhr) {
                        if (result.data){
                            if (DEBUG) {
                                console.log(result.data);
                            }
                            docId = result.data.id;
                            docName = result.data.doc;
                            docCreateDate = result.data.create_date;
                            docPrintedBy = result.data.user_name;
                            docPrintDate = result.data.print_date;
                            docSupplierBy = result.data.supplier_user;
                            searchResp = true;
                            // Validate user-id given.
                            if (validUseId(userId)) {
                                // Update ticket print info if not already.
                                $("#search-folio-title").text('Documento ' + docFolio);
                                if (docId) {
                                    $("#search-folio-id").val(docId);
                                    $("#search-folio-doc").text(check_value(docName));
                                    $("#search-folio-create-date").text(check_value(docCreateDate));
                                    $("#search-folio-printed-by").text(check_value(docPrintedBy));
                                    $("#search-folio-printed-date").text(check_value(docPrintDate));
                                    $("#search-folio-supplier-by").text(check_value(docSupplierBy));
                                    $('#search-folio-print').prop('disabled', false);
                                    if (DEBUG) {
                                        console.log(userName);
                                    }
                                    updateStockPickingModel(docId, 'NOUSER', 'GET');
                                    $('#search-folio-modal').modal('show');
                                }
                            } else {
                                return 0;
                            }
                        }
                    };
                    var getPickingError = function(){
                        $("#search-folio-body").html("<p>No fue encontrado, por favor verifica el folio.</p>");
                        $('#search-folio-print').prop('disabled', true);
                        $('#search-folio-modal').modal('show');
                    };
                    // search doc by folio
                    docFolio = self.$("#buscar-tabla")[0].value;
                    if (docFolio == false) {
                        alert("Por favor, ingrese un folio e.g. SO/0000123 ");
                        return 0;
                    }
                    if (DEBUG) {
                        console.log(docFolio, userId);
                    }
                    // now search for the given folio.
                    self.$.ajax({
                        dataType: "json",
                        timeout: 3000,
                        url: '/assortment/get/ticket?folio=' + encodeURIComponent(docFolio, "UTF-8"),
                        success: getPikingCallback,
                        error: getPickingError
                    });
                }
            },
            'click #search-folio-print': function (e) {
                e.preventDefault();
                var albaran = $("#search-folio-id").val();
                var userName = $("#user-name").val();
                printAndUpdateDoc(albaran, userName);
            },
            'keyup #user-id': function (e) {
                if (e.which == 13) {
                    self.$( "#enviar-data" ).trigger( "click" );
                }
            },
            'click #recargar': function (e) {
                e.preventDefault();
                $("#buscar-tabla").val("");
                $("#user-id").val("");
                $("#user-name").val("");
                $("#search-folio-id").val("");
                this.get_picking_id();
            },
            'click #enviar-data': function (e) {
                e.preventDefault();
                var userId = $("#user-id").val().toUpperCase();
                // Error user name get API call
                var userIdError = function(xhr, status, error) {
                    // Get checked elements to print
                    $("input:checked").each(function() {
                        if (DEBUG) {
                            console.log("Fallo");
                        }
                        self.$("#user-name").text("No encontrado");
                        var albaran = $(this).val();
                        if (validUseId(userId)) {
                            // Prepare opened window to be printed
                            printAndUpdateDoc(albaran, userId);
                        } else {
                            return 0;
                        }
                    });
                };
                // Success user name get API call
                var userIdSuccess = function(result, status, xhr) {
                    var responseToken = decodeResponseToken(result);
                    var resultBodyDecodeJson = responseToken[1];

                    if (DEBUG) {
                        var resultHeaderDecode = responseToken[0];
                        var resultToken = responseToken[2];
                        var signature = responseToken[3];
                        console.log('response info:');
                        console.log(resultHeaderDecode, resultBodyDecodeJson, resultToken, signature);
                    }
                    // Verify API response
                    if (resultBodyDecodeJson.estatus == false) {
                        alert(resultBodyDecodeJson.mensaje);
                        return 0;
                    } else if (resultBodyDecodeJson.estatus == true && resultBodyDecodeJson.datos){
                        var userName = resultBodyDecodeJson.datos.nombre;
                        // Hiden field is used to store "ejecutivo" name for the ticket
                        $("#user-name").val(userName);
                        // Get checked elements to print
                        $("input:checked").each(function() {
                            var albaran = $(this).val();
                            var successCallback = function (result, status, xhr) {
                                    if (status == 'success' && result.data.stock_picking_print_id) {
                                        if (DEBUG) {
                                            console.log(result);
                                        }
                                        var tr = self.$(("#picking-line-") + result.data.stock_picking_id);
                                        var user_name = result.data.user_name;
                                        var print_date = result.data.print_date;
                                        var stock_picking_id = result.data.stock_picking_id;

                                        $('input[value="' + result.data.stock_picking_id + '"]').attr('checked', false);
                                        $('#user-id-' + stock_picking_id).text(user_name);
                                        $('#print-date-' + stock_picking_id).text(print_date);
                                        $("#user-id").val("");
                                    }
                                };
                            if (userName) {
                                printAndUpdateDoc(albaran, userName);
                            }
                        });
                    } else {
                        alert("something went wrong, please contact IT");
                    }
                };
                // Validate user-id given.
                if (validUseId(userId)) {
                    var signedToken = createRequestToken(userId);
                    //console.log("AJAX call to raul's api: " + signedToken);
                    self.$.ajax({
                        contentType: "text/plain; charset=utf-8",
                        type: "POST",
                        timeout: 3000,
                        url: requestUrl,
                        data: signedToken,
                        success: userIdSuccess,
                        error: userIdError
                    });
                } else {
                    return 0;
                }
                if ($("input:checked").length <=0) {
                    alert("Por favor, seleccione al menos un documento a imprimir");
                    return 0;
                }
            }
        },

        init: function(parent,params){
            console.log('Funcion init main');
            this._super(parent,params);
            var self = this;
            $(window).bind('hashchange', function(){
                var states = $.bbq.getState();
                if (states.action === "ticket.menu"){
                    self.do_action({
                        type:   'ir.actions.client',
                        tag:    'ticket.menu',
                        target: 'current',
                    },{
                        clear_breadcrumbs: true,
                    });
                }
            });
            this.pickings = [];
            this.rows = [];
            this.get_picking_id();
            // Page table needs to be refreshing, 1 min.
            setInterval(this.get_picking_id, 60000);
        },

        get_picking_id: function(){
            var priorityDocMax = 11;
            var priorityShipMax = 11;
            var htmlOrdered = new Array(priorityShipMax);
            for (var i = 1; i < priorityShipMax; i++) {
                htmlOrdered[i] = new Array(priorityDocMax);
            }
            var htmlUnordered = '';
            var self = this;
            var result = $.Deferred();
            var model = new instance.web.Model('configuration.assortment').call('get_pickings_ids', [] ).then(function(pickings_ids) {
                self.pickings = pickings_ids;
                result.resolve(pickings_ids);
                var html = "";
                for(var i = 0; i <= pickings_ids.length-1; i++) {
                    var picking_id = pickings_ids[i].picking_id;
                    pickings_ids[i].priority_ship = parseInt(pickings_ids[i].priority_ship);
                    pickings_ids[i].priority_doc = parseInt(pickings_ids[i].priority_doc);
                    html = '';
                    // Condicionar para solo agregar elementos con estado de transferido en false
                    if(pickings_ids[i].picking_state == "done") {
                        html += '<tr id="picking-line-' + picking_id + '" class="text-center hide-item';
                    } else {
                        html += '<tr id="picking-line-' + picking_id + '" class="text-center';
                    }
                    switch(pickings_ids[i].priority_ship) {
                        case 1:
                            html += ' danger';
                            break;
                        case 2:
                            html += ' warning';
                            break;
                        case 3:
                            html += ' info';
                            break;
                        case 4:
                            html += ' success';
                            break;
                        case 5:
                            html += ' active';
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            break;
                    }
                    html += '">';
                    html += '<td id="picking-name-'+ picking_id + '">' + check_value(pickings_ids[i].picking_name) + '</td>';
                    html += '<td id="picking-origin-' + picking_id +'">' + check_value(pickings_ids[i].picking_origin) + '</td>';
                    html += '<td id="delivery-car-name-' + picking_id + '">' + check_value(pickings_ids[i].delivery_car_name) + '</td>';
                    html += '<td id="creation-date-picking-' + picking_id + '">' + check_value(pickings_ids[i].creation_date_picking) + '</td>';
                    html += '<td id="partner-name-' + picking_id+'">'  + check_value(pickings_ids[i].partner_name) + '</td>';
                    html += '<td id="product-qty-' + picking_id +'">' + check_value(pickings_ids[i].product_qty) + '</td>';
                    html += '<td id="supplied-by-' + picking_id +'">' + check_value(pickings_ids[i].login_id) + '</td>';
                    if (pickings_ids[i].user_id) {
                        html += '<td id="user-id-' + picking_id + '">' + pickings_ids[i].user_id + '</td>';
                    } else {
                        html += '<td id="user-id-' + picking_id + '">No impreso</td>';
                    }
                    if (pickings_ids[i].print_date) {
                        html += '<td id="print-date-' + picking_id +'">' + pickings_ids[i].print_date + "</td>";
                    } else {
                        html += '<td id="print-date-' + picking_id +'">No impreso</td>';
                    }
                    html += '<td><input type="checkbox" name="db-id-' + picking_id + '" value="'+ picking_id +'"></td>';
                    html += '</tr>';

                    // Sales lines should be ordered
                    if (pickings_ids[i].prefix == 'SO') {
                        if (htmlOrdered[pickings_ids[i].priority_ship][pickings_ids[i].priority_doc] == undefined) {
                            htmlOrdered[pickings_ids[i].priority_ship][pickings_ids[i].priority_doc] = [];
                        }
                        htmlOrdered[pickings_ids[i].priority_ship][pickings_ids[i].priority_doc].push(html);
                    } else {
                        htmlUnordered += html;
                    }
                }
                // All sales are ordered by priority_ship given at configuration
                var htmlToView = '';
                for (var j = 1; j < priorityShipMax; j++) {
                    for (var k = 1; k < priorityDocMax; k++) {
                        if (htmlOrdered[j][k]) {
                            htmlToView += htmlOrdered[j][k].join('');
                        }
                    }
                }
                //console.log(htmlToView)
                //console.log(htmlUnordered)
                self.$("#table-data-custom").html(htmlToView + htmlUnordered);
            });
            result.promise().then(function(ids){
                if (DEBUG){
                    console.log(ids, 'ids');
                }
            });
            if (DEBUG){
                    console.log(self.pickings);
                }
            return self.pickings;
        },

        load: function(warehouse_id){
            var self = this;
        },
        start: function(){
            console.log('Funcion start');
            this._super();
            var self = this;
            instance.webclient.set_content_full_screen(true);
        },
        refresh_ui: function(warehouse_id){
            var self = this;
        },
        destroy: function(){
            this._super();
            instance.webclient.set_content_full_screen(false);
        },
        renderElement: function(){
            console.log('Funcion render');
            var self = this;
            this._super();
        },
    });
    openerp.web.client_actions.add('ticket.ui', 'instance.ticket.TicketMainWidget');

}

openerp.ticket = function(openerp) {
    openerp.ticket = openerp.ticket || {};
    openerp_ticket_widgets(openerp);
};

