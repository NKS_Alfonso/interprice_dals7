��          �      �           	           .  
   G  
   R     ]     m     {     �     �     �     �     �     �     �      �            	     A   %  K   g  A   �  E  �     ;     T     c  
   {  	   �     �     �  	   �     �     �     �     �     �          ,     5     U     a     q  H   z  ;   �  4   �             	                    
                                                                       Active Delivery Method Configuration Configuration Assortment Created by Created on Delivery Method Delivery line Document Document line Document type ID Impresor de surtido Last Updated by Last Updated on Quantity Quantity must be greater than 0. Sum delivery Sum document Warehouse You can not assign larger delivery method quantities to documents You can not assign the same delivery method more than once to the document. You can not assign the same document more than once to the store. Project-Id-Version: Odoo Server 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-06-14 00:08+0000
PO-Revision-Date: 2017-06-13 19:22-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_MX
X-Generator: Poedit 1.8.7.1
 Método de envío activo Configuración Configuración impresor Creado por Creado en Método de envío Lineas de envio Documento Lineas de documento Tipo de documento ID Impresor de surtido Última actualizacón por Última actualizacón en Cantidad La cantidad debe ser mayor a 0. Suma envios Suma documentos Almacén No puede asignar cantidades de métodos de envio mayor a los documentos. No puede asignar el mismo método de envio más de una vez. No puede asignar el mismo documento más de una vez. 