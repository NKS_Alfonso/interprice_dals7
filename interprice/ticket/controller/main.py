# -*- coding: utf-8 -*-
# Copyright © 2017 To-Do - All Rights Reserved
# Author      TO-DO Developers

from openerp import http
from openerp.http import request
import logging
import json


_logger = logging.getLogger(__name__)


class AssortmentImprintedController(http.Controller):

    @http.route(['/assortment/imprinted/ticket'], type='http', auth='user')
    def assortment_imprinted_ticket(self, debug=False, **k):
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/assortment/imprinted/ticket')
        return request.render('ticket.todo_tmpl_assortment_imprinted_ticket')

    @http.route(['/assortment/process/ticket'], type='http', auth='user')
    def assortment_process_ticket(self, picking_id, user_name, type_req):
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/assortment/imprinted/ticket')
        if user_name == 'Desconocido':
            return json.dumps({'error': {}})
        picking_id_obj = request.env['stock.picking'].sudo(request.session.uid).browse(int(picking_id))
        print_picking_obj = request.env['stock.picking.print'].sudo(request.session.uid)
        resp = {}

        if type_req == u'GET' or picking_id_obj.print_ids:
            resp = {
                'data': {
                    'user_name': picking_id_obj.print_ids[0].user_name if picking_id_obj.print_ids else 'No impreso',
                    'print_date': picking_id_obj.print_ids[0].print_date if picking_id_obj.print_ids else 'No impreso',
                    'stock_picking_id': picking_id_obj.id or 'false',
                    'stock_picking_print_id': picking_id_obj.print_ids[0].id if picking_id_obj.print_ids else 'false',
                    'supplier_user': picking_id_obj.print_ids[0].supplier_user.login if picking_id_obj.print_ids else 'No surtido'
                }
            }
        elif type_req == u'POST' and not picking_id_obj.print_ids:
            try:
                vals = {
                    'user_name': user_name,
                    'stock_picking_id': str(picking_id_obj.id)
                }
                stock_picking_print = print_picking_obj.sudo(request.session.uid).create(vals)
                resp = {
                    'data': {
                        'user_name': user_name,
                        'print_date': stock_picking_print.print_date,
                        'stock_picking_id': stock_picking_print.stock_picking_id.id,
                        'stock_picking_print_id': stock_picking_print.id
                    }
                }
            except Exception as e:
                return e
        return json.dumps(resp or {'error': {}})

    @http.route(['/assortment/get/ticket'], type='http', auth='user')
    def assortment_search_ticket(self, folio):
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/assortment/imprinted/ticket')
        picking_obj = request.env['stock.picking'].sudo(request.session.uid)
        picking_id = picking_obj.search([('origin', '=', folio)])
        if picking_id:
            resp = {
                'data': {
                    'id': picking_id.id,
                    'doc': picking_id.name,
                    'create_date': picking_id.create_date,
                    'user_name': picking_id.print_ids[0].user_name if picking_id.print_ids else 'No impreso',
                    'print_date': picking_id.print_ids[0].print_date if picking_id.print_ids else 'No impreso',
                    'supplier_user': picking_id.print_ids[0].supplier_user.login if picking_id.print_ids else 'No surtido'
                }
            }
        return json.dumps(resp or {'error': {}})
