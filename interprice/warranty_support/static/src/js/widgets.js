openerp.warranty_support = function(instance, m)
{
	var _t = instance.web._t,
	_lt = instance.web._lt;
	QWeb = instance.web.qweb;
	var fields;
	var parent;
	instance.web.form.widgets = instance.web.form.widgets.extend(
		{
			'one2many_selectable' : 'instance.web.form.One2ManySelectable',
		});

		instance.web.form.One2ManySelectable = instance.web.form.FieldOne2Many.extend(
			{
				multi_selection: true,
				start: function()
				{
					this._super.apply(this, arguments);
					var self=this;
					this.$el.prepend(QWeb.render("One2ManySelectable", {widget: this}));
					this.$el.find(".ep_button_confirm").click(function(){
						self.action_selected_lines();

					});
					parent = this.getParent();
					fields = parent.fields;
				},

				action_selected_lines: function()
				{
					var self=this;
					selected_ids=self.get_selected_ids_one2many();

					for(i=0;i<selected_ids.length;i++)
					{
						if(isNaN(selected_ids[i]))
						{
							new instance.web.Dialog(this, {
								title: _t("Warning"),
								size: 'medium',
							}, $("<div />").text(_t("Some selected items have not been saved! " +
							"Please save the record first before proceeding."))).open();
							return false;
						}
					}
					var model_obj=new instance.web.Model(this.dataset.model);
					model_obj.call('get_selected_lines',[selected_ids],{context:self.dataset.context, traffic:fields.id.get_value()})
					.then(function(result){
						fields.received_user.set_value(result.received_user);
						fields.received_date.set_value(result.received_date);
						parent.save(true);
					});
					return true;

				},
				get_selected_ids_one2many: function ()
				{
					var ids =[];
					this.$el.find('th.oe_list_record_selector input:checked')
					.closest('tr').each(function () {
						var children = $(this).children();
						ids.push(parseInt($(this).context.dataset.id));
						$(children[8]).text(_t('Received in corporative'));
					});

					this.$el.find('th.oe_list_record_selector input:not(:checked)')
					.closest('tr').each(function(){
						var children = $(this).children();
						$(children[8]).text(_t('Rejected Traffic'));
					});

					this.$el.find('th.oe_list_record_selector input').removeAttr('checked');
					this.$el.find('th.oe_list_record_selector').removeAttr('checked');

					return ids;
				},
			});

		}
