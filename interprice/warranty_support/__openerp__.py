# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

{
    'name': 'Warranty and Support',
    'version': '0.1',
    'author': 'Copyright © 2018 TO-DO - All Rights Reserved',
    'category': '',
    'description': """
        Warranty and Support Module
         """,
    'website': 'http://www.grupovadeto.com',
    'license': 'AGPL-3',
    'depends': [
        'web',
        'base',
        'account_cost_center',
        'centro_de_costos',
        'account',
        'stock',
        'product',
    ],
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/default_output_view.xml',
        'views/technician_diagnosis_view.xml',
        'views/service_order_view.xml',
        'views/credit_note_request.xml',
        'views/multiple_movements.xml',
        'views/services_catalogue_view.xml',
        'views/failures_catalogue_view.xml',
        'views/solutions_catalogue_view.xml',
        'views/prefix_catalogue_view.xml',
        'wizard/assign_technician.xml',
        'wizard/assign_rma_number.xml',
        'wizard/delivery_data.xml',
        'wizard/cancel_sd.xml',
        'views/traffic_view.xml',
        "views/web_assets.xml",
        'wizard/assign_credit_note.xml',
        'views/res_config_view.xml',
        'report/product_delivery.xml',
        'report/product_delivery_template.xml',
        'report/service_orders_report.xml',
        'report/service_order_report.xml',
        'report/service_order_report_template.xml',
        'report/traffic_order_report.xml',
        'report/traffic_order_report_template.xml'
    ],
    "qweb": [
        'static/src/xml/widget_view.xml',
    ],
    'installable': True,
    'active': False,
    "application": False,
    "external_dependencies": {
        'python': [],
    },
}
