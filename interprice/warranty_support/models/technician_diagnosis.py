# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _


class TechnicianDiagnosis(models.Model):
    _name = 'technician.diagnosis'
    _inherit = ['mail.thread']

    def get_service_order(self):
        so_obj = self.env['service.order']
        data = so_obj.browse(self._context.get('active_id'))
        return data.id

    name = fields.Char(_('Diagnosis'), readonly=True)
    service_order = fields.Many2one('service.order', _('Service Order'),
                                    default=get_service_order, readonly=True)
    so_date = fields.Datetime(_('Reception'),
                          related='service_order.create_date', readonly=True)
    so_priority = fields.Selection(
        _('Priority'),
        related='service_order.priority',
        readonly=True
    )
    so_supplier = fields.Char(
        _('Supplier'),
        related='service_order.supplier.name',
        readonly=True
    )
    so_product = fields.Char(
        _('Product'),
        related='service_order.product.name',
        readonly=True
    )
    so_product_line = fields.Char(
        _('Product Line'),
        related='service_order.product_line',
        readonly=True
    )
    so_failure = fields.Char(
        _('Failure'),
        related='service_order.failure_type.name',
        readonly=True
    )
    so_technician = fields.Char(
        _('Technician'),
        related='service_order.technician.name',
        readonly=True
    )
    solution = fields.Many2one(
        'solutions.catalogue', _('Solution'), required=True)
    observations = fields.Text(_('Observations'))
    service_order_status = fields.Selection([
        ('fixed', _('Fixed')),
        ('no_fix', _('Not Fixed'))
    ], string=_("Service Order Status"), index=True, required=True)

    @api.one
    def create_function(self):
        pass

    @api.model
    def create(self, vals):
        res = super(TechnicianDiagnosis, self).create(vals)
        # We validate the length of service_order as it comes as an object
        # if it has an ID then the length is > 0
        if len(res.service_order) > 0:
            so_data = self.env['service.order'].\
                browse(self._context.get('active_id'))

            if vals['service_order_status'] == 'fixed':
                so_data.write({'status': 'fixed'})
            if vals['service_order_status'] == 'no_fix':
                so_data.write({'status': 'no_fix'})

            if vals['solution']:
                so_data.write({'solution_type': vals['solution']})

            if res:
                so_data.write({'diagnosis': res.id})

            res['name'] =\
                self.env['ir.sequence'].next_by_code(
                    'technician.diagnosis.folio')
        else:
            raise exceptions.Warning(_('You cannot create a Diagnosis without\
         a linked Service Order information'))
        return res
