# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import models, fields, api, _


class SolutionsCatalogue(models.Model):
    _name = 'solutions.catalogue'
    _inherit = ['mail.thread']

    def get_technicians(self):
        group_obj = self.env['res.groups']
        data = group_obj.search([('name', '=', _('Technician'))])
        users = data.users
        return [('id', 'in', users.ids)]

    name = fields.Char(_('Solution'), required=True)
    technician = fields.Many2one(
        'res.users',
        _('Technician'),
        domain=get_technicians
    )
    description = fields.Text(_('Description'))
    time_required = fields.Integer(_('Time required'))
    time_type = fields.Selection([
        ('minutes', _('Minute(s)')),
        ('hours', _('Hour(s)')),
        ('days', _('Day(s)')),
        ('weeks', _('Week(s)')),
        ('months', _('Month(s)')),
        ('years', _('Year(s)'))],
        string="Time type",
        index=True,
    )
    create_date = fields.Date(_('Creation Date'),
                              default=lambda self: fields.datetime.now(),
                              readonly=True)
