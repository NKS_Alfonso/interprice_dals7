# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, exceptions, _
from dateutil.relativedelta import relativedelta
import datetime


class ServiceOrder(models.Model):
    _name = "service.order"
    _rec_name = "name"
    _inherit = ['mail.thread']

    # We validate the user's center costs assigned that he only can assign
    @api.model
    def center_cost_domain(self):
        res = []
        res = self.env.user.centro_costo_id.ids
        if not res:
            raise exceptions.Warning(
                _("You don't have a center cost assigned"))
        return [('id', 'in', res)]

    name = fields.Char(_('Service Order'), readonly=True)
    create_user = fields.Many2one(
        'res.users',
        _('Received'),
        default=lambda self: self.env.user,
        readonly=True
    )
    create_date = fields.Datetime(_('Creation Date'), default=lambda self:
                              fields.datetime.now(), readonly=True)
    stock_validate_compute = fields.Integer(default=0)
    customer = fields.Many2one('res.partner', _('Customer'))
    customer_contact = fields.Char(_('Entregó'))
    supplier = fields.Many2one('res.partner', _('Supplier'), readonly=True)
    invoice = fields.Many2one('account.invoice', _('Invoice'))
    internal_wh_move = fields.Many2one('document.inventory',
                                       _('Internal Warehouse Movement'))
    default_output = fields.Many2one(
        'default.output',
        _('Default Output'),
        readonly=True
    )
    date_of_issue_qweb = fields.Datetime(_('Date of issue'))
    delivery_date_qweb = fields.Datetime(_('Delivery Date'))
    related_document_qweb = fields.Char(_('Related Document'))
    document_date = fields.Date(_('Date'), readonly=True)
    product = fields.Many2one('product.product', _('Product'), required=True)
    product_serie = fields.Char(_('Serie'))
    product_line = fields.Char(
        _('Line'),
        related='product.product_tmpl_id.line.name',
        readonly=True
    )
    accessories = fields.Char(_('Accessories'))
    warranty_date = fields.Date(_('Warranty end date'), readonly=True)
    warranty_notification = fields.Char(_('Warranty?'), readonly=True)
    warranty_user = fields.Many2one('res.users', _('Receiver User'),
                                    readonly=True)
    warranty_type = fields.Selection([
                    ('local', _('Local')),
                    ('foreign', _('Foreign'))
                ], string=_('Tipo de Garantía'))
    stock_avaliable = fields.Char(_('Stock Information'), readonly=True)
    service_type = fields.Many2one('services.catalogue', _('Service'),
                                   required=True)
    failure_type = fields.Many2one('failures.catalogue', _('Failure'),
                                   required=True)
    solution_type = fields.Many2one('solutions.catalogue', _('Solution'),
                                    readonly=True)
    technician = fields.Many2one('res.users', _('Technician'), readonly=True)
    diagnosis = fields.Many2one('technician.diagnosis', _('Diagnosis'),
                                readonly=True)
    credit_note = fields.Many2one('account.invoice', _('Credit Note'),
                                  readonly=True)
    observations = fields.Text(_('Observations'))
    delivered_to = fields.Char(_('Delivered to'), readonly=True)
    delivery_observations = fields.Text(_('Delivery Observations'), readonly=True)

    branch_office = fields.Many2one(
        'account.cost.center',
        domain=center_cost_domain,
        string=_("Branch office"),
        required=True
    )

    document_model_ref = fields.Selection([
        ('invoice', _('Invoice')),
        ('stocking_wh', _('Warehouse Move')),
        # ('stocking_suc', _('Internal Sucursal Move')),
        # ('def_output', _('Default Output'))
    ], string=_("Support Document"),
    )

    priority = fields.Selection([
        ('low', _('Low')),
        ('medium', _('Medium')),
        ('high', _('High'))
    ], index=True, string=_("Priority"),
        required=True
    )

    status = fields.Selection([
        ('received', _('Received')),
        ('assigned', _('Assigned')),
        ('no_fix', _('Not Fixed')),
        ('authorized_sd', _('SD Authorized')),
        ('ready_sd', _('SD Ready')),
        ('fixed', _('Fixed')),
        ('delivered', _('Delivered')),
        ('cancel', _('Canceled'))
    ],  default='received',
        index=True,
        track_visibility='always'
    )

    state_a = fields.Selection(related='status', track_visibility=False)
    state_b = fields.Selection(related='status', track_visibility=False)
    state_c = fields.Selection(related='status', track_visibility=False)
    state_d = fields.Selection(related='status', track_visibility=False)

    #ADITIONAL FIELDS FOR TREE REPORT

    supplier_code = fields.Char(
        related="supplier.supplier_number",
        string=_("Supplier code")
    )

    warehouse = fields.Char(
        related="default_output.related_wh_move.warehouse_id.name",
        string=_("Warehouse")
    )

    customer_code = fields.Char(
        related="customer.client_number",
        string=_("Customer code")
    )

    product_code = fields.Char(
        related="product.product_tmpl_id.default_code",
        string=_("Product code")
    )

    serie = fields.Char(default='')

    doc_ref_folio_invoice = fields.Char(
        related="invoice.number",
        string=_("Reference Document Folio (INVOICE)")
    )

    doc_ref_folio_wh = fields.Char(
        related="internal_wh_move.number",
        string=_("Reference Document Folio (WAREHOUSE MOVE)")
    )

    reception_date = fields.Datetime(
        related="write_date",
        string=_("Reception Date")
    )

    warehouse_delivery_date = fields.Datetime(
        related="default_output.related_wh_move.picking_id.date_done",
        string=_("Warehouse Delivery Date")
    )

    promedy_cost = fields.Float(
        related="product.product_tmpl_id.average_cost",
        string=_("Promedy Cost")
    )

    @api.constrains('observations')
    def _constrain_observations_field(self):
        if self.observations:
            if len(self.observations) > 500:
                raise exceptions.ValidationError(_("""
                        You can only write 500 characters in observations field
                    """))

    @api.one
    def cancel(self):
        self.write({
            'status': 'cancel'
        })

    def upper_string(self, string=''):
        if string:
            string = string.upper()
        return string

    def get_prefix(self):
        prefix_catalogue = self.env['prefix.catalogue']
        prefix = prefix_catalogue.search(
            [('branch_office','=',self.branch_office.id)]).prefix
        return prefix

    def get_document_ref_value(self):
        return dict(self._fields['document_model_ref'].\
            _description_selection(self.env)).get(self.document_model_ref)

    def get_warranty_type_name(self):
        return dict(self._fields['warranty_type'].\
            _description_selection(self.env)).get(self.warranty_type)

    def get_company_name(self):
        return self.env.user.company_id.name

    @api.multi
    def print_so(self):
        return self.env['report'].get_action(
            self,
            'warranty_support.service_order_report_template'
        )

    @api.multi
    def open_delivery_data_wizard(self):
        if not self.delivered_to or not self.delivery_observations:
            wizard_form = self.env.ref(
                'warranty_support.delivery_data_form_view',
                False
            )

            view_id = self.env['delivery.data']
            vals = {}
            new = view_id.create(vals)

            return {
                'name': 'Delivery Data',
                'type': 'ir.actions.act_window',
                'res_model': 'delivery.data',
                'res_id': new.id,
                'view_id': wizard_form.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new'
            }
        else:
            return self.env['report'].get_action(
                self,
                'warranty_support.product_delivery_template'
            )

    @api.multi
    def print_delivery(self):
        self.filtered(
            lambda s: s.status == 'ready_sd' or s.status == 'fixed'\
            or s.status == 'no_fix'
        ).write({'status': 'delivered'})
        if not self.date_of_issue_qweb:
            self.write({'date_of_issue_qweb': datetime.datetime.now()})
        if not self.related_document_qweb:
            if self.invoice:
                self.write({'related_document_qweb': str(self.invoice.number)})
            elif self.internal_wh_move:
                self.write({'related_document_qweb':
                            str(self.internal_wh_move.number)})
            elif self.default_output:
                self.write({'related_document_qweb':
                            str(self.default_output.name)})
        if not self.delivery_date_qweb:
            self.write({'delivery_date_qweb': datetime.datetime.now()})
        action_dict = self.env['report'].get_action(
            self,
            'warranty_support.product_delivery_template'
        )
        del action_dict['report_type']
        return action_dict

    # When changing the type of document a new field shows with its domain
    @api.onchange('document_model_ref')
    def onchange_document_model_ref(self):
        conf_obj = self.env['internal.warranty.support.config']
        data = conf_obj.search([], limit=1)
        product_ids = []

        if self.document_model_ref == 'invoice':
            self.internal_wh_move = False
            self.default_output = False
            self.invoice = False
            self.document_date = False
            self.product = False
            # if self.invoice:
            self.document_date = self.invoice.invoice_datetime
            for record in self.invoice.invoice_line:
                if record.product_id.product_tmpl_id.type != 'service':
                    product_ids.append(record.product_id.id)
            return {
                'domain': {
                    'invoice': [
                        ('partner_id', '=', self.customer.id),
                        ('type', '=', 'out_invoice'),
                        ('state', 'not in', ['draft','cancel'])
                    ],
                    'internal_wh_move': False,
                    'product': [('id', 'in', product_ids)]
                }
            }

        # We use internal_wh_move for the next three
        # conditions so we dont create another variable for each one
        elif self.document_model_ref == 'stocking_wh':
            self.invoice = False
            self.default_output = False
            self.internal_wh_move = False
            self.document_date = False
            self.product = False
            # if self.internal_wh_move:
            self.document_date = self.internal_wh_move.date
            for record in self.internal_wh_move.lines_ids:
                if record.product_id.product_tmpl_id.type != 'service':
                    product_ids.append(record.product_id.id)
            return {
                'domain': {
                    'internal_wh_move': [('type_document.type_operation', '=',
                                          'outgoing'),('state','=','supplied')],
                    'invoice': False,
                    'product': [('id', 'in', product_ids)]
                }
            }

        # elif self.document_model_ref == 'stocking_suc':
        #     self.invoice = False
        #     self.default_output = False
        #     self.internal_wh_move = False
        #     self.document_date = False
        #     self.product = False
        #     # if self.internal_wh_move:
        #     self.document_date = self.internal_wh_move.date
        #     for record in self.internal_wh_move.lines_ids:
        #         if record.product_id.product_tmpl_id.type != 'service':
        #             product_ids.append(record.product_id.id)
        #     return {
        #         'domain': {
        #             'internal_wh_move': [('type_document', '=',
        #                                   data.intersucursal_doc_type_id.id)],
        #             'invoice': False,
        #             'product': [('id', 'in', product_ids)]
        #         }
        #     }
        #
        # elif self.document_model_ref == 'def_output':
        #     self.internal_wh_move = False
        #     self.invoice = False
        #     self.internal_wh_move = False
        #     self.document_date = False
        #     self.product = False
        #     # if self.internal_wh_move:
        #     self.document_date = self.internal_wh_move.date
        #     for record in self.internal_wh_move.lines_ids:
        #         if record.product_id.product_tmpl_id.type != 'service':
        #             product_ids.append(record.product_id.id)
        #     return {
        #         'domain': {
        #             'internal_wh_move': [('type_document', '=',
        #                                   data.type_document.id)],
        #             'invoice': False,
        #             'product': [('id', 'in', product_ids)]
        #         }
        #     }
        else:
            return {'domain': {'product': False}}

    @api.onchange('product')
    def onchange_product(self):
        self.warranty_date = False
        self.warranty_notification = False
        if self.product:
            if self.invoice or self.internal_wh_move and self.document_date:
                date_now = datetime.datetime.now()
                doc_date = datetime.datetime.strptime(self.document_date, "%Y-%m-%d")
                year_sum = doc_date + relativedelta(years=1)
                warranty_date = datetime.datetime.strftime(year_sum, "%Y-%m-%d")
                self.update({'warranty_date': warranty_date})
                if date_now.date() <= year_sum.date():
                    self.update({'warranty_notification': _('In warranty')})
                else:
                    self.update({'warranty_notification': _('Not in warranty')})

    # When changing an invoice, domain is set
    # with the products that are in that invoice
    @api.onchange('invoice')
    def onchange_invoice(self):
        product_ids = []
        if self.invoice:
            self.document_date = self.invoice.invoice_datetime
            for record in self.invoice.invoice_line:
                if record.product_id.product_tmpl_id.type != 'service':
                    product_ids.append(record.product_id.id)
            self.product = False
            self.warranty_date = False
            self.warranty_notification = False
            return {'domain': {'product': [('id', 'in', product_ids)]}}

    # When changing an invoice, domain
    # is set with the products that are in that invoice
    @api.onchange('internal_wh_move')
    def onchange_internal_wh_move(self):
        product_ids = []
        if self.internal_wh_move:
            self.document_date = self.internal_wh_move.date
            for record in self.internal_wh_move.lines_ids:
                if record.product_id.product_tmpl_id.type != 'service':
                    product_ids.append(record.product_id.id)
            self.product = False
            self.warranty_date = False
            self.warranty_notification = False
            return {'domain': {'product': [('id', 'in', product_ids)]}}

    # When changing customer, changes domain for invoices
    @api.onchange('customer')
    def onchange_customer(self):
        if self.customer:
            self.invoice = False
            self.document_date = False
            self.product = False
            return {
                'domain': {
                    'invoice': [
                        ('partner_id', '=', self.customer.id),
                        ('type', '=', 'out_invoice'),
                        ('state', 'not in', ['draft','cancel'])
                    ],
                    'product': False
                }
            }
        else:
            return {'domain': {'invoice': []}}

    # Button to call Default Output form view
    # to create an SD under this OS values
    @api.multi
    def generate_sd(self):
        wizard_form = self.env.ref(
            'warranty_support.default_output_form_view',
            False
        )

        view_id = self.env['default.output']
        vals = {
            'related_service_order': self.id,
            'product': self.product.id,
            'product_serie': self.product_serie,
        }
        new = view_id.create(vals)

        return {
            'name': 'Default Output',
            'type': 'ir.actions.act_window',
            'res_model': 'default.output',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    # Button to assign a technician to the current OS
    @api.multi
    def assign_technician(self):
        wizard_form = self.env.ref(
            'warranty_support.assign_technician_form_view',
            False
        )

        view_id = self.env['assign.technician']
        vals = {}
        new = view_id.create(vals)

        return {
            'name': 'Assign Technician',
            'type': 'ir.actions.act_window',
            'res_model': 'assign.technician',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    # Button to assign a credit note to the current OS
    @api.multi
    def assign_credit_note(self):
        wizard_form = self.env.ref(
            'warranty_support.assign_credit_note_form_view',
            False
        )

        view_id = self.env['assign.credit.note']
        vals = {}
        new = view_id.create(vals)

        return {
            'name': 'Assign Credit Note',
            'type': 'ir.actions.act_window',
            'res_model': 'assign.credit.note',
            'res_id': new.id,
            'view_id': wizard_form.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new'
        }

    @api.multi
    def copy(self):
        raise exceptions.Warning(_("You can't duplicate service orders"))

    @api.multi
    def validate_stock(self):
        warranty_conf_obj = self.env['warranty.support.config']
        warranty_config = warranty_conf_obj.search([], limit=1)
        if warranty_config.warehouse_id:
            warehouse_id = warranty_config.warehouse_id.id
            product = self.product.id
            self._cr.execute("""SELECT SUM(sq.qty)
                        FROM stock_warehouse AS sw INNER JOIN
                        stock_location AS sl ON sw.lot_stock_id = sl.id
                        INNER JOIN stock_quant sq ON sl.id=sq.location_id
                        AND reservation_id is null
                        WHERE sl.usage='internal' and sw.id=%s
                        and product_id=%s""", (warehouse_id, product))
            if self._cr.rowcount:
                qty = self._cr.fetchone()[0]
                flag = False
                if qty > 0:
                    self.stock_validate_compute = 1
                    self.stock_avaliable = _('Stock avaliable: %.2f %s(s)')%(qty,
                                    self.product.product_tmpl_id.uom_id.name)
                else:
                    if not qty:
                        qty = 0.0
                    self.stock_validate_compute = 2
                    self.stock_avaliable = _('Stock avaliable: %.2f %s(s)')%(qty,
                                    self.product.product_tmpl_id.uom_id.name)

    @api.model
    def create(self, vals):
        res = super(ServiceOrder, self).create(vals)
        if res.invoice:
            res.update({'document_date': res.invoice.invoice_datetime})
        if res.internal_wh_move:
            res.update({'document_date': res.internal_wh_move.date})
        if res.invoice or res.internal_wh_move and res.document_date and res.product:
            date_now = datetime.datetime.now()
            doc_date = datetime.datetime.strptime(res.document_date, "%Y-%m-%d")
            year_sum = doc_date + relativedelta(years=1)
            warranty_date = datetime.datetime.strftime(year_sum, "%Y-%m-%d")
            res.update({'warranty_date': warranty_date})
            if date_now.date() <= year_sum.date():
                res.update({'warranty_notification': _('In warranty')})
                res.update({'warranty_user': self.env.user})
            else:
                res.update({'warranty_notification': _('Not in warranty')})
                res.update({'warranty_user': self.env.user})
        pref_obj = self.env['prefix.catalogue']
        prefix = pref_obj.search([
            ('branch_office', '=', vals['branch_office'])
        ]).prefix
        if prefix:
            res.name = prefix + str(res.id)
        else:
            raise exceptions.Warning(_('You must create a Prefix Catalogue \
                before creating a Service Order'))
        return res

    @api.multi
    def write(self, vals):
        vals_invoice = vals.get('invoice')
        vals_internal_wh_move = vals.get('internal_wh_move')
        document_date = ''
        if vals_invoice:
            invoice_obj = self.env['account.invoice']
            invoice = invoice_obj.browse(vals_invoice)
            document_date = invoice.invoice_datetime
            document_date = datetime.datetime.strptime(document_date, "%Y-%m-%d %H:%M:%S").date()
            document_date = datetime.datetime.strftime(document_date, "%Y-%m-%d")
        if vals_internal_wh_move:
            move_obj = self.env['document.inventory']
            move = move_obj.browse(vals_internal_wh_move)
            document_date = move.date
        if document_date:
            vals['document_date'] = document_date
            if 'document_date' in vals:
                if vals_invoice or vals_internal_wh_move and 'product' in vals:
                    date_now = datetime.datetime.now()
                    doc_date = datetime.datetime.strptime(vals.get('document_date'), "%Y-%m-%d")
                    year_sum = doc_date + relativedelta(years=1)
                    warranty_date = datetime.datetime.strftime(year_sum, "%Y-%m-%d")
                    vals.update({'warranty_date': warranty_date})
                    if date_now.date() <= year_sum.date():
                        vals.update({'warranty_notification': _('In warranty')})
                    else:
                        vals.update({'warranty_notification': _('Not in warranty')})
            print vals
        return super(ServiceOrder, self).write(vals)
