# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _
import calendar
import datetime
from datetime import timedelta


class Traffic(models.Model):
    _name = 'traffic'
    _rec_name = "id"
    _inherit = ['mail.thread']

    traffic_lines = fields.One2many(
        'traffic.lines',
        'traffic_id',
        'Traffic lines'
    )

    received_date = fields.Datetime(_('Received date'))
    received_user = fields.Many2one('res.users', _('Received User'))

    def str_id(self, id=0):
        return str(id)

    def format_date(self, date=''):
        str_date = ''
        traduced_months = [
                "",
                _("Jan"),
                _("Feb"),
                _("Mar"),
                _("Apr"),
                _("May"),
                _("Jun"),
                _("Jul"),
                _("Aug"),
                _("Sep"),
                _("Oct"),
                _("Nov"),
                _("Dec")
            ]
        if date:
            date_t = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
            day = date_t.day
            month = _(calendar.month_abbr[date_t.month])
            year = date_t.year
            str_date = "%s %s %s"%(str(day), month, str(year))
        return str_date

    @api.multi
    def print_traffic_order(self):
        return self.env['report'].get_action(
            self,
            'warranty_support.traffic_order_report_template'
        )
