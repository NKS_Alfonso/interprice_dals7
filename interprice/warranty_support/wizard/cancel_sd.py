# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, api, _


class CancelSD(models.TransientModel):
    _name = 'cancel.sd'



    observations = fields.Text(_('Observations'), required=True)

    @api.multi
    def action_yes(self):
        if 'active_id' in self._context:
            active_id = self._context.get('active_id')
            sd_obj = self.env['default.output']
            sd_id = sd_obj.browse(active_id)
            wizard_form = self.env.ref(
                'warranty_support.default_output_form_view',
                False
            )

            vals = {
                'related_service_order': sd_id.related_service_order.id,
                'product': sd_id.related_service_order.product.id,
                'product_serie': sd_id.related_service_order.product_serie,
            }
            new = sd_obj.create(vals)
            if new.id:
                sd_id.related_service_order.default_output = new.id
                sd_id.observations = self.observations
                sd_id.status = 'cancelled'

            return {
                'name': 'Default Output',
                'type': 'ir.actions.act_window',
                'res_model': 'default.output',
                'res_id': new.id,
                'sd_obj': wizard_form.id,
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new'
            }

    @api.multi
    def action_no(self):
        if 'active_id' in self._context:
            active_id = self._context.get('active_id')
            sd_obj = self.env['default.output']
            sd_id = sd_obj.browse(active_id)
            sd_id.observations = self.observations
            sd_id.status = 'cancelled'
