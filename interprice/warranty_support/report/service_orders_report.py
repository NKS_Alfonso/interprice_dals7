# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, fields, models, _
from openerp import tools
from olib.oCsv.oCsv import OCsv
from datetime import datetime as dt
from dateutil import tz


class ServiceOrdersReport(models.Model):
    _name = 'service.order.report'

    start_date = fields.Date(
        string=_("Start Date"),
        required=True,
    )
    end_date = fields.Date(
        string=_("End Date"),
        required=True,
    )

    def get_ids(self):
        sql = """
            SELECT id FROM service_order AS so
            WHERE so.create_date IS NOT NULL
            AND so.create_date >= '{0}'
            AND so.create_date <= '{1}'
        """.format(self.start_date, self.end_date)
        return sql

    @api.multi
    def screen_report(self):
        tree_form = self.env.ref(
            'warranty_support.warranty_support_report_tree',
            False
        )
        sql = self.get_ids()
        self.env.cr.execute(sql)

        so_ids = self.env.cr.fetchall()

        return {
            'name': 'Service Order Report Data',
            'type': 'ir.actions.act_window',
            'res_model': 'service.order',
            'view_id': tree_form.id,
            'view_type': 'tree',
            'view_mode': 'tree',
            'target': 'current',
            'domain': [('id', 'in', so_ids)],
        }

    @api.multi
    def file_report(self):
        id = self.to_csv_by_ids()

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id.id),
            'target': 'self',
        }

    def to_csv_by_ids(self):
        service_order_obj = self.env['service.order']
        pref_obj = self.env['prefix.catalogue']
        sql = self.get_ids()
        self.env.cr.execute(sql)

        so_dict_ids = self.env.cr.dictfetchall()
        so_ids = []
        for record in so_dict_ids:
            so_ids.append(record.get('id'))
        service_orders = service_order_obj.browse(so_ids)

        dataset_csv = [
            [
                _("Folio"),
                _("Supplier code"),
                _("Supplier"),
                _("Warehouse"),
                _("Customer code"),
                _("Customer"),
                _("Status"),
                _("Product code"),
                _("Product Description"),
                _("Serie"),
                _("Warranty"),
                _("Technician name"),
                _("Reception Date"),
                _("Type of Reference Document"),
                _("Reference Document Folio"),
                _("Service"),
                _("Priority"),
                _("Warehouse Delivery Date"),
                _("Credit Note Folio"),
                _("SD Folio"),
                _("Promedy Cost")
            ]
        ]

        for row in service_orders:
            if row.invoice:
                document_ref_folio = row.invoice.number
            elif row.internal_wh_move:
                document_ref_folio = row.internal_wh_move.number
            dataset_csv.append([
                unicode(self.set_default(row.name)).encode('utf8'),
                unicode(self.set_default(row.supplier.supplier_number
                    if row.supplier.supplier_number else 'S/N')).encode('utf8'),
                unicode(self.set_default(row.supplier.name if row.supplier.name
                    else 'S/N')).encode('utf8'),
                unicode(self.set_default(row.default_output.related_wh_move.warehouse_id.name
                    if row.default_output.related_wh_move.warehouse_id.name
                    else 'S/N')).encode('utf8'),
                unicode(self.set_default(row.customer.client_number
                    if row.customer.client_number else 'S/N')).encode('utf8'),
                unicode(self.set_default(row.customer.name)).encode('utf8'),
                unicode(self.set_default(_(dict(row._fields['status'].selection)\
                        .get(row.status)))).encode('utf8'),
                unicode(self.set_default(row.product.product_tmpl_id.default_code)).encode('utf8'),
                unicode(self.set_default(row.product.product_tmpl_id.name)).encode('utf8'),
                unicode(self.set_default('S/N')).encode('utf8'),
                unicode(self.set_default(row.warranty_notification)).encode('utf8'),
                unicode(self.set_default(row.technician.name)).encode('utf8'),
                unicode(self.utc_mexico_city(row.write_date)).encode('utf8'),
                unicode(self.set_default(_(dict(row._fields['document_model_ref'].selection)\
                        .get(row.document_model_ref)))).encode('utf8'),
                unicode(self.set_default(document_ref_folio if document_ref_folio else 'S/N')).encode('utf8'),
                unicode(self.set_default(row.service_type.name)).encode('utf8'),
                unicode(self.set_default(_(dict(row._fields['priority'].selection)\
                        .get(row.priority)))).encode('utf8'),
                unicode(self.utc_mexico_city(row.delivery_date_qweb)).encode('utf8'),
                unicode(self.set_default(row.credit_note.number
                    if row.credit_note.number else 'S/N')).encode('utf8'),
                unicode(self.set_default(row.default_output.name)).encode('utf8'),
                unicode(self.set_default(row.product.product_tmpl_id.average_cost)).encode('utf8')
            ])

        obj_ocsv = OCsv()
        file = obj_ocsv.csv_base64(dataset_csv)
        return self.env['r.download'].create(vals={
            'file_name': _('service_orders_report') + '.csv',
            'type_file': 'csv',
            'file': file
        })

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default

    def utc_mexico_city(self, date):
        if date:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('America/Mexico_City')
            utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S')
            utc = utc.replace(tzinfo=from_zone)
            central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
            return central
        else:
            return ''
