# -*- coding: utf-8 -*-

###############################################################################
#  @version     : 1.0                                                         #
#  @autor       : Viridiana Cruz Santos(gvadeto)2016                          #
#  @linea       : Maximo 80 chars                                             #
###############################################################################

# OpenERP imports
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs
###############################################################################
#                          Facturas de servicios                              #
###############################################################################


class facturas_x_servicio(osv.osv):
    # Nombre del modelo
    _name = 'gv.factura.x.servicio'
    # Nombre de la tabla
    _table = '_reporte_servicios'

    def imprimir_listado_facturas(self, cr, uid, ids, context=None):

        data = self.browse(cr, uid, ids)
        cr.execute("""
            UPDATE account_invoice set gv_factura_x_servicio_id=null
            """)

        if data.filtro_all_or_suplier == 'a':
            cr.execute("""
                UPDATE account_invoice SET gv_factura_x_servicio_id=%s 
                WHERE state IN ('paid', 'open', 'cancel') AND type='in_invoice'
                AND date_invoice>=%s AND date_invoice<=%s
                AND id IN (SELECT invoice_id FROM account_invoice_line p 
                            INNER JOIN product_product o ON o.id = p.product_id 
                            INNER JOIN product_template m ON m.id = o.id)
                """, (data.id, data.date_from, data.date_to,)
            )
        elif data.filtro_all_or_suplier == 'b':
            cr.execute("""
                UPDATE account_invoice SET gv_factura_x_servicio_id=%s 
                WHERE state IN ('paid', 'open', 'cancel') AND type='in_invoice'
                AND date_invoice>=%s AND date_invoice<=%s AND partner_id=%s
                AND id IN (SELECT invoice_id FROM account_invoice_line p 
                            INNER JOIN product_product o ON o.id = p.product_id 
                            INNER JOIN product_template m ON m.id = o.id)
                """, (data.id, data.date_from, data.date_to, data.partner_id.id,)
            )
        cr.commit()

    def action_wizard(self, cr, uid, ids, context=None):
        """Funcion que exporta a exel"""
        columna = "\"Proveedor\",\"Fecha factura\",\"Numero\",\"Responsable\",\"Fecha de vencimiento\",\"Documento origen\",\"Moneda\",\"Saldo pendiente\",\"Subtotal\",\"Total\",\"Estado\"\n"
        nombre_arch = "Facturas de servicios"
        data = self.browse(cr, uid, ids)

        for factura in data.invoice_ids:
            if factura.state == 'open': state = "Abierta"
            if factura.state == 'paid': state = "Pagada"
            if factura.state == 'cancel': state = "Cancelado"

            columna += "\""+unicode(factura.partner_id.name).encode("latin-1")+"\","\
              +"\""+unicode(factura.date_invoice).encode("latin-1")+"\","\
              +"\""+unicode(factura.number).encode("latin-1")+"\","\
              +"\""+unicode(factura.user_id.name).encode("latin-1")+"\","\
              +"\""+unicode(factura.date_due).encode("latin-1")+"\","\
              +"\""+unicode(factura.origin).encode("latin-1")+"\","\
              +"\""+unicode(factura.currency_id.name).encode("latin-1")+"\","\
              +"\""+unicode(factura.residual).encode("latin-1")+"\","\
              +"\""+unicode(factura.amount_untaxed).encode("latin-1")+"\","\
              +"\""+unicode(factura.amount_total).encode("latin-1")+"\","\
              +"\""+unicode(state).encode("latin-1")+"\","+"\n"

    #--------- termina contenido archivo

        buf = cStringIO.StringIO()
        buf.write(columna)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        filename_psn = nombre_arch+".csv"
        write_vals = {
            'name': filename_psn,
            'data': out,
            'gv_factura_x_servicio_id': ids[0]
        }
        id=self.pool.get('wizard.export.invoice').create(cr, uid, write_vals, context= context)

        # --------end------------------------
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'wizard.export.invoice',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': id,
            'views': [(False, 'form')],
            'target': 'new',
        }
      # Columnas y/o campos Tree & Form
        _columns = {
            'date_from': fields.datetime('De: ', required=True),
            'date_to': fields.datetime('A: ', required=True),
            'invoice_ids' : fields.one2many('account.invoice', 'gv_factura_x_servicio_id', readonly=True),
            'filtro_all_or_suplier': fields.selection((('a', 'Todos'), ('b', 'Proveedor')), 'Filtrar por:'),
            'partner_id': fields.many2one('res.partner', string='Proveedor:', domain=[('supplier','=',True)]),
        }

        _defaults = {}

facturas_x_servicio()


class wizard_export(osv.osv_memory):
    _name = "wizard.export.invoice"

    _columns = {
        'name': fields.char('File Name', readonly=True),
        'data': fields.binary('File', readonly=True),
        #CAMPO PARA EL REPORTE DE FACTURAS PROVEEDORES DE SERVICIOS
        'gv_factura_x_servicio_id': fields.many2one('gv.factura.x.servicio'),
    }
    _defaults = {}
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
