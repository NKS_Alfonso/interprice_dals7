# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : Viridiana Cruz Santos(gvadeto)2016                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports

from openerp.osv import fields, osv
from datetime import date
import time
from datetime import datetime
from openerp import models, api, _

####################################################################################################################################################
#                                      Agregar en wizard boton que imprima Informe de Impuestos                                                    #
###################################################################################################################################################
# class account_vat_declaration(osv.osv_memory):
  
#   _inherit = 'account.vat.declaration'
#   def create_vat(self, cr, uid, ids, context=None):
        
#             if context is None:
#                 context = {}
                
#             data = self.browse(cr, uid, ids)
#             cr.execute("DELETE FROM wizard_export_tax")
#             self.pool.get('wizard.export.tax').create(cr, uid, {'account_vat_id':data.id,}, context)
            
#             datas = {'ids': context.get('active_ids', [])}
#             datas['model'] = 'account.tax.code'
#             datas['form'] = self.read(cr, uid, ids, context=context)[0]
    
#             for field in datas['form'].keys():
#                 if isinstance(datas['form'][field], tuple):
#                     datas['form'][field] = datas['form'][field][0]
    
#             taxcode_obj = self.pool.get('account.tax.code')
#             taxcode_id = datas['form']['chart_tax_id']
#             taxcode = taxcode_obj.browse(cr, uid, [taxcode_id], context=context)[0]
#             datas['form']['company_id'] = taxcode.company_id.id
             
#             self.write(cr, uid,ids, {'activo': True}, context=context)
            
#             return self.pool['report'].get_action(cr, uid, [], 'account.report_vat', data=datas, context=context)
          
#   def exportar(self, cr, uid, ids, context=None):
#     data = self.browse(cr, uid, ids)
#     if data.activo == True:
#        cr.execute('SELECT id FROM wizard_export_tax')
#        id_wizard = cr.fetchone()[0]
       
#        return {
#         'type': 'ir.actions.act_window',
#         'res_model': 'wizard.export.tax',
#         'view_mode': 'form',
#         'view_type': 'form',
#         'res_id': int(id_wizard),
#         'views': [(False, 'form')],
#         'target': 'new',
#        }
     
#     else:
#      raise osv.except_osv(_('Alerta!'), _("Primero tiene que Imprimir declaración de Impuestos"))
    
#   _columns = {
#     'account_vat_id' : fields.many2one ('account.vat.declaration', 'Account'),
#     'activo' : fields.boolean ('....'),
#   }
#   _defaults = {
#       'activo': False,
#     } 
        
# account_vat_declaration()


class wizard_export(osv.osv_memory):
    _name = "wizard.export.tax"
    
    _columns = {
            'name': fields.char('File Name', readonly=True),
            'data': fields.binary('File', readonly=True),
             
            'account_vat_id' : fields.many2one('account.vat.declaration')
    }
    _defaults = {}
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2: