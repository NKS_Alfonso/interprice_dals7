# -*- coding: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Jose J. H. Bautista'
# __email__ = 'jose.bautista@gvadeto.com'

from openerp import models, fields, api, _
from openerp.osv import osv
import openerp.tools as tools
import cStringIO, base64
from olib.oCsv.oCsv import OCsv
import datetime


class ReportsSalesSalesByProduct(osv.osv_memory):
    _name = 'r.sales_by_product'

    date_from = fields.Datetime(string=_('Fecha inicio'))
    date_to = fields.Datetime(string=_('Fecha fin'))
    product_from = fields.Many2one('product.product', string=_('Producto'))
    partner_id = fields.Many2one('res.partner', string='Cliente', domain=[('customer', '=', True), ('is_company', '=', True)])
    user_id = fields.Many2one('res.users', string='Ejecutivo', domain=[])
    warehouse_id = fields.Many2one('stock.warehouse', string='Almacén')
    currency_id = fields.Many2one('res.currency', string='Moneda',
                                  help='Seleccione la moneda por la que será convertida los movimientos.')
    section_id = fields.Many2one('product.category', 'Sección')
    line_id = fields.Many2one('product.category', 'Línea')
    brand_id = fields.Many2one('product.category', 'Marca')
    serial_id = fields.Many2one('product.category', 'Serie')

    def _apply_filter(self, cr, uid, ids, context=None, type=None):
        dataset_ids = []
        data = self.browse(cr, uid, ids, context=None)
        iuser_id = ''
        iwarehouse_id = ''
        and_partner_id = ''
        if data.partner_id:
            and_partner_id = ' and partner_id = %s ' % (data.partner_id.id,)
        and_product = ''
        if data.product_from:
            and_product = ' and product_id = %s' % data.product_from.id
        if data.user_id:
            iuser_id = ' and sales_agent_id = %s ' % (data.user_id.id,)
        if data.warehouse_id:
            iwarehouse_id = ' and warehouse_id = %s ' % (data.warehouse_id.id,)
        _currency = False
        if data.currency_id:
            _currency = data.currency_id

        strCategorias = ''
        if data.section_id.id:
          strCategorias = """ AND section_id = {} """.format(data.section_id.id)
        if data.line_id.id:
          strCategorias += """ AND line_id = {} """.format(data.line_id.id)
        if data.brand_id.id:
          strCategorias += """ AND brand_id = {} """.format(data.brand_id.id)
        if data.serial_id.id:
          strCategorias += """ AND serial_id = {} """.format(data.serial_id.id)

        query = "select id from r_sales_by_product_data where create_doc IS NOT NULL and create_doc >='%s' " \
                "and create_doc <= '%s' %s %s %s %s %s" \
                % (data.date_from, data.date_to, and_product, and_partner_id, iuser_id, iwarehouse_id, strCategorias)
        cr.execute(query)
        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            if type == 'show':
                cr.execute('delete from r_sales_by_product where id=%s', (ids[0],))
        else:
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids, _currency

    @api.multi
    def show(self):
        search_view_id = self.env['ir.model.data'].get_object_reference('reports', 'view_sales_by_product_screen_search')[1]
        data_ids, currency = self._apply_filter(type='show')
        screen=self.env['sales_by_product_screen']
        screen.with_context({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False}).show_screen(data_ids)
        return {
            'name': 'Ventas por producto / cliente',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,search',
            'res_model': 'sales_by_product_screen',
            'search_view_id': search_view_id,
            'target': 'current',
        }

    def csv(self, cr, uid, ids, context=None):
        dataset_ids, currency = self._apply_filter(cr, uid, ids, context, type='csv')
        context.update({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False})
        id = self.pool['r.sales.by.product.data'].to_csv_by_ids(cr, uid, dataset_ids, context=context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id),
            'target': 'self',
        }


class ReportsSalesSalesByProduct(models.Model):
    _name = 'r.sales.by.product.data'
    _table = 'r_sales_by_product_data'
    _auto = False
    _order = 'id desc'

    sale_name = fields.Char(string=_('Folio venta'))
    out_name = fields.Char(string=_('Orden de entrega'))
    client_order_ref = fields.Char(string=_('Reference'))
    create_doc = fields.Datetime(string=_('Fecha de creación'))
    partner = fields.Char(string=_('Cliente'))
    sales_agent = fields.Char(string=_('Ejecutivo'))
    type_doc = fields.Char(string=_('Tipo de documento'))
    warehouse = fields.Char(string=_('Almacén'))
    invoice_number = fields.Char(string=_('Folio de la  factura'))
    pay_method = fields.Char(string=_('Método de pago'))
    state = fields.Char(string=_('Estado'))
    currency = fields.Char(string=_('Moneda'))
    currency_copy = fields.Char(string=_('Moneda'))
    currency_copy_id = fields.Many2one('res.currency', string = 'Moneda documento')
    product_code = fields.Char(string=_('Código'))
    product_name = fields.Char(string=_('Producto'))
    product_section = fields.Char(string=_('Sección'))
    product_line = fields.Char(string=_('Línea'))
    product_brand = fields.Char(string=_('Marca'))
    product_serial = fields.Char(string=_('Serie'))
    product_price_unit = fields.Float(string=_('Precio unitario'), digits=(10, 2))
    product_price_unit_copy = fields.Float(string=_('Precio unitario'), digits=(10, 2))
    product_subtotal = fields.Float(string=_('Subtotal'), digits=(10, 2))
    product_subtotal_copy = fields.Float(string=_('Subtotal'), digits=(10, 2))
    invoice_total = fields.Float(string=_('Total'), digits=(10, 2))
    invoice_total_copy = fields.Float(string=_('Total'), digits=(10, 2))
    product_qty = fields.Integer(string=_('Cantidad'))
    cost = fields.Float(string=_('Costo promedio'), digits=(10, 2))
    cost_copy = fields.Float(string=_('Costo promedio'), digits=(10, 2))
    subtotal_cost = fields.Float(string=_('Costo subtotal'), digits=(10, 2))
    subtotal_cost_copy = fields.Float(string=_('Costo subtotal'), digits=(10, 2))
    profit = fields.Float(string=_('Utilidad'), digits=(10, 2))
    profit_percent = fields.Float(string=_('Porcentaje de utilidad'))
    rate = fields.Float(string='Tipo de cambio', digits=(10, 4))
    currency_company = fields.Integer(string='Company Currency')

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_sales_by_product_data')
        cr.execute("""
                            CREATE OR REPLACE FUNCTION getProductCategoryBy(category_id INTEGER, by VARCHAR(100))
                              RETURNS VARCHAR(1000) as $category_name$
                              DECLARE
                                category VARCHAR[];
                                categories_str VARCHAR(1000);
                                category_str VARCHAR(1000);
                                category_name VARCHAR(1000);
                                i INTEGER;
                              BEGIN
                                IF category_id  IS NULL THEN
                                    RETURN '';
                                END IF;
                                categories_str :=getProductCategory(category_id, '');
                                IF by = 'FULL' THEN
                                  RETURN  categories_str;
                                END IF;

                                category := string_to_array(categories_str, '/');
                                i := 0;
                                category_name := '';
                                FOREACH category_str IN ARRAY category
                                  LOOP
                                    i := i+1;
                                    IF i = 1 AND by='SECTION' THEN
                                      category_name := category_str;
                                    ELSEIF i = 2 AND by='LINE' THEN
                                      category_name :=category_str;
                                    ELSEIF i = 3 AND by='BRAND' THEN
                                      category_name :=category_str;
                                    END IF;
                                  END LOOP;
                                RETURN category_name;
                              END;
                              $category_name$ LANGUAGE plpgsql;
                        """)
        cr.execute("""
                    CREATE OR REPLACE FUNCTION getProductCategory(category_id INTEGER, fcategory_name VARCHAR(1000) )
                      RETURNS VARCHAR(1000) AS $fcategory_name$
                      DECLARE
                        fcategory_name_tmp VARCHAR(1000);
                        fcategory_id_root INTEGER;
                        fparent_id INTEGER;
                      BEGIN
                        SELECT pc.id , COALESCE( pc.parent_id, -1), pc.name into fcategory_id_root, fparent_id, fcategory_name_tmp
                        FROM product_category pc WHERE pc.id = category_id;
                        fcategory_name =  (fcategory_name_tmp||  ' / ' ||fcategory_name ) ;
                        IF fparent_id > 0 THEN
                          RETURN getProductCategory(fparent_id, fcategory_name);
                        ELSE
                          RETURN fcategory_name;
                        END IF;
                      END;
                      $fcategory_name$ LANGUAGE plpgsql;
                """)
        cr.execute("""
            create or replace view r_sales_by_product_data as (
              SELECT
                  row_number() over (order by T1.create_doc, T1.out_name) as id,
                  T1.sale_name,
                  T1.out_name,
                  T1.client_order_ref,
                  T1.create_doc as create_doc,
                  T1.partner_id,
                  T1.partner,
                  T1.sales_agent_id,
                  T1.sales_agent,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN 'FAC'
                    WHEN 'out_refund' THEN 'NCC'
                  ELSE '' END type_doc,
                  T1.warehouse_id,
                  T1.warehouse,
                  T1.invoice_number,
                  T1.pay_method,
                  CASE T1.invoice_state WHEN 'open' THEN 'Abierto'
                    WHEN 'cancel' THEN 'Cancelado'
                    WHEN 'draft' THEN 'Borrador'
                    WHEN 'paid' THEN 'Pagado'
                  ELSE '' END state,
                  T1.currency as currency_copy,
                  T1.currency_id as currency_copy_id,
                  T1.product_id,
                  T1.product_code,
                  T1.product_name,
                  T1.section as product_section,
                  T1.line as product_line,
                  T1.brand as product_brand,
                  T1.serial as product_serial,
                  T1.section_id as section_id,
                  T1.line_id as line_id,
                  T1.brand_id as brand_id,
                  T1.serial_id as serial_id,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN T1.product_qty
                    WHEN 'out_refund' THEN -(T1.product_qty) END  as product_qty,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN T1.product_price_unit
                    WHEN 'out_refund' THEN -(T1.product_price_unit) END as product_price_unit_copy,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN T1.product_subtotal
                    WHEN 'out_refund' THEN -(T1.product_subtotal) END as product_subtotal_copy,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN round(cast(T1.cost AS NUMERIC),2)
                    WHEN 'out_refund' THEN -(round(cast(T1.cost AS NUMERIC),2)) END as cost_copy,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN round(cast(T1.cost * T1.product_qty AS NUMERIC),2)
                    WHEN 'out_refund' THEN -(round(cast((T1.cost * T1.product_qty )AS NUMERIC),2)) END as subtotal_cost_copy,
                  CASE
                    WHEN T1.invoice_type ='out_invoice' and T1.invoice_number !='' THEN T1.product_subtotal - (T1.cost * T1.product_qty)
                    WHEN T1.invoice_type = 'out_refund' and T1.invoice_number !='' THEN ((-T1.product_subtotal) - (-(T1.cost * T1.product_qty)))
                  END as profit_copy,
                  CASE
                    WHEN T1.invoice_type = 'out_invoice' and T1.product_subtotal > 0 THEN
                    (T1.product_subtotal - (T1.cost * T1.product_qty)  ) * 100 / T1.product_subtotal
                    WHEN T1.invoice_type = 'out_refund' and T1.product_subtotal >0 THEN
                    ((-T1.product_subtotal) - (-(T1.cost * T1.product_qty))  ) * 100 / T1.product_subtotal
                  ELSE 0 END as  profit_percent,
                  T1.invoice_total as invoice_total_copy,
                  T1.rate,
                  T1.currency_company
                FROM (
                       SELECT
                         CASE
                            WHEN ai.state IS NOT NULL THEN ai.invoice_datetime
                            WHEN ai.state IS NULL AND sp.date_done IS NOT NULL THEN sp.date_done
                         ELSE so.date_order END AS create_doc,
                         sw.id as warehouse_id,
                         sw.name as warehouse,
                         CASE
                            WHEN ai.state IS NULL THEN so.partner_id
                            ELSE rp.id END as partner_id,
                         CASE
                            WHEN ai.state IS NULL THEN (SELECT rp.name FROM res_partner rp WHERE rp.id=so.partner_id)
                         ELSE rp.name END as partner,
                         CASE
                            WHEN so.name IS NOT NULL AND ai.state IS NULL THEN so.user_id ELSE ai.user_id END as sales_agent_id,
                         rpu.name as sales_agent,
                         CASE
                            WHEN ai.state IS NOT NULL THEN (select name from res_currency where id=ai.currency_id)
                            ELSE '' END as currency,
                        CASE
                            WHEN ai.state IS NOT NULL THEN ai.currency_id
                            ELSE NULL END currency_id,
                         so.name as sale_name,
                         sp.name as out_name,
                         so.client_order_ref as client_order_ref,
                         COALESCE(ai.number,'') AS invoice_number,
                         COALESCE(ai.state,'') AS invoice_state,
                         COALESCE(ai.type, '') AS invoice_type,
                         (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                            INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                            WHERE aipmr.account_invoice_id = ai.id
                         )as pay_method,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pc.name--getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=ail.product_id),'SECTION')
                            ELSE '' END as section,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pc.id
                            ELSE NULL END as section_id,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pca.name --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=ail.product_id),'LINE')
                            ELSE '' END as "line",
                         CASE
                            WHEN ai.state IS NOT NULL THEN pca.id
                            ELSE NULL END as "line_id",
                         CASE
                            WHEN ai.state IS NOT NULL THEN pcat.name --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=ail.product_id),'BRAND')
                            ELSE '' END as brand,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pcat.id
                            ELSE NULL END as brand_id,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pcatt.name --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=ail.product_id),'BRAND')
                            ELSE '' END as "serial",
                         CASE
                            WHEN ai.state IS NOT NULL THEN pcatt.id
                            ELSE NULL END as "serial_id",
                         CASE
                            WHEN ai.state IS NOT NULL THEN pp.id ELSE 0 END as product_id,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pp.default_code ELSE '' END as product_code,
                         CASE
                            WHEN ai.state IS NOT NULL THEN pp.name_template ELSE '' END as product_name,
                         CASE
                            WHEN ai.state IS NOT NULL THEN ail.quantity ELSE  0 END as product_qty,
                         CASE
                            WHEN ai.state IS NOT NULL THEN ail.price_unit ELSE 0 END as product_price_unit,
                         CASE
                            WHEN ai.state IS NOT NULL THEN (ail.quantity* ail.price_unit) ELSE 0 END as product_subtotal,

                          CASE WHEN ai.state IS NOT NULL THEN(
                            SELECT
                              round(CAST (COALESCE(pph.cost,0) AS NUMERIC), 2)  product_cost
                            FROM
                              product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                            WHERE pp.id=ail.product_id AND pph.create_date <=sp.date  ORDER BY pph.create_date desc LIMIT 1
                          ) ELSE 0 END as cost,
                         CASE
                            WHEN ai.state IS NOT NULL THEN ai.amount_total ELSE 0 END as invoice_total,
                         CASE WHEN ai.picking_dev_id IS NOT NULL THEN doc_tc(ai.id, ai.company_id, 'INV', sp.date)
                         ELSE doc_tc(ai.id ,ai.company_id ,'INV', ai.invoice_datetime) END AS rate,
                         rcom.currency_id as currency_company
                    FROM
                         sale_order so
                         INNER JOIN stock_picking sp ON so.name=sp.origin
                         INNER JOIN account_invoice ai ON sp.id = ai.picking_dev_id and ai.id is not NULL
                         LEFT JOIN account_invoice_line ail ON  ai.id = ail.invoice_id and (ail.origin=sp.name or ail.origin=so.name)
                         LEFT JOIN product_product pp on ail.product_id = pp.id
                         LEFT JOIN product_template ptt on ptt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pc on pc.id = ptt.section
                         LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pca on pca.id = pt.line
                         LEFT JOIN product_template ptm on ptm.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcat on pcat.id = ptm.brand
                         LEFT JOIN product_template ptmt on ptmt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcatt on pcatt.id = ptmt.serial
                         INNER JOIN stock_warehouse sw on so.warehouse_id = sw.id
                         LEFT JOIN res_partner rp on ai.partner_id = rp.id
                         INNER JOIN res_users ru on so.user_id=ru.id
                         INNER JOIN res_partner rpu on ru.partner_id =rpu.id
                         LEFT JOIN res_company rcom on rcom.id= ai.company_id
                         LEFT JOIN res_currency rcc on rcc.id= rcom.currency_id
                         LEFT JOIN res_currency rc on ai.currency_id= rc.id -- --
                    WHERE so.state NOT IN ('draft','sent')
           UNION ALL
                       SELECT
                         ai.invoice_datetime AS create_doc,
                         sw.id AS warehouse_id,
                         CASE
                            WHEN so.name IS NULL THEN '' ELSE sw.name END as warehouse,
                         rp.id partner_id, rp.name partner,
                         ai.user_id AS sales_agent_id,
                         (
                            SELECT rpt.display_name FROM res_partner as rpt
                            where id =(SELECT rs.partner_id FROM res_users as rs where id= ai.user_id)
                         ) as sales_agent,
                         rc.name as currency ,
                         ai.currency_id as currency_id,
                         '' as sale_name,
                         CASE
                            WHEN ai.type = 'out_refund' AND ai.picking_dev_id is not NULL THEN
                            (SELECT sp.name FROM stock_picking as sp WHERE id=ai.picking_dev_id)
                         ELSE '' END as out_name,
                         CASE
                            WHEN (SELECT name FROM sale_order WHERE name = ail.origin) IS NOT NULL THEN
                            (SELECT client_order_ref FROM sale_order WHERE name = ail.origin)
                            WHEN (SELECT name FROM stock_picking WHERE name = ail.origin) IS NOT NULL THEN
                            (SELECT salo.client_order_ref FROM sale_order salo
                            INNER JOIN stock_picking spick ON spick.name = ail.origin WHERE salo.name = spick.origin)
                         ELSE '' END AS client_order_ref,
                         CASE
                            WHEN ai.number IS NULL THEN '' ELSE ai.number END AS invoice_number, ai.state AS invoice_state, ai.type AS invoice_type,
                            (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                              INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                            WHERE aipmr.account_invoice_id = ai.id
                            )as pay_method,
                         pc.name as section, --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'SECTION') as section,
                         pc.id as section_id,
                         pca.name as "line", --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'LINE') as line,
                         pca.id as line_id,
                         pcat.name as brand, --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'BRAND') as brand,
                         pcat.id as brand_id,
                         pcatt.name as "serial", --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'BRAND') as brand,
                         pcatt.id as serial_id,
                         pp.id as product_id, pp.default_code as product_code, pp.name_template as product_name,
                         ail.quantity as product_qty, ail.price_unit, (ail.quantity * ail.price_unit) as product_subtotal,
                         (
                            SELECT
                              round(CAST (COALESCE(pph.cost,0) AS NUMERIC), 2) product_cost
                            FROM
                              product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                            WHERE pp.id=ail.product_id AND pph.create_date <= ai.invoice_datetime  ORDER BY pph.create_date desc LIMIT 1
                          ) as cost,
                          ai.amount_total as invoice_total,
                          CASE WHEN ai.picking_dev_id IS NOT NULL THEN doc_tc(ai.id, ai.company_id, 'INV', sp.date)
                          ELSE doc_tc(ai.id ,ai.company_id ,'INV', ai.invoice_datetime) END AS rate,
                          rcom.currency_id as currency_company
                    FROM account_invoice ai
                         INNER JOIN account_invoice_line ail on ai.id=ail.invoice_id
                         INNER JOIN product_product pp on ail.product_id = pp.id
                         LEFT JOIN product_template ptt on ptt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pc on pc.id = ptt.section
                         LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pca on pca.id = pt.line
                         LEFT JOIN product_template ptm on ptm.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcat on pcat.id = ptm.brand
                         LEFT JOIN product_template ptmt on ptmt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcatt on pcatt.id = ptmt.serial
                         LEFT JOIN stock_picking spd ON spd.id=ai.picking_dev_id
                         /*LEFT JOIN stock_picking sp ON spd.origin like concat('%', replace(sp.name, '\\\\', '\\\\\\\\'))*/
                         LEFT JOIN stock_picking sp ON spd.origin = sp.name
                         LEFT JOIN sale_order so on so.name = sp.origin
                         LEFT JOIN stock_warehouse sw on so.warehouse_id = sw.id
                         INNER JOIN res_partner rp on ai.partner_id =rp.id
                         INNER JOIN res_currency rc on ai.currency_id= rc.id
                         INNER JOIN res_company rcom on rcom.id= ai.company_id
                         INNER JOIN res_currency rcc on rcc.id= rcom.currency_id
                         LEFT JOIN res_users ru on so.user_id=ru.id
                         LEFT JOIN res_partner rpu on ru.partner_id =rpu.id
                    WHERE ((ai.type IN ('out_invoice') AND ai.picking_dev_id is null) or ai.type IN ('out_refund'))
                     )AS T1
                WHERE T1.invoice_state != 'draft'
            )
        """)

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    def to_csv_by_ids(self, cr, uid, ids, context=None):
        ccontext = context['currency2convertname']
        ccost=ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        dataset_csv = [
            [
                # 'id',
                'Folio de venta',
                'Referencia',
                unicode('Fecha de creación').encode('utf8'),
                'Cliente',
                'Ejecutivo',
                'Tipo de documento',
                unicode('Almacén').encode('utf8'),
                unicode('Orden de entrega').encode('utf8'),
                'Folio de la  factura',
                unicode('Método de pago').encode('utf8'),
                'Estado',
                'Moneda',
                unicode('Código').encode('utf8'),
                'Producto',
                unicode('Sección').encode('utf8'),
                unicode('Línea').encode('utf8'),
                'Marca',
                'Serie',
                'Cantidad',
                'Precio unitario',
                'Subtotal',
                'Costo promedio',
                'Costo subtotal',
                'Utilidad',
                'Porcentaje de utilidad',
                'Tipo de cambio'
            ]
        ]
        query = """
            DROP TABLE IF EXISTS csv_sales_by_product;
            CREATE TEMPORARY TABLE csv_sales_by_product (id INTEGER, sale_name CHARACTER VARYING, client_order_ref CHARACTER VARYING,create_doc TIMESTAMP WITHOUT TIME ZONE,
            partner CHARACTER VARYING, sales_agent CHARACTER VARYING, type_doc CHARACTER VARYING, warehouse CHARACTER VARYING,
            out_name CHARACTER VARYING, invoice_number CHARACTER VARYING, pay_method CHARACTER VARYING, state CHARACTER VARYING,
            currency_copy CHARACTER VARYING, product_code CHARACTER VARYING, product_name CHARACTER VARYING, product_section CHARACTER VARYING,
            product_line CHARACTER VARYING, product_brand CHARACTER VARYING, product_serial CHARACTER VARYING, product_qty NUMERIC, product_price_unit NUMERIC, product_subtotal NUMERIC,
            "cost" NUMERIC, subtotal_cost NUMERIC, profit NUMERIC, profit_percent NUMERIC, rate CHARACTER VARYING);
            INSERT INTO csv_sales_by_product
            SELECT rspd.id, rspd.sale_name, rspd.client_order_ref, rspd.create_doc, rspd.partner, rspd.sales_agent, rspd.type_doc, rspd.warehouse,
            rspd.out_name, rspd.invoice_number, rspd.pay_method, rspd.state, rspd.currency_copy, rspd.product_code, rspd.product_name,
            rspd.product_section, rspd.product_line, rspd.product_brand, rspd.product_serial, rspd.product_qty,
            CASE WHEN rspd.product_price_unit_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_price_unit_copy,rspd.create_doc) ELSE NULL END as product_price_unit,
            CASE WHEN rspd.product_subtotal_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_subtotal_copy,rspd.create_doc) ELSE NULL END as product_subtotal,
            CASE WHEN rspd.cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.cost_copy,rspd.create_doc) ELSE NULL END as cost,
            CASE WHEN rspd.subtotal_cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.subtotal_cost_copy,rspd.create_doc) ELSE NULL END as subtotal_cost,
            NULL, NULL, rspd.rate
            FROM r_sales_by_product_data rspd
            LEFT OUTER JOIN res_currency rc on rc.id=rspd.currency_company
            WHERE rspd.id in {1};
            UPDATE csv_sales_by_product SET profit = round(abs(product_subtotal)-abs(subtotal_cost),2) WHERE type_doc in ('FAC','NCC') and product_subtotal > 0;
            UPDATE csv_sales_by_product SET profit_percent = round(profit * 100 / abs(product_subtotal),2) WHERE profit > 0;
            SELECT * FROM csv_sales_by_product ORDER BY id ASC;
        """.format(ccontext,data_ids,ccost)
        cr.execute(query)
        dataset = cr.dictfetchall()
        sum_subtotal = 0.00
        sum_total = 0.00
        sum_cost = 0.00
        sum_utilidad = 0.00
        for row in dataset:
            if row['state'] not in ['Borrador','Cancelado'] and row['product_subtotal'] != None:
                sum_subtotal += row['product_subtotal'] if row['product_subtotal'] else 0
                sum_cost += row['cost'] if row['cost'] else 0
                sum_total += row['subtotal_cost'] if row['subtotal_cost'] else 0
                sum_utilidad += row['profit'] if row['profit'] else 0
            dataset_csv.append(
                [
                    unicode(self.set_default(row['sale_name'])).encode('utf8'),
                    unicode(self.set_default(row['client_order_ref'])).encode('utf8'),
                    unicode(self.utc_mexico_city(row['create_doc'])).encode('utf8'),
                    unicode(self.set_default(row['partner'])).encode('utf8'),
                    unicode(self.set_default(row['sales_agent'])).encode('utf8'),
                    unicode(self.set_default(row['type_doc'])).encode('utf8'),
                    unicode(self.set_default(row['warehouse'])).encode('utf8'),
                    unicode(self.set_default(row['out_name'])).encode('utf8'),
                    unicode(self.set_default(row['invoice_number'])).encode('utf8'),
                    unicode(self.set_default(row['pay_method'])).encode('utf8'),
                    unicode(self.set_default(row['state'])).encode('utf8'),
                    unicode(self.set_default(row['currency_copy'])).encode('utf8'),
                    unicode(self.set_default(row['product_code'])).encode('utf8'),
                    unicode(self.set_default(row['product_name'])).encode('utf8'),
                    unicode(self.set_default(row['product_section'])).encode('utf8'),
                    unicode(self.set_default(row['product_line'])).encode('utf8'),
                    unicode(self.set_default(row['product_brand'])).encode('utf8'),
                    unicode(self.set_default(row['product_serial'])).encode('utf8'),
                    unicode(self.set_default(row['product_qty'], 0)).encode('utf8'),
                    unicode(self.set_default(row['product_price_unit'], 0)).encode('utf8'),
                    unicode(self.set_default(row['product_subtotal'], 0)).encode('utf8'),
                    unicode(self.set_default(row['cost'], 0)).encode('utf8'),
                    unicode(self.set_default(row['subtotal_cost'], 0)).encode('utf8'),
                    unicode(self.set_default(row['profit'], 0)).encode('utf8'),
                    unicode(self.set_default(row['profit_percent'], 0)).encode('utf8'),
                    unicode(self.set_default(row['rate'], 0)).encode('utf8')
                    ]
            )
        sum_subtotal = round(sum_subtotal, 2)
        sum_cost = round(sum_cost, 2)
        sum_total = round(sum_total, 2)
        sum_utilidad = round(sum_utilidad, 2)
        profit_percent_general = format(((sum_subtotal-sum_cost) * 100 / sum_subtotal), '.2f') if sum_subtotal > 0 else 0
        dataset_csv.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', sum_subtotal, sum_cost, sum_total, sum_utilidad, profit_percent_general, '',])
        obj_csv = OCsv()
        file = obj_csv.csv_base64(dataset_csv)
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': 'Sales_by_product.csv',
            'type_file': 'csv',
            'file': file,
        }, context=None)

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default


class ReportSalesByProductScreen(osv.osv_memory):
    _name = 'sales_by_product_screen'

    sale_name = fields.Char(string=_('Folio venta'))
    out_name = fields.Char(string=_('Orden de entrega'))
    client_order_ref = fields.Char(string=_('Referencia'))
    create_doc = fields.Datetime(string=_('Fecha de creación'))
    partner = fields.Char(string=_('Cliente'))
    sales_agent = fields.Char(string=_('Ejecutivo'))
    type_doc = fields.Char(string=_('Tipo de documento'))
    warehouse = fields.Char(string=_('Almacén'))
    invoice_number = fields.Char(string=_('Folio de la  factura'))
    pay_method = fields.Char(string=_('Método de pago'))
    state = fields.Char(string=_('Estado'))
    currency = fields.Char(string=_('Moneda'))
    currency_copy = fields.Char(string=_('Moneda'))
    currency_copy_id = fields.Many2one('res.currency', string = 'Moneda documento')
    product_code = fields.Char(string=_('Código'))
    product_name = fields.Char(string=_('Producto'))
    product_section = fields.Char(string=_('Sección'))
    product_line = fields.Char(string=_('Línea'))
    product_brand = fields.Char(string=_('Marca'))
    product_serial = fields.Char(string=_('Serie'))
    product_price_unit = fields.Float(string=_('Precio unitario'), digits=(10, 2))
    product_price_unit_copy = fields.Float(string=_('Precio unitario'), digits=(10, 2))
    product_subtotal = fields.Float(string=_('Subtotal'), digits=(10, 2))
    product_subtotal_copy = fields.Float(string=_('Subtotal'), digits=(10, 2))
    invoice_total = fields.Float(string=_('Total'), digits=(10, 2))
    invoice_total_copy = fields.Float(string=_('Total'), digits=(10, 2))
    product_qty = fields.Integer(string=_('Cantidad'))
    cost = fields.Float(string=_('Costo promedio'), digits=(10, 2))
    cost_copy = fields.Float(string=_('Costo promedio'), digits=(10, 2))
    subtotal_cost = fields.Float(string=_('Costo subtotal'), digits=(10, 2))
    subtotal_cost_copy = fields.Float(string=_('Costo subtotal'), digits=(10, 2))
    profit = fields.Float(string=_('Utilidad'), digits=(10, 2))
    profit_percent = fields.Float(string=_('Porcentaje de utilidad'), digits=(10, 2))
    rate = fields.Float(string='Tipo de cambio', digits=(10, 4))
    currency_company = fields.Integer(string='Company Currency')
    currency_show = fields.Char(string="Moneda del reporte")

    @api.multi
    def show_screen(self, ids):
        ccontext = self._context['currency2convertname']
        ccost = ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        currency_show = ccontext if self._context['currency2convertname'] else 'rspd.currency_copy'
        query = """
            TRUNCATE TABLE sales_by_product_screen;
            INSERT INTO sales_by_product_screen (id, sale_name, client_order_ref, create_doc, partner, sales_agent, type_doc, warehouse,
            out_name, invoice_number, pay_method, state, currency_copy, currency, product_code, product_name, product_section, product_line,
            product_brand, product_serial, product_qty, product_price_unit, product_subtotal, "cost", subtotal_cost, profit, profit_percent, rate, currency_show,
            create_uid, write_uid)
            SELECT rspd.id, rspd.sale_name, rspd.client_order_ref, rspd.create_doc, rspd.partner, rspd.sales_agent, rspd.type_doc, rspd.warehouse,
            rspd.out_name, rspd.invoice_number, rspd.pay_method, rspd.state, rspd.currency_copy, rspd.currency_copy as currency, rspd.product_code, rspd.product_name,
            rspd.product_section, rspd.product_line, rspd.product_brand, rspd.product_serial, rspd.product_qty,
            CASE WHEN rspd.product_price_unit_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_price_unit_copy,rspd.create_doc) ELSE NULL END as product_price_unit,
            CASE WHEN rspd.product_subtotal_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_subtotal_copy,rspd.create_doc) ELSE NULL END as product_subtotal,
            CASE WHEN rspd.cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.cost_copy,rspd.create_doc) ELSE NULL END as cost,
            CASE WHEN rspd.subtotal_cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.subtotal_cost_copy,rspd.create_doc) ELSE NULL END as subtotal_cost,
            NULL, NULL, rspd.rate, {3} as currency_show, {4}, {4}
            FROM r_sales_by_product_data rspd
            LEFT OUTER JOIN res_currency rc on rc.id=rspd.currency_company
            WHERE rspd.id in {1};
            UPDATE sales_by_product_screen SET profit = round(abs(product_subtotal)-abs(subtotal_cost),2) WHERE type_doc in ('FAC','NCC') and product_subtotal > 0;
            UPDATE sales_by_product_screen SET profit_percent = round(profit * 100 / abs(product_subtotal),2) WHERE profit > 0;
        """.format(ccontext,data_ids,ccost,currency_show,self.env.uid)
        self.env.cr.execute(query)
        return True
