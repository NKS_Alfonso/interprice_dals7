# -*- coding: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Jose J. H. Bautista'
# __modified__ = 'Saul Favela'
# __email__ = 'jose.bautista@gvadeto.com'

from openerp import fields, models, api, _
from openerp.osv import osv
import openerp.tools as tools
from olib.oCsv.oCsv import OCsv
import datetime
import logging
_logger = logging.getLogger(__name__)

class ReportsSalesSalesByDate(osv.osv_memory):
    _name = 'r.sales_by_date'

    date_from = fields.Datetime(string='Fecha inicio')
    date_to = fields.Datetime(string='Fecha fin')
    partner_id = fields.Many2one('res.partner', string='Cliente', domain=[('customer', '=', True), ('is_company', '=', True)])
    user_id = fields.Many2one('res.users', string='Ejecutivo', domain=[])
    warehouse_id = fields.Many2one('stock.warehouse', string='Almacén', domain=[])
    currency_id = fields.Many2one('res.currency', string='Moneda',
                                  help='Seleccione la moneda por la que será convertida los movimientos.')
    include = fields.Selection([('0','Todos'),('1', 'Sin Factura'),('2','Facturados')], default='2', string="Documentos a incluir", required=True)
    include_cancel = fields.Boolean(string='¿Incluir cancelados?', default=False)

    def _get_ids(self, cr, uid, ids, type):
        dataset_ids = []
        data = self.browse(cr, uid, ids, context=None)
        ipartner_id = ''
        iuser_id = ''
        iwarehouse_id = ''
        iinclude = ''
        if data.partner_id:
            ipartner_id = ' and customer_id = %s ' % (data.partner_id.id,)
        if data.user_id:
            iuser_id = ' and sales_name_id = %s ' % (data.user_id.id,)
        if data.warehouse_id:
            iwarehouse_id = ' and warehouse_id = %s ' % (data.warehouse_id.id,)
        if data.include == '1':
            iinclude = " and related_invoice isnull and type_doc not in ('FAC','NCC') "
        elif data.include == '2':
            iinclude = " and type_doc in ('FAC','NCC') "
        _currency = False
        if data.currency_id:
            _currency = data.currency_id
        cr.execute(
            "select id from r_sales_by_date_data where create_doc IS NOT NULL AND create_doc >='%s'  and"
            " create_doc <= '%s' %s %s %s %s"
            % (data.date_from, data.date_to, ipartner_id, iuser_id, iwarehouse_id, iinclude))
        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            ids_to_keep = "("+','.join(str(e) for e in dataset_ids)+")"
            report_id = ids[0]
            if type == 'show':
                cr.execute('delete from r_sales_by_date where id not in {}'.format(ids_to_keep))
        else:
            data.unlink()
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids, _currency
    

    @api.multi
    def show(self):
        search_view_id = self.env['ir.ui.view'].search([('model', '=', 'sales_by_date_screen'), ('name', '=', 'r.sales.by.date.screen.search')]).id
        data_ids, currency = self._get_ids(type='show')
        screen=self.env['sales_by_date_screen']
        wiz = self.env['r.sales_by_date'].search([('id','=',self.id)])            
        screen.with_context({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False,'include_cancel':wiz.include_cancel}).show_screen(data_ids)
        return {
            'name': 'Ventas por fecha / cliente',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,search',
            'res_model': 'sales_by_date_screen',
            'search_view_id': search_view_id,
            'target': 'current',
        }
    @api.multi
    def csv(self):
        dataset_ids, currency = self._get_ids(type='csv')
        #context.update({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False,'include_cancel':self.include_cancel})        
        id = self.env['r.sales.by.date.data'].with_context({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False,'include_cancel':self.include_cancel}).to_csv_by_ids(dataset_ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id.id),
            'target': 'self',
        }


class ReportsSalesSalesByDateData(models.Model):
    _name = 'r.sales.by.date.data'
    _table = 'r_sales_by_date_data'
    _auto = False
    _order = 'id desc'

    sale_name = fields.Char(string='Folio de venta')
    out_name = fields.Char(string='Orden de entrega')
    sales_name = fields.Char(string='Ejecutivo')
    create_doc = fields.Datetime(string='Fecha de creacion')
    type_doc = fields.Char(string='Tipo de documento')
    warehouse = fields.Char(string='Almacen')
    invoice_number = fields.Char(string='Folio de la  factura')
    state = fields.Char(string='Estatus')
    pay_method = fields.Char(string=_('Método de pago'))
    currency_copy = fields.Char(string='Moneda')
    currency_copy_id = fields.Many2one('res.currency', string = 'Moneda documento')
    subtotal_copy = fields.Float(string='Subtotal', digits=(10, 2))
    tax_copy = fields.Float(string='Impuesto', digits=(10, 2))
    total_copy = fields.Float(string='Total', digits=(10, 2))
    customer_name = fields.Char(string='Cliente')
    rate = fields.Char(string='Tipo de cambio')
    cost_copy = fields.Float(string='Costo', digits=(10, 2))
    profit_copy = fields.Float(digits=(10, 2))
    account_invoice_id = fields.Many2one('account.invoice', string='account invoice')
    currency_company = fields.Integer(string='Company Currency')
    related_invoice = fields.Integer(string='Related Invoice')
    
    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_sales_by_date_data')
        if cr._cnx.server_version >=90500:
            indexes = """
                    CREATE INDEX IF NOT EXISTS product_price_history_costdate_idx ON product_price_history(cost,create_date);
                    CREATE INDEX IF NOT EXISTS product_price_history_prodtmpl_idx ON product_price_history(product_template_id);
                    CREATE INDEX IF NOT EXISTS product_product_prodtmpl_idx ON product_product(product_tmpl_id);
                    CREATE INDEX IF NOT EXISTS stock_picking_origin_idx on stock_picking(origin);
                    CREATE INDEX IF NOT EXISTS sale_order_state_idx on sale_order(state);
                    CREATE INDEX IF NOT EXISTS sale_order_name_idx on sale_order(name);
                    CREATE INDEX IF NOT EXISTS account_invoice_state_idx on account_invoice(state);
                    CREATE INDEX IF NOT EXISTS account_invoice_type_idx on account_invoice(type);
            """
        else:
            indexes = """
                DO
                $$
                BEGIN
                    /*PRODUCT PRICE HISTORY COSTDATE IDX*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'product_price_history_costdate_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX product_price_history_costdate_idx ON product_price_history(cost,create_date);
                    END IF;
                    /*PRODUCT PRICE HISTORY PRODUCT TEMPLATE */
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'product_price_history_prodtmpl_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX product_price_history_prodtmpl_idx ON product_price_history(product_template_id);
                    END IF;
                    /*PRODUCT PRODUCT PRODUCT TEMPLATE*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'product_product_prodtmpl_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX product_product_prodtmpl_idx ON product_product(product_tmpl_id);
                    END IF;
                    /*STOCK PICKING ORIGIN*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'stock_picking_origin_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX stock_picking_origin_idx ON stock_picking(origin);
                    END IF;
                    /*SALE ORDER*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'sale_order_state_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX sale_order_state_idx ON sale_order(state);
                    END IF;
                    /*SALE ORDER NAME*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'sale_order_name_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX sale_order_name_idx ON sale_order(name);
                    END IF;
                    /*ACCOUNT INVOICE STATE*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'account_invoice_state_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX account_invoice_state_idx ON account_invoice(state);
                    END IF;
                    /*ACCOUNT INVOICE TYPE*/
                    IF NOT EXISTS (
                        SELECT *
                        FROM pg_class c
                        JOIN pg_namespace n on n.oid = c.relnamespace
                        WHERE c.relname = 'account_invoice_type_idx'
                        AND n.nspname = 'public'
                    ) THEN
                        CREATE INDEX account_invoice_type_idx ON account_invoice(type);
                    END IF;
                END
                $$;
            """
        cr.execute(indexes+"""
                DROP FUNCTION IF EXISTS currency_convert (TEXT, TEXT, NUMERIC, TIMESTAMP);    
                CREATE OR REPLACE FUNCTION currency_convert (TEXT, TEXT, NUMERIC, TIMESTAMP)
                      RETURNS numeric
                    AS
                    $$
                    DECLARE
                        curr_from ALIAS FOR $1;
                        curr_to ALIAS FOR $2;
                        amount ALIAS FOR $3;
                        tc_doc_date ALIAS FOR $4;
                        rate_from numeric := 0.00;
                        rate_to numeric := 0.00;
                        rate_convert numeric := 0.00;
                        new_amount numeric := 0;

                    BEGIN
                        IF curr_to = 'False' THEN RETURN amount; END IF;
                        /*Get rate currency_from*/
                        SELECT get_rate('sale',curr_from,tc_doc_date) INTO rate_from;
                        /*Get rate currency_to*/
                        SELECT get_rate('sale',curr_to,tc_doc_date) INTO rate_to;
                        /*Validate rate results*/
                        IF rate_from = 0.00 OR rate_to = 0.00 THEN rate_convert := 0.00; ELSE rate_convert := ((1/rate_to)/(1/rate_from)); END IF;
                        /*Determinate the new amount*/
                        new_amount := round(amount * rate_convert,2);
                        RETURN new_amount;
                    END;
                    $$
                    LANGUAGE plpgsql;
                DROP FUNCTION IF EXISTS doc_tc (INT, INT, CHARACTER VARYING, TIMESTAMP);
                CREATE OR REPLACE FUNCTION doc_tc (INT, INT, CHARACTER VARYING, TIMESTAMP)
                      RETURNS numeric
                    AS
                    $$
                    DECLARE
                      tc_id ALIAS FOR $1;
                      tc_company_id ALIAS FOR $2;
                      tc_doc_type ALIAS FOR $3;
                      tc_doc_date ALIAS FOR $4;

                      tc_tc numeric := 0;

                      company_currency_id INTEGER;
                      company_currency_name VARCHAR(5);
                      is_company_tc BOOLEAN :=FALSE ;

                      document_tc FLOAT;
                      document_currency_id INTEGER;
                      document_currency_name VARCHAR(5);

                    BEGIN
                      /*Get currency company*/
                      SELECT rcm.currency_id, rcu.name  INTO company_currency_id, company_currency_name
                      FROM res_company rcm INNER JOIN res_currency rcu
                          ON rcm.currency_id = rcu.id
                      WHERE rcm.id=tc_company_id;

                      IF tc_doc_type = 'SO' THEN
                        SELECT currency_rate_alter, currency_id, rc.name INTO document_tc, document_currency_id, document_currency_name
                        FROM sale_order so INNER JOIN res_currency rc ON so.currency_id = rc.id WHERE so.id = tc_id;
                      ELSEIF tc_doc_type = 'INV' THEN
                        SELECT ai.currency_rate_alter, ai.currency_id, rc.name INTO document_tc, document_currency_id, document_currency_name
                        FROM account_invoice ai INNER JOIN res_currency rc ON ai.currency_id = rc.id WHERE ai.id = tc_id;
                      ELSEIF tc_doc_type = 'CURRENT' THEN
                        document_tc:=0;
                        tc_doc_date:=current_timestamp AT TIME ZONE 'UTC';
                      END IF;

                      IF document_tc is NULL or document_tc = 0 THEN
                        IF company_currency_name = 'USD' THEN
                          SELECT rcr.rate INTO document_tc FROM res_currency rc INNER JOIN res_currency_rate rcr ON rc.id = rcr.currency_id WHERE
                          rc.name = 'MXN' AND rcr.name <= tc_doc_date ORDER BY rcr.name DESC LIMIT 1;
                        ELSEIF  company_currency_name = 'MXN' THEN
                          SELECT rcr.rate INTO document_tc FROM res_currency rc INNER JOIN res_currency_rate rcr ON rc.id = rcr.currency_id WHERE
                            rc.name = 'USD' AND rcr.name <= tc_doc_date ORDER BY rcr.name DESC LIMIT 1;
                        END IF;
                        is_company_tc :=TRUE ;
                      END IF;
                      IF document_tc IS NULL OR document_tc = 0 THEN
                        document_tc :=1;
                      END IF;

                      IF is_company_tc = TRUE THEN
                        tc_tc :=  1/document_tc;
                      ELSE
                        tc_tc := document_tc;
                      END IF;
                      tc_tc:= round(tc_tc, 4);
                      RETURN tc_tc;
                    END;
                    $$
                    LANGUAGE plpgsql;
                """)
        cr.execute("""
            DROP VIEW IF EXISTS r_sales_by_date_data;
            create or replace view r_sales_by_date_data as (
                SELECT
                    row_number() over (order by T1.create_doc, T1.out_name) as id,
                    T1.sale_name,T1.out_name,T1.sales_name_id,T1.sales_name,
                    T1.create_doc as create_doc,
                    T1.customer_id,T1.customer_name,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN 'FAC'
                        WHEN 'out_refund' THEN 'NCC'
                        ELSE '' END type_doc,
                    T1.warehouse_id,T1.warehouse, T1.invoice_number,
                    CASE T1.invoice_state
                        WHEN 'open' THEN 'Abierto'
                        WHEN 'cancel' THEN 'Cancelado'
                        WHEN 'draft' THEN 'Borrador'
                        WHEN 'paid' THEN 'Pagado'
                        WHEN 'done' THEN 'Completado'
                        WHEN 'waiting_date' THEN 'Esperando fecha planificada'
                        WHEN 'progress' THEN 'Pedidos de venta'
                        WHEN 'manual' THEN 'Pedido a facturar'
                        WHEN 'shipping_except' THEN 'Excepción de envío'
                        WHEN 'invoice_except' THEN 'Excepción de factura'
                        WHEN 'wait_risk' THEN 'Waiting risk approval'
                        ELSE '' END state,
                    T1.pay_method,
                    T1.currency as currency_copy,
                    T1.currency_id as currency_copy_id,--currency,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN T1.subtotal
                        WHEN 'out_refund' THEN -(T1.subtotal)
                        END as subtotal_copy, --subtotal,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN T1.tax
                        WHEN 'out_refund' THEN -(T1.tax)
                        END as tax_copy,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN T1.total
                        WHEN 'out_refund' THEN -(T1.total)
                        END as total_copy, --total,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN round(cast(T1.cost as NUMERIC) , 2)
                        WHEN 'out_refund' THEN -( round(cast(T1.cost as NUMERIC) , 2))
                        END as cost_copy,
                    T1.rate ,
                    CASE
                        WHEN T1.invoice_type  = 'out_invoice' THEN ((T1.total-T1.tax) - T1.cost)
                        WHEN T1.invoice_type  = 'out_refund' THEN (-(T1.total-T1.tax) ) - (-T1.cost)
                        END as profit_copy,
                    CASE
                        WHEN T1.invoice_type = 'out_invoice' and T1.total > 0 THEN
                        ((T1.total-T1.tax) - T1.cost  ) * 100 / (T1.total-T1.tax)
                        WHEN T1.invoice_type = 'out_refund' and T1.total > 0 THEN
                        (((-(T1.total-T1.tax)) - (-T1.cost)) * 100 / (T1.total-T1.tax))
                        ELSE 0
                        END as profit_percent,
                    T1.account_invoice_id,
                    T1.currency_company,
                    NULL as currency,
                    NULL as subtotal,
                    NULL as tax,
                    NULL as total,
                    NULL as cost,
                    NULL as profit,
                    T1.related_invoice,
                    T1.client_order_ref
                FROM (
                    SELECT
                        CASE
                            WHEN ai.state IS NOT NULL THEN ai.invoice_datetime
                            WHEN ai.state IS NULL AND sp.date_done IS NOT NULL THEN sp.date_done
                            ELSE so.date_order
                            END create_doc,
                        sw.id as warehouse_id, sw.name as warehouse,
                        CASE
                            WHEN ai.state IS NULL THEN so.partner_id
                            ELSE rp.id
                            END as customer_id,
                        CASE
                            WHEN ai.state IS NULL THEN (SELECT rp.name FROM res_partner rp WHERE rp.id=so.partner_id)
                            ELSE rp.name
                            END as customer_name,
                        CASE
                            WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN (select name from res_currency where id=soia.currency_id) 
                            ELSE ''
                            END currency,
                        CASE
                            WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN soia.currency_id
                            ELSE NULL
                            END currency_id,
                        so.name as sale_name,
                        sp.name as out_name,
                        CASE
                            WHEN so.name IS NOT NULL THEN so.user_id
                            ELSE ai.user_id
                            END as sales_name_id,
                        rpu.name as sales_name,
                        COALESCE(ai.number,soia.number) AS invoice_number,
                        COALESCE(soia.state,so.state) AS invoice_state,
                        (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                           INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                                     WHERE aipmr.account_invoice_id = soia.id
                        )as pay_method,
                        COALESCE(soia.type, '') AS invoice_type,
                        CASE
                            WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN soia.amount_untaxed
                            ELSE 0
                            END as subtotal,
                        CASE
                            WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN soia.amount_tax
                            ELSE 0
                            END as tax, 
                        CASE
                            WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN soia.amount_total
                            ELSE 0
                            END as total, 
                        CASE
                            WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN(
                                SELECT  sum(
                                     (
                                       SELECT
                                         round(CAST (COALESCE(pph.cost,0) AS NUMERIC), 2) * ail.quantity as costo_producto
                                       FROM
                                        product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                                       WHERE pph.cost >0 and pp.id=ail.product_id AND pph.create_date <=sp.date ORDER BY pph.datetime desc LIMIT 1
                                     )
                                 )
                                 FROM account_invoice_line ail WHERE ail.invoice_id = soia.id
                            )
                            ELSE 0
                            END as cost,
                            CASE
                                WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN
                                    CAST(doc_tc(soia.id ,soia.company_id ,'INV',soia.invoice_datetime) AS TEXT)
                                ELSE NULL
                                END AS rate,
                            CASE
                                WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN soia.id
                                ELSE 0
                                END as account_invoice_id,
                            CASE
                                WHEN so.id = (select min(order_id) from sale_order_invoice_rel where invoice_id=soi.invoice_id) THEN rcom.currency_id
                                ELSE NULL
                                END as currency_company,
                            soi.invoice_id as related_invoice,
                            so.client_order_ref
                        FROM
                           sale_order so
                        INNER JOIN
                            stock_picking sp ON so.name=sp.origin
                        LEFT JOIN
                            account_invoice ai ON sp.id = ai.picking_dev_id
                            AND
                            ai.id is not null
                            AND
                            ai.type='out_refund'
                        INNER JOIN
                            stock_warehouse sw ON so.warehouse_id = sw.id
                        INNER JOIN
                            res_users ru ON so.user_id=ru.id
                        INNER JOIN
                            res_partner rpu ON ru.partner_id =rpu.id
                        LEFT JOIN
                            sale_order_invoice_rel soi ON soi.order_id=so.id
                        LEFT JOIN
                            account_invoice soia ON soia.id=soi.invoice_id
                        LEFT JOIN
                            res_partner rp ON soia.partner_id = rp.id
                        LEFT JOIN
                            res_currency rc ON soia.currency_id = rc.id
                        LEFT JOIN
                            res_company rcom ON soia.company_id = rcom.id
                        LEFT JOIN
                            res_currency rcc on rcc.id= rcom.currency_id
                        WHERE
                            so.state NOT IN ('draft','sent')
                            AND
                            (soia.type != 'out_refund' OR soia.type isnull)
                    UNION ALL

                    SELECT
                        ai.invoice_datetime as create_doc, sw.id as warehouse_id,
                        CASE
                            WHEN so.name IS NULL THEN '' ELSE sw.name
                            END as warehouse,
                        rp.id customer_id, rp.name customer_name,
                        rc.name as currency, ai.currency_id as currency_id,
                        CASE
                            WHEN ai.type='out_invoice' IS NULL THEN (SELECT s2o.name FROM sale_order_invoice_rel s2oi INNER JOIN sale_order s2o on s2o.id=s2oi.order_id where s2oi.invoice_id=53 order by s2oi.order_id limit 1)
                            ELSE so.name
                            END as sale_name,
                        CASE
                            WHEN ai.type = 'out_refund' AND ai.picking_dev_id is not NULL THEN (SELECT sp.name FROM stock_picking as sp WHERE id=ai.picking_dev_id)
                            ELSE ''
                            END as out_name,
                        ai.user_id as sales_name_id,
                        CASE
                            WHEN so.name IS NULL OR ai.type='out_refund' THEN (SELECT rp.name FROM res_partner as rp
                                WHERE rp.id=(SELECT ru.partner_id from res_users as ru WHERE ru.id=ai.user_id))
                            ELSE rpu.name
                            END as sales_name,
                        CASE
                            WHEN ai.number IS NULL THEN ''
                            ELSE ai.number
                            END AS invoice_number,
                        ai.state AS invoice_state,
                        (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                              INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                            WHERE aipmr.account_invoice_id = ai.id
                        )as pay_method,
                        ai.type AS invoice_type,
                        ai.amount_untaxed as subtotal, ai.amount_tax as tax, ai.amount_total as total, 
                        (
                            SELECT sum((
                                         SELECT
                                           round(CAST (COALESCE(pph.cost,0) AS NUMERIC),2) * ail.quantity costo_producto
                                         FROM
                                           product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                                         WHERE pp.id=ail.product_id AND pph.create_date <=ai.invoice_datetime ORDER BY pph.create_date desc LIMIT 1
                                       ))
                            FROM account_invoice_line ail
                            WHERE ail.invoice_id =ai.id
                        ) as cost,
                        CAST(doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS TEXT) AS rate,
                        COALESCE(ai.id,0) account_invoice_id,
                        rcom.currency_id as currency_company,
                        soi.invoice_id as related_invoice,
                        so.client_order_ref
                    FROM
                        account_invoice ai
                    LEFT JOIN
                        stock_picking spd ON spd.id=ai.picking_dev_id and ai.type='out_refund'
                    LEFT JOIN
                        stock_picking sp ON sp.name=spd.origin
                    LEFT JOIN
                        sale_order so on so.name = sp.origin
                    LEFT JOIN
                        stock_warehouse sw on so.warehouse_id = sw.id
                    INNER JOIN
                        res_partner rp on ai.partner_id =rp.id
                    INNER JOIN
                        res_currency rc on ai.currency_id=rc.id
                    INNER JOIN
                        res_company rcom on rcom.id= ai.company_id
                    INNER JOIN
                        res_currency rcc on rcc.id= rcom.currency_id
                    LEFT JOIN
                        res_users ru on so.user_id=ru.id
                    LEFT JOIN
                        res_partner rpu on ru.partner_id =rpu.id
                    LEFT JOIN
                        sale_order_invoice_rel soi on soi.order_id=so.id and soi.invoice_id=ai.id
                    WHERE ai.type IN ('out_refund')
                )AS T1
                WHERE
                    T1.invoice_state != 'draft'
            )
        """)

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        try:
            utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        except Exception:
            utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S.%f')            
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central
    
    @api.multi
    def to_csv_by_ids(self,ids):
        cancel = self._context['include_cancel']
        include_cancel = "" if cancel else " AND rssd.state != 'Cancelado' "
        ccontext = self._context['currency2convertname']
        ccost=ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        dataset_csv = [
            [
                'Folio de venta',
                'Referencia',
                unicode('Fecha de creación').encode('utf8'),
                'Ejecutivo',
                'Cliente',
                'Tipo de documento',
                unicode('Almacén').encode('utf8'),
                unicode('Orden de entrega').encode('utf8'),
                'Folio de la  factura',
                'Estado',
                'Método de pago',
                'Moneda Original del Documento',
                'Subtotal',
                'Impuesto',
                'Total',
                'Costo',
                'Utilidad',
                'Porcentaje de utilidad',
                'Tipo de cambio',
                'Moneda del Reporte',
            ]
        ]
        query = """DROP TABLE IF EXISTS csv_sales_by_date;
                   CREATE TEMPORARY TABLE csv_sales_by_date (id integer, sale_name CHARACTER VARYING, create_doc timestamp without time zone,
                   sales_name CHARACTER VARYING, customer_name CHARACTER VARYING, type_doc CHARACTER VARYING, warehouse CHARACTER VARYING,
                   out_name CHARACTER VARYING, invoice_number CHARACTER VARYING, state CHARACTER VARYING, pay_method CHARACTER VARYING, 
                   currency_copy CHARACTER VARYING, subtotal numeric, tax numeric, total numeric, "cost" numeric, profit numeric,
                   profit_percent numeric, rate CHARACTER VARYING, client_order_ref CHARACTER VARYING);
                   INSERT INTO csv_sales_by_date
                   SELECT rssd.id, rssd.sale_name, rssd.create_doc, rssd.sales_name, rssd.customer_name, rssd.type_doc, rssd.warehouse, rssd.out_name,
                   rssd.invoice_number, rssd.state, rssd.pay_method, rssd.currency_copy,
                   CASE WHEN rssd.subtotal_copy NOTNULL THEN currency_convert(rssd.currency_copy,{0},rssd.subtotal_copy,rssd.create_doc) ELSE NULL END as subtotal,
                   CASE WHEN rssd.tax_copy NOTNULL THEN currency_convert(rssd.currency_copy,{0},rssd.tax_copy,rssd.create_doc) ELSE NULL END,
                   CASE WHEN rssd.total_copy NOTNULL THEN currency_convert(rssd.currency_copy,{0},rssd.total_copy,rssd.create_doc) ELSE NULL END,
                   CASE WHEN rssd.cost_copy NOTNULL THEN currency_convert(rc.name,{2},rssd.cost_copy,rssd.create_doc) ELSE NULL END as cost,
                   NULL, NULL,
                   rssd.rate, rssd.client_order_ref
                   FROM r_sales_by_date_data rssd
                   LEFT OUTER JOIN res_currency rc on rc.id=rssd.currency_company
                   WHERE rssd.id in {1} {3};
                   UPDATE csv_sales_by_date set profit = round(abs(subtotal)-abs("cost"),2) WHERE type_doc in ('FAC','NCC') and subtotal NOTNULL;
                   UPDATE csv_sales_by_date set profit_percent = round(profit * 100 / abs(subtotal),2) WHERE profit>0 AND profit NOTNULL;
                   SELECT * FROM csv_sales_by_date order by id desc;
                   """.format(ccontext,data_ids,ccost,include_cancel)
        self.env.cr.execute(query)
        dataset = self.env.cr.dictfetchall()
        sum_subtotal = 0.00
        sum_total = 0.00
        sum_tax = 0.00
        sum_residual = 0.00
        for row in dataset:
            if row['state'] != 'Cancelado' and row['subtotal']!=None:
                sum_subtotal += row['subtotal']
                sum_tax += row['tax'] 
                sum_total += row['total']
            dataset_csv.append([
                unicode(self.set_default(row['sale_name'])).encode('utf8'),
                unicode(self.set_default(row['client_order_ref'])).encode('utf8'),
                unicode(self.utc_mexico_city(row['create_doc'])).encode('utf8'),
                unicode(self.set_default(row['sales_name'])).encode('utf8'),
                unicode(self.set_default(row['customer_name'])).encode('utf8'),
                unicode(self.set_default(row['type_doc'])).encode('utf8'),
                unicode(self.set_default(row['warehouse'])).encode('utf8'),
                unicode(self.set_default(row['out_name'])).encode('utf8'),
                unicode(self.set_default(row['invoice_number'])).encode('utf8'),
                unicode(self.set_default(row['state'])).encode('utf8'),
                unicode(self.set_default(row['pay_method'])).encode('utf8'),
                unicode(self.set_default(row['currency_copy'])).encode('utf8'),
                unicode(self.set_default(row['subtotal'], 0)).encode('utf8'),
                unicode(self.set_default(row['tax'], 0)).encode('utf8'),
                unicode(self.set_default(row['total'], 0)).encode('utf8'),
                unicode(self.set_default(row['cost'], 0)).encode('utf8'),
                unicode(self.set_default(row['profit'], 0)).encode('utf8'),
                unicode(self.set_default(row['profit_percent'], 0)).encode('utf8'),
                unicode(self.set_default(row['rate'], 0)).encode('utf8'),
                unicode(self.set_default(ccontext.replace("'",'') if self._context['currency2convertname'] else row['currency_copy'])).encode('utf8')
            ])
        dataset_csv.append(['', '', '', '', '', '', '', '', '', '', '', round(sum_subtotal,2), round(sum_tax,2), round(sum_total,2), '', ])
        obj_ocsv = OCsv()
        file = obj_ocsv.csv_base64(dataset_csv)
        return self.env['r.download'].create(vals={
            'file_name': 'Sales_by_date.csv',
            'type_file': 'csv',
            'file': file,
        })

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
class ReportsSalesSalesByDateScreen(osv.osv_memory):
    _name = 'sales_by_date_screen'
    
    sale_name = fields.Char(string='Folio de venta')
    client_order_ref = fields.Char(string='Referencia')
    out_name = fields.Char(string='Orden de entrega')
    sales_name = fields.Char(string='Ejecutivo')
    create_doc = fields.Datetime(string='Fecha de creacion')
    type_doc = fields.Char(string='Tipo de documento')
    warehouse = fields.Char(string='Almacen')
    invoice_number = fields.Char(string='Folio de la  factura')
    state = fields.Char(string='Estatus')
    pay_method = fields.Char(string=_('Método de pago'))
    currency_copy = fields.Char(string='Moneda')
    currency_copy_id = fields.Many2one('res.currency', string = 'Moneda documento')
    currency = fields.Char(string='Moneda del documento')
    subtotal_copy = fields.Float(string='Subtotal', digits=(10, 2))
    subtotal = fields.Float(string='Subtotal', digits=(10, 2))
    tax_copy = fields.Float(string='Impuesto', digits=(10, 2))
    tax = fields.Float(string='Impuesto',digits=(10, 2))
    total_copy = fields.Float(string='Total', digits=(10, 2))
    total = fields.Float(string='Total', digits=(10, 2))
    customer_name = fields.Char(string='Cliente')
    rate = fields.Char(string='Tipo de cambio')
    cost = fields.Float(string='Costo', digits=(10, 2))
    cost_copy = fields.Float(string='Costo', digits=(10, 2))
    profit = fields.Float(string='Utilidad', digits=(10, 2))
    profit_copy = fields.Float(digits=(10, 2))
    profit_percent = fields.Float(string='Porcentaje de utilidad', digits=(10, 2))
    account_invoice_id = fields.Many2one('account.invoice', string='account invoice')
    currency_company = fields.Integer(string='Company Currency')
    currency_show = fields.Char(string="Moneda del reporte")
    
    @api.multi
    def show_screen(self,ids):
        ccontext = self._context['currency2convertname']
        cancel = self._context['include_cancel']
        include_cancel = "" if cancel else " AND rssd.state != 'Cancelado' "
        ccost=ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        currency_show = ccontext if self._context['currency2convertname'] else 'rssd.currency_copy'
        query = """TRUNCATE TABLE sales_by_date_screen;
                   INSERT INTO sales_by_date_screen (id, sale_name, create_doc, sales_name, customer_name, type_doc, warehouse,
                   out_name, invoice_number, state, pay_method, currency_copy, currency, subtotal, tax, total, "cost", profit,
                   profit_percent, rate, currency_show,create_uid,write_uid,client_order_ref) 
                   SELECT rssd.id, rssd.sale_name, rssd.create_doc, rssd.sales_name, rssd.customer_name, rssd.type_doc, rssd.warehouse, rssd.out_name,
                   rssd.invoice_number, rssd.state, rssd.pay_method, rssd.currency_copy, rssd.currency_copy as currency,
                   CASE WHEN rssd.subtotal_copy NOTNULL THEN currency_convert(rssd.currency_copy,{0},rssd.subtotal_copy,rssd.create_doc) ELSE NULL END as subtotal,
                   CASE WHEN rssd.tax_copy NOTNULL THEN currency_convert(rssd.currency_copy,{0},rssd.tax_copy,rssd.create_doc) ELSE NULL END,
                   CASE WHEN rssd.total_copy NOTNULL THEN currency_convert(rssd.currency_copy,{0},rssd.total_copy,rssd.create_doc) ELSE NULL END,
                   CASE WHEN rssd.cost_copy NOTNULL THEN currency_convert(rc.name,{2},rssd.cost_copy,rssd.create_doc) ELSE NULL END as cost,
                   NULL, NULL,
                   rssd.rate, {3} as currency_show, {5}, {5}, rssd.client_order_ref
                   FROM r_sales_by_date_data rssd
                   LEFT OUTER JOIN res_currency rc on rc.id=rssd.currency_company
                   WHERE rssd.id in {1} {4};
                   UPDATE sales_by_date_screen set profit = round(abs(subtotal)-abs("cost"),2) WHERE type_doc in ('FAC','NCC') and subtotal NOTNULL;
                   UPDATE sales_by_date_screen set profit_percent = round(profit * 100 / abs(subtotal),2) WHERE profit>0 AND profit NOTNULL;
                   """.format(ccontext,data_ids,ccost,currency_show,include_cancel,self.env.uid)
        self.env.cr.execute(query)
        return True