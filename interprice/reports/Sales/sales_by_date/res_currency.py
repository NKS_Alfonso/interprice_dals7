# -*- coding: utf-8 -*-
# Copyright © 2019 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import api, models, fields, _
import logging
_logger = logging.getLogger(__name__)


class ResCurrencyInherit(models.Model):
    _inherit = 'res.currency'

#    @api.model
    def _get_conversion_rate(self, cr, uid, from_currency, to_currency, context=None):
        res = super(ResCurrencyInherit, self).\
        _get_conversion_rate(cr=cr, uid=uid, from_currency=from_currency, to_currency=to_currency, context=context)
        if 'date' in context and 'type' in context and 'sales_report' in context:
            date = context.get('date')
            type = context.get('type')
            sales_report = context.get('sales_report')
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if sales_report:
                sql = """select get_rate('{0}', '{1}', '{2}')""".format(type, from_currency.name, date)
                cr.execute(sql)
                result = cr.fetchone()[0]
                sql = """select get_rate('{0}', '{1}', '{2}')""".format(type, to_currency.name, date)
                cr.execute(sql)
                result2 = cr.fetchone()[0]
                if result== 0 or result2 == 0:
                    resultf = 0
                else:
                    resultf = (1/result2)/(1/result)
        return res
