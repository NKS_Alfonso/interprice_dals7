# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from olib.oCsv.oCsv import OCsv
from openerp import _, api, fields, exceptions, models


class ReportsBalanceOfBank(models.Model):
    _name = 'r.balance_of_bank'

    # domain methods
    @api.model
    def bank_domain(self):
        bank_ids = []
        self.env.cr.execute(
            """
            SELECT rpb.id FROM res_partner_bank rpb
            JOIN res_company rc ON rc.id=rpb.company_id
            ORDER BY rpb.id ASC
            """)
        if self.env.cr.rowcount:
            bank_ids = [bank_id[0] for bank_id in self.env.cr.fetchall()]

        return [('id', 'in', bank_ids)]

    account_bank_id = fields.Many2one(
        comodel_name='res.partner.bank',
        domain=bank_domain,
        help=_("Account bank of balance of bank report"),
        string=_("Account bank")
    )

    def _get_ids(self):
        account_bank_id = ''
        if self.account_bank_id:
            account_bank_id = ' WHERE bank_account_number_id = %s ' % \
                              (self.account_bank_id.id,)

        self.env.cr.execute(" SELECT id FROM r_balance_of_bank_data %s ORDER BY currency_name, bank_name ASC"
                            % (account_bank_id,))

        if self.env.cr.rowcount:
            dataset_ids_tmp = self.env.cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            report_id = self.ids[0]
            if type == 'show':
                self.env.cr.execute('DELETE FROM r_balance_of_bank WHERE id=%s', (report_id,))
        else:
            self.unlink()
            raise exceptions.Warning(_("No records found!"))
        return dataset_ids

    @api.multi
    def show_screen(self):
        data_ids = self._get_ids()
        return {
            'name': _('Balance of bank'),
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.balance.of.bank.data',
            'target': 'current',
            'context': {},
            'domain': [('id', 'in', data_ids)],
        }

    @api.multi
    def show_xls(self):
        ids = self._get_ids()
        csv_id = self.to_csv_by_ids(ids)

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    def to_csv_by_ids(self, _ids):
        data = self.env['r.balance.of.bank.data'].search([('id', 'in', _ids)])

        data_csv = [
            [
                _('Bank'),
                _('Account'),
                _('Currency'),
                _('MXN balance'),
                _('USD balance'),
                _('Rate'),
                _('MXN check'),
                _('USD check'),
                _('MXN check due'),
                _('USD check due'),
                _('MXN check expired'),
                _('USD check expired'),
                _('MXN deposit file'),
                _('USD deposit file'),
            ]
        ]
        sum_balance_bank_mxn = 0
        sum_balance_bank_usd = 0
        sum_check_balance_mxn = 0
        sum_check_balance_usd = 0
        sum_check_balance_due_mxn = 0
        sum_check_balance_due_usd = 0
        sum_expired_check_balance_mxn = 0
        sum_expired_check_balance_usd = 0
        sum_deposit_file_mxn = 0
        sum_deposit_file_usd = 0
        for row in data:
            sum_balance_bank_mxn += row.balance_bank_mxn
            sum_balance_bank_usd += row.balance_bank_usd
            sum_check_balance_mxn += row.check_balance_mxn
            sum_check_balance_usd += row.check_balance_usd
            sum_check_balance_due_mxn += row.check_balance_due_mxn
            sum_check_balance_due_usd += row.check_balance_due_usd
            sum_expired_check_balance_mxn += row.expired_check_balance_mxn
            sum_expired_check_balance_usd += row.expired_check_balance_usd
            sum_deposit_file_mxn += row.deposit_file_mxn
            sum_deposit_file_usd += row.deposit_file_usd
            data_csv.append([
                unicode(self._set_default(row.bank_name)).encode('utf8'),
                unicode(self._set_default(row.bank_account_number)).encode('utf8'),
                unicode(self._set_default(row.currency_name)).encode('utf8'),
                unicode(self._set_default(row.balance_bank_mxn, 0)).encode('utf8'),
                unicode(self._set_default(row.balance_bank_usd, 0)).encode('utf8'),
                unicode(self._set_default(row.purchase_rate_current)).encode('utf8'),
                unicode(self._set_default(row.check_balance_mxn, 0)).encode('utf8'),
                unicode(self._set_default(row.check_balance_usd, 0)).encode('utf8'),
                unicode(self._set_default(row.check_balance_due_mxn, 0)).encode('utf8'),
                unicode(self._set_default(row.check_balance_due_usd, 0)).encode('utf8'),
                unicode(self._set_default(row.expired_check_balance_mxn, 0)).encode('utf8'),
                unicode(self._set_default(row.expired_check_balance_usd, 0)).encode('utf8'),
                unicode(self._set_default(row.deposit_file_mxn, 0)).encode('utf8'),
                unicode(self._set_default(row.deposit_file_usd, 0)).encode('utf8')
            ])
        data_csv.append(['', '', '',
                         sum_balance_bank_mxn, sum_balance_bank_usd, '',
                         sum_check_balance_mxn, sum_check_balance_usd,
                         sum_check_balance_due_mxn, sum_check_balance_due_usd,
                         sum_expired_check_balance_mxn,
                         sum_expired_check_balance_usd, sum_deposit_file_mxn,
                         sum_deposit_file_usd])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'BalanceBankReport.csv',
                'type_file': 'csv',
                'file': file,
            })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default

    @api.multi
    def show_pdf(self):
        return self.env['report'].get_action(
            self.env['r.balance.of.bank.data'].browse(self._get_ids()),
            'reports.balance_bank_report_pdf'
        )
