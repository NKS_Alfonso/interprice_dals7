# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import _, fields, models
from openerp.tools.sql import drop_view_if_exists


class ReportsBalanceOfBankData(models.Model):
    _name = 'r.balance.of.bank.data'
    _table = 'r_balance_of_bank_data'
    _auto = False
    _order = 'currency_name, bank_name'

    bank_name = fields.Char(
        help=_("Bank name in balance of bank report"),
        string=_("Bank")
    )
    bank_account_number = fields.Char(
        help=_("Account name in balance of bank report"),
        string=_("Account")
    )
    currency_name = fields.Char(
        help=_("Currency name in balance of bank report"),
        string=_("Currency")
    )
    purchase_rate_current = fields.Char(
        help=_("Purchase rate current in balance of bank report"),
        string=_("Purchase rate current")
    )
    balance_bank_mxn = fields.Float(
        help=_("Balance bank MXN in balance of bank report"),
        string=_("Balance bank MXN")
    )
    balance_bank_usd = fields.Float(
        help=_("Balance bank USD in balance of bank report"),
        string=_("Balance bank USD")
    )
    check_balance_mxn = fields.Float(
        help=_("Check balance MXN in balance of bank report"),
        string=_("Check balance MXN")
    )
    check_balance_usd = fields.Float(
        help=_("Check balance USD in balance of bank report"),
        string=_("Check balance USD")
    )
    check_balance_due_mxn = fields.Float(
        help=_("Check balance due MXN in balance of bank report"),
        string=_("Check balance due MXN")
    )
    check_balance_due_usd = fields.Float(
        help=_("Check balance due USD in balance of bank report"),
        string=_("Check balance due USD")
    )
    expired_check_balance_mxn = fields.Float(
        help=_("Expired check balance MXN in balance of bank report"),
        string=_("Expired check balance MXN")
    )
    expired_check_balance_usd = fields.Float(
        help=_("Expired check balance USD in balance of bank report"),
        string=_("Expired check balance USD")
    )
    deposit_file_mxn = fields.Float(
        help=_("Deposit file MXN in balance of bank report"),
        string=_("Deposit file MXN")
    )
    deposit_file_usd = fields.Float(
        help=_("Deposit file USD in balance of bank report"),
        string=_("Deposit file USD")
    )

    def init(self, cr):
        drop_view_if_exists(cr, 'r_balance_of_bank_data')
        drop_view_if_exists(cr, 'r_balance_of_check_data')
        cr.execute(""" CREATE or REPLACE view r_balance_of_check_data as (
                        SELECT row_number() over (order by T1.account_check_id) AS id,
                            T1.balance_check,
                            T1.payment_date,
                            T1.journal_id,
                            T1.currency_id,
                            T1.company_currency_id,
                            CASE WHEN T1.currency_id IS NULL OR T1.currency_id = T1.company_currency_id
                            THEN T1.balance_check ELSE 0 END check_balace_mxn,
                            CASE WHEN T1.currency_id IS NOT NULL AND T1.currency_id != T1.company_currency_id
                            THEN T1.balance_check ELSE 0 END check_balace_usd
                        FROM
                        (
                            SELECT
                            ack.id account_check_id,
                            ack.amount balance_check,
                            ack.payment_date payment_date,
                            aj.id journal_id,
                            rc.id currency_id,
                            rccompany.id company_currency_id
                            FROM account_check ack
                            INNER JOIN account_journal aj ON aj.id=ack.journal_id
                            LEFT JOIN res_currency rc ON rc.id=aj.currency
                            INNER JOIN res_company rcompany ON rcompany.id=ack.destiny_partner_id
                            INNER JOIN res_currency rccompany ON rccompany.id=rcompany.currency_id
                            where ack.type='issue_check'
                            AND ack.state='handed'
                        )
                        AS T1
                        )"""
                   )

        cr.execute(""" CREATE or REPLACE view r_balance_of_bank_data as (
                        SELECT row_number() over (order by T1.bank_account_number_id) AS id,
                            T1.bank_account_number_id,
                            T1.bank_account_number,
                            T1.bank_name,
                            T1.currency_id,
                            T1.currency_name,
                            T1.company_currency_id,
                            T1.purchase_rate_current,
                            CASE WHEN T1.currency_id = T1.company_currency_id
                                THEN T1.balance_mxn ELSE 0 END balance_bank_mxn,
                            CASE WHEN T1.currency_id != T1.company_currency_id
                                THEN T1.balance_usd ELSE 0 END balance_bank_usd,
                            CASE WHEN T1.currency_id = T1.company_currency_id
                                THEN T1.check_mxn ELSE 0 END check_balance_mxn,
                            CASE WHEN T1.currency_id != T1.company_currency_id
                                THEN T1.check_usd ELSE 0 END check_balance_usd,
                            CASE WHEN T1.currency_id = T1.company_currency_id
                                THEN T1.check_due_mxn ELSE 0 END check_balance_due_mxn,
                            CASE WHEN T1.currency_id != T1.company_currency_id
                                THEN T1.check_due_usd ELSE 0 END check_balance_due_usd,
                            CASE WHEN T1.currency_id = T1.company_currency_id
                                THEN T1.expired_check_mxn ELSE 0 END expired_check_balance_mxn,
                            CASE WHEN T1.currency_id != T1.company_currency_id
                                THEN T1.expired_check_usd ELSE 0 END expired_check_balance_usd,
                            CASE WHEN T1.currency_id = T1.company_currency_id
                                THEN T1.deposit_file_amount ELSE 0 END deposit_file_mxn,
                            CASE WHEN T1.currency_id != T1.company_currency_id
                                THEN T1.deposit_file_amount ELSE 0 END deposit_file_usd
                        FROM
                        (
                            SELECT
                            rpb.id bank_account_number_id,
                            rpb.acc_number bank_account_number,
                            rb.name bank_name,
                            CASE WHEN rc.id IS NULL THEN rccompany.id
                             ELSE rc.id END currency_id,
                            CASE WHEN rc.name IS NULL THEN rccompany.name
                             ELSE rc.name END currency_name,
                            rccompany.id company_currency_id,
                            (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                WHERE name AT TIME ZONE 'utc' <= current_date
                                ORDER BY name DESC LIMIT 1) purchase_rate_current,
                            (SELECT
                                sum(debit)-sum(credit)
                                FROM account_move am
                                INNER JOIN account_move_line aml
                                ON aml.move_id=am.id
                                WHERE aml.account_id=aj.default_debit_account_id
                                AND am.state='posted') balance_mxn,
                            (SELECT
                                sum(amount_currency)
                                FROM account_move am
                                INNER JOIN account_move_line aml
                                ON aml.move_id=am.id
                                WHERE aml.account_id=aj.default_debit_account_id
                                AND am.state='posted') balance_usd,
                            (SELECT COALESCE(sum(check_balace_mxn),0) FROM r_balance_of_check_data
                                WHERE journal_id=aj.id) check_mxn,
                            (SELECT COALESCE(sum(check_balace_usd),0) FROM r_balance_of_check_data
                                WHERE journal_id=aj.id) check_usd,
                            (SELECT COALESCE(sum(check_balace_mxn), 0) FROM r_balance_of_check_data
                                WHERE journal_id=aj.id AND payment_date >= current_date) check_due_mxn,
                            (SELECT COALESCE(sum(check_balace_usd), 0) FROM r_balance_of_check_data
                                WHERE journal_id=aj.id AND payment_date >= current_date) check_due_usd,
                            (SELECT COALESCE(sum(check_balace_mxn), 0) FROM r_balance_of_check_data
                                WHERE journal_id=aj.id AND payment_date < current_date) expired_check_mxn,
                            (SELECT COALESCE(sum(check_balace_usd), 0) FROM r_balance_of_check_data
                                WHERE journal_id=aj.id AND payment_date < current_date) expired_check_usd,
                            (SELECT COALESCE(sum(dfv.amount), 0) deposit_file_amount
                                FROM deposit_file df
                                INNER JOIN deposit_file_voucher dfv
                                ON dfv.deposit_file=df.id
                                INNER JOIN res_partner_bank rpb
                                ON rpb.id=df.bank
                                INNER JOIN account_voucher av
                                ON av.id=dfv.voucher
                                INNER JOIN res_company rcompany
                                ON rcompany.id=av.company_id
                                WHERE rpb.journal_id=aj.id
                                AND df.state='normal') deposit_file_amount
                            FROM res_partner_bank rpb
                            INNER JOIN res_bank rb ON rb.id=rpb.bank
                            LEFT JOIN res_currency rc ON rc.id=rpb.currency2_id
                            INNER JOIN account_journal aj
                            ON aj.id=rpb.journal_id
                            INNER JOIN res_company rcompany
                            ON rcompany.id=rpb.company_id
                            INNER JOIN res_currency rccompany
                            ON rccompany.id=rcompany.currency_id
                        )
                        AS T1
                        )"""
                   )
