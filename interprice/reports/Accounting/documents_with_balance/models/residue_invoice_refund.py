# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, models, tools
from .r_residue_doc_customer import DocumentsWithResidueCustomer
from .r_residue_doc_supplier import DocumentsWithResidueSupplier
from .r_payment_doc_customer import DocumentsWithPaymentCustomer
from .r_payment_doc_supplier import DocumentsWithBalanceSupplier
from .voucher_line_receipt_payment import VoucherLineReceiptPayment
from .voucher_receipt_payment import VoucherReceiptPayment


class ResidueInvoiceRefund(models.Model):
    """Documents With Balance report"""
    _auto = False
    _name = 'residue_invoice_refund'
    _order = 'doc_date_issue, doc_id, doc_number ASC'

    def init(self, cr):
        # Drop view
        tools.sql.drop_view_if_exists(cr, 'residue_invoice_refund')
        # Create view and function
        cr.execute(
            """
            CREATE OR REPLACE FUNCTION get_rate(TEXT, TEXT, TIMESTAMP)
              RETURNS NUMERIC
            LANGUAGE plpgsql
            AS $$
            DECLARE
              type_rate ALIAS FOR $1;
              currency ALIAS FOR $2;
              last_update ALIAS FOR $3;

              tc_tc NUMERIC(10, 4) := 1;

              tv    FLOAT;
              tc    FLOAT;


            BEGIN

              SELECT
                rcr.rate_sale,
                rcr.rate
              INTO tv, tc
              FROM
                res_currency rc INNER JOIN res_currency_rate rcr ON rc.id = rcr.currency_id
              WHERE
                rc.name = currency AND rcr.name <= last_update
              ORDER BY rcr.name DESC
              LIMIT 1;
              IF type_rate = 'sale'
              THEN
                IF tv > 0
                THEN
                  tc_tc:= 1 / tv;
                END IF;

              ELSE
                IF tc > 0
                THEN
                  tc_tc:= 1 / tc;
                END IF;
              END IF;

              RETURN tc_tc;
            END;
            $$;


            CREATE OR REPLACE VIEW residue_invoice_refund AS (
              SELECT
                ai.id               doc_id,
                ai.type             doc_type,
                ai.state            doc_state,
                ai.partner_id,
                rp.name             partner_name,
                apts.name           supplier_days_credit,
                0                   supplier_limit_credit,
                aptc.name           customer_days_credit,
                ipl.value_float     customer_limit_credit,
                ai.number           doc_number,
                ai.origin           doc_origin,
                ai.invoice_datetime doc_date_issue,
                ai.date_due + INTERVAL '23 hours' doc_date_due,
                rc.name             doc_currency,
                ai.amount_untaxed   doc_amount_untaxed,
                ai.amount_tax       doc_amount_taxed,
                0                   doc_residue,
                0                   doc_residue_usd,
                ai.amount_total     doc_amount
              FROM
                account_invoice ai
                INNER JOIN res_partner rp ON ai.partner_id = rp.id
                LEFT JOIN ir_property ips ON ips.name = 'property_supplier_payment_term' AND
                                             cast(regexp_replace(ips.res_id, '\D', '', 'g') AS INT) = rp.id
                LEFT JOIN account_payment_term apts
                  ON cast(regexp_replace(ips.value_reference, '\D', '', 'g') AS INT) = apts.id

                LEFT JOIN ir_property ipc
                  ON ipc.name = 'property_payment_term' AND cast(regexp_replace(ipc.res_id, '\D', '', 'g') AS INT) = rp.id
                LEFT JOIN account_payment_term aptc
                  ON cast(regexp_replace(ipc.value_reference, '\D', '', 'g') AS INT) = aptc.id

                LEFT JOIN ir_property ipl
                  ON ipl.name = 'company_credit_limit' AND cast(regexp_replace(ipl.res_id, '\D', '', 'g') AS INT) = rp.id
                INNER JOIN res_currency rc ON ai.currency_id = rc.id
            );
            """
        )
        DocumentsWithResidueCustomer.create_r_residue_doc_customer(cr)
        DocumentsWithResidueSupplier.create_r_residue_doc_supplier(cr)
        VoucherReceiptPayment.create_voucher_receipt_payment(cr)
        VoucherLineReceiptPayment.create_voucher_line_receipt_payment(cr)
        DocumentsWithPaymentCustomer.create_r_payment_doc_customer(cr)
        DocumentsWithBalanceSupplier.create_r_payment_doc_supplier(cr)

