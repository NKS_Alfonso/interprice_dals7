# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, api, tools
import openerp.addons.decimal_precision as dp
from r_payment_doc_customer import doc_state_payment_residue_list, \
    doc_type_payment_residue_list, doc_days_credit_list


class DocumentsWithResidueSupplier(models.Model):
    """Documents With Residue Supplier report"""

    # odoo model properties
    _name = 'r.residue.doc.supplier'
    _table = 'r_residue_doc_supplier'
    _description = 'Documents With Residue Supplier report'
    _auto = False  # Don't let odoo create table
    _order = 'doc_date_issue, doc_id, doc_number ASC'

    # initial methods
    @api.multi
    def get_false(self):
        """Method False"""
        pass

    @api.one
    def get_residue(self):
        if self.doc_currency == 'USD':
            self.doc_residue_usd = self.doc_id.residual
            self.doc_residue = self.doc_id.residual * self.current_rate
            self.amount_usd = self.doc_amount
            self.amount_mxn = self.doc_amount * self.current_rate
        else:
            self.doc_residue = self.doc_id.residual
            doc_residue_usd = self.doc_id.residual / self.current_rate
            self.doc_residue_usd = doc_residue_usd
            self.amount_mxn = self.doc_amount
            amount_usd = self.doc_amount / self.current_rate
            self.amount_usd = amount_usd

    # Selection list
    doc_type_list = doc_type_payment_residue_list
    doc_state_list = doc_state_payment_residue_list
    doc_days_credit_list = doc_days_credit_list
    # View fields
    doc_id = fields.Many2one(
        comodel_name='account.invoice',
        readonly=True,
        string=_("Document ID"),
    )
    doc_type = fields.Selection(
        selection=doc_type_list,
        readonly=True,
        string=_("Type Document"),
    )
    partner_id = fields.Integer(
        readonly=True,
        string=_("Partner ID"),
    )
    partner_name = fields.Char(
        readonly=True,
        string=_("Partner Name"),
    )
    supplier_days_credit = fields.Selection(
        selection=doc_days_credit_list,
        readonly=True,
        string=_("Supplier Days credit"),
    )
    supplier_limit_credit = fields.Float(
        readonly=True,
        string=_("Supplier Limit credit"),
    )
    doc_state = fields.Selection(
        selection=doc_state_list,
        readonly=True,
        string=_("State"),
    )
    doc_number = fields.Char(
        readonly=True,
        string=_("Document Number"),
    )
    doc_origin = fields.Char(
        readonly=True,
        string=_("Document Origin"),
    )
    doc_date_issue = fields.Datetime(
        readonly=True,
        string=_("Date Issue"),
    )
    doc_date_due = fields.Datetime(
        readonly=True,
        string=_("Date Due"),
    )
    doc_currency = fields.Char(
        readonly=True,
        string=_("Currency"),
    )
    doc_amount_untaxed = fields.Float(
        digits=dp.get_precision('Account'),
        readonly=True,
        string=_("Amount untaxed"),
    )
    doc_amount_taxed = fields.Float(
        digits=dp.get_precision('Account'),
        readonly=True,
        string=_("Amount taxed"),
    )
    amount_mxn = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Amount MXN"),
    )
    amount_usd = fields.Float(
        digits=(10, 4),
        compute=get_false,
        readonly=True,
        string=_("Amount USD"),
    )
    doc_residue = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Residual MXN"),
    )
    doc_residue_usd = fields.Float(
        digits=(10, 4),
        compute=get_false,
        readonly=True,
        string=_("Residual USD"),
    )
    doc_amount = fields.Float(
        readonly=True,
        string=_("Amount"),
    )
    current_rate = fields.Float(
        digits=(10, 4),
        readonly=True,
        string=_("Current rate"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_r_residue_doc_supplier(cr):
        tools.sql.drop_view_if_exists(cr, 'r_residue_doc_supplier')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_residue_doc_supplier AS (
            SELECT
              row_number()
              OVER (
                ORDER BY doc_id ) AS id,
              *,
              get_rate('purchase', 'USD', current_timestamp AT TIME ZONE 'utc') current_rate
            FROM (
                   SELECT
                     doc_id,
                     doc_type,
                     partner_id,
                     partner_name,
                     supplier_days_credit,
                     supplier_limit_credit,
                     doc_state,
                     doc_number,
                     doc_origin,
                     doc_date_issue,
                     doc_date_due,
                     doc_currency,
                     doc_amount_untaxed,
                     doc_amount_taxed,
                     doc_residue,
                     doc_residue_usd,
                     doc_amount
                   FROM residue_invoice_refund rir
                   WHERE doc_type IN ('in_invoice', 'in_refund')
                 ) r_residue_doc_supplier
            );
            """
        )

