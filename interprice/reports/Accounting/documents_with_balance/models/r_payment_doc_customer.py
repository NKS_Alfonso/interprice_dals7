# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, api, tools

doc_state_payment_residue_list = [
        ('open', _('open')),
        ('generated', _('generated')),
        ('paid', _('paid')),
        ('applied partial', _('applied partial')),
        ('associated', _('associated')),
        ('partial_applied', _('partial_applied')),
        ('canceled', _('canceled')),
        ('cancel', _('cancel')),
        ('posted', _('posted')),
        ('applied', _('applied')),
    ]
doc_type_payment_residue_list = [
        ('in_invoice', _('in_invoice')),
        ('out_invoice', _('out_invoice')),
        ('voucher_payment', _('voucher_payment')),
        ('voucher_receipt', _('voucher_receipt')),
        ('in_refund', _('in_refund')),
        ('out_refund', _('out_refund')),
        ('advance_payment', _('advance_payment')),
        ('unidentified_deposit', _('unidentified_deposit')),
    ]
doc_days_credit_list = [
        ('Immediate Payment', _('Immediate Payment')),
        ('15 Days', _('15 Days')),
        ('30 Net Days', _('30 Net Days')),
        ('60 Dias', _('60 Dias')),
    ]


class DocumentsWithPaymentCustomer(models.Model):
    """Documents With Receive Customer report"""

    # odoo model properties
    _name = 'r.payment.doc.customer'
    _table = 'r_payment_doc_customer'
    _description = 'Documents With Receive Customer report'
    _auto = False  # Don't let odoo create table
    _order = 'doc_date_issue, doc_id, doc_number ASC'

    # initial methods
    @api.multi
    def get_false(self):
        """Method False"""
        pass

    @api.one
    def get_residue(self):
        if self.doc_id:
            if self.doc_currency == 'USD':
                self.doc_residue_usd = self.doc_id.residual
                self.doc_residue_mxn = self.doc_id.residual * self.current_rate
                self.doc_amount_usd = self.doc_amount
                self.doc_amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue_mxn = self.doc_id.residual
                self.doc_residue_usd = self.doc_id.residual / self.current_rate
                self.doc_amount_usd = self.doc_amount / self.current_rate
                self.doc_amount_mxn = self.doc_amount
        elif self.doc_id_ud:
            current_balance = self.doc_id_ud.current_balance
            if self.doc_currency == 'USD':
                self.doc_residue_usd = current_balance
                self.doc_residue_mxn = current_balance * self.current_rate
                self.doc_amount_usd = self.doc_amount
                self.doc_amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue_mxn = current_balance
                self.doc_residue_usd = current_balance / self.current_rate
                self.doc_amount_usd = self.doc_amount / self.current_rate
                self.doc_amount_mxn = self.doc_amount
        elif self.doc_id_ap:
            current_balance = self.doc_id_ap.current_balance
            if self.doc_currency == 'USD':
                self.doc_residue_usd = current_balance
                self.doc_residue_mxn = current_balance * self.current_rate
                self.doc_amount_usd = self.doc_amount
                self.doc_amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue_mxn = current_balance
                self.doc_residue_usd = current_balance / self.current_rate
                self.doc_amount_usd = self.doc_amount / self.current_rate
                self.doc_amount_mxn = self.doc_amount
        elif self.doc_id_av:
            if self.doc_currency == 'USD':
                self.doc_residue_mxn = self.doc_residue * self.current_rate
                self.doc_residue_usd = self.doc_residue
                self.doc_amount_usd = self.doc_amount
                self.doc_amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue_mxn = self.doc_residue
                self.doc_residue_usd = self.doc_residue / self.current_rate
                self.doc_amount_usd = self.doc_amount / self.current_rate
                self.doc_amount_mxn = self.doc_amount

    # Selection list

    # View fields
    doc_id = fields.Many2one(
        comodel_name='account.invoice',
        readonly=True,
        string=_("Document ID"),
    )
    doc_id_ud = fields.Many2one(
        comodel_name='dni.dni',
        readonly=True,
        string=_("DNI ID"),
    )
    doc_id_ap = fields.Many2one(
        comodel_name='advance.payment',
        readonly=True,
        string=_("Advance ID"),
    )
    doc_id_av = fields.Many2one(
        comodel_name='account.voucher',
        readonly=True,
        string=_("Voucher ID"),
    )
    doc_type = fields.Selection(
        selection=doc_type_payment_residue_list,
        readonly=True,
        string=_("Type Document"),
    )
    partner_id = fields.Integer(
        readonly=True,
        string=_("Partner ID"),
    )
    partner_name = fields.Char(
        readonly=True,
        string=_("Partner Name"),
    )
    customer_days_credit = fields.Selection(
        selection=doc_days_credit_list,
        readonly=True,
        string=_("Customer Days credit"),
    )
    customer_limit_credit = fields.Float(
        readonly=True,
        string=_("Customer Limit credit"),
    )
    doc_state = fields.Selection(
        selection=doc_state_payment_residue_list,
        readonly=True,
        string=_("State"),
    )
    doc_number = fields.Char(
        readonly=True,
        string=_("Document Number"),
    )
    doc_origin = fields.Char(
        readonly=True,
        string=_("Document Origin"),
    )
    doc_date_issue = fields.Datetime(
        readonly=True,
        string=_("Date Issue"),
    )
    doc_date_due = fields.Datetime(
        readonly=True,
        string=_("Date Due"),
    )
    doc_currency = fields.Char(
        readonly=True,
        string=_("Currency"),
    )
    doc_amount = fields.Float(
        readonly=True,
        string=_("Amount"),
    )
    doc_amount_mxn = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Amount MXN"),
    )
    doc_amount_usd = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Amount USD"),
    )
    doc_residue = fields.Float(
        readonly=True,
        string=_("Residual"),
    )
    doc_residue_mxn = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Residual MXN"),
    )
    doc_residue_usd = fields.Float(
        digits=(10, 4),
        compute=get_false,
        readonly=True,
        string=_("Residual USD"),
    )
    current_rate = fields.Float(
        digits=(10, 4),
        readonly=True,
        string=_("Current rate"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_r_payment_doc_customer(cr):
        tools.sql.drop_view_if_exists(cr, 'r_payment_doc_customer')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_payment_doc_customer AS (
                SELECT
                    row_number()
                    OVER (
                    ORDER BY doc_id ) AS id,
                    *,
                    get_rate('sale', 'USD', CURRENT_TIMESTAMP AT TIME ZONE 'utc') current_rate
                FROM (
                 SELECT
                   doc_id,
                   doc_id_ud,
                   doc_id_ap,
                   0 doc_id_av,
                   doc_type,
                   partner_id,
                   partner_name,
                   customer_days_credit,
                   customer_limit_credit,
                   doc_state,
                   doc_number,
                   doc_origin,
                   doc_date_issue,
                   doc_date_due,
                   doc_currency,
                   doc_amount,
                   0 doc_amount_mxn,
                   0 doc_amount_usd,
                   doc_residue,
                   0 doc_residue_mxn,
                   0 doc_residue_usd
                 FROM r_residue_doc_customer

                 UNION ALL

                 SELECT
                   0      doc_id,
                   0      doc_id_ud,
                   0      doc_id_ap,
                   doc_id doc_ic_av,
                   doc_type,
                   partner_id,
                   partner_name,
                   customer_days_credit,
                   customer_limit_credit,
                   doc_state,
                   doc_number,
                   doc_origin,
                   doc_date_issue,
                   doc_date_due,
                   doc_currency,
                   doc_amount,
                   doc_amount_mxn,
                   doc_amount_usd,
                   doc_residue,
                   doc_residue_mxn,
                   doc_residue_usd
                 FROM
                   voucher_line_receipt_payment
                 WHERE doc_type = 'voucher_receipt'
                ) T3
              ORDER BY doc_id, doc_number, doc_date_issue ASC);
            """
        )
