# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, fields, models, api, tools
import openerp.addons.decimal_precision as dp
from r_payment_doc_customer import doc_state_payment_residue_list, \
    doc_type_payment_residue_list, doc_days_credit_list


class DocumentsWithResidueCustomer(models.Model):
    """Documents With Residue Customer report"""

    # odoo model properties
    _name = 'r.residue.doc.customer'
    _table = 'r_residue_doc_customer'
    _description = 'Documents With Residue Customer report'
    _auto = False  # Don't let odoo create table
    _order = 'doc_date_issue, doc_id, doc_number ASC'

    # initial methods
    @api.multi
    def get_false(self):
        """Method False"""
        pass

    @api.one
    def get_residue(self):
        if self.doc_id:
            if self.doc_currency == 'USD':
                self.doc_residue_usd = self.doc_id.residual
                self.doc_residue = self.doc_id.residual * self.current_rate
                self.amount_usd = self.doc_amount
                self.amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue = self.doc_id.residual
                self.doc_residue_usd = self.doc_id.residual / self.current_rate
                self.amount_mxn = self.doc_amount
                self.amount_usd = self.doc_amount / self.current_rate
        elif self.doc_id_ud:
            current_balance = self.doc_id_ud.current_balance
            if self.doc_currency == 'USD':
                self.doc_residue_usd = current_balance
                self.doc_residue = current_balance * self.current_rate
                self.amount_usd = self.doc_amount
                self.amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue = current_balance
                self.doc_residue_usd = current_balance / self.current_rate
                self.amount_mxn = self.doc_amount
                self.amount_usd = self.doc_amount / self.current_rate
        elif self.doc_id_ap:
            current_balance = self.doc_id_ap.current_balance
            if self.doc_currency == 'USD':
                self.doc_residue_usd = current_balance
                self.doc_residue = current_balance * self.current_rate
                self.amount_usd = self.doc_amount
                self.amount_mxn = self.doc_amount * self.current_rate
            else:
                self.doc_residue = current_balance
                self.doc_residue_usd = current_balance / self.current_rate
                self.amount_mxn = self.doc_amount
                self.amount_usd = self.doc_amount / self.current_rate

    # Selection list
    doc_type_list = doc_type_payment_residue_list
    doc_state_list = doc_state_payment_residue_list
    doc_days_credit_list = doc_days_credit_list
    # View fields
    doc_id = fields.Many2one(
        comodel_name='account.invoice',
        readonly=True,
        string=_("Document ID"),
    )
    doc_id_ud = fields.Many2one(
        comodel_name='dni.dni',
        readonly=True,
        string=_("DNI ID"),
    )
    doc_id_ap = fields.Many2one(
        comodel_name='advance.payment',
        readonly=True,
        string=_("Advance ID"),
    )
    doc_type = fields.Selection(
        selection=doc_type_list,
        readonly=True,
        string=_("Type Document"),
    )
    partner_id = fields.Integer(
        readonly=True,
        string=_("Partner ID"),
    )
    partner_name = fields.Char(
        readonly=True,
        string=_("Partner Name"),
    )
    customer_days_credit = fields.Selection(
        selection=doc_days_credit_list,
        readonly=True,
        string=_("Customer Days credit"),
    )
    customer_limit_credit = fields.Float(
        readonly=True,
        string=_("Customer Limit credit"),
    )
    doc_state = fields.Selection(
        selection=doc_state_list,
        readonly=True,
        string=_("State"),
    )
    doc_number = fields.Char(
        readonly=True,
        string=_("Document Number"),
    )
    doc_origin = fields.Char(
        readonly=True,
        string=_("Document Origin"),
    )
    doc_date_issue = fields.Datetime(
        readonly=True,
        string=_("Date Issue"),
    )
    doc_date_due = fields.Datetime(
        readonly=True,
        string=_("Date Due"),
    )
    doc_currency = fields.Char(
        readonly=True,
        string=_("Currency"),
    )
    doc_amount_untaxed = fields.Float(
        digits=dp.get_precision('Account'),
        readonly=True,
        string=_("Amount untaxed"),
    )
    doc_amount_taxed = fields.Float(
        digits=dp.get_precision('Account'),
        readonly=True,
        string=_("Amount taxed"),
    )
    amount_mxn = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Amount MXN"),
    )
    amount_usd = fields.Float(
        digits=(10, 4),
        compute=get_false,
        readonly=True,
        string=_("Amount USD"),
    )
    doc_residue = fields.Float(
        digits=(10, 4),
        compute=get_residue,
        readonly=True,
        string=_("Residual MXN"),
    )
    doc_residue_usd = fields.Float(
        digits=(10, 4),
        compute=get_false,
        readonly=True,
        string=_("Residual USD"),
    )
    doc_amount = fields.Float(
        readonly=True,
        string=_("Amount"),
    )
    current_rate = fields.Float(
        digits=(10, 4),
        readonly=True,
        string=_("Current rate"),
    )

    # Create PostgreSQL view
    @staticmethod
    def create_r_residue_doc_customer(cr):
        tools.sql.drop_view_if_exists(cr, 'r_residue_doc_customer')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_residue_doc_customer AS (
            SELECT
              row_number()
              OVER (
                ORDER BY doc_id ) AS id,
              *,
              get_rate('sale', 'USD', current_timestamp AT TIME ZONE 'utc') current_rate
            FROM (
                   SELECT
                     doc_id,
                     0 doc_id_ud,
                     0 doc_id_ap,
                     doc_type,
                     partner_id,
                     partner_name,
                     customer_days_credit,
                     customer_limit_credit,
                     doc_state,
                     doc_number,
                     doc_origin,
                     doc_date_issue,
                     doc_date_due,
                     doc_currency,
                     doc_amount_untaxed,
                     doc_amount_taxed,
                     doc_residue,
                     doc_residue_usd,
                     doc_amount
                   FROM residue_invoice_refund rir
                   WHERE doc_type IN ('out_invoice', 'out_refund')

                   UNION ALL

                   SELECT
                     0                      doc_id,
                     dni.id                 doc_id_ud,
                     0                      doc_id_ap,
                     'unidentified_deposit' doc_type,
                     dni.partner_id,
                     rp.name                partner_name,
                     aptc.name              customer_days_credit,
                     ipl.value_float        customer_limit_credit,
                     dni.state              doc_state,
                     dni.name               doc_number,
                     ''                     doc_origin,
                     dni.registration_date  doc_date_issue,
                     dni.registration_date  doc_date_due,
                     rc.name                doc_currency,
                     amount - tax           doc_amount_untaxed,
                     tax                    doc_amount_taxed,
                     0                      doc_residue,
                     0                      doc_residue_usd,
                     dni.amount             doc_amount
                   FROM
                     dni_dni dni INNER JOIN res_partner rp ON dni.partner_id = rp.id
                     LEFT JOIN ir_property ipc
                       ON ipc.name = 'property_payment_term' AND cast(regexp_replace(ipc.res_id, '\D', '', 'g') AS INT) = rp.id
                     LEFT JOIN account_payment_term aptc
                       ON cast(regexp_replace(ipc.value_reference, '\D', '', 'g') AS INT) = aptc.id

                     LEFT JOIN ir_property ipl
                       ON ipl.name = 'company_credit_limit' AND cast(regexp_replace(ipl.res_id, '\D', '', 'g') AS INT) = rp.id
                     INNER JOIN res_currency rc ON dni.currency_id = rc.id

                   UNION ALL

                   SELECT
                     0                  doc_id,
                     0                  doc_id_ud,
                     ap.id              doc_id_ap,
                     'advance_payment'  doc_type,
                     ap.partner_id,
                     rp.name            partner_name,
                     aptc.name          customer_days_credit,
                     ipl.value_float    customer_limit_credit,
                     ap.state           doc_state,
                     ap.number          doc_number,
                     ''                 doc_origin,
                     ap.register_date   doc_date_issue,
                     ap.register_date   doc_date_due,
                     rc.name            doc_currency,
                     ap.amount - ap.tax doc_amount_untaxec,
                     ap.tax             doc_amount_taxed,
                     0                  doc_residue,
                     0                  doc_residue_usd,
                     ap.amount          doc_amount

                   FROM advance_payment ap
                     INNER JOIN res_partner rp ON ap.partner_id = rp.id
                     LEFT JOIN ir_property ipc
                       ON ipc.name = 'property_payment_term' AND cast(regexp_replace(ipc.res_id, '\D', '', 'g') AS INT) = rp.id
                     LEFT JOIN account_payment_term aptc
                       ON cast(regexp_replace(ipc.value_reference, '\D', '', 'g') AS INT) = aptc.id

                     LEFT JOIN ir_property ipl
                       ON ipl.name = 'company_credit_limit' AND cast(regexp_replace(ipl.res_id, '\D', '', 'g') AS INT) = rp.id
                     INNER JOIN res_currency rc ON ap.currency_id = rc.id

                 ) r_residue_doc_customer
                 );
            """
        )
