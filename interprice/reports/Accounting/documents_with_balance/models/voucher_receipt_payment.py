# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import tools


class VoucherReceiptPayment:
    """Documents With Balance report"""

    @staticmethod
    def create_voucher_receipt_payment(cr):
        # Drop view
        tools.sql.drop_view_if_exists(cr, 'voucher_receipt_payment')
        # Create view and function
        cr.execute(
            """
            CREATE OR REPLACE FUNCTION sum_voucher_line_amount(INTEGER, TEXT)
              RETURNS NUMERIC
            LANGUAGE plpgsql
            AS $$
            DECLARE
              doc_id ALIAS FOR $1;
              type_line ALIAS FOR $2;

              sum_lines FLOAT := 0;

            BEGIN

              SELECT coalesce(sum(avl.amount), 0)
              INTO sum_lines
              FROM
                account_voucher_line avl
              WHERE
                avl.voucher_id = doc_id AND avl.amount > 0
                AND avl.type = type_line;
              RETURN sum_lines;
            END;
            $$;


            CREATE OR REPLACE VIEW voucher_receipt_payment AS (
              SELECT
            av.id                 doc_id,
            'voucher_' || av.type doc_type,
            av.partner_id,
            rp.name               partner_name,
            apts.name             supplier_days_credit,
            0                     supplier_limit_credit,
            aptc.name             customer_days_credit,
            ipl.value_float       customer_limit_credit,
            av.state              doc_state,
            av.number             doc_number,
            CAST('|' AS TEXT)     doc_origin,
            CASE WHEN am.create_date IS NOT NULL
              THEN
                am.create_date
            ELSE
              av.date + INTERVAL '5 hours'
            END
                                  doc_date_issue,
            CASE WHEN am.create_date IS NOT NULL
              THEN
                am.create_date
            ELSE
              av.date + INTERVAL '5 hours'
            END
                                  doc_date_due,
            rc.name               doc_currency,
            av.amount +
            CASE WHEN av.type = 'receipt'
              THEN
                sum_voucher_line_amount(av.id, 'dr')
            ELSE
              sum_voucher_line_amount(av.id, 'cr')
            END
                                  doc_amount,
            0                     doc_amount_mxn,
            0                     doc_amount_usd,
            (av.amount +
             CASE WHEN av.type = 'receipt'
               THEN
                 sum_voucher_line_amount(av.id, 'dr')
             ELSE
               sum_voucher_line_amount(av.id, 'cr')
             END) -
            (CASE WHEN av.type = 'receipt'
              THEN
                sum_voucher_line_amount(av.id, 'cr')
             ELSE
               sum_voucher_line_amount(av.id, 'dr')
             END)
                                  doc_residue,
            0                     doc_residue_mxn,
            0                     doc_residue_usd
          FROM account_voucher av
            INNER JOIN res_partner rp ON av.partner_id = rp.id
            LEFT JOIN ir_property ips ON ips.name = 'property_supplier_payment_term' AND
                                         cast(regexp_replace(ips.res_id, '\D', '', 'g') AS INT) = rp.id
            LEFT JOIN account_payment_term apts
              ON cast(regexp_replace(ips.value_reference, '\D', '', 'g') AS INT) = apts.id

            LEFT JOIN ir_property ipc
              ON ipc.name = 'property_payment_term' AND cast(regexp_replace(ipc.res_id, '\D', '', 'g') AS INT) = rp.id
            LEFT JOIN account_payment_term aptc
              ON cast(regexp_replace(ipc.value_reference, '\D', '', 'g') AS INT) = aptc.id

            LEFT JOIN ir_property ipl
              ON ipl.name = 'company_credit_limit' AND cast(regexp_replace(ipl.res_id, '\D', '', 'g') AS INT) = rp.id
            INNER JOIN account_journal aj ON av.journal_id = aj.id
            INNER JOIN res_currency rc ON
                                         CASE WHEN aj.currency IS NULL
                                           THEN
                                             rc.id = (SELECT rcom.currency_id
                                                      FROM res_company rcom
                                                      WHERE rcom.id = aj.company_id
                                                      LIMIT 1)
                                         ELSE
                                           rc.id = aj.currency
                                         END
            LEFT JOIN account_move am ON av.move_id = am.id

            );

            CREATE OR REPLACE VIEW voucher_line_receipt_payment AS (
              SELECT *
              FROM voucher_receipt_payment
              --  WHERE doc_type = 'voucher_receipt' --'CHA/0000016'

              UNION ALL

              SELECT
                vrp.doc_id,
                vrp.doc_type,
                vrp.partner_id,
                vrp.partner_name,
                vrp.supplier_days_credit,
                vrp.supplier_limit_credit,
                vrp.customer_days_credit,
                vrp.customer_limit_credit,
                vrp.doc_state,
                vrp.doc_number,
                CASE WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'cr' OR
                          vrp.doc_type = 'voucher_payment' AND avl.type = 'dr'
                  THEN
                    ' \-'
                WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'dr' OR
                     vrp.doc_type = 'voucher_payment' AND avl.type = 'cr'
                  THEN
                    '  \--'
                END ||
                CAST(am.name || ' (' || aml.ref || ')' AS TEXT) doc_origin,

                doc_date_issue +
                CASE WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'cr' OR
                          vrp.doc_type = 'voucher_payment' AND avl.type = 'dr'
                  THEN
                    INTERVAL ' 1 minutes'
                WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'dr' OR
                     vrp.doc_type = 'voucher_payment' AND avl.type = 'cr'
                  THEN
                    INTERVAL ' 2 minutes'
                END AS
                                                                doc_date_issue,
                doc_date_due +
                CASE WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'cr' OR
                          vrp.doc_type = 'voucher_payment' AND avl.type = 'dr'
                  THEN
                    INTERVAL ' 1 minutes'
                WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'dr' OR
                     vrp.doc_type = 'voucher_payment' AND avl.type = 'cr'
                  THEN
                    INTERVAL ' 2 minutes'
                END AS                                          doc_date_due,
                doc_currency,
                avl.amount                                      doc_amount,
                0                                               doc_amount_mxn,
                0                                               doc_amount_usd,

                avl.amount_unreconciled - avl.amount            doc_residue,
                0                                               doc_residue_mxn,
                0                                               doc_residue_usd

              FROM voucher_receipt_payment vrp
                INNER JOIN account_voucher_line avl ON vrp.doc_id = avl.voucher_id AND avl.amount > 0
                INNER JOIN account_move_line aml ON avl.move_line_id = aml.id
                INNER JOIN account_move am ON aml.move_id = am.id
              --WHERE vrp.doc_type = 'voucher_receipt' --'CHA/0000016'
            );

            CREATE OR REPLACE VIEW r_payment_doc_customer AS (
                SELECT
                    row_number()
                    OVER (
                    ORDER BY doc_id ) AS id,
                    *,
                    get_rate('sale', 'USD', CURRENT_TIMESTAMP AT TIME ZONE 'utc') current_rate
                FROM (
                 SELECT
                   doc_id,
                   doc_id_ud,
                   doc_id_ap,
                   0 doc_id_av,
                   doc_type,
                   partner_id,
                   partner_name,
                   customer_days_credit,
                   customer_limit_credit,
                   doc_state,
                   doc_number,
                   doc_origin,
                   doc_date_issue,
                   doc_date_due,
                   doc_currency,
                   doc_amount,
                   0 doc_amount_mxn,
                   0 doc_amount_usd,
                   doc_residue,
                   0 doc_residue_mxn,
                   0 doc_residue_usd
                 FROM r_residue_doc_customer

                 UNION ALL

                 SELECT
                   0      doc_id,
                   0      doc_id_ud,
                   0      doc_id_ap,
                   doc_id doc_ic_av,
                   doc_type,
                   partner_id,
                   partner_name,
                   customer_days_credit,
                   customer_limit_credit,
                   doc_state,
                   doc_number,
                   doc_origin,
                   doc_date_issue,
                   doc_date_due,
                   doc_currency,
                   doc_amount,
                   doc_amount_mxn,
                   doc_amount_usd,
                   doc_residue,
                   doc_residue_mxn,
                   doc_residue_usd
                 FROM
                   voucher_line_receipt_payment
                 WHERE doc_type = 'voucher_receipt'
                ) T3
              ORDER BY doc_id, doc_number, doc_date_issue ASC);
            """
        )
