# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import tools


class VoucherLineReceiptPayment:
    """Documents With Balance report"""

    @staticmethod
    def create_voucher_line_receipt_payment(cr):
        # Drop view
        tools.sql.drop_view_if_exists(cr, 'voucher_line_receipt_payment')
        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW voucher_line_receipt_payment AS (
              SELECT *
              FROM voucher_receipt_payment
              --  WHERE doc_type = 'voucher_receipt' --'CHA/0000016'

              UNION ALL

              SELECT
                vrp.doc_id,
                vrp.doc_type,
                vrp.partner_id,
                vrp.partner_name,
                vrp.supplier_days_credit,
                vrp.supplier_limit_credit,
                vrp.customer_days_credit,
                vrp.customer_limit_credit,
                vrp.doc_state,
                vrp.doc_number,
                CASE WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'cr' OR
                          vrp.doc_type = 'voucher_payment' AND avl.type = 'dr'
                  THEN
                    ' \-'
                WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'dr' OR
                     vrp.doc_type = 'voucher_payment' AND avl.type = 'cr'
                  THEN
                    '  \--'
                END ||
                CAST(am.name || ' (' || aml.ref || ')' AS TEXT) doc_origin,

                doc_date_issue +
                CASE WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'cr' OR
                          vrp.doc_type = 'voucher_payment' AND avl.type = 'dr'
                  THEN
                    INTERVAL ' 1 minutes'
                WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'dr' OR
                     vrp.doc_type = 'voucher_payment' AND avl.type = 'cr'
                  THEN
                    INTERVAL ' 2 minutes'
                END AS
                                                                doc_date_issue,
                doc_date_due +
                CASE WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'cr' OR
                          vrp.doc_type = 'voucher_payment' AND avl.type = 'dr'
                  THEN
                    INTERVAL ' 1 minutes'
                WHEN vrp.doc_type = 'voucher_receipt' AND avl.type = 'dr' OR
                     vrp.doc_type = 'voucher_payment' AND avl.type = 'cr'
                  THEN
                    INTERVAL ' 2 minutes'
                END AS                                          doc_date_due,
                doc_currency,
                avl.amount                                      doc_amount,
                0                                               doc_amount_mxn,
                0                                               doc_amount_usd,

                avl.amount_unreconciled - avl.amount            doc_residue,
                0                                               doc_residue_mxn,
                0                                               doc_residue_usd

              FROM voucher_receipt_payment vrp
                INNER JOIN account_voucher_line avl ON vrp.doc_id = avl.voucher_id AND avl.amount > 0
                INNER JOIN account_move_line aml ON avl.move_line_id = aml.id
                INNER JOIN account_move am ON aml.move_id = am.id
              --WHERE vrp.doc_type = 'voucher_receipt' --'CHA/0000016'
            );
            """
        )
