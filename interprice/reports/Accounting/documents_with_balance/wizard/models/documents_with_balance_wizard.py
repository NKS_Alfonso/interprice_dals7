# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import api, exceptions, fields, models, _, tools
# creator excel file lib


class DocumentsWithBalanceWizard(models.TransientModel):
    """This wizard sample the balance of documents on screen, pdf and csv."""
    _name = 'documents_with_balance_wizard'
    _description = _('Documents With Balance Wizard')

    # field selection options
    by_date_list = [
        ('date issuance', _('Issue')),
        ('date due', _('Expiration'))
    ]

    # Global variables
    _query = ''
    _invoices_ids = []

    # domain methods
    @api.one
    def get_false(self):
        """Method False"""
        pass

    @api.one
    @api.depends('purchase_rate')
    def get_rate(self):
        """Simple method to get purchase rate"""
        rate = self.env['res.currency'].search([('name', '=', 'USD')])
        self.purchase_rate = 1 / rate.rate_silent if rate.rate_silent > 0.0 else 1
        self.sale_rate = 1 / rate.rate_sale_silent if rate.rate_sale_silent > 0.0 else 1

    def get_with_list(self):
        filter_list = []
        if self._context.get('customer', False):
            filter_list = [
                ('balance', _('Balance')),
                ('receive', _('Receive'))
            ]
        elif self._context.get('supplier', False):
            filter_list = [
                ('balance', _('Balance')),
                ('payment', _('Payment'))
            ]
        return filter_list

    # Fields
    purchase_rate = fields.Float(
        compute=get_false,
        string=_("Purchase Rate"),
        help=_("Purchase rate of the day"),
    )
    sale_rate = fields.Float(
        compute=get_rate,
        string=_("Sale Rate"),
        help=_("Sale rate of the day"),
    )
    partner = fields.Many2one(
        required=True,
        comodel_name='res.partner',
        string=_("Partner"),
        help=_("The pertner information."),
    )
    status_partner = fields.Char(
        readonly=True,
        string=_("Status"),
        help=_("Status of partner")
    )
    balance_partner = fields.Float(
        readonly=True,
        string=_("Balance"),
        help=_("Balance of partner")
    )
    with_ = fields.Selection(
        required=True,
        selection=get_with_list,
        string=_("With"),
        help=_("Balance: or Payment:")
    )
    by_date = fields.Selection(
        required=True,
        selection=by_date_list,
        string=_("By date of"),
        help=_("By date of issue (date on which the document is generated) o "
               "Due date (date on which the document credit expires)")
    )
    start_date = fields.Datetime(
        required=True,
        help=_("Initial date to filter"),
        string=_("Initial date")
    )
    end_date = fields.Datetime(
        required=True,
        help=_("Final date to filter"),
        string=_("Final date")
    )

    # onchange methods
    @api.onchange('partner')
    def onchange_partner(self):
        if self.partner:
            self.status_partner = _("Active") if self.partner.active else _("Deactivate")
            if self._context.get('customer', False):
                self.balance_partner = self.partner.credit
            elif self._context.get('supplier', False):
                self.balance_partner = self.partner.debit
        else:
            self.balance_partner = False
            self.status_partner = False

    @api.multi
    def screen(self):
        self._get_ids()
        if self._context.get('customer', False):
            res_model = 'r.residue.doc.customer' if \
                self.with_ == 'balance' else \
                'r.payment.doc.customer'
            name = _('Documents With Balance Customer') if \
                self.with_ == 'balance' else \
                _('Documents With Receive Customer')
        elif self._context.get('supplier', False):
            res_model = 'r.residue.doc.supplier' if \
                self.with_ == 'balance' else \
                'r.payment.doc.supplier'
            name = _('Documents With Balance Supplier') if \
                self.with_ == 'balance' else \
                _('Documents With Payment Supplier')
        # if self.with_ == 'receive':
        #     self.restructure_doc_ids(res_model)
        return {
            'name': name,
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': res_model,
            'target': 'current',
            'context': {},
            'domain': [('id', 'in', self._doc_ids)]
        }

    @api.multi
    def show_pdf(self):
        self._get_ids()
        if self._context.get('customer', False):
            if self.with_ == 'balance':
                model = 'r.residue.doc.customer'
                report = 'reports.r_residue_doc_customer'
            else:
                model = 'r.payment.doc.customer'
                report = 'reports.r_payment_doc_customer'
                # self.restructure_doc_ids(model)
        elif self._context.get('supplier', False):
            if self.with_ == 'balance':
                model = 'r.residue.doc.supplier'
                report = 'reports.r_residue_doc_supplier'
            else:
                model = 'r.payment.doc.supplier'
                report = 'reports.r_payment_doc_supplier'
                # self.restructure_doc_ids(model)

        _ids = self.env[model].search([('id', 'in', self._doc_ids)],
                                      order='doc_date_issue, doc_id, doc_number ASC')

        return self.env['report'].get_action(_ids, report)

    @api.multi
    def show_xls(self):
        """Download the document, XLS"""
        self._get_ids()
        csv_id = self.csv_by_ids()
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    @api.multi
    def restructure_doc_ids(self, model):
        """Restructure the ids of documents, delete those that have no payment applied"""
        model = self.env[model].browse(self._invoices_ids)
        for m in model:
            if m.doc_id.state not in 'cancel':
                if not m.doc_id.payment_ids:
                    if m.id in self._doc_ids:
                        self._doc_ids.remove(m.id)

    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        df = self.start_date
        dt = self.end_date

        if df and dt and df > dt:
            self.start_date = False
            self.end_date = False
            raise exceptions.Warning(
                _("'Date to' must be higher than 'Date from'."))

    def _get_ids(self):
        # Get filter values
        partner = self.partner
        with_ = self.with_
        by_date = self.by_date
        s_date = self.start_date
        e_date = self.end_date

        query = "SELECT DISTINCT rrd.id\n"

        if self._context.get('customer', False):
            query += "FROM r_residue_doc_customer rrd\n" \
                     "\tWHERE rrd.id IS NOT NULL\n" if \
                        with_ == "balance" else \
                     "FROM r_payment_doc_customer rrd\n" \
                     "\tWHERE rrd.id IS NOT NULL\n"
        elif self._context.get('supplier', False):
            query += "FROM r_residue_doc_supplier rrd\n" \
                     "\tWHERE rrd.id IS NOT NULL\n" if \
                        with_ == "balance" else \
                     "FROM r_payment_doc_supplier rrd\n" \
                     "\tWHERE rrd.id IS NOT NULL\n"
        if partner:
            query += "\tAND rrd.partner_id = CAST(%s AS INT)\n" % (partner.id)
        if by_date:
            if by_date == 'date issuance':
                if s_date:
                    query += "\tAND rrd.doc_date_issue >= CAST('%s' AS TIMESTAMP)\n" % (s_date)
                if e_date:
                    query += "\tAND rrd.doc_date_issue <= CAST('%s' AS TIMESTAMP)\n" % (e_date)
            elif by_date == 'date due':
                if s_date:
                    query += "\tAND rrd.doc_date_due >= CAST('%s' AS TIMESTAMP)\n" % (s_date)
                if e_date:
                    query += "\tAND rrd.doc_date_due <= CAST('%s' AS TIMESTAMP)\n" % (e_date)
        else:
            if s_date:
                query += "\tAND rrd.doc_date_issue >= CAST('%s' AS TIMESTAMP)\n" % (s_date)
            if e_date:
                query += "\tAND rrd.doc_date_issue <= CAST('%s' AS TIMESTAMP)\n" % (e_date)

        if self._context.get('customer', False):
            if self.with_ == 'balance':
                query += "\tAND rrd.doc_state in ('open','generated','applied partial','associated','partial_applied')\n"
            else:
                query += "\tAND rrd.doc_state in ('cancel','posted','open','paid','partial_applied','applied partial','canceled')\n"
        elif self._context.get('supplier', False):
            if self.with_ == 'balance':
                query += "\tAND rrd.doc_state in ('open')\n"
            else:
                query += "\tAND rrd.doc_state in ('cancel','posted','open')\n"

        if self.with_ == 'receive' and partner.customer or self.with_ == 'payment' and partner.supplier:
            self._query = query
            self._query += "\tAND rrd.doc_id > 0;\n"
            self.env.cr.execute(self._query)
            if self.env.cr.rowcount:
                self._invoices_ids = [id[0] for id in self.env.cr.fetchall()]

        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            self._doc_ids = [id[0] for id in self.env.cr.fetchall()]
        else:
            raise exceptions.Warning(_("No records found!"))

    def csv_by_ids(self):
        if self._context.get('customer', False):
            if self.with_ == 'balance':
                model = 'r.residue.doc.customer'
            else:
                model = 'r.payment.doc.customer'
                # self.restructure_doc_ids(model)

            customer = [
                _('Customer Days credit'),
                _('Customer Limit credit'),
            ]
            supplier = False
        elif self._context.get('supplier', False):
            if self.with_ == 'balance':
                model = 'r.residue.doc.supplier'

            else:
                model = 'r.payment.doc.supplier'
                # self.restructure_doc_ids(model)

            customer = False
            supplier = [
                _('Supplier Days credit'),
                _('Supplier Limit credit'),
            ]

        data = self.env[model].search([('id', 'in', self._doc_ids)],
                                      order='doc_date_issue, doc_id, doc_number ASC')

        data_csv = [
            [
                _('Type Document'),
                _('Customer') if customer else _('Supplier'),
                ]
        ]
        data_csv[0] += customer or supplier
        data_csv[0] += [
                _('Document Number'),
                _('State'),
                _('Document Origin'),
                _('Date Issue'),
                _('Date Due'),
                _('Currency'),
                _('Amount MXN'),
                _('Amount USD'),
                _('Residual MXN'),
                _('Residual USD'),
                _('Current rate'),
        ]

        for row in data:
            data_csv.append([
                unicode(self._set_default(_(row.doc_type))).encode('utf8'),
                unicode(self._set_default(row.partner_name)).encode('utf8'),
                unicode(self._set_default(_(row.customer_days_credit))).encode('utf8') if customer and not supplier
                else unicode(self._set_default(_(row.supplier_days_credit))).encode('utf8'),
                unicode(self._set_default(_(row.supplier_limit_credit), 0)).encode('utf8') if not customer and supplier
                else unicode(self._set_default(_(row.customer_limit_credit), 0)).encode('utf8'),
                unicode(self._set_default(row.doc_number)).encode('utf8'),
                unicode(self._set_default(_(row.doc_state))).encode('utf8'),
                unicode(self._set_default(row.doc_origin)).encode('utf8'),
                unicode(self._set_default(tools.server_to_local_timestamp(
                    row.doc_date_issue,
                    tools.DEFAULT_SERVER_DATETIME_FORMAT,
                    tools.DEFAULT_SERVER_DATETIME_FORMAT,
                    self._context.get('tz', 'America/Mexico_City')))).encode('utf8'),
                unicode(self._set_default(tools.server_to_local_timestamp(
                    row.doc_date_due,
                    tools.DEFAULT_SERVER_DATETIME_FORMAT,
                    tools.DEFAULT_SERVER_DATETIME_FORMAT,
                    self._context.get('tz', 'America/Mexico_City')))).encode('utf8'),
                unicode(self._set_default(row.doc_currency)).encode('utf8'),
                unicode(self._set_default(round(row.amount_mxn, 4) if self.with_ == 'balance' else row.doc_amount_mxn, 0)).encode('utf8'),
                unicode(self._set_default(round(row.amount_usd, 4) if self.with_ == 'balance' else row.doc_amount_usd, 0)).encode('utf8'),
                unicode(self._set_default(round(row.doc_residue, 4) if self.with_ == 'balance' else row.doc_residue_mxn, 0)).encode('utf8'),
                unicode(self._set_default(round(row.doc_residue_usd, 4), 0)).encode('utf8'),
                unicode(self._set_default(row.current_rate, 0)).encode('utf8')
            ])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'DocumentsWithBalance.csv',
                'type_file': 'csv',
                'file': file,
            })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
