# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, api, fields, models, tools


class AccountBankStatusReportView(models.Model):
    """Account Bank status report by date / bank / currency / document type Wizard"""

    # odoo model properties
    _name = 'account.bank.status.report.view'
    _table = 'account_bank_status_report_view'
    _description = 'Account Bank Status Report'
    _auto = False  # Don't let odoo create table
    _order = 'bank_name, account_number, document_name_ref, folio, folio_ref, register_date'

    # View fieds
    movement_type = fields.Char(
        help=_("Type document name"),
        readonly=True,
        string=_('Movement')
    )
    bank_name = fields.Char(
        help=_("Account bank name"),
        readonly=True,
        string=_("Bank name"),
    )
    account_number = fields.Char(
        help=_("Account number"),
        readonly=True,
        string=_("Account number")
    )
    balance = fields.Float(
        compute='_balance_by_date',
        string=_('Balance'),

    )
    document_name_ref = fields.Char(
        help=_("Document name"),
        readonly=True,
        string=_('Document')
    )
    folio = fields.Char(
        help=_("Document folio"),
        readonly=True,
        string=_('Folio')
    )
    debit = fields.Float(
        help=_("Amount on debit account"),
        readonly=True,
        string=_('Debit')
    )
    credit = fields.Float(
        help=_("Amount on credit account"),
        readonly=True,
        string=_('Credit')
    )
    currency_name = fields.Char(
        help=_("Movement currency name"),
        readonly=True,
        string=_('Currency')
    )
    currency_id = fields.Char(
        string=_('Currency id')
    )
    company_currency = fields.Char(
        string=_('company currency')

    )
    parity = fields.Char(
        help=_("Exchange rate"),
        string=_('Parity')
    )
    document_date = fields.Date(
        help=_("Account date on document"),
        readonly=True,
        string=_("Document date"),
    )
    register_date = fields.Date(
        help=_("Register movement date"),
        readonly=True,
        string=_("Register date"),
    )
    status = fields.Char(
        help=_("Document status"),
        readonly=True,
        string=_('Status')
    )
    observations = fields.Text(
        help=_("Observations on document"),
        readonly=True,
        string=_("Observations")
    )
    folio_ref = fields.Char(
        help=_("Folio to reference"),
        readonly=True,
        string=_('Folio Ref.')
    )

    default_account_id = fields.Char(
        help=_("Account default id"),
        string=_('account default id')
    )
    journal_id = fields.Char(
        help=_("Journal id"),
        string=_('journal id')
    )
    move_id = fields.Char(
        help=_('Move id'),
        string=_('Move id')
    )

    @api.multi
    def _balance_by_date(self):
        data = self.env['account.bank.status.report.view'].search([('id', 'in', self._ids)])

        account_number = ''
        flag = 0
        self.env.cr.execute(""" SELECT start_date, end_date
                                FROM account_bank_status_report_wizard
                                ORDER BY id DESC LIMIT 1""")

        if self.env.cr.rowcount:
            filter_dates = self.env.cr.dictfetchall()[0]
            start_date = filter_dates.get('start_date')
            end_date = filter_dates.get('end_date')

        for record in data:
            flag += 1

            if account_number != record.account_number:
                resta = 0
                flag = 0

                self.env.cr.execute(""" SELECT move_id FROM account_bank_status_report_view
                                        WHERE account_number = '%s' """
                                    % (record.account_number))
                if self.env.cr.rowcount:
                    move_ids = [move_id[0] for move_id in self.env.cr.fetchall()]

            if record.currency_id == record.company_currency:  # MXN
                balance_sum = """ COALESCE(sum(aml.debit)-sum(aml.credit), 0)"""
            elif record.currency_id != record.company_currency:  # USD
                balance_sum = """ COALESCE(sum(aml.amount_currency), 0) """
            if len(move_ids) == 1:
                move_ids.append(0)

            self.env.cr.execute(""" SELECT %s
                                        FROM account_move_line aml
                                        WHERE aml.account_id=%s
                                        AND aml.date >= '%s'
                                        AND aml.date <= '%s'
                                        AND aml.journal_id<>%s
                                        AND aml.move_id NOT IN %s"""
                                % (balance_sum, record.default_account_id, start_date,
                                   end_date, record.journal_id, tuple(move_ids)))
            if self.env.cr.rowcount:
                if flag == 0:
                    account_balance = self.env.cr.fetchone()[0]
                else:
                    account_balance = 0

            if record.currency_id == record.company_currency:  # MXN
                resta += account_balance + (record.debit - record.credit)
                record.balance = resta
                account_number = record.account_number
            else:  # USD
                resta += account_balance + (record.debit - (record.credit * -1))
                record.balance = resta
                account_number = record.account_number

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'account_bank_status_report_view')
        tools.sql.drop_view_if_exists(cr, 'account_bank_currency_id_filter')
        tools.sql.drop_view_if_exists(cr, 'account_bank_amount_usd')

        # Create views
        cr.execute(
            """
                CREATE OR REPLACE view account_bank_amount_usd as (
                SELECT
                    T1.move_id,
                    T1.line_id,
                    T1.move_ref,
                    T1.line_debit,
                    T1.line_credit,
                    T1.line_divisa,
                    T1.line_currency_id,
                    T1.line_account_id,
                    T1.line_journal_id,
                    T1.move_date,
                    T1.cash_back_id
                    FROM
                    (
                        SELECT
                            am.id move_id,
                            am.name move_name,
                            am.ref move_ref,
                            am.journal_id move_journal,
                            am.state move_state,
                            am.date move_date,
                            am.cash_back cash_back_id,
                            aml.id line_id,
                            COALESCE(aml.debit,0) line_debit,
                            COALESCE(aml.credit,0) line_credit,
                            COALESCE(aml.amount_currency,0) line_divisa,
                            aml.currency_id line_currency_id,
                            aml.account_id line_account_id,
                            aml.journal_id line_journal_id
                        FROM account_move am
                        INNER JOIN account_move_line aml ON aml.move_id=am.id
                        --WHERE am.state='posted'
                ) AS T1
            )
            """
        )

        cr.execute(
            """
                CREATE OR REPLACE view account_bank_currency_id_filter as (
                    SELECT
                        rpb.id,
                        CASE WHEN rpb.currency2_id IS NULL
                            THEN resc.currency_id
                            ELSE rpb.currency2_id
                            END currency_
                    FROM res_partner_bank rpb
                    LEFT JOIN res_currency rc
                    ON rpb.currency2_id=rc.id
                    JOIN res_company resc
                    ON rpb.company_id=resc.id
                    )
            """
        )

        cr.execute(
            """
            CREATE OR REPLACE VIEW account_bank_status_report_view as
            (
                SELECT  row_number() over (order by T1.account_bank_id) AS id,
                    T1.account_bank_id,
                    T1.movement_type,
                    T1.bank_name,
                    T1.account_number,
                    T1.document_name_ref,
                    T1.folio,
                    CASE WHEN T1.currency_id <> T1.company_currency AND T1.debit > 0 THEN T1.divisa
                        WHEN T1.currency_id = T1.company_currency AND T1.debit > 0 THEN T1.debit
                        ELSE 0 END debit,
                    CASE WHEN T1.currency_id <> T1.company_currency AND T1.credit > 0 THEN T1.divisa
                        WHEN T1.currency_id = T1.company_currency AND T1.credit > 0 THEN T1.credit
                        ELSE 0 END credit,
                    T1.balance,
                    T1.currency_id,
                    T1.currency_name,
                    T1.company_currency,
                    T1.parity,
                    T1.document_date,
                    T1.register_date,
                    T1.status,
                    T1.folio_ref,
                    T1.observations,
                    T1.default_account_id,
                    T1.journal_id,
                    T1.move_id
                FROM
                (
                    SELECT
                        inegre.cuenta_bancaria account_bank_id,
                        CAST('ingresos.egresos' AS TEXT) as movement_type,
                        am.date register_date,
                        inegre.fecha document_date,
                        inegre.folio_interno folio,
                        CASE WHEN inegre.state = 'generado' THEN 'Generado'
                                ELSE 'Cancelado'
                                END status,
                        inegre.observacion observations,
                        inegre.tipo_cambio parity,
                        'Movimientos Bancarios' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        CASE WHEN am.ref LIKE '%Mov. Cancelado'
                            THEN (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                                    WHERE move_id=inegre.cancelado AND line_account_id = aj.default_debit_account_id)
                            ELSE (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                                    WHERE move_id=inegre.movement AND line_account_id = aj.default_debit_account_id)
                            END debit,
                        CASE WHEN am.ref LIKE '%Mov. Cancelado'
                            THEN (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                                    WHERE move_id=inegre.cancelado AND line_account_id = aj.default_debit_account_id)
                            ELSE (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                                    WHERE move_id=inegre.movement AND line_account_id = aj.default_debit_account_id)
                            END credit,
                        CASE WHEN am.ref LIKE '%Mov. Cancelado'
                            THEN (SELECT COALESCE(line_divisa,0) FROM account_bank_amount_usd
                                    WHERE move_id=inegre.cancelado AND line_account_id = aj.default_debit_account_id)
                            ELSE (SELECT COALESCE(line_divisa,0) FROM account_bank_amount_usd
                                    WHERE move_id=inegre.movement AND line_account_id = aj.default_debit_account_id)
                            END divisa,

                        0 balance,
                        rc.id currency_id,
                        rc.name currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency

                    FROM documentos_bancarios docban
                    JOIN ingresos_egresos inegre
                    ON inegre.doc_bancario = docban.id
                    JOIN res_partner_bank rpb
                    ON inegre.cuenta_bancaria = rpb.id
                    JOIN res_currency rc
                    ON rc.id = inegre.tipo_moneda
                    JOIN account_journal aj
                    ON aj.id = rpb.journal_id
                    JOIN res_company rescom
                    ON rescom.id = aj.company_id
                    JOIN res_currency rescur
                    ON rescur.id = rescom.currency_id
                    JOIN account_move am
                    ON am.id = inegre.movement or am.id = inegre.cancelado
                    WHERE inegre.state IN ('generado','cancelado') AND am.state='posted'

            UNION ALL

                    SELECT
                        cb.bank account_bank_id,
                        'cash.back' as movement_type,
                        am.date register_date,
                        cb.date document_date,
                        cb.folio folio,
                        CASE WHEN cb.status = 'generated' THEN 'Generado'
                            WHEN cb.status = 'canceled' THEN 'Cancelado'
                            END status,
                        cb.observations observations,
                        (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                        WHERE name AT TIME ZONE 'utc' <= am.date
                                        ORDER BY name DESC LIMIT 1) parity,
                        'Devolucion de Efectivo' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        CASE WHEN am.ref LIKE 'Cash back canceled%'
                            THEN (SELECT COALESCE(line_debit, 0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id)
                            ELSE (SELECT COALESCE(line_debit, 0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id)
                            END debit,
                        CASE WHEN am.ref LIKE 'Cash back canceled%'
                            THEN (SELECT COALESCE(line_credit, 0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id)
                            ELSE (SELECT COALESCE(line_credit, 0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id)
                            END credit,
                        CASE WHEN am.ref LIKE 'Cash back canceled%'
                            THEN (SELECT COALESCE(line_divisa, 0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id)
                            ELSE (SELECT COALESCE(line_divisa, 0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id)
                            END divisa,
                        0 balance,
                        rc.id currency_id,
                        rc.name currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM cash_back cb
                    JOIN account_move am
                    ON am.cash_back = cb.id
                    JOIN res_partner_bank rpb
                    ON rpb.id = cb.bank
                    JOIN account_journal aj
                    ON aj.id = am.journal_id
                    JOIN res_currency rc
                    ON rc.id = cb.currency
                    JOIN res_company rescom
                    ON rescom.id = aj.company_id
                    JOIN res_currency rescur
                    ON rescur.id = rescom.currency_id
                    WHERE cb.status IN ('generated','canceled')

                UNION ALL

                    SELECT
                        dni.bank_id account_bank_id,
                        'dni.dni' as movement_type,
                        dni.registration_date register_date,
                        dni.deposit_date document_date,
                        dni.name folio,
                        dni.state status,
                        dni.note_cancel observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= dni.registration_date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        'Deposito no Identificado' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        aml.debit debit,
                        aml.credit credit,
                        aml.amount_currency divisa,
                        0 balance,
                        rc.id currency_id,
                        rc.name currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM  dni_dni dni
                    JOIN account_move am ON am.id=dni.policy_create_id or am.id=dni.policy_cancel_id
                    JOIN account_move_line aml ON aml.move_id=am.id
                    JOIN res_partner_bank rpb ON rpb.id = dni.bank_id
                    JOIN account_journal aj ON aj.id = rpb.journal_id
                    JOIN res_currency rc ON rc.id = dni.currency_id
                    JOIN res_company rescom ON rescom.id = aj.company_id
                    JOIN res_currency rescur ON rescur.id = rescom.currency_id
                    WHERE dni.state IN ('generated','associated','partial_applied','applied','cancel')
                    AND aj.default_debit_account_id=aml.account_id

                UNION ALL

                    SELECT
                        ap.account_bank account_bank_id,
                        'advance.payment' as movement_type,
                        ap.register_date register_date,
                        ap.register_date document_date,
                        ap.number folio,
                        CASE WHEN ap.state = 'generated' THEN 'Generado'
                            WHEN ap.state = 'applied_partial' THEN 'Aplicado parcial'
                            WHEN ap.state = 'applied' THEN 'Aplicado'
                            WHEN ap.state = 'canceled' THEN 'Cancelado'
                            END status,
                        ap.observations observations,
                        ap.rate parity,
                        'Anticipo' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        (SELECT COALESCE(line_debit, 0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id) debit,
                        (SELECT COALESCE(line_credit, 0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id) credit,
                        (SELECT COALESCE(line_divisa, 0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id) divisa,
                        0 balance,
                        rc.id currency_id,
                        rc.name currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM advance_payment ap
                    join res_partner_bank rpb ON rpb.id = ap.account_bank
                    JOIN account_move am ON am.advance_payment = ap.id
                    JOIN account_journal aj ON aj.id = rpb.journal_id
                    JOIN res_currency rc ON rc.id = ap.currency_id
                    JOIN res_company rescom ON rescom.id = aj.company_id
                    JOIN res_currency rescur ON rescur.id = rescom.currency_id
                    WHERE ap.state IN ('generated','applied_partial','applied','canceled')

                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        'inter_pagos.pago' movement_type,
                        am.date register_date,
                        am.date document_date,
                        ip.folio folio,
                        CASE WHEN ip.process_state = 'completed' THEN 'Completado'
                            WHEN ip.process_state = 'canceled' THEN 'Cancelado'
                            END status,
                        ip.comments observations,
                        ip.exchange_rate parity,
                        'Transferencias bancarias' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        CASE WHEN am.ref LIKE '%Transferencia cancelada%'
                            THEN (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                                    WHERE move_id=ip.canceled AND line_account_id = aj.default_debit_account_id)
                            ELSE (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                                    WHERE move_id=ip.movement AND line_account_id = aj.default_debit_account_id)
                            END debit,
                        CASE WHEN am.ref LIKE '%Transferencia cancelada%'
                            THEN (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                                WHERE move_id=ip.canceled AND line_account_id = aj.default_debit_account_id)
                            ELSE (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                                    WHERE move_id=ip.movement AND line_account_id = aj.default_debit_account_id)
                            END credit,
                        CASE WHEN am.ref LIKE '%Transferencia cancelada%'
                            THEN (SELECT COALESCE(line_divisa,0) FROM account_bank_amount_usd
                                    WHERE move_id=ip.canceled AND line_account_id = aj.default_debit_account_id)
                            ELSE (SELECT COALESCE(line_divisa,0) FROM account_bank_amount_usd
                                    WHERE move_id=ip.movement AND line_account_id = aj.default_debit_account_id)
                            END divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM res_partner_bank rpb
                    JOIN account_journal aj on aj.id=rpb.journal_id
                    LEFT JOIN res_currency rc on rc.id=aj.currency
                    JOIN res_company rescom on rescom.id=aj.company_id
                    JOIN res_currency rescur on rescur.id=rescom.currency_id
                    JOIN inter_pagos_pago ip ON ip.journal_origin=rpb.journal_id
                        OR ip.journal_destiny=rpb.journal_id
                    JOIN account_move am ON am.id=ip.movement OR am.id=ip.canceled
                    WHERE ip.process_state IN ('completed','canceled')

                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        'account.owns_checks' movement_type,
                        ack.issue_date register_date,
                        am.date document_date,
                        CAST (ack.number AS VARCHAR) folio,
                        ack.state status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        'Cheques propios' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        aml.debit debit,
                        aml.credit credit,
                        aml.amount_currency divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM account_check ack
                    JOIN account_move am ON am.id=ack.debit_account_move_id
                    JOIN account_move_line aml ON aml.move_id=am.id
                    JOIN account_journal aj ON aj.id=ack.journal_id
                    JOIN res_partner_bank rpb ON rpb.journal_id=aj.id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    WHERE ack.type='issue_check' AND ack.state='debited'
                    AND aj.default_debit_account_id=aml.account_id

                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        'account.owns_checks' movement_type,
                        ack.issue_date register_date,
                        am.date document_date,
                        CAST (ack.number AS VARCHAR) folio,
                        ack.state status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        'Cheques propios' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        aml.debit debit,
                        aml.credit credit,
                        aml.amount_currency divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM account_check ack
                    JOIN account_move am ON am.id=ack.debit_account_move_id
                    JOIN account_move_line aml ON aml.move_id=am.id
                    JOIN account_journal aj ON aj.id=am.journal_id
                    JOIN res_partner_bank rpb ON rpb.journal_id=aj.id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    WHERE ack.type='issue_check' AND ack.state='debited'
                    AND aj.default_debit_account_id=aml.account_id
                    AND am.journal_id<>ack.journal_id

                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        'account.thirdss_checks' movement_type,
                        ack.issue_date register_date,
                        am.date document_date,
                        CAST (ack.number AS VARCHAR) folio,
                        CASE WHEN ack.state = 'deposited' THEN 'Depositado'
                            WHEN ack.state = 'debited' THEN 'Debitado'
                            WHEN ack.state = 'cancel' THEN 'Cancelado'
                            END status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        'Cheques terceros' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        CASE WHEN aj.id <> ack.journal_id
                            THEN (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id)
                            ELSE 0 END debit,
                        CASE WHEN aj.id <> ack.journal_id
                            THEN (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id)
                            ELSE 0 END credit,
                        CASE WHEN aj.id <> ack.journal_id
                            THEN (SELECT COALESCE(line_divisa,0) FROM account_bank_amount_usd
                                    WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id)
                            ELSE 0 END divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM account_check ack
                    JOIN account_move am ON am.id=ack.deposit_account_move_id
                    JOIN res_partner_bank rpb ON rpb.journal_id=am.journal_id
                    JOIN account_journal aj ON aj.id=am.journal_id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    WHERE ack.type='third_check' AND ack.state='deposited'
                    AND ack.voucher_id IS NOT NULL
            
                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        'account.account_bank_transfer' as movement_type,
                        am.date register_date,
                        am.date document_date,
                        aabt.ref folio,
                        CASE WHEN aabt.state = 'apply' THEN 'Aplicado' END status,
                        aabt.description observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        'Tranferencia de cuentas' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        (SELECT COALESCE(line_debit, 0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id) debit,
                        (SELECT COALESCE(line_credit, 0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id) credit,
                        (SELECT COALESCE(line_divisa, 0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_credit_account_id) divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM account_account_bank_transfer aabt
                    JOIN account_move am ON am.id = aabt.account_move_id
                    JOIN account_journal aj ON aj.id = aabt.account_journal_id
                    JOIN res_partner_bank rpb ON rpb.journal_id = aj.id
                    LEFT JOIN res_currency rc ON rc.id = aj.currency
                    JOIN res_company rescom ON rescom.id = aj.company_id
                    JOIN res_currency rescur ON rescur.id = rescom.currency_id
                    WHERE aabt.state IN ('apply')

                UNION ALL

                    SELECT
                        df.bank account_bank_id,
                        'deposit.file' movement_type,
                        df.date register_date,
                        am.date document_date,
                        df.folio folio,
                        CASE WHEN df.state = 'accepted' THEN 'Aceptado'
                            WHEN df.state = 'partial_cancelled' THEN 'Cancelacion Parcial'
                            WHEN df.state = 'cancelled' THEN 'Cancelado'
                            END status,
                        df.observations observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        'Fichas de deposito' document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id)debit,
                        (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id) credit,
                        (SELECT COALESCE(line_divisa,0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id) divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM deposit_file df
                    JOIN res_partner_bank rpb ON rpb.id=df.bank
                    JOIN account_move am ON am.deposit_file=df.id
                    JOIN account_journal aj ON aj.id=am.journal_id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    WHERE df.state IN ('accepted','partial_cancelled','cancelled')

                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        CASE WHEN av.type = 'receipt' THEN 'account.voucher_receipt'
                            WHEN av.type = 'payment' THEN 'account.voucher_payment'
                            END movement_type,
                        av.date register_date,
                        am.date document_date,
                        av.number folio,
                        CASE WHEN av.state = 'posted' THEN 'Posteado' END status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        CASE WHEN av.type = 'receipt' THEN 'Pago cliente'
                            WHEN av.type = 'payment' THEN 'Pago proveedor'
                            END document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        (SELECT COALESCE(line_debit,0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id) debit,
                        (SELECT COALESCE(line_credit,0) FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id) credit,
                        (SELECT COALESCE(line_divisa ,0)FROM account_bank_amount_usd
                            WHERE move_id=am.id AND line_account_id = aj.default_debit_account_id) divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    FROM account_voucher av
                    JOIN account_move am ON am.id=av.move_id
                    JOIN account_journal aj ON aj.id=av.journal_id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    JOIN res_partner_bank rpb ON rpb.journal_id=aj.id
                    WHERE av.state='posted'
                    --AND av.type IN ('receipt','payment')

                UNION ALL

                    SELECT
                        rpb.id account_bank_id,
                        CASE WHEN av.type = 'receipt' THEN 'account.voucher_receipt'
                            WHEN av.type = 'payment' THEN 'account.voucher_payment'
                            END movement_type,
                        av.date register_date,
                        am.date document_date,
                        am.name folio,
                        av.state status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                WHERE name AT TIME ZONE 'utc' <= am.date
                                ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        CASE WHEN av.type = 'receipt' THEN 'Pago cliente'
                            WHEN av.type = 'payment' THEN 'Pago proveedor'
                            END document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        aml.debit debit,
                        aml.credit credit,
                        aml.amount_currency divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                  from account_voucher av
                  join account_journal aj on aj.id=av.journal_id
                  LEFT JOIN res_currency rc ON rc.id=aj.currency
                  JOIN res_company rescom ON rescom.id=aj.company_id
                  JOIN res_currency rescur ON rescur.id=rescom.currency_id
                  join res_partner_bank rpb on rpb.journal_id=aj.id
                  join account_move am on am.name=av.number
                  join account_move_line aml on aml.move_id=am.id
                  where av.state='cancel'
                  and aml.account_id=aj.default_debit_account_id

                UNION ALL

                    select
                        rpb.id account_bank_id,
                        CASE WHEN rp.customer = True THEN 'account.voucher_receipt'
                            WHEN rp.supplier = True THEN 'account.voucher_payment'
                            END movement_type,
                        am.date register_date,
                        am.date document_date,
                        am.name folio,
                        am.state status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                            THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                WHERE name AT TIME ZONE 'utc' <= am.date
                                ORDER BY name DESC LIMIT 1)
                            ELSE 1 END parity,
                        CASE WHEN rp.customer = True THEN 'Pago cliente'
                            WHEN rp.supplier = True THEN 'Pago proveedor'
                            END document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        aml.debit debit,
                        aml.credit credit,
                        aml.amount_currency divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    from account_move am
                    join account_move_line aml on aml.move_id=am.id
                    join res_partner rp on rp.id=aml.partner_id
                    join account_journal aj on aj.id=am.journal_id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    join res_partner_bank rpb on rpb.journal_id=aj.id
                    where aml.account_id=aj.default_debit_account_id
                    and am.ref like 'Pago Cancelado %'

                UNION ALL

                    select
                        rpb.id account_bank_id,
                        CASE WHEN rp.customer = True THEN 'account.voucher_receipt'
                             WHEN rp.supplier = True THEN 'account.voucher_payment'
                                END movement_type,
                        am.date register_date,
                        am.date document_date,
                        am.name folio,
                        am.state status,
                        '' observations,
                        CASE WHEN rc.id <> rescom.currency_id
                                THEN (SELECT ROUND(1.0/rate,4) FROM res_currency_rate
                                    WHERE name AT TIME ZONE 'utc' <= am.date
                                    ORDER BY name DESC LIMIT 1)
                                ELSE 1 END parity,
                        CASE WHEN rp.customer = True THEN 'Pago cliente'
                               WHEN rp.supplier = True THEN 'Pago proveedor'
                                END document_name_ref,
                        am.id move_id,
                        am.ref folio_ref,
                        aml.debit debit,
                        aml.credit credit,
                        aml.amount_currency divisa,
                        0 balance,
                        CASE WHEN rc.id IS NULL THEN rescur.id ELSE rc.id END currency_id,
                        CASE WHEN rc.name IS NULL THEN rescur.name ELSE rc.name END currency_name,
                        rpb.id account_id,
                        rpb.bank_name bank_name,
                        rpb.acc_number account_number,
                        aj.id journal_id,
                        aj.default_debit_account_id default_account_id,
                        rescom.currency_id company_currency
                    from account_move am
                    join account_move_line aml on aml.move_id=am.id
                    left join res_partner rp on rp.id=aml.partner_id
                    join account_journal aj on aj.id=am.journal_id
                    LEFT JOIN res_currency rc ON rc.id=aj.currency
                    JOIN res_company rescom ON rescom.id=aj.company_id
                    JOIN res_currency rescur ON rescur.id=rescom.currency_id
                    join res_partner_bank rpb on rpb.journal_id=aj.id
                    where aml.account_id=aj.default_debit_account_id
                    and am.ref like '% Pago cancelado %'

                ) AS T1
            )
            """
        )
