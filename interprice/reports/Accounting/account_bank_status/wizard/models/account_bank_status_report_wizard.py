# -*- coding: utf-8 -*-
# Copyright © 2017 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import _, api, exceptions, fields, models
from openerp.osv import osv, fields as fieldsv7


class AccountBankStatusReportWizard(models.TransientModel):
    """This wizard is meant to filter the results for the
    account status reports.
    """
    _name = 'account_bank_status_report_wizard'

    # domain methods
    @api.model
    def account_bank_domain(self):
        """
        Returns all account bank ids where its journals are used on movements.
        """
        bank_ids = []
        self.env.cr.execute(
            """SELECT rpb.id
                FROM res_partner_bank rpb
                JOIN account_journal aj
                ON aj.id = rpb.journal_id
                WHERE aj.type = 'bank';
            """
        )
        if self.env.cr.rowcount:
            bank_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', bank_ids)]

    # Fields
    account_bank = fields.Many2one(
        comodel_name='res.partner.bank',
        domain=account_bank_domain,
        help=_("Account number of bank"),
        string=_("Account bank")
    )
    document_type_selection = fields.Selection(
        selection=[
            ('ingresos.egresos', _('Bank movement')),
            ('deposit.file', _('Deposit file')),
            ('advance.payment', _('Advance payment')),
            ('dni.dni', _('Unindentified deposit')),
            ('cash.back', _('Cash back')),
            ('inter_pagos.pago', _('Bank transfer')),
            ('account.owns_checks', _("Own's check")),
            ('account.thirdss_checks', _("Thirds's checks")),
            ('account.account_bank_transfer', _('Account bank transfer')),
            ('account.voucher_receipt', _('Client payment')),
            ('account.voucher_payment', _('Supplier payment'))],
        string=_('Document type'),
    )
    start_date = fields.Date(
        required=True,
        help=_("Initial date to filter deposit files"),
        string=_("Initial date")
    )
    end_date = fields.Date(
        required=True,
        help=_("Final date to filter deposit files"),
        string=_("Final date")
    )
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        help=_('Account bank currency'),
        string=_('Currency')
    )

    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        df = self.start_date
        dt = self.end_date

        if df and dt and df > dt:
            self.start_date = False
            self.end_date = False
            raise exceptions.Warning(
                _("'End date' must be higher than 'Initial date'."))

    @api.onchange('currency_id')
    def onchange_currency(self):
        res = {}
        if self.currency_id.id:
            self._cr.execute(""" SELECT id
                                    FROM account_bank_currency_id_filter
                                    WHERE currency_=%s """,
                             (self.currency_id.id,))

            if self._cr.rowcount:
                account_bank_list_ids = self._cr.dictfetchall()
                account_bank_ids = [id['id'] for id in account_bank_list_ids]
                domain_account_bank_id = [('id', 'in', account_bank_ids)]

                res['domain'] = {
                    'account_bank': domain_account_bank_id
                }
            else:
                raise exceptions.Warning(
                        _("Dont exist bank related to this currency")
                    )
        return res

    def _get_ids(self):
        # Get filter values
        currency_id = self.currency_id
        s_date = self.start_date
        e_date = self.end_date
        account_bank_id = self.account_bank
        document_type = self.document_type_selection

        query = """SELECT id  
                FROM account_bank_status_report_view
                WHERE document_date >= '%s' AND  
                document_date <= '%s' \n
                """ % (s_date, e_date)

        if currency_id:
            query += "\tAND currency_id = %s \n" % (currency_id.id)
        if account_bank_id:
            query += "\tAND account_bank_id = %s \n" % (account_bank_id.id)
        if document_type:
            query += "\tAND movement_type = '%s' \n" % (document_type)

        query += "\tORDER BY bank_name, account_number, document_name_ref, folio, folio_ref, register_date ASC"

        self.env.cr.execute(query)

        if self.env.cr.rowcount:
            ids = [id[0] for id in self.env.cr.fetchall()]
        else:
            raise exceptions.Warning(_("No records found!"))

        return ids

    @api.multi
    def screen(self):
        ids = self._get_ids()
        return {
            'name': _('Account bank status report'),
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree',
            'res_model': 'account.bank.status.report.view',
            'target': 'current',
            'context': '{"start_date":"%s", "end_date":"%s"}' % (self.start_date,self.end_date),
            'domain': [('id', 'in', ids)]
        }

    @api.multi
    def show_xls(self):
        ids = self._get_ids()
        context = {'start_date': self.start_date, 'end_date': self.end_date}
        csv_id = self.csv_by_ids(ids, context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
            'context': '{"start_date":"%s", "end_date":"%s"}' % (self.start_date, self.end_date),
        }

    def csv_by_ids(self, _ids, _context):
        data = self.env['account.bank.status.report.view'].with_context(start_date=_context.get('start_date'),
                                                                        end_date=_context.get('end_date')).browse(_ids)

        data_csv = [
            [
                _('Bank name'),
                _('Account number'),
                _('Document'),
                _('Folio'),
                _('Folio Ref.'),
                _('Debit'),
                _('Credit'),
                _('Balance'),
                _('Currency'),
                _('Document date'),
                _('Register date'),
                _('Parity'),
                _('Status'),
                _('Observations')
            ]
        ]

        account_number = ''
        debit = 0
        credit = 0
        flag = 0
        for row in data:
            flag += 1
            if account_number != row.account_number:
                if flag != 1:
                   data_csv.append(['', '', '', '', _('Cut:'), debit, credit])
                debit = 0
                credit = 0
            debit += row.debit
            credit += row.credit
            account_number = row.account_number

            data_csv.append([
                unicode(self._set_default(row.bank_name)).encode('utf8'),
                unicode(self._set_default(row.account_number)).encode('utf8'),
                unicode(self._set_default(row.document_name_ref)).encode('utf8'),
                unicode(self._set_default(row.folio)).encode('utf8'),
                unicode(self._set_default(row.folio_ref)).encode('utf8'),
                unicode(self._set_default(row.debit)).encode('utf8'),
                unicode(self._set_default(row.credit)).encode('utf8'),
                unicode(self._set_default(row.balance)).encode('utf8'),
                unicode(self._set_default(row.currency_name)).encode('utf8'),
                unicode(self._set_default(row.document_date)).encode('utf8'),
                unicode(self._set_default(row.register_date)).encode('utf8'),
                unicode(self._set_default(row.parity)).encode('utf8'),
                unicode(self._set_default(row.status)).encode('utf8'),
                unicode(self._set_default(row.observations)).encode('utf8')
            ])
            if flag == len(data):
                data_csv.append(['', '', '', '', _('Cut:'), debit, credit])

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'AccountBankStatusReport.csv',
                'type_file': 'csv',
                'file': file,
                })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default

    @api.multi
    def show_pdf(self):
        return self.env['report'].get_action(
            self.env['account.bank.status.report.view'].browse(self._get_ids()),
            'reports.account_bank_status_report_pdf'
        )