# -*- coding: utf-8 -*-
#                            Odoo Module                                   #
#    Copyright (C) 2016  jose.bautista@gvadeto.com                         #
#                                                                          #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #

import os
from openerp import models


class ReportAccountingGainsAndLosses(models.Model):
    _inherit = 'accounting.report'

    def check_report_csv(self, cr, uid, ids, context=None):
        path_dir = os.path.dirname(os.path.abspath(__file__))
        file_py = path_dir + '/scrapy_af.py'
        context['csv_type_report'] = 'lost_again'
        context['csv_script_py'] = file_py
        return self.check_report(cr, uid, ids, context)


class ReportReport(models.Model):
    _inherit = 'report'

    def html2csv(self, location_script, location_html):
        os.system("python %s %s" % (location_script, location_html))