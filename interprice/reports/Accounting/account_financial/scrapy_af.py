# -*- coding: utf-8 -*-
import scrapy
from scrapy.crawler import CrawlerProcess
from types import NoneType
import sys

class AF2CSV(scrapy.Spider):
    name = 'PG'

    def parse(self, response):
        file_name = response.request.headers.get('User-Agent')
        file = ""
        _encode = "utf8"
        # self.___()
        title = response.xpath('//body/div/h2/text()').extract_first()
        # print title
        file = file + unicode(title + "\n").encode(_encode)
        # self.___()
        headers = response.xpath('//body/div/div/div')
        for header in headers:
            # print  header
            _filter = header.xpath('strong/text()').extract_first()
            for p in header.xpath('p'):
                h = ''
                if len(p.xpath('text()').extract()) > 1:
                    k = 0
                    p_list = p.xpath('text()').extract()
                    span_list = p.xpath('span/text()').extract()
                    num_text_p = 0
                    for p in p_list: num_text_p += 1 if len(p.strip()) > 0 else 0
                    if num_text_p > 0:
                        for p in p_list:
                            if len(p.strip()) > 0:
                                h = h + p.strip() + '' + span_list[k].strip() + "--"
                                k += 1
                    else:
                        for s in span_list:
                            h += s.strip()if len(s.strip()) > 0 else ''
                else:
                    _text = p.xpath('text()').extract_first()
                    if _text and len(_text.strip()) > 0:
                        h = p.xpath('text()').extract_first()
            # file.append([_filter, h])
            file = file + unicode(_filter + "," + h + "\n").encode(_encode)
        # self.___()
        table = response.xpath('//body/div/table')
        for row in table:
            if row.xpath('thead'):
                f = ''
                for col in row.xpath('thead/tr/th'):
                    val = col.xpath('text()').extract_first()
                    if type(val) == NoneType:
                        val = col.xpath('span/text()').extract_first()
                    f = f + val.strip() + ','
                file = file + str(f) + '\n'
            if row.xpath('tbody'):
                for tb in row.xpath('tbody'):
                    for tr in tb.xpath('tr'):
                        r = ''
                        for td in tr.xpath('td'):
                            for value in td.xpath('span/text()').extract():
                                _val = value
                                if '$' in value:
                                    _val = ''.join([i if ord(i) < 128 else ' ' for i in _val])
                                    # print _val.decode('ascii', 'ignore')
                                    # break
                                    _val = ','+_val.strip().replace(',', '').replace(' ', '')
                                else:
                                    _val = _val.strip().replace(',', ' ')
                                r = r + _val
                        # print r
                        file = file + unicode(r+ "\n").encode(_encode)
        # self.___(s='//\\', n=20)
        # self.___(s='//\\', n=20)
        # print file
        with open(file_name,  "a") as f:
            f.write(file)
            f.close()

    def ___(self, s='-', n=100):
        print (s * n)


if len(sys.argv) > 1:
    try:
        file_name = sys.argv[1]
        start_urls = [
            'file://127.0.0.1' + file_name
        ]
        file_csv = file_name.rsplit('.', 1)[0]
        file_csv = file_csv + '.csv'
        process = CrawlerProcess({'USER_AGENT': file_csv})
        process.crawl(AF2CSV, start_urls=start_urls)
        process.start()
    except Exception, e:
        print e.args
else:
    sys.exit()
