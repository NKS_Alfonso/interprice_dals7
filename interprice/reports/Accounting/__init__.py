# -*- coding: utf-8 -*-
#                            Odoo Module                                   #
#    Copyright (C) 2016  jose.bautista@gvadeto.com                         #
#                                                                          #
#    This program is free software: you can redistribute it and/or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation, either version 3 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>. #

import account_financial
import residue_of_supplier
import residue_of_customer
import payment_reception
import balance_of_bank
import documents_with_balance
import account_bank_status
import invoices_by_date
import invoices_by_product

