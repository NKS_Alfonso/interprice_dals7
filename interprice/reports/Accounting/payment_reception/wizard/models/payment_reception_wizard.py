# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
# local application/library specific imports
from openerp import _, api, exceptions, fields, models, tools


class PaymentReceptionWizard(models.TransientModel):
    """This wizard is meant to filter the results for the
    Payment reception report (Show info about client payments).
    """
    _name = 'payment_reception_wizard'

    # domain methods
    @api.model
    def journal_domain(self):
        journal_ids = []
        self.env.cr.execute(
            """
            SELECT DISTINCT journal_id AS id
            FROM payment_reception_journal_view;
            """
        )
        if self.env.cr.rowcount:
            journal_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', journal_ids)]

    @api.model
    def user_domain(self):
        user_ids = [av.write_uid.id for av in self.env[
            'account.voucher'].search([('write_uid', '!=', None)])]
        return [('id', 'in', user_ids)]

    @api.model
    def document_domain(self):
        piramide_ids = []
        self.env.cr.execute(
            """
            SELECT DISTINCT piramide_de_pago_id
            FROM payment_reception_journal_view;
            """
        )
        if self.env.cr.rowcount:
            piramide_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', piramide_ids)]

    @api.model
    def cost_center_domain(self):
        cost_center_ids = []
        self.env.cr.execute(
            """
            SELECT DISTINCT account_cost_center_id
            FROM payment_reception_journal_view;
            """
        )
        if self.env.cr.rowcount:
            cost_center_ids = [j[0] for j in self.env.cr.fetchall()]

        return [('id', 'in', cost_center_ids)]

    # Fields
    start_date = fields.Date(
        string=_("Initial date"),
        help=_("Initial date to filter payments")
    )
    end_date = fields.Date(
        string=_("Final date"),
        help=_("Final date to filter payments")
    )

    journal = fields.Many2one(
        string=_("Journal"),
        help=_("Payment pyramid journals used on payments"),
        comodel_name='account.journal',
        domain=journal_domain
    )
    user = fields.Many2one(
        string=_("User"),
        help=_("Last payment modification user"),
        comodel_name='res.users',
        domain=user_domain
    )
    document = fields.Many2one(
        string=_("Document"),
        help=_("Payment pyramid concept for journals used on payments"),
        comodel_name='piramide_de_pago',
        domain=document_domain
    )
    cost_center = fields.Many2one(
        string=_("Cost center"),
        help=_("Cost centers of pyramid payment journals used on payments"),
        comodel_name='account.cost.center',
        domain=cost_center_domain
    )
    summary = fields.Boolean(
        string=_("Include summary"),
        help=_("Show a brief summary of all the information in the report"),
        default=True
    )

    @api.multi
    def show_pdf(self):
        return self.env['report'].with_context(
            summary=self.summary).get_action(
            self.env['account.voucher'].sudo().browse(self._get_ids()),
            'reports.payment_reception_report'
        )

    @api.multi
    def show_xls(self):
        ids = self._get_ids('csv')
        csv_id = self.env['r.payment.reception.report.view'].csv_by_ids(ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id={id}'.format(id=csv_id.id),
            'target': 'self',
        }

    @api.multi
    def show_screen_report(self):
        payment_recp_obj = self.env['payment.reception.report.transient']
        return payment_recp_obj.screen_data(self._get_ids('screen'))

    @api.onchange('start_date', 'end_date')
    def validate_filters(self):
        df = self.start_date
        dt = self.end_date

        if df and dt and df > dt:
            self.start_date = False
            self.end_date = False
            raise exceptions.Warning(
                "'Date to' must be higher than 'Date from'.")

    def _get_ids(self, file='pdf'):
        # Get filter values
        user = self.user
        s_date = self.start_date
        e_date = self.end_date
        journal = self.journal
        document = self.document
        cost_center = self.cost_center

        # Get journal ids for document and cost_center
        journal_ids = []

        query = "SELECT DISTINCT\n"\
                "   journal_id\n"\
                "FROM payment_reception_journal_view\n" \
                "WHERE journal_id IS NOT NULL\n"
        if journal:
            query += "  AND journal_id = CAST(%s AS int)\n" % (journal.id)

        if document:
            query += "  AND piramide_de_pago_id = CAST(%s AS int)\n" % (
                document.id)

        if cost_center:
            query += "  AND account_cost_center_id = CAST(%s AS int)\n" % (
                cost_center.id)

        query += "ORDER BY journal_id ASC;"
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            journal_ids = [j_id[0] for j_id in self.env.cr.fetchall()]

        journal_ids = tuple(journal_ids)
        """
        Format ids to string because of the tuple format form with one element
        (1,)
        """
        if len(journal_ids) == 1:
            journal_ids_str = "(%s)" % (journal_ids[0])
        elif len(journal_ids) > 1:
            journal_ids_str = "%s" % (journal_ids,)
        else:
            journal_ids_str = "()"

        # Building SQL query string
        if file == 'pdf':
            query = "SELECT id FROM account_voucher av\n" \
                    "WHERE journal_id IN (\n"\
                    "   SELECT DISTINCT account_journal_id\n"\
                    "   FROM account_journal_piramide_de_pago_rel\n"\
                    ")\n"\
                    "AND av.state != 'draft'\n"
        elif file == 'csv':
            query = "SELECT id FROM r_payment_reception_report_view av\n"\
                    "WHERE id IS NOT NULL\n"

        elif file == 'screen':
            query = "SELECT id FROM r_payment_reception_report_view av\n"\
                    "WHERE id IS NOT NULL\n"

        if user:
            query += "  AND av.write_uid = CAST(%s AS int)\n" % (user.id)

        if s_date:
            query += "  AND av.date >= CAST('%s' AS date)\n" % (s_date)

        if e_date:
            query += "  AND av.date <= CAST('%s' AS date)\n" % (e_date)

        if journal_ids:
            query += "  AND av.journal_id IN %s\n" % (journal_ids_str)

        query += "ORDER BY id ASC;"  # Close query string

        self.env.cr.execute(query)

        if self.env.cr.rowcount:
            ids = [id[0] for id in self.env.cr.fetchall()]
        else:
            raise exceptions.Warning(_("No records found!"))

        return ids

    @api.onchange('document', 'cost_center', 'journal')
    def domain_filters(self):
        """Filters all banks configured with the same currency
        as the current cash back document.
        If the cash back transaction includes a check
        then it will retrieve all banks with checkbooks."""
        res = {}
        document_domain = []
        cost_center_domain = []
        journal_domain = []
        document_ids = []
        cost_center_ids = []
        journal_ids = []
        query = "SELECT\n" \
                "   journal_id,\n" \
                "   piramide_de_pago_id,\n" \
                "   account_cost_center_id\n" \
                "FROM payment_reception_journal_view\n" \
                "WHERE journal_id IS NOT NULL\n"

        if self.document:
            query += "  AND piramide_de_pago_id = CAST(%s AS int)\n" % (
                self.document.id)
        if self.cost_center:
            query += "  AND account_cost_center_id = CAST(%s AS int)\n" % (
                self.cost_center.id)
        if self.journal:
            query += "  AND journal_id = CAST(%s AS int)\n" % (
                self.journal.id)
        query += "ORDER BY journal_id ASC;"
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            objects = [obj for obj in self.env.cr.fetchall()]
            for obj in objects:
                journal_ids.append(obj[0])
                document_ids.append(obj[1])
                cost_center_ids.append(obj[2])
            journal_ids = list(set(journal_ids))
            document_ids = list(set(document_ids))
            cost_center_ids = list(set(cost_center_ids))
            document_domain = [('id', 'in', document_ids)]
            cost_center_domain = [('id', 'in', cost_center_ids)]
            journal_domain = [('id', 'in', journal_ids)]

        res['domain'] = {
            'document': document_domain,
            'cost_center': cost_center_domain,
            'journal': journal_domain
        }

        return res


class PaymentReceptionJournalView(models.Model):
    """Cash back report by date/bank Wizard"""

    # odoo model properties
    _name = 'payment.reception.journal.view'
    _table = 'payment_reception_journal_view'
    _description = 'Payment reception journal view'
    _auto = False  # Don't let odoo create table
    # _order = 'id DESC'

    # View fieds
    journal_id = fields.Integer(
        readonly=True,
        string=_("Journal")
    )
    piramide_de_pago_id = fields.Integer(
        readonly=True,
        string=_("Piramide de pago")
    )
    account_cost_center_id = fields.Integer(
        readonly=True,
        string=_("Cost center")
    )

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'payment_reception_journal_view')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW payment_reception_journal_view
                AS (
                    SELECT
                        DISTINCT av.journal_id,
                        ajpdp.piramide_de_pago_id,
                        accaj.account_cost_center_id
                    FROM account_voucher av
                    JOIN account_journal_piramide_de_pago_rel ajpdp
                        ON (ajpdp.account_journal_id = av.journal_id)
                    JOIN account_cost_center_account_journal_rel accaj
                        ON (accaj.account_journal_id = ajpdp.account_journal_id)
                    JOIN account_voucher_line avl
                        ON (avl.voucher_id = av.id)
                    WHERE av.state != 'draft'
                        AND avl.amount > 0
                        AND avl.type = 'cr'
                    ORDER BY av.journal_id ASC
                )
            """
        )
