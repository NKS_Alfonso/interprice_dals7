# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, _


class PaymentReceptionReportLines(models.Model):
    _name = 'payment.reception.report.lines'


    payment_reception = fields.Many2one(
        comodel_name='r.payment.reception.report.view',
        string=_("Payment Reception Report")
    )

    lines_ids = fields.One2many(
        comodel_name='payment.reception.report.transient',
        inverse_name='line_id',
        string=_("Payments data")
    )
