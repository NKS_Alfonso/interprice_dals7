# -*- coding: utf-8 -*-
# Copyright © 2018 TO-DO - All Rights Reserved
# Author      TO-DO Developers

from openerp import fields, models, _


class PaymentReceptionReportTransient(models.TransientModel):
    _name = 'payment.reception.report.transient'

    payment_reception = fields.Many2one(
        comodel_name='r.payment.reception.report.view',
        string=_("Payment reception")
    )

    line_id = fields.Many2one(
        comodel_name='payment.reception.report.lines',
        string=_("Line ID")
    )
    # View fieds
    id = fields.Integer(
        readonly=True,
        string=_("Voucher line")
    )
    parity = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Parity")
    )
    piramide_name = fields.Char(
        string=_("Pyramid name"),
        readonly=True
    )
    reference = fields.Char(
        string=_("Payment reference"),
        readonly=True
    )
    number = fields.Char(
        string=_("Payment number"),
        readonly=True
    )
    date = fields.Date(
        readonly=True,
        string="Date",
    )
    doc_amount = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Payment amount")
    )
    currency_name = fields.Char(
        string=_("Payment currency name"),
        readonly=True
    )
    move_ref = fields.Char(
        string=_("Move reference"),
        readonly=True
    )
    client_number = fields.Char(
        string=_("Payment client number"),
        readonly=True
    )
    client_name = fields.Char(
        string=_("Payment client name"),
        readonly=True
    )
    amount_original = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Amount original")
    )
    move_currency_name = fields.Char(
        string=_("Move currency name"),
        readonly=True
    )
    amount = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Amount")
    )
    state = fields.Char(
        string=_("Payment state"),
        readonly=True
    )
    user_name = fields.Char(
        string=_("Payment user name"),
        readonly=True
    )
    journal_id = fields.Integer(
        readonly=True,
        string=_("Journal")
    )
    voucher_id = fields.Integer(
        readonly=True,
        string=_("Voucher")
    )


    def screen_data(self, _ids):
        if _ids:
            tree_form = self.env.ref(
                'reports.payment_reception_data',
                False
            )
            data_array = []
            data = self.env['r.payment.reception.report.view'].sudo().browse(_ids)
            doc_number = None
            doc_amount = 0

            for row in data:
                if doc_number != row.number:
                    doc_amount = row.doc_amount
                else:
                    doc_amount = 0

                parity = self.env['account.voucher'].browse(
                [row.voucher_id]
                ).get_voucher_parity()

                data_array.append((0, 0, {
                    'parity': parity,
                    'piramide_name': row.piramide_name,
                    'reference': row.reference,
                    'number': row.number,
                    'date': row.date,
                    'doc_amount': doc_amount or 0,
                    'currency_name': row.currency_name,
                    'move_ref': row.move_ref,
                    'client_number': row.client_number,
                    'client_name': row.client_name,
                    'amount_original': row.amount_original,
                    'move_currency_name': row.move_currency_name,
                    'amount': row.amount,
                    'state': row.state,
                    'user_name': row.user_name
                }))
                doc_number = row.number

            values = {
                'lines_ids': data_array
            }

            new = self.env['payment.reception.report.lines'].create(values)

            return {
                'name': 'Payment reception data',
                'type': 'ir.actions.act_window',
                'res_model': 'payment.reception.report.lines',
                'res_id': new.id,
                'view_id': tree_form.id,
                'view_type': 'tree',
                'view_mode': 'form',
                'target': 'current',
            }
