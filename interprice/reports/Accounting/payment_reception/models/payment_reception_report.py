# -*- coding: utf-8 -*-
# Copyright © 2016 TO-DO - All Rights Reserved
# Author      TO-DO Developers

# standard library imports
# related third party imports
from olib.oCsv.oCsv import OCsv
# local application/library specific imports
from openerp import models, fields, _, tools


class AccountVoucher(models.Model):
    """New method to retrieve information for the
    payment reception report
    """

    # odoo model properties
    _inherit = 'account.voucher'

    # Report methods
    def get_voucher_currency_id(self):
        return self.journal_id.currency or self.journal_id.company_id.currency_id

    def get_voucher_currency(self):
        return self.journal_id.currency.name or self.journal_id.company_id.currency_id.name

    def get_doc_amount(self):
        amount = self.amount
        if self.amount == 0.00 and len(self.line_dr_ids) > 0:
            for line in self.line_dr_ids:
                amount += line.amount
        return amount

    def get_voucher_parity(self):
        parity = 1.0
        if self.payment_rate != 0.00 or self.payment_rate != 1.00:
            self.env.cr.execute(
                """
                SELECT rate_sale
                FROM res_currency_rate rcr
                WHERE rcr.currency_id = CAST(%s AS integer)
                    AND rcr.name <= %s
                    AND rcr.rate_sale > 0
                ORDER BY name DESC
                LIMIT 1
                """,
                (
                    self.get_voucher_currency_id().id,
                    self.date
                )
            )
            if self.env.cr.rowcount:
                rate = self.env.cr.fetchall()[0][0]
            else:
                rate = 1.0
            amount = self.get_doc_amount()
            try:
                parity = ((amount / rate) / amount)
            except ZeroDivisionError:
                parity = self.payment_rate or 1.0

        return "%.4f" % parity


class AccountVoucherLine(models.Model):
    """New method to retrieve information for the
    payment reception report lines
    """

    # odoo model properties
    _inherit = 'account.voucher.line'

    # Report methods
    def get_move_currency(self):
        return self.move_line_id.currency_id.name or self.company_id.currency_id.name

    def get_piramide_de_pago(self):
        piramide_id = []
        self.env.cr.execute(
            """
            SELECT pdp.id
            FROM piramide_de_pago pdp
            JOIN account_journal_piramide_de_pago_rel ajpdp
                ON (ajpdp.piramide_de_pago_id = pdp.id)
            WHERE ajpdp.account_journal_id = CAST(%s AS int)
            LIMIT 1;
            """,
            (self.voucher_id.journal_id.id,)
        )
        if self.env.cr.rowcount:
            piramide_id = [j[0] for j in self.env.cr.fetchall()]

        return self.env['piramide_de_pago'].browse(piramide_id)


class ResUser(models.Model):
    """Extended class for adding methods to calculate totals by user
    """
    _inherit = 'res.users'

    def get_piramide_de_pago(self):
        piramide_ids = []
        self.env.cr.execute(
            """
            SELECT DISTINCT piramide_de_pago_id AS id
            FROM account_journal_piramide_de_pago_rel ajpdp
            JOIN piramide_de_pago pdp ON (id = pdp.id)
            WHERE ajpdp.account_journal_id IN (
                SELECT DISTINCT journal_id FROM account_voucher av
                    WHERE av.journal_id IS NOT NULL
                        AND av.write_uid = CAST(%s AS int)
            )
            ORDER BY id ASC;
            """,
            (self.id,)
        )
        if self.env.cr.rowcount:
            piramide_ids = [j[0] for j in self.env.cr.fetchall()]

        return self.env['piramide_de_pago'].browse(piramide_ids)


class PiramideDePago(models.Model):
    """Extended class for adding methods to calculate totals by user
    """
    _inherit = 'piramide_de_pago'

    def get_account_voucher_amount(self, _av_ids, _currency_name):
        amount = 0.0
        av_ids = []
        self.env.cr.execute(
            """
            SELECT av.id FROM account_voucher av
            JOIN account_journal_piramide_de_pago_rel ajpdp
                ON (ajpdp.account_journal_id = av.journal_id)
            JOIN account_journal aj
                ON (aj.id = av.journal_id)
            WHERE ajpdp.piramide_de_pago_id = CAST(%s AS int)
                AND av.id IN %s
            ORDER BY av.id ASC;
            """,
            (self.id, tuple(_av_ids))
        )
        if self.env.cr.rowcount:
            av_ids = [a[0] for a in self.env.cr.fetchall()]
        for av in self.env['account.voucher'].browse(av_ids):
            if av.get_voucher_currency() == _currency_name:
                amount += av.get_doc_amount()
        return amount


class PaymentReceptionReportView(models.Model):
    """Cash back report by date/bank Wizard"""

    # odoo model properties
    _name = 'r.payment.reception.report.view'
    _table = 'r_payment_reception_report_view'
    _description = 'Payment reception report'
    _auto = False  # Don't let odoo create table
    # _order = 'id DESC'

    # View fieds
    id = fields.Integer(
        readonly=True,
        string=_("Voucher line")
    )
    parity = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Parity")
    )
    piramide_name = fields.Char(
        string=_("Pyramid name"),
        readonly=True
    )
    reference = fields.Char(
        string=_("Payment reference"),
        readonly=True
    )
    number = fields.Char(
        string=_("Payment number"),
        readonly=True
    )
    date = fields.Date(
        readonly=True,
        string="Date",
    )
    doc_amount = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Payment amount")
    )
    currency_name = fields.Char(
        string=_("Payment currency name"),
        readonly=True
    )
    move_ref = fields.Char(
        string=_("Move reference"),
        readonly=True
    )
    client_number = fields.Char(
        string=_("Payment client number"),
        readonly=True
    )
    client_name = fields.Char(
        string=_("Payment client name"),
        readonly=True
    )
    amount_original = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Amount original")
    )
    move_currency_name = fields.Char(
        string=_("Move currency name"),
        readonly=True
    )
    amount = fields.Float(
        digits=(12, 4),
        readonly=True,
        string=_("Amount")
    )
    state = fields.Char(
        string=_("Payment state"),
        readonly=True
    )
    user_name = fields.Char(
        string=_("Payment user name"),
        readonly=True
    )
    journal_id = fields.Integer(
        readonly=True,
        string=_("Journal")
    )
    voucher_id = fields.Integer(
        readonly=True,
        string=_("Voucher")
    )

    # Create PostgreSQL view
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'r_payment_reception_report_view')

        # Create view
        cr.execute(
            """
            CREATE OR REPLACE VIEW r_payment_reception_report_view
                AS (
                WITH account_voucher_info AS (
                SELECT
                    av.id AS voucher_id,
                    round(COALESCE(CASE WHEN av.amount > 0
                        THEN av.amount
                        ELSE (
                            SELECT SUM(avld.amount)
                            FROM account_voucher_line avld
                            WHERE avld.voucher_id = av.id
                                AND avld.amount > 0
                                AND avld.type = 'dr'
                        )
                        END, 0), 4) AS doc_amount,
                    round(COALESCE(rcr.rate_sale, 1), 4) AS rate_sale,
                    rcur.name AS currency_name,
                    av.number AS number,
                    av.reference,
                    av.date,
                    pdp.str_nombre AS piramide_name,
                    rp.client_number,
                    rp.name AS client_name,
                    CASE WHEN av.state = 'cancel'
                            THEN 'Cancelado'
                        WHEN av.state = 'proforma'
                            THEN 'Pro-forma'
                        WHEN av.state = 'posted'
                            THEN 'Contabilizado'
                        ELSE ''
                        END AS state,
                    rpar.name AS user_name,
                    av.journal_id,
                    av.write_uid
                FROM account_voucher av
                JOIN res_users ru
                    ON (ru.id = av.write_uid)
                JOIN res_partner rpar
                    ON (rpar.id = ru.partner_id)
                JOIN account_journal aj
                    ON (aj.id = av.journal_id)
                JOIN res_company rcom
                    ON (rcom.id = aj.company_id)
                JOIN res_currency rcur
                    ON (rcur.id = COALESCE(aj.currency, rcom.currency_id))
                LEFT JOIN res_currency_rate rcr
                    ON (rcr.id = (
                        SELECT rcra.id FROM res_currency_rate rcra
                        WHERE rcra.currency_id = rcur.id
                            AND rcr.name <= av.date
                            AND rcr.rate_sale > 0
                        ORDER BY rcra.name DESC
                        LIMIT 1
                        )
                    )
                JOIN account_journal_piramide_de_pago_rel ajpdp
                    ON (ajpdp.account_journal_id = av.journal_id)
                JOIN piramide_de_pago pdp
                    ON (pdp.id = ajpdp.piramide_de_pago_id)
                JOIN res_partner rp
                    ON (rp.id = av.partner_id)
                WHERE ajpdp.account_journal_id = av.journal_id
                    AND av.state != 'draft'
                ORDER BY voucher_id ASC
            )
            SELECT
                avl.id,
                CASE WHEN avi.doc_amount <= 0
                    THEN round(1, 4)
                    ELSE (round((avi.doc_amount / avi.rate_sale) / avi.doc_amount, 4))
                    END AS parity,
                avi.piramide_name,
                avi.reference,
                avi.number,
                avi.date,
                avi.doc_amount,
                avi.currency_name,
                aml.ref AS move_ref,
                avi.client_number,
                avi.client_name,
                round(avl.amount_original, 4) AS amount_original,
                rcuml.name AS move_currency_name,
                avl.amount,
                avi.state,
                avi.user_name,
                avi.journal_id,
                avi.write_uid,
                avi.voucher_id
            FROM account_voucher_line avl
                JOIN account_voucher_info avi
                    ON (avi.voucher_id = avl.voucher_id)
                JOIN account_move_line aml
                    ON (aml.id = avl.move_line_id)
                JOIN res_company rcml
                    ON (rcml.id = aml.company_id)
                JOIN res_currency rcuml
                    ON (rcuml.id = COALESCE(aml.currency_id, rcml.currency_id))
            WHERE avl.amount > 0
                AND avl.type = 'cr'
            ORDER BY avl.id ASC
                )
            """
        )

    def csv_by_ids(self, _ids):
        data = self.env['r.payment.reception.report.view'].sudo().browse(_ids)

        data_csv = [
            [
                _('Parity'),
                _('Document'),
                _('Reference'),
                _('Folio'),
                _('Date'),
                _('Amount'),
                _('Currency'),
                _('Ref. Document.'),
                _('Client number'),
                _('Name'),
                _('Ref. Amount'),
                _('Ref. Currency'),
                _('Applied amount'),
                _('Applied currency'),
                _('Status'),
                _('User')
            ]
        ]
        doc_number = None
        doc_amount = 0
        for row in data:
            if doc_number != row.number:
                doc_amount = row.doc_amount
            else:
                doc_amount = 0

            parity = self.env['account.voucher'].browse(
                [row.voucher_id]
            ).get_voucher_parity()
            data_csv.append([
                unicode(self._set_default(parity)).encode('utf8'),
                unicode(self._set_default(row.piramide_name)).encode('utf8'),
                unicode(self._set_default(row.reference)).encode('utf8'),
                unicode(self._set_default(row.number)).encode('utf8'),
                unicode(self._set_default(row.date)).encode('utf8'),
                unicode(self._set_default(doc_amount, 0)).encode('utf8'),
                unicode(self._set_default(row.currency_name)).encode('utf8'),
                unicode(self._set_default(row.move_ref)).encode('utf8'),
                unicode(self._set_default(row.client_number)).encode('utf8'),
                unicode(self._set_default(row.client_name)).encode('utf8'),
                unicode(self._set_default(
                    row.amount_original, 0)).encode('utf8'),
                unicode(self._set_default(
                    row.move_currency_name)).encode('utf8'),
                unicode(self._set_default(row.amount, 0)).encode('utf8'),
                unicode(self._set_default(row.currency_name)).encode('utf8'),
                unicode(self._set_default(row.state)).encode('utf8'),
                unicode(self._set_default(row.user_name)).encode('utf8')
            ])
            doc_number = row.number

        file = OCsv().csv_base64(data_csv)
        return self.env['r.download'].create(
            vals={
                'file_name': 'PaymentReceptionReport.csv',
                'type_file': 'csv',
                'file': file,
            })

    def _set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
