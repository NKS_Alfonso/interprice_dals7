# -*- coding: utf-8 -*-
# @version     : 1.0
# @autor       : Viridiana Cruz Santos

from openerp import fields, models, api, _
from openerp.osv import osv, fields as fieldsv7
import openerp.tools as tools
from datetime import datetime
from olib.oCsv.oCsv import OCsv
from openerp.tools.translate import _

class ReportsCustomerStatement(osv.osv_memory):
    _name = 'r.customer_statement'

    partner_id = fields.Many2one('res.partner', string='Cliente', required=True, domain=[('customer', '=', True)])
    status = fields.Char(string='Status', readonly=True)
    paridad = fields.Char(string='Paridad', readonly=True)
    saldo_cuenta = fields.Char(string='Saldo cuenta', readonly=True)
    date_from = fields.Datetime(string='De')
    date_to = fields.Datetime(string='A')
    filter_by = fields.Selection([('saldo', 'Sólo con saldo'), ('cobro', 'Con cobros')], 'Filtrar', required=True)
    filter_date = fields.Selection([('emision', 'Fecha emisión'), ('vencimiento', 'Fecha vencimiento')], 'Filtrar', required=True)

    def onchange_datos(self, cr, uid, ids, partner_id):
        rate = self.pool.get('res.users').browse(cr, uid, uid)
        paridad = rate.company_id.currency_id.rate_silent
        data = self.pool.get('res.partner').browse(cr, uid, partner_id)
        status = ''
        saldo_cuenta = data.credit
        if data.active == True:
            status = 'Activo'
        else:
            status = 'Inactivo'

        if partner_id == False:
            status = ''
            paridad = ''
            saldo_cuenta = ''

        return {
            'value': {
                'status': status,
                'saldo_cuenta': saldo_cuenta,
                'paridad': paridad}
                }

    def _get_ids(self, cr, uid, ids, type):
        dataset_ids = []
        data = self.browse(cr, uid, ids, context=None)
        query_and = ''
        if data.filter_by == 'saldo':
            if data.filter_date == 'emision':
                query_and = """ AND create_doc_utc >= '%s' AND create_doc_utc <= '%s' AND invoice_number_copy = ''
                                AND filtro in ('facturas_notas','pagos') AND saldo >= 0 """\
                            % (data.date_from, data.date_to,)
            elif data.filter_date == 'vencimiento':
                query_and = """ AND due_doc_utc >= '%s' AND due_doc_utc <= '%s' AND invoice_number_copy = ''
                                AND filtro in ('facturas_notas', 'pagos') AND saldo >= 0 """ \
                            % (data.date_from, data.date_to,)

        elif data.filter_by == 'cobro':
            if data.filter_date in ['emision', 'vencimiento']:
                query_and = """ AND create_doc_utc >= '%s' AND create_doc_utc <= '%s' AND type_doc in ('Anticipo')
                                AND saldo_ant > 0 AND filtro in ('pagos','pagos_aplicados') """\
                            % (data.date_from, data.date_to,)

        cr.execute(""" SELECT id FROM r_customer_statement_data
                        WHERE customer_id = %s %s """ % (data.partner_id.id, query_and))

        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            report_id = ids[0]
            if type == 'show':
                cr.execute('DELETE FROM r_customer_statement WHERE id=%s', (report_id,))
        else:
            data.unlink()
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids

    def show(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=None)
        data_ids = self._get_ids(cr, uid, ids, type='show')
        return {
            'name': 'Reporte',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.customer.statement.data',
            'target': 'current',
            'context': '{"date_from":"%s", "date_to":"%s", "filter_by":"%s"}' % (data.date_from, data.date_to, data.filter_by),
            'domain': [('id', 'in', data_ids)],
        }

    def csv(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=None)
        dataset_ids = self._get_ids(cr, uid, ids, type='csv')
        dataset_ids = self.pool.get('r.customer.statement.data').search(cr, uid, [('id', 'in', dataset_ids)], context=None, order='invoice_id asc, invoice_number_copy asc, invoice_date asc')
        context = {'date_from': data.date_from, 'date_to': data.date_to, 'filter_by': data.filter_by}
        id = self.pool['r.customer.statement.data'].to_csv_by_ids(cr, uid, dataset_ids, context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id),
            'target': 'self',
            'context': '{"date_from":"%s", "date_to":"%s", "filter_by":"%s"}' % (data.date_from, data.date_to, data.filter_by),
        }

class ReportsAccountingCustomerStatement(models.Model):
    _name = 'r.customer.statement.data'
    _table = 'r_customer_statement_data'
    _auto = False
    _order = 'invoice_id asc, invoice_number_copy asc, invoice_date asc'

    customer_id = fields.Char(string='ID')
    customer_name = fields.Char(string='Cliente')
    invoice_id = fields.Char(string='ID FAC')
    invoice_number = fields.Char(string='Documento')
    invoice_number_copy = fields.Char(string='Documento ANT')
    refe = fields.Char(string='REF')
    crdr = fields.Char(string='crDr')
    invoice_date = fields.Datetime(string='Fecha emisión')
    invoice_date_due = fields.Date(string='Fecha vencimiento')
    state = fields.Char(string='Estado')
    type_doc = fields.Char(string='Tipo documento')
    warehouse = fields.Char(string='Referencia')
    origin = fields.Char(string='Pedido')
    pay_days = fields.Char(string='Días de crédito')
    total = fields.Float(string='Importe', digits=(10, 2))
    saldo = fields.Float(string='Saldo Pendiente')
    saldo_ant = fields.Float(string='Saldo Pago')
    saldo_mxn = fields.Float(compute='_mxn_usdResidual', digits=(10, 2), string='Saldo MXN')
    saldo_usd = fields.Float(compute='_mxn_usdResidual', digits=(10, 2), string='Saldo USD')
    iva = fields.Char(string='IVA')
    currency = fields.Char(string='Moneda')
    currency_journal = fields.Char(string='Moneda diario')
    currency_comp = fields.Char(string='Moneda compania')
    rate = fields.Float(string='Paridad', digits=(10, 2),)
    company = fields.Char(string='Compañia')
    filtro = fields.Char(string='Filtro')

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    @api.multi
    def _mxn_usdResidual(self):
        date_from = self._context['date_from']
        date_to = self._context['date_to']
        filter_by = self._context['filter_by']
        date_from_utc = self.utc_mexico_city(date_from)
        date_to_utc = self.utc_mexico_city(date_to)

        for rec in self:
            if filter_by == 'saldo':
                if rec.state != 'Cancelado':
                    if rec.type_doc in ('FAC', 'NCC'):
                        self.env.cr.execute(""" SELECT ai.id FROM account_invoice ai WHERE ai.id=%s """, (rec.invoice_id,))
                        if self.env.cr.rowcount:
                            invoice = self.env.cr.fetchone()[0]
                            idInvoice = self.env['account.invoice'].browse(invoice)
                            credit = 0.00
                            amount_total = idInvoice.amount_total
                            for payment_id in idInvoice.payment_ids:
                                pay_date = payment_id.date

                                if payment_id.amount_currency > 0:
                                    amount_currency = payment_id.amount_currency
                                else:
                                    amount_currency = payment_id.amount_currency * -1
                                payment = payment_id.credit
                                payment_cancel = payment_id.move_id.ref

                                if pay_date >= date_from_utc and pay_date <= date_to_utc:
                                    if idInvoice.currency_id.name == 'USD':
                                        if payment_cancel.find('cancelado') == -1 and payment_cancel.find('Cancelada') == -1:
                                            credit = credit + amount_currency

                                    if idInvoice.currency_id.name == 'MXN':
                                        if payment_cancel.find('cancelado') == -1 and payment_cancel.find('Cancelada') == -1:
                                            credit = credit + payment
                                amount_total = idInvoice.amount_total - credit
                            if rec.currency == 'MXN':
                                rec.saldo_mxn = amount_total
                                rec.saldo_usd = amount_total / rec.rate
                            if rec.currency == 'USD':
                                rec.saldo_mxn = amount_total * rec.rate
                                rec.saldo_usd = amount_total

                    elif rec.type_doc in ('Anticipo'):
                        self.env.cr.execute(""" SELECT COUNT(*) FROM account_voucher_line avl
                                                  INNER JOIN account_move_line aml ON avl.move_line_id=aml.id
                                                  WHERE aml.name=%s """,
                                            (rec.invoice_number,))
                        count = self.env.cr.fetchone()[0]
                        if count > 0:
                            suma = 0
                            recon = 0
                            self.env.cr.execute(""" select av.id
                                                    from account_move_line aml
                                                    inner join account_voucher_line avl on aml.id = avl.move_line_id
                                                    inner join account_voucher av on avl.voucher_id=av.id
                                                    inner join account_journal aj on av.journal_id=aj.id
                                                    left join res_currency rc on aj.currency=rc.id
                                                    where aml.name = %s and avl.type='dr' """,
                                                (rec.invoice_number,))
                            if self.env.cr.rowcount:
                                idPayment = self.env.cr.fetchall()

                                for idPay in idPayment:
                                    Payment_id = idPay[0]
                                    idVoucher = self.env['account.voucher'].browse(Payment_id)

                                    for idAnt in idVoucher:
                                        payCurrency = idAnt.journal_id.currency.name
                                        if payCurrency == False:
                                            currencyPay = rec.currency_comp
                                        else:
                                            currencyPay = payCurrency

                                    for line in idVoucher.line_dr_ids:
                                        amount = line.amount
                                        currencyAnt = line.move_line_id.journal_id.currency.name
                                        pay_date = idVoucher.date
                                        if currencyAnt == False:
                                            currencyAnticipo = rec.currency_comp
                                        else:
                                            currencyAnticipo = currencyAnt
                                    if currencyAnticipo == 'USD':
                                        if currencyPay == 'MXN':
                                            recon = amount / rec.rate
                                        else:
                                            recon = amount
                                    if currencyAnticipo == 'MXN':
                                        if currencyPay == 'USD':
                                            recon = amount * rec.rate
                                        else:
                                            recon = amount

                                    if pay_date >= date_from_utc and pay_date <= date_to_utc:
                                        if idVoucher.state == 'posted':
                                            suma = suma + recon

                                        amount_total = rec.total - suma
                                        if rec.currency == 'USD':
                                            rec.saldo_usd = amount_total
                                            rec.saldo_mxn = amount_total * rec.rate
                                        elif rec.currency == 'MXN':
                                            rec.saldo_usd = amount_total / rec.rate
                                            rec.saldo_mxn = amount_total
                        else:
                            if rec.currency == 'USD':
                                rec.saldo_usd = rec.total
                                rec.saldo_mxn = rec.total * rec.rate
                            elif rec.currency == 'MXN':
                                rec.saldo_usd = rec.total / rec.rate
                                rec.saldo_mxn = rec.total

                else:
                    rec.saldo_mxn = 0.00
                    rec.saldo_usd = 0.00

            if filter_by == 'cobro':
                self.env.cr.execute(""" SELECT COALESCE(sum(avl.amount),0)
                                                        FROM account_voucher_line avl
                                                        INNER join account_voucher av on avl.voucher_id = av.id
                                                        WHERE av.number =%s AND avl.type='cr'
                                                        AND av.date >=%s AND av.date <=%s
                                                        AND av.state='posted' """,
                                    (rec.invoice_number, date_from_utc, date_to_utc,))
                pagos_cr = self.env.cr.fetchone()[0]

                self.env.cr.execute(""" SELECT COALESCE(sum(avl.amount),0)
                                                        FROM account_voucher_line avl
                                                        INNER join account_voucher av on avl.voucher_id = av.id
                                                        INNER JOIN account_move_line aml ON avl.move_line_id = aml.id
                                                        AND avl.type = 'dr' AND aml.name = %s
                                                        WHERE av.date >= %s AND av.date <=%s
                                                        AND av.state='posted' """,
                                    (rec.invoice_number, date_from_utc, date_to_utc,))
                pagos_dr = self.env.cr.fetchone()[0]

                sum_pagos = pagos_cr + pagos_dr
                saldo = rec.total - sum_pagos
                if rec.currency == 'MXN':
                    rec.saldo_mxn = saldo
                    rec.saldo_usd = saldo / rec.rate
                if rec.currency == 'USD':
                    rec.saldo_mxn = saldo * rec.rate
                    rec.saldo_usd = saldo

                if rec.crdr == 'dr':
                    if rec.invoice_number_copy != '':
                        rec.invoice_number = '->' + rec.invoice_number_copy
                        if rec.currency_journal == 'USD':
                            if rec.currency == 'MXN':
                                importe = rec.total * rec.rate
                                rec.total = importe
                                saldo_pendiente = rec.saldo * rec.rate
                                rec.saldo_mxn = saldo_pendiente
                                rec.saldo_usd = saldo_pendiente / rec.rate
                            if rec.currency == 'USD':
                                rec.saldo_mxn = saldo_pendiente * rec.rate
                                rec.saldo_usd = saldo_pendiente

                        if rec.currency_journal == 'MXN':
                            if rec.currency == 'MXN':
                                saldo_pendiente = rec.saldo
                                rec.saldo_mxn = saldo_pendiente
                                rec.saldo_usd = saldo_pendiente / rec.rate
                            if rec.currency == 'USD':
                                rec.saldo_mxn = saldo_pendiente * rec.rate
                                rec.saldo_usd = saldo_pendiente

                if rec.crdr == 'cr':
                    if rec.invoice_number_copy != '':
                        rec.invoice_number = '-->' + rec.invoice_number_copy
                        if rec.currency_journal == 'USD':
                            if rec.currency == 'MXN':
                                saldo_pendiente = rec.saldo * rec.rate
                                rec.saldo_mxn = saldo_pendiente
                                rec.saldo_usd = saldo_pendiente / rec.rate
                            if rec.currency == 'USD':
                                rec.saldo_mxn = rec.saldo * rec.rate
                                rec.saldo_usd = rec.saldo

                        if rec.currency_journal == 'MXN':
                            if rec.currency == 'MXN':
                                rec.saldo_mxn = rec.saldo
                                rec.saldo_usd = rec.saldo / rec.rate
                            if rec.currency == 'USD':
                                rec.saldo_mxn = rec.saldo
                                rec.saldo_usd = rec.saldo / rec.rate

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_customer_statement_data')

        cr.execute("""
            create or replace view r_customer_statement_data as (
            SELECT
            row_number() over (order by T1.invoice_date) as id,
                T1.invoice_id,
                T1.invoice_number,
                T1.refe,
                T1.invoice_number_copy,
                T1.warehouse,
                T1.origin,
                CASE T1.state
                   WHEN 'open' THEN 'Abierto'
                   WHEN 'paid' THEN 'Pagado'
                   WHEN 'cancel' THEN 'Cancelado'
                   WHEN 'posted' THEN 'Contabilizado'
                   WHEN 'proforma' THEN 'Pro-forma'
                   ELSE '' END state,
                T1.invoice_date,
                T1.invoice_date_due,
                T1.invoice_date as create_doc_utc,
                T1.invoice_date_due as due_doc_utc,
                T1.rate_currency_comp,
                T1.rate_currency_usd,
                T1.rate_currency_mxn,
                CASE T1.currency_comp WHEN 'MXN' then 1/T1.rate_currency_usd
                                      WHEN 'USD' THEN 1/T1.rate_currency_mxn end rate,
                T1.company,
                --T1.total,
                T1.saldo_ant,
                T1.saldo_ant_dr,
                CASE WHEN T1.type_invoice = 'out_invoice' THEN T1.total
                                     WHEN T1.type_invoice = 'out_refund' THEN T1.total
                                     WHEN T1.type_invoice = 'receipt' and T1.filtro = 'pagos'
                                     THEN T1.total + T1.saldo_ant_dr
                                     WHEN T1.type_invoice = 'receipt' and T1.filtro = 'pagos_aplicados'
                                     THEN T1.total END total,
                T1.balance,
                CASE T1.type_invoice WHEN 'out_invoice' THEN T1.saldo
                                     WHEN 'out_refund' THEN T1.saldo
                                     WHEN 'receipt' THEN T1.balance - T1.saldo_ant END saldo,
                T1.saldo_mxn,
                T1.saldo_usd,
                T1.iva,
                CASE T1.type_invoice WHEN 'out_invoice' THEN 'FAC'
                   WHEN 'out_refund' THEN 'NCC'
                   WHEN 'receipt' THEN 'Anticipo'
                   ELSE '' END type_doc,
                T1.customer_id,
                T1.customer_name,
                T1.pay_days,
                T1.currency_doc,
                T1.currency_comp,
                CASE WHEN T1.currency_journal = '' then T1.currency_comp else T1.currency_journal end currency_journal,
                CASE WHEN T1.currency_doc = '' then T1.currency_comp else T1.currency_doc end currency,
                T1.crdr,
                T1.filtro
            FROM (
                SELECT
                    ai.id invoice_id,
                    ai.number invoice_number,
                    '' refe,
                    '' invoice_number_copy,
                    COALESCE(sp.name,'') warehouse,
                    COALESCE(sp.origin,'') origin,
                    ai.state state,
                    ai.invoice_datetime invoice_date,
                    ai.date_due at time zone 'UTC' invoice_date_due,
                    ai.amount_total total,
                    0.0 balance,
                    0.00 saldo_ant,
                    0.00 saldo_ant_dr,
                    ai.residual saldo,
                    0 saldo_mxn,
                    0 saldo_usd,
                    COALESCE(ai.amount_tax, 0.00) iva,
                    ai.type type_invoice,
                    (SELECT rcr.rate
                        FROM res_company rcm
                        INNER JOIN res_currency rc on rcm.currency_id=rc.id
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        WHERE rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_comp,
                    (SELECT rcr.rate
                        FROM res_currency rc
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        where rc.name='USD' AND rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_usd,
                    (SELECT rcr.rate
                        FROM res_currency rc
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        where rc.name='MXN' AND rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_mxn,
                    ai.company_id company,
                    rp.id customer_id,
                    rp.name customer_name,
                    COALESCE(apl.days, 0) pay_days,
                    COALESCE(rc.name, '') currency_doc,
                    coalesce(rc.name, '') currency_journal,
                    (SELECT rc.name FROM res_company rcm INNER JOIN res_currency rc on rcm.currency_id=rc.id ) currency_comp,
                    '' crdr,
                    'facturas_notas' filtro
                FROM account_invoice ai
                INNER JOIN res_partner rp ON ai.partner_id=rp.id
                LEFT JOIN stock_picking sp on ai.picking_dev_id=sp.id
                LEFT JOIN account_payment_term apt on ai.payment_term=apt.id
                LEFT join account_payment_term_line apl on apt.id=apl.payment_id
                --LEFT JOIN account_journal aj on ai.journal_id=aj.id
                LEFT JOIN res_currency rc on ai.currency_id=rc.id
                WHERE ai.type IN ('out_invoice','out_refund') and ai.state not in ('paid','draft')

                UNION ALL

                SELECT
                    av.id invoice_id,
                    av.number invoice_number,
                    av.number refe,
                    '' invoice_number_copy,
                    COALESCE(av.reference, '') warehouse,
                    '' origin,
                    av.state state,
                    av.date at time zone 'UTC' invoice_date,
                    av.date at time zone 'UTC' invoice_date_due,
                    av.net_amount total,
                    0.0 balance,
                    (SELECT COALESCE(sum(avl.amount), 0.00) FROM account_voucher_line avl WHERE avl.voucher_id = av.id
                        AND type='cr') saldo_ant,
                    (SELECT COALESCE(sum(avl.amount), 0.00) FROM account_voucher_line avl WHERE avl.voucher_id = av.id
                        AND type='dr') saldo_ant_dr,
                    0.0 saldo,
                    0 saldo_mxn,
                    0 saldo_usd,
                    0.00 iva,
                    av.type type_invoice,
                    (SELECT rcr.rate
                        FROM res_company rcm
                        INNER JOIN res_currency rc on rcm.currency_id=rc.id
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        WHERE rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_comp,
                    (SELECT rcr.rate
                        FROM res_currency rc
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        where rc.name='USD' AND rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_usd,
                    (SELECT rcr.rate
                        FROM res_currency rc
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        where rc.name='MXN' AND rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_mxn,
                    rp.company_id company,
                    rp.id customer_id,
                    rp.name customer_name,
                    0 pay_days,
                    COALESCE(rc.name, '') currency_doc,
                    coalesce(rc.name, '') currency_journal,
                    (SELECT rc.name FROM res_company rcm INNER JOIN res_currency rc on rcm.currency_id=rc.id ) currency_comp,
                    '' crdr,
                    'pagos' filtro
                FROM account_voucher AS av
                INNER JOIN res_partner rp ON av.partner_id=rp.id
                INNER JOIN res_company rcm on rp.company_id=rcm.id
                LEFT JOIN account_journal aj on av.journal_id=aj.id
                LEFT JOIN res_currency rc on aj.currency=rc.id
                WHERE av.state in ('posted') and av.type in ('receipt')

                UNION ALL

                SELECT
                    av.id invoice_id,
                    av.number invoice_number,
                    av.number refe,
                    CASE WHEN aml.name = '/' THEN aml.ref ELSE aml.name END as invoice_number_copy,
                    COALESCE( (SELECT sp.name FROM account_invoice ai LEFT JOIN stock_picking sp on ai.picking_dev_id=sp.id
                                    WHERE ai.number = aml.ref), '') warehouse,
                    COALESCE( (SELECT sp.origin FROM account_invoice ai LEFT JOIN stock_picking sp on ai.picking_dev_id=sp.id
                                    WHERE ai.number = aml.ref), '') origin,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(ai.state, '') FROM account_invoice ai
                                                    WHERE ai.number = aml.ref) ELSE av.state  END state,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(ai.invoice_datetime, null) FROM account_invoice ai
                                                    WHERE ai.number = aml.ref) ELSE av.date at time zone 'UTC'  END invoice_date,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(ai.date_due at time zone 'UTC', null) FROM account_invoice ai
                                                    WHERE ai.number = aml.ref) ELSE av.date  END invoice_date_due,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(ai.amount_total, 0) FROM account_invoice ai
                                                    WHERE ai.number = aml.ref) ELSE avl.amount_original END total,
                    avl.amount_unreconciled balance,
                    (SELECT COALESCE(sum(avl.amount), 0.00) FROM account_voucher_line avl WHERE avl.voucher_id = av.id
                        AND type='cr') saldo_ant,
                    (SELECT COALESCE(sum(avl.amount), 0.00) FROM account_voucher_line avl WHERE avl.voucher_id = av.id
                        AND type='dr') saldo_ant_dr,
                    0.0 saldo,
                    0 saldo_mxn,
                    0 saldo_usd,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(ai.amount_tax, 0) FROM account_invoice ai
                                                    WHERE ai.number = aml.ref) ELSE 0 END iva,
                    av.type type_invoice,
                    (SELECT rcr.rate
                        FROM res_company rcm
                        INNER JOIN res_currency rc on rcm.currency_id=rc.id
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        WHERE rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_comp,
                    (SELECT rcr.rate
                        FROM res_currency rc
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        where rc.name='USD' AND rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_usd,
                    (SELECT rcr.rate
                        FROM res_currency rc
                        INNER JOIN res_currency_rate rcr on rc.id=rcr.currency_id
                        where rc.name='MXN' AND rcr.name at time zone 'UTC' <=LOCALTIMESTAMP GROUP BY rcr.id
                        ORDER BY rcr.name DESC LIMIT 1) rate_currency_mxn,
                    rp.company_id company,
                    rp.id customer_id,
                    rp.name customer_name,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(apl.days, 0) FROM account_invoice ai
                                                    LEFT JOIN account_payment_term apt on ai.payment_term=apt.id
                                                    LEFT join account_payment_term_line apl on apt.id=apl.payment_id
                                                    WHERE ai.number = aml.ref) ELSE 0  END pay_days,
                    CASE WHEN aml.name = '/' THEN (SELECT coalesce(rrc.name, '') FROM account_invoice ai
                                                    LEFT JOIN res_currency rrc on ai.currency_id=rrc.id
                                                    WHERE ai.number = aml.ref)
                                             ELSE coalesce((SELECT rcc.name FROM account_voucher avv
                                                    LEFT JOIN account_journal aaj on avv.journal_id=aaj.id
                                                    LEFT JOIN res_currency rcc on aaj.currency=rcc.id
                                                    WHERE avv.number = aml.ref), '') END currency_doc,
                    coalesce(rc.name, '') currency_journal,
                    (SELECT rc.name FROM res_company rcm INNER JOIN res_currency rc on rcm.currency_id=rc.id ) currency_comp,
                    avl.type crdr,
                    'pagos_aplicados' filtro
                FROM account_voucher AS av
                INNER JOIN res_partner rp ON av.partner_id=rp.id
                INNER JOIN res_company rcm on rp.company_id=rcm.id
                LEFT JOIN account_journal aj on av.journal_id=aj.id
                LEFT JOIN res_currency rc on aj.currency=rc.id
                left join account_voucher_line avl on av.id=avl.voucher_id
                left join account_move_line aml on avl.move_line_id = aml.id
                WHERE av.state in ('posted') and av.type in ('receipt') AND avl.amount > 0
                AND avl.type in ('dr','cr') AND aml.ref NOT LIKE ('Pago Cancelado%') AND aml.ref NOT LIKE ('%Fac. Cancelada')
                )
                AS T1 ) """)

    def to_csv_by_ids(self, cr, uid, ids, context=None):
        dataset = self.pool['r.customer.statement.data'].browse(cr, uid, ids, context)

        dateset_str = [['Documento', 'Referencia', 'Pedido', 'Estado', 'Fecha emisión', 'Fecha vencimiento',
                        'Días de crédito', 'Moneda', 'Importe', 'IVA', 'Paridad', 'Saldo MXN', 'Saldo USD', ]]

        for row in dataset:
            dateset_str.append([str(row.invoice_number).encode('utf-8'),
                                unicode(row.warehouse).encode('utf-8'),
                                unicode(row.origin).encode('utf-8'),
                                unicode(row.state).encode('utf-8'),
                                unicode(self.utc_mexico_city(row.invoice_date)),
                                unicode(row.invoice_date_due).encode('utf-8'),
                                row.pay_days if row.pay_days else '',
                                unicode(row.currency).encode('utf-8'),
                                row.total,
                                row.iva,
                                row.rate,
                                row.saldo_mxn if row.saldo_mxn else 0,
                                row.saldo_usd if row.saldo_usd else 0,

                                ])

        obj_osv = OCsv()

        file = obj_osv.csv_base64(dateset_str)
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': 'Estado_cuenta_cliente.csv',
            'type_file': 'csv',
            'file': file,
        }, context=None)
