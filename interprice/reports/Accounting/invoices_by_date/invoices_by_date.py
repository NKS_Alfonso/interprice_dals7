# -*- coding: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Saúl Favela'
# __email__ = 'saul.favela@gvadeto.com'

from openerp import fields, models, api, _
from openerp.osv import osv
import openerp.tools as tools
from olib.oCsv.oCsv import OCsv
import datetime
import logging
_logger = logging.getLogger(__name__)

class ReportsAccountingInvoicesByDate(osv.osv_memory):
    _name = 'r.invoices_by_date'

    date_from = fields.Datetime(string='From date')
    date_to = fields.Datetime(string='To date')
    partner_id = fields.Many2one('res.partner', string='Customer', domain=[('customer', '=', True), ('is_company', '=', True)])
    user_id = fields.Many2one('res.users', string='Vendor', domain=[])
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', domain=[])
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  help=_('Select the currency to show all documents'))
    #include = fields.Selection([('0','Todos'),('1', 'Sin Factura'),('2','Facturados')], default='2', string="Documentos a incluir", required=True)
    include_cancel = fields.Boolean(string='Include canceled?', default=False)

    def _get_ids(self, cr, uid, ids, type):
        dataset_ids = []
        data = self.browse(cr, uid, ids, context=None)
        ipartner_id = ''
        iuser_id = ''
        iwarehouse_id = ''
        iinclude = ''
        if data.partner_id:
            ipartner_id = ' and customer_id = %s ' % (data.partner_id.id,)
        if data.user_id:
            iuser_id = ' and sales_name_id = %s ' % (data.user_id.id,)
        if data.warehouse_id:
            iwarehouse_id = ' and warehouse_id = %s ' % (data.warehouse_id.id,)
        _currency = False
        if data.currency_id:
            _currency = data.currency_id
        cr.execute(
            "select id from r_invoices_by_date_data where create_doc IS NOT NULL AND create_doc >='%s'  and"
            " create_doc <= '%s' %s %s %s "
            % (data.date_from, data.date_to, ipartner_id, iuser_id, iwarehouse_id))
        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            ids_to_keep = "("+','.join(str(e) for e in dataset_ids)+")"
            report_id = ids[0]
            if type == 'show':
                cr.execute('delete from r_invoices_by_date where id not in {}'.format(ids_to_keep))
        else:
            data.unlink()
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids, _currency
    

    @api.multi
    def show(self):
        search_view_id = self.env['ir.ui.view'].search([('model', '=', 'invoices_by_date_screen'), ('name', '=', 'r.invoices.by.date.screen.search')]).id
        data_ids, currency = self._get_ids(type='show')
        screen=self.env['invoices_by_date_screen']
        wiz = self.env['r.invoices_by_date'].search([('id','=',self.id)])            
        screen.with_context({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False,'include_cancel':wiz.include_cancel}).show_screen(data_ids)
        return {
            'name': 'Sales invoices by date / customer',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,search',
            'res_model': 'invoices_by_date_screen',
            'search_view_id': search_view_id,
            'target': 'current',
        }
    @api.multi
    def csv(self):
        dataset_ids, currency = self._get_ids(type='csv')
        #context.update({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False,'include_cancel':self.include_cancel})        
        id = self.env['r.invoices.by.date.data'].with_context({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False,'include_cancel':self.include_cancel}).to_csv_by_ids(dataset_ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id.id),
            'target': 'self',
        }


class ReportsAccountingInvoicesByDateData(models.Model):
    _name = 'r.invoices.by.date.data'
    _table = 'r_invoices_by_date_data'
    _auto = False
    _order = 'id desc'

    sale_name = fields.Char(string='Sale order')
    out_name = fields.Char(string='Out name')
    sales_name = fields.Char(string='Vendor')
    create_doc = fields.Datetime(string='Sign date')
    create_date = fields.Datetime(string='Create date')
    type_doc = fields.Char(string='Document type')
    warehouse = fields.Char(string='Warehouse')
    invoice_number = fields.Char(string='Invoice number')
    state = fields.Char(string='State')
    signed = fields.Char(string='Sign state')
    pay_method = fields.Char(string=_('Pay method'))
    currency_copy = fields.Char(string='Currency')
    currency_copy_id = fields.Many2one('res.currency', string = 'Document currency')
    subtotal_copy = fields.Float(string='Subtotal', digits=(10, 2))
    tax_copy = fields.Float(string='Tax', digits=(10, 2))
    total_copy = fields.Float(string='Total', digits=(10, 2))
    customer_name = fields.Char(string='Customer')
    rate = fields.Char(string='Exchange rate')
    cost_copy = fields.Float(string='Cost', digits=(10, 2))
    profit_copy = fields.Float(digits=(10, 2))
    account_invoice_id = fields.Many2one('account.invoice', string='Invoice id')
    currency_company = fields.Integer(string='Company Currency')
    
    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_invoices_by_date_data')
        cr.execute("""
            DROP VIEW IF EXISTS r_invoices_by_date_data;
            create or replace view r_invoices_by_date_data as (
                SELECT
                    row_number() over (order by T1.create_doc, T1.out_name) as id,
                    T1.sale_name,T1.out_name,T1.sales_name_id,T1.sales_name,
                    T1.create_doc as create_doc, T1.create_date,
                    T1.customer_id,T1.customer_name,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN 'FAC'
                        WHEN 'out_refund' THEN 'NCC'
                        ELSE '' END type_doc,
                    T1.warehouse_id,T1.warehouse, T1.invoice_number,
                    CASE T1.invoice_state
                        WHEN 'open' THEN 'Abierto'
                        WHEN 'cancel' THEN 'Cancelado'
                        WHEN 'draft' THEN 'Borrador'
                        WHEN 'paid' THEN 'Pagado'
                        WHEN 'done' THEN 'Completado'
                        WHEN 'waiting_date' THEN 'Esperando fecha planificada'
                        WHEN 'progress' THEN 'Pedidos de venta'
                        WHEN 'manual' THEN 'Pedido a facturar'
                        WHEN 'shipping_except' THEN 'Excepción de envío'
                        WHEN 'invoice_except' THEN 'Excepción de factura'
                        WHEN 'wait_risk' THEN 'Waiting risk approval'
                        ELSE '' END state,
                    CASE T1.signed
                        WHEN 'not_signed' THEN 'SIN FIRMAR'
                        WHEN 'signed' THEN 'FIRMADA'
                        WHEN 'cancelled' THEN 'CANCELADA'
                        END signed,
                    T1.pay_method,
                    T1.currency as currency_copy,
                    T1.currency_id as currency_copy_id,--currency,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN T1.subtotal
                        WHEN 'out_refund' THEN -(T1.subtotal)
                        END as subtotal_copy, --subtotal,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN T1.tax
                        WHEN 'out_refund' THEN -(T1.tax)
                        END as tax_copy,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN T1.total
                        WHEN 'out_refund' THEN -(T1.total)
                        END as total_copy, --total,
                    CASE T1.invoice_type
                        WHEN 'out_invoice' THEN round(cast(T1.cost as NUMERIC) , 2)
                        WHEN 'out_refund' THEN -( round(cast(T1.cost as NUMERIC) , 2))
                        END as cost_copy,
                    T1.rate ,
                    CASE
                        WHEN T1.invoice_type  = 'out_invoice' THEN ((T1.total-T1.tax) - T1.cost)
                        WHEN T1.invoice_type  = 'out_refund' THEN (-(T1.total-T1.tax) ) - (-T1.cost)
                        END as profit_copy,
                    CASE
                        WHEN T1.invoice_type = 'out_invoice' and T1.total > 0 THEN
                        ((T1.total-T1.tax) - T1.cost  ) * 100 / (T1.total-T1.tax)
                        WHEN T1.invoice_type = 'out_refund' and T1.total > 0 THEN
                        (((-(T1.total-T1.tax)) - (-T1.cost)) * 100 / (T1.total-T1.tax))
                        ELSE 0
                        END as profit_percent,
                    T1.account_invoice_id,
                    T1.currency_company,
                    NULL as currency,
                    NULL as subtotal,
                    NULL as tax,
                    NULL as total,
                    NULL as cost,
                    NULL as profit
                FROM (
                    SELECT
                        ai.invoice_datetime as create_doc, ai.create_date, ai.state_document as signed, sw.id as warehouse_id,
                        CASE
                            WHEN so.name IS NULL THEN '' ELSE sw.name
                            END as warehouse,
                        rp.id customer_id, rp.name customer_name,
                        rc.name as currency, ai.currency_id as currency_id,
                        COALESCE(so.name,'') as sale_name,
                        COALESCE(sp.name,'') as out_name,
                        ai.user_id as sales_name_id,
                        CASE
                            WHEN so.name IS NULL OR ai.type='out_refund' THEN (SELECT rp.name FROM res_partner as rp
                                WHERE rp.id=(SELECT ru.partner_id from res_users as ru WHERE ru.id=ai.user_id))
                            ELSE rpu.name
                            END as sales_name,
                        CASE
                            WHEN ai.number IS NULL THEN ''
                            ELSE ai.number
                            END AS invoice_number,
                        ai.state AS invoice_state,
                        (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                              INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                            WHERE aipmr.account_invoice_id = ai.id
                        )as pay_method,
                        ai.type AS invoice_type,
                        ai.amount_untaxed as subtotal, ai.amount_tax as tax, ai.amount_total as total, 
                        COALESCE((
                            SELECT sum((
                                         SELECT
                                           round(CAST (COALESCE(pph.cost,0) AS NUMERIC),2) * ail.quantity costo_producto
                                         FROM
                                           product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                                         WHERE pp.id=ail.product_id AND pph.create_date <=ai.invoice_datetime ORDER BY pph.create_date desc LIMIT 1
                                       ))
                            FROM account_invoice_line ail
                            WHERE ail.invoice_id =ai.id
                        ),0.000000) as cost,
                        CAST(doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS TEXT) AS rate,
                        COALESCE(ai.id,0) account_invoice_id,
                        rcom.currency_id as currency_company
                    FROM
                        account_invoice ai
                    LEFT JOIN
                        (SELECT
                            min(order_id) as order_id,
                            invoice_id
                        FROM
                            sale_order_invoice_rel
                        GROUP BY
                            invoice_id) as soi on ai.id = soi.invoice_id
                    LEFT JOIN
                        sale_order so on soi.order_id = so.id
                    LEFT JOIN
                        stock_picking sp ON so.name = sp.origin and sp.name in (ai.origin)
                    LEFT JOIN
                        stock_warehouse sw on so.warehouse_id = sw.id
                    INNER JOIN
                        res_partner rp on ai.partner_id = rp.id
                    INNER JOIN
                        res_currency rc on ai.currency_id = rc.id
                    INNER JOIN
                        res_company rcom on ai.company_id = rcom.id
                    INNER JOIN
                        res_currency rcc on rcom.currency_id = rcc.id
                    LEFT JOIN
                        res_users ru on so.user_id = ru.id
                    LEFT JOIN
                        res_partner rpu on ru.partner_id = rpu.id
                    WHERE ai.type IN ('out_invoice')
                    
                    UNION ALL

                    SELECT
                        ai.invoice_datetime as create_doc, ai.create_date, ai.state_document as signed, sw.id as warehouse_id,
                        CASE
                            WHEN so.name IS NULL THEN '' ELSE sw.name
                            END as warehouse,
                        rp.id customer_id, rp.name customer_name,
                        rc.name as currency, ai.currency_id as currency_id,
                        CASE
                            WHEN ai.type='out_invoice' THEN (SELECT s2o.name FROM sale_order_invoice_rel s2oi INNER JOIN sale_order s2o on s2o.id=s2oi.order_id where s2oi.invoice_id=53 order by s2oi.order_id limit 1)
                            ELSE so.name
                            END as sale_name,
                        CASE
                            WHEN ai.type = 'out_refund' AND ai.picking_dev_id is not NULL THEN (SELECT sp.name FROM stock_picking as sp WHERE id=ai.picking_dev_id)
                            ELSE ''
                            END as out_name,
                        ai.user_id as sales_name_id,
                        CASE
                            WHEN so.name IS NULL OR ai.type='out_refund' THEN (SELECT rp.name FROM res_partner as rp
                                WHERE rp.id=(SELECT ru.partner_id from res_users as ru WHERE ru.id=ai.user_id))
                            ELSE rpu.name
                            END as sales_name,
                        CASE
                            WHEN ai.number IS NULL THEN ''
                            ELSE ai.number
                            END AS invoice_number,
                        ai.state AS invoice_state,
                        (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                              INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                            WHERE aipmr.account_invoice_id = ai.id
                        )as pay_method,
                        ai.type AS invoice_type,
                        ai.amount_untaxed as subtotal, ai.amount_tax as tax, ai.amount_total as total, 
                        (
                            SELECT sum((
                                         SELECT
                                           round(CAST (COALESCE(pph.cost,0) AS NUMERIC),2) * ail.quantity costo_producto
                                         FROM
                                           product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                                         WHERE pp.id=ail.product_id AND pph.create_date <=ai.invoice_datetime ORDER BY pph.create_date desc LIMIT 1
                                       ))
                            FROM account_invoice_line ail
                            WHERE ail.invoice_id =ai.id
                        ) as cost,
                        CAST(doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS TEXT) AS rate,
                        COALESCE(ai.id,0) account_invoice_id,
                        rcom.currency_id as currency_company
                    FROM
                        account_invoice ai
                    LEFT JOIN
                        stock_picking spd ON spd.id=ai.picking_dev_id and ai.type='out_refund'
                    LEFT JOIN
                        stock_picking sp ON sp.name=spd.origin
                    LEFT JOIN
                        sale_order so on so.name = sp.origin
                    LEFT JOIN
                        stock_warehouse sw on so.warehouse_id = sw.id
                    INNER JOIN
                        res_partner rp on ai.partner_id =rp.id
                    INNER JOIN
                        res_currency rc on ai.currency_id=rc.id
                    INNER JOIN
                        res_company rcom on rcom.id= ai.company_id
                    INNER JOIN
                        res_currency rcc on rcc.id= rcom.currency_id
                    LEFT JOIN
                        res_users ru on so.user_id=ru.id
                    LEFT JOIN
                        res_partner rpu on ru.partner_id =rpu.id
                    WHERE ai.type IN ('out_refund')
                )AS T1
                /*WHERE
                    T1.invoice_state != 'draft'*/
            )
        """)
    
    def convert_date_timezone(self, date):
        from datetime import datetime as dt
        from dateutil import tz
        if date:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz(self.env.user.partner_id.tz)
            if to_zone == False:
                to_zone == tz.gettz('America/Mexico_City')
            try:
                utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S')
            except Exception:
                utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            utc = utc.replace(tzinfo=from_zone)
            central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
            return central
        else:
            return ''
    
    
    @api.multi
    def to_csv_by_ids(self,ids):
        cancel = self._context['include_cancel']
        include_cancel = "" if cancel else " AND rssd.state != 'Cancelado' "
        ccontext = self._context['currency2convertname']
        ccost=ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        dataset_csv = [
            [
                unicode(_('Invoice number')).encode('utf8'),
                unicode(_('Sale order')).encode('utf8'),
                unicode(_('Create date')).encode('utf8'),
                unicode(_('Invoice date')).encode('utf8'),
                unicode(_('Vendor')).encode('utf8'),
                unicode(_('Customer')).encode('utf8'),
                unicode(_('Document type')).encode('utf8'),
                unicode(_('Warehouse')).encode('utf8'),
                unicode(_('Out name')).encode('utf8'),
                unicode(_('State')).encode('utf8'),
                unicode(_('Sign state')).encode('utf8'),
                unicode(_('Pay method')).encode('utf8'),
                unicode(_('Document currency')).encode('utf8'),
                unicode(_('Subtotal')).encode('utf8'),
                unicode(_('Tax')).encode('utf8'),
                unicode(_('Total')).encode('utf8'),
                unicode(_('Cost')).encode('utf8'),
                unicode(_('Profit')).encode('utf8'),
                unicode(_('Profit percent')).encode('utf8'),
                unicode(_('Exchange rate')).encode('utf8'),
                unicode(_('Report currency')).encode('utf8'),
            ]
        ]
        query = """
            DROP TABLE IF EXISTS
                csv_invoices_by_date;
            CREATE TEMPORARY TABLE
                csv_invoices_by_date (
                    id integer,
                    sale_name CHARACTER VARYING,
                    create_doc timestamp without time zone,
                    create_date timestamp without time zone,
                    sales_name CHARACTER VARYING,
                    customer_name CHARACTER VARYING,
                    type_doc CHARACTER VARYING,
                    warehouse CHARACTER VARYING,
                    out_name CHARACTER VARYING,
                    invoice_number CHARACTER VARYING,
                    state CHARACTER VARYING,
                    signed CHARACTER VARYING,
                    pay_method CHARACTER VARYING, 
                    currency_copy CHARACTER VARYING,
                    subtotal numeric,
                    tax numeric,
                    total numeric,
                    "cost" numeric,
                    profit numeric,
                    profit_percent numeric,
                    rate CHARACTER VARYING
                );
            INSERT INTO
                csv_invoices_by_date
            SELECT
                rssd.id,
                rssd.sale_name,
                rssd.create_doc,
                rssd.create_date,
                rssd.sales_name,
                rssd.customer_name,
                rssd.type_doc,
                rssd.warehouse,
                rssd.out_name,
                rssd.invoice_number,
                rssd.state,
                rssd.signed,
                rssd.pay_method,
                rssd.currency_copy,
                CASE
                    WHEN rssd.subtotal_copy NOTNULL
                        THEN currency_convert(rssd.currency_copy,{0},rssd.subtotal_copy,rssd.create_doc)
                        ELSE NULL
                    END as subtotal,
                CASE
                    WHEN rssd.tax_copy NOTNULL
                        THEN currency_convert(rssd.currency_copy,{0},rssd.tax_copy,rssd.create_doc)
                        ELSE NULL
                    END,
                CASE
                    WHEN rssd.total_copy NOTNULL
                        THEN currency_convert(rssd.currency_copy,{0},rssd.total_copy,rssd.create_doc)
                        ELSE NULL
                    END,
                CASE
                    WHEN rssd.cost_copy NOTNULL
                        THEN currency_convert(rc.name,{2},rssd.cost_copy,rssd.create_doc)
                        ELSE NULL
                    END as cost,
                NULL,
                NULL,
                CAST(currency_convert(rssd.currency_copy,{0},1,rssd.create_doc) AS TEXT) AS rate
                --rssd.rate
            FROM
                r_invoices_by_date_data rssd
            LEFT OUTER JOIN
                res_currency rc on rc.id=rssd.currency_company
            WHERE
                rssd.id in {1} {3};
            UPDATE
                csv_invoices_by_date
            SET
                profit = round(abs(subtotal)-abs("cost"),2)
            WHERE
                type_doc in ('FAC','NCC')
                AND
                subtotal NOTNULL;
            UPDATE
                csv_invoices_by_date
            SET
                profit_percent = round(profit * 100 / abs(subtotal),2)
            WHERE
                profit>0
                AND
                profit NOTNULL;
            SELECT
                *
            FROM
                csv_invoices_by_date
            ORDER BY
                id DESC;
        """.format(ccontext,data_ids,ccost,include_cancel)
        self.env.cr.execute(query)
        dataset = self.env.cr.dictfetchall()
        sum_subtotal = 0.00
        sum_total = 0.00
        sum_tax = 0.00
        sum_residual = 0.00
        for row in dataset:
            if row['state'] != 'Cancelado' and row['subtotal']!=None:
                sum_subtotal += row['subtotal']
                sum_tax += row['tax'] 
                sum_total += row['total']
            dataset_csv.append([
                unicode(self.set_default(row['invoice_number'])).encode('utf8'),
                unicode(self.set_default(row['sale_name'])).encode('utf8'),
                unicode(self.convert_date_timezone(row['create_date'])).encode('utf8'),
                unicode(self.convert_date_timezone(row['create_doc'])).encode('utf8'),
                unicode(self.set_default(row['sales_name'])).encode('utf8'),
                unicode(self.set_default(row['customer_name'])).encode('utf8'),
                unicode(self.set_default(row['type_doc'])).encode('utf8'),
                unicode(self.set_default(row['warehouse'])).encode('utf8'),
                unicode(self.set_default(row['out_name'])).encode('utf8'),
                unicode(self.set_default(row['state'])).encode('utf8'),
                unicode(self.set_default(row['signed'])).encode('utf8'),
                unicode(self.set_default(row['pay_method'])).encode('utf8'),
                unicode(self.set_default(row['currency_copy'])).encode('utf8'),
                unicode(self.set_default(row['subtotal'], 0)).encode('utf8'),
                unicode(self.set_default(row['tax'], 0)).encode('utf8'),
                unicode(self.set_default(row['total'], 0)).encode('utf8'),
                unicode(self.set_default(row['cost'], 0)).encode('utf8'),
                unicode(self.set_default(row['profit'], 0)).encode('utf8'),
                unicode(self.set_default(row['profit_percent'], 0)).encode('utf8'),
                unicode(self.set_default(row['rate'], 0)).encode('utf8'),
                unicode(self.set_default(ccontext.replace("'",'') if self._context['currency2convertname'] else row['currency_copy'])).encode('utf8')
            ])
        dataset_csv.append(['', '', '', '', '', '', '', '', '', '', '', '', '', round(sum_subtotal,2), round(sum_tax,2), round(sum_total,2), '', ])
        obj_ocsv = OCsv()
        file = obj_ocsv.csv_base64(dataset_csv)
        return self.env['r.download'].create(vals={
            'file_name': 'Invoices_by_date.csv',
            'type_file': 'csv',
            'file': file,
        })

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
class ReportsAccountingInvoicesByDateScreen(osv.osv_memory):
    _name = 'invoices_by_date_screen'
    
    sale_name = fields.Char(string='Sale order')
    out_name = fields.Char(string='Out name')
    sales_name = fields.Char(string='Vendor')
    create_doc = fields.Datetime(string='Invoice date')
    create_date = fields.Datetime(string='Create date')
    type_doc = fields.Char(string='Document type')
    warehouse = fields.Char(string='Warehouse')
    invoice_number = fields.Char(string='Invoice number')
    state = fields.Char(string='State')
    signed = fields.Char(string='Sign state')
    pay_method = fields.Char(string=_('Pay method'))
    currency_copy = fields.Char(string='Currency')
    currency_copy_id = fields.Many2one('res.currency', string = 'Document currency')
    currency = fields.Char(string='Document currency')
    subtotal_copy = fields.Float(string='Subtotal', digits=(10, 2))
    subtotal = fields.Float(string='Subtotal', digits=(10, 2))
    tax_copy = fields.Float(string='Tax', digits=(10, 2))
    tax = fields.Float(string='Tax',digits=(10, 2))
    total_copy = fields.Float(string='Total', digits=(10, 2))
    total = fields.Float(string='Total', digits=(10, 2))
    customer_name = fields.Char(string='Customer')
    rate = fields.Char(string='Exchange rate')
    cost = fields.Float(string='Cost', digits=(10, 2))
    cost_copy = fields.Float(string='Cost', digits=(10, 2))
    profit = fields.Float(string='Profit', digits=(10, 2))
    profit_copy = fields.Float(digits=(10, 2))
    profit_percent = fields.Float(string='Profit percent', digits=(10, 2))
    account_invoice_id = fields.Many2one('account.invoice', string='Account invoice')
    currency_company = fields.Integer(string='Company Currency')
    currency_show = fields.Char(string="Report currency")
    
    @api.multi
    def show_screen(self,ids):
        ccontext = self._context['currency2convertname']
        cancel = self._context['include_cancel']
        include_cancel = "" if cancel else " AND rssd.state != 'Cancelado' "
        ccost=ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        currency_show = ccontext if self._context['currency2convertname'] else 'rssd.currency_copy'
        query = """
            TRUNCATE TABLE
                invoices_by_date_screen;
            INSERT INTO
                invoices_by_date_screen (
                    id,
                    sale_name,
                    create_doc,
                    create_date,
                    sales_name,
                    customer_name,
                    type_doc,
                    warehouse,
                    out_name,
                    invoice_number,
                    state,
                    signed,
                    pay_method,
                    currency_copy,
                    currency,
                    subtotal,
                    tax,
                    total,
                    "cost",
                    profit,
                    profit_percent,
                    rate,
                    currency_show,
                    create_uid,
                    write_uid
                ) 
                SELECT
                    rssd.id,
                    rssd.sale_name,
                    rssd.create_doc,
                    rssd.create_date,
                    rssd.sales_name,
                    rssd.customer_name,
                    rssd.type_doc,
                    rssd.warehouse,
                    rssd.out_name,
                    rssd.invoice_number,
                    rssd.state,
                    rssd.signed,
                    rssd.pay_method,
                    rssd.currency_copy,
                    rssd.currency_copy as currency,
                    CASE
                        WHEN rssd.subtotal_copy NOTNULL
                            THEN currency_convert(rssd.currency_copy,{0},rssd.subtotal_copy,rssd.create_doc)
                            ELSE NULL
                        END as subtotal,
                    CASE
                        WHEN rssd.tax_copy NOTNULL
                            THEN currency_convert(rssd.currency_copy,{0},rssd.tax_copy,rssd.create_doc)
                            ELSE NULL
                        END,
                    CASE
                        WHEN rssd.total_copy NOTNULL
                            THEN currency_convert(rssd.currency_copy,{0},rssd.total_copy,rssd.create_doc)
                            ELSE NULL
                        END,
                    CASE
                        WHEN rssd.cost_copy NOTNULL
                            THEN currency_convert(rc.name,{2},rssd.cost_copy,rssd.create_doc)
                            ELSE NULL
                        END as cost,
                    NULL,
                    NULL,
                    CAST(currency_convert(rc.name,{2},1,rssd.create_doc) AS TEXT) AS rate,
                    --rssd.rate
                    {3} as currency_show,
                    {5},
                    {5}
                FROM
                    r_invoices_by_date_data rssd
                LEFT OUTER JOIN
                    res_currency rc ON rc.id=rssd.currency_company
                WHERE
                    rssd.id in {1} {4};
            UPDATE
                invoices_by_date_screen
            SET
                profit = round(abs(subtotal)-abs("cost"),2)
            WHERE
                type_doc in ('FAC','NCC')
                AND
                subtotal NOTNULL;
            UPDATE
                invoices_by_date_screen
            SET
                profit_percent = round((profit * 100) / abs(subtotal),2)
            WHERE
                profit > 0
                AND
                profit NOTNULL;
        """.format(ccontext,data_ids,ccost,currency_show,include_cancel,self.env.uid)
        self.env.cr.execute(query)
        return True