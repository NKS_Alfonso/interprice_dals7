# -*- coding: utf-8 -*-
# @version     : 1.0
# @autor       : Viridiana Cruz Santos

from openerp import fields, models, api, _
from openerp.osv import osv, fields as fieldsv7
import openerp.tools as tools
from datetime import datetime
from olib.oCsv.oCsv import OCsv


class ReportsResidueOfCustomer(osv.osv_memory):
    _name = 'r.residue_of_customer'

    date_to = fields.Datetime(string='A')
    partner_id = fields.Many2one('res.partner', string='Cliente', domain=[('customer', '=', True)])
    filter_by = fields.Selection([('a', 'Facturas'),
                                  ('b', 'Notas de crédito'),
                                  ('c', 'Pagos')],
                                 'Filtrar', required=False)

    def _get_ids(self, cr, uid, ids, type):
        dataset_ids = []
        data = self.browse(cr, uid, ids, context=None)
        ipartner_id = ''
        if data.partner_id:
            ipartner_id = "and customer_id = %s " % (data.partner_id.id,)

        if data.partner_id and data.filter_by == 'a':
            ipartner_id = "and customer_id = %s and type_doc = 'Factura' " % (data.partner_id.id,)
        if data.partner_id and data.filter_by == 'b':
            ipartner_id = "and customer_id = %s and type_doc = 'Nota de credito' " % (data.partner_id.id,)
        if data.partner_id and data.filter_by == 'c':
            ipartner_id = "and customer_id = %s and type_doc = 'Anticipo' " % (data.partner_id.id,)

        if not data.partner_id and data.filter_by == 'a':
            ipartner_id = "and type_doc = 'Factura' " % ()
        if not data.partner_id and data.filter_by == 'b':
            ipartner_id = "and type_doc = 'Nota de credito' " % ()
        if not data.partner_id and data.filter_by == 'c':
            ipartner_id = "and type_doc = 'Anticipo' " % ()

        cr.execute(" SELECT id FROM r_residue_of_customer_data WHERE create_doc_utc <= '%s' %s "
                   % (data.date_to, ipartner_id,))

        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            report_id = ids[0]
            if type == 'show':
                cr.execute('DELETE FROM r_residue_of_customer WHERE id=%s', (report_id,))
        else:
            data.unlink()
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids

    def show(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=None)
        data_ids = self._get_ids(cr, uid, ids, type='show')
        context.update({"fecha_filtro": data.date_to})
        dataset = self.pool['r.residue.of.customer.data'].browse(cr, uid, data_ids, context=context)
        ids = []
        for row in dataset:
            if round(row.saldo, 2) != 0:
                ids.append(row.id)
        return {
            'name': 'Reporte',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.residue.of.customer.data',
            'target': 'current',
            'context': '{"fecha_filtro":"%s"}' % (data.date_to),
            'domain': [('id', 'in', ids)],
        }

    def csv(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=None)
        dataset_ids = self._get_ids(cr, uid, ids, type='csv')
        context['fecha_filtro'] = data.date_to
        id = self.pool['r.residue.of.customer.data'].to_csv_by_ids(cr, uid, dataset_ids, context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id),
            'target': 'self',
            'context': '{"fecha_filtro":"%s"}' % (data.date_to),
        }


class ReportsResidueResidueOfCustomer(models.Model):
    _name = 'r.residue.of.customer.data'
    _table = 'r_residue_of_customer_data'
    _auto = False
    _order = 'id desc'

    customer_name = fields.Char(string='Cliente')
    invoice_number = fields.Char(string='Folio')
    invoice_date = fields.Datetime(string='Fecha emisión')
    invoice_date_due = fields.Date(string='Fecha vencimiento')
    type_doc = fields.Char(string='Tipo documento')
    state = fields.Char(string='Estado')
    dias_atrasados = fields.Char(compute='_dif_dias', string='Días atrasados')
    amount_total = fields.Float(string='Importe')
    saldo = fields.Float(compute='_sum_pays', string='Saldo', digits=(10, 2))
    suma_pagos = fields.Float(compute='_sum_current', string='Current')
    days_due_01to30 = fields.Float(compute='_sum_current', string='De 1 a 30 días')
    days_due_31to60 = fields.Float(compute='_sum_current', string='De 31 a 60 días')
    days_due_61to90 = fields.Float(compute='_sum_current', string='De 61 a 90 días')
    days_due_91to120 = fields.Float(compute='_sum_current', string='De 91 a 120 días')
    days_due_121togr = fields.Float(compute='_sum_current', string='De 121 o más días')
    invoice_currency = fields.Char(string='Moneda')
    invoice_id = fields.Many2one('account.invoice', string='ID factura')

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    @api.multi
    def _dif_dias(self):
        date_to = self._context['fecha_filtro']
        date_to_utc = self.utc_mexico_city(date_to)
        for rec in self:
            if rec.type_doc in ('Factura', 'Nota de credito'):
                date_to_date = datetime.strptime(str(date_to_utc), "%Y-%m-%d %H:%M:%S")
                date_due = datetime.strptime(str(rec.invoice_date_due), "%Y-%m-%d")
                delta = date_due - date_to_date
                delta_symb = delta * -1
                if delta_symb.days < 0:
                    rec.dias_atrasados = 0
                else:
                    rec.dias_atrasados = delta_symb.days
            if rec.type_doc in ('Anticipo'):
                rec.dias_atrasados = 0

    @api.multi
    def _sum_pays(self):
        date_to = self._context['fecha_filtro']
        date_to_utc = self.utc_mexico_city(date_to)

        for reco in self:
            done_payment = 0
            amount_currency = 0
            amount_currency_change = 0
            rate_pay_apply = 0
            payment_name = ''
            invoice_obj = reco.invoice_id #self.env['account.invoice'].search([('id', '=', )])

            if invoice_obj.payment_ids:
                for payment_id in invoice_obj.payment_ids:  # For each payment on the invoice.
                    if date_to_utc >= payment_id.date:  # If filter date is higher or equal payment date
                        if invoice_obj.currency_id.id != invoice_obj.company_id.currency_id.id:  # Invoice USD currency
                            if invoice_obj.currency_id.id != invoice_obj.journal_id.currency.id:  # Invoice USD currency - Journal MXN currency
                                if payment_id.journal_id.currency.id == invoice_obj.currency_id.id:  # USD payment - invoice USD
                                    if payment_id.name == '/':  # Apply payment
                                        amount_currency += payment_id.amount_currency  # Sum divisa payment USD

                                elif payment_id.journal_id.currency.id != invoice_obj.currency_id.id:  # MXN payment - invoice USD
                                    if payment_id.name == '/':  # Apply payment
                                        amount_currency += payment_id.amount_currency  # Sum divisa payment MXN
                                        rate_pay_apply = (payment_id.debit - payment_id.credit) / payment_id.amount_currency  # Rate apply in payment
                                        payment_name = payment_id.name
                                    if payment_name != payment_id.name and not payment_id.name in ('Desajuste', 'Write-Off'):
                                        if payment_id.amount_currency == 0:
                                            amount_currency_change += (payment_id.debit - payment_id.credit) / rate_pay_apply  # Sum changes USD
                                        else:
                                            amount_currency_change += payment_id.amount_currency
                            else:  # Invoice USD currency - Journal USD currency
                                if payment_id.name == '/':
                                    amount_currency += payment_id.amount_currency

                            done_payment = amount_currency + amount_currency_change

                        elif invoice_obj.currency_id.id == invoice_obj.company_id.currency_id.id:  # Invoice MXN currency
                            if payment_id.name == '/':
                                done_payment += payment_id.debit - payment_id.credit

            if reco.type_doc == 'Factura':  # Document type invoice
                saldo = invoice_obj.amount_total - abs(done_payment)
                reco.saldo = saldo

            if reco.type_doc == 'Nota de credito':  # Document type refund
                saldo = invoice_obj.amount_total - abs(done_payment)
                reco.saldo = -(saldo)

    # @api.multi
    # def _sum_pays(self):
    #     date_to = self._context['fecha_filtro']
    #     date_to_utc = self.utc_mexico_city(date_to)
    #     for reco in self:
    #         date_to_date = datetime.strptime(str(date_to_utc), "%Y-%m-%d %H:%M:%S")
    #         if reco.type_doc in ('Factura', 'Nota de credito'):
    #             self.env.cr.execute(""" SELECT COALESCE(sum(avl.amount),0)
    #                                     FROM account_voucher av
    #                                     INNER JOIN  account_voucher_line avl on av.id=avl.voucher_id
    #                                     INNER JOIN account_move_line aml on avl.move_line_id=aml.id
    #                                     INNER JOIN account_invoice aii on aml.move_id=aii.move_id
    #                                     WHERE aii.number = %s and av.date <= %s and av.move_id is not null """,
    #                                 (reco.invoice_number, date_to_date,))
    #             if self.env.cr.rowcount:
    #                 anticipos = self.env.cr.fetchone()[0]
    #                 if reco.type_doc == 'Factura':
    #                     reco.saldo = reco.amount_total - anticipos
    #                 if reco.type_doc == 'Nota de credito':
    #                     neg_saldo = abs(reco.amount_total) - anticipos
    #                     reco.saldo = -(neg_saldo)
    #
    #         if reco.type_doc in ('Anticipo'):
    #             self.env.cr.execute(""" SELECT COALESCE(sum(avl.amount),0)
    #                                                 FROM account_voucher_line avl
    #                                                 INNER join account_voucher av on avl.voucher_id = av.id
    #                                                 WHERE av.number =%s AND avl.type='cr' AND av.date <=%s
    #                                                 AND av.state='posted' """,
    #                                 (reco.invoice_number, date_to_date,))
    #             pagos_cr = self.env.cr.fetchone()[0]
    #
    #             self.env.cr.execute(""" SELECT COALESCE(sum(avl.amount),0)
    #                                                 FROM account_voucher_line avl
    #                                                 INNER join account_voucher av on avl.voucher_id = av.id
    #                                                 INNER JOIN account_move_line aml ON avl.move_line_id = aml.id
    #                                                 AND avl.type = 'dr' AND aml.name = %s WHERE av.date <= %s
    #                                                 AND av.state='posted' """,
    #                                 (reco.invoice_number, date_to_date,))
    #             pagos_dr = self.env.cr.fetchone()[0]
    #             sum_pagos = pagos_cr + pagos_dr
    #             saldo = abs(reco.amount_total) - sum_pagos
    #             reco.saldo = -(saldo)

    @api.multi
    def _sum_current(self):
        for reco in self:
            reco.suma_pagos = reco.saldo
            if int(reco.dias_atrasados) >= 1 and int(reco.dias_atrasados) <= 30:
                reco.days_due_01to30 = reco.suma_pagos
                reco.suma_pagos = 0.00
            else:
                reco.days_due_01to30 = 0.00
            if int(reco.dias_atrasados) >= 31 and int(reco.dias_atrasados) <= 60:
                reco.days_due_31to60 = reco.suma_pagos
                reco.suma_pagos = 0.00
            else:
                reco.days_due_31to60 = 0.00
            if int(reco.dias_atrasados) >= 61 and int(reco.dias_atrasados) <= 90:
                reco.days_due_61to90 = reco.suma_pagos
                reco.suma_pagos = 0.00
            else:
                reco.days_due_61to90 = 0.00
            if int(reco.dias_atrasados) >= 91 and int(reco.dias_atrasados) <= 120:
                reco.days_due_91to120 = reco.suma_pagos
                reco.suma_pagos = 0.00
            else:
                reco.days_due_91to120 = 0.00
            if int(reco.dias_atrasados) >= 121:
                reco.days_due_121togr = reco.suma_pagos
                reco.suma_pagos = 0.00
            else:
                reco.days_due_121togr = 0.00

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_residue_of_customer_data')
        cr.execute("""
            create or replace view r_residue_of_customer_data as (
            SELECT
                row_number() over (order by T1.invoice_date) as id,
                T1.customer_id,
                T1.customer_name,
                T1.invoice_number,
                T1.invoice_date,
                T1.invoice_date_due,
                T1.invoice_date as create_doc_utc,
                T1.type_invoice,
                CASE T1.type_invoice WHEN 'out_invoice' THEN 'Factura'
                   WHEN 'out_refund' THEN 'Nota de credito'
                   WHEN 'receipt' THEN 'Anticipo'
                   ELSE '' END type_doc,
                T1.dias_atrasados,
                CASE T1.type_invoice WHEN 'out_invoice' THEN T1.amount_total
                   WHEN 'out_refund' THEN -(T1.amount_total)
                   WHEN 'receipt' THEN -(T1.amount_total) END amount_total,
                CASE T1.state
                   WHEN 'open' THEN 'Abierto'
                   WHEN 'paid' THEN 'Pagado'
                   WHEN 'posted' THEN 'Contabilizado'
                   WHEN 'proforma' THEN 'Pro-forma'
                   ELSE '' END state,
                T1.saldo,
                T1.invoice_currency,
                T1.suma_pagos,
                T1.days_due_01to30,
                T1.days_due_31to60,
                T1.days_due_61to90,
                T1.days_due_91to120,
                T1.days_due_121togr,
                T1.invoice_id
            FROM
             (
             SELECT
                 ai.id invoice_id,
                 COALESCE(ai.number,'') invoice_number,
                 ai.invoice_datetime invoice_date,
                 ai.type type_invoice,
                 ai.date_due invoice_date_due,
                 ai.amount_total amount_total,
                 ai.state state,
                 0.00 saldo,
                 rc.name invoice_currency,
                 rp.id customer_id,
                 rp.name customer_name,
                 0.00 dias_atrasados,
                 0.00 suma_pagos,
                 0.00 days_due_01to30,
                 0.00 days_due_31to60,
                 0.00 days_due_61to90,
                 0.00 days_due_91to120,
                 0.00 days_due_121togr
             FROM account_invoice ai
                INNER JOIN res_partner rp on ai.partner_id =rp.id
                INNER JOIN res_currency rc on ai.currency_id=rc.id
                WHERE ai.type IN ('out_invoice','out_refund') AND ai.state not in ('cancel','draft')

             ) AS T1) """)


             # UNION ALL
             #
             #  SELECT
             #      av.id invoice_id,
             #      COALESCE(av.number,'') invoice_number,
             #      av.date at time zone 'utc' invoice_date,
             #      av.type type_invoice,
             #      null invoice_date_due,
             #      av.net_amount amount_total,
             #      av.state state,
             #      0.00 saldo,
             #      rc.name invoice_currency,
             #      rp.id customer_id,
             #      rp.name customer_name,
             #      0.00 dias_atrasados,
             #      0.00 suma_pagos,
             #      0.00 days_due_01to30,
             #      0.00 days_due_31to60,
             #      0.00 days_due_61to90,
             #      0.00 days_due_91to120,
             #      0.00 days_due_121togr
             #      FROM account_voucher av
             #      INNER JOIN res_partner rp on av.partner_id=rp.id
             #      INNER JOIN res_currency rc on av.payment_rate_currency_id=rc.id
             #      where av.type in ('receipt') and av.net_amount > 0 AND av.state not in ('cancel','draft')

    def to_csv_by_ids(self, cr, uid, ids, context=None):
        dataset = self.pool['r.residue.of.customer.data'].browse(cr, uid, ids, context)
        dateset_str = [['Cliente', 'Días atrasados', 'Folio', 'Fecha emisión', 'Fecha vencimiento',
                        'Tipo de documento', 'Estado', 'Importe', 'Saldo', 'Moneda', 'Current',
                        'De 1 a 30 días', 'De 31 a 60 días', 'De 61 a 90 días ', 'De 91 a 120 días',
                        'De 121 o más días']]
        """sum_amount_total = 0.00
        sum_saldo = 0.00
        sum_suma_pagos = 0.00
        sum_days_due_01to30 = 0.00
        sum_days_due_31to60 = 0.00
        sum_days_due_61to90 = 0.00
        sum_days_due_91to120 = 0.00
        sum_days_due_121togr = 0.00 """
        balance_with_currency = {}
        for row in dataset:
            if round(row.saldo, 2) != 0:
                """sum_amount_total += row.amount_total
                sum_saldo += row.saldo
                sum_suma_pagos += row.suma_pagos
                sum_days_due_01to30 += row.days_due_01to30
                sum_days_due_31to60 += row.days_due_31to60
                sum_days_due_61to90 += row.days_due_61to90
                sum_days_due_91to120 += row.days_due_91to120
                sum_days_due_121togr += row.days_due_121togr"""
                if row.invoice_currency in balance_with_currency:
                    _row_currency = balance_with_currency[row.invoice_currency]
                    balance_with_currency[row.invoice_currency].update(
                        {
                            'amount_total': _row_currency['amount_total'] + row.amount_total,
                            'saldo': _row_currency['saldo'] + row.saldo,
                            'suma_pagos': _row_currency['suma_pagos'] + row.suma_pagos,
                            'days_due_01to30': _row_currency['days_due_01to30'] + row.days_due_01to30,
                            'days_due_31to60': _row_currency['days_due_31to60'] + row.days_due_31to60,
                            'days_due_61to90': _row_currency['days_due_61to90'] + row.days_due_61to90,
                            'days_due_91to120': _row_currency['days_due_91to120'] + row.days_due_91to120,
                            'days_due_121togr': _row_currency['days_due_121togr'] + row.days_due_121togr
                        }
                    )
                else:
                    balance_with_currency.update(
                        {
                            row.invoice_currency: {
                                'amount_total': row.amount_total,
                                'saldo': row.saldo,
                                'suma_pagos': row.suma_pagos,
                                'days_due_01to30': row.days_due_01to30,
                                'days_due_31to60': row.days_due_31to60,
                                'days_due_61to90': row.days_due_61to90,
                                'days_due_91to120': row.days_due_91to120,
                                'days_due_121togr': row.days_due_121togr
                            }
                        }
                    )
                dateset_str.append([str(row.customer_name).encode('utf-8'),
                                    row.dias_atrasados,
                                    unicode(row.invoice_number),
                                    unicode(self.utc_mexico_city(row.invoice_date)),
                                    unicode(row.invoice_date_due) if row.invoice_date_due else '',
                                    unicode(row.type_doc).encode('utf-8'),
                                    unicode(row.state).encode('utf-8'),
                                    row.amount_total,
                                    row.saldo,
                                    row.invoice_currency,
                                    row.suma_pagos,
                                    row.days_due_01to30,
                                    row.days_due_31to60,
                                    row.days_due_61to90,
                                    row.days_due_91to120,
                                    row.days_due_121togr,
                                    ])

        for key, row in balance_with_currency.iteritems():
            dateset_str.append(['', '', '', '', '', '', '', row['amount_total'], row['saldo'], key, row['suma_pagos'],
                                row['days_due_01to30'], row['days_due_31to60'], row['days_due_61to90'],
                                row['days_due_91to120'], row['days_due_121togr'], ])
        """dateset_str.append(['', '', '', '', '', '', '', sum_amount_total, sum_saldo, '', sum_suma_pagos,
                            sum_days_due_01to30, sum_days_due_31to60, sum_days_due_61to90, sum_days_due_91to120,
                            sum_days_due_121togr, ])
        """
        obj_osv = OCsv()

        file = obj_osv.csv_base64(dateset_str)
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': 'Saldo_clientes.csv',
            'type_file': 'csv',
            'file': file,
        }, context=None)
