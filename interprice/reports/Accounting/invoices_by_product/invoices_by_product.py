# -*- coding: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Christian Daniel Medina'
# __email__ = 'christian.medina@gvadeto.com'

from openerp import models, fields, api, _
from openerp.osv import osv
import openerp.tools as tools
import cStringIO, base64
from olib.oCsv.oCsv import OCsv
import datetime


class ReportsAccountingInvoicesByProduct(osv.osv_memory):
    _name = 'r.invoices_by_product'

    date_from = fields.Datetime(string=_('Start Date'))
    date_to = fields.Datetime(string=_('End Date'))
    product_from = fields.Many2one('product.product', string=_('Product'))
    partner_id = fields.Many2one('res.partner', string=_('Customer'), domain=[('customer', '=', True), ('is_company', '=', True)])
    user_id = fields.Many2one('res.users', string=_('Vendor'), domain=[])
    warehouse_id = fields.Many2one('stock.warehouse', string=_('Warehouse'))
    currency_id = fields.Many2one('res.currency', string=_('Currency'),
                                  help=_('Select the currency that will be used to convert the movements'))
    section_id = fields.Many2one('product.category', _('Section'))
    line_id = fields.Many2one('product.category', _('Line'))
    brand_id = fields.Many2one('product.category', _('Brand'))
    serial_id = fields.Many2one('product.category', _('Serial'))

    @api.multi
    def _apply_filter(self, type=None):
        dataset_ids = []
        iuser_id = ''
        iwarehouse_id = ''
        and_partner_id = ''
        if self.partner_id:
            and_partner_id = ' and partner_id = %s ' % (self.partner_id.id,)
        and_product = ''
        if self.product_from:
            and_product = ' and product_id = %s' % self.product_from.id
        if self.user_id:
            iuser_id = ' and sales_agent_id = %s ' % (self.user_id.id,)
        if self.warehouse_id:
            iwarehouse_id = ' and warehouse_id = %s ' % (self.warehouse_id.id,)
        _currency = False
        if self.currency_id:
            _currency = self.currency_id

        strCategorias = ''
        if self.section_id.id:
            strCategorias = """ AND section_id = {} """.format(self.section_id.id)
            if self.line_id.id:
                strCategorias += """ AND line_id = {} """.format(self.line_id.id)
                if self.brand_id.id:
                    strCategorias += """ AND brand_id = {} """.format(self.brand_id.id)
                    if self.serial_id.id:
                        strCategorias += """ AND serial_id = {} """.format(self.serial_id.id)

        query = "select id from r_invoices_by_product_data where create_doc IS NOT NULL and create_doc >='%s' " \
                "and create_doc <= '%s' %s %s %s %s %s" \
                % (self.date_from, self.date_to, and_product, and_partner_id, iuser_id, iwarehouse_id, strCategorias)
        self.env.cr.execute(query)
        if self.env.cr.rowcount:
            dataset_ids_tmp = self.env.cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            if type == 'show':
                self.env.cr.execute('delete from r_invoices_by_product where id=%s', (ids[0],))
        else:
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids, _currency

    @api.multi
    def show(self):
        search_view_id = self.env['ir.model.data'].get_object_reference('reports', 'view_invoices_by_product_screen_search')[1]
        data_ids, currency = self._apply_filter(type='show')
        screen=self.env['invoices_by_product_screen']
        screen.with_context({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False}).show_screen(data_ids)
        return {
            'name': _('Invoices by product / customer'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,search',
            'res_model': 'invoices_by_product_screen',
            'search_view_id': search_view_id,
            'target': 'current',
        }

    @api.multi
    def csv(self):
        dataset_ids, currency = self._apply_filter(type='csv')
        ctx = self._context.copy()
        ctx.update({'currency2convert': currency.id if currency else False,'currency2convertname':currency.name if currency else False})
        id = self.env['r.invoices.by.product.data'].with_context(ctx).to_csv_by_ids(dataset_ids)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id.id),
            'target': 'self',
        }


class ReportsAccountingInvoicesByProductData(models.Model):
    _name = 'r.invoices.by.product.data'
    _table = 'r_invoices_by_product_data'
    _auto = False
    _order = 'id desc'

    sale_name = fields.Char(string=_('Sale number'))
    invoice_number = fields.Char(string=_('Invoice number'))
    out_name = fields.Char(string=_('Delivery order')) #picking_dev_id - account.invoice
    create_doc = fields.Datetime(string=_('Create date')) #date_invoice - account.invoice
    partner = fields.Char(string=_('Customer')) #partner_id - account.invoice
    sales_agent = fields.Char(string=_('Vendor')) #user_id - account.invoice
    type_doc = fields.Char(string=_('Type of document'))
    warehouse = fields.Char(string=_('Warehouse'))
    pay_method = fields.Char(string=_('Payment method')) #pay_method_sat_id - account.invoice
    state = fields.Char(string=_('State'))
    currency = fields.Char(string=_('Currency'))
    currency_copy = fields.Char(string=_('Currency'))
    currency_copy_id = fields.Many2one('res.currency', string=_('Currency id'))
    product_code = fields.Char(string=_('Code'))
    product_name = fields.Char(string=_('Product'))
    product_section = fields.Char(string=_('Section'))
    product_line = fields.Char(string=_('Line'))
    product_brand = fields.Char(string=_('Brand'))
    product_serial = fields.Char(string=_('Serial'))
    product_price_unit = fields.Float(string=_('Price unit'), digits=(10, 2))
    product_price_unit_copy = fields.Float(string=_('Price unit'), digits=(10, 2))
    product_subtotal = fields.Float(string=_('Subtotal'), digits=(10, 2))
    product_subtotal_copy = fields.Float(string=_('Subtotal id'), digits=(10, 2))
    invoice_total = fields.Float(string=_('Total'), digits=(10, 2))
    invoice_total_copy = fields.Float(string=_('Total'), digits=(10, 2))
    product_qty = fields.Integer(string=_('Quantity'))
    cost = fields.Float(string=_('Promedy cost'), digits=(10, 2))
    cost_copy = fields.Float(string=_('Promedy cost'), digits=(10, 2))
    subtotal_cost = fields.Float(string=_('Subtotal cost'), digits=(10, 2))
    subtotal_cost_copy = fields.Float(string=_('Subtotal cost'), digits=(10, 2))
    profit = fields.Float(string=_('Utility'), digits=(10, 2))
    profit_percent = fields.Float(string=_('Utility percentage'))
    rate = fields.Float(string=_('Type of exchange'), digits=(10, 4))
    currency_company = fields.Integer(string=_('Company currency'))

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_invoices_by_product_data')
        cr.execute("""
            create or replace view r_invoices_by_product_data as (
              SELECT
                  row_number() over (order by T1.create_doc) as id,
                  T1.sale_name,
                  T1.out_name,
                  T1.create_doc as create_doc,
                  T1.partner_id,
                  T1.partner,
                  T1.sales_agent_id,
                  T1.sales_agent,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN 'FAC'
                    WHEN 'out_refund' THEN 'NCC'
                  ELSE '' END type_doc,
                  T1.warehouse_id,
                  T1.warehouse,
                  T1.invoice_number,
                  T1.pay_method,
                  CASE T1.invoice_state WHEN 'open' THEN 'Abierto'
                    WHEN 'cancel' THEN 'Cancelado'
                    WHEN 'draft' THEN 'Borrador'
                    WHEN 'paid' THEN 'Pagado'
                  ELSE '' END state,
                  T1.currency as currency_copy,
                  T1.currency_id as currency_copy_id,
                  T1.product_id,
                  T1.product_code,
                  T1.product_name,
                  T1.section as product_section,
                  T1.line as product_line,
                  T1.brand as product_brand,
                  T1.serial as product_serial,
                  T1.section_id as section_id,
                  T1.line_id as line_id,
                  T1.brand_id as brand_id,
                  T1.serial_id as serial_id,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN T1.product_qty
                    WHEN 'out_refund' THEN -(T1.product_qty) END  as product_qty,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN T1.product_price_unit
                    WHEN 'out_refund' THEN -(T1.product_price_unit) END as product_price_unit_copy,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN T1.product_subtotal
                    WHEN 'out_refund' THEN -(T1.product_subtotal) END as product_subtotal_copy,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN round(cast(T1.cost AS NUMERIC),2)
                    WHEN 'out_refund' THEN -(round(cast(T1.cost AS NUMERIC),2)) END as cost_copy,
                  CASE T1.invoice_type
                    WHEN 'out_invoice' THEN round(cast(T1.cost * T1.product_qty AS NUMERIC),2)
                    WHEN 'out_refund' THEN -(round(cast((T1.cost * T1.product_qty )AS NUMERIC),2)) END as subtotal_cost_copy,
                  CASE
                    WHEN T1.invoice_type ='out_invoice' and T1.invoice_number !='' THEN T1.product_subtotal - (T1.cost * T1.product_qty)
                    WHEN T1.invoice_type = 'out_refund' and T1.invoice_number !='' THEN ((-T1.product_subtotal) - (-(T1.cost * T1.product_qty)))
                  END as profit_copy,
                  CASE
                    WHEN T1.invoice_type = 'out_invoice' and T1.product_subtotal > 0 THEN
                    (T1.product_subtotal - (T1.cost * T1.product_qty)  ) * 100 / T1.product_subtotal
                    WHEN T1.invoice_type = 'out_refund' and T1.product_subtotal >0 THEN
                    ((-T1.product_subtotal) - (-(T1.cost * T1.product_qty))  ) * 100 / T1.product_subtotal
                  ELSE 0 END as  profit_percent,
                  T1.invoice_total as invoice_total_copy,
                  T1.rate,
                  T1.currency_company
                FROM (
                       SELECT
                         ai.invoice_datetime as create_doc,
                         sw.id as warehouse_id,
                         sw.name as warehouse,
                         (SELECT rp.id FROM res_partner rp WHERE rp.id=ai.partner_id) as partner_id,
                         (SELECT rp.name FROM res_partner rp WHERE rp.id=ai.partner_id) as partner,
                         ai.user_id as sales_agent_id,
                         (SELECT rp.name FROM res_partner rp
                           INNER JOIN res_users ru ON ru.partner_id = rp.id WHERE ru.id = ai.user_id) as sales_agent,
                         (SELECT rc.name FROM res_currency rc WHERE id = ai.currency_id) as currency,
                         ai.currency_id as currency_id,
                         COALESCE(so.name,'') as sale_name,
                         CASE
                            WHEN sp.name IS NULL THEN
                                (SELECT spcd.name FROM stock_picking spcd
                                    INNER JOIN
                                    (
                                        SELECT soi.name as soi_name, spi.name as spi_name
                                        FROM account_invoice_line aili
                                            INNER JOIN account_invoice aii ON aii.id = aili.invoice_id
                                            LEFT JOIN stock_picking spi ON spi.name = aili.origin
                                            LEFT JOIN sale_order soi ON soi.name = spi.origin or soi.name = aili.origin
                                        WHERE aili.invoice_id = ai.id AND soi.name = so.name AND spi.name IS NOT NULL LIMIT 1
                                    ) as result ON result.soi_name = so.name
                                    WHERE spcd.name = result.spi_name)
                            ELSE sp.name END AS out_name,
                         COALESCE(ai.number,'') AS invoice_number,
                         COALESCE(ai.state,'') AS invoice_state,
                         COALESCE(ai.type, '') AS invoice_type,
                         (SELECT pm.description FROM pay_method_sat pm WHERE pm.id = ai.pay_method_sat_id) as pay_method,
                         pc.name as section,
                         pc.id as section_id,
                         pca.name as "line",
                         pca.id as "line_id",
                         pcat.name as brand,
                         pcat.id as brand_id,
                         pcatt.name as "serial",
                         pcatt.id as "serial_id",
                         pp.id as product_id,
                         pp.default_code as product_code,
                         pp.name_template as product_name,
                         ail.quantity as product_qty,
                         ail.price_unit as product_price_unit,
                         (ail.quantity* ail.price_unit) as product_subtotal,
                         (
                            SELECT
                              round(CAST (COALESCE(pph.cost,0) AS NUMERIC), 2)  product_cost
                            FROM
                              product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                            WHERE pp.id=ail.product_id AND pph.create_date <=sp.date  ORDER BY pph.create_date desc LIMIT 1
                         ) as cost,
                         ai.amount_total as invoice_total,
                         CASE WHEN ai.picking_dev_id IS NOT NULL THEN doc_tc(ai.id, ai.company_id, 'INV', sp.date)
                         ELSE doc_tc(ai.id ,ai.company_id ,'INV', ai.invoice_datetime) END AS rate,
                         rcom.currency_id as currency_company
                    FROM
                         account_invoice_line ail
                         INNER JOIN account_invoice ai ON ai.id = ail.invoice_id
                         LEFT JOIN stock_picking sp ON sp.name = ail.origin
                         LEFT JOIN sale_order so ON so.name = sp.origin OR so.name = ail.origin
                         LEFT JOIN stock_warehouse sw ON sw.id = so.warehouse_id
                         LEFT JOIN product_product pp on ail.product_id = pp.id
                         LEFT JOIN product_template ptt on ptt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pc on pc.id = ptt.section
                         LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pca on pca.id = pt.line
                         LEFT JOIN product_template ptm on ptm.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcat on pcat.id = ptm.brand
                         LEFT JOIN product_template ptmt on ptmt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcatt on pcatt.id = ptmt.serial
                         LEFT JOIN res_partner rp on ai.partner_id = rp.id
                         INNER JOIN res_users ru on ai.user_id=ru.id
                         LEFT JOIN res_company rcom on rcom.id= ai.company_id
                         INNER JOIN res_currency rc on ai.currency_id= rc.id
                    WHERE ai.state NOT IN ('draft') AND ai.type in ('out_invoice')
           UNION ALL
                       SELECT
                         ai.invoice_datetime AS create_doc,
                         sw.id AS warehouse_id,
                         CASE
                            WHEN so.name IS NULL THEN '' ELSE sw.name END as warehouse,
                         rp.id partner_id, rp.name partner,
                         ai.user_id AS sales_agent_id,
                         (
                            SELECT rpt.display_name FROM res_partner as rpt
                            where id =(SELECT rs.partner_id FROM res_users as rs where id= ai.user_id)
                         ) as sales_agent,
                         rc.name as currency ,
                         ai.currency_id as currency_id,
                         COALESCE(so.name,'') as sale_name,
                         CASE
                             WHEN ai.type = 'out_refund' AND ai.picking_dev_id is not NULL THEN (SELECT sp.name FROM stock_picking as sp WHERE id=ai.picking_dev_id)
                             ELSE ''
                             END as out_name,
                         CASE
                            WHEN ai.number IS NULL THEN '' ELSE ai.number END AS invoice_number, ai.state AS invoice_state, ai.type AS invoice_type,
                            (SELECT COALESCE(string_agg(pm.name,'/ '),'') FROM account_invoice_pay_method_rel aipmr
                              INNER JOIN pay_method pm ON aipmr.pay_method_id = pm.id
                            WHERE aipmr.account_invoice_id = ai.id
                            )as pay_method,
                         pc.name as section, --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'SECTION') as section,
                         pc.id as section_id,
                         pca.name as "line", --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'LINE') as line,
                         pca.id as line_id,
                         pcat.name as brand, --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'BRAND') as brand,
                         pcat.id as brand_id,
                         pcatt.name as "serial", --getProductCategoryBy((SELECT categ_id FROM product_template ptt WHERE ptt.id=pp.id),'BRAND') as brand,
                         pcatt.id as serial_id,
                         pp.id as product_id, pp.default_code as product_code, pp.name_template as product_name,
                         ail.quantity as product_qty, ail.price_unit, (ail.quantity * ail.price_unit) as product_subtotal,
                         (
                            SELECT
                              round(CAST (COALESCE(pph.cost,0) AS NUMERIC), 2) product_cost
                            FROM
                              product_price_history pph INNER JOIN product_product pp on pph.product_template_id = pp.product_tmpl_id
                            WHERE pp.id=ail.product_id AND pph.create_date <= ai.invoice_datetime  ORDER BY pph.create_date desc LIMIT 1
                          ) as cost,
                          ai.amount_total as invoice_total,
                          CASE WHEN ai.picking_dev_id IS NOT NULL THEN doc_tc(ai.id, ai.company_id, 'INV', sp.date)
                          ELSE doc_tc(ai.id ,ai.company_id ,'INV', ai.invoice_datetime) END AS rate,
                          rcom.currency_id as currency_company
                    FROM account_invoice_line ail
                         INNER JOIN account_invoice ai ON ai.id = ail.invoice_id
                         LEFT JOIN stock_picking spd ON spd.id = ai.picking_dev_id and ai.type = 'out_refund'
                         LEFT JOIN stock_picking sp ON sp.name = spd.origin
                         LEFT JOIN sale_order so on so.name = sp.origin
                         LEFT JOIN stock_warehouse sw ON sw.id = so.warehouse_id
                         INNER JOIN product_product pp on ail.product_id = pp.id
                         LEFT JOIN product_template ptt on ptt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pc on pc.id = ptt.section
                         LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pca on pca.id = pt.line
                         LEFT JOIN product_template ptm on ptm.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcat on pcat.id = ptm.brand
                         LEFT JOIN product_template ptmt on ptmt.id = pp.product_tmpl_id
                         LEFT JOIN product_category pcatt on pcatt.id = ptmt.serial
                         INNER JOIN res_partner rp on ai.partner_id =rp.id
                         INNER JOIN res_currency rc on ai.currency_id= rc.id
                         INNER JOIN res_company rcom on rcom.id= ai.company_id
                         INNER JOIN res_users ru on so.user_id=ru.id
                    WHERE ai.state NOT IN ('draft') AND ai.type in ('out_refund')
                     )AS T1
            )
        """)

    def convert_date_timezone(self, date):
        from datetime import datetime as dt
        from dateutil import tz
        if date:
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz(self.env.user.partner_id.tz)
            if to_zone == False:
                to_zone == tz.gettz('America/Mexico_City')
            try:
                utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S')
            except Exception:
                utc = dt.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
            utc = utc.replace(tzinfo=from_zone)
            central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
            return central
        else:
            return ''

    def to_csv_by_ids(self, ids):
        ccontext = self._context.get('currency2convertname')
        ccost=ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        dataset_csv = [
            [
                # 'id',
                _('Invoice number'),
                _('Sale number'),
                unicode(_('Creation date')).encode('utf8'),
                _('Customer'),
                _('Vendor'),
                _('Type of document'),
                unicode(_('Warehouse')).encode('utf8'),
                unicode(_('Delivery order')).encode('utf8'),
                unicode(_('Payment method')).encode('utf8'),
                _('State'),
                _('Currency'),
                unicode(_('Code')).encode('utf8'),
                _('Product'),
                unicode(_('Section')).encode('utf8'),
                unicode(_('Line')).encode('utf8'),
                _('Brand'),
                _('Serial'),
                _('Quantity'),
                _('Price unit'),
                _('Subtotal'),
                _('Promedy cost'),
                _('Subtotal cost'),
                _('Utility'),
                _('Utility percentage'),
                _('Type of exchange')
            ]
        ]
        query = """
            DROP TABLE IF EXISTS csv_invoices_by_product;
            CREATE TEMPORARY TABLE csv_invoices_by_product (id INTEGER, sale_name CHARACTER VARYING, create_doc TIMESTAMP WITHOUT TIME ZONE,
            partner CHARACTER VARYING, sales_agent CHARACTER VARYING, type_doc CHARACTER VARYING, warehouse CHARACTER VARYING,
            out_name CHARACTER VARYING, invoice_number CHARACTER VARYING, pay_method CHARACTER VARYING, state CHARACTER VARYING,
            currency_copy CHARACTER VARYING, product_code CHARACTER VARYING, product_name CHARACTER VARYING, product_section CHARACTER VARYING,
            product_line CHARACTER VARYING, product_brand CHARACTER VARYING, product_serial CHARACTER VARYING, product_qty NUMERIC, product_price_unit NUMERIC, product_subtotal NUMERIC,
            "cost" NUMERIC, subtotal_cost NUMERIC, profit NUMERIC, profit_percent NUMERIC, rate CHARACTER VARYING);
            INSERT INTO csv_invoices_by_product
            SELECT rspd.id, rspd.sale_name, rspd.create_doc, rspd.partner, rspd.sales_agent, rspd.type_doc, rspd.warehouse,
            rspd.out_name, rspd.invoice_number, rspd.pay_method, rspd.state, rspd.currency_copy, rspd.product_code, rspd.product_name,
            rspd.product_section, rspd.product_line, rspd.product_brand, rspd.product_serial, rspd.product_qty,
            CASE WHEN rspd.product_price_unit_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_price_unit_copy,rspd.create_doc) ELSE NULL END as product_price_unit,
            CASE WHEN rspd.product_subtotal_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_subtotal_copy,rspd.create_doc) ELSE NULL END as product_subtotal,
            CASE WHEN rspd.cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.cost_copy,rspd.create_doc) ELSE NULL END as cost,
            CASE WHEN rspd.subtotal_cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.subtotal_cost_copy,rspd.create_doc) ELSE NULL END as subtotal_cost,
            NULL, NULL, rspd.rate
            FROM r_invoices_by_product_data rspd
            LEFT OUTER JOIN res_currency rc on rc.id=rspd.currency_company
            WHERE rspd.id in {1};
            UPDATE csv_invoices_by_product SET profit = round(abs(product_subtotal)-abs(subtotal_cost),2) WHERE type_doc in ('FAC','NCC') and product_subtotal > 0;
            UPDATE csv_invoices_by_product SET profit_percent = round(profit * 100 / abs(product_subtotal),2) WHERE profit > 0;
            SELECT * FROM csv_invoices_by_product ORDER BY id ASC;
        """.format(ccontext,data_ids,ccost)
        self.env.cr.execute(query)
        dataset = self.env.cr.dictfetchall()
        sum_subtotal = 0.00
        sum_total = 0.00
        sum_cost = 0.00
        sum_utilidad = 0.00
        for row in dataset:
            if row['state'] not in ['Borrador'] and row['product_subtotal'] != None:
                sum_subtotal += row['product_subtotal'] if row['product_subtotal'] else 0
                sum_cost += row['cost'] if row['cost'] else 0
                sum_total += row['subtotal_cost'] if row['subtotal_cost'] else 0
                sum_utilidad += row['profit'] if row['profit'] else 0
            dataset_csv.append(
                [
                    unicode(self.set_default(row['invoice_number'])).encode('utf8'),
                    unicode(self.set_default(row['sale_name'])).encode('utf8'),
                    unicode(self.convert_date_timezone(row['create_doc'])).encode('utf8'),
                    unicode(self.set_default(row['partner'])).encode('utf8'),
                    unicode(self.set_default(row['sales_agent'])).encode('utf8'),
                    unicode(self.set_default(row['type_doc'])).encode('utf8'),
                    unicode(self.set_default(row['warehouse'])).encode('utf8'),
                    unicode(self.set_default(row['out_name'])).encode('utf8'),
                    unicode(self.set_default(row['pay_method'])).encode('utf8'),
                    unicode(self.set_default(row['state'])).encode('utf8'),
                    unicode(self.set_default(row['currency_copy'])).encode('utf8'),
                    unicode(self.set_default(row['product_code'])).encode('utf8'),
                    unicode(self.set_default(row['product_name'])).encode('utf8'),
                    unicode(self.set_default(row['product_section'])).encode('utf8'),
                    unicode(self.set_default(row['product_line'])).encode('utf8'),
                    unicode(self.set_default(row['product_brand'])).encode('utf8'),
                    unicode(self.set_default(row['product_serial'])).encode('utf8'),
                    unicode(self.set_default(row['product_qty'], 0)).encode('utf8'),
                    unicode(self.set_default(row['product_price_unit'], 0)).encode('utf8'),
                    unicode(self.set_default(row['product_subtotal'], 0)).encode('utf8'),
                    unicode(self.set_default(row['cost'], 0)).encode('utf8'),
                    unicode(self.set_default(row['subtotal_cost'], 0)).encode('utf8'),
                    unicode(self.set_default(row['profit'], 0)).encode('utf8'),
                    unicode(self.set_default(row['profit_percent'], 0)).encode('utf8'),
                    unicode(self.set_default(row['rate'], 0)).encode('utf8')
                    ]
            )
        sum_subtotal = round(sum_subtotal, 2)
        sum_cost = round(sum_cost, 2)
        sum_total = round(sum_total, 2)
        sum_utilidad = round(sum_utilidad, 2)
        profit_percent_general = format(((sum_subtotal-sum_cost) * 100 / sum_subtotal), '.2f') if sum_subtotal > 0 else 0
        dataset_csv.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', sum_subtotal, sum_cost, sum_total, sum_utilidad, profit_percent_general, '',])
        obj_csv = OCsv()
        file = obj_csv.csv_base64(dataset_csv)
        return self.env['r.download'].create(vals={
            'file_name': _('Invoices_by_product.csv'),
            'type_file': 'csv',
            'file': file,
        })

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default


class ReportInvoicesByProductScreen(osv.osv_memory):
    _name = 'invoices_by_product_screen'

    sale_name = fields.Char(string=_('Sale number'))
    out_name = fields.Char(string=_('Delivery order'))
    create_doc = fields.Datetime(string=_('Creation date'))
    partner = fields.Char(string=_('Customer'))
    sales_agent = fields.Char(string=_('Vendor'))
    type_doc = fields.Char(string=_('Type of document'))
    warehouse = fields.Char(string=_('Warehouse'))
    invoice_number = fields.Char(string=_('Invoice number'))
    pay_method = fields.Char(string=_('Payment method'))
    state = fields.Char(string=_('State'))
    currency = fields.Char(string=_('Currency'))
    currency_copy = fields.Char(string=_('Currency'))
    currency_copy_id = fields.Many2one('res.currency', string=_('Currency'))
    product_code = fields.Char(string=_('Code'))
    product_name = fields.Char(string=_('Product'))
    product_section = fields.Char(string=_('Section'))
    product_line = fields.Char(string=_('Line'))
    product_brand = fields.Char(string=_('Brand'))
    product_serial = fields.Char(string=_('Serial'))
    product_price_unit = fields.Float(string=_('Price unit'), digits=(10, 2))
    product_price_unit_copy = fields.Float(string=_('Price unit'), digits=(10, 2))
    product_subtotal = fields.Float(string=_('Subtotal'), digits=(10, 2))
    product_subtotal_copy = fields.Float(string=_('Subtotal'), digits=(10, 2))
    invoice_total = fields.Float(string=_('Total'), digits=(10, 2))
    invoice_total_copy = fields.Float(string=_('Total'), digits=(10, 2))
    product_qty = fields.Integer(string=_('Quantity'))
    cost = fields.Float(string=_('Promedy cost'), digits=(10, 2))
    cost_copy = fields.Float(string=_('Promedy cost'), digits=(10, 2))
    subtotal_cost = fields.Float(string=_('Subtotal cost'), digits=(10, 2))
    subtotal_cost_copy = fields.Float(string=_('Subtotal cost'), digits=(10, 2))
    profit = fields.Float(string=_('Utility'), digits=(10, 2))
    profit_percent = fields.Float(string=_('Utility percentage'), digits=(10, 2))
    rate = fields.Float(string=_('Type of exchange'), digits=(10, 4))
    currency_company = fields.Integer(string=_('Company currency'))
    currency_show = fields.Char(string=_('Document currency'))

    @api.multi
    def show_screen(self, ids):
        ccontext = self._context['currency2convertname']
        ccost = ccontext
        if not ccontext:
            ccontext="'False'"
            ccost = 'currency_copy'
        else:
            ccontext = "'"+ccontext+"'"
            ccost = ccontext
        data_ids="("+','.join(str(e) for e in ids)+")"
        currency_show = ccontext if self._context['currency2convertname'] else 'rspd.currency_copy'
        query = """
            TRUNCATE TABLE invoices_by_product_screen;
            INSERT INTO invoices_by_product_screen (id, sale_name, create_doc, partner, sales_agent, type_doc, warehouse,
            out_name, invoice_number, pay_method, state, currency_copy, currency, product_code, product_name, product_section, product_line,
            product_brand, product_serial, product_qty, product_price_unit, product_subtotal, "cost", subtotal_cost, profit, profit_percent, rate, currency_show,
            create_uid, write_uid)
            SELECT rspd.id, rspd.sale_name, rspd.create_doc, rspd.partner, rspd.sales_agent, rspd.type_doc, rspd.warehouse,
            rspd.out_name, rspd.invoice_number, rspd.pay_method, rspd.state, rspd.currency_copy, rspd.currency_copy as currency, rspd.product_code, rspd.product_name,
            rspd.product_section, rspd.product_line, rspd.product_brand, rspd.product_serial, rspd.product_qty,
            CASE WHEN rspd.product_price_unit_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_price_unit_copy,rspd.create_doc) ELSE NULL END as product_price_unit,
            CASE WHEN rspd.product_subtotal_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.product_subtotal_copy,rspd.create_doc) ELSE NULL END as product_subtotal,
            CASE WHEN rspd.cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.cost_copy,rspd.create_doc) ELSE NULL END as cost,
            CASE WHEN rspd.subtotal_cost_copy NOTNULL THEN currency_convert(rspd.currency_copy,{0},rspd.subtotal_cost_copy,rspd.create_doc) ELSE NULL END as subtotal_cost,
            NULL, NULL, rspd.rate, {3} as currency_show, {4}, {4}
            FROM r_invoices_by_product_data rspd
            LEFT OUTER JOIN res_currency rc on rc.id=rspd.currency_company
            WHERE rspd.id in {1};
            UPDATE invoices_by_product_screen SET profit = round(abs(product_subtotal)-abs(subtotal_cost),2) WHERE type_doc in ('FAC','NCC') and product_subtotal > 0;
            UPDATE invoices_by_product_screen SET profit_percent = round(profit * 100 / abs(product_subtotal),2) WHERE profit > 0;
        """.format(ccontext,data_ids,ccost,currency_show,self.env.uid)
        self.env.cr.execute(query)
        return True
