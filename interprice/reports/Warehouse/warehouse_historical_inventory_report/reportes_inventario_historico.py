# -*- coding: utf-8 -*-

######################################################################################################################################################
#  @version     : 1.0                                                                                                                                #
#  @autor       : #Javier Barajas(gvadeto)2015                                                                                                #                                                                                                 #
#  @linea       : Maximo 150 chars                                                                                                                   #
######################################################################################################################################################

#OpenERP imports
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from datetime import date
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import decimal
from openerp import models, api, _
import base64
import cStringIO
import openerp.addons.decimal_precision as dp
import codecs


class existencia_historico_habi(osv.osv):

    def action_wizard(self, cr, uid, ids, context=None):
        """Funcion que exporta a exel"""
        columna = "\"ALMACEN\",\"CLAVE ARTICULO\",\"DESCRIPCION\",\"EXISTENCIA\",\"COSTO PROMEDIO UNITARIO\",\"IMPORTE\"\n"
        nombre_arch = "inventario_historico"
        lista = []
        state = ""
        for fac in self.browse(cr, uid, ids):
            columna += "\"" + unicode(fac.almacen).encode("utf-8") + "\"," + "\"" + unicode(
                fac.clave_articulo).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.descripcion.replace('"', "''")).encode(
                "utf-8") + "\"," + "\"" + unicode(fac.existencia).encode("utf-8") + "\"," \
                       + "\"" + unicode(fac.costo_promedio_unitario).encode("utf-8") + "\"," + "\"" + unicode(
                fac.importe).encode("utf-8") + "\"," + "\n"

        # --------- termina contenido archivo
        buf = cStringIO.StringIO()
        buf.write(columna)
        out = base64.encodestring(buf.getvalue())
        buf.close()
        filename_psn = nombre_arch + ".csv"
        # --------end------------------------
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': filename_psn,
            'type_file': 'csv',
            'file': out,
        }, context=None)

    def _get_info(self, cr, uid, data, context=None):
        #data = self.browse(cr, uid, ids)
        cr.execute("DELETE FROM _gv_inventario_historico")

        #strCondicionConsignacion = ''  # Todos los articulos
        #if data.select_by == 'sinconsigna':  # Articulos sin consignacion
        #    strCondicionConsignacion = "AND SW.type != 'pre'"
        #elif data.select_by == 'soconsigna':  # Articulos que tengan consignacion solamente
        #    strCondicionConsignacion = "AND SW.type = 'pre'"

        strCondicionProductos = ''  # Todos los productos
        if data.product_id.default_code and data.id_product.default_code:
            strCondicionProductos = """AND PP.default_code>='{}' AND PP.default_code<='{}'""".format(data.product_id.default_code, data.id_product.default_code)
        elif data.product_id.default_code:
            strCondicionProductos = """AND PP.default_code ='{}'""".format(data.product_id.default_code)

        strCondicionAlmacenes = ''  # Todos los Almacenes
        if data.warehouse_id.id and data.warehouse_dest_id.id:
            strCondicionAlmacenes = """AND HT.warehouse_id >= {} AND HT.warehouse_id <= {}""".format(data.warehouse_id.id, data.warehouse_dest_id.id)
        elif data.warehouse_id.id:
            strCondicionAlmacenes = """AND HT.warehouse_id = {}""".format(data.warehouse_id.id)

        cadena_consulta = """   SELECT 	HT.product_id AS iIdProducto,
                                        PP.product_tmpl_id AS iIdProductoTemplate,
                                        PP.default_code AS strSku,
                                        PP.name_template AS strNombre,
                                        HT.warehouse_id AS iAlmacen,
                                        SW.name AS strNombreAlmacen,
                                        SW.name||'('||HT.warehouse_id||')' AS strAlmacen,
                                        SUM(HT.product_qty_move) AS iExistencia,
                                        MAX(HT.create_date) AS dtFechaRegistro
                                FROM	_history_stock AS HT
                                        INNER JOIN product_product AS PP
                                        ON HT.product_id = PP.id
                                        INNER JOIN stock_warehouse AS SW
                                        ON HT.warehouse_id = SW.id
                                WHERE 	HT.create_date <= '{} 23:59:59' AT TIME ZONE 'utc'
                                        {}
                                        {}
                                        {}
                                GROUP BY HT.product_id, PP.default_code, PP.name_template, HT.warehouse_id, SW.name, PP.product_tmpl_id
                                ORDER BY HT.product_id
        """

        cr.execute(cadena_consulta.format(
            data.date_from,
            strCondicionAlmacenes,
            strCondicionProductos,
            "", #strCondicionConsignacion,
            ))

        lista_productos = {}
        if cr.rowcount:
            for (iIdProducto, iIdProductoTemplate, strSku, strNombre, iAlmacen, strNombreAlmacen, strAlmacen, iExistencia, dtFechaRegistro) in cr.fetchall():
                #lista_productos.update({iIdProducto: {iAlmacen: {'iExistencia': iExistencia,
                #                                                'iIdProductoTemplate': iIdProductoTemplate,
                #                                                'strAlmacen': strNombreAlmacen,
                #                                                'strAlmacenClave': strAlmacen,
                #                                                'strCveProd': strSku,
                #                                                'strNombreProd': strNombre,
                #                                                'dtFechaRegistro': dtFechaRegistro,}}})
                if not lista_productos:
                    lista_productos.update(
                        {
                            iIdProducto: {
                                iAlmacen: {
                                    'iExistencia': iExistencia,
                                    'iIdProductoTemplate': iIdProductoTemplate,
                                    'strAlmacen': strNombreAlmacen,
                                    'strAlmacenClave': strAlmacen,
                                    'strCveProd': strSku,
                                    'strNombreProd': strNombre,
                                    'dtFechaRegistro': dtFechaRegistro
                                }
                            }
                        }
                    )
                else:
                    if iIdProducto in lista_productos:
                        lista_productos[iIdProducto].update({
                            iAlmacen: {
                                'iExistencia': iExistencia,
                                'iIdProductoTemplate': iIdProductoTemplate,
                                'strAlmacen': strNombreAlmacen,
                                'strAlmacenClave': strAlmacen,
                                'strCveProd': strSku,
                                'strNombreProd': strNombre,
                                'dtFechaRegistro': dtFechaRegistro
                            }
                        }
                        )
                    else:
                        lista_productos.update(
                            {
                                iIdProducto: {
                                    iAlmacen: {
                                        'iExistencia': iExistencia,
                                        'iIdProductoTemplate': iIdProductoTemplate,
                                        'strAlmacen': strNombreAlmacen,
                                        'strAlmacenClave': strAlmacen,
                                        'strCveProd': strSku,
                                        'strNombreProd': strNombre,
                                        'dtFechaRegistro': dtFechaRegistro
                                    }
                                }
                            }
                        )
        else:
            raise osv.except_osv(_('Alerta!'), _("No hay ninguna coincidencia"))
        return lista_productos

    def _get_costo(self, cr, uid, data, context=None):
        #data = self.browse(cr, uid, ids)
        cadena_consulta = """
            SELECT pp.product_tmpl_id AS iIdProducto,
              COALESCE((
                     SELECT round(CAST(pph.cost AS NUMERIC),2) from product_price_history pph
                     WHERE pp.product_tmpl_id=pph.product_template_id AND pph.cost>0 AND
                           CAST(datetime AS DATE) <= '{}'
                     ORDER BY pph.datetime DESC LIMIT 1
                   ), 0) iCosto
            FROM product_product AS pp
            ORDER BY pp.id DESC
        """

        cr.execute(cadena_consulta.format(
            data.date_from,))

        lista = {}
        if cr.rowcount:
            for (iIdProducto, iCosto, ) in cr.fetchall():
                lista.update({iIdProducto: iCosto})

        return lista

    def _get_historico(self, cr, uid, data, context=None):
        """Reporte historico"""
        lista = self._get_info(cr, uid, data, context=None) #Obtenemos la Lista Principal de productos
        lista_costos = self._get_costo(cr, uid, data, context=None) #Obtenemos la lista con los costos de los productos

        obj_historico = self.pool.get('gv.inventario.historico')
        arrIds = []
        for producto in lista:
            for almacen in lista[producto]:
                fila = lista[producto][almacen]
                #objeto = obj_history.browse(cr, uid, producto, context=context)

                if fila['iExistencia'] > 0:
                    vals = {
                        'date_to': fila['dtFechaRegistro'],
                        #'gv_inventario_historico_id': data.id,
                        'almacen': fila['strAlmacenClave'],
                        'clave_articulo': fila['strCveProd'],
                        'descripcion': fila['strNombreProd'],
                        'existencia': fila['iExistencia'],
                        'costo_promedio_unitario': lista_costos.get(fila['iIdProductoTemplate'], 0),
                        'importe': fila['iExistencia'] * lista_costos.get(fila['iIdProductoTemplate'], 0),

                    }
                    arrIds.append(obj_historico.create(cr, uid, vals, context=context))
        return arrIds



    # Nombre del modelo
    _name = 'gv.inventario.historico'
    # Nombre de la tabla
    _table = '_gv_inventario_historico'
    _rec_name = 'id'
    # Columnas y/o campos Tree & Form

    _columns = {

        'date_from': fields.date('DE: ', required=False),
        'date_to': fields.datetime('FECHA'),
        'almacen': fields.char('ALMACEN', size=254),
        'clave_articulo': fields.char('CLAVE ARTICULO'),
        'descripcion': fields.char('DESCRIPCION', size=254),
        'existencia': fields.char('EXISTENCIA', size=254),
        'costo_promedio_unitario': fields.float('COSTO PROMEDIO UNITARIO',
                                                digits_compute=dp.get_precision('Product Price')),
        'importe': fields.float('IMPORTE', digits_compute=dp.get_precision('Product Price')),


        'gv_inventario_historico_ids': fields.one2many('gv.inventario.historico', 'gv_inventario_historico_id',
                                                       'Historial'),
        'gv_inventario_historico_id': fields.many2one('gv.inventario.historico', 'inventarioid'),


    }



class existencia_historico_filtros(osv.osv):
    _name = "r.filtros.existencia.historico"

    _columns = {

        'date_from': fields.date('DE: ', required=False),
        #'select_by': fields.selection([
        #    ('consigna', 'Incluir artículos en consignación'),
        #    ('sinconsigna', 'Sin incluir artículos en consignación'),
        #    ('soconsigna', 'Solo artículos en consignación'),
        #], 'Consginación:', required=False, ),
        'product_id': fields.many2one('product.product', 'Inicial'),
        'id_product': fields.many2one('product.product', 'Final'),

        'warehouse_id': fields.many2one('stock.warehouse', 'Inicial'),
        'warehouse_dest_id': fields.many2one('stock.warehouse', 'Final'),
        'active_all': fields.boolean('Todos:'),
    }

    def ValidarFiltros(self, cr, uid, data, context=None):
        if (not data.warehouse_id and data.warehouse_dest_id) or (not data.product_id and data.id_product):
            raise osv.except_osv(_('Alerta!'), _("Verifique que sus filtros sean correctos"))

    def Pantalla(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)
        self.ValidarFiltros(cr, uid, data, context=None)
        list_ids = self.pool.get('gv.inventario.historico')._get_historico(cr, uid, data, context=None)
        #print list_ids
        return {
            'name': 'Historico',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'gv.inventario.historico',
            'target': 'current',
            'domain': [('id', 'in', list_ids)],
            # 'target': 'new',
        }

    def Archivo(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)
        self.ValidarFiltros(cr, uid, data, context=None)
        list_ids = self.pool.get('gv.inventario.historico')._get_historico(cr, uid, data, context=None)
        id_archivo = self.pool.get('gv.inventario.historico').action_wizard(cr, uid, list_ids, context=None)

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id_archivo),
            'target': 'self',
        }


    def onchage_date_from(self, cr, uid, ids, date_from):
        """
        Valida que la fecha se eliga no sea la actual ni mayor que la actual
        """
        # # try:
        return {'value': {},
                'warning': {'title': 'Alerta',
                            'message': str(time.strftime(date_from)) + " : " + str(datetime.today())}}
        return {'value': {},
                'warning': {'title': 'Alerta',
                            'message': str(time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)) + " : " + str(
                                datetime.today() - relativedelta(days=1))},}
        # except:
        #   return {'value' : {}}

    @api.onchange('warehouse_aux_id')
    def _onchange_warehouse_id_aux(self):
        self.warehouse_id = self.warehouse_aux_id

    @api.onchange('warehouse_aux_dest_id')
    def _onchange_warehouse_dest_aux(self):
        self.warehouse_dest_id = self.warehouse_aux_dest_id

    def onchage_active_all(self, cr, uid, ids, active_all):
        """
         Todos los productos, ordenados por SKU
        """
        try:
            if active_all == True:
                cr.execute("""select min(default_code) from product_product""")
                first = cr.fetchone()[0]
                min_id = self.pool.get('product.product').search(cr, uid, [('default_code', '=', first)])[0]
                cr.execute("""select max(default_code) from product_product""")
                maxx = cr.fetchone()[0]
                max_id = self.pool.get('product.product').search(cr, uid, [('default_code', '=', maxx)])[0]
                return {'value': {'product_id': min_id, 'id_product': max_id},}
            else:
                return {'value': {'product_id': 0, 'id_product': 0},}
        except:
            return {'value': {}}

    #def onchage_select_by(self, cr, uid, ids, select_by):
    #    """
    #     almacenes consignacion
    #    """
    #    try:
    #        if select_by == 'soconsigna':
    #            cr.execute("""SELECT id_warehouse from _res_configuracion""")
    #            if cr.rowcount:
    #                for this in cr.fetchall():
    #                    cr.execute("""update stock_warehouse set gvconsigacion=True where id=%s""", (this,))
    #                return {'value': {'warehouse_aux_id': 0,
    #                                  'warehouse_aux_dest_id': 0,
    #                                  'warehouse_id': 0,
    #                                  'warehouse_dest_id': 0},}
    #        elif select_by == 'sinconsigna':
    #            return {'value': {'warehouse_aux_id': 0,
    #                              'warehouse_aux_dest_id': 0,
    #                              'warehouse_id': 0,
    #                              'warehouse_dest_id': 0},}
    #        else:
    #            return {'value': {'warehouse_aux_id': 0,
    #                              'warehouse_aux_dest_id': 0,
    #                              'warehouse_id': 0,
    #                              'warehouse_dest_id': 0},}
    #    except:
    #        return {'value': {}}

    def onchage_validate_date(self, cr, uid, ids, date_from):
        """
          Valida que la fecha que elijan sea menor a la fecha actual..
        """
        try:
            if date_from < datetime.now().strftime('%Y-%m-%d'):
                return {'value': {},}
            else:
                return {'value': {'date_from': 0},
                        'warning': {'title': 'Alerta', 'message': 'Favor de elegir una fecha menor a la fecha actual'}}
        except:
            return {'value': {}}

    def onchage_validate_id_product(self, cr, uid, ids, id_product, product_id):
        """
          Valida que la fecha que elijan sea menor a la fecha actual..
        """
        try:
            object_product = self.pool.get('product.product')
            cr.execute("select default_code from product_product where id=%s", (product_id,))
            product_one = cr.fetchone()[0]
            cr.execute("select default_code from product_product where id=%s", (id_product,))
            product_2 = cr.fetchone()[0]
            if product_2 < product_one:
                return {'value': {'product_id': id_product, 'id_product': product_id},}
        except:
            return {'value': {}}

    def _get_min_warehouse(self, cr, uid, context=None):
        try:
            cr.execute("""select min(id) from stock_warehouse""")
            first = cr.fetchone()[0]
            return first
        except Exception, ex:
            return False

    _defaults = {
        # 'warehouse_id':_get_min_warehouse,
    }