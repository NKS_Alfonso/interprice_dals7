Modulo de reportes para ventas, compras,
recepción, saldos bancarios, antigüedad
de saldo clientes y proveedores.


Filtado de reporte de ventas por fecha

.. figure:: ../reports/static/img/sale_report_date.png
    :alt: Menu reporte de ventas por fecha
    :width: 100%

Filtado de reporte de ventas por producto

.. figure:: ../reports/static/img/sale_report_product.png
    :alt: Menu reporte de ventas por producto
    :width: 100%

Filtado de reporte de compras por fecha

.. figure:: ../reports/static/img/pur_report_date.png
    :alt: Menu reporte de ventas por fecha
    :width: 100%

Filtado de reporte de compras por producto

.. figure:: ../reports/static/img/pur_report_product.png
    :alt: Menu reporte de ventas por producto
    :width: 100%

Filtado de reporte antigüedad de saldo clientes

.. figure:: ../reports/static/img/balance_customer.png
    :alt: Menu reporte de saldos bancarios
    :width: 100%

Filtado de reporte de recepción

.. figure:: ../reports/static/img/reception_report.png
    :alt: Menu reporte de saldos bancarios
    :width: 100%

Filtado de reporte de saldos bancarios

.. figure:: ../reports/static/img/balance_report.png
    :alt: Menu reporte de saldos bancarios
    :width: 100%

