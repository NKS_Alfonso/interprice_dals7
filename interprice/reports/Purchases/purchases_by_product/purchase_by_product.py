# -*- coding: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Enrique Avendaño Trujillo'
# __email__ = 'enrique.avendano@gvadeto.com'

from openerp import models, fields, api, _
from openerp.osv import osv
import openerp.tools as tools
from olib.oCsv.oCsv import OCsv


class ReportsPurchasesPurchasesByProduct(osv.osv_memory):
    _name = 'r.purchases_by_product'

    date_from = fields.Date(string=_('Fecha inicial'), help="Coloca la fecha en la que deseas iniciar tu reporte")
    date_to = fields.Date(string=_('Fecha final'), help="Coloca la fecha en la que deseas finalizar tu reporte")
    product_from = fields.Many2one('product.product', string=_('Producto inicial'), domain=[], help="Coloca el producto inicial de tu reporte")
    product_to = fields.Many2one('product.product', string=_('Producto final'), readonly=True, help="Coloca el producto final de tu reporte")
    currency_id = fields.Many2one('res.currency', string='Moneda',
                                  help='Seleccione la moneda por la que será convertida los movimientos.',
                                  domain=[('name', 'in', ['MXN', 'USD'])])

    def _apply_filter(self, cr, uid, ids, context=None, type=None):
        dataset_ids = []
        _currency = ''
        data = self.browse(cr, uid, ids, context=None)
        if data.currency_id:
            _currency = data.currency_id.name
        and_product = ''
        if data.product_from and not data.product_to:
            and_product = " and product_code = '%s' " % data.product_from.default_code
        if data.product_from and data.product_to:
            and_product = " and product_code >= '%s' and product_code <= '%s' " % (data.product_from.default_code, data.product_to.default_code)

        query = "select id from r_purchases_by_product_data where state <> 'Cancelado' and create_doc_utc >='%s' " \
                "and create_doc_utc <= '%s' %s" \
                % (data.date_from, data.date_to, and_product)
        cr.execute(query)
        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            if type == 'show':
                cr.execute('delete from r_purchases_by_product where id=%s', (ids[0],))
        else:
            # data.unlink()
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids, _currency

    def show(self, cr, uid, ids, context=None):
        data_ids, currency = self._apply_filter(cr, uid, ids, context, type='show')
        return {
            'name': 'Title',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.purchases.by.product.data',
            'target': 'current',
            'context': {'currency2convert':currency},
            'domain': [('id', 'in', data_ids)],
        }

    def csv(self, cr, uid, ids, context=None):
        dataset_ids, currency = self._apply_filter(cr, uid, ids, context, type='csv')
        context.update({'currency2convert': currency})
        id = self.pool['r.purchases.by.product.data'].to_csv_by_ids(cr, uid, dataset_ids, context=context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id),
            'target': 'self',
        }

class ReportsPurchasesPurchasesByProductData(models.Model):
    _name = 'r.purchases.by.product.data'
    _table = 'r_purchases_by_product_data'
    _auto = False
    _order = 'id'

    @api.multi
    def _currency2convert(self):
        _currency = self._context['currency2convert']
        for row in self:
            if _currency in ['MXN', 'USD'] and row.currency_copy != _currency:
                row.currency = row.currency_copy
                _rate = float(row.rate)
                if row.currency_copy == 'USD':
                    row.product_price_unit = row.product_price_unit_copy * _rate
                    row.product_subtotal = row.product_subtotal_copy * _rate
                    row.invoice_total = row.invoice_total_copy * _rate
                elif row.currency_copy == 'MXN':
                    row.product_price_unit = row.product_price_unit_copy / _rate
                    row.product_subtotal = row.product_subtotal_copy /_rate
                    row.invoice_total = row.invoice_total_copy / _rate
            elif _currency == '' or row.currency_copy == _currency or _currency not in ['MXN', 'USD']:
                row.currency = row.currency_copy
                row.product_price_unit = row.product_price_unit_copy
                row.product_subtotal = row.product_subtotal_copy
                row.invoice_total = row.invoice_total_copy
            if row.type_doc == 'NCP':
                row.currency = -1 * row.currency
                row.product_price_unit = -1 * row.product_price_unit
                row.product_subtotal = -1 * row.product_subtotal
                row.invoice_total = -1 * row.invoice_total
                row.product_qty = -1 * row.product_qty_copy
            else:
                row.product_qty = row.product_qty_copy

    purchase_name = fields.Char(string=_('Folio Compra'))
    purchase_date = fields.Datetime(string=_('Fecha Compra'))
    purchase_agent = fields.Char(string=_('Agente de compra'))
    type_doc = fields.Char(string=_('Tipo de documento'))
    in_name = fields.Char(string=_('REN'))
    invoice_number = fields.Char(string=_('Folio Factura'))
    state = fields.Char(string=_('Estado Factura'))
    date_invoice = fields.Date(string=_('Fecha Factura'))
    currency_copy = fields.Char(string=_('Moneda'))
    currency = fields.Char(string=_('Moneda'), compute=_currency2convert)
    partner = fields.Char(string=_('Proveedor'))
    product_code = fields.Char(string=_('Código Producto'))
    product_name = fields.Char(string=_('Nombre Producto'))
    product_qty_copy = fields.Integer(string=_('Cantidad'))
    product_qty = fields.Integer(string=_('Cantidad'), compute=_currency2convert)
    product_price_unit_copy = fields.Float(string=_('Precio Unitario'), digits=(10, 2))
    product_price_unit = fields.Float(string=_('Precio Unitario'), digits=(10, 2), compute=_currency2convert)
    product_subtotal_copy = fields.Float(string=_('Subtotal'), digits=(10, 2), sum='Subtotal')
    product_subtotal = fields.Float(string=_('Subtotal'), digits=(10, 2), compute=_currency2convert)
    invoice_total_copy = fields.Float(string=_('Total'), digits=(10, 2), sum='Subtotal')
    invoice_total = fields.Float(string=_('Total'), digits=(10, 2), compute=_currency2convert)
    rate = fields.Float(string='Tipo de cambio', digits=(10, 2))
    factura_proveedor = fields.Char(string=_('Factura de Proveedor'))
    sucursal = fields.Char(string=_('Sucursal'))

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_purchases_by_product_data')
        tools.drop_view_if_exists(cr, 'r_purchases_by_product_data_inv_pic_po')
        cr.execute("""
            CREATE OR REPLACE view r_purchases_by_product_data_inv_pic_po as (
              SELECT
              po.date_order as purchase_date,
              rp.name partner,
              ai.supplier_invoice_number AS factura_proveedor,
	          acc.name as sucursal,
              (SELECT rpt.display_name FROM res_partner as rpt where id = (SELECT rs.partner_id FROM res_users as rs where id= po.write_uid)) as purchase_agent,
              rc.name as currency,
              po.name as purchase_name,
              sp.name in_name,
              COALESCE(ai.number,'') AS invoice_number,
              COALESCE(ai.state,'') AS invoice_state,
              COALESCE(ai.type, '') AS invoice_type,
              CASE WHEN ai.state IS NOT NULL and ai.date_invoice IS NULL THEN ai.invoice_datetime WHEN ai.date_invoice IS NOT NULL THEN ai.date_invoice ELSE SP.date_done END date_invoice ,
              CASE WHEN ai.state IS NOT NULL THEN pp.id ELSE 0 END as product_id,
              CASE WHEN ai.state IS NOT NULL THEN pp.default_code ELSE '' END as product_code,
              CASE WHEN ai.state IS NOT NULL THEN pp.name_template ELSE '' END as product_name,
              CASE WHEN ai.state IS NOT NULL THEN ail.quantity ELSE 0 END as product_qty,
              CASE WHEN ai.state IS NOT NULL THEN ail.price_unit ELSE 0 END as product_price_unit,
              CASE WHEN ai.state IS NOT NULL THEN (ail.quantity * ail.price_unit) ELSE 0 END as product_subtotal,
              ai.amount_total as invoice_total,
              doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS rate
            FROM account_invoice ai
              INNER JOIN res_partner rp on ai.partner_id = rp.id
              INNER JOIN res_currency rc on ai.currency_id= rc.id
              INNER JOIN account_invoice_line ail ON  ai.id = ail.invoice_id
              INNER JOIN product_product pp on ail.product_id = pp.id
              LEFT JOIN stock_picking sp ON ai.origin=sp.name
              LEFT JOIN purchase_order AS PO ON PO.name = SP.origin
              LEFT JOIN account_cost_center acc ON ai.cost_center_id = acc.id
            WHERE po.state NOT IN ('draft') --GROUP BY purchase_name
            )
        """)
        cr.execute("""
            create or replace view r_purchases_by_product_data as (
                SELECT
                     row_number() over (order by T1.purchase_date) as id,
                     T1.purchase_name, T1.in_name,
                     T1.purchase_date,
                     T1.purchase_date as create_doc,
                     T1.partner,
                     T1.factura_proveedor,
                     T1.sucursal,
                     T1.purchase_agent,
                     T1.date_invoice,
                     T1.date_invoice as create_doc_utc,
                     CASE T1.invoice_type
                     WHEN 'in_invoice' THEN 'FAC'
                     WHEN 'in_refund' THEN 'NCP'
                     ELSE '' END type_doc,
                     T1.invoice_number,
                     CASE T1.invoice_state
                     WHEN 'open' THEN 'Abierto'
                     WHEN 'cancel' THEN 'Cancelado'
                     WHEN 'draft' THEN 'Borrador'
                     WHEN 'paid' THEN 'Pagado'
                     ELSE '' END state,
                     T1.currency as currency_copy,
                     T1.product_id,
                     T1.product_code,
                     T1.product_name,
                     T1.product_qty as product_qty_copy,
                     T1.product_price_unit as product_price_unit_copy,
                     T1.product_subtotal as product_subtotal_copy,
                     T1.invoice_total as invoice_total_copy,
                     T1.rate
                    FROM (

--                        SELECT
                        --   po.date_order as purchase_date,
                        --   rp.name partner,
                        --   (SELECT rpt.display_name FROM res_partner as rpt where id = (SELECT rs.partner_id FROM res_users as rs where id= po.write_uid)) as purchase_agent,
                        --   rc.name as currency,
                        --   po.name as purchase_name,
                        --   CASE WHEN po.name IS NOT NULL THEN (SELECT string_agg(name,', ') FROM stock_picking WHERE origin=po.name) ELSE '' END in_name,
                        --   COALESCE(ai.number,'') AS invoice_number,
                        --   COALESCE(ai.state,'') AS invoice_state,
                        --   COALESCE(ai.type, '') AS invoice_type,
                        --   ai.date_invoice as date_invoice,
                        --   CASE WHEN ai.state IS NOT NULL THEN pp.id ELSE 0 END as product_id,
                        --   CASE WHEN ai.state IS NOT NULL THEN pp.default_code ELSE '' END as product_code,
                        --   CASE WHEN ai.state IS NOT NULL THEN pp.name_template ELSE '' END as product_name,
                        --   CASE WHEN ai.state IS NOT NULL THEN ail.quantity ELSE 0 END as product_qty,
                        --   CASE WHEN ai.state IS NOT NULL THEN ail.price_unit ELSE 0 END as product_price_unit,
                        --   CASE WHEN ai.state IS NOT NULL THEN (ail.quantity* ail.price_unit) ELSE 0 END as product_subtotal,
                        --   ai.amount_total as invoice_total,
                        --   doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS rate
                        -- FROM
                        --   purchase_order po
                        --   LEFT JOIN account_invoice ai ON po.name=ai.origin
                        --   INNER JOIN stock_picking sp ON po.name=sp.origin
                        --   LEFT JOIN account_invoice_line ail ON  ai.id = ail.invoice_id
                        --   LEFT JOIN product_product pp on ail.product_id = pp.id
                        --   INNER JOIN res_partner rp on po.partner_id = rp.id
                        --   INNER JOIN res_currency rc on ai.currency_id= rc.id
                        -- WHERE po.state NOT IN ('draft') --AND ai.origin LIKE 'PO%'
                        --
                        -- UNION ALL
                            SELECT
                              po.date_order as purchase_date,
                              CASE WHEN ai.state IS NOT NULL THEN rp.name ELSE '' END partner,
                              ai.supplier_invoice_number AS factura_proveedor,
                              acc.name AS sucursal,
                              (SELECT rpt.display_name FROM res_partner as rpt where id = (SELECT rs.partner_id FROM res_users as rs where id= po.write_uid)) as purchase_agent,
                              CASE WHEN ai.state IS NOT NULL THEN rc.name ELSE '' END currency,
                              po.name as purchase_name,
                              sp.name in_name ,
                              COALESCE(ai.number,'') AS invoice_number,
                              COALESCE(ai.state,'') AS invoice_state,
                              COALESCE(ai.type, '') AS invoice_type,
                              CASE WHEN ai.state IS NOT NULL and ai.date_invoice IS NULL THEN ai.invoice_datetime WHEN ai.date_invoice IS NOT NULL THEN ai.date_invoice ELSE SP.date_done END date_invoice ,
                              CASE WHEN ai.state IS NOT NULL THEN pp.id ELSE 0 END as product_id,
                              CASE WHEN ai.state IS NOT NULL THEN pp.default_code ELSE '' END as product_code,
                              CASE WHEN ai.state IS NOT NULL THEN pp.name_template ELSE '' END as product_name,
                              CASE WHEN ai.state IS NOT NULL THEN ail.quantity ELSE 0 END as product_qty,
                              CASE WHEN ai.state IS NOT NULL THEN ail.price_unit ELSE 0 END as product_price_unit,
                              CASE WHEN ai.state IS NOT NULL THEN (ail.quantity * ail.price_unit) ELSE 0 END as product_subtotal,
                              CASE WHEN ai.state IS NOT NULL THEN ai.amount_total ELSE 0 END invoice_total ,
                              doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS rate
                            FROM purchase_order po
                              INNER JOIN stock_picking sp ON sp.origin=po.name
                                LEFT JOIN  account_invoice ai on sp.name = ai.origin
                              LEFT JOIN account_invoice_line ail ON  ai.id = ail.invoice_id
                              LEFT JOIN  product_product AS pp ON pp.id = ail.product_id
                              LEFT JOIN res_partner rp on ai.partner_id = rp.id
                              LEFT JOIN res_currency rc on ai.currency_id= rc.id
                              LEFT JOIN r_purchases_by_product_data_inv_pic_po ipp ON ipp.purchase_name =po.name
                              LEFT JOIN account_cost_center acc ON ai.cost_center_id = acc.id
                            WHERE PO.state IN ('approved','done','except_invoice') AND ipp.purchase_name is null

                            UNION ALL

                            SELECT purchase_date, partner, factura_proveedor, sucursal, purchase_agent, currency, purchase_name,
                            in_name, invoice_number, invoice_state, invoice_type, date_invoice, product_id, product_code, product_name, product_qty,
                            product_price_unit, product_subtotal, invoice_total, rate
                            FROM r_purchases_by_product_data_inv_pic_po

                            UNION ALL

                            SELECT
                              null as purchase_date,
                              rp.name partner,
                              ai.supplier_invoice_number AS factura_proveedor,
                              acc.name AS sucursal,
                              (SELECT rpt.display_name FROM res_partner as rpt
                              WHERE id =(SELECT rs.partner_id FROM res_users as rs where id= ai.write_uid)) as purchase_agent,
                              rc.name as currency,
                              '' as purchase_name,
                              CASE WHEN ai.type = 'in_refund' AND ai.picking_dev_id is not NULL THEN (SELECT sp.origin FROM stock_picking as sp
                              WHERE id=ai.picking_dev_id) ELSE '' END as in_name,
                              COALESCE(ai.number,'') AS invoice_number,
                              ai.state AS invoice_state,
                              ai.type AS invoice_type,
                              ai.date_invoice as date_invoice,
                              pp.id as product_id,
                              COALESCE(pp.default_code,'') as product_code,
                              pp.name_template as product_name,
                              ail.quantity as product_qty,
                              ail.price_unit as product_price_unit,
                              (ail.quantity * ail.price_unit) as product_subtotal,
                              ai.amount_total as invoice_total,
                              doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS rate
                                FROM account_invoice ai
                                INNER JOIN account_invoice_line ail on ai.id=ail.invoice_id
                                  INNER JOIN product_product pp on ail.product_id = pp.id
                                  INNER JOIN res_partner rp on ai.partner_id =rp.id
                                  INNER JOIN res_currency rc on ai.currency_id= rc.id
                                  LEFT JOIN account_cost_center acc ON ai.cost_center_id = acc.id
                                WHERE (ai.type IN ('in_invoice') AND ai.origin is null) or ai.type IN ('in_refund')
                ) AS T1)
        """)

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    def to_csv_by_ids(self, cr, uid, ids, context=None):
        dataset = self.pool['r.purchases.by.product.data'].browse(cr, uid, ids, context=context)
        dateset_str = [['Folio Compra', 'Fecha Compra', 'Agente de Compra', 'Tipo de documento', 'REN', 'Folio Factura', 'Estado Factura',
                        'Fecha Factura', 'Moneda', 'Proveedor', 'Factura de Proveedor', 'Sucursal', 'Código Producto', 'Nombre Producto', 'Cantidad', 'Precio Unitario', 'Subtotal',
                        'Total', 'Tipo de cambio'
                         ]]

        sum_subtotal = 0.00
        sum_total = 0.00
        for row in dataset:
            if row.state != 'Cancelado':
                sum_subtotal += row.product_subtotal
                sum_total += row.invoice_total
            dateset_str.append([unicode(row.purchase_name),
                                str(self.utc_mexico_city(row.purchase_date) if row.purchase_date else ''),
                                self.set_default(row.purchase_agent),
                                self.set_default(row.type_doc),
                                self.set_default(row.in_name),
                                self.set_default(row.invoice_number),
                                self.set_default(row.state),
                                self.set_default(row.date_invoice),
                                self.set_default(row.currency),
                                unicode(self.set_default(row.partner)).encode('utf-8'),
                                self.set_default(row.factura_proveedor),
                                self.set_default(row.sucursal),
                                self.set_default(row.product_code),
                                unicode(self.set_default(row.product_name)).encode('utf-8'),
                                self.set_default(row.product_qty, 0),
                                self.set_default(row.product_price_unit, 0),
                                self.set_default(row.product_subtotal, 0),
                                self.set_default(row.invoice_total, 0), self.set_default(row.rate,0)])

        # dateset_str.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', sum_subtotal, sum_total, ])


        obj_osv = OCsv()

        file = obj_osv.csv_base64(dateset_str)
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': 'Purchases_by_product.csv',
            'type_file': 'csv',
            'file': file,
        }, context=None)

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
