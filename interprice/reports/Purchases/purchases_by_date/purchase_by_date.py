# -*- coding: utf-8 -*-
# @version     : 1.0
# @autor       : Viridiana Cruz Santos

from openerp import fields, models, api, _
from openerp.osv import osv, fields as fieldsv7
import openerp.tools as tools
import cStringIO
import base64
from olib.oCsv.oCsv import OCsv

class ReportsPurchasesPurchasesByDate(osv.osv_memory):
    _name = 'r.purchases_by_date'

    date_from = fields.Date(string='Fecha inicio')
    date_to = fields.Date(string='Fecha fin')
    partner_id = fields.Many2one('res.partner', string='Proveedor', domain=[('supplier', '=', True)])
    currency_id = fields.Many2one('res.currency', string='Moneda',
                                  help='Seleccione la moneda por la que será convertida los movimientos.',
                                  domain=[('name', 'in', ['MXN', 'USD'])])

    def _get_ids(self, cr, uid, ids, type):
        dataset_ids = []
        _currency = ''
        data = self.browse(cr, uid, ids, context=None)
        if data.currency_id:
            _currency = data.currency_id.name
        ipartner_id = ''
        if data.partner_id:
            ipartner_id = ' and supplier_id = %s ' % (data.partner_id.id,)

        cr.execute(" SELECT id FROM r_purchases_by_date_data WHERE create_doc_utc >='%s' "
                   "AND create_doc_utc <= '%s' %s "
                   % (data.date_from, data.date_to, ipartner_id,))

        if cr.rowcount:
            dataset_ids_tmp = cr.fetchall()
            dataset_ids = [int(i[0]) for i in dataset_ids_tmp]
            report_id = ids[0]
            if type == 'show':
                cr.execute('delete from r_purchases_by_date where id=%s', (report_id,))
        else:
            data.unlink()
            raise osv.except_osv(_('warning'), _('Not found records!.'))
        return dataset_ids, _currency

    def show(self, cr, uid, ids, context=None):
        data_ids, currency = self._get_ids(cr, uid, ids, type='show')
        return {
            'name': 'Compras por fecha / proveedor',
            'type': 'ir.actions.act_window',
            'view_type': 'tree',
            'view_mode': 'tree,',
            'res_model': 'r.purchases.by.date.data',
            'target': 'current',
            'context': {'currency2convert': currency},
            'domain': [('id', 'in', data_ids)],
        }

    def csv(self, cr, uid, ids, context=None):
        dataset_ids, currency = self._get_ids(cr, uid, ids, type='csv')
        context.update({'currency2convert': currency})
        id = self.pool['r.purchases.by.date.data'].to_csv_by_ids(cr, uid, dataset_ids, context=context)
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download/file?id=%s' % (id),
            'target': 'self',
        }

class ReportsPurchasesPurchasesByDateData(models.Model):
    _name = 'r.purchases.by.date.data'
    _table = 'r_purchases_by_date_data'
    _auto = False
    _order = 'id desc'

    @api.multi
    def _currency2convert(self):
        _currency = self._context['currency2convert']
        for row in self:
            if _currency in ['MXN', 'USD'] and row.currency_copy != _currency:
                row.currency = row.currency_copy
                _rate = float(row.rate)
                if row.currency_copy == 'USD':
                    row.tax = row.tax_copy * _rate
                    row.total = row.total_copy * _rate
                    row.residual = row.residual_copy * _rate
                    row.subtotal = row.subtotal_copy * _rate
                elif row.currency_copy == 'MXN':
                    row.tax = row.tax_copy / _rate
                    row.total = row.total_copy / _rate
                    row.residual = row.residual_copy / _rate
                    row.subtotal = row.subtotal_copy / _rate
            elif _currency == '' or row.currency_copy == _currency or _currency not in ['MXN', 'USD']:
                row.currency = row.currency_copy
                row.tax = row.tax_copy
                row.total = row.total_copy
                row.residual = row.residual_copy
                row.subtotal = row.subtotal_copy

    purchase_name = fields.Char(string='Folio de compra')
    purchase_date = fields.Datetime(string='Fecha de creación compra')
    invoice_number = fields.Char(string='Folio de la  factura')
    date_invoice = fields.Date(string='Fecha de creación factura')
    state = fields.Char(string='Estatus')
    currency_copy = fields.Char(string='Moneda')
    currency = fields.Char(string='Moneda', compute=_currency2convert)
    subtotal_copy = fields.Float(string='Subtotal', digits=(10, 2))
    subtotal = fields.Float(string='Subtotal', digits=(10, 2),compute=_currency2convert)
    tax_copy = fields.Float(string='IVA', digits=(10, 2))
    tax = fields.Float(string='IVA', compute=_currency2convert, digits=(10, 2))
    total_copy = fields.Float(string='Total', digits=(10, 2))
    total = fields.Float(string='Total', digits=(10, 2),compute=_currency2convert)
    residual_copy = fields.Float(string='Saldo', digits=(10, 2), related='account_invoice_id.residual')
    residual = fields.Float(string='Saldo', digits=(10, 2), compute=_currency2convert)
    supplier_name = fields.Char(string='Proveedor')
    picking_name = fields.Char(string='REN')
    picking_date = fields.Char(string='Fecha ingreso REN')
    rate = fields.Char(string='Tipo de cambio')
    factura_proveedor = fields.Char(string=_('Factura de proveedor'))
    sucursal = fields.Char(string=_('Sucursal'))
    account_invoice_id = fields.Many2one('account.invoice', string='account invoice')

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'r_purchases_by_date_data')
        tools.drop_view_if_exists(cr, 'r_purchases_by_date_data_inv_pic_po')
        # cr.execute("""
        #         -- CREATE OR REPLACE FUNCTION doc_tc (INT, INT, TEXT, TIMESTAMP)
        #         --   RETURNS numeric
        #         -- AS
        #         -- $$
        #         -- DECLARE
        #         --   tc_id ALIAS FOR $1;
        #         --   tc_company_id ALIAS FOR $2;
        #         --   tc_doc_type ALIAS FOR $3;
        #         --   tc_doc_date ALIAS FOR $4;
        #         --
        #         --   tc_tc numeric := 0;
        #         --
        #         --   company_currency_id INTEGER;
        #         --   company_currency_name VARCHAR(5);
        #         --   is_company_tc BOOLEAN :=FALSE ;
        #         --
        #         --   document_tc FLOAT;
        #         --   document_currency_id INTEGER;
        #         --   document_currency_name VARCHAR(5);
        #         --
        #         -- BEGIN
        #         --   /*Get currency company*/
        #         --   SELECT rcm.currency_id, rcu.name  INTO company_currency_id, company_currency_name
        #         --   FROM res_company rcm INNER JOIN res_currency rcu
        #         --       ON rcm.currency_id = rcu.id
        #         --   WHERE rcm.id=tc_company_id;
        #         --
        #         --   IF tc_doc_type = 'PO' THEN
        #         --     SELECT currency_rate_alter, currency_id, rc.name INTO document_tc, document_currency_id, document_currency_name
        #         --     FROM purchase_order po INNER JOIN res_currency rc ON po.currency_id = rc.id WHERE po.id = tc_id;
        #         --   ELSEIF tc_doc_type = 'INV' THEN
        #         --     SELECT ai.currency_rate_alter, ai.currency_id, rc.name INTO document_tc, document_currency_id, document_currency_name
        #         --     FROM account_invoice ai INNER JOIN res_currency rc ON ai.currency_id = rc.id WHERE ai.id = tc_id;
        #         --   END IF;
        #         --
        #         --   IF document_tc is NULL or document_tc = 0 THEN
        #         --     IF company_currency_name = 'USD' THEN
        #         --       SELECT rcr.rate INTO document_tc FROM res_currency rc INNER JOIN res_currency_rate rcr ON rc.id = rcr.currency_id WHERE
        #         --         rc.name = 'MXN' AND rcr.name <= tc_doc_date ORDER BY rcr.name DESC LIMIT 1;
        #         --     ELSEIF  company_currency_name = 'MXN' THEN
        #         --       SELECT rcr.rate INTO document_tc FROM res_currency rc INNER JOIN res_currency_rate rcr ON rc.id = rcr.currency_id WHERE
        #         --         rc.name = 'USD' AND rcr.name <= tc_doc_date ORDER BY rcr.name DESC LIMIT 1;
        #         --     END IF;
        #         --     is_company_tc :=TRUE ;
        #         --   END IF;
        #         --   IF document_tc IS NULL OR document_tc = 0 THEN
        #         --     document_tc :=1;
        #         --   END IF;
        #         --
        #         --   IF is_company_tc = TRUE THEN
        #         --     tc_tc :=  1/document_tc;
        #         --   ELSE
        #         --     tc_tc := document_tc;
        #         --   END IF;
        #         --   tc_tc:= trunc(tc_tc, 2);
        #         --   RETURN tc_tc;
        #         -- END;
        #         -- $$
        #         -- LANGUAGE plpgsql;
        # """)
        cr.execute("""
        create or replace view r_purchases_by_date_data_inv_pic_po as (
        SELECT
                PO.date_order AS purchase_date,
                PO.Name As purchase_name,
                COALESCE(AI.number, '') AS invoice_number,
                CASE WHEN ai.state IS NOT NULL and ai.date_invoice IS NULL THEN ai.invoice_datetime WHEN ai.date_invoice IS NOT NULL THEN ai.date_invoice ELSE SP.date_done END date_invoice ,
                AI.state invoice_state,
                AI.amount_untaxed subtotal,
                AI.amount_tax tax,
                AI.amount_total total,
                AI.residual residual,
                SP.name as picking_name,
                CAST(SP.date_done AT TIME ZONE 'utc' AS TEXT )AS picking_date,
                RP.id supplier_id,
                RP.name supplier_name,
                AI.supplier_invoice_number AS factura_proveedor,
                ACC.name AS Sucursal,
                RC.name currency,
                CAST(doc_tc(AI.id ,AI.company_id ,'INV',AI.invoice_datetime ) AS TEXT) AS rate
                ,COALESCE(AI.id,0) account_invoice_id
            FROM        account_invoice AS AI
                LEFT JOIN stock_picking AS SP
                ON SP.name = AI.origin
                LEFT JOIN purchase_order AS PO
                ON PO.name = SP.origin
                INNER JOIN res_partner RP
                ON PO.partner_id = RP.id
                INNER JOIN res_currency RC
                ON AI.currency_id = RC.id
                INNER JOIN res_partner RPT ON AI.partner_id = RPT.id
                LEFT JOIN account_cost_center ACC ON AI.cost_center_id = ACC.id
            WHERE       --AI.origin LIKE 'WH%' AND
            PO.state NOT IN ('draft'))
        """)
        cr.execute(""" create or replace view r_purchases_by_date_data as (
                         SELECT  row_number() over (order by T1.purchase_date) AS id,
                          T1.purchase_name,
                          T1.purchase_date,
                          T1.invoice_number,
                          T1.date_invoice,
                          T1.date_invoice create_doc_utc,
                          CASE T1.invoice_state
                          WHEN 'open' THEN 'Abierto'
                          WHEN 'cancel' THEN 'Cancelado'
                          WHEN 'draft' THEN 'Borrador'
                          WHEN 'paid' THEN 'Pagado'
                          ELSE '' END state,
                          T1.supplier_id,
                          T1.supplier_name,
                          T1.factura_proveedor,
                          T1.sucursal,
                          T1.subtotal as subtotal_copy,
                          T1.tax as tax_copy,
                          T1.total as total_copy,
                          T1.residual as residual_copy,
                          T1.currency as currency_copy,
                          T1.picking_name,
                          T1.picking_date,
                          T1.rate,
                          T1.account_invoice_id
                          FROM (
                           -- SELECT po.date_order purchase_date, po.name purchase_name,
                              --COALESCE(ai.number,'') invoice_number,
                              --ai.date_invoice date_invoice,
                              --ai.state invoice_state, ai.amount_untaxed subtotal, ai.amount_tax tax,
                              --ai.amount_total total, ai.residual residual,
                              --CASE  WHEN po.name IS NOT NULL THEN (SELECT string_agg(name,', ') FROM stock_picking
                                --WHERE origin=po.name) ELSE '' END picking_name,
                              --CASE  WHEN po.name IS NOT NULL THEN (SELECT string_agg(cast(date_done AT TIME ZONE 'utc' AS VARCHAR(19)),', ')
                                --FROM stock_picking WHERE origin=po.name) ELSE '' END picking_date,
                              --rp.id supplier_id, rp.name supplier_name,
                              --rc.name currency,
                              --(SELECT string_agg(cast(rcr.rate AS VARCHAR(4)),', ')
                                --FROM res_currency rc
                                --INNER JOIN res_currency_rate rcr ON rc.id = rcr.currency_id
                                --INNER JOIN stock_picking sp On po.name=sp.origin WHERE rc.name='MXN'
                                --AND rcr.name<=sp.date GROUP BY rcr.id ORDER BY rcr.name DESC LIMIT 1) rate
                          --  FROM purchase_order po
                            --  RIGHT JOIN account_invoice ai ON po.name=ai.origin
                              --INNER JOIN res_partner rp ON po.partner_id=rp.id
                              --INNER JOIN res_currency rc ON po.currency_id = rc.id
                            --WHERE po.state NOT IN ('draft') --AND ai.origin LIKE 'PO%'

                            --UNION ALL

                            SELECT PO.date_order AS purchase_date,
                                        PO.Name As purchase_name,
                                        COALESCE(AI.number, '') AS invoice_number,
                                       CASE WHEN ai.state IS NOT NULL and ai.date_invoice IS NULL THEN ai.invoice_datetime WHEN ai.date_invoice IS NOT NULL THEN ai.date_invoice ELSE SP.date_done END date_invoice ,
                                        COALESCE(AI.state,'')invoice_state,
                                        COALESCE(AI.amount_untaxed,0) subtotal,
                                       COALESCE(AI.amount_tax,0) tax,
                                       COALESCE(AI.amount_total,0 ) total,
                                       COALESCE(AI.residual,0) residual,
                                        SP.name as picking_name,
                                        CAST(SP.date_done AT TIME ZONE 'utc' AS TEXT ) picking_date,
                                        RP.id supplier_id,
                                        COALESCE(RP.name,'') supplier_name,
                                        AI.supplier_invoice_number AS factura_proveedor,
                                        ACC.name AS sucursal,
                                        COALESCE(RC.name,'') currency,
                                        CAST(doc_tc(AI.id ,AI.company_id ,'INV',AI.invoice_datetime ) AS TEXT) AS rate,
                                        COALESCE(AI.id,0) account_invoice_id
                            FROM  purchase_order AS PO
                              INNER JOIN stock_picking AS SP
                                ON PO.name = SP.origin
                              LEFT JOIN  account_invoice AS AI
                                ON SP.name = AI.origin
                              LEFT JOIN res_partner RP
                                ON AI.partner_id = RP.id
                              LEFT JOIN res_currency RC
                                ON AI.currency_id = RC.id
                            LEFT JOIN r_purchases_by_date_data_inv_pic_po ipp ON PO.name = ipp.purchase_name
                            LEFT JOIN account_cost_center ACC ON AI.cost_center_id = ACC.id
                            WHERE       --AI.origin LIKE 'WH%' AND
                              ipp.purchase_name is NULL
                            AND PO.state IN ('approved','done','except_invoice')

                             UNION ALL
                            SELECT
                                purchase_date, purchase_name, invoice_number, date_invoice, invoice_state, subtotal,
                                tax, total, residual, picking_name, picking_date,supplier_id, supplier_name, factura_proveedor, sucursal,
                                currency, rate, account_invoice_id FROM r_purchases_by_date_data_inv_pic_po
                            UNION ALL

                            SELECT null purchase_date, '' purchase_name,
                              COALESCE(ai.number,'') invoice_number,
                              ai.date_invoice date_invoice,
                              ai.state invoice_state, ai.amount_untaxed subtotal, ai.amount_tax tax,
                              ai.amount_total total, ai.residual residual,
                              CASE WHEN ai.picking_dev_id IS NOT NULL THEN (SELECT string_agg(name,', ')
                                FROM stock_picking
                                WHERE id=ai.picking_dev_id) ELSE '' END picking_name,
                              CASE  WHEN ai.picking_dev_id IS NOT NULL THEN
                                (SELECT string_agg(cast(date_done AT TIME ZONE 'utc' AS VARCHAR(19)),', ')
                                    FROM stock_picking WHERE id=ai.picking_dev_id) ELSE '' END picking_date,
                              rp.id supplier_id, rp.name supplier_name,
                              ai.supplier_invoice_number AS factura_proveedor,
                              acc.name AS sucursal,
                              rc.name currency,
                              CAST(doc_tc(ai.id ,ai.company_id ,'INV',ai.invoice_datetime ) AS TEXT) AS rate,
                              COALESCE(ai.id,0) account_invoice_id
                            FROM account_invoice ai
                              INNER JOIN res_partner rp ON ai.partner_id=rp.id
                              INNER JOIN res_currency rc ON ai.currency_id = rc.id
                              LEFT JOIN account_cost_center acc ON ai.cost_center_id = acc.id
                            WHERE (ai.type IN ('in_invoice') AND ai.picking_dev_id is null) or ai.type IN ('in_refund'))
                            AS T1 )""")

    def utc_mexico_city(self, datetime_from):
        from datetime import datetime as dt
        from dateutil import tz
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Mexico_City')
        utc = dt.strptime(datetime_from, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = dt.strftime(utc.astimezone(to_zone), '%Y-%m-%d %H:%M:%S')
        return central

    def to_csv_by_ids(self, cr, uid, ids, context=None):
        print context
        dataset = self.pool['r.purchases.by.date.data'].browse(cr, uid, ids, context=context)
        dateset_str =[['Folio de compra', 'Fecha de creación compra', 'REN', 'Fecha ingreso REN', 'Folio factura',
                       'Fecha de creación factura', 'Proveedor', 'Factura de proveedor', 'Sucursal', 'Moneda', 'Estatus', 'Subtotal',
                      'IVA', 'Total', 'Saldo', 'Tipo de cambio', ]]

        sum_subtotal = 0.00
        sum_total = 0.00
        sum_tax = 0.00
        sum_residual = 0.00
        for row in dataset:
            if row.state != 'Cancelado':
                sum_subtotal += row.subtotal
                sum_tax += row.tax
                sum_total += row.total
                sum_residual += row.residual

            dateset_str.append([unicode(row.purchase_name),
                                str(self.utc_mexico_city(row.purchase_date) if row.purchase_date else ''),
                                self.set_default(row.picking_name),
                                self.set_default(row.picking_date),
                                self.set_default(row.invoice_number),
                                self.set_default(row.date_invoice),
                                self.set_default(row.supplier_name),
                                self.set_default(row.factura_proveedor),
                                self.set_default(row.sucursal),
                                self.set_default(row.currency),
                                self.set_default(row.state),
                                self.set_default(row.subtotal, 0),
                                self.set_default(row.tax, 0),
                                self.set_default(row.total, 0),
                                self.set_default(row.residual, 0),
                                self.set_default(row.rate, 0)])

        # dateset_str.append(['', '', '', '', '', '', '', '', '', sum_subtotal, sum_tax, sum_total, sum_residual, '', ])

        obj_osv = OCsv()

        file = obj_osv.csv_base64(dateset_str)
        return self.pool['r.download'].create(cr, uid, vals={
            'file_name': 'Purchases_by_date.csv',
            'type_file': 'csv',
            'file': file,
        }, context=None)

    def set_default(self, val, default=''):
        if val:
            return val
        else:
            return default
