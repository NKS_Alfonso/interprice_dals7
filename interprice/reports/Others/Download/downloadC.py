#! -*- config: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Jose J. H. Bautista'
# __email__ = 'jose.bautista@gvadeto.com'

from openerp import http
from openerp.http import request
from openerp.addons.web.controllers.main import content_disposition
import base64


class ReportsOthersDownloadC(http.Controller):

    @http.route('/web/binary/download/file', type='http', auth='public')
    def download_file(self, id):
        download_model = request.registry['r.download']
        cr, uid, context = request.cr, request.uid, request.context
        id_int = int(str(id))
        download_resource = download_model.read(cr, uid, [id_int], context=context)
        if not download_resource:
            return request.not_found()
        else:
            # download_resource = download_model.browse(cr, uid, download_ids, context)
            if download_resource and download_resource[0]:
                download_resource_first = download_resource[0]
                file = base64.b64decode(download_resource_first['file'])
                filename = download_resource_first['file_name']
                cr.execute('delete from r_download where id=%s', (id_int,))
                return request.make_response(file,
                                             [('Content-Type', 'application/octet-stream'),
                                              ('Content-Disposition', content_disposition(filename))])
            else:
                return request.not_found()
        return request.not_found()