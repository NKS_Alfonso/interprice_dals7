#! -*- config: utf-8 -*-
# __copyright__ = 'GVADETO'
# __author__ = 'Jose J. H. Bautista'
# __email__ = 'jose.bautista@gvadeto.com'

from openerp import fields, api, models, _


class ReportsOthersDownloadM(models.Model):
    _name = 'r.download'

    file_name = fields.Char(string=_('File name'))  # In underscore example_sdfs.csv
    type_file = fields.Char(string=_('Type file'))  # csv, xml, jpg,
    file = fields.Binary(string=_('File'))
    model_name = fields.Char(string=_('Model name'))  # r.download
