# -*- coding: utf-8 -*-
import logging


class oLog:
    _logger = None

    def __init__(self, class_name=None):
        self._logger = logging.getLogger(class_name)

    def debug(self, msg):
        self._logger.debug(msg)
        return self

    def info(self, msg):
        self._logger.info(msg)
        return self

    def warning(self, msg):
        self._logger.warning(msg)
        return self

    def error(self, msg):
        self._logger.error(msg)
        return self

    def critical(self, msg):
        self._logger.critical(msg)
        return self
