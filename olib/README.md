# Odoo Library (oLib) #

This README would normally document whatever steps are necessary to get your application up and running.

### Prefix your commit with ###
* [IMP] for improvements
* [FIX] for bug fixes
* [REF] for refactoring
* [ADD] for adding new resources
* [REM] for removing of resources
* [MERGE] for merge commits (only for forward/back-port)
* [CLA] for signing the Odoo Individual Contributor License [Read more](https://www.odoo.com/documentation/8.0/reference/guidelines.html#git)

e.g 
```
[FIX] website, website_mail: remove unused alert div, fixes look of input-group-btn

Bootstrap's CSS depends on the input-group-btn
element being the first/last child of its parent.
This was not the case because of the invisible
and useless alert.

[IMP] fields: reduce memory footprint of list/set field attributes

[REF] web: add module system to the web client

This commit introduces a new module system for the javascript code.
Instead of using global ...
```
