# -*- coding: utf-8 -*-
import csv
from StringIO import StringIO
import base64


class OCsv:

    def __init__(self):
        pass

    def csv_base64(self, data):
        buffer = StringIO()
        csv.writer(buffer).writerows(data)
        csv_base64 = base64.b64encode(buffer.getvalue())
        return csv_base64