# -*- coding: utf-8 -*-

import psycopg2 as psql
import psycopg2.extras
import os, ConfigParser


class PsqlConnector:

    __host = 'localhost'
    __port = 5432
    __database = None
    __user = None
    __password = None
    __connection = None
    __cursor = None

    def __init__(self, host=None, database=None, user=None, password=None, port=None, server=None):
        try:
            if server:
                if server == 'main':
                    self.__database = database
                    self.__boot_server()
                else:
                    raise Exception("The server(%s) not exists in the dictionary" % (server ))
            else:
                if host:
                    self.__host = host
                if database:
                    self.__database = database
                if user:
                    self.__user = user
                if password:
                    self.__password = password
                if port:
                    self.__port = port
                self.__connect()
        except Exception, e:
            print (e)

    def __connect(self):
        '''

        :return: Object connection postgresql
        '''
        try:
            str_connection = "host='%s' dbname='%s' user='%s' password='%s'" % \
                                  (self.__host, self.__database, self.__user, self.__password)
            self.__connection = psql.connect(str_connection)
            self.__cursor = self.__connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        except Exception, e:
            print e
        return self.__connection

    def __boot_server(self):
        '''

        :return:
        '''
        try:
            # path = os.getcwd()
            path = '/home/joseb/PycharmProjects/interprice/debian/openerp-server-joseb.conf'
            if os.path.exists(path):
                config = ConfigParser.ConfigParser()
                if config.read(path):
                    if 'options' in config.sections():
                        options_at_section = config.options('options')
                        if 'db_host' in options_at_section  and \
                            'db_password' in options_at_section and \
                            'db_user' in options_at_section and \
                            'db_name' in options_at_section:

                            self.__host = config.get('options', 'db_host')
                            self.__user = config.get('options', 'db_user')
                            self.__password = config.get('options', 'db_password')
                            if config.get('options', 'db_name') != 'False' and not self.__database:
                                self.__database = config.get('options', 'db_name')
                            return self.__connect()
                else:
                    raise Exception("The file no is readable!")
            else:
                raise Exception("The file configuration not exits, verify the path (%s)." % (path))
        except Exception, e:
            print e

    def execute(self, query, params=None, autocommit=True):
        self.__cursor.execute(query, params)
        if autocommit:
            self.commit()
            return self.__cursor

    def close(self):
        self.__connection.close()

    def commit(self):
        self.__connection.commit()

    def rollback(self):
        self.__connection.rollback()

    def one(self):
        return self.__cursor.fetchone()

    def many(self, size=0):
        _many = None
        if size > 0:
            _many = self.__cursor.fetchmany(size)
        else:
            _many = self.__cursor.fetchall()
        return _many

    def count(self):
        return self.__cursor.rowcount


# if __name__ == '__main__':
#     g = PsqlConnector(server='main', database='inter_04')
#     # print g.execute("SELECT * FROM stock_pack_operation")
#     # print g.count()
#     cars = (1, 'Audi', 52642)
#     print type(cars)
